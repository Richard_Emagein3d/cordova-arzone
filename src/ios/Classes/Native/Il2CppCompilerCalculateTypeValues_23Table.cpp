﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// DG.Tweening.EaseFunction
struct EaseFunction_t3531141372;
// DG.Tweening.Plugins.Core.ITweenPlugin
struct ITweenPlugin_t1960132748;
// DG.Tweening.Plugins.Core.PathCore.ABSPathDecoder
struct ABSPathDecoder_t2613982196;
// DG.Tweening.Plugins.Core.PathCore.CatmullRomDecoder
struct CatmullRomDecoder_t2053048079;
// DG.Tweening.Plugins.Core.PathCore.ControlPoint[]
struct ControlPointU5BU5D_t1567961855;
// DG.Tweening.Plugins.Core.PathCore.LinearDecoder
struct LinearDecoder_t2708327777;
// DG.Tweening.Sequence
struct Sequence_t2050373119;
// DG.Tweening.Tween
struct Tween_t2342918553;
// DG.Tweening.TweenCallback
struct TweenCallback_t3727756325;
// DG.Tweening.TweenCallback`1<System.Int32>
struct TweenCallback_1_t3009965658;
// DG.Tweening.Tween[]
struct TweenU5BU5D_t1320466404;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Collections.Generic.Dictionary`2<System.Type,DG.Tweening.Plugins.Core.ITweenPlugin>
struct Dictionary_2_t109512516;
// System.Collections.Generic.List`1<DG.Tweening.Tween>
struct List_1_t3814993295;
// System.Collections.Generic.List`1<System.Char>
struct List_1_t811567916;
// System.Collections.Generic.Stack`1<DG.Tweening.Tween>
struct Stack_1_t3186308008;
// System.Single[]
struct SingleU5BU5D_t1444911251;
// System.String
struct String_t;
// System.Text.StringBuilder
struct StringBuilder_t;
// System.Type
struct Type_t;
// System.Void
struct Void_t1185182177;
// UnityEngine.Light
struct Light_t3756812086;
// UnityEngine.Material
struct Material_t340375123;
// UnityEngine.RectOffset
struct RectOffset_t1369453676;
// UnityEngine.Transform
struct Transform_t3600365921;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1718750761;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CWAITFORCOMPLETIONU3ED__14_T1419758899_H
#define U3CWAITFORCOMPLETIONU3ED__14_T1419758899_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.DOTweenComponent/<WaitForCompletion>d__14
struct  U3CWaitForCompletionU3Ed__14_t1419758899  : public RuntimeObject
{
public:
	// System.Int32 DG.Tweening.Core.DOTweenComponent/<WaitForCompletion>d__14::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object DG.Tweening.Core.DOTweenComponent/<WaitForCompletion>d__14::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// DG.Tweening.Tween DG.Tweening.Core.DOTweenComponent/<WaitForCompletion>d__14::t
	Tween_t2342918553 * ___t_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CWaitForCompletionU3Ed__14_t1419758899, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CWaitForCompletionU3Ed__14_t1419758899, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_t_2() { return static_cast<int32_t>(offsetof(U3CWaitForCompletionU3Ed__14_t1419758899, ___t_2)); }
	inline Tween_t2342918553 * get_t_2() const { return ___t_2; }
	inline Tween_t2342918553 ** get_address_of_t_2() { return &___t_2; }
	inline void set_t_2(Tween_t2342918553 * value)
	{
		___t_2 = value;
		Il2CppCodeGenWriteBarrier((&___t_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CWAITFORCOMPLETIONU3ED__14_T1419758899_H
#ifndef U3CWAITFORELAPSEDLOOPSU3ED__17_T1514468280_H
#define U3CWAITFORELAPSEDLOOPSU3ED__17_T1514468280_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.DOTweenComponent/<WaitForElapsedLoops>d__17
struct  U3CWaitForElapsedLoopsU3Ed__17_t1514468280  : public RuntimeObject
{
public:
	// System.Int32 DG.Tweening.Core.DOTweenComponent/<WaitForElapsedLoops>d__17::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object DG.Tweening.Core.DOTweenComponent/<WaitForElapsedLoops>d__17::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// DG.Tweening.Tween DG.Tweening.Core.DOTweenComponent/<WaitForElapsedLoops>d__17::t
	Tween_t2342918553 * ___t_2;
	// System.Int32 DG.Tweening.Core.DOTweenComponent/<WaitForElapsedLoops>d__17::elapsedLoops
	int32_t ___elapsedLoops_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CWaitForElapsedLoopsU3Ed__17_t1514468280, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CWaitForElapsedLoopsU3Ed__17_t1514468280, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_t_2() { return static_cast<int32_t>(offsetof(U3CWaitForElapsedLoopsU3Ed__17_t1514468280, ___t_2)); }
	inline Tween_t2342918553 * get_t_2() const { return ___t_2; }
	inline Tween_t2342918553 ** get_address_of_t_2() { return &___t_2; }
	inline void set_t_2(Tween_t2342918553 * value)
	{
		___t_2 = value;
		Il2CppCodeGenWriteBarrier((&___t_2), value);
	}

	inline static int32_t get_offset_of_elapsedLoops_3() { return static_cast<int32_t>(offsetof(U3CWaitForElapsedLoopsU3Ed__17_t1514468280, ___elapsedLoops_3)); }
	inline int32_t get_elapsedLoops_3() const { return ___elapsedLoops_3; }
	inline int32_t* get_address_of_elapsedLoops_3() { return &___elapsedLoops_3; }
	inline void set_elapsedLoops_3(int32_t value)
	{
		___elapsedLoops_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CWAITFORELAPSEDLOOPSU3ED__17_T1514468280_H
#ifndef U3CWAITFORKILLU3ED__16_T383710286_H
#define U3CWAITFORKILLU3ED__16_T383710286_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.DOTweenComponent/<WaitForKill>d__16
struct  U3CWaitForKillU3Ed__16_t383710286  : public RuntimeObject
{
public:
	// System.Int32 DG.Tweening.Core.DOTweenComponent/<WaitForKill>d__16::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object DG.Tweening.Core.DOTweenComponent/<WaitForKill>d__16::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// DG.Tweening.Tween DG.Tweening.Core.DOTweenComponent/<WaitForKill>d__16::t
	Tween_t2342918553 * ___t_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CWaitForKillU3Ed__16_t383710286, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CWaitForKillU3Ed__16_t383710286, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_t_2() { return static_cast<int32_t>(offsetof(U3CWaitForKillU3Ed__16_t383710286, ___t_2)); }
	inline Tween_t2342918553 * get_t_2() const { return ___t_2; }
	inline Tween_t2342918553 ** get_address_of_t_2() { return &___t_2; }
	inline void set_t_2(Tween_t2342918553 * value)
	{
		___t_2 = value;
		Il2CppCodeGenWriteBarrier((&___t_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CWAITFORKILLU3ED__16_T383710286_H
#ifndef U3CWAITFORPOSITIONU3ED__18_T1811625055_H
#define U3CWAITFORPOSITIONU3ED__18_T1811625055_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.DOTweenComponent/<WaitForPosition>d__18
struct  U3CWaitForPositionU3Ed__18_t1811625055  : public RuntimeObject
{
public:
	// System.Int32 DG.Tweening.Core.DOTweenComponent/<WaitForPosition>d__18::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object DG.Tweening.Core.DOTweenComponent/<WaitForPosition>d__18::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// DG.Tweening.Tween DG.Tweening.Core.DOTweenComponent/<WaitForPosition>d__18::t
	Tween_t2342918553 * ___t_2;
	// System.Single DG.Tweening.Core.DOTweenComponent/<WaitForPosition>d__18::position
	float ___position_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CWaitForPositionU3Ed__18_t1811625055, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CWaitForPositionU3Ed__18_t1811625055, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_t_2() { return static_cast<int32_t>(offsetof(U3CWaitForPositionU3Ed__18_t1811625055, ___t_2)); }
	inline Tween_t2342918553 * get_t_2() const { return ___t_2; }
	inline Tween_t2342918553 ** get_address_of_t_2() { return &___t_2; }
	inline void set_t_2(Tween_t2342918553 * value)
	{
		___t_2 = value;
		Il2CppCodeGenWriteBarrier((&___t_2), value);
	}

	inline static int32_t get_offset_of_position_3() { return static_cast<int32_t>(offsetof(U3CWaitForPositionU3Ed__18_t1811625055, ___position_3)); }
	inline float get_position_3() const { return ___position_3; }
	inline float* get_address_of_position_3() { return &___position_3; }
	inline void set_position_3(float value)
	{
		___position_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CWAITFORPOSITIONU3ED__18_T1811625055_H
#ifndef U3CWAITFORREWINDU3ED__15_T2339862385_H
#define U3CWAITFORREWINDU3ED__15_T2339862385_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.DOTweenComponent/<WaitForRewind>d__15
struct  U3CWaitForRewindU3Ed__15_t2339862385  : public RuntimeObject
{
public:
	// System.Int32 DG.Tweening.Core.DOTweenComponent/<WaitForRewind>d__15::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object DG.Tweening.Core.DOTweenComponent/<WaitForRewind>d__15::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// DG.Tweening.Tween DG.Tweening.Core.DOTweenComponent/<WaitForRewind>d__15::t
	Tween_t2342918553 * ___t_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CWaitForRewindU3Ed__15_t2339862385, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CWaitForRewindU3Ed__15_t2339862385, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_t_2() { return static_cast<int32_t>(offsetof(U3CWaitForRewindU3Ed__15_t2339862385, ___t_2)); }
	inline Tween_t2342918553 * get_t_2() const { return ___t_2; }
	inline Tween_t2342918553 ** get_address_of_t_2() { return &___t_2; }
	inline void set_t_2(Tween_t2342918553 * value)
	{
		___t_2 = value;
		Il2CppCodeGenWriteBarrier((&___t_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CWAITFORREWINDU3ED__15_T2339862385_H
#ifndef U3CWAITFORSTARTU3ED__19_T3428776308_H
#define U3CWAITFORSTARTU3ED__19_T3428776308_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.DOTweenComponent/<WaitForStart>d__19
struct  U3CWaitForStartU3Ed__19_t3428776308  : public RuntimeObject
{
public:
	// System.Int32 DG.Tweening.Core.DOTweenComponent/<WaitForStart>d__19::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object DG.Tweening.Core.DOTweenComponent/<WaitForStart>d__19::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// DG.Tweening.Tween DG.Tweening.Core.DOTweenComponent/<WaitForStart>d__19::t
	Tween_t2342918553 * ___t_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CWaitForStartU3Ed__19_t3428776308, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CWaitForStartU3Ed__19_t3428776308, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_t_2() { return static_cast<int32_t>(offsetof(U3CWaitForStartU3Ed__19_t3428776308, ___t_2)); }
	inline Tween_t2342918553 * get_t_2() const { return ___t_2; }
	inline Tween_t2342918553 ** get_address_of_t_2() { return &___t_2; }
	inline void set_t_2(Tween_t2342918553 * value)
	{
		___t_2 = value;
		Il2CppCodeGenWriteBarrier((&___t_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CWAITFORSTARTU3ED__19_T3428776308_H
#ifndef DEBUGGER_T1756157868_H
#define DEBUGGER_T1756157868_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.Debugger
struct  Debugger_t1756157868  : public RuntimeObject
{
public:

public:
};

struct Debugger_t1756157868_StaticFields
{
public:
	// System.Int32 DG.Tweening.Core.Debugger::logPriority
	int32_t ___logPriority_0;

public:
	inline static int32_t get_offset_of_logPriority_0() { return static_cast<int32_t>(offsetof(Debugger_t1756157868_StaticFields, ___logPriority_0)); }
	inline int32_t get_logPriority_0() const { return ___logPriority_0; }
	inline int32_t* get_address_of_logPriority_0() { return &___logPriority_0; }
	inline void set_logPriority_0(int32_t value)
	{
		___logPriority_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEBUGGER_T1756157868_H
#ifndef BOUNCE_T852251727_H
#define BOUNCE_T852251727_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.Easing.Bounce
struct  Bounce_t852251727  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOUNCE_T852251727_H
#ifndef EXTENSIONS_T3835977652_H
#define EXTENSIONS_T3835977652_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.Extensions
struct  Extensions_t3835977652  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXTENSIONS_T3835977652_H
#ifndef TWEENMANAGER_T374091826_H
#define TWEENMANAGER_T374091826_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.TweenManager
struct  TweenManager_t374091826  : public RuntimeObject
{
public:

public:
};

struct TweenManager_t374091826_StaticFields
{
public:
	// System.Int32 DG.Tweening.Core.TweenManager::maxActive
	int32_t ___maxActive_3;
	// System.Int32 DG.Tweening.Core.TweenManager::maxTweeners
	int32_t ___maxTweeners_4;
	// System.Int32 DG.Tweening.Core.TweenManager::maxSequences
	int32_t ___maxSequences_5;
	// System.Boolean DG.Tweening.Core.TweenManager::hasActiveTweens
	bool ___hasActiveTweens_6;
	// System.Boolean DG.Tweening.Core.TweenManager::hasActiveDefaultTweens
	bool ___hasActiveDefaultTweens_7;
	// System.Boolean DG.Tweening.Core.TweenManager::hasActiveLateTweens
	bool ___hasActiveLateTweens_8;
	// System.Boolean DG.Tweening.Core.TweenManager::hasActiveFixedTweens
	bool ___hasActiveFixedTweens_9;
	// System.Boolean DG.Tweening.Core.TweenManager::hasActiveManualTweens
	bool ___hasActiveManualTweens_10;
	// System.Int32 DG.Tweening.Core.TweenManager::totActiveTweens
	int32_t ___totActiveTweens_11;
	// System.Int32 DG.Tweening.Core.TweenManager::totActiveDefaultTweens
	int32_t ___totActiveDefaultTweens_12;
	// System.Int32 DG.Tweening.Core.TweenManager::totActiveLateTweens
	int32_t ___totActiveLateTweens_13;
	// System.Int32 DG.Tweening.Core.TweenManager::totActiveFixedTweens
	int32_t ___totActiveFixedTweens_14;
	// System.Int32 DG.Tweening.Core.TweenManager::totActiveManualTweens
	int32_t ___totActiveManualTweens_15;
	// System.Int32 DG.Tweening.Core.TweenManager::totActiveTweeners
	int32_t ___totActiveTweeners_16;
	// System.Int32 DG.Tweening.Core.TweenManager::totActiveSequences
	int32_t ___totActiveSequences_17;
	// System.Int32 DG.Tweening.Core.TweenManager::totPooledTweeners
	int32_t ___totPooledTweeners_18;
	// System.Int32 DG.Tweening.Core.TweenManager::totPooledSequences
	int32_t ___totPooledSequences_19;
	// System.Int32 DG.Tweening.Core.TweenManager::totTweeners
	int32_t ___totTweeners_20;
	// System.Int32 DG.Tweening.Core.TweenManager::totSequences
	int32_t ___totSequences_21;
	// System.Boolean DG.Tweening.Core.TweenManager::isUpdateLoop
	bool ___isUpdateLoop_22;
	// DG.Tweening.Tween[] DG.Tweening.Core.TweenManager::_activeTweens
	TweenU5BU5D_t1320466404* ____activeTweens_23;
	// DG.Tweening.Tween[] DG.Tweening.Core.TweenManager::_pooledTweeners
	TweenU5BU5D_t1320466404* ____pooledTweeners_24;
	// System.Collections.Generic.Stack`1<DG.Tweening.Tween> DG.Tweening.Core.TweenManager::_PooledSequences
	Stack_1_t3186308008 * ____PooledSequences_25;
	// System.Collections.Generic.List`1<DG.Tweening.Tween> DG.Tweening.Core.TweenManager::_KillList
	List_1_t3814993295 * ____KillList_26;
	// System.Int32 DG.Tweening.Core.TweenManager::_maxActiveLookupId
	int32_t ____maxActiveLookupId_27;
	// System.Boolean DG.Tweening.Core.TweenManager::_requiresActiveReorganization
	bool ____requiresActiveReorganization_28;
	// System.Int32 DG.Tweening.Core.TweenManager::_reorganizeFromId
	int32_t ____reorganizeFromId_29;
	// System.Int32 DG.Tweening.Core.TweenManager::_minPooledTweenerId
	int32_t ____minPooledTweenerId_30;
	// System.Int32 DG.Tweening.Core.TweenManager::_maxPooledTweenerId
	int32_t ____maxPooledTweenerId_31;
	// System.Boolean DG.Tweening.Core.TweenManager::_despawnAllCalledFromUpdateLoopCallback
	bool ____despawnAllCalledFromUpdateLoopCallback_32;

public:
	inline static int32_t get_offset_of_maxActive_3() { return static_cast<int32_t>(offsetof(TweenManager_t374091826_StaticFields, ___maxActive_3)); }
	inline int32_t get_maxActive_3() const { return ___maxActive_3; }
	inline int32_t* get_address_of_maxActive_3() { return &___maxActive_3; }
	inline void set_maxActive_3(int32_t value)
	{
		___maxActive_3 = value;
	}

	inline static int32_t get_offset_of_maxTweeners_4() { return static_cast<int32_t>(offsetof(TweenManager_t374091826_StaticFields, ___maxTweeners_4)); }
	inline int32_t get_maxTweeners_4() const { return ___maxTweeners_4; }
	inline int32_t* get_address_of_maxTweeners_4() { return &___maxTweeners_4; }
	inline void set_maxTweeners_4(int32_t value)
	{
		___maxTweeners_4 = value;
	}

	inline static int32_t get_offset_of_maxSequences_5() { return static_cast<int32_t>(offsetof(TweenManager_t374091826_StaticFields, ___maxSequences_5)); }
	inline int32_t get_maxSequences_5() const { return ___maxSequences_5; }
	inline int32_t* get_address_of_maxSequences_5() { return &___maxSequences_5; }
	inline void set_maxSequences_5(int32_t value)
	{
		___maxSequences_5 = value;
	}

	inline static int32_t get_offset_of_hasActiveTweens_6() { return static_cast<int32_t>(offsetof(TweenManager_t374091826_StaticFields, ___hasActiveTweens_6)); }
	inline bool get_hasActiveTweens_6() const { return ___hasActiveTweens_6; }
	inline bool* get_address_of_hasActiveTweens_6() { return &___hasActiveTweens_6; }
	inline void set_hasActiveTweens_6(bool value)
	{
		___hasActiveTweens_6 = value;
	}

	inline static int32_t get_offset_of_hasActiveDefaultTweens_7() { return static_cast<int32_t>(offsetof(TweenManager_t374091826_StaticFields, ___hasActiveDefaultTweens_7)); }
	inline bool get_hasActiveDefaultTweens_7() const { return ___hasActiveDefaultTweens_7; }
	inline bool* get_address_of_hasActiveDefaultTweens_7() { return &___hasActiveDefaultTweens_7; }
	inline void set_hasActiveDefaultTweens_7(bool value)
	{
		___hasActiveDefaultTweens_7 = value;
	}

	inline static int32_t get_offset_of_hasActiveLateTweens_8() { return static_cast<int32_t>(offsetof(TweenManager_t374091826_StaticFields, ___hasActiveLateTweens_8)); }
	inline bool get_hasActiveLateTweens_8() const { return ___hasActiveLateTweens_8; }
	inline bool* get_address_of_hasActiveLateTweens_8() { return &___hasActiveLateTweens_8; }
	inline void set_hasActiveLateTweens_8(bool value)
	{
		___hasActiveLateTweens_8 = value;
	}

	inline static int32_t get_offset_of_hasActiveFixedTweens_9() { return static_cast<int32_t>(offsetof(TweenManager_t374091826_StaticFields, ___hasActiveFixedTweens_9)); }
	inline bool get_hasActiveFixedTweens_9() const { return ___hasActiveFixedTweens_9; }
	inline bool* get_address_of_hasActiveFixedTweens_9() { return &___hasActiveFixedTweens_9; }
	inline void set_hasActiveFixedTweens_9(bool value)
	{
		___hasActiveFixedTweens_9 = value;
	}

	inline static int32_t get_offset_of_hasActiveManualTweens_10() { return static_cast<int32_t>(offsetof(TweenManager_t374091826_StaticFields, ___hasActiveManualTweens_10)); }
	inline bool get_hasActiveManualTweens_10() const { return ___hasActiveManualTweens_10; }
	inline bool* get_address_of_hasActiveManualTweens_10() { return &___hasActiveManualTweens_10; }
	inline void set_hasActiveManualTweens_10(bool value)
	{
		___hasActiveManualTweens_10 = value;
	}

	inline static int32_t get_offset_of_totActiveTweens_11() { return static_cast<int32_t>(offsetof(TweenManager_t374091826_StaticFields, ___totActiveTweens_11)); }
	inline int32_t get_totActiveTweens_11() const { return ___totActiveTweens_11; }
	inline int32_t* get_address_of_totActiveTweens_11() { return &___totActiveTweens_11; }
	inline void set_totActiveTweens_11(int32_t value)
	{
		___totActiveTweens_11 = value;
	}

	inline static int32_t get_offset_of_totActiveDefaultTweens_12() { return static_cast<int32_t>(offsetof(TweenManager_t374091826_StaticFields, ___totActiveDefaultTweens_12)); }
	inline int32_t get_totActiveDefaultTweens_12() const { return ___totActiveDefaultTweens_12; }
	inline int32_t* get_address_of_totActiveDefaultTweens_12() { return &___totActiveDefaultTweens_12; }
	inline void set_totActiveDefaultTweens_12(int32_t value)
	{
		___totActiveDefaultTweens_12 = value;
	}

	inline static int32_t get_offset_of_totActiveLateTweens_13() { return static_cast<int32_t>(offsetof(TweenManager_t374091826_StaticFields, ___totActiveLateTweens_13)); }
	inline int32_t get_totActiveLateTweens_13() const { return ___totActiveLateTweens_13; }
	inline int32_t* get_address_of_totActiveLateTweens_13() { return &___totActiveLateTweens_13; }
	inline void set_totActiveLateTweens_13(int32_t value)
	{
		___totActiveLateTweens_13 = value;
	}

	inline static int32_t get_offset_of_totActiveFixedTweens_14() { return static_cast<int32_t>(offsetof(TweenManager_t374091826_StaticFields, ___totActiveFixedTweens_14)); }
	inline int32_t get_totActiveFixedTweens_14() const { return ___totActiveFixedTweens_14; }
	inline int32_t* get_address_of_totActiveFixedTweens_14() { return &___totActiveFixedTweens_14; }
	inline void set_totActiveFixedTweens_14(int32_t value)
	{
		___totActiveFixedTweens_14 = value;
	}

	inline static int32_t get_offset_of_totActiveManualTweens_15() { return static_cast<int32_t>(offsetof(TweenManager_t374091826_StaticFields, ___totActiveManualTweens_15)); }
	inline int32_t get_totActiveManualTweens_15() const { return ___totActiveManualTweens_15; }
	inline int32_t* get_address_of_totActiveManualTweens_15() { return &___totActiveManualTweens_15; }
	inline void set_totActiveManualTweens_15(int32_t value)
	{
		___totActiveManualTweens_15 = value;
	}

	inline static int32_t get_offset_of_totActiveTweeners_16() { return static_cast<int32_t>(offsetof(TweenManager_t374091826_StaticFields, ___totActiveTweeners_16)); }
	inline int32_t get_totActiveTweeners_16() const { return ___totActiveTweeners_16; }
	inline int32_t* get_address_of_totActiveTweeners_16() { return &___totActiveTweeners_16; }
	inline void set_totActiveTweeners_16(int32_t value)
	{
		___totActiveTweeners_16 = value;
	}

	inline static int32_t get_offset_of_totActiveSequences_17() { return static_cast<int32_t>(offsetof(TweenManager_t374091826_StaticFields, ___totActiveSequences_17)); }
	inline int32_t get_totActiveSequences_17() const { return ___totActiveSequences_17; }
	inline int32_t* get_address_of_totActiveSequences_17() { return &___totActiveSequences_17; }
	inline void set_totActiveSequences_17(int32_t value)
	{
		___totActiveSequences_17 = value;
	}

	inline static int32_t get_offset_of_totPooledTweeners_18() { return static_cast<int32_t>(offsetof(TweenManager_t374091826_StaticFields, ___totPooledTweeners_18)); }
	inline int32_t get_totPooledTweeners_18() const { return ___totPooledTweeners_18; }
	inline int32_t* get_address_of_totPooledTweeners_18() { return &___totPooledTweeners_18; }
	inline void set_totPooledTweeners_18(int32_t value)
	{
		___totPooledTweeners_18 = value;
	}

	inline static int32_t get_offset_of_totPooledSequences_19() { return static_cast<int32_t>(offsetof(TweenManager_t374091826_StaticFields, ___totPooledSequences_19)); }
	inline int32_t get_totPooledSequences_19() const { return ___totPooledSequences_19; }
	inline int32_t* get_address_of_totPooledSequences_19() { return &___totPooledSequences_19; }
	inline void set_totPooledSequences_19(int32_t value)
	{
		___totPooledSequences_19 = value;
	}

	inline static int32_t get_offset_of_totTweeners_20() { return static_cast<int32_t>(offsetof(TweenManager_t374091826_StaticFields, ___totTweeners_20)); }
	inline int32_t get_totTweeners_20() const { return ___totTweeners_20; }
	inline int32_t* get_address_of_totTweeners_20() { return &___totTweeners_20; }
	inline void set_totTweeners_20(int32_t value)
	{
		___totTweeners_20 = value;
	}

	inline static int32_t get_offset_of_totSequences_21() { return static_cast<int32_t>(offsetof(TweenManager_t374091826_StaticFields, ___totSequences_21)); }
	inline int32_t get_totSequences_21() const { return ___totSequences_21; }
	inline int32_t* get_address_of_totSequences_21() { return &___totSequences_21; }
	inline void set_totSequences_21(int32_t value)
	{
		___totSequences_21 = value;
	}

	inline static int32_t get_offset_of_isUpdateLoop_22() { return static_cast<int32_t>(offsetof(TweenManager_t374091826_StaticFields, ___isUpdateLoop_22)); }
	inline bool get_isUpdateLoop_22() const { return ___isUpdateLoop_22; }
	inline bool* get_address_of_isUpdateLoop_22() { return &___isUpdateLoop_22; }
	inline void set_isUpdateLoop_22(bool value)
	{
		___isUpdateLoop_22 = value;
	}

	inline static int32_t get_offset_of__activeTweens_23() { return static_cast<int32_t>(offsetof(TweenManager_t374091826_StaticFields, ____activeTweens_23)); }
	inline TweenU5BU5D_t1320466404* get__activeTweens_23() const { return ____activeTweens_23; }
	inline TweenU5BU5D_t1320466404** get_address_of__activeTweens_23() { return &____activeTweens_23; }
	inline void set__activeTweens_23(TweenU5BU5D_t1320466404* value)
	{
		____activeTweens_23 = value;
		Il2CppCodeGenWriteBarrier((&____activeTweens_23), value);
	}

	inline static int32_t get_offset_of__pooledTweeners_24() { return static_cast<int32_t>(offsetof(TweenManager_t374091826_StaticFields, ____pooledTweeners_24)); }
	inline TweenU5BU5D_t1320466404* get__pooledTweeners_24() const { return ____pooledTweeners_24; }
	inline TweenU5BU5D_t1320466404** get_address_of__pooledTweeners_24() { return &____pooledTweeners_24; }
	inline void set__pooledTweeners_24(TweenU5BU5D_t1320466404* value)
	{
		____pooledTweeners_24 = value;
		Il2CppCodeGenWriteBarrier((&____pooledTweeners_24), value);
	}

	inline static int32_t get_offset_of__PooledSequences_25() { return static_cast<int32_t>(offsetof(TweenManager_t374091826_StaticFields, ____PooledSequences_25)); }
	inline Stack_1_t3186308008 * get__PooledSequences_25() const { return ____PooledSequences_25; }
	inline Stack_1_t3186308008 ** get_address_of__PooledSequences_25() { return &____PooledSequences_25; }
	inline void set__PooledSequences_25(Stack_1_t3186308008 * value)
	{
		____PooledSequences_25 = value;
		Il2CppCodeGenWriteBarrier((&____PooledSequences_25), value);
	}

	inline static int32_t get_offset_of__KillList_26() { return static_cast<int32_t>(offsetof(TweenManager_t374091826_StaticFields, ____KillList_26)); }
	inline List_1_t3814993295 * get__KillList_26() const { return ____KillList_26; }
	inline List_1_t3814993295 ** get_address_of__KillList_26() { return &____KillList_26; }
	inline void set__KillList_26(List_1_t3814993295 * value)
	{
		____KillList_26 = value;
		Il2CppCodeGenWriteBarrier((&____KillList_26), value);
	}

	inline static int32_t get_offset_of__maxActiveLookupId_27() { return static_cast<int32_t>(offsetof(TweenManager_t374091826_StaticFields, ____maxActiveLookupId_27)); }
	inline int32_t get__maxActiveLookupId_27() const { return ____maxActiveLookupId_27; }
	inline int32_t* get_address_of__maxActiveLookupId_27() { return &____maxActiveLookupId_27; }
	inline void set__maxActiveLookupId_27(int32_t value)
	{
		____maxActiveLookupId_27 = value;
	}

	inline static int32_t get_offset_of__requiresActiveReorganization_28() { return static_cast<int32_t>(offsetof(TweenManager_t374091826_StaticFields, ____requiresActiveReorganization_28)); }
	inline bool get__requiresActiveReorganization_28() const { return ____requiresActiveReorganization_28; }
	inline bool* get_address_of__requiresActiveReorganization_28() { return &____requiresActiveReorganization_28; }
	inline void set__requiresActiveReorganization_28(bool value)
	{
		____requiresActiveReorganization_28 = value;
	}

	inline static int32_t get_offset_of__reorganizeFromId_29() { return static_cast<int32_t>(offsetof(TweenManager_t374091826_StaticFields, ____reorganizeFromId_29)); }
	inline int32_t get__reorganizeFromId_29() const { return ____reorganizeFromId_29; }
	inline int32_t* get_address_of__reorganizeFromId_29() { return &____reorganizeFromId_29; }
	inline void set__reorganizeFromId_29(int32_t value)
	{
		____reorganizeFromId_29 = value;
	}

	inline static int32_t get_offset_of__minPooledTweenerId_30() { return static_cast<int32_t>(offsetof(TweenManager_t374091826_StaticFields, ____minPooledTweenerId_30)); }
	inline int32_t get__minPooledTweenerId_30() const { return ____minPooledTweenerId_30; }
	inline int32_t* get_address_of__minPooledTweenerId_30() { return &____minPooledTweenerId_30; }
	inline void set__minPooledTweenerId_30(int32_t value)
	{
		____minPooledTweenerId_30 = value;
	}

	inline static int32_t get_offset_of__maxPooledTweenerId_31() { return static_cast<int32_t>(offsetof(TweenManager_t374091826_StaticFields, ____maxPooledTweenerId_31)); }
	inline int32_t get__maxPooledTweenerId_31() const { return ____maxPooledTweenerId_31; }
	inline int32_t* get_address_of__maxPooledTweenerId_31() { return &____maxPooledTweenerId_31; }
	inline void set__maxPooledTweenerId_31(int32_t value)
	{
		____maxPooledTweenerId_31 = value;
	}

	inline static int32_t get_offset_of__despawnAllCalledFromUpdateLoopCallback_32() { return static_cast<int32_t>(offsetof(TweenManager_t374091826_StaticFields, ____despawnAllCalledFromUpdateLoopCallback_32)); }
	inline bool get__despawnAllCalledFromUpdateLoopCallback_32() const { return ____despawnAllCalledFromUpdateLoopCallback_32; }
	inline bool* get_address_of__despawnAllCalledFromUpdateLoopCallback_32() { return &____despawnAllCalledFromUpdateLoopCallback_32; }
	inline void set__despawnAllCalledFromUpdateLoopCallback_32(bool value)
	{
		____despawnAllCalledFromUpdateLoopCallback_32 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWEENMANAGER_T374091826_H
#ifndef UTILS_T849021263_H
#define UTILS_T849021263_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.Utils
struct  Utils_t849021263  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UTILS_T849021263_H
#ifndef ABSTWEENPLUGIN_3_T1116502072_H
#define ABSTWEENPLUGIN_3_T1116502072_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>
struct  ABSTweenPlugin_3_t1116502072  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTWEENPLUGIN_3_T1116502072_H
#ifndef ABSTWEENPLUGIN_3_T3746269868_H
#define ABSTWEENPLUGIN_3_T3746269868_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Double,System.Double,DG.Tweening.Plugins.Options.NoOptions>
struct  ABSTweenPlugin_3_t3746269868  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTWEENPLUGIN_3_T3746269868_H
#ifndef ABSTWEENPLUGIN_3_T2924512484_H
#define ABSTWEENPLUGIN_3_T2924512484_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>
struct  ABSTweenPlugin_3_t2924512484  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTWEENPLUGIN_3_T2924512484_H
#ifndef ABSTWEENPLUGIN_3_T1969933424_H
#define ABSTWEENPLUGIN_3_T1969933424_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>
struct  ABSTweenPlugin_3_t1969933424  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTWEENPLUGIN_3_T1969933424_H
#ifndef ABSTWEENPLUGIN_3_T420566061_H
#define ABSTWEENPLUGIN_3_T420566061_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>
struct  ABSTweenPlugin_3_t420566061  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTWEENPLUGIN_3_T420566061_H
#ifndef ABSTWEENPLUGIN_3_T2451549449_H
#define ABSTWEENPLUGIN_3_T2451549449_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.String,System.String,DG.Tweening.Plugins.Options.StringOptions>
struct  ABSTweenPlugin_3_t2451549449  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTWEENPLUGIN_3_T2451549449_H
#ifndef ABSTWEENPLUGIN_3_T1601250467_H
#define ABSTWEENPLUGIN_3_T1601250467_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.UintOptions>
struct  ABSTweenPlugin_3_t1601250467  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTWEENPLUGIN_3_T1601250467_H
#ifndef ABSTWEENPLUGIN_3_T4012845184_H
#define ABSTWEENPLUGIN_3_T4012845184_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>
struct  ABSTweenPlugin_3_t4012845184  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTWEENPLUGIN_3_T4012845184_H
#ifndef ABSTWEENPLUGIN_3_T281541932_H
#define ABSTWEENPLUGIN_3_T281541932_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>
struct  ABSTweenPlugin_3_t281541932  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTWEENPLUGIN_3_T281541932_H
#ifndef ABSTWEENPLUGIN_3_T3321825548_H
#define ABSTWEENPLUGIN_3_T3321825548_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Quaternion,UnityEngine.Quaternion,DG.Tweening.Plugins.Options.NoOptions>
struct  ABSTweenPlugin_3_t3321825548  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTWEENPLUGIN_3_T3321825548_H
#ifndef ABSTWEENPLUGIN_3_T835568657_H
#define ABSTWEENPLUGIN_3_T835568657_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions>
struct  ABSTweenPlugin_3_t835568657  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTWEENPLUGIN_3_T835568657_H
#ifndef ABSTWEENPLUGIN_3_T1253391633_H
#define ABSTWEENPLUGIN_3_T1253391633_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>
struct  ABSTweenPlugin_3_t1253391633  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTWEENPLUGIN_3_T1253391633_H
#ifndef ABSTWEENPLUGIN_3_T1044496576_H
#define ABSTWEENPLUGIN_3_T1044496576_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.RectOffset,UnityEngine.RectOffset,DG.Tweening.Plugins.Options.NoOptions>
struct  ABSTweenPlugin_3_t1044496576  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTWEENPLUGIN_3_T1044496576_H
#ifndef ABSTWEENPLUGIN_3_T3542497879_H
#define ABSTWEENPLUGIN_3_T3542497879_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>
struct  ABSTweenPlugin_3_t3542497879  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTWEENPLUGIN_3_T3542497879_H
#ifndef ABSTWEENPLUGIN_3_T2576148903_H
#define ABSTWEENPLUGIN_3_T2576148903_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions>
struct  ABSTweenPlugin_3_t2576148903  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTWEENPLUGIN_3_T2576148903_H
#ifndef ABSTWEENPLUGIN_3_T2480340187_H
#define ABSTWEENPLUGIN_3_T2480340187_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>
struct  ABSTweenPlugin_3_t2480340187  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTWEENPLUGIN_3_T2480340187_H
#ifndef ABSTWEENPLUGIN_3_T487868235_H
#define ABSTWEENPLUGIN_3_T487868235_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector3,UnityEngine.Vector3[],DG.Tweening.Plugins.Options.Vector3ArrayOptions>
struct  ABSTweenPlugin_3_t487868235  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTWEENPLUGIN_3_T487868235_H
#ifndef ABSTWEENPLUGIN_3_T1371845967_H
#define ABSTWEENPLUGIN_3_T1371845967_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>
struct  ABSTweenPlugin_3_t1371845967  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTWEENPLUGIN_3_T1371845967_H
#ifndef ABSPATHDECODER_T2613982196_H
#define ABSPATHDECODER_T2613982196_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Core.PathCore.ABSPathDecoder
struct  ABSPathDecoder_t2613982196  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSPATHDECODER_T2613982196_H
#ifndef PLUGINSMANAGER_T1753812699_H
#define PLUGINSMANAGER_T1753812699_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Core.PluginsManager
struct  PluginsManager_t1753812699  : public RuntimeObject
{
public:

public:
};

struct PluginsManager_t1753812699_StaticFields
{
public:
	// DG.Tweening.Plugins.Core.ITweenPlugin DG.Tweening.Plugins.Core.PluginsManager::_floatPlugin
	RuntimeObject* ____floatPlugin_0;
	// DG.Tweening.Plugins.Core.ITweenPlugin DG.Tweening.Plugins.Core.PluginsManager::_doublePlugin
	RuntimeObject* ____doublePlugin_1;
	// DG.Tweening.Plugins.Core.ITweenPlugin DG.Tweening.Plugins.Core.PluginsManager::_intPlugin
	RuntimeObject* ____intPlugin_2;
	// DG.Tweening.Plugins.Core.ITweenPlugin DG.Tweening.Plugins.Core.PluginsManager::_uintPlugin
	RuntimeObject* ____uintPlugin_3;
	// DG.Tweening.Plugins.Core.ITweenPlugin DG.Tweening.Plugins.Core.PluginsManager::_longPlugin
	RuntimeObject* ____longPlugin_4;
	// DG.Tweening.Plugins.Core.ITweenPlugin DG.Tweening.Plugins.Core.PluginsManager::_ulongPlugin
	RuntimeObject* ____ulongPlugin_5;
	// DG.Tweening.Plugins.Core.ITweenPlugin DG.Tweening.Plugins.Core.PluginsManager::_vector2Plugin
	RuntimeObject* ____vector2Plugin_6;
	// DG.Tweening.Plugins.Core.ITweenPlugin DG.Tweening.Plugins.Core.PluginsManager::_vector3Plugin
	RuntimeObject* ____vector3Plugin_7;
	// DG.Tweening.Plugins.Core.ITweenPlugin DG.Tweening.Plugins.Core.PluginsManager::_vector4Plugin
	RuntimeObject* ____vector4Plugin_8;
	// DG.Tweening.Plugins.Core.ITweenPlugin DG.Tweening.Plugins.Core.PluginsManager::_quaternionPlugin
	RuntimeObject* ____quaternionPlugin_9;
	// DG.Tweening.Plugins.Core.ITweenPlugin DG.Tweening.Plugins.Core.PluginsManager::_colorPlugin
	RuntimeObject* ____colorPlugin_10;
	// DG.Tweening.Plugins.Core.ITweenPlugin DG.Tweening.Plugins.Core.PluginsManager::_rectPlugin
	RuntimeObject* ____rectPlugin_11;
	// DG.Tweening.Plugins.Core.ITweenPlugin DG.Tweening.Plugins.Core.PluginsManager::_rectOffsetPlugin
	RuntimeObject* ____rectOffsetPlugin_12;
	// DG.Tweening.Plugins.Core.ITweenPlugin DG.Tweening.Plugins.Core.PluginsManager::_stringPlugin
	RuntimeObject* ____stringPlugin_13;
	// DG.Tweening.Plugins.Core.ITweenPlugin DG.Tweening.Plugins.Core.PluginsManager::_vector3ArrayPlugin
	RuntimeObject* ____vector3ArrayPlugin_14;
	// DG.Tweening.Plugins.Core.ITweenPlugin DG.Tweening.Plugins.Core.PluginsManager::_color2Plugin
	RuntimeObject* ____color2Plugin_15;
	// System.Collections.Generic.Dictionary`2<System.Type,DG.Tweening.Plugins.Core.ITweenPlugin> DG.Tweening.Plugins.Core.PluginsManager::_customPlugins
	Dictionary_2_t109512516 * ____customPlugins_17;

public:
	inline static int32_t get_offset_of__floatPlugin_0() { return static_cast<int32_t>(offsetof(PluginsManager_t1753812699_StaticFields, ____floatPlugin_0)); }
	inline RuntimeObject* get__floatPlugin_0() const { return ____floatPlugin_0; }
	inline RuntimeObject** get_address_of__floatPlugin_0() { return &____floatPlugin_0; }
	inline void set__floatPlugin_0(RuntimeObject* value)
	{
		____floatPlugin_0 = value;
		Il2CppCodeGenWriteBarrier((&____floatPlugin_0), value);
	}

	inline static int32_t get_offset_of__doublePlugin_1() { return static_cast<int32_t>(offsetof(PluginsManager_t1753812699_StaticFields, ____doublePlugin_1)); }
	inline RuntimeObject* get__doublePlugin_1() const { return ____doublePlugin_1; }
	inline RuntimeObject** get_address_of__doublePlugin_1() { return &____doublePlugin_1; }
	inline void set__doublePlugin_1(RuntimeObject* value)
	{
		____doublePlugin_1 = value;
		Il2CppCodeGenWriteBarrier((&____doublePlugin_1), value);
	}

	inline static int32_t get_offset_of__intPlugin_2() { return static_cast<int32_t>(offsetof(PluginsManager_t1753812699_StaticFields, ____intPlugin_2)); }
	inline RuntimeObject* get__intPlugin_2() const { return ____intPlugin_2; }
	inline RuntimeObject** get_address_of__intPlugin_2() { return &____intPlugin_2; }
	inline void set__intPlugin_2(RuntimeObject* value)
	{
		____intPlugin_2 = value;
		Il2CppCodeGenWriteBarrier((&____intPlugin_2), value);
	}

	inline static int32_t get_offset_of__uintPlugin_3() { return static_cast<int32_t>(offsetof(PluginsManager_t1753812699_StaticFields, ____uintPlugin_3)); }
	inline RuntimeObject* get__uintPlugin_3() const { return ____uintPlugin_3; }
	inline RuntimeObject** get_address_of__uintPlugin_3() { return &____uintPlugin_3; }
	inline void set__uintPlugin_3(RuntimeObject* value)
	{
		____uintPlugin_3 = value;
		Il2CppCodeGenWriteBarrier((&____uintPlugin_3), value);
	}

	inline static int32_t get_offset_of__longPlugin_4() { return static_cast<int32_t>(offsetof(PluginsManager_t1753812699_StaticFields, ____longPlugin_4)); }
	inline RuntimeObject* get__longPlugin_4() const { return ____longPlugin_4; }
	inline RuntimeObject** get_address_of__longPlugin_4() { return &____longPlugin_4; }
	inline void set__longPlugin_4(RuntimeObject* value)
	{
		____longPlugin_4 = value;
		Il2CppCodeGenWriteBarrier((&____longPlugin_4), value);
	}

	inline static int32_t get_offset_of__ulongPlugin_5() { return static_cast<int32_t>(offsetof(PluginsManager_t1753812699_StaticFields, ____ulongPlugin_5)); }
	inline RuntimeObject* get__ulongPlugin_5() const { return ____ulongPlugin_5; }
	inline RuntimeObject** get_address_of__ulongPlugin_5() { return &____ulongPlugin_5; }
	inline void set__ulongPlugin_5(RuntimeObject* value)
	{
		____ulongPlugin_5 = value;
		Il2CppCodeGenWriteBarrier((&____ulongPlugin_5), value);
	}

	inline static int32_t get_offset_of__vector2Plugin_6() { return static_cast<int32_t>(offsetof(PluginsManager_t1753812699_StaticFields, ____vector2Plugin_6)); }
	inline RuntimeObject* get__vector2Plugin_6() const { return ____vector2Plugin_6; }
	inline RuntimeObject** get_address_of__vector2Plugin_6() { return &____vector2Plugin_6; }
	inline void set__vector2Plugin_6(RuntimeObject* value)
	{
		____vector2Plugin_6 = value;
		Il2CppCodeGenWriteBarrier((&____vector2Plugin_6), value);
	}

	inline static int32_t get_offset_of__vector3Plugin_7() { return static_cast<int32_t>(offsetof(PluginsManager_t1753812699_StaticFields, ____vector3Plugin_7)); }
	inline RuntimeObject* get__vector3Plugin_7() const { return ____vector3Plugin_7; }
	inline RuntimeObject** get_address_of__vector3Plugin_7() { return &____vector3Plugin_7; }
	inline void set__vector3Plugin_7(RuntimeObject* value)
	{
		____vector3Plugin_7 = value;
		Il2CppCodeGenWriteBarrier((&____vector3Plugin_7), value);
	}

	inline static int32_t get_offset_of__vector4Plugin_8() { return static_cast<int32_t>(offsetof(PluginsManager_t1753812699_StaticFields, ____vector4Plugin_8)); }
	inline RuntimeObject* get__vector4Plugin_8() const { return ____vector4Plugin_8; }
	inline RuntimeObject** get_address_of__vector4Plugin_8() { return &____vector4Plugin_8; }
	inline void set__vector4Plugin_8(RuntimeObject* value)
	{
		____vector4Plugin_8 = value;
		Il2CppCodeGenWriteBarrier((&____vector4Plugin_8), value);
	}

	inline static int32_t get_offset_of__quaternionPlugin_9() { return static_cast<int32_t>(offsetof(PluginsManager_t1753812699_StaticFields, ____quaternionPlugin_9)); }
	inline RuntimeObject* get__quaternionPlugin_9() const { return ____quaternionPlugin_9; }
	inline RuntimeObject** get_address_of__quaternionPlugin_9() { return &____quaternionPlugin_9; }
	inline void set__quaternionPlugin_9(RuntimeObject* value)
	{
		____quaternionPlugin_9 = value;
		Il2CppCodeGenWriteBarrier((&____quaternionPlugin_9), value);
	}

	inline static int32_t get_offset_of__colorPlugin_10() { return static_cast<int32_t>(offsetof(PluginsManager_t1753812699_StaticFields, ____colorPlugin_10)); }
	inline RuntimeObject* get__colorPlugin_10() const { return ____colorPlugin_10; }
	inline RuntimeObject** get_address_of__colorPlugin_10() { return &____colorPlugin_10; }
	inline void set__colorPlugin_10(RuntimeObject* value)
	{
		____colorPlugin_10 = value;
		Il2CppCodeGenWriteBarrier((&____colorPlugin_10), value);
	}

	inline static int32_t get_offset_of__rectPlugin_11() { return static_cast<int32_t>(offsetof(PluginsManager_t1753812699_StaticFields, ____rectPlugin_11)); }
	inline RuntimeObject* get__rectPlugin_11() const { return ____rectPlugin_11; }
	inline RuntimeObject** get_address_of__rectPlugin_11() { return &____rectPlugin_11; }
	inline void set__rectPlugin_11(RuntimeObject* value)
	{
		____rectPlugin_11 = value;
		Il2CppCodeGenWriteBarrier((&____rectPlugin_11), value);
	}

	inline static int32_t get_offset_of__rectOffsetPlugin_12() { return static_cast<int32_t>(offsetof(PluginsManager_t1753812699_StaticFields, ____rectOffsetPlugin_12)); }
	inline RuntimeObject* get__rectOffsetPlugin_12() const { return ____rectOffsetPlugin_12; }
	inline RuntimeObject** get_address_of__rectOffsetPlugin_12() { return &____rectOffsetPlugin_12; }
	inline void set__rectOffsetPlugin_12(RuntimeObject* value)
	{
		____rectOffsetPlugin_12 = value;
		Il2CppCodeGenWriteBarrier((&____rectOffsetPlugin_12), value);
	}

	inline static int32_t get_offset_of__stringPlugin_13() { return static_cast<int32_t>(offsetof(PluginsManager_t1753812699_StaticFields, ____stringPlugin_13)); }
	inline RuntimeObject* get__stringPlugin_13() const { return ____stringPlugin_13; }
	inline RuntimeObject** get_address_of__stringPlugin_13() { return &____stringPlugin_13; }
	inline void set__stringPlugin_13(RuntimeObject* value)
	{
		____stringPlugin_13 = value;
		Il2CppCodeGenWriteBarrier((&____stringPlugin_13), value);
	}

	inline static int32_t get_offset_of__vector3ArrayPlugin_14() { return static_cast<int32_t>(offsetof(PluginsManager_t1753812699_StaticFields, ____vector3ArrayPlugin_14)); }
	inline RuntimeObject* get__vector3ArrayPlugin_14() const { return ____vector3ArrayPlugin_14; }
	inline RuntimeObject** get_address_of__vector3ArrayPlugin_14() { return &____vector3ArrayPlugin_14; }
	inline void set__vector3ArrayPlugin_14(RuntimeObject* value)
	{
		____vector3ArrayPlugin_14 = value;
		Il2CppCodeGenWriteBarrier((&____vector3ArrayPlugin_14), value);
	}

	inline static int32_t get_offset_of__color2Plugin_15() { return static_cast<int32_t>(offsetof(PluginsManager_t1753812699_StaticFields, ____color2Plugin_15)); }
	inline RuntimeObject* get__color2Plugin_15() const { return ____color2Plugin_15; }
	inline RuntimeObject** get_address_of__color2Plugin_15() { return &____color2Plugin_15; }
	inline void set__color2Plugin_15(RuntimeObject* value)
	{
		____color2Plugin_15 = value;
		Il2CppCodeGenWriteBarrier((&____color2Plugin_15), value);
	}

	inline static int32_t get_offset_of__customPlugins_17() { return static_cast<int32_t>(offsetof(PluginsManager_t1753812699_StaticFields, ____customPlugins_17)); }
	inline Dictionary_2_t109512516 * get__customPlugins_17() const { return ____customPlugins_17; }
	inline Dictionary_2_t109512516 ** get_address_of__customPlugins_17() { return &____customPlugins_17; }
	inline void set__customPlugins_17(Dictionary_2_t109512516 * value)
	{
		____customPlugins_17 = value;
		Il2CppCodeGenWriteBarrier((&____customPlugins_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLUGINSMANAGER_T1753812699_H
#ifndef SPECIALPLUGINSUTILS_T1116177272_H
#define SPECIALPLUGINSUTILS_T1116177272_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Core.SpecialPluginsUtils
struct  SpecialPluginsUtils_t1116177272  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPECIALPLUGINSUTILS_T1116177272_H
#ifndef STRINGPLUGINEXTENSIONS_T3521198905_H
#define STRINGPLUGINEXTENSIONS_T3521198905_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.StringPluginExtensions
struct  StringPluginExtensions_t3521198905  : public RuntimeObject
{
public:

public:
};

struct StringPluginExtensions_t3521198905_StaticFields
{
public:
	// System.Char[] DG.Tweening.Plugins.StringPluginExtensions::ScrambledCharsAll
	CharU5BU5D_t3528271667* ___ScrambledCharsAll_0;
	// System.Char[] DG.Tweening.Plugins.StringPluginExtensions::ScrambledCharsUppercase
	CharU5BU5D_t3528271667* ___ScrambledCharsUppercase_1;
	// System.Char[] DG.Tweening.Plugins.StringPluginExtensions::ScrambledCharsLowercase
	CharU5BU5D_t3528271667* ___ScrambledCharsLowercase_2;
	// System.Char[] DG.Tweening.Plugins.StringPluginExtensions::ScrambledCharsNumerals
	CharU5BU5D_t3528271667* ___ScrambledCharsNumerals_3;
	// System.Int32 DG.Tweening.Plugins.StringPluginExtensions::_lastRndSeed
	int32_t ____lastRndSeed_4;

public:
	inline static int32_t get_offset_of_ScrambledCharsAll_0() { return static_cast<int32_t>(offsetof(StringPluginExtensions_t3521198905_StaticFields, ___ScrambledCharsAll_0)); }
	inline CharU5BU5D_t3528271667* get_ScrambledCharsAll_0() const { return ___ScrambledCharsAll_0; }
	inline CharU5BU5D_t3528271667** get_address_of_ScrambledCharsAll_0() { return &___ScrambledCharsAll_0; }
	inline void set_ScrambledCharsAll_0(CharU5BU5D_t3528271667* value)
	{
		___ScrambledCharsAll_0 = value;
		Il2CppCodeGenWriteBarrier((&___ScrambledCharsAll_0), value);
	}

	inline static int32_t get_offset_of_ScrambledCharsUppercase_1() { return static_cast<int32_t>(offsetof(StringPluginExtensions_t3521198905_StaticFields, ___ScrambledCharsUppercase_1)); }
	inline CharU5BU5D_t3528271667* get_ScrambledCharsUppercase_1() const { return ___ScrambledCharsUppercase_1; }
	inline CharU5BU5D_t3528271667** get_address_of_ScrambledCharsUppercase_1() { return &___ScrambledCharsUppercase_1; }
	inline void set_ScrambledCharsUppercase_1(CharU5BU5D_t3528271667* value)
	{
		___ScrambledCharsUppercase_1 = value;
		Il2CppCodeGenWriteBarrier((&___ScrambledCharsUppercase_1), value);
	}

	inline static int32_t get_offset_of_ScrambledCharsLowercase_2() { return static_cast<int32_t>(offsetof(StringPluginExtensions_t3521198905_StaticFields, ___ScrambledCharsLowercase_2)); }
	inline CharU5BU5D_t3528271667* get_ScrambledCharsLowercase_2() const { return ___ScrambledCharsLowercase_2; }
	inline CharU5BU5D_t3528271667** get_address_of_ScrambledCharsLowercase_2() { return &___ScrambledCharsLowercase_2; }
	inline void set_ScrambledCharsLowercase_2(CharU5BU5D_t3528271667* value)
	{
		___ScrambledCharsLowercase_2 = value;
		Il2CppCodeGenWriteBarrier((&___ScrambledCharsLowercase_2), value);
	}

	inline static int32_t get_offset_of_ScrambledCharsNumerals_3() { return static_cast<int32_t>(offsetof(StringPluginExtensions_t3521198905_StaticFields, ___ScrambledCharsNumerals_3)); }
	inline CharU5BU5D_t3528271667* get_ScrambledCharsNumerals_3() const { return ___ScrambledCharsNumerals_3; }
	inline CharU5BU5D_t3528271667** get_address_of_ScrambledCharsNumerals_3() { return &___ScrambledCharsNumerals_3; }
	inline void set_ScrambledCharsNumerals_3(CharU5BU5D_t3528271667* value)
	{
		___ScrambledCharsNumerals_3 = value;
		Il2CppCodeGenWriteBarrier((&___ScrambledCharsNumerals_3), value);
	}

	inline static int32_t get_offset_of__lastRndSeed_4() { return static_cast<int32_t>(offsetof(StringPluginExtensions_t3521198905_StaticFields, ____lastRndSeed_4)); }
	inline int32_t get__lastRndSeed_4() const { return ____lastRndSeed_4; }
	inline int32_t* get_address_of__lastRndSeed_4() { return &____lastRndSeed_4; }
	inline void set__lastRndSeed_4(int32_t value)
	{
		____lastRndSeed_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGPLUGINEXTENSIONS_T3521198905_H
#ifndef U3CU3EC__DISPLAYCLASS57_0_T2368928457_H
#define U3CU3EC__DISPLAYCLASS57_0_T2368928457_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass57_0
struct  U3CU3Ec__DisplayClass57_0_t2368928457  : public RuntimeObject
{
public:
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass57_0::target
	Transform_t3600365921 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass57_0_t2368928457, ___target_0)); }
	inline Transform_t3600365921 * get_target_0() const { return ___target_0; }
	inline Transform_t3600365921 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Transform_t3600365921 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS57_0_T2368928457_H
#ifndef U3CU3EC__DISPLAYCLASS58_0_T2368207561_H
#define U3CU3EC__DISPLAYCLASS58_0_T2368207561_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass58_0
struct  U3CU3Ec__DisplayClass58_0_t2368207561  : public RuntimeObject
{
public:
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass58_0::target
	Transform_t3600365921 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass58_0_t2368207561, ___target_0)); }
	inline Transform_t3600365921 * get_target_0() const { return ___target_0; }
	inline Transform_t3600365921 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Transform_t3600365921 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS58_0_T2368207561_H
#ifndef U3CU3EC__DISPLAYCLASS59_0_T2368273097_H
#define U3CU3EC__DISPLAYCLASS59_0_T2368273097_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass59_0
struct  U3CU3Ec__DisplayClass59_0_t2368273097  : public RuntimeObject
{
public:
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass59_0::target
	Transform_t3600365921 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass59_0_t2368273097, ___target_0)); }
	inline Transform_t3600365921 * get_target_0() const { return ___target_0; }
	inline Transform_t3600365921 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Transform_t3600365921 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS59_0_T2368273097_H
#ifndef U3CU3EC__DISPLAYCLASS60_0_T2368731850_H
#define U3CU3EC__DISPLAYCLASS60_0_T2368731850_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass60_0
struct  U3CU3Ec__DisplayClass60_0_t2368731850  : public RuntimeObject
{
public:
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass60_0::target
	Transform_t3600365921 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass60_0_t2368731850, ___target_0)); }
	inline Transform_t3600365921 * get_target_0() const { return ___target_0; }
	inline Transform_t3600365921 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Transform_t3600365921 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS60_0_T2368731850_H
#ifndef U3CU3EC__DISPLAYCLASS61_0_T2368797386_H
#define U3CU3EC__DISPLAYCLASS61_0_T2368797386_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass61_0
struct  U3CU3Ec__DisplayClass61_0_t2368797386  : public RuntimeObject
{
public:
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass61_0::target
	Transform_t3600365921 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass61_0_t2368797386, ___target_0)); }
	inline Transform_t3600365921 * get_target_0() const { return ___target_0; }
	inline Transform_t3600365921 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Transform_t3600365921 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS61_0_T2368797386_H
#ifndef U3CU3EC__DISPLAYCLASS62_0_T2368600778_H
#define U3CU3EC__DISPLAYCLASS62_0_T2368600778_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass62_0
struct  U3CU3Ec__DisplayClass62_0_t2368600778  : public RuntimeObject
{
public:
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass62_0::target
	Transform_t3600365921 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass62_0_t2368600778, ___target_0)); }
	inline Transform_t3600365921 * get_target_0() const { return ___target_0; }
	inline Transform_t3600365921 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Transform_t3600365921 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS62_0_T2368600778_H
#ifndef U3CU3EC__DISPLAYCLASS63_0_T2368666314_H
#define U3CU3EC__DISPLAYCLASS63_0_T2368666314_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass63_0
struct  U3CU3Ec__DisplayClass63_0_t2368666314  : public RuntimeObject
{
public:
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass63_0::target
	Transform_t3600365921 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass63_0_t2368666314, ___target_0)); }
	inline Transform_t3600365921 * get_target_0() const { return ___target_0; }
	inline Transform_t3600365921 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Transform_t3600365921 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS63_0_T2368666314_H
#ifndef U3CU3EC__DISPLAYCLASS64_0_T2368993994_H
#define U3CU3EC__DISPLAYCLASS64_0_T2368993994_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass64_0
struct  U3CU3Ec__DisplayClass64_0_t2368993994  : public RuntimeObject
{
public:
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass64_0::target
	Transform_t3600365921 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass64_0_t2368993994, ___target_0)); }
	inline Transform_t3600365921 * get_target_0() const { return ___target_0; }
	inline Transform_t3600365921 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Transform_t3600365921 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS64_0_T2368993994_H
#ifndef U3CU3EC__DISPLAYCLASS65_0_T2369059530_H
#define U3CU3EC__DISPLAYCLASS65_0_T2369059530_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass65_0
struct  U3CU3Ec__DisplayClass65_0_t2369059530  : public RuntimeObject
{
public:
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass65_0::target
	Transform_t3600365921 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass65_0_t2369059530, ___target_0)); }
	inline Transform_t3600365921 * get_target_0() const { return ___target_0; }
	inline Transform_t3600365921 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Transform_t3600365921 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS65_0_T2369059530_H
#ifndef U3CU3EC__DISPLAYCLASS66_0_T2368862922_H
#define U3CU3EC__DISPLAYCLASS66_0_T2368862922_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass66_0
struct  U3CU3Ec__DisplayClass66_0_t2368862922  : public RuntimeObject
{
public:
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass66_0::target
	Transform_t3600365921 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass66_0_t2368862922, ___target_0)); }
	inline Transform_t3600365921 * get_target_0() const { return ___target_0; }
	inline Transform_t3600365921 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Transform_t3600365921 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS66_0_T2368862922_H
#ifndef U3CU3EC__DISPLAYCLASS67_0_T2368928458_H
#define U3CU3EC__DISPLAYCLASS67_0_T2368928458_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass67_0
struct  U3CU3Ec__DisplayClass67_0_t2368928458  : public RuntimeObject
{
public:
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass67_0::target
	Transform_t3600365921 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass67_0_t2368928458, ___target_0)); }
	inline Transform_t3600365921 * get_target_0() const { return ___target_0; }
	inline Transform_t3600365921 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Transform_t3600365921 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS67_0_T2368928458_H
#ifndef U3CU3EC__DISPLAYCLASS70_0_T2368731851_H
#define U3CU3EC__DISPLAYCLASS70_0_T2368731851_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass70_0
struct  U3CU3Ec__DisplayClass70_0_t2368731851  : public RuntimeObject
{
public:
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass70_0::target
	Transform_t3600365921 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass70_0_t2368731851, ___target_0)); }
	inline Transform_t3600365921 * get_target_0() const { return ___target_0; }
	inline Transform_t3600365921 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Transform_t3600365921 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS70_0_T2368731851_H
#ifndef U3CU3EC__DISPLAYCLASS71_0_T2368797387_H
#define U3CU3EC__DISPLAYCLASS71_0_T2368797387_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass71_0
struct  U3CU3Ec__DisplayClass71_0_t2368797387  : public RuntimeObject
{
public:
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass71_0::target
	Transform_t3600365921 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass71_0_t2368797387, ___target_0)); }
	inline Transform_t3600365921 * get_target_0() const { return ___target_0; }
	inline Transform_t3600365921 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Transform_t3600365921 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS71_0_T2368797387_H
#ifndef U3CU3EC__DISPLAYCLASS72_0_T2368600779_H
#define U3CU3EC__DISPLAYCLASS72_0_T2368600779_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass72_0
struct  U3CU3Ec__DisplayClass72_0_t2368600779  : public RuntimeObject
{
public:
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass72_0::target
	Transform_t3600365921 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass72_0_t2368600779, ___target_0)); }
	inline Transform_t3600365921 * get_target_0() const { return ___target_0; }
	inline Transform_t3600365921 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Transform_t3600365921 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS72_0_T2368600779_H
#ifndef U3CU3EC__DISPLAYCLASS73_0_T2368666315_H
#define U3CU3EC__DISPLAYCLASS73_0_T2368666315_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass73_0
struct  U3CU3Ec__DisplayClass73_0_t2368666315  : public RuntimeObject
{
public:
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass73_0::target
	Transform_t3600365921 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass73_0_t2368666315, ___target_0)); }
	inline Transform_t3600365921 * get_target_0() const { return ___target_0; }
	inline Transform_t3600365921 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Transform_t3600365921 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS73_0_T2368666315_H
#ifndef U3CU3EC__DISPLAYCLASS74_0_T2368993995_H
#define U3CU3EC__DISPLAYCLASS74_0_T2368993995_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass74_0
struct  U3CU3Ec__DisplayClass74_0_t2368993995  : public RuntimeObject
{
public:
	// DG.Tweening.Tween DG.Tweening.ShortcutExtensions/<>c__DisplayClass74_0::target
	Tween_t2342918553 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass74_0_t2368993995, ___target_0)); }
	inline Tween_t2342918553 * get_target_0() const { return ___target_0; }
	inline Tween_t2342918553 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Tween_t2342918553 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS74_0_T2368993995_H
#ifndef TWEENSETTINGSEXTENSIONS_T101259202_H
#define TWEENSETTINGSEXTENSIONS_T101259202_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.TweenSettingsExtensions
struct  TweenSettingsExtensions_t101259202  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWEENSETTINGSEXTENSIONS_T101259202_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef PUREQUATERNIONPLUGIN_T587122414_H
#define PUREQUATERNIONPLUGIN_T587122414_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.CustomPlugins.PureQuaternionPlugin
struct  PureQuaternionPlugin_t587122414  : public ABSTweenPlugin_3_t3321825548
{
public:

public:
};

struct PureQuaternionPlugin_t587122414_StaticFields
{
public:
	// DG.Tweening.CustomPlugins.PureQuaternionPlugin DG.Tweening.CustomPlugins.PureQuaternionPlugin::_plug
	PureQuaternionPlugin_t587122414 * ____plug_0;

public:
	inline static int32_t get_offset_of__plug_0() { return static_cast<int32_t>(offsetof(PureQuaternionPlugin_t587122414_StaticFields, ____plug_0)); }
	inline PureQuaternionPlugin_t587122414 * get__plug_0() const { return ____plug_0; }
	inline PureQuaternionPlugin_t587122414 ** get_address_of__plug_0() { return &____plug_0; }
	inline void set__plug_0(PureQuaternionPlugin_t587122414 * value)
	{
		____plug_0 = value;
		Il2CppCodeGenWriteBarrier((&____plug_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PUREQUATERNIONPLUGIN_T587122414_H
#ifndef COLOR2PLUGIN_T2483663196_H
#define COLOR2PLUGIN_T2483663196_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Color2Plugin
struct  Color2Plugin_t2483663196  : public ABSTweenPlugin_3_t1116502072
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR2PLUGIN_T2483663196_H
#ifndef COLORPLUGIN_T4137411927_H
#define COLORPLUGIN_T4137411927_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.ColorPlugin
struct  ColorPlugin_t4137411927  : public ABSTweenPlugin_3_t281541932
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORPLUGIN_T4137411927_H
#ifndef CATMULLROMDECODER_T2053048079_H
#define CATMULLROMDECODER_T2053048079_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Core.PathCore.CatmullRomDecoder
struct  CatmullRomDecoder_t2053048079  : public ABSPathDecoder_t2613982196
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CATMULLROMDECODER_T2053048079_H
#ifndef LINEARDECODER_T2708327777_H
#define LINEARDECODER_T2708327777_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Core.PathCore.LinearDecoder
struct  LinearDecoder_t2708327777  : public ABSPathDecoder_t2613982196
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINEARDECODER_T2708327777_H
#ifndef DOUBLEPLUGIN_T2037284588_H
#define DOUBLEPLUGIN_T2037284588_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.DoublePlugin
struct  DoublePlugin_t2037284588  : public ABSTweenPlugin_3_t3746269868
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOUBLEPLUGIN_T2037284588_H
#ifndef FLOATPLUGIN_T2370056133_H
#define FLOATPLUGIN_T2370056133_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.FloatPlugin
struct  FloatPlugin_t2370056133  : public ABSTweenPlugin_3_t420566061
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FLOATPLUGIN_T2370056133_H
#ifndef INTPLUGIN_T942328046_H
#define INTPLUGIN_T942328046_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.IntPlugin
struct  IntPlugin_t942328046  : public ABSTweenPlugin_3_t2924512484
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPLUGIN_T942328046_H
#ifndef LONGPLUGIN_T809278878_H
#define LONGPLUGIN_T809278878_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.LongPlugin
struct  LongPlugin_t809278878  : public ABSTweenPlugin_3_t1969933424
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LONGPLUGIN_T809278878_H
#ifndef COLOROPTIONS_T1487297155_H
#define COLOROPTIONS_T1487297155_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Options.ColorOptions
struct  ColorOptions_t1487297155 
{
public:
	// System.Boolean DG.Tweening.Plugins.Options.ColorOptions::alphaOnly
	bool ___alphaOnly_0;

public:
	inline static int32_t get_offset_of_alphaOnly_0() { return static_cast<int32_t>(offsetof(ColorOptions_t1487297155, ___alphaOnly_0)); }
	inline bool get_alphaOnly_0() const { return ___alphaOnly_0; }
	inline bool* get_address_of_alphaOnly_0() { return &___alphaOnly_0; }
	inline void set_alphaOnly_0(bool value)
	{
		___alphaOnly_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of DG.Tweening.Plugins.Options.ColorOptions
struct ColorOptions_t1487297155_marshaled_pinvoke
{
	int32_t ___alphaOnly_0;
};
// Native definition for COM marshalling of DG.Tweening.Plugins.Options.ColorOptions
struct ColorOptions_t1487297155_marshaled_com
{
	int32_t ___alphaOnly_0;
};
#endif // COLOROPTIONS_T1487297155_H
#ifndef FLOATOPTIONS_T1203667100_H
#define FLOATOPTIONS_T1203667100_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Options.FloatOptions
struct  FloatOptions_t1203667100 
{
public:
	// System.Boolean DG.Tweening.Plugins.Options.FloatOptions::snapping
	bool ___snapping_0;

public:
	inline static int32_t get_offset_of_snapping_0() { return static_cast<int32_t>(offsetof(FloatOptions_t1203667100, ___snapping_0)); }
	inline bool get_snapping_0() const { return ___snapping_0; }
	inline bool* get_address_of_snapping_0() { return &___snapping_0; }
	inline void set_snapping_0(bool value)
	{
		___snapping_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of DG.Tweening.Plugins.Options.FloatOptions
struct FloatOptions_t1203667100_marshaled_pinvoke
{
	int32_t ___snapping_0;
};
// Native definition for COM marshalling of DG.Tweening.Plugins.Options.FloatOptions
struct FloatOptions_t1203667100_marshaled_com
{
	int32_t ___snapping_0;
};
#endif // FLOATOPTIONS_T1203667100_H
#ifndef NOOPTIONS_T313102519_H
#define NOOPTIONS_T313102519_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Options.NoOptions
struct  NoOptions_t313102519 
{
public:
	union
	{
		struct
		{
		};
		uint8_t NoOptions_t313102519__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOOPTIONS_T313102519_H
#ifndef RECTOPTIONS_T1018205596_H
#define RECTOPTIONS_T1018205596_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Options.RectOptions
struct  RectOptions_t1018205596 
{
public:
	// System.Boolean DG.Tweening.Plugins.Options.RectOptions::snapping
	bool ___snapping_0;

public:
	inline static int32_t get_offset_of_snapping_0() { return static_cast<int32_t>(offsetof(RectOptions_t1018205596, ___snapping_0)); }
	inline bool get_snapping_0() const { return ___snapping_0; }
	inline bool* get_address_of_snapping_0() { return &___snapping_0; }
	inline void set_snapping_0(bool value)
	{
		___snapping_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of DG.Tweening.Plugins.Options.RectOptions
struct RectOptions_t1018205596_marshaled_pinvoke
{
	int32_t ___snapping_0;
};
// Native definition for COM marshalling of DG.Tweening.Plugins.Options.RectOptions
struct RectOptions_t1018205596_marshaled_com
{
	int32_t ___snapping_0;
};
#endif // RECTOPTIONS_T1018205596_H
#ifndef UINTOPTIONS_T1006674242_H
#define UINTOPTIONS_T1006674242_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Options.UintOptions
struct  UintOptions_t1006674242 
{
public:
	// System.Boolean DG.Tweening.Plugins.Options.UintOptions::isNegativeChangeValue
	bool ___isNegativeChangeValue_0;

public:
	inline static int32_t get_offset_of_isNegativeChangeValue_0() { return static_cast<int32_t>(offsetof(UintOptions_t1006674242, ___isNegativeChangeValue_0)); }
	inline bool get_isNegativeChangeValue_0() const { return ___isNegativeChangeValue_0; }
	inline bool* get_address_of_isNegativeChangeValue_0() { return &___isNegativeChangeValue_0; }
	inline void set_isNegativeChangeValue_0(bool value)
	{
		___isNegativeChangeValue_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of DG.Tweening.Plugins.Options.UintOptions
struct UintOptions_t1006674242_marshaled_pinvoke
{
	int32_t ___isNegativeChangeValue_0;
};
// Native definition for COM marshalling of DG.Tweening.Plugins.Options.UintOptions
struct UintOptions_t1006674242_marshaled_com
{
	int32_t ___isNegativeChangeValue_0;
};
#endif // UINTOPTIONS_T1006674242_H
#ifndef PATHPLUGIN_T1182715676_H
#define PATHPLUGIN_T1182715676_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.PathPlugin
struct  PathPlugin_t1182715676  : public ABSTweenPlugin_3_t2576148903
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PATHPLUGIN_T1182715676_H
#ifndef QUATERNIONPLUGIN_T495488984_H
#define QUATERNIONPLUGIN_T495488984_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.QuaternionPlugin
struct  QuaternionPlugin_t495488984  : public ABSTweenPlugin_3_t835568657
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNIONPLUGIN_T495488984_H
#ifndef RECTOFFSETPLUGIN_T3656342773_H
#define RECTOFFSETPLUGIN_T3656342773_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.RectOffsetPlugin
struct  RectOffsetPlugin_t3656342773  : public ABSTweenPlugin_3_t1044496576
{
public:

public:
};

struct RectOffsetPlugin_t3656342773_StaticFields
{
public:
	// UnityEngine.RectOffset DG.Tweening.Plugins.RectOffsetPlugin::_r
	RectOffset_t1369453676 * ____r_0;

public:
	inline static int32_t get_offset_of__r_0() { return static_cast<int32_t>(offsetof(RectOffsetPlugin_t3656342773_StaticFields, ____r_0)); }
	inline RectOffset_t1369453676 * get__r_0() const { return ____r_0; }
	inline RectOffset_t1369453676 ** get_address_of__r_0() { return &____r_0; }
	inline void set__r_0(RectOffset_t1369453676 * value)
	{
		____r_0 = value;
		Il2CppCodeGenWriteBarrier((&____r_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECTOFFSETPLUGIN_T3656342773_H
#ifndef RECTPLUGIN_T2513065920_H
#define RECTPLUGIN_T2513065920_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.RectPlugin
struct  RectPlugin_t2513065920  : public ABSTweenPlugin_3_t1253391633
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECTPLUGIN_T2513065920_H
#ifndef STRINGPLUGIN_T660282191_H
#define STRINGPLUGIN_T660282191_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.StringPlugin
struct  StringPlugin_t660282191  : public ABSTweenPlugin_3_t2451549449
{
public:

public:
};

struct StringPlugin_t660282191_StaticFields
{
public:
	// System.Text.StringBuilder DG.Tweening.Plugins.StringPlugin::_Buffer
	StringBuilder_t * ____Buffer_0;
	// System.Collections.Generic.List`1<System.Char> DG.Tweening.Plugins.StringPlugin::_OpenedTags
	List_1_t811567916 * ____OpenedTags_1;

public:
	inline static int32_t get_offset_of__Buffer_0() { return static_cast<int32_t>(offsetof(StringPlugin_t660282191_StaticFields, ____Buffer_0)); }
	inline StringBuilder_t * get__Buffer_0() const { return ____Buffer_0; }
	inline StringBuilder_t ** get_address_of__Buffer_0() { return &____Buffer_0; }
	inline void set__Buffer_0(StringBuilder_t * value)
	{
		____Buffer_0 = value;
		Il2CppCodeGenWriteBarrier((&____Buffer_0), value);
	}

	inline static int32_t get_offset_of__OpenedTags_1() { return static_cast<int32_t>(offsetof(StringPlugin_t660282191_StaticFields, ____OpenedTags_1)); }
	inline List_1_t811567916 * get__OpenedTags_1() const { return ____OpenedTags_1; }
	inline List_1_t811567916 ** get_address_of__OpenedTags_1() { return &____OpenedTags_1; }
	inline void set__OpenedTags_1(List_1_t811567916 * value)
	{
		____OpenedTags_1 = value;
		Il2CppCodeGenWriteBarrier((&____OpenedTags_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGPLUGIN_T660282191_H
#ifndef UINTPLUGIN_T2664954793_H
#define UINTPLUGIN_T2664954793_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.UintPlugin
struct  UintPlugin_t2664954793  : public ABSTweenPlugin_3_t1601250467
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UINTPLUGIN_T2664954793_H
#ifndef ULONGPLUGIN_T1878880915_H
#define ULONGPLUGIN_T1878880915_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.UlongPlugin
struct  UlongPlugin_t1878880915  : public ABSTweenPlugin_3_t4012845184
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ULONGPLUGIN_T1878880915_H
#ifndef VECTOR2PLUGIN_T1245585431_H
#define VECTOR2PLUGIN_T1245585431_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Vector2Plugin
struct  Vector2Plugin_t1245585431  : public ABSTweenPlugin_3_t3542497879
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2PLUGIN_T1245585431_H
#ifndef VECTOR3ARRAYPLUGIN_T1419427579_H
#define VECTOR3ARRAYPLUGIN_T1419427579_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Vector3ArrayPlugin
struct  Vector3ArrayPlugin_t1419427579  : public ABSTweenPlugin_3_t487868235
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3ARRAYPLUGIN_T1419427579_H
#ifndef VECTOR3PLUGIN_T1104422930_H
#define VECTOR3PLUGIN_T1104422930_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Vector3Plugin
struct  Vector3Plugin_t1104422930  : public ABSTweenPlugin_3_t2480340187
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3PLUGIN_T1104422930_H
#ifndef VECTOR4PLUGIN_T4128927717_H
#define VECTOR4PLUGIN_T4128927717_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Vector4Plugin
struct  Vector4Plugin_t4128927717  : public ABSTweenPlugin_3_t1371845967
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR4PLUGIN_T4128927717_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef QUATERNION_T2301928331_H
#define QUATERNION_T2301928331_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t2301928331 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t2301928331_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t2301928331  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t2301928331  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t2301928331 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t2301928331  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T2301928331_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_5)); }
	inline Vector3_t3722313464  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_t3722313464  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_6)); }
	inline Vector3_t3722313464  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_t3722313464 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_t3722313464  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_7)); }
	inline Vector3_t3722313464  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_t3722313464 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_t3722313464  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_8)); }
	inline Vector3_t3722313464  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_t3722313464 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_t3722313464  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_9)); }
	inline Vector3_t3722313464  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_t3722313464 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_t3722313464  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_10)); }
	inline Vector3_t3722313464  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_t3722313464 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_t3722313464  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_11)); }
	inline Vector3_t3722313464  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_t3722313464  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_12)); }
	inline Vector3_t3722313464  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_t3722313464 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_t3722313464  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef AUTOPLAY_T1346164433_H
#define AUTOPLAY_T1346164433_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.AutoPlay
struct  AutoPlay_t1346164433 
{
public:
	// System.Int32 DG.Tweening.AutoPlay::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AutoPlay_t1346164433, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTOPLAY_T1346164433_H
#ifndef AXISCONSTRAINT_T2771958344_H
#define AXISCONSTRAINT_T2771958344_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.AxisConstraint
struct  AxisConstraint_t2771958344 
{
public:
	// System.Int32 DG.Tweening.AxisConstraint::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AxisConstraint_t2771958344, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISCONSTRAINT_T2771958344_H
#ifndef SETTINGSLOCATION_T2227561762_H
#define SETTINGSLOCATION_T2227561762_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.DOTweenSettings/SettingsLocation
struct  SettingsLocation_t2227561762 
{
public:
	// System.Int32 DG.Tweening.Core.DOTweenSettings/SettingsLocation::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SettingsLocation_t2227561762, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETTINGSLOCATION_T2227561762_H
#ifndef FILTERTYPE_T3228157936_H
#define FILTERTYPE_T3228157936_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.Enums.FilterType
struct  FilterType_t3228157936 
{
public:
	// System.Int32 DG.Tweening.Core.Enums.FilterType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FilterType_t3228157936, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILTERTYPE_T3228157936_H
#ifndef OPERATIONTYPE_T2152057980_H
#define OPERATIONTYPE_T2152057980_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.Enums.OperationType
struct  OperationType_t2152057980 
{
public:
	// System.Int32 DG.Tweening.Core.Enums.OperationType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(OperationType_t2152057980, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OPERATIONTYPE_T2152057980_H
#ifndef SPECIALSTARTUPMODE_T1644068939_H
#define SPECIALSTARTUPMODE_T1644068939_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.Enums.SpecialStartupMode
struct  SpecialStartupMode_t1644068939 
{
public:
	// System.Int32 DG.Tweening.Core.Enums.SpecialStartupMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SpecialStartupMode_t1644068939, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPECIALSTARTUPMODE_T1644068939_H
#ifndef UPDATEMODE_T1164472539_H
#define UPDATEMODE_T1164472539_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.Enums.UpdateMode
struct  UpdateMode_t1164472539 
{
public:
	// System.Int32 DG.Tweening.Core.Enums.UpdateMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(UpdateMode_t1164472539, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPDATEMODE_T1164472539_H
#ifndef UPDATENOTICE_T1001625488_H
#define UPDATENOTICE_T1001625488_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.Enums.UpdateNotice
struct  UpdateNotice_t1001625488 
{
public:
	// System.Int32 DG.Tweening.Core.Enums.UpdateNotice::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(UpdateNotice_t1001625488, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPDATENOTICE_T1001625488_H
#ifndef CAPACITYINCREASEMODE_T828064684_H
#define CAPACITYINCREASEMODE_T828064684_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.TweenManager/CapacityIncreaseMode
struct  CapacityIncreaseMode_t828064684 
{
public:
	// System.Int32 DG.Tweening.Core.TweenManager/CapacityIncreaseMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CapacityIncreaseMode_t828064684, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAPACITYINCREASEMODE_T828064684_H
#ifndef EASE_T4010715394_H
#define EASE_T4010715394_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Ease
struct  Ease_t4010715394 
{
public:
	// System.Int32 DG.Tweening.Ease::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Ease_t4010715394, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EASE_T4010715394_H
#ifndef LOGBEHAVIOUR_T1548882435_H
#define LOGBEHAVIOUR_T1548882435_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.LogBehaviour
struct  LogBehaviour_t1548882435 
{
public:
	// System.Int32 DG.Tweening.LogBehaviour::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(LogBehaviour_t1548882435, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOGBEHAVIOUR_T1548882435_H
#ifndef LOOPTYPE_T3049802818_H
#define LOOPTYPE_T3049802818_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.LoopType
struct  LoopType_t3049802818 
{
public:
	// System.Int32 DG.Tweening.LoopType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(LoopType_t3049802818, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOOPTYPE_T3049802818_H
#ifndef PATHMODE_T2165603100_H
#define PATHMODE_T2165603100_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.PathMode
struct  PathMode_t2165603100 
{
public:
	// System.Int32 DG.Tweening.PathMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PathMode_t2165603100, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PATHMODE_T2165603100_H
#ifndef PATHTYPE_T3777299409_H
#define PATHTYPE_T3777299409_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.PathType
struct  PathType_t3777299409 
{
public:
	// System.Int32 DG.Tweening.PathType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PathType_t3777299409, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PATHTYPE_T3777299409_H
#ifndef CONTROLPOINT_T3892672090_H
#define CONTROLPOINT_T3892672090_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Core.PathCore.ControlPoint
struct  ControlPoint_t3892672090 
{
public:
	// UnityEngine.Vector3 DG.Tweening.Plugins.Core.PathCore.ControlPoint::a
	Vector3_t3722313464  ___a_0;
	// UnityEngine.Vector3 DG.Tweening.Plugins.Core.PathCore.ControlPoint::b
	Vector3_t3722313464  ___b_1;

public:
	inline static int32_t get_offset_of_a_0() { return static_cast<int32_t>(offsetof(ControlPoint_t3892672090, ___a_0)); }
	inline Vector3_t3722313464  get_a_0() const { return ___a_0; }
	inline Vector3_t3722313464 * get_address_of_a_0() { return &___a_0; }
	inline void set_a_0(Vector3_t3722313464  value)
	{
		___a_0 = value;
	}

	inline static int32_t get_offset_of_b_1() { return static_cast<int32_t>(offsetof(ControlPoint_t3892672090, ___b_1)); }
	inline Vector3_t3722313464  get_b_1() const { return ___b_1; }
	inline Vector3_t3722313464 * get_address_of_b_1() { return &___b_1; }
	inline void set_b_1(Vector3_t3722313464  value)
	{
		___b_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLPOINT_T3892672090_H
#ifndef ORIENTTYPE_T1731166963_H
#define ORIENTTYPE_T1731166963_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Options.OrientType
struct  OrientType_t1731166963 
{
public:
	// System.Int32 DG.Tweening.Plugins.Options.OrientType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(OrientType_t1731166963, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ORIENTTYPE_T1731166963_H
#ifndef ROTATEMODE_T2548570174_H
#define ROTATEMODE_T2548570174_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.RotateMode
struct  RotateMode_t2548570174 
{
public:
	// System.Int32 DG.Tweening.RotateMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(RotateMode_t2548570174, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROTATEMODE_T2548570174_H
#ifndef SCRAMBLEMODE_T1285273342_H
#define SCRAMBLEMODE_T1285273342_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ScrambleMode
struct  ScrambleMode_t1285273342 
{
public:
	// System.Int32 DG.Tweening.ScrambleMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ScrambleMode_t1285273342, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCRAMBLEMODE_T1285273342_H
#ifndef U3CU3EC__DISPLAYCLASS68_0_T2368207562_H
#define U3CU3EC__DISPLAYCLASS68_0_T2368207562_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass68_0
struct  U3CU3Ec__DisplayClass68_0_t2368207562  : public RuntimeObject
{
public:
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass68_0::target
	Transform_t3600365921 * ___target_0;
	// System.Boolean DG.Tweening.ShortcutExtensions/<>c__DisplayClass68_0::offsetYSet
	bool ___offsetYSet_1;
	// System.Single DG.Tweening.ShortcutExtensions/<>c__DisplayClass68_0::offsetY
	float ___offsetY_2;
	// DG.Tweening.Sequence DG.Tweening.ShortcutExtensions/<>c__DisplayClass68_0::s
	Sequence_t2050373119 * ___s_3;
	// UnityEngine.Vector3 DG.Tweening.ShortcutExtensions/<>c__DisplayClass68_0::endValue
	Vector3_t3722313464  ___endValue_4;
	// System.Single DG.Tweening.ShortcutExtensions/<>c__DisplayClass68_0::startPosY
	float ___startPosY_5;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass68_0_t2368207562, ___target_0)); }
	inline Transform_t3600365921 * get_target_0() const { return ___target_0; }
	inline Transform_t3600365921 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Transform_t3600365921 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}

	inline static int32_t get_offset_of_offsetYSet_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass68_0_t2368207562, ___offsetYSet_1)); }
	inline bool get_offsetYSet_1() const { return ___offsetYSet_1; }
	inline bool* get_address_of_offsetYSet_1() { return &___offsetYSet_1; }
	inline void set_offsetYSet_1(bool value)
	{
		___offsetYSet_1 = value;
	}

	inline static int32_t get_offset_of_offsetY_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass68_0_t2368207562, ___offsetY_2)); }
	inline float get_offsetY_2() const { return ___offsetY_2; }
	inline float* get_address_of_offsetY_2() { return &___offsetY_2; }
	inline void set_offsetY_2(float value)
	{
		___offsetY_2 = value;
	}

	inline static int32_t get_offset_of_s_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass68_0_t2368207562, ___s_3)); }
	inline Sequence_t2050373119 * get_s_3() const { return ___s_3; }
	inline Sequence_t2050373119 ** get_address_of_s_3() { return &___s_3; }
	inline void set_s_3(Sequence_t2050373119 * value)
	{
		___s_3 = value;
		Il2CppCodeGenWriteBarrier((&___s_3), value);
	}

	inline static int32_t get_offset_of_endValue_4() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass68_0_t2368207562, ___endValue_4)); }
	inline Vector3_t3722313464  get_endValue_4() const { return ___endValue_4; }
	inline Vector3_t3722313464 * get_address_of_endValue_4() { return &___endValue_4; }
	inline void set_endValue_4(Vector3_t3722313464  value)
	{
		___endValue_4 = value;
	}

	inline static int32_t get_offset_of_startPosY_5() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass68_0_t2368207562, ___startPosY_5)); }
	inline float get_startPosY_5() const { return ___startPosY_5; }
	inline float* get_address_of_startPosY_5() { return &___startPosY_5; }
	inline void set_startPosY_5(float value)
	{
		___startPosY_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS68_0_T2368207562_H
#ifndef U3CU3EC__DISPLAYCLASS69_0_T2368273098_H
#define U3CU3EC__DISPLAYCLASS69_0_T2368273098_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass69_0
struct  U3CU3Ec__DisplayClass69_0_t2368273098  : public RuntimeObject
{
public:
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass69_0::target
	Transform_t3600365921 * ___target_0;
	// System.Boolean DG.Tweening.ShortcutExtensions/<>c__DisplayClass69_0::offsetYSet
	bool ___offsetYSet_1;
	// System.Single DG.Tweening.ShortcutExtensions/<>c__DisplayClass69_0::offsetY
	float ___offsetY_2;
	// DG.Tweening.Sequence DG.Tweening.ShortcutExtensions/<>c__DisplayClass69_0::s
	Sequence_t2050373119 * ___s_3;
	// UnityEngine.Vector3 DG.Tweening.ShortcutExtensions/<>c__DisplayClass69_0::endValue
	Vector3_t3722313464  ___endValue_4;
	// System.Single DG.Tweening.ShortcutExtensions/<>c__DisplayClass69_0::startPosY
	float ___startPosY_5;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass69_0_t2368273098, ___target_0)); }
	inline Transform_t3600365921 * get_target_0() const { return ___target_0; }
	inline Transform_t3600365921 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Transform_t3600365921 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}

	inline static int32_t get_offset_of_offsetYSet_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass69_0_t2368273098, ___offsetYSet_1)); }
	inline bool get_offsetYSet_1() const { return ___offsetYSet_1; }
	inline bool* get_address_of_offsetYSet_1() { return &___offsetYSet_1; }
	inline void set_offsetYSet_1(bool value)
	{
		___offsetYSet_1 = value;
	}

	inline static int32_t get_offset_of_offsetY_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass69_0_t2368273098, ___offsetY_2)); }
	inline float get_offsetY_2() const { return ___offsetY_2; }
	inline float* get_address_of_offsetY_2() { return &___offsetY_2; }
	inline void set_offsetY_2(float value)
	{
		___offsetY_2 = value;
	}

	inline static int32_t get_offset_of_s_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass69_0_t2368273098, ___s_3)); }
	inline Sequence_t2050373119 * get_s_3() const { return ___s_3; }
	inline Sequence_t2050373119 ** get_address_of_s_3() { return &___s_3; }
	inline void set_s_3(Sequence_t2050373119 * value)
	{
		___s_3 = value;
		Il2CppCodeGenWriteBarrier((&___s_3), value);
	}

	inline static int32_t get_offset_of_endValue_4() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass69_0_t2368273098, ___endValue_4)); }
	inline Vector3_t3722313464  get_endValue_4() const { return ___endValue_4; }
	inline Vector3_t3722313464 * get_address_of_endValue_4() { return &___endValue_4; }
	inline void set_endValue_4(Vector3_t3722313464  value)
	{
		___endValue_4 = value;
	}

	inline static int32_t get_offset_of_startPosY_5() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass69_0_t2368273098, ___startPosY_5)); }
	inline float get_startPosY_5() const { return ___startPosY_5; }
	inline float* get_address_of_startPosY_5() { return &___startPosY_5; }
	inline void set_startPosY_5(float value)
	{
		___startPosY_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS69_0_T2368273098_H
#ifndef U3CU3EC__DISPLAYCLASS75_0_T2369059531_H
#define U3CU3EC__DISPLAYCLASS75_0_T2369059531_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass75_0
struct  U3CU3Ec__DisplayClass75_0_t2369059531  : public RuntimeObject
{
public:
	// UnityEngine.Color DG.Tweening.ShortcutExtensions/<>c__DisplayClass75_0::to
	Color_t2555686324  ___to_0;
	// UnityEngine.Light DG.Tweening.ShortcutExtensions/<>c__DisplayClass75_0::target
	Light_t3756812086 * ___target_1;

public:
	inline static int32_t get_offset_of_to_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass75_0_t2369059531, ___to_0)); }
	inline Color_t2555686324  get_to_0() const { return ___to_0; }
	inline Color_t2555686324 * get_address_of_to_0() { return &___to_0; }
	inline void set_to_0(Color_t2555686324  value)
	{
		___to_0 = value;
	}

	inline static int32_t get_offset_of_target_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass75_0_t2369059531, ___target_1)); }
	inline Light_t3756812086 * get_target_1() const { return ___target_1; }
	inline Light_t3756812086 ** get_address_of_target_1() { return &___target_1; }
	inline void set_target_1(Light_t3756812086 * value)
	{
		___target_1 = value;
		Il2CppCodeGenWriteBarrier((&___target_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS75_0_T2369059531_H
#ifndef U3CU3EC__DISPLAYCLASS76_0_T2368862923_H
#define U3CU3EC__DISPLAYCLASS76_0_T2368862923_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass76_0
struct  U3CU3Ec__DisplayClass76_0_t2368862923  : public RuntimeObject
{
public:
	// UnityEngine.Color DG.Tweening.ShortcutExtensions/<>c__DisplayClass76_0::to
	Color_t2555686324  ___to_0;
	// UnityEngine.Material DG.Tweening.ShortcutExtensions/<>c__DisplayClass76_0::target
	Material_t340375123 * ___target_1;

public:
	inline static int32_t get_offset_of_to_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass76_0_t2368862923, ___to_0)); }
	inline Color_t2555686324  get_to_0() const { return ___to_0; }
	inline Color_t2555686324 * get_address_of_to_0() { return &___to_0; }
	inline void set_to_0(Color_t2555686324  value)
	{
		___to_0 = value;
	}

	inline static int32_t get_offset_of_target_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass76_0_t2368862923, ___target_1)); }
	inline Material_t340375123 * get_target_1() const { return ___target_1; }
	inline Material_t340375123 ** get_address_of_target_1() { return &___target_1; }
	inline void set_target_1(Material_t340375123 * value)
	{
		___target_1 = value;
		Il2CppCodeGenWriteBarrier((&___target_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS76_0_T2368862923_H
#ifndef U3CU3EC__DISPLAYCLASS77_0_T2368928459_H
#define U3CU3EC__DISPLAYCLASS77_0_T2368928459_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass77_0
struct  U3CU3Ec__DisplayClass77_0_t2368928459  : public RuntimeObject
{
public:
	// UnityEngine.Color DG.Tweening.ShortcutExtensions/<>c__DisplayClass77_0::to
	Color_t2555686324  ___to_0;
	// UnityEngine.Material DG.Tweening.ShortcutExtensions/<>c__DisplayClass77_0::target
	Material_t340375123 * ___target_1;
	// System.String DG.Tweening.ShortcutExtensions/<>c__DisplayClass77_0::property
	String_t* ___property_2;

public:
	inline static int32_t get_offset_of_to_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass77_0_t2368928459, ___to_0)); }
	inline Color_t2555686324  get_to_0() const { return ___to_0; }
	inline Color_t2555686324 * get_address_of_to_0() { return &___to_0; }
	inline void set_to_0(Color_t2555686324  value)
	{
		___to_0 = value;
	}

	inline static int32_t get_offset_of_target_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass77_0_t2368928459, ___target_1)); }
	inline Material_t340375123 * get_target_1() const { return ___target_1; }
	inline Material_t340375123 ** get_address_of_target_1() { return &___target_1; }
	inline void set_target_1(Material_t340375123 * value)
	{
		___target_1 = value;
		Il2CppCodeGenWriteBarrier((&___target_1), value);
	}

	inline static int32_t get_offset_of_property_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass77_0_t2368928459, ___property_2)); }
	inline String_t* get_property_2() const { return ___property_2; }
	inline String_t** get_address_of_property_2() { return &___property_2; }
	inline void set_property_2(String_t* value)
	{
		___property_2 = value;
		Il2CppCodeGenWriteBarrier((&___property_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS77_0_T2368928459_H
#ifndef U3CU3EC__DISPLAYCLASS78_0_T2368207563_H
#define U3CU3EC__DISPLAYCLASS78_0_T2368207563_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass78_0
struct  U3CU3Ec__DisplayClass78_0_t2368207563  : public RuntimeObject
{
public:
	// UnityEngine.Vector3 DG.Tweening.ShortcutExtensions/<>c__DisplayClass78_0::to
	Vector3_t3722313464  ___to_0;
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass78_0::target
	Transform_t3600365921 * ___target_1;

public:
	inline static int32_t get_offset_of_to_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass78_0_t2368207563, ___to_0)); }
	inline Vector3_t3722313464  get_to_0() const { return ___to_0; }
	inline Vector3_t3722313464 * get_address_of_to_0() { return &___to_0; }
	inline void set_to_0(Vector3_t3722313464  value)
	{
		___to_0 = value;
	}

	inline static int32_t get_offset_of_target_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass78_0_t2368207563, ___target_1)); }
	inline Transform_t3600365921 * get_target_1() const { return ___target_1; }
	inline Transform_t3600365921 ** get_address_of_target_1() { return &___target_1; }
	inline void set_target_1(Transform_t3600365921 * value)
	{
		___target_1 = value;
		Il2CppCodeGenWriteBarrier((&___target_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS78_0_T2368207563_H
#ifndef U3CU3EC__DISPLAYCLASS79_0_T2368273099_H
#define U3CU3EC__DISPLAYCLASS79_0_T2368273099_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass79_0
struct  U3CU3Ec__DisplayClass79_0_t2368273099  : public RuntimeObject
{
public:
	// UnityEngine.Vector3 DG.Tweening.ShortcutExtensions/<>c__DisplayClass79_0::to
	Vector3_t3722313464  ___to_0;
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass79_0::target
	Transform_t3600365921 * ___target_1;

public:
	inline static int32_t get_offset_of_to_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass79_0_t2368273099, ___to_0)); }
	inline Vector3_t3722313464  get_to_0() const { return ___to_0; }
	inline Vector3_t3722313464 * get_address_of_to_0() { return &___to_0; }
	inline void set_to_0(Vector3_t3722313464  value)
	{
		___to_0 = value;
	}

	inline static int32_t get_offset_of_target_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass79_0_t2368273099, ___target_1)); }
	inline Transform_t3600365921 * get_target_1() const { return ___target_1; }
	inline Transform_t3600365921 ** get_address_of_target_1() { return &___target_1; }
	inline void set_target_1(Transform_t3600365921 * value)
	{
		___target_1 = value;
		Il2CppCodeGenWriteBarrier((&___target_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS79_0_T2368273099_H
#ifndef U3CU3EC__DISPLAYCLASS80_0_T2368731860_H
#define U3CU3EC__DISPLAYCLASS80_0_T2368731860_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass80_0
struct  U3CU3Ec__DisplayClass80_0_t2368731860  : public RuntimeObject
{
public:
	// UnityEngine.Quaternion DG.Tweening.ShortcutExtensions/<>c__DisplayClass80_0::to
	Quaternion_t2301928331  ___to_0;
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass80_0::target
	Transform_t3600365921 * ___target_1;

public:
	inline static int32_t get_offset_of_to_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass80_0_t2368731860, ___to_0)); }
	inline Quaternion_t2301928331  get_to_0() const { return ___to_0; }
	inline Quaternion_t2301928331 * get_address_of_to_0() { return &___to_0; }
	inline void set_to_0(Quaternion_t2301928331  value)
	{
		___to_0 = value;
	}

	inline static int32_t get_offset_of_target_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass80_0_t2368731860, ___target_1)); }
	inline Transform_t3600365921 * get_target_1() const { return ___target_1; }
	inline Transform_t3600365921 ** get_address_of_target_1() { return &___target_1; }
	inline void set_target_1(Transform_t3600365921 * value)
	{
		___target_1 = value;
		Il2CppCodeGenWriteBarrier((&___target_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS80_0_T2368731860_H
#ifndef U3CU3EC__DISPLAYCLASS81_0_T2368797396_H
#define U3CU3EC__DISPLAYCLASS81_0_T2368797396_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass81_0
struct  U3CU3Ec__DisplayClass81_0_t2368797396  : public RuntimeObject
{
public:
	// UnityEngine.Quaternion DG.Tweening.ShortcutExtensions/<>c__DisplayClass81_0::to
	Quaternion_t2301928331  ___to_0;
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass81_0::target
	Transform_t3600365921 * ___target_1;

public:
	inline static int32_t get_offset_of_to_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass81_0_t2368797396, ___to_0)); }
	inline Quaternion_t2301928331  get_to_0() const { return ___to_0; }
	inline Quaternion_t2301928331 * get_address_of_to_0() { return &___to_0; }
	inline void set_to_0(Quaternion_t2301928331  value)
	{
		___to_0 = value;
	}

	inline static int32_t get_offset_of_target_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass81_0_t2368797396, ___target_1)); }
	inline Transform_t3600365921 * get_target_1() const { return ___target_1; }
	inline Transform_t3600365921 ** get_address_of_target_1() { return &___target_1; }
	inline void set_target_1(Transform_t3600365921 * value)
	{
		___target_1 = value;
		Il2CppCodeGenWriteBarrier((&___target_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS81_0_T2368797396_H
#ifndef U3CU3EC__DISPLAYCLASS82_0_T2368600788_H
#define U3CU3EC__DISPLAYCLASS82_0_T2368600788_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass82_0
struct  U3CU3Ec__DisplayClass82_0_t2368600788  : public RuntimeObject
{
public:
	// UnityEngine.Vector3 DG.Tweening.ShortcutExtensions/<>c__DisplayClass82_0::to
	Vector3_t3722313464  ___to_0;
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass82_0::target
	Transform_t3600365921 * ___target_1;

public:
	inline static int32_t get_offset_of_to_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass82_0_t2368600788, ___to_0)); }
	inline Vector3_t3722313464  get_to_0() const { return ___to_0; }
	inline Vector3_t3722313464 * get_address_of_to_0() { return &___to_0; }
	inline void set_to_0(Vector3_t3722313464  value)
	{
		___to_0 = value;
	}

	inline static int32_t get_offset_of_target_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass82_0_t2368600788, ___target_1)); }
	inline Transform_t3600365921 * get_target_1() const { return ___target_1; }
	inline Transform_t3600365921 ** get_address_of_target_1() { return &___target_1; }
	inline void set_target_1(Transform_t3600365921 * value)
	{
		___target_1 = value;
		Il2CppCodeGenWriteBarrier((&___target_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS82_0_T2368600788_H
#ifndef TWEENTYPE_T1971673186_H
#define TWEENTYPE_T1971673186_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.TweenType
struct  TweenType_t1971673186 
{
public:
	// System.Int32 DG.Tweening.TweenType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TweenType_t1971673186, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWEENTYPE_T1971673186_H
#ifndef UPDATETYPE_T3937729206_H
#define UPDATETYPE_T3937729206_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.UpdateType
struct  UpdateType_t3937729206 
{
public:
	// System.Int32 DG.Tweening.UpdateType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(UpdateType_t3937729206, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPDATETYPE_T3937729206_H
#ifndef NULLABLE_1_T1149908250_H
#define NULLABLE_1_T1149908250_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<UnityEngine.Vector3>
struct  Nullable_1_t1149908250 
{
public:
	// T System.Nullable`1::value
	Vector3_t3722313464  ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t1149908250, ___value_0)); }
	inline Vector3_t3722313464  get_value_0() const { return ___value_0; }
	inline Vector3_t3722313464 * get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(Vector3_t3722313464  value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t1149908250, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T1149908250_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef ABSSEQUENTIABLE_T3376041011_H
#define ABSSEQUENTIABLE_T3376041011_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.ABSSequentiable
struct  ABSSequentiable_t3376041011  : public RuntimeObject
{
public:
	// DG.Tweening.TweenType DG.Tweening.Core.ABSSequentiable::tweenType
	int32_t ___tweenType_0;
	// System.Single DG.Tweening.Core.ABSSequentiable::sequencedPosition
	float ___sequencedPosition_1;
	// System.Single DG.Tweening.Core.ABSSequentiable::sequencedEndPosition
	float ___sequencedEndPosition_2;
	// DG.Tweening.TweenCallback DG.Tweening.Core.ABSSequentiable::onStart
	TweenCallback_t3727756325 * ___onStart_3;

public:
	inline static int32_t get_offset_of_tweenType_0() { return static_cast<int32_t>(offsetof(ABSSequentiable_t3376041011, ___tweenType_0)); }
	inline int32_t get_tweenType_0() const { return ___tweenType_0; }
	inline int32_t* get_address_of_tweenType_0() { return &___tweenType_0; }
	inline void set_tweenType_0(int32_t value)
	{
		___tweenType_0 = value;
	}

	inline static int32_t get_offset_of_sequencedPosition_1() { return static_cast<int32_t>(offsetof(ABSSequentiable_t3376041011, ___sequencedPosition_1)); }
	inline float get_sequencedPosition_1() const { return ___sequencedPosition_1; }
	inline float* get_address_of_sequencedPosition_1() { return &___sequencedPosition_1; }
	inline void set_sequencedPosition_1(float value)
	{
		___sequencedPosition_1 = value;
	}

	inline static int32_t get_offset_of_sequencedEndPosition_2() { return static_cast<int32_t>(offsetof(ABSSequentiable_t3376041011, ___sequencedEndPosition_2)); }
	inline float get_sequencedEndPosition_2() const { return ___sequencedEndPosition_2; }
	inline float* get_address_of_sequencedEndPosition_2() { return &___sequencedEndPosition_2; }
	inline void set_sequencedEndPosition_2(float value)
	{
		___sequencedEndPosition_2 = value;
	}

	inline static int32_t get_offset_of_onStart_3() { return static_cast<int32_t>(offsetof(ABSSequentiable_t3376041011, ___onStart_3)); }
	inline TweenCallback_t3727756325 * get_onStart_3() const { return ___onStart_3; }
	inline TweenCallback_t3727756325 ** get_address_of_onStart_3() { return &___onStart_3; }
	inline void set_onStart_3(TweenCallback_t3727756325 * value)
	{
		___onStart_3 = value;
		Il2CppCodeGenWriteBarrier((&___onStart_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSSEQUENTIABLE_T3376041011_H
#ifndef PATH_T3614338981_H
#define PATH_T3614338981_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Core.PathCore.Path
struct  Path_t3614338981  : public RuntimeObject
{
public:
	// System.Single[] DG.Tweening.Plugins.Core.PathCore.Path::wpLengths
	SingleU5BU5D_t1444911251* ___wpLengths_2;
	// DG.Tweening.PathType DG.Tweening.Plugins.Core.PathCore.Path::type
	int32_t ___type_3;
	// System.Int32 DG.Tweening.Plugins.Core.PathCore.Path::subdivisionsXSegment
	int32_t ___subdivisionsXSegment_4;
	// System.Int32 DG.Tweening.Plugins.Core.PathCore.Path::subdivisions
	int32_t ___subdivisions_5;
	// UnityEngine.Vector3[] DG.Tweening.Plugins.Core.PathCore.Path::wps
	Vector3U5BU5D_t1718750761* ___wps_6;
	// DG.Tweening.Plugins.Core.PathCore.ControlPoint[] DG.Tweening.Plugins.Core.PathCore.Path::controlPoints
	ControlPointU5BU5D_t1567961855* ___controlPoints_7;
	// System.Single DG.Tweening.Plugins.Core.PathCore.Path::length
	float ___length_8;
	// System.Boolean DG.Tweening.Plugins.Core.PathCore.Path::isFinalized
	bool ___isFinalized_9;
	// System.Single[] DG.Tweening.Plugins.Core.PathCore.Path::timesTable
	SingleU5BU5D_t1444911251* ___timesTable_10;
	// System.Single[] DG.Tweening.Plugins.Core.PathCore.Path::lengthsTable
	SingleU5BU5D_t1444911251* ___lengthsTable_11;
	// System.Int32 DG.Tweening.Plugins.Core.PathCore.Path::linearWPIndex
	int32_t ___linearWPIndex_12;
	// DG.Tweening.Plugins.Core.PathCore.Path DG.Tweening.Plugins.Core.PathCore.Path::_incrementalClone
	Path_t3614338981 * ____incrementalClone_13;
	// System.Int32 DG.Tweening.Plugins.Core.PathCore.Path::_incrementalIndex
	int32_t ____incrementalIndex_14;
	// DG.Tweening.Plugins.Core.PathCore.ABSPathDecoder DG.Tweening.Plugins.Core.PathCore.Path::_decoder
	ABSPathDecoder_t2613982196 * ____decoder_15;
	// System.Boolean DG.Tweening.Plugins.Core.PathCore.Path::_changed
	bool ____changed_16;
	// UnityEngine.Vector3[] DG.Tweening.Plugins.Core.PathCore.Path::nonLinearDrawWps
	Vector3U5BU5D_t1718750761* ___nonLinearDrawWps_17;
	// UnityEngine.Vector3 DG.Tweening.Plugins.Core.PathCore.Path::targetPosition
	Vector3_t3722313464  ___targetPosition_18;
	// System.Nullable`1<UnityEngine.Vector3> DG.Tweening.Plugins.Core.PathCore.Path::lookAtPosition
	Nullable_1_t1149908250  ___lookAtPosition_19;
	// UnityEngine.Color DG.Tweening.Plugins.Core.PathCore.Path::gizmoColor
	Color_t2555686324  ___gizmoColor_20;

public:
	inline static int32_t get_offset_of_wpLengths_2() { return static_cast<int32_t>(offsetof(Path_t3614338981, ___wpLengths_2)); }
	inline SingleU5BU5D_t1444911251* get_wpLengths_2() const { return ___wpLengths_2; }
	inline SingleU5BU5D_t1444911251** get_address_of_wpLengths_2() { return &___wpLengths_2; }
	inline void set_wpLengths_2(SingleU5BU5D_t1444911251* value)
	{
		___wpLengths_2 = value;
		Il2CppCodeGenWriteBarrier((&___wpLengths_2), value);
	}

	inline static int32_t get_offset_of_type_3() { return static_cast<int32_t>(offsetof(Path_t3614338981, ___type_3)); }
	inline int32_t get_type_3() const { return ___type_3; }
	inline int32_t* get_address_of_type_3() { return &___type_3; }
	inline void set_type_3(int32_t value)
	{
		___type_3 = value;
	}

	inline static int32_t get_offset_of_subdivisionsXSegment_4() { return static_cast<int32_t>(offsetof(Path_t3614338981, ___subdivisionsXSegment_4)); }
	inline int32_t get_subdivisionsXSegment_4() const { return ___subdivisionsXSegment_4; }
	inline int32_t* get_address_of_subdivisionsXSegment_4() { return &___subdivisionsXSegment_4; }
	inline void set_subdivisionsXSegment_4(int32_t value)
	{
		___subdivisionsXSegment_4 = value;
	}

	inline static int32_t get_offset_of_subdivisions_5() { return static_cast<int32_t>(offsetof(Path_t3614338981, ___subdivisions_5)); }
	inline int32_t get_subdivisions_5() const { return ___subdivisions_5; }
	inline int32_t* get_address_of_subdivisions_5() { return &___subdivisions_5; }
	inline void set_subdivisions_5(int32_t value)
	{
		___subdivisions_5 = value;
	}

	inline static int32_t get_offset_of_wps_6() { return static_cast<int32_t>(offsetof(Path_t3614338981, ___wps_6)); }
	inline Vector3U5BU5D_t1718750761* get_wps_6() const { return ___wps_6; }
	inline Vector3U5BU5D_t1718750761** get_address_of_wps_6() { return &___wps_6; }
	inline void set_wps_6(Vector3U5BU5D_t1718750761* value)
	{
		___wps_6 = value;
		Il2CppCodeGenWriteBarrier((&___wps_6), value);
	}

	inline static int32_t get_offset_of_controlPoints_7() { return static_cast<int32_t>(offsetof(Path_t3614338981, ___controlPoints_7)); }
	inline ControlPointU5BU5D_t1567961855* get_controlPoints_7() const { return ___controlPoints_7; }
	inline ControlPointU5BU5D_t1567961855** get_address_of_controlPoints_7() { return &___controlPoints_7; }
	inline void set_controlPoints_7(ControlPointU5BU5D_t1567961855* value)
	{
		___controlPoints_7 = value;
		Il2CppCodeGenWriteBarrier((&___controlPoints_7), value);
	}

	inline static int32_t get_offset_of_length_8() { return static_cast<int32_t>(offsetof(Path_t3614338981, ___length_8)); }
	inline float get_length_8() const { return ___length_8; }
	inline float* get_address_of_length_8() { return &___length_8; }
	inline void set_length_8(float value)
	{
		___length_8 = value;
	}

	inline static int32_t get_offset_of_isFinalized_9() { return static_cast<int32_t>(offsetof(Path_t3614338981, ___isFinalized_9)); }
	inline bool get_isFinalized_9() const { return ___isFinalized_9; }
	inline bool* get_address_of_isFinalized_9() { return &___isFinalized_9; }
	inline void set_isFinalized_9(bool value)
	{
		___isFinalized_9 = value;
	}

	inline static int32_t get_offset_of_timesTable_10() { return static_cast<int32_t>(offsetof(Path_t3614338981, ___timesTable_10)); }
	inline SingleU5BU5D_t1444911251* get_timesTable_10() const { return ___timesTable_10; }
	inline SingleU5BU5D_t1444911251** get_address_of_timesTable_10() { return &___timesTable_10; }
	inline void set_timesTable_10(SingleU5BU5D_t1444911251* value)
	{
		___timesTable_10 = value;
		Il2CppCodeGenWriteBarrier((&___timesTable_10), value);
	}

	inline static int32_t get_offset_of_lengthsTable_11() { return static_cast<int32_t>(offsetof(Path_t3614338981, ___lengthsTable_11)); }
	inline SingleU5BU5D_t1444911251* get_lengthsTable_11() const { return ___lengthsTable_11; }
	inline SingleU5BU5D_t1444911251** get_address_of_lengthsTable_11() { return &___lengthsTable_11; }
	inline void set_lengthsTable_11(SingleU5BU5D_t1444911251* value)
	{
		___lengthsTable_11 = value;
		Il2CppCodeGenWriteBarrier((&___lengthsTable_11), value);
	}

	inline static int32_t get_offset_of_linearWPIndex_12() { return static_cast<int32_t>(offsetof(Path_t3614338981, ___linearWPIndex_12)); }
	inline int32_t get_linearWPIndex_12() const { return ___linearWPIndex_12; }
	inline int32_t* get_address_of_linearWPIndex_12() { return &___linearWPIndex_12; }
	inline void set_linearWPIndex_12(int32_t value)
	{
		___linearWPIndex_12 = value;
	}

	inline static int32_t get_offset_of__incrementalClone_13() { return static_cast<int32_t>(offsetof(Path_t3614338981, ____incrementalClone_13)); }
	inline Path_t3614338981 * get__incrementalClone_13() const { return ____incrementalClone_13; }
	inline Path_t3614338981 ** get_address_of__incrementalClone_13() { return &____incrementalClone_13; }
	inline void set__incrementalClone_13(Path_t3614338981 * value)
	{
		____incrementalClone_13 = value;
		Il2CppCodeGenWriteBarrier((&____incrementalClone_13), value);
	}

	inline static int32_t get_offset_of__incrementalIndex_14() { return static_cast<int32_t>(offsetof(Path_t3614338981, ____incrementalIndex_14)); }
	inline int32_t get__incrementalIndex_14() const { return ____incrementalIndex_14; }
	inline int32_t* get_address_of__incrementalIndex_14() { return &____incrementalIndex_14; }
	inline void set__incrementalIndex_14(int32_t value)
	{
		____incrementalIndex_14 = value;
	}

	inline static int32_t get_offset_of__decoder_15() { return static_cast<int32_t>(offsetof(Path_t3614338981, ____decoder_15)); }
	inline ABSPathDecoder_t2613982196 * get__decoder_15() const { return ____decoder_15; }
	inline ABSPathDecoder_t2613982196 ** get_address_of__decoder_15() { return &____decoder_15; }
	inline void set__decoder_15(ABSPathDecoder_t2613982196 * value)
	{
		____decoder_15 = value;
		Il2CppCodeGenWriteBarrier((&____decoder_15), value);
	}

	inline static int32_t get_offset_of__changed_16() { return static_cast<int32_t>(offsetof(Path_t3614338981, ____changed_16)); }
	inline bool get__changed_16() const { return ____changed_16; }
	inline bool* get_address_of__changed_16() { return &____changed_16; }
	inline void set__changed_16(bool value)
	{
		____changed_16 = value;
	}

	inline static int32_t get_offset_of_nonLinearDrawWps_17() { return static_cast<int32_t>(offsetof(Path_t3614338981, ___nonLinearDrawWps_17)); }
	inline Vector3U5BU5D_t1718750761* get_nonLinearDrawWps_17() const { return ___nonLinearDrawWps_17; }
	inline Vector3U5BU5D_t1718750761** get_address_of_nonLinearDrawWps_17() { return &___nonLinearDrawWps_17; }
	inline void set_nonLinearDrawWps_17(Vector3U5BU5D_t1718750761* value)
	{
		___nonLinearDrawWps_17 = value;
		Il2CppCodeGenWriteBarrier((&___nonLinearDrawWps_17), value);
	}

	inline static int32_t get_offset_of_targetPosition_18() { return static_cast<int32_t>(offsetof(Path_t3614338981, ___targetPosition_18)); }
	inline Vector3_t3722313464  get_targetPosition_18() const { return ___targetPosition_18; }
	inline Vector3_t3722313464 * get_address_of_targetPosition_18() { return &___targetPosition_18; }
	inline void set_targetPosition_18(Vector3_t3722313464  value)
	{
		___targetPosition_18 = value;
	}

	inline static int32_t get_offset_of_lookAtPosition_19() { return static_cast<int32_t>(offsetof(Path_t3614338981, ___lookAtPosition_19)); }
	inline Nullable_1_t1149908250  get_lookAtPosition_19() const { return ___lookAtPosition_19; }
	inline Nullable_1_t1149908250 * get_address_of_lookAtPosition_19() { return &___lookAtPosition_19; }
	inline void set_lookAtPosition_19(Nullable_1_t1149908250  value)
	{
		___lookAtPosition_19 = value;
	}

	inline static int32_t get_offset_of_gizmoColor_20() { return static_cast<int32_t>(offsetof(Path_t3614338981, ___gizmoColor_20)); }
	inline Color_t2555686324  get_gizmoColor_20() const { return ___gizmoColor_20; }
	inline Color_t2555686324 * get_address_of_gizmoColor_20() { return &___gizmoColor_20; }
	inline void set_gizmoColor_20(Color_t2555686324  value)
	{
		___gizmoColor_20 = value;
	}
};

struct Path_t3614338981_StaticFields
{
public:
	// DG.Tweening.Plugins.Core.PathCore.CatmullRomDecoder DG.Tweening.Plugins.Core.PathCore.Path::_catmullRomDecoder
	CatmullRomDecoder_t2053048079 * ____catmullRomDecoder_0;
	// DG.Tweening.Plugins.Core.PathCore.LinearDecoder DG.Tweening.Plugins.Core.PathCore.Path::_linearDecoder
	LinearDecoder_t2708327777 * ____linearDecoder_1;

public:
	inline static int32_t get_offset_of__catmullRomDecoder_0() { return static_cast<int32_t>(offsetof(Path_t3614338981_StaticFields, ____catmullRomDecoder_0)); }
	inline CatmullRomDecoder_t2053048079 * get__catmullRomDecoder_0() const { return ____catmullRomDecoder_0; }
	inline CatmullRomDecoder_t2053048079 ** get_address_of__catmullRomDecoder_0() { return &____catmullRomDecoder_0; }
	inline void set__catmullRomDecoder_0(CatmullRomDecoder_t2053048079 * value)
	{
		____catmullRomDecoder_0 = value;
		Il2CppCodeGenWriteBarrier((&____catmullRomDecoder_0), value);
	}

	inline static int32_t get_offset_of__linearDecoder_1() { return static_cast<int32_t>(offsetof(Path_t3614338981_StaticFields, ____linearDecoder_1)); }
	inline LinearDecoder_t2708327777 * get__linearDecoder_1() const { return ____linearDecoder_1; }
	inline LinearDecoder_t2708327777 ** get_address_of__linearDecoder_1() { return &____linearDecoder_1; }
	inline void set__linearDecoder_1(LinearDecoder_t2708327777 * value)
	{
		____linearDecoder_1 = value;
		Il2CppCodeGenWriteBarrier((&____linearDecoder_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PATH_T3614338981_H
#ifndef PATHOPTIONS_T2074623791_H
#define PATHOPTIONS_T2074623791_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Options.PathOptions
struct  PathOptions_t2074623791 
{
public:
	// DG.Tweening.PathMode DG.Tweening.Plugins.Options.PathOptions::mode
	int32_t ___mode_0;
	// DG.Tweening.Plugins.Options.OrientType DG.Tweening.Plugins.Options.PathOptions::orientType
	int32_t ___orientType_1;
	// DG.Tweening.AxisConstraint DG.Tweening.Plugins.Options.PathOptions::lockPositionAxis
	int32_t ___lockPositionAxis_2;
	// DG.Tweening.AxisConstraint DG.Tweening.Plugins.Options.PathOptions::lockRotationAxis
	int32_t ___lockRotationAxis_3;
	// System.Boolean DG.Tweening.Plugins.Options.PathOptions::isClosedPath
	bool ___isClosedPath_4;
	// UnityEngine.Vector3 DG.Tweening.Plugins.Options.PathOptions::lookAtPosition
	Vector3_t3722313464  ___lookAtPosition_5;
	// UnityEngine.Transform DG.Tweening.Plugins.Options.PathOptions::lookAtTransform
	Transform_t3600365921 * ___lookAtTransform_6;
	// System.Single DG.Tweening.Plugins.Options.PathOptions::lookAhead
	float ___lookAhead_7;
	// System.Boolean DG.Tweening.Plugins.Options.PathOptions::hasCustomForwardDirection
	bool ___hasCustomForwardDirection_8;
	// UnityEngine.Quaternion DG.Tweening.Plugins.Options.PathOptions::forward
	Quaternion_t2301928331  ___forward_9;
	// System.Boolean DG.Tweening.Plugins.Options.PathOptions::useLocalPosition
	bool ___useLocalPosition_10;
	// UnityEngine.Transform DG.Tweening.Plugins.Options.PathOptions::parent
	Transform_t3600365921 * ___parent_11;
	// System.Boolean DG.Tweening.Plugins.Options.PathOptions::isRigidbody
	bool ___isRigidbody_12;
	// UnityEngine.Quaternion DG.Tweening.Plugins.Options.PathOptions::startupRot
	Quaternion_t2301928331  ___startupRot_13;
	// System.Single DG.Tweening.Plugins.Options.PathOptions::startupZRot
	float ___startupZRot_14;

public:
	inline static int32_t get_offset_of_mode_0() { return static_cast<int32_t>(offsetof(PathOptions_t2074623791, ___mode_0)); }
	inline int32_t get_mode_0() const { return ___mode_0; }
	inline int32_t* get_address_of_mode_0() { return &___mode_0; }
	inline void set_mode_0(int32_t value)
	{
		___mode_0 = value;
	}

	inline static int32_t get_offset_of_orientType_1() { return static_cast<int32_t>(offsetof(PathOptions_t2074623791, ___orientType_1)); }
	inline int32_t get_orientType_1() const { return ___orientType_1; }
	inline int32_t* get_address_of_orientType_1() { return &___orientType_1; }
	inline void set_orientType_1(int32_t value)
	{
		___orientType_1 = value;
	}

	inline static int32_t get_offset_of_lockPositionAxis_2() { return static_cast<int32_t>(offsetof(PathOptions_t2074623791, ___lockPositionAxis_2)); }
	inline int32_t get_lockPositionAxis_2() const { return ___lockPositionAxis_2; }
	inline int32_t* get_address_of_lockPositionAxis_2() { return &___lockPositionAxis_2; }
	inline void set_lockPositionAxis_2(int32_t value)
	{
		___lockPositionAxis_2 = value;
	}

	inline static int32_t get_offset_of_lockRotationAxis_3() { return static_cast<int32_t>(offsetof(PathOptions_t2074623791, ___lockRotationAxis_3)); }
	inline int32_t get_lockRotationAxis_3() const { return ___lockRotationAxis_3; }
	inline int32_t* get_address_of_lockRotationAxis_3() { return &___lockRotationAxis_3; }
	inline void set_lockRotationAxis_3(int32_t value)
	{
		___lockRotationAxis_3 = value;
	}

	inline static int32_t get_offset_of_isClosedPath_4() { return static_cast<int32_t>(offsetof(PathOptions_t2074623791, ___isClosedPath_4)); }
	inline bool get_isClosedPath_4() const { return ___isClosedPath_4; }
	inline bool* get_address_of_isClosedPath_4() { return &___isClosedPath_4; }
	inline void set_isClosedPath_4(bool value)
	{
		___isClosedPath_4 = value;
	}

	inline static int32_t get_offset_of_lookAtPosition_5() { return static_cast<int32_t>(offsetof(PathOptions_t2074623791, ___lookAtPosition_5)); }
	inline Vector3_t3722313464  get_lookAtPosition_5() const { return ___lookAtPosition_5; }
	inline Vector3_t3722313464 * get_address_of_lookAtPosition_5() { return &___lookAtPosition_5; }
	inline void set_lookAtPosition_5(Vector3_t3722313464  value)
	{
		___lookAtPosition_5 = value;
	}

	inline static int32_t get_offset_of_lookAtTransform_6() { return static_cast<int32_t>(offsetof(PathOptions_t2074623791, ___lookAtTransform_6)); }
	inline Transform_t3600365921 * get_lookAtTransform_6() const { return ___lookAtTransform_6; }
	inline Transform_t3600365921 ** get_address_of_lookAtTransform_6() { return &___lookAtTransform_6; }
	inline void set_lookAtTransform_6(Transform_t3600365921 * value)
	{
		___lookAtTransform_6 = value;
		Il2CppCodeGenWriteBarrier((&___lookAtTransform_6), value);
	}

	inline static int32_t get_offset_of_lookAhead_7() { return static_cast<int32_t>(offsetof(PathOptions_t2074623791, ___lookAhead_7)); }
	inline float get_lookAhead_7() const { return ___lookAhead_7; }
	inline float* get_address_of_lookAhead_7() { return &___lookAhead_7; }
	inline void set_lookAhead_7(float value)
	{
		___lookAhead_7 = value;
	}

	inline static int32_t get_offset_of_hasCustomForwardDirection_8() { return static_cast<int32_t>(offsetof(PathOptions_t2074623791, ___hasCustomForwardDirection_8)); }
	inline bool get_hasCustomForwardDirection_8() const { return ___hasCustomForwardDirection_8; }
	inline bool* get_address_of_hasCustomForwardDirection_8() { return &___hasCustomForwardDirection_8; }
	inline void set_hasCustomForwardDirection_8(bool value)
	{
		___hasCustomForwardDirection_8 = value;
	}

	inline static int32_t get_offset_of_forward_9() { return static_cast<int32_t>(offsetof(PathOptions_t2074623791, ___forward_9)); }
	inline Quaternion_t2301928331  get_forward_9() const { return ___forward_9; }
	inline Quaternion_t2301928331 * get_address_of_forward_9() { return &___forward_9; }
	inline void set_forward_9(Quaternion_t2301928331  value)
	{
		___forward_9 = value;
	}

	inline static int32_t get_offset_of_useLocalPosition_10() { return static_cast<int32_t>(offsetof(PathOptions_t2074623791, ___useLocalPosition_10)); }
	inline bool get_useLocalPosition_10() const { return ___useLocalPosition_10; }
	inline bool* get_address_of_useLocalPosition_10() { return &___useLocalPosition_10; }
	inline void set_useLocalPosition_10(bool value)
	{
		___useLocalPosition_10 = value;
	}

	inline static int32_t get_offset_of_parent_11() { return static_cast<int32_t>(offsetof(PathOptions_t2074623791, ___parent_11)); }
	inline Transform_t3600365921 * get_parent_11() const { return ___parent_11; }
	inline Transform_t3600365921 ** get_address_of_parent_11() { return &___parent_11; }
	inline void set_parent_11(Transform_t3600365921 * value)
	{
		___parent_11 = value;
		Il2CppCodeGenWriteBarrier((&___parent_11), value);
	}

	inline static int32_t get_offset_of_isRigidbody_12() { return static_cast<int32_t>(offsetof(PathOptions_t2074623791, ___isRigidbody_12)); }
	inline bool get_isRigidbody_12() const { return ___isRigidbody_12; }
	inline bool* get_address_of_isRigidbody_12() { return &___isRigidbody_12; }
	inline void set_isRigidbody_12(bool value)
	{
		___isRigidbody_12 = value;
	}

	inline static int32_t get_offset_of_startupRot_13() { return static_cast<int32_t>(offsetof(PathOptions_t2074623791, ___startupRot_13)); }
	inline Quaternion_t2301928331  get_startupRot_13() const { return ___startupRot_13; }
	inline Quaternion_t2301928331 * get_address_of_startupRot_13() { return &___startupRot_13; }
	inline void set_startupRot_13(Quaternion_t2301928331  value)
	{
		___startupRot_13 = value;
	}

	inline static int32_t get_offset_of_startupZRot_14() { return static_cast<int32_t>(offsetof(PathOptions_t2074623791, ___startupZRot_14)); }
	inline float get_startupZRot_14() const { return ___startupZRot_14; }
	inline float* get_address_of_startupZRot_14() { return &___startupZRot_14; }
	inline void set_startupZRot_14(float value)
	{
		___startupZRot_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of DG.Tweening.Plugins.Options.PathOptions
struct PathOptions_t2074623791_marshaled_pinvoke
{
	int32_t ___mode_0;
	int32_t ___orientType_1;
	int32_t ___lockPositionAxis_2;
	int32_t ___lockRotationAxis_3;
	int32_t ___isClosedPath_4;
	Vector3_t3722313464  ___lookAtPosition_5;
	Transform_t3600365921 * ___lookAtTransform_6;
	float ___lookAhead_7;
	int32_t ___hasCustomForwardDirection_8;
	Quaternion_t2301928331  ___forward_9;
	int32_t ___useLocalPosition_10;
	Transform_t3600365921 * ___parent_11;
	int32_t ___isRigidbody_12;
	Quaternion_t2301928331  ___startupRot_13;
	float ___startupZRot_14;
};
// Native definition for COM marshalling of DG.Tweening.Plugins.Options.PathOptions
struct PathOptions_t2074623791_marshaled_com
{
	int32_t ___mode_0;
	int32_t ___orientType_1;
	int32_t ___lockPositionAxis_2;
	int32_t ___lockRotationAxis_3;
	int32_t ___isClosedPath_4;
	Vector3_t3722313464  ___lookAtPosition_5;
	Transform_t3600365921 * ___lookAtTransform_6;
	float ___lookAhead_7;
	int32_t ___hasCustomForwardDirection_8;
	Quaternion_t2301928331  ___forward_9;
	int32_t ___useLocalPosition_10;
	Transform_t3600365921 * ___parent_11;
	int32_t ___isRigidbody_12;
	Quaternion_t2301928331  ___startupRot_13;
	float ___startupZRot_14;
};
#endif // PATHOPTIONS_T2074623791_H
#ifndef QUATERNIONOPTIONS_T2974423933_H
#define QUATERNIONOPTIONS_T2974423933_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Options.QuaternionOptions
struct  QuaternionOptions_t2974423933 
{
public:
	// DG.Tweening.RotateMode DG.Tweening.Plugins.Options.QuaternionOptions::rotateMode
	int32_t ___rotateMode_0;
	// DG.Tweening.AxisConstraint DG.Tweening.Plugins.Options.QuaternionOptions::axisConstraint
	int32_t ___axisConstraint_1;
	// UnityEngine.Vector3 DG.Tweening.Plugins.Options.QuaternionOptions::up
	Vector3_t3722313464  ___up_2;

public:
	inline static int32_t get_offset_of_rotateMode_0() { return static_cast<int32_t>(offsetof(QuaternionOptions_t2974423933, ___rotateMode_0)); }
	inline int32_t get_rotateMode_0() const { return ___rotateMode_0; }
	inline int32_t* get_address_of_rotateMode_0() { return &___rotateMode_0; }
	inline void set_rotateMode_0(int32_t value)
	{
		___rotateMode_0 = value;
	}

	inline static int32_t get_offset_of_axisConstraint_1() { return static_cast<int32_t>(offsetof(QuaternionOptions_t2974423933, ___axisConstraint_1)); }
	inline int32_t get_axisConstraint_1() const { return ___axisConstraint_1; }
	inline int32_t* get_address_of_axisConstraint_1() { return &___axisConstraint_1; }
	inline void set_axisConstraint_1(int32_t value)
	{
		___axisConstraint_1 = value;
	}

	inline static int32_t get_offset_of_up_2() { return static_cast<int32_t>(offsetof(QuaternionOptions_t2974423933, ___up_2)); }
	inline Vector3_t3722313464  get_up_2() const { return ___up_2; }
	inline Vector3_t3722313464 * get_address_of_up_2() { return &___up_2; }
	inline void set_up_2(Vector3_t3722313464  value)
	{
		___up_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNIONOPTIONS_T2974423933_H
#ifndef STRINGOPTIONS_T3992490940_H
#define STRINGOPTIONS_T3992490940_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Options.StringOptions
struct  StringOptions_t3992490940 
{
public:
	// System.Boolean DG.Tweening.Plugins.Options.StringOptions::richTextEnabled
	bool ___richTextEnabled_0;
	// DG.Tweening.ScrambleMode DG.Tweening.Plugins.Options.StringOptions::scrambleMode
	int32_t ___scrambleMode_1;
	// System.Char[] DG.Tweening.Plugins.Options.StringOptions::scrambledChars
	CharU5BU5D_t3528271667* ___scrambledChars_2;
	// System.Int32 DG.Tweening.Plugins.Options.StringOptions::startValueStrippedLength
	int32_t ___startValueStrippedLength_3;
	// System.Int32 DG.Tweening.Plugins.Options.StringOptions::changeValueStrippedLength
	int32_t ___changeValueStrippedLength_4;

public:
	inline static int32_t get_offset_of_richTextEnabled_0() { return static_cast<int32_t>(offsetof(StringOptions_t3992490940, ___richTextEnabled_0)); }
	inline bool get_richTextEnabled_0() const { return ___richTextEnabled_0; }
	inline bool* get_address_of_richTextEnabled_0() { return &___richTextEnabled_0; }
	inline void set_richTextEnabled_0(bool value)
	{
		___richTextEnabled_0 = value;
	}

	inline static int32_t get_offset_of_scrambleMode_1() { return static_cast<int32_t>(offsetof(StringOptions_t3992490940, ___scrambleMode_1)); }
	inline int32_t get_scrambleMode_1() const { return ___scrambleMode_1; }
	inline int32_t* get_address_of_scrambleMode_1() { return &___scrambleMode_1; }
	inline void set_scrambleMode_1(int32_t value)
	{
		___scrambleMode_1 = value;
	}

	inline static int32_t get_offset_of_scrambledChars_2() { return static_cast<int32_t>(offsetof(StringOptions_t3992490940, ___scrambledChars_2)); }
	inline CharU5BU5D_t3528271667* get_scrambledChars_2() const { return ___scrambledChars_2; }
	inline CharU5BU5D_t3528271667** get_address_of_scrambledChars_2() { return &___scrambledChars_2; }
	inline void set_scrambledChars_2(CharU5BU5D_t3528271667* value)
	{
		___scrambledChars_2 = value;
		Il2CppCodeGenWriteBarrier((&___scrambledChars_2), value);
	}

	inline static int32_t get_offset_of_startValueStrippedLength_3() { return static_cast<int32_t>(offsetof(StringOptions_t3992490940, ___startValueStrippedLength_3)); }
	inline int32_t get_startValueStrippedLength_3() const { return ___startValueStrippedLength_3; }
	inline int32_t* get_address_of_startValueStrippedLength_3() { return &___startValueStrippedLength_3; }
	inline void set_startValueStrippedLength_3(int32_t value)
	{
		___startValueStrippedLength_3 = value;
	}

	inline static int32_t get_offset_of_changeValueStrippedLength_4() { return static_cast<int32_t>(offsetof(StringOptions_t3992490940, ___changeValueStrippedLength_4)); }
	inline int32_t get_changeValueStrippedLength_4() const { return ___changeValueStrippedLength_4; }
	inline int32_t* get_address_of_changeValueStrippedLength_4() { return &___changeValueStrippedLength_4; }
	inline void set_changeValueStrippedLength_4(int32_t value)
	{
		___changeValueStrippedLength_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of DG.Tweening.Plugins.Options.StringOptions
struct StringOptions_t3992490940_marshaled_pinvoke
{
	int32_t ___richTextEnabled_0;
	int32_t ___scrambleMode_1;
	uint8_t* ___scrambledChars_2;
	int32_t ___startValueStrippedLength_3;
	int32_t ___changeValueStrippedLength_4;
};
// Native definition for COM marshalling of DG.Tweening.Plugins.Options.StringOptions
struct StringOptions_t3992490940_marshaled_com
{
	int32_t ___richTextEnabled_0;
	int32_t ___scrambleMode_1;
	uint8_t* ___scrambledChars_2;
	int32_t ___startValueStrippedLength_3;
	int32_t ___changeValueStrippedLength_4;
};
#endif // STRINGOPTIONS_T3992490940_H
#ifndef VECTOR3ARRAYOPTIONS_T534739431_H
#define VECTOR3ARRAYOPTIONS_T534739431_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Options.Vector3ArrayOptions
struct  Vector3ArrayOptions_t534739431 
{
public:
	// DG.Tweening.AxisConstraint DG.Tweening.Plugins.Options.Vector3ArrayOptions::axisConstraint
	int32_t ___axisConstraint_0;
	// System.Boolean DG.Tweening.Plugins.Options.Vector3ArrayOptions::snapping
	bool ___snapping_1;
	// System.Single[] DG.Tweening.Plugins.Options.Vector3ArrayOptions::durations
	SingleU5BU5D_t1444911251* ___durations_2;

public:
	inline static int32_t get_offset_of_axisConstraint_0() { return static_cast<int32_t>(offsetof(Vector3ArrayOptions_t534739431, ___axisConstraint_0)); }
	inline int32_t get_axisConstraint_0() const { return ___axisConstraint_0; }
	inline int32_t* get_address_of_axisConstraint_0() { return &___axisConstraint_0; }
	inline void set_axisConstraint_0(int32_t value)
	{
		___axisConstraint_0 = value;
	}

	inline static int32_t get_offset_of_snapping_1() { return static_cast<int32_t>(offsetof(Vector3ArrayOptions_t534739431, ___snapping_1)); }
	inline bool get_snapping_1() const { return ___snapping_1; }
	inline bool* get_address_of_snapping_1() { return &___snapping_1; }
	inline void set_snapping_1(bool value)
	{
		___snapping_1 = value;
	}

	inline static int32_t get_offset_of_durations_2() { return static_cast<int32_t>(offsetof(Vector3ArrayOptions_t534739431, ___durations_2)); }
	inline SingleU5BU5D_t1444911251* get_durations_2() const { return ___durations_2; }
	inline SingleU5BU5D_t1444911251** get_address_of_durations_2() { return &___durations_2; }
	inline void set_durations_2(SingleU5BU5D_t1444911251* value)
	{
		___durations_2 = value;
		Il2CppCodeGenWriteBarrier((&___durations_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of DG.Tweening.Plugins.Options.Vector3ArrayOptions
struct Vector3ArrayOptions_t534739431_marshaled_pinvoke
{
	int32_t ___axisConstraint_0;
	int32_t ___snapping_1;
	float* ___durations_2;
};
// Native definition for COM marshalling of DG.Tweening.Plugins.Options.Vector3ArrayOptions
struct Vector3ArrayOptions_t534739431_marshaled_com
{
	int32_t ___axisConstraint_0;
	int32_t ___snapping_1;
	float* ___durations_2;
};
#endif // VECTOR3ARRAYOPTIONS_T534739431_H
#ifndef VECTOROPTIONS_T1354903650_H
#define VECTOROPTIONS_T1354903650_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Options.VectorOptions
struct  VectorOptions_t1354903650 
{
public:
	// DG.Tweening.AxisConstraint DG.Tweening.Plugins.Options.VectorOptions::axisConstraint
	int32_t ___axisConstraint_0;
	// System.Boolean DG.Tweening.Plugins.Options.VectorOptions::snapping
	bool ___snapping_1;

public:
	inline static int32_t get_offset_of_axisConstraint_0() { return static_cast<int32_t>(offsetof(VectorOptions_t1354903650, ___axisConstraint_0)); }
	inline int32_t get_axisConstraint_0() const { return ___axisConstraint_0; }
	inline int32_t* get_address_of_axisConstraint_0() { return &___axisConstraint_0; }
	inline void set_axisConstraint_0(int32_t value)
	{
		___axisConstraint_0 = value;
	}

	inline static int32_t get_offset_of_snapping_1() { return static_cast<int32_t>(offsetof(VectorOptions_t1354903650, ___snapping_1)); }
	inline bool get_snapping_1() const { return ___snapping_1; }
	inline bool* get_address_of_snapping_1() { return &___snapping_1; }
	inline void set_snapping_1(bool value)
	{
		___snapping_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of DG.Tweening.Plugins.Options.VectorOptions
struct VectorOptions_t1354903650_marshaled_pinvoke
{
	int32_t ___axisConstraint_0;
	int32_t ___snapping_1;
};
// Native definition for COM marshalling of DG.Tweening.Plugins.Options.VectorOptions
struct VectorOptions_t1354903650_marshaled_com
{
	int32_t ___axisConstraint_0;
	int32_t ___snapping_1;
};
#endif // VECTOROPTIONS_T1354903650_H
#ifndef TWEENPARAMS_T4171191025_H
#define TWEENPARAMS_T4171191025_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.TweenParams
struct  TweenParams_t4171191025  : public RuntimeObject
{
public:
	// System.Object DG.Tweening.TweenParams::id
	RuntimeObject * ___id_1;
	// System.Object DG.Tweening.TweenParams::target
	RuntimeObject * ___target_2;
	// DG.Tweening.UpdateType DG.Tweening.TweenParams::updateType
	int32_t ___updateType_3;
	// System.Boolean DG.Tweening.TweenParams::isIndependentUpdate
	bool ___isIndependentUpdate_4;
	// DG.Tweening.TweenCallback DG.Tweening.TweenParams::onStart
	TweenCallback_t3727756325 * ___onStart_5;
	// DG.Tweening.TweenCallback DG.Tweening.TweenParams::onPlay
	TweenCallback_t3727756325 * ___onPlay_6;
	// DG.Tweening.TweenCallback DG.Tweening.TweenParams::onRewind
	TweenCallback_t3727756325 * ___onRewind_7;
	// DG.Tweening.TweenCallback DG.Tweening.TweenParams::onUpdate
	TweenCallback_t3727756325 * ___onUpdate_8;
	// DG.Tweening.TweenCallback DG.Tweening.TweenParams::onStepComplete
	TweenCallback_t3727756325 * ___onStepComplete_9;
	// DG.Tweening.TweenCallback DG.Tweening.TweenParams::onComplete
	TweenCallback_t3727756325 * ___onComplete_10;
	// DG.Tweening.TweenCallback DG.Tweening.TweenParams::onKill
	TweenCallback_t3727756325 * ___onKill_11;
	// DG.Tweening.TweenCallback`1<System.Int32> DG.Tweening.TweenParams::onWaypointChange
	TweenCallback_1_t3009965658 * ___onWaypointChange_12;
	// System.Boolean DG.Tweening.TweenParams::isRecyclable
	bool ___isRecyclable_13;
	// System.Boolean DG.Tweening.TweenParams::isSpeedBased
	bool ___isSpeedBased_14;
	// System.Boolean DG.Tweening.TweenParams::autoKill
	bool ___autoKill_15;
	// System.Int32 DG.Tweening.TweenParams::loops
	int32_t ___loops_16;
	// DG.Tweening.LoopType DG.Tweening.TweenParams::loopType
	int32_t ___loopType_17;
	// System.Single DG.Tweening.TweenParams::delay
	float ___delay_18;
	// System.Boolean DG.Tweening.TweenParams::isRelative
	bool ___isRelative_19;
	// DG.Tweening.Ease DG.Tweening.TweenParams::easeType
	int32_t ___easeType_20;
	// DG.Tweening.EaseFunction DG.Tweening.TweenParams::customEase
	EaseFunction_t3531141372 * ___customEase_21;
	// System.Single DG.Tweening.TweenParams::easeOvershootOrAmplitude
	float ___easeOvershootOrAmplitude_22;
	// System.Single DG.Tweening.TweenParams::easePeriod
	float ___easePeriod_23;

public:
	inline static int32_t get_offset_of_id_1() { return static_cast<int32_t>(offsetof(TweenParams_t4171191025, ___id_1)); }
	inline RuntimeObject * get_id_1() const { return ___id_1; }
	inline RuntimeObject ** get_address_of_id_1() { return &___id_1; }
	inline void set_id_1(RuntimeObject * value)
	{
		___id_1 = value;
		Il2CppCodeGenWriteBarrier((&___id_1), value);
	}

	inline static int32_t get_offset_of_target_2() { return static_cast<int32_t>(offsetof(TweenParams_t4171191025, ___target_2)); }
	inline RuntimeObject * get_target_2() const { return ___target_2; }
	inline RuntimeObject ** get_address_of_target_2() { return &___target_2; }
	inline void set_target_2(RuntimeObject * value)
	{
		___target_2 = value;
		Il2CppCodeGenWriteBarrier((&___target_2), value);
	}

	inline static int32_t get_offset_of_updateType_3() { return static_cast<int32_t>(offsetof(TweenParams_t4171191025, ___updateType_3)); }
	inline int32_t get_updateType_3() const { return ___updateType_3; }
	inline int32_t* get_address_of_updateType_3() { return &___updateType_3; }
	inline void set_updateType_3(int32_t value)
	{
		___updateType_3 = value;
	}

	inline static int32_t get_offset_of_isIndependentUpdate_4() { return static_cast<int32_t>(offsetof(TweenParams_t4171191025, ___isIndependentUpdate_4)); }
	inline bool get_isIndependentUpdate_4() const { return ___isIndependentUpdate_4; }
	inline bool* get_address_of_isIndependentUpdate_4() { return &___isIndependentUpdate_4; }
	inline void set_isIndependentUpdate_4(bool value)
	{
		___isIndependentUpdate_4 = value;
	}

	inline static int32_t get_offset_of_onStart_5() { return static_cast<int32_t>(offsetof(TweenParams_t4171191025, ___onStart_5)); }
	inline TweenCallback_t3727756325 * get_onStart_5() const { return ___onStart_5; }
	inline TweenCallback_t3727756325 ** get_address_of_onStart_5() { return &___onStart_5; }
	inline void set_onStart_5(TweenCallback_t3727756325 * value)
	{
		___onStart_5 = value;
		Il2CppCodeGenWriteBarrier((&___onStart_5), value);
	}

	inline static int32_t get_offset_of_onPlay_6() { return static_cast<int32_t>(offsetof(TweenParams_t4171191025, ___onPlay_6)); }
	inline TweenCallback_t3727756325 * get_onPlay_6() const { return ___onPlay_6; }
	inline TweenCallback_t3727756325 ** get_address_of_onPlay_6() { return &___onPlay_6; }
	inline void set_onPlay_6(TweenCallback_t3727756325 * value)
	{
		___onPlay_6 = value;
		Il2CppCodeGenWriteBarrier((&___onPlay_6), value);
	}

	inline static int32_t get_offset_of_onRewind_7() { return static_cast<int32_t>(offsetof(TweenParams_t4171191025, ___onRewind_7)); }
	inline TweenCallback_t3727756325 * get_onRewind_7() const { return ___onRewind_7; }
	inline TweenCallback_t3727756325 ** get_address_of_onRewind_7() { return &___onRewind_7; }
	inline void set_onRewind_7(TweenCallback_t3727756325 * value)
	{
		___onRewind_7 = value;
		Il2CppCodeGenWriteBarrier((&___onRewind_7), value);
	}

	inline static int32_t get_offset_of_onUpdate_8() { return static_cast<int32_t>(offsetof(TweenParams_t4171191025, ___onUpdate_8)); }
	inline TweenCallback_t3727756325 * get_onUpdate_8() const { return ___onUpdate_8; }
	inline TweenCallback_t3727756325 ** get_address_of_onUpdate_8() { return &___onUpdate_8; }
	inline void set_onUpdate_8(TweenCallback_t3727756325 * value)
	{
		___onUpdate_8 = value;
		Il2CppCodeGenWriteBarrier((&___onUpdate_8), value);
	}

	inline static int32_t get_offset_of_onStepComplete_9() { return static_cast<int32_t>(offsetof(TweenParams_t4171191025, ___onStepComplete_9)); }
	inline TweenCallback_t3727756325 * get_onStepComplete_9() const { return ___onStepComplete_9; }
	inline TweenCallback_t3727756325 ** get_address_of_onStepComplete_9() { return &___onStepComplete_9; }
	inline void set_onStepComplete_9(TweenCallback_t3727756325 * value)
	{
		___onStepComplete_9 = value;
		Il2CppCodeGenWriteBarrier((&___onStepComplete_9), value);
	}

	inline static int32_t get_offset_of_onComplete_10() { return static_cast<int32_t>(offsetof(TweenParams_t4171191025, ___onComplete_10)); }
	inline TweenCallback_t3727756325 * get_onComplete_10() const { return ___onComplete_10; }
	inline TweenCallback_t3727756325 ** get_address_of_onComplete_10() { return &___onComplete_10; }
	inline void set_onComplete_10(TweenCallback_t3727756325 * value)
	{
		___onComplete_10 = value;
		Il2CppCodeGenWriteBarrier((&___onComplete_10), value);
	}

	inline static int32_t get_offset_of_onKill_11() { return static_cast<int32_t>(offsetof(TweenParams_t4171191025, ___onKill_11)); }
	inline TweenCallback_t3727756325 * get_onKill_11() const { return ___onKill_11; }
	inline TweenCallback_t3727756325 ** get_address_of_onKill_11() { return &___onKill_11; }
	inline void set_onKill_11(TweenCallback_t3727756325 * value)
	{
		___onKill_11 = value;
		Il2CppCodeGenWriteBarrier((&___onKill_11), value);
	}

	inline static int32_t get_offset_of_onWaypointChange_12() { return static_cast<int32_t>(offsetof(TweenParams_t4171191025, ___onWaypointChange_12)); }
	inline TweenCallback_1_t3009965658 * get_onWaypointChange_12() const { return ___onWaypointChange_12; }
	inline TweenCallback_1_t3009965658 ** get_address_of_onWaypointChange_12() { return &___onWaypointChange_12; }
	inline void set_onWaypointChange_12(TweenCallback_1_t3009965658 * value)
	{
		___onWaypointChange_12 = value;
		Il2CppCodeGenWriteBarrier((&___onWaypointChange_12), value);
	}

	inline static int32_t get_offset_of_isRecyclable_13() { return static_cast<int32_t>(offsetof(TweenParams_t4171191025, ___isRecyclable_13)); }
	inline bool get_isRecyclable_13() const { return ___isRecyclable_13; }
	inline bool* get_address_of_isRecyclable_13() { return &___isRecyclable_13; }
	inline void set_isRecyclable_13(bool value)
	{
		___isRecyclable_13 = value;
	}

	inline static int32_t get_offset_of_isSpeedBased_14() { return static_cast<int32_t>(offsetof(TweenParams_t4171191025, ___isSpeedBased_14)); }
	inline bool get_isSpeedBased_14() const { return ___isSpeedBased_14; }
	inline bool* get_address_of_isSpeedBased_14() { return &___isSpeedBased_14; }
	inline void set_isSpeedBased_14(bool value)
	{
		___isSpeedBased_14 = value;
	}

	inline static int32_t get_offset_of_autoKill_15() { return static_cast<int32_t>(offsetof(TweenParams_t4171191025, ___autoKill_15)); }
	inline bool get_autoKill_15() const { return ___autoKill_15; }
	inline bool* get_address_of_autoKill_15() { return &___autoKill_15; }
	inline void set_autoKill_15(bool value)
	{
		___autoKill_15 = value;
	}

	inline static int32_t get_offset_of_loops_16() { return static_cast<int32_t>(offsetof(TweenParams_t4171191025, ___loops_16)); }
	inline int32_t get_loops_16() const { return ___loops_16; }
	inline int32_t* get_address_of_loops_16() { return &___loops_16; }
	inline void set_loops_16(int32_t value)
	{
		___loops_16 = value;
	}

	inline static int32_t get_offset_of_loopType_17() { return static_cast<int32_t>(offsetof(TweenParams_t4171191025, ___loopType_17)); }
	inline int32_t get_loopType_17() const { return ___loopType_17; }
	inline int32_t* get_address_of_loopType_17() { return &___loopType_17; }
	inline void set_loopType_17(int32_t value)
	{
		___loopType_17 = value;
	}

	inline static int32_t get_offset_of_delay_18() { return static_cast<int32_t>(offsetof(TweenParams_t4171191025, ___delay_18)); }
	inline float get_delay_18() const { return ___delay_18; }
	inline float* get_address_of_delay_18() { return &___delay_18; }
	inline void set_delay_18(float value)
	{
		___delay_18 = value;
	}

	inline static int32_t get_offset_of_isRelative_19() { return static_cast<int32_t>(offsetof(TweenParams_t4171191025, ___isRelative_19)); }
	inline bool get_isRelative_19() const { return ___isRelative_19; }
	inline bool* get_address_of_isRelative_19() { return &___isRelative_19; }
	inline void set_isRelative_19(bool value)
	{
		___isRelative_19 = value;
	}

	inline static int32_t get_offset_of_easeType_20() { return static_cast<int32_t>(offsetof(TweenParams_t4171191025, ___easeType_20)); }
	inline int32_t get_easeType_20() const { return ___easeType_20; }
	inline int32_t* get_address_of_easeType_20() { return &___easeType_20; }
	inline void set_easeType_20(int32_t value)
	{
		___easeType_20 = value;
	}

	inline static int32_t get_offset_of_customEase_21() { return static_cast<int32_t>(offsetof(TweenParams_t4171191025, ___customEase_21)); }
	inline EaseFunction_t3531141372 * get_customEase_21() const { return ___customEase_21; }
	inline EaseFunction_t3531141372 ** get_address_of_customEase_21() { return &___customEase_21; }
	inline void set_customEase_21(EaseFunction_t3531141372 * value)
	{
		___customEase_21 = value;
		Il2CppCodeGenWriteBarrier((&___customEase_21), value);
	}

	inline static int32_t get_offset_of_easeOvershootOrAmplitude_22() { return static_cast<int32_t>(offsetof(TweenParams_t4171191025, ___easeOvershootOrAmplitude_22)); }
	inline float get_easeOvershootOrAmplitude_22() const { return ___easeOvershootOrAmplitude_22; }
	inline float* get_address_of_easeOvershootOrAmplitude_22() { return &___easeOvershootOrAmplitude_22; }
	inline void set_easeOvershootOrAmplitude_22(float value)
	{
		___easeOvershootOrAmplitude_22 = value;
	}

	inline static int32_t get_offset_of_easePeriod_23() { return static_cast<int32_t>(offsetof(TweenParams_t4171191025, ___easePeriod_23)); }
	inline float get_easePeriod_23() const { return ___easePeriod_23; }
	inline float* get_address_of_easePeriod_23() { return &___easePeriod_23; }
	inline void set_easePeriod_23(float value)
	{
		___easePeriod_23 = value;
	}
};

struct TweenParams_t4171191025_StaticFields
{
public:
	// DG.Tweening.TweenParams DG.Tweening.TweenParams::Params
	TweenParams_t4171191025 * ___Params_0;

public:
	inline static int32_t get_offset_of_Params_0() { return static_cast<int32_t>(offsetof(TweenParams_t4171191025_StaticFields, ___Params_0)); }
	inline TweenParams_t4171191025 * get_Params_0() const { return ___Params_0; }
	inline TweenParams_t4171191025 ** get_address_of_Params_0() { return &___Params_0; }
	inline void set_Params_0(TweenParams_t4171191025 * value)
	{
		___Params_0 = value;
		Il2CppCodeGenWriteBarrier((&___Params_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWEENPARAMS_T4171191025_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef SCRIPTABLEOBJECT_T2528358522_H
#define SCRIPTABLEOBJECT_T2528358522_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_t2528358522  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_pinvoke : public Object_t631007953_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_com : public Object_t631007953_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_T2528358522_H
#ifndef DOTWEENSETTINGS_T4078947542_H
#define DOTWEENSETTINGS_T4078947542_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.DOTweenSettings
struct  DOTweenSettings_t4078947542  : public ScriptableObject_t2528358522
{
public:
	// System.Boolean DG.Tweening.Core.DOTweenSettings::useSafeMode
	bool ___useSafeMode_5;
	// System.Single DG.Tweening.Core.DOTweenSettings::timeScale
	float ___timeScale_6;
	// System.Boolean DG.Tweening.Core.DOTweenSettings::useSmoothDeltaTime
	bool ___useSmoothDeltaTime_7;
	// System.Single DG.Tweening.Core.DOTweenSettings::maxSmoothUnscaledTime
	float ___maxSmoothUnscaledTime_8;
	// System.Boolean DG.Tweening.Core.DOTweenSettings::showUnityEditorReport
	bool ___showUnityEditorReport_9;
	// DG.Tweening.LogBehaviour DG.Tweening.Core.DOTweenSettings::logBehaviour
	int32_t ___logBehaviour_10;
	// System.Boolean DG.Tweening.Core.DOTweenSettings::drawGizmos
	bool ___drawGizmos_11;
	// System.Boolean DG.Tweening.Core.DOTweenSettings::defaultRecyclable
	bool ___defaultRecyclable_12;
	// DG.Tweening.AutoPlay DG.Tweening.Core.DOTweenSettings::defaultAutoPlay
	int32_t ___defaultAutoPlay_13;
	// DG.Tweening.UpdateType DG.Tweening.Core.DOTweenSettings::defaultUpdateType
	int32_t ___defaultUpdateType_14;
	// System.Boolean DG.Tweening.Core.DOTweenSettings::defaultTimeScaleIndependent
	bool ___defaultTimeScaleIndependent_15;
	// DG.Tweening.Ease DG.Tweening.Core.DOTweenSettings::defaultEaseType
	int32_t ___defaultEaseType_16;
	// System.Single DG.Tweening.Core.DOTweenSettings::defaultEaseOvershootOrAmplitude
	float ___defaultEaseOvershootOrAmplitude_17;
	// System.Single DG.Tweening.Core.DOTweenSettings::defaultEasePeriod
	float ___defaultEasePeriod_18;
	// System.Boolean DG.Tweening.Core.DOTweenSettings::defaultAutoKill
	bool ___defaultAutoKill_19;
	// DG.Tweening.LoopType DG.Tweening.Core.DOTweenSettings::defaultLoopType
	int32_t ___defaultLoopType_20;
	// DG.Tweening.Core.DOTweenSettings/SettingsLocation DG.Tweening.Core.DOTweenSettings::storeSettingsLocation
	int32_t ___storeSettingsLocation_21;

public:
	inline static int32_t get_offset_of_useSafeMode_5() { return static_cast<int32_t>(offsetof(DOTweenSettings_t4078947542, ___useSafeMode_5)); }
	inline bool get_useSafeMode_5() const { return ___useSafeMode_5; }
	inline bool* get_address_of_useSafeMode_5() { return &___useSafeMode_5; }
	inline void set_useSafeMode_5(bool value)
	{
		___useSafeMode_5 = value;
	}

	inline static int32_t get_offset_of_timeScale_6() { return static_cast<int32_t>(offsetof(DOTweenSettings_t4078947542, ___timeScale_6)); }
	inline float get_timeScale_6() const { return ___timeScale_6; }
	inline float* get_address_of_timeScale_6() { return &___timeScale_6; }
	inline void set_timeScale_6(float value)
	{
		___timeScale_6 = value;
	}

	inline static int32_t get_offset_of_useSmoothDeltaTime_7() { return static_cast<int32_t>(offsetof(DOTweenSettings_t4078947542, ___useSmoothDeltaTime_7)); }
	inline bool get_useSmoothDeltaTime_7() const { return ___useSmoothDeltaTime_7; }
	inline bool* get_address_of_useSmoothDeltaTime_7() { return &___useSmoothDeltaTime_7; }
	inline void set_useSmoothDeltaTime_7(bool value)
	{
		___useSmoothDeltaTime_7 = value;
	}

	inline static int32_t get_offset_of_maxSmoothUnscaledTime_8() { return static_cast<int32_t>(offsetof(DOTweenSettings_t4078947542, ___maxSmoothUnscaledTime_8)); }
	inline float get_maxSmoothUnscaledTime_8() const { return ___maxSmoothUnscaledTime_8; }
	inline float* get_address_of_maxSmoothUnscaledTime_8() { return &___maxSmoothUnscaledTime_8; }
	inline void set_maxSmoothUnscaledTime_8(float value)
	{
		___maxSmoothUnscaledTime_8 = value;
	}

	inline static int32_t get_offset_of_showUnityEditorReport_9() { return static_cast<int32_t>(offsetof(DOTweenSettings_t4078947542, ___showUnityEditorReport_9)); }
	inline bool get_showUnityEditorReport_9() const { return ___showUnityEditorReport_9; }
	inline bool* get_address_of_showUnityEditorReport_9() { return &___showUnityEditorReport_9; }
	inline void set_showUnityEditorReport_9(bool value)
	{
		___showUnityEditorReport_9 = value;
	}

	inline static int32_t get_offset_of_logBehaviour_10() { return static_cast<int32_t>(offsetof(DOTweenSettings_t4078947542, ___logBehaviour_10)); }
	inline int32_t get_logBehaviour_10() const { return ___logBehaviour_10; }
	inline int32_t* get_address_of_logBehaviour_10() { return &___logBehaviour_10; }
	inline void set_logBehaviour_10(int32_t value)
	{
		___logBehaviour_10 = value;
	}

	inline static int32_t get_offset_of_drawGizmos_11() { return static_cast<int32_t>(offsetof(DOTweenSettings_t4078947542, ___drawGizmos_11)); }
	inline bool get_drawGizmos_11() const { return ___drawGizmos_11; }
	inline bool* get_address_of_drawGizmos_11() { return &___drawGizmos_11; }
	inline void set_drawGizmos_11(bool value)
	{
		___drawGizmos_11 = value;
	}

	inline static int32_t get_offset_of_defaultRecyclable_12() { return static_cast<int32_t>(offsetof(DOTweenSettings_t4078947542, ___defaultRecyclable_12)); }
	inline bool get_defaultRecyclable_12() const { return ___defaultRecyclable_12; }
	inline bool* get_address_of_defaultRecyclable_12() { return &___defaultRecyclable_12; }
	inline void set_defaultRecyclable_12(bool value)
	{
		___defaultRecyclable_12 = value;
	}

	inline static int32_t get_offset_of_defaultAutoPlay_13() { return static_cast<int32_t>(offsetof(DOTweenSettings_t4078947542, ___defaultAutoPlay_13)); }
	inline int32_t get_defaultAutoPlay_13() const { return ___defaultAutoPlay_13; }
	inline int32_t* get_address_of_defaultAutoPlay_13() { return &___defaultAutoPlay_13; }
	inline void set_defaultAutoPlay_13(int32_t value)
	{
		___defaultAutoPlay_13 = value;
	}

	inline static int32_t get_offset_of_defaultUpdateType_14() { return static_cast<int32_t>(offsetof(DOTweenSettings_t4078947542, ___defaultUpdateType_14)); }
	inline int32_t get_defaultUpdateType_14() const { return ___defaultUpdateType_14; }
	inline int32_t* get_address_of_defaultUpdateType_14() { return &___defaultUpdateType_14; }
	inline void set_defaultUpdateType_14(int32_t value)
	{
		___defaultUpdateType_14 = value;
	}

	inline static int32_t get_offset_of_defaultTimeScaleIndependent_15() { return static_cast<int32_t>(offsetof(DOTweenSettings_t4078947542, ___defaultTimeScaleIndependent_15)); }
	inline bool get_defaultTimeScaleIndependent_15() const { return ___defaultTimeScaleIndependent_15; }
	inline bool* get_address_of_defaultTimeScaleIndependent_15() { return &___defaultTimeScaleIndependent_15; }
	inline void set_defaultTimeScaleIndependent_15(bool value)
	{
		___defaultTimeScaleIndependent_15 = value;
	}

	inline static int32_t get_offset_of_defaultEaseType_16() { return static_cast<int32_t>(offsetof(DOTweenSettings_t4078947542, ___defaultEaseType_16)); }
	inline int32_t get_defaultEaseType_16() const { return ___defaultEaseType_16; }
	inline int32_t* get_address_of_defaultEaseType_16() { return &___defaultEaseType_16; }
	inline void set_defaultEaseType_16(int32_t value)
	{
		___defaultEaseType_16 = value;
	}

	inline static int32_t get_offset_of_defaultEaseOvershootOrAmplitude_17() { return static_cast<int32_t>(offsetof(DOTweenSettings_t4078947542, ___defaultEaseOvershootOrAmplitude_17)); }
	inline float get_defaultEaseOvershootOrAmplitude_17() const { return ___defaultEaseOvershootOrAmplitude_17; }
	inline float* get_address_of_defaultEaseOvershootOrAmplitude_17() { return &___defaultEaseOvershootOrAmplitude_17; }
	inline void set_defaultEaseOvershootOrAmplitude_17(float value)
	{
		___defaultEaseOvershootOrAmplitude_17 = value;
	}

	inline static int32_t get_offset_of_defaultEasePeriod_18() { return static_cast<int32_t>(offsetof(DOTweenSettings_t4078947542, ___defaultEasePeriod_18)); }
	inline float get_defaultEasePeriod_18() const { return ___defaultEasePeriod_18; }
	inline float* get_address_of_defaultEasePeriod_18() { return &___defaultEasePeriod_18; }
	inline void set_defaultEasePeriod_18(float value)
	{
		___defaultEasePeriod_18 = value;
	}

	inline static int32_t get_offset_of_defaultAutoKill_19() { return static_cast<int32_t>(offsetof(DOTweenSettings_t4078947542, ___defaultAutoKill_19)); }
	inline bool get_defaultAutoKill_19() const { return ___defaultAutoKill_19; }
	inline bool* get_address_of_defaultAutoKill_19() { return &___defaultAutoKill_19; }
	inline void set_defaultAutoKill_19(bool value)
	{
		___defaultAutoKill_19 = value;
	}

	inline static int32_t get_offset_of_defaultLoopType_20() { return static_cast<int32_t>(offsetof(DOTweenSettings_t4078947542, ___defaultLoopType_20)); }
	inline int32_t get_defaultLoopType_20() const { return ___defaultLoopType_20; }
	inline int32_t* get_address_of_defaultLoopType_20() { return &___defaultLoopType_20; }
	inline void set_defaultLoopType_20(int32_t value)
	{
		___defaultLoopType_20 = value;
	}

	inline static int32_t get_offset_of_storeSettingsLocation_21() { return static_cast<int32_t>(offsetof(DOTweenSettings_t4078947542, ___storeSettingsLocation_21)); }
	inline int32_t get_storeSettingsLocation_21() const { return ___storeSettingsLocation_21; }
	inline int32_t* get_address_of_storeSettingsLocation_21() { return &___storeSettingsLocation_21; }
	inline void set_storeSettingsLocation_21(int32_t value)
	{
		___storeSettingsLocation_21 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOTWEENSETTINGS_T4078947542_H
#ifndef SEQUENCECALLBACK_T1025787276_H
#define SEQUENCECALLBACK_T1025787276_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.SequenceCallback
struct  SequenceCallback_t1025787276  : public ABSSequentiable_t3376041011
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SEQUENCECALLBACK_T1025787276_H
#ifndef TWEEN_T2342918553_H
#define TWEEN_T2342918553_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Tween
struct  Tween_t2342918553  : public ABSSequentiable_t3376041011
{
public:
	// System.Single DG.Tweening.Tween::timeScale
	float ___timeScale_4;
	// System.Boolean DG.Tweening.Tween::isBackwards
	bool ___isBackwards_5;
	// System.Object DG.Tweening.Tween::id
	RuntimeObject * ___id_6;
	// System.Object DG.Tweening.Tween::target
	RuntimeObject * ___target_7;
	// DG.Tweening.UpdateType DG.Tweening.Tween::updateType
	int32_t ___updateType_8;
	// System.Boolean DG.Tweening.Tween::isIndependentUpdate
	bool ___isIndependentUpdate_9;
	// DG.Tweening.TweenCallback DG.Tweening.Tween::onPlay
	TweenCallback_t3727756325 * ___onPlay_10;
	// DG.Tweening.TweenCallback DG.Tweening.Tween::onPause
	TweenCallback_t3727756325 * ___onPause_11;
	// DG.Tweening.TweenCallback DG.Tweening.Tween::onRewind
	TweenCallback_t3727756325 * ___onRewind_12;
	// DG.Tweening.TweenCallback DG.Tweening.Tween::onUpdate
	TweenCallback_t3727756325 * ___onUpdate_13;
	// DG.Tweening.TweenCallback DG.Tweening.Tween::onStepComplete
	TweenCallback_t3727756325 * ___onStepComplete_14;
	// DG.Tweening.TweenCallback DG.Tweening.Tween::onComplete
	TweenCallback_t3727756325 * ___onComplete_15;
	// DG.Tweening.TweenCallback DG.Tweening.Tween::onKill
	TweenCallback_t3727756325 * ___onKill_16;
	// DG.Tweening.TweenCallback`1<System.Int32> DG.Tweening.Tween::onWaypointChange
	TweenCallback_1_t3009965658 * ___onWaypointChange_17;
	// System.Boolean DG.Tweening.Tween::isFrom
	bool ___isFrom_18;
	// System.Boolean DG.Tweening.Tween::isBlendable
	bool ___isBlendable_19;
	// System.Boolean DG.Tweening.Tween::isRecyclable
	bool ___isRecyclable_20;
	// System.Boolean DG.Tweening.Tween::isSpeedBased
	bool ___isSpeedBased_21;
	// System.Boolean DG.Tweening.Tween::autoKill
	bool ___autoKill_22;
	// System.Single DG.Tweening.Tween::duration
	float ___duration_23;
	// System.Int32 DG.Tweening.Tween::loops
	int32_t ___loops_24;
	// DG.Tweening.LoopType DG.Tweening.Tween::loopType
	int32_t ___loopType_25;
	// System.Single DG.Tweening.Tween::delay
	float ___delay_26;
	// System.Boolean DG.Tweening.Tween::isRelative
	bool ___isRelative_27;
	// DG.Tweening.Ease DG.Tweening.Tween::easeType
	int32_t ___easeType_28;
	// DG.Tweening.EaseFunction DG.Tweening.Tween::customEase
	EaseFunction_t3531141372 * ___customEase_29;
	// System.Single DG.Tweening.Tween::easeOvershootOrAmplitude
	float ___easeOvershootOrAmplitude_30;
	// System.Single DG.Tweening.Tween::easePeriod
	float ___easePeriod_31;
	// System.Type DG.Tweening.Tween::typeofT1
	Type_t * ___typeofT1_32;
	// System.Type DG.Tweening.Tween::typeofT2
	Type_t * ___typeofT2_33;
	// System.Type DG.Tweening.Tween::typeofTPlugOptions
	Type_t * ___typeofTPlugOptions_34;
	// System.Boolean DG.Tweening.Tween::active
	bool ___active_35;
	// System.Boolean DG.Tweening.Tween::isSequenced
	bool ___isSequenced_36;
	// DG.Tweening.Sequence DG.Tweening.Tween::sequenceParent
	Sequence_t2050373119 * ___sequenceParent_37;
	// System.Int32 DG.Tweening.Tween::activeId
	int32_t ___activeId_38;
	// DG.Tweening.Core.Enums.SpecialStartupMode DG.Tweening.Tween::specialStartupMode
	int32_t ___specialStartupMode_39;
	// System.Boolean DG.Tweening.Tween::creationLocked
	bool ___creationLocked_40;
	// System.Boolean DG.Tweening.Tween::startupDone
	bool ___startupDone_41;
	// System.Boolean DG.Tweening.Tween::playedOnce
	bool ___playedOnce_42;
	// System.Single DG.Tweening.Tween::position
	float ___position_43;
	// System.Single DG.Tweening.Tween::fullDuration
	float ___fullDuration_44;
	// System.Int32 DG.Tweening.Tween::completedLoops
	int32_t ___completedLoops_45;
	// System.Boolean DG.Tweening.Tween::isPlaying
	bool ___isPlaying_46;
	// System.Boolean DG.Tweening.Tween::isComplete
	bool ___isComplete_47;
	// System.Single DG.Tweening.Tween::elapsedDelay
	float ___elapsedDelay_48;
	// System.Boolean DG.Tweening.Tween::delayComplete
	bool ___delayComplete_49;
	// System.Int32 DG.Tweening.Tween::miscInt
	int32_t ___miscInt_50;

public:
	inline static int32_t get_offset_of_timeScale_4() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___timeScale_4)); }
	inline float get_timeScale_4() const { return ___timeScale_4; }
	inline float* get_address_of_timeScale_4() { return &___timeScale_4; }
	inline void set_timeScale_4(float value)
	{
		___timeScale_4 = value;
	}

	inline static int32_t get_offset_of_isBackwards_5() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___isBackwards_5)); }
	inline bool get_isBackwards_5() const { return ___isBackwards_5; }
	inline bool* get_address_of_isBackwards_5() { return &___isBackwards_5; }
	inline void set_isBackwards_5(bool value)
	{
		___isBackwards_5 = value;
	}

	inline static int32_t get_offset_of_id_6() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___id_6)); }
	inline RuntimeObject * get_id_6() const { return ___id_6; }
	inline RuntimeObject ** get_address_of_id_6() { return &___id_6; }
	inline void set_id_6(RuntimeObject * value)
	{
		___id_6 = value;
		Il2CppCodeGenWriteBarrier((&___id_6), value);
	}

	inline static int32_t get_offset_of_target_7() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___target_7)); }
	inline RuntimeObject * get_target_7() const { return ___target_7; }
	inline RuntimeObject ** get_address_of_target_7() { return &___target_7; }
	inline void set_target_7(RuntimeObject * value)
	{
		___target_7 = value;
		Il2CppCodeGenWriteBarrier((&___target_7), value);
	}

	inline static int32_t get_offset_of_updateType_8() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___updateType_8)); }
	inline int32_t get_updateType_8() const { return ___updateType_8; }
	inline int32_t* get_address_of_updateType_8() { return &___updateType_8; }
	inline void set_updateType_8(int32_t value)
	{
		___updateType_8 = value;
	}

	inline static int32_t get_offset_of_isIndependentUpdate_9() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___isIndependentUpdate_9)); }
	inline bool get_isIndependentUpdate_9() const { return ___isIndependentUpdate_9; }
	inline bool* get_address_of_isIndependentUpdate_9() { return &___isIndependentUpdate_9; }
	inline void set_isIndependentUpdate_9(bool value)
	{
		___isIndependentUpdate_9 = value;
	}

	inline static int32_t get_offset_of_onPlay_10() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___onPlay_10)); }
	inline TweenCallback_t3727756325 * get_onPlay_10() const { return ___onPlay_10; }
	inline TweenCallback_t3727756325 ** get_address_of_onPlay_10() { return &___onPlay_10; }
	inline void set_onPlay_10(TweenCallback_t3727756325 * value)
	{
		___onPlay_10 = value;
		Il2CppCodeGenWriteBarrier((&___onPlay_10), value);
	}

	inline static int32_t get_offset_of_onPause_11() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___onPause_11)); }
	inline TweenCallback_t3727756325 * get_onPause_11() const { return ___onPause_11; }
	inline TweenCallback_t3727756325 ** get_address_of_onPause_11() { return &___onPause_11; }
	inline void set_onPause_11(TweenCallback_t3727756325 * value)
	{
		___onPause_11 = value;
		Il2CppCodeGenWriteBarrier((&___onPause_11), value);
	}

	inline static int32_t get_offset_of_onRewind_12() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___onRewind_12)); }
	inline TweenCallback_t3727756325 * get_onRewind_12() const { return ___onRewind_12; }
	inline TweenCallback_t3727756325 ** get_address_of_onRewind_12() { return &___onRewind_12; }
	inline void set_onRewind_12(TweenCallback_t3727756325 * value)
	{
		___onRewind_12 = value;
		Il2CppCodeGenWriteBarrier((&___onRewind_12), value);
	}

	inline static int32_t get_offset_of_onUpdate_13() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___onUpdate_13)); }
	inline TweenCallback_t3727756325 * get_onUpdate_13() const { return ___onUpdate_13; }
	inline TweenCallback_t3727756325 ** get_address_of_onUpdate_13() { return &___onUpdate_13; }
	inline void set_onUpdate_13(TweenCallback_t3727756325 * value)
	{
		___onUpdate_13 = value;
		Il2CppCodeGenWriteBarrier((&___onUpdate_13), value);
	}

	inline static int32_t get_offset_of_onStepComplete_14() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___onStepComplete_14)); }
	inline TweenCallback_t3727756325 * get_onStepComplete_14() const { return ___onStepComplete_14; }
	inline TweenCallback_t3727756325 ** get_address_of_onStepComplete_14() { return &___onStepComplete_14; }
	inline void set_onStepComplete_14(TweenCallback_t3727756325 * value)
	{
		___onStepComplete_14 = value;
		Il2CppCodeGenWriteBarrier((&___onStepComplete_14), value);
	}

	inline static int32_t get_offset_of_onComplete_15() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___onComplete_15)); }
	inline TweenCallback_t3727756325 * get_onComplete_15() const { return ___onComplete_15; }
	inline TweenCallback_t3727756325 ** get_address_of_onComplete_15() { return &___onComplete_15; }
	inline void set_onComplete_15(TweenCallback_t3727756325 * value)
	{
		___onComplete_15 = value;
		Il2CppCodeGenWriteBarrier((&___onComplete_15), value);
	}

	inline static int32_t get_offset_of_onKill_16() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___onKill_16)); }
	inline TweenCallback_t3727756325 * get_onKill_16() const { return ___onKill_16; }
	inline TweenCallback_t3727756325 ** get_address_of_onKill_16() { return &___onKill_16; }
	inline void set_onKill_16(TweenCallback_t3727756325 * value)
	{
		___onKill_16 = value;
		Il2CppCodeGenWriteBarrier((&___onKill_16), value);
	}

	inline static int32_t get_offset_of_onWaypointChange_17() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___onWaypointChange_17)); }
	inline TweenCallback_1_t3009965658 * get_onWaypointChange_17() const { return ___onWaypointChange_17; }
	inline TweenCallback_1_t3009965658 ** get_address_of_onWaypointChange_17() { return &___onWaypointChange_17; }
	inline void set_onWaypointChange_17(TweenCallback_1_t3009965658 * value)
	{
		___onWaypointChange_17 = value;
		Il2CppCodeGenWriteBarrier((&___onWaypointChange_17), value);
	}

	inline static int32_t get_offset_of_isFrom_18() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___isFrom_18)); }
	inline bool get_isFrom_18() const { return ___isFrom_18; }
	inline bool* get_address_of_isFrom_18() { return &___isFrom_18; }
	inline void set_isFrom_18(bool value)
	{
		___isFrom_18 = value;
	}

	inline static int32_t get_offset_of_isBlendable_19() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___isBlendable_19)); }
	inline bool get_isBlendable_19() const { return ___isBlendable_19; }
	inline bool* get_address_of_isBlendable_19() { return &___isBlendable_19; }
	inline void set_isBlendable_19(bool value)
	{
		___isBlendable_19 = value;
	}

	inline static int32_t get_offset_of_isRecyclable_20() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___isRecyclable_20)); }
	inline bool get_isRecyclable_20() const { return ___isRecyclable_20; }
	inline bool* get_address_of_isRecyclable_20() { return &___isRecyclable_20; }
	inline void set_isRecyclable_20(bool value)
	{
		___isRecyclable_20 = value;
	}

	inline static int32_t get_offset_of_isSpeedBased_21() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___isSpeedBased_21)); }
	inline bool get_isSpeedBased_21() const { return ___isSpeedBased_21; }
	inline bool* get_address_of_isSpeedBased_21() { return &___isSpeedBased_21; }
	inline void set_isSpeedBased_21(bool value)
	{
		___isSpeedBased_21 = value;
	}

	inline static int32_t get_offset_of_autoKill_22() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___autoKill_22)); }
	inline bool get_autoKill_22() const { return ___autoKill_22; }
	inline bool* get_address_of_autoKill_22() { return &___autoKill_22; }
	inline void set_autoKill_22(bool value)
	{
		___autoKill_22 = value;
	}

	inline static int32_t get_offset_of_duration_23() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___duration_23)); }
	inline float get_duration_23() const { return ___duration_23; }
	inline float* get_address_of_duration_23() { return &___duration_23; }
	inline void set_duration_23(float value)
	{
		___duration_23 = value;
	}

	inline static int32_t get_offset_of_loops_24() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___loops_24)); }
	inline int32_t get_loops_24() const { return ___loops_24; }
	inline int32_t* get_address_of_loops_24() { return &___loops_24; }
	inline void set_loops_24(int32_t value)
	{
		___loops_24 = value;
	}

	inline static int32_t get_offset_of_loopType_25() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___loopType_25)); }
	inline int32_t get_loopType_25() const { return ___loopType_25; }
	inline int32_t* get_address_of_loopType_25() { return &___loopType_25; }
	inline void set_loopType_25(int32_t value)
	{
		___loopType_25 = value;
	}

	inline static int32_t get_offset_of_delay_26() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___delay_26)); }
	inline float get_delay_26() const { return ___delay_26; }
	inline float* get_address_of_delay_26() { return &___delay_26; }
	inline void set_delay_26(float value)
	{
		___delay_26 = value;
	}

	inline static int32_t get_offset_of_isRelative_27() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___isRelative_27)); }
	inline bool get_isRelative_27() const { return ___isRelative_27; }
	inline bool* get_address_of_isRelative_27() { return &___isRelative_27; }
	inline void set_isRelative_27(bool value)
	{
		___isRelative_27 = value;
	}

	inline static int32_t get_offset_of_easeType_28() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___easeType_28)); }
	inline int32_t get_easeType_28() const { return ___easeType_28; }
	inline int32_t* get_address_of_easeType_28() { return &___easeType_28; }
	inline void set_easeType_28(int32_t value)
	{
		___easeType_28 = value;
	}

	inline static int32_t get_offset_of_customEase_29() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___customEase_29)); }
	inline EaseFunction_t3531141372 * get_customEase_29() const { return ___customEase_29; }
	inline EaseFunction_t3531141372 ** get_address_of_customEase_29() { return &___customEase_29; }
	inline void set_customEase_29(EaseFunction_t3531141372 * value)
	{
		___customEase_29 = value;
		Il2CppCodeGenWriteBarrier((&___customEase_29), value);
	}

	inline static int32_t get_offset_of_easeOvershootOrAmplitude_30() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___easeOvershootOrAmplitude_30)); }
	inline float get_easeOvershootOrAmplitude_30() const { return ___easeOvershootOrAmplitude_30; }
	inline float* get_address_of_easeOvershootOrAmplitude_30() { return &___easeOvershootOrAmplitude_30; }
	inline void set_easeOvershootOrAmplitude_30(float value)
	{
		___easeOvershootOrAmplitude_30 = value;
	}

	inline static int32_t get_offset_of_easePeriod_31() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___easePeriod_31)); }
	inline float get_easePeriod_31() const { return ___easePeriod_31; }
	inline float* get_address_of_easePeriod_31() { return &___easePeriod_31; }
	inline void set_easePeriod_31(float value)
	{
		___easePeriod_31 = value;
	}

	inline static int32_t get_offset_of_typeofT1_32() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___typeofT1_32)); }
	inline Type_t * get_typeofT1_32() const { return ___typeofT1_32; }
	inline Type_t ** get_address_of_typeofT1_32() { return &___typeofT1_32; }
	inline void set_typeofT1_32(Type_t * value)
	{
		___typeofT1_32 = value;
		Il2CppCodeGenWriteBarrier((&___typeofT1_32), value);
	}

	inline static int32_t get_offset_of_typeofT2_33() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___typeofT2_33)); }
	inline Type_t * get_typeofT2_33() const { return ___typeofT2_33; }
	inline Type_t ** get_address_of_typeofT2_33() { return &___typeofT2_33; }
	inline void set_typeofT2_33(Type_t * value)
	{
		___typeofT2_33 = value;
		Il2CppCodeGenWriteBarrier((&___typeofT2_33), value);
	}

	inline static int32_t get_offset_of_typeofTPlugOptions_34() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___typeofTPlugOptions_34)); }
	inline Type_t * get_typeofTPlugOptions_34() const { return ___typeofTPlugOptions_34; }
	inline Type_t ** get_address_of_typeofTPlugOptions_34() { return &___typeofTPlugOptions_34; }
	inline void set_typeofTPlugOptions_34(Type_t * value)
	{
		___typeofTPlugOptions_34 = value;
		Il2CppCodeGenWriteBarrier((&___typeofTPlugOptions_34), value);
	}

	inline static int32_t get_offset_of_active_35() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___active_35)); }
	inline bool get_active_35() const { return ___active_35; }
	inline bool* get_address_of_active_35() { return &___active_35; }
	inline void set_active_35(bool value)
	{
		___active_35 = value;
	}

	inline static int32_t get_offset_of_isSequenced_36() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___isSequenced_36)); }
	inline bool get_isSequenced_36() const { return ___isSequenced_36; }
	inline bool* get_address_of_isSequenced_36() { return &___isSequenced_36; }
	inline void set_isSequenced_36(bool value)
	{
		___isSequenced_36 = value;
	}

	inline static int32_t get_offset_of_sequenceParent_37() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___sequenceParent_37)); }
	inline Sequence_t2050373119 * get_sequenceParent_37() const { return ___sequenceParent_37; }
	inline Sequence_t2050373119 ** get_address_of_sequenceParent_37() { return &___sequenceParent_37; }
	inline void set_sequenceParent_37(Sequence_t2050373119 * value)
	{
		___sequenceParent_37 = value;
		Il2CppCodeGenWriteBarrier((&___sequenceParent_37), value);
	}

	inline static int32_t get_offset_of_activeId_38() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___activeId_38)); }
	inline int32_t get_activeId_38() const { return ___activeId_38; }
	inline int32_t* get_address_of_activeId_38() { return &___activeId_38; }
	inline void set_activeId_38(int32_t value)
	{
		___activeId_38 = value;
	}

	inline static int32_t get_offset_of_specialStartupMode_39() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___specialStartupMode_39)); }
	inline int32_t get_specialStartupMode_39() const { return ___specialStartupMode_39; }
	inline int32_t* get_address_of_specialStartupMode_39() { return &___specialStartupMode_39; }
	inline void set_specialStartupMode_39(int32_t value)
	{
		___specialStartupMode_39 = value;
	}

	inline static int32_t get_offset_of_creationLocked_40() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___creationLocked_40)); }
	inline bool get_creationLocked_40() const { return ___creationLocked_40; }
	inline bool* get_address_of_creationLocked_40() { return &___creationLocked_40; }
	inline void set_creationLocked_40(bool value)
	{
		___creationLocked_40 = value;
	}

	inline static int32_t get_offset_of_startupDone_41() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___startupDone_41)); }
	inline bool get_startupDone_41() const { return ___startupDone_41; }
	inline bool* get_address_of_startupDone_41() { return &___startupDone_41; }
	inline void set_startupDone_41(bool value)
	{
		___startupDone_41 = value;
	}

	inline static int32_t get_offset_of_playedOnce_42() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___playedOnce_42)); }
	inline bool get_playedOnce_42() const { return ___playedOnce_42; }
	inline bool* get_address_of_playedOnce_42() { return &___playedOnce_42; }
	inline void set_playedOnce_42(bool value)
	{
		___playedOnce_42 = value;
	}

	inline static int32_t get_offset_of_position_43() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___position_43)); }
	inline float get_position_43() const { return ___position_43; }
	inline float* get_address_of_position_43() { return &___position_43; }
	inline void set_position_43(float value)
	{
		___position_43 = value;
	}

	inline static int32_t get_offset_of_fullDuration_44() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___fullDuration_44)); }
	inline float get_fullDuration_44() const { return ___fullDuration_44; }
	inline float* get_address_of_fullDuration_44() { return &___fullDuration_44; }
	inline void set_fullDuration_44(float value)
	{
		___fullDuration_44 = value;
	}

	inline static int32_t get_offset_of_completedLoops_45() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___completedLoops_45)); }
	inline int32_t get_completedLoops_45() const { return ___completedLoops_45; }
	inline int32_t* get_address_of_completedLoops_45() { return &___completedLoops_45; }
	inline void set_completedLoops_45(int32_t value)
	{
		___completedLoops_45 = value;
	}

	inline static int32_t get_offset_of_isPlaying_46() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___isPlaying_46)); }
	inline bool get_isPlaying_46() const { return ___isPlaying_46; }
	inline bool* get_address_of_isPlaying_46() { return &___isPlaying_46; }
	inline void set_isPlaying_46(bool value)
	{
		___isPlaying_46 = value;
	}

	inline static int32_t get_offset_of_isComplete_47() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___isComplete_47)); }
	inline bool get_isComplete_47() const { return ___isComplete_47; }
	inline bool* get_address_of_isComplete_47() { return &___isComplete_47; }
	inline void set_isComplete_47(bool value)
	{
		___isComplete_47 = value;
	}

	inline static int32_t get_offset_of_elapsedDelay_48() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___elapsedDelay_48)); }
	inline float get_elapsedDelay_48() const { return ___elapsedDelay_48; }
	inline float* get_address_of_elapsedDelay_48() { return &___elapsedDelay_48; }
	inline void set_elapsedDelay_48(float value)
	{
		___elapsedDelay_48 = value;
	}

	inline static int32_t get_offset_of_delayComplete_49() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___delayComplete_49)); }
	inline bool get_delayComplete_49() const { return ___delayComplete_49; }
	inline bool* get_address_of_delayComplete_49() { return &___delayComplete_49; }
	inline void set_delayComplete_49(bool value)
	{
		___delayComplete_49 = value;
	}

	inline static int32_t get_offset_of_miscInt_50() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___miscInt_50)); }
	inline int32_t get_miscInt_50() const { return ___miscInt_50; }
	inline int32_t* get_address_of_miscInt_50() { return &___miscInt_50; }
	inline void set_miscInt_50(int32_t value)
	{
		___miscInt_50 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWEEN_T2342918553_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef TWEENER_T436044680_H
#define TWEENER_T436044680_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Tweener
struct  Tweener_t436044680  : public Tween_t2342918553
{
public:
	// System.Boolean DG.Tweening.Tweener::hasManuallySetStartValue
	bool ___hasManuallySetStartValue_51;
	// System.Boolean DG.Tweening.Tweener::isFromAllowed
	bool ___isFromAllowed_52;

public:
	inline static int32_t get_offset_of_hasManuallySetStartValue_51() { return static_cast<int32_t>(offsetof(Tweener_t436044680, ___hasManuallySetStartValue_51)); }
	inline bool get_hasManuallySetStartValue_51() const { return ___hasManuallySetStartValue_51; }
	inline bool* get_address_of_hasManuallySetStartValue_51() { return &___hasManuallySetStartValue_51; }
	inline void set_hasManuallySetStartValue_51(bool value)
	{
		___hasManuallySetStartValue_51 = value;
	}

	inline static int32_t get_offset_of_isFromAllowed_52() { return static_cast<int32_t>(offsetof(Tweener_t436044680, ___isFromAllowed_52)); }
	inline bool get_isFromAllowed_52() const { return ___isFromAllowed_52; }
	inline bool* get_address_of_isFromAllowed_52() { return &___isFromAllowed_52; }
	inline void set_isFromAllowed_52(bool value)
	{
		___isFromAllowed_52 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWEENER_T436044680_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef DOTWEENCOMPONENT_T828035757_H
#define DOTWEENCOMPONENT_T828035757_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.DOTweenComponent
struct  DOTweenComponent_t828035757  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 DG.Tweening.Core.DOTweenComponent::inspectorUpdater
	int32_t ___inspectorUpdater_4;
	// System.Single DG.Tweening.Core.DOTweenComponent::_unscaledTime
	float ____unscaledTime_5;
	// System.Single DG.Tweening.Core.DOTweenComponent::_unscaledDeltaTime
	float ____unscaledDeltaTime_6;
	// System.Boolean DG.Tweening.Core.DOTweenComponent::_duplicateToDestroy
	bool ____duplicateToDestroy_7;

public:
	inline static int32_t get_offset_of_inspectorUpdater_4() { return static_cast<int32_t>(offsetof(DOTweenComponent_t828035757, ___inspectorUpdater_4)); }
	inline int32_t get_inspectorUpdater_4() const { return ___inspectorUpdater_4; }
	inline int32_t* get_address_of_inspectorUpdater_4() { return &___inspectorUpdater_4; }
	inline void set_inspectorUpdater_4(int32_t value)
	{
		___inspectorUpdater_4 = value;
	}

	inline static int32_t get_offset_of__unscaledTime_5() { return static_cast<int32_t>(offsetof(DOTweenComponent_t828035757, ____unscaledTime_5)); }
	inline float get__unscaledTime_5() const { return ____unscaledTime_5; }
	inline float* get_address_of__unscaledTime_5() { return &____unscaledTime_5; }
	inline void set__unscaledTime_5(float value)
	{
		____unscaledTime_5 = value;
	}

	inline static int32_t get_offset_of__unscaledDeltaTime_6() { return static_cast<int32_t>(offsetof(DOTweenComponent_t828035757, ____unscaledDeltaTime_6)); }
	inline float get__unscaledDeltaTime_6() const { return ____unscaledDeltaTime_6; }
	inline float* get_address_of__unscaledDeltaTime_6() { return &____unscaledDeltaTime_6; }
	inline void set__unscaledDeltaTime_6(float value)
	{
		____unscaledDeltaTime_6 = value;
	}

	inline static int32_t get_offset_of__duplicateToDestroy_7() { return static_cast<int32_t>(offsetof(DOTweenComponent_t828035757, ____duplicateToDestroy_7)); }
	inline bool get__duplicateToDestroy_7() const { return ____duplicateToDestroy_7; }
	inline bool* get_address_of__duplicateToDestroy_7() { return &____duplicateToDestroy_7; }
	inline void set__duplicateToDestroy_7(bool value)
	{
		____duplicateToDestroy_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOTWEENCOMPONENT_T828035757_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2300 = { sizeof (U3CU3Ec__DisplayClass57_0_t2368928457), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2300[1] = 
{
	U3CU3Ec__DisplayClass57_0_t2368928457::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2301 = { sizeof (U3CU3Ec__DisplayClass58_0_t2368207561), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2301[1] = 
{
	U3CU3Ec__DisplayClass58_0_t2368207561::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2302 = { sizeof (U3CU3Ec__DisplayClass59_0_t2368273097), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2302[1] = 
{
	U3CU3Ec__DisplayClass59_0_t2368273097::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2303 = { sizeof (U3CU3Ec__DisplayClass60_0_t2368731850), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2303[1] = 
{
	U3CU3Ec__DisplayClass60_0_t2368731850::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2304 = { sizeof (U3CU3Ec__DisplayClass61_0_t2368797386), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2304[1] = 
{
	U3CU3Ec__DisplayClass61_0_t2368797386::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2305 = { sizeof (U3CU3Ec__DisplayClass62_0_t2368600778), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2305[1] = 
{
	U3CU3Ec__DisplayClass62_0_t2368600778::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2306 = { sizeof (U3CU3Ec__DisplayClass63_0_t2368666314), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2306[1] = 
{
	U3CU3Ec__DisplayClass63_0_t2368666314::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2307 = { sizeof (U3CU3Ec__DisplayClass64_0_t2368993994), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2307[1] = 
{
	U3CU3Ec__DisplayClass64_0_t2368993994::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2308 = { sizeof (U3CU3Ec__DisplayClass65_0_t2369059530), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2308[1] = 
{
	U3CU3Ec__DisplayClass65_0_t2369059530::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2309 = { sizeof (U3CU3Ec__DisplayClass66_0_t2368862922), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2309[1] = 
{
	U3CU3Ec__DisplayClass66_0_t2368862922::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2310 = { sizeof (U3CU3Ec__DisplayClass67_0_t2368928458), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2310[1] = 
{
	U3CU3Ec__DisplayClass67_0_t2368928458::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2311 = { sizeof (U3CU3Ec__DisplayClass68_0_t2368207562), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2311[6] = 
{
	U3CU3Ec__DisplayClass68_0_t2368207562::get_offset_of_target_0(),
	U3CU3Ec__DisplayClass68_0_t2368207562::get_offset_of_offsetYSet_1(),
	U3CU3Ec__DisplayClass68_0_t2368207562::get_offset_of_offsetY_2(),
	U3CU3Ec__DisplayClass68_0_t2368207562::get_offset_of_s_3(),
	U3CU3Ec__DisplayClass68_0_t2368207562::get_offset_of_endValue_4(),
	U3CU3Ec__DisplayClass68_0_t2368207562::get_offset_of_startPosY_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2312 = { sizeof (U3CU3Ec__DisplayClass69_0_t2368273098), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2312[6] = 
{
	U3CU3Ec__DisplayClass69_0_t2368273098::get_offset_of_target_0(),
	U3CU3Ec__DisplayClass69_0_t2368273098::get_offset_of_offsetYSet_1(),
	U3CU3Ec__DisplayClass69_0_t2368273098::get_offset_of_offsetY_2(),
	U3CU3Ec__DisplayClass69_0_t2368273098::get_offset_of_s_3(),
	U3CU3Ec__DisplayClass69_0_t2368273098::get_offset_of_endValue_4(),
	U3CU3Ec__DisplayClass69_0_t2368273098::get_offset_of_startPosY_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2313 = { sizeof (U3CU3Ec__DisplayClass70_0_t2368731851), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2313[1] = 
{
	U3CU3Ec__DisplayClass70_0_t2368731851::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2314 = { sizeof (U3CU3Ec__DisplayClass71_0_t2368797387), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2314[1] = 
{
	U3CU3Ec__DisplayClass71_0_t2368797387::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2315 = { sizeof (U3CU3Ec__DisplayClass72_0_t2368600779), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2315[1] = 
{
	U3CU3Ec__DisplayClass72_0_t2368600779::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2316 = { sizeof (U3CU3Ec__DisplayClass73_0_t2368666315), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2316[1] = 
{
	U3CU3Ec__DisplayClass73_0_t2368666315::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2317 = { sizeof (U3CU3Ec__DisplayClass74_0_t2368993995), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2317[1] = 
{
	U3CU3Ec__DisplayClass74_0_t2368993995::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2318 = { sizeof (U3CU3Ec__DisplayClass75_0_t2369059531), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2318[2] = 
{
	U3CU3Ec__DisplayClass75_0_t2369059531::get_offset_of_to_0(),
	U3CU3Ec__DisplayClass75_0_t2369059531::get_offset_of_target_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2319 = { sizeof (U3CU3Ec__DisplayClass76_0_t2368862923), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2319[2] = 
{
	U3CU3Ec__DisplayClass76_0_t2368862923::get_offset_of_to_0(),
	U3CU3Ec__DisplayClass76_0_t2368862923::get_offset_of_target_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2320 = { sizeof (U3CU3Ec__DisplayClass77_0_t2368928459), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2320[3] = 
{
	U3CU3Ec__DisplayClass77_0_t2368928459::get_offset_of_to_0(),
	U3CU3Ec__DisplayClass77_0_t2368928459::get_offset_of_target_1(),
	U3CU3Ec__DisplayClass77_0_t2368928459::get_offset_of_property_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2321 = { sizeof (U3CU3Ec__DisplayClass78_0_t2368207563), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2321[2] = 
{
	U3CU3Ec__DisplayClass78_0_t2368207563::get_offset_of_to_0(),
	U3CU3Ec__DisplayClass78_0_t2368207563::get_offset_of_target_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2322 = { sizeof (U3CU3Ec__DisplayClass79_0_t2368273099), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2322[2] = 
{
	U3CU3Ec__DisplayClass79_0_t2368273099::get_offset_of_to_0(),
	U3CU3Ec__DisplayClass79_0_t2368273099::get_offset_of_target_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2323 = { sizeof (U3CU3Ec__DisplayClass80_0_t2368731860), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2323[2] = 
{
	U3CU3Ec__DisplayClass80_0_t2368731860::get_offset_of_to_0(),
	U3CU3Ec__DisplayClass80_0_t2368731860::get_offset_of_target_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2324 = { sizeof (U3CU3Ec__DisplayClass81_0_t2368797396), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2324[2] = 
{
	U3CU3Ec__DisplayClass81_0_t2368797396::get_offset_of_to_0(),
	U3CU3Ec__DisplayClass81_0_t2368797396::get_offset_of_target_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2325 = { sizeof (U3CU3Ec__DisplayClass82_0_t2368600788), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2325[2] = 
{
	U3CU3Ec__DisplayClass82_0_t2368600788::get_offset_of_to_0(),
	U3CU3Ec__DisplayClass82_0_t2368600788::get_offset_of_target_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2326 = { sizeof (TweenParams_t4171191025), -1, sizeof(TweenParams_t4171191025_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2326[24] = 
{
	TweenParams_t4171191025_StaticFields::get_offset_of_Params_0(),
	TweenParams_t4171191025::get_offset_of_id_1(),
	TweenParams_t4171191025::get_offset_of_target_2(),
	TweenParams_t4171191025::get_offset_of_updateType_3(),
	TweenParams_t4171191025::get_offset_of_isIndependentUpdate_4(),
	TweenParams_t4171191025::get_offset_of_onStart_5(),
	TweenParams_t4171191025::get_offset_of_onPlay_6(),
	TweenParams_t4171191025::get_offset_of_onRewind_7(),
	TweenParams_t4171191025::get_offset_of_onUpdate_8(),
	TweenParams_t4171191025::get_offset_of_onStepComplete_9(),
	TweenParams_t4171191025::get_offset_of_onComplete_10(),
	TweenParams_t4171191025::get_offset_of_onKill_11(),
	TweenParams_t4171191025::get_offset_of_onWaypointChange_12(),
	TweenParams_t4171191025::get_offset_of_isRecyclable_13(),
	TweenParams_t4171191025::get_offset_of_isSpeedBased_14(),
	TweenParams_t4171191025::get_offset_of_autoKill_15(),
	TweenParams_t4171191025::get_offset_of_loops_16(),
	TweenParams_t4171191025::get_offset_of_loopType_17(),
	TweenParams_t4171191025::get_offset_of_delay_18(),
	TweenParams_t4171191025::get_offset_of_isRelative_19(),
	TweenParams_t4171191025::get_offset_of_easeType_20(),
	TweenParams_t4171191025::get_offset_of_customEase_21(),
	TweenParams_t4171191025::get_offset_of_easeOvershootOrAmplitude_22(),
	TweenParams_t4171191025::get_offset_of_easePeriod_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2327 = { sizeof (TweenSettingsExtensions_t101259202), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2328 = { sizeof (LogBehaviour_t1548882435)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2328[4] = 
{
	LogBehaviour_t1548882435::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2329 = { sizeof (Tween_t2342918553), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2329[47] = 
{
	Tween_t2342918553::get_offset_of_timeScale_4(),
	Tween_t2342918553::get_offset_of_isBackwards_5(),
	Tween_t2342918553::get_offset_of_id_6(),
	Tween_t2342918553::get_offset_of_target_7(),
	Tween_t2342918553::get_offset_of_updateType_8(),
	Tween_t2342918553::get_offset_of_isIndependentUpdate_9(),
	Tween_t2342918553::get_offset_of_onPlay_10(),
	Tween_t2342918553::get_offset_of_onPause_11(),
	Tween_t2342918553::get_offset_of_onRewind_12(),
	Tween_t2342918553::get_offset_of_onUpdate_13(),
	Tween_t2342918553::get_offset_of_onStepComplete_14(),
	Tween_t2342918553::get_offset_of_onComplete_15(),
	Tween_t2342918553::get_offset_of_onKill_16(),
	Tween_t2342918553::get_offset_of_onWaypointChange_17(),
	Tween_t2342918553::get_offset_of_isFrom_18(),
	Tween_t2342918553::get_offset_of_isBlendable_19(),
	Tween_t2342918553::get_offset_of_isRecyclable_20(),
	Tween_t2342918553::get_offset_of_isSpeedBased_21(),
	Tween_t2342918553::get_offset_of_autoKill_22(),
	Tween_t2342918553::get_offset_of_duration_23(),
	Tween_t2342918553::get_offset_of_loops_24(),
	Tween_t2342918553::get_offset_of_loopType_25(),
	Tween_t2342918553::get_offset_of_delay_26(),
	Tween_t2342918553::get_offset_of_isRelative_27(),
	Tween_t2342918553::get_offset_of_easeType_28(),
	Tween_t2342918553::get_offset_of_customEase_29(),
	Tween_t2342918553::get_offset_of_easeOvershootOrAmplitude_30(),
	Tween_t2342918553::get_offset_of_easePeriod_31(),
	Tween_t2342918553::get_offset_of_typeofT1_32(),
	Tween_t2342918553::get_offset_of_typeofT2_33(),
	Tween_t2342918553::get_offset_of_typeofTPlugOptions_34(),
	Tween_t2342918553::get_offset_of_active_35(),
	Tween_t2342918553::get_offset_of_isSequenced_36(),
	Tween_t2342918553::get_offset_of_sequenceParent_37(),
	Tween_t2342918553::get_offset_of_activeId_38(),
	Tween_t2342918553::get_offset_of_specialStartupMode_39(),
	Tween_t2342918553::get_offset_of_creationLocked_40(),
	Tween_t2342918553::get_offset_of_startupDone_41(),
	Tween_t2342918553::get_offset_of_playedOnce_42(),
	Tween_t2342918553::get_offset_of_position_43(),
	Tween_t2342918553::get_offset_of_fullDuration_44(),
	Tween_t2342918553::get_offset_of_completedLoops_45(),
	Tween_t2342918553::get_offset_of_isPlaying_46(),
	Tween_t2342918553::get_offset_of_isComplete_47(),
	Tween_t2342918553::get_offset_of_elapsedDelay_48(),
	Tween_t2342918553::get_offset_of_delayComplete_49(),
	Tween_t2342918553::get_offset_of_miscInt_50(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2330 = { sizeof (Tweener_t436044680), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2330[2] = 
{
	Tweener_t436044680::get_offset_of_hasManuallySetStartValue_51(),
	Tweener_t436044680::get_offset_of_isFromAllowed_52(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2331 = { sizeof (TweenType_t1971673186)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2331[4] = 
{
	TweenType_t1971673186::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2332 = { sizeof (UpdateType_t3937729206)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2332[5] = 
{
	UpdateType_t3937729206::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2333 = { sizeof (Color2Plugin_t2483663196), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2334 = { sizeof (DoublePlugin_t2037284588), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2335 = { sizeof (LongPlugin_t809278878), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2336 = { sizeof (UlongPlugin_t1878880915), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2337 = { sizeof (Vector3ArrayPlugin_t1419427579), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2338 = { sizeof (PathPlugin_t1182715676), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2338[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2339 = { sizeof (ColorPlugin_t4137411927), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2340 = { sizeof (IntPlugin_t942328046), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2341 = { sizeof (QuaternionPlugin_t495488984), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2342 = { sizeof (RectOffsetPlugin_t3656342773), -1, sizeof(RectOffsetPlugin_t3656342773_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2342[1] = 
{
	RectOffsetPlugin_t3656342773_StaticFields::get_offset_of__r_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2343 = { sizeof (RectPlugin_t2513065920), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2344 = { sizeof (UintPlugin_t2664954793), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2345 = { sizeof (Vector2Plugin_t1245585431), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2346 = { sizeof (Vector4Plugin_t4128927717), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2347 = { sizeof (StringPlugin_t660282191), -1, sizeof(StringPlugin_t660282191_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2347[2] = 
{
	StringPlugin_t660282191_StaticFields::get_offset_of__Buffer_0(),
	StringPlugin_t660282191_StaticFields::get_offset_of__OpenedTags_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2348 = { sizeof (StringPluginExtensions_t3521198905), -1, sizeof(StringPluginExtensions_t3521198905_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2348[5] = 
{
	StringPluginExtensions_t3521198905_StaticFields::get_offset_of_ScrambledCharsAll_0(),
	StringPluginExtensions_t3521198905_StaticFields::get_offset_of_ScrambledCharsUppercase_1(),
	StringPluginExtensions_t3521198905_StaticFields::get_offset_of_ScrambledCharsLowercase_2(),
	StringPluginExtensions_t3521198905_StaticFields::get_offset_of_ScrambledCharsNumerals_3(),
	StringPluginExtensions_t3521198905_StaticFields::get_offset_of__lastRndSeed_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2349 = { sizeof (FloatPlugin_t2370056133), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2350 = { sizeof (Vector3Plugin_t1104422930), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2351 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2352 = { sizeof (OrientType_t1731166963)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2352[5] = 
{
	OrientType_t1731166963::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2353 = { sizeof (PathOptions_t2074623791)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2353[15] = 
{
	PathOptions_t2074623791::get_offset_of_mode_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PathOptions_t2074623791::get_offset_of_orientType_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PathOptions_t2074623791::get_offset_of_lockPositionAxis_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PathOptions_t2074623791::get_offset_of_lockRotationAxis_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PathOptions_t2074623791::get_offset_of_isClosedPath_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PathOptions_t2074623791::get_offset_of_lookAtPosition_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PathOptions_t2074623791::get_offset_of_lookAtTransform_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PathOptions_t2074623791::get_offset_of_lookAhead_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PathOptions_t2074623791::get_offset_of_hasCustomForwardDirection_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PathOptions_t2074623791::get_offset_of_forward_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PathOptions_t2074623791::get_offset_of_useLocalPosition_10() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PathOptions_t2074623791::get_offset_of_parent_11() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PathOptions_t2074623791::get_offset_of_isRigidbody_12() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PathOptions_t2074623791::get_offset_of_startupRot_13() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PathOptions_t2074623791::get_offset_of_startupZRot_14() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2354 = { sizeof (QuaternionOptions_t2974423933)+ sizeof (RuntimeObject), sizeof(QuaternionOptions_t2974423933 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2354[3] = 
{
	QuaternionOptions_t2974423933::get_offset_of_rotateMode_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	QuaternionOptions_t2974423933::get_offset_of_axisConstraint_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	QuaternionOptions_t2974423933::get_offset_of_up_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2355 = { sizeof (UintOptions_t1006674242)+ sizeof (RuntimeObject), sizeof(UintOptions_t1006674242_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2355[1] = 
{
	UintOptions_t1006674242::get_offset_of_isNegativeChangeValue_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2356 = { sizeof (Vector3ArrayOptions_t534739431)+ sizeof (RuntimeObject), sizeof(Vector3ArrayOptions_t534739431_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2356[3] = 
{
	Vector3ArrayOptions_t534739431::get_offset_of_axisConstraint_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Vector3ArrayOptions_t534739431::get_offset_of_snapping_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Vector3ArrayOptions_t534739431::get_offset_of_durations_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2357 = { sizeof (NoOptions_t313102519)+ sizeof (RuntimeObject), sizeof(NoOptions_t313102519 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2358 = { sizeof (ColorOptions_t1487297155)+ sizeof (RuntimeObject), sizeof(ColorOptions_t1487297155_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2358[1] = 
{
	ColorOptions_t1487297155::get_offset_of_alphaOnly_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2359 = { sizeof (FloatOptions_t1203667100)+ sizeof (RuntimeObject), sizeof(FloatOptions_t1203667100_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2359[1] = 
{
	FloatOptions_t1203667100::get_offset_of_snapping_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2360 = { sizeof (RectOptions_t1018205596)+ sizeof (RuntimeObject), sizeof(RectOptions_t1018205596_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2360[1] = 
{
	RectOptions_t1018205596::get_offset_of_snapping_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2361 = { sizeof (StringOptions_t3992490940)+ sizeof (RuntimeObject), sizeof(StringOptions_t3992490940_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2361[5] = 
{
	StringOptions_t3992490940::get_offset_of_richTextEnabled_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	StringOptions_t3992490940::get_offset_of_scrambleMode_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	StringOptions_t3992490940::get_offset_of_scrambledChars_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	StringOptions_t3992490940::get_offset_of_startValueStrippedLength_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	StringOptions_t3992490940::get_offset_of_changeValueStrippedLength_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2362 = { sizeof (VectorOptions_t1354903650)+ sizeof (RuntimeObject), sizeof(VectorOptions_t1354903650_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2362[2] = 
{
	VectorOptions_t1354903650::get_offset_of_axisConstraint_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	VectorOptions_t1354903650::get_offset_of_snapping_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2363 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2364 = { sizeof (SpecialPluginsUtils_t1116177272), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2365 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2366 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2367 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2368 = { sizeof (PluginsManager_t1753812699), -1, sizeof(PluginsManager_t1753812699_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2368[18] = 
{
	PluginsManager_t1753812699_StaticFields::get_offset_of__floatPlugin_0(),
	PluginsManager_t1753812699_StaticFields::get_offset_of__doublePlugin_1(),
	PluginsManager_t1753812699_StaticFields::get_offset_of__intPlugin_2(),
	PluginsManager_t1753812699_StaticFields::get_offset_of__uintPlugin_3(),
	PluginsManager_t1753812699_StaticFields::get_offset_of__longPlugin_4(),
	PluginsManager_t1753812699_StaticFields::get_offset_of__ulongPlugin_5(),
	PluginsManager_t1753812699_StaticFields::get_offset_of__vector2Plugin_6(),
	PluginsManager_t1753812699_StaticFields::get_offset_of__vector3Plugin_7(),
	PluginsManager_t1753812699_StaticFields::get_offset_of__vector4Plugin_8(),
	PluginsManager_t1753812699_StaticFields::get_offset_of__quaternionPlugin_9(),
	PluginsManager_t1753812699_StaticFields::get_offset_of__colorPlugin_10(),
	PluginsManager_t1753812699_StaticFields::get_offset_of__rectPlugin_11(),
	PluginsManager_t1753812699_StaticFields::get_offset_of__rectOffsetPlugin_12(),
	PluginsManager_t1753812699_StaticFields::get_offset_of__stringPlugin_13(),
	PluginsManager_t1753812699_StaticFields::get_offset_of__vector3ArrayPlugin_14(),
	PluginsManager_t1753812699_StaticFields::get_offset_of__color2Plugin_15(),
	0,
	PluginsManager_t1753812699_StaticFields::get_offset_of__customPlugins_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2369 = { sizeof (ControlPoint_t3892672090)+ sizeof (RuntimeObject), sizeof(ControlPoint_t3892672090 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2369[2] = 
{
	ControlPoint_t3892672090::get_offset_of_a_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ControlPoint_t3892672090::get_offset_of_b_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2370 = { sizeof (ABSPathDecoder_t2613982196), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2371 = { sizeof (CatmullRomDecoder_t2053048079), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2372 = { sizeof (LinearDecoder_t2708327777), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2373 = { sizeof (Path_t3614338981), -1, sizeof(Path_t3614338981_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2373[21] = 
{
	Path_t3614338981_StaticFields::get_offset_of__catmullRomDecoder_0(),
	Path_t3614338981_StaticFields::get_offset_of__linearDecoder_1(),
	Path_t3614338981::get_offset_of_wpLengths_2(),
	Path_t3614338981::get_offset_of_type_3(),
	Path_t3614338981::get_offset_of_subdivisionsXSegment_4(),
	Path_t3614338981::get_offset_of_subdivisions_5(),
	Path_t3614338981::get_offset_of_wps_6(),
	Path_t3614338981::get_offset_of_controlPoints_7(),
	Path_t3614338981::get_offset_of_length_8(),
	Path_t3614338981::get_offset_of_isFinalized_9(),
	Path_t3614338981::get_offset_of_timesTable_10(),
	Path_t3614338981::get_offset_of_lengthsTable_11(),
	Path_t3614338981::get_offset_of_linearWPIndex_12(),
	Path_t3614338981::get_offset_of__incrementalClone_13(),
	Path_t3614338981::get_offset_of__incrementalIndex_14(),
	Path_t3614338981::get_offset_of__decoder_15(),
	Path_t3614338981::get_offset_of__changed_16(),
	Path_t3614338981::get_offset_of_nonLinearDrawWps_17(),
	Path_t3614338981::get_offset_of_targetPosition_18(),
	Path_t3614338981::get_offset_of_lookAtPosition_19(),
	Path_t3614338981::get_offset_of_gizmoColor_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2374 = { sizeof (PureQuaternionPlugin_t587122414), -1, sizeof(PureQuaternionPlugin_t587122414_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2374[1] = 
{
	PureQuaternionPlugin_t587122414_StaticFields::get_offset_of__plug_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2375 = { sizeof (ABSSequentiable_t3376041011), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2375[4] = 
{
	ABSSequentiable_t3376041011::get_offset_of_tweenType_0(),
	ABSSequentiable_t3376041011::get_offset_of_sequencedPosition_1(),
	ABSSequentiable_t3376041011::get_offset_of_sequencedEndPosition_2(),
	ABSSequentiable_t3376041011::get_offset_of_onStart_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2376 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2377 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2378 = { sizeof (Debugger_t1756157868), -1, sizeof(Debugger_t1756157868_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2378[1] = 
{
	Debugger_t1756157868_StaticFields::get_offset_of_logPriority_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2379 = { sizeof (DOTweenComponent_t828035757), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2379[4] = 
{
	DOTweenComponent_t828035757::get_offset_of_inspectorUpdater_4(),
	DOTweenComponent_t828035757::get_offset_of__unscaledTime_5(),
	DOTweenComponent_t828035757::get_offset_of__unscaledDeltaTime_6(),
	DOTweenComponent_t828035757::get_offset_of__duplicateToDestroy_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2380 = { sizeof (U3CWaitForCompletionU3Ed__14_t1419758899), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2380[3] = 
{
	U3CWaitForCompletionU3Ed__14_t1419758899::get_offset_of_U3CU3E1__state_0(),
	U3CWaitForCompletionU3Ed__14_t1419758899::get_offset_of_U3CU3E2__current_1(),
	U3CWaitForCompletionU3Ed__14_t1419758899::get_offset_of_t_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2381 = { sizeof (U3CWaitForRewindU3Ed__15_t2339862385), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2381[3] = 
{
	U3CWaitForRewindU3Ed__15_t2339862385::get_offset_of_U3CU3E1__state_0(),
	U3CWaitForRewindU3Ed__15_t2339862385::get_offset_of_U3CU3E2__current_1(),
	U3CWaitForRewindU3Ed__15_t2339862385::get_offset_of_t_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2382 = { sizeof (U3CWaitForKillU3Ed__16_t383710286), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2382[3] = 
{
	U3CWaitForKillU3Ed__16_t383710286::get_offset_of_U3CU3E1__state_0(),
	U3CWaitForKillU3Ed__16_t383710286::get_offset_of_U3CU3E2__current_1(),
	U3CWaitForKillU3Ed__16_t383710286::get_offset_of_t_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2383 = { sizeof (U3CWaitForElapsedLoopsU3Ed__17_t1514468280), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2383[4] = 
{
	U3CWaitForElapsedLoopsU3Ed__17_t1514468280::get_offset_of_U3CU3E1__state_0(),
	U3CWaitForElapsedLoopsU3Ed__17_t1514468280::get_offset_of_U3CU3E2__current_1(),
	U3CWaitForElapsedLoopsU3Ed__17_t1514468280::get_offset_of_t_2(),
	U3CWaitForElapsedLoopsU3Ed__17_t1514468280::get_offset_of_elapsedLoops_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2384 = { sizeof (U3CWaitForPositionU3Ed__18_t1811625055), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2384[4] = 
{
	U3CWaitForPositionU3Ed__18_t1811625055::get_offset_of_U3CU3E1__state_0(),
	U3CWaitForPositionU3Ed__18_t1811625055::get_offset_of_U3CU3E2__current_1(),
	U3CWaitForPositionU3Ed__18_t1811625055::get_offset_of_t_2(),
	U3CWaitForPositionU3Ed__18_t1811625055::get_offset_of_position_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2385 = { sizeof (U3CWaitForStartU3Ed__19_t3428776308), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2385[3] = 
{
	U3CWaitForStartU3Ed__19_t3428776308::get_offset_of_U3CU3E1__state_0(),
	U3CWaitForStartU3Ed__19_t3428776308::get_offset_of_U3CU3E2__current_1(),
	U3CWaitForStartU3Ed__19_t3428776308::get_offset_of_t_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2386 = { sizeof (DOTweenSettings_t4078947542), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2386[18] = 
{
	0,
	DOTweenSettings_t4078947542::get_offset_of_useSafeMode_5(),
	DOTweenSettings_t4078947542::get_offset_of_timeScale_6(),
	DOTweenSettings_t4078947542::get_offset_of_useSmoothDeltaTime_7(),
	DOTweenSettings_t4078947542::get_offset_of_maxSmoothUnscaledTime_8(),
	DOTweenSettings_t4078947542::get_offset_of_showUnityEditorReport_9(),
	DOTweenSettings_t4078947542::get_offset_of_logBehaviour_10(),
	DOTweenSettings_t4078947542::get_offset_of_drawGizmos_11(),
	DOTweenSettings_t4078947542::get_offset_of_defaultRecyclable_12(),
	DOTweenSettings_t4078947542::get_offset_of_defaultAutoPlay_13(),
	DOTweenSettings_t4078947542::get_offset_of_defaultUpdateType_14(),
	DOTweenSettings_t4078947542::get_offset_of_defaultTimeScaleIndependent_15(),
	DOTweenSettings_t4078947542::get_offset_of_defaultEaseType_16(),
	DOTweenSettings_t4078947542::get_offset_of_defaultEaseOvershootOrAmplitude_17(),
	DOTweenSettings_t4078947542::get_offset_of_defaultEasePeriod_18(),
	DOTweenSettings_t4078947542::get_offset_of_defaultAutoKill_19(),
	DOTweenSettings_t4078947542::get_offset_of_defaultLoopType_20(),
	DOTweenSettings_t4078947542::get_offset_of_storeSettingsLocation_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2387 = { sizeof (SettingsLocation_t2227561762)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2387[4] = 
{
	SettingsLocation_t2227561762::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2388 = { sizeof (Extensions_t3835977652), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2389 = { sizeof (SequenceCallback_t1025787276), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2390 = { sizeof (TweenManager_t374091826), -1, sizeof(TweenManager_t374091826_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2390[33] = 
{
	0,
	0,
	0,
	TweenManager_t374091826_StaticFields::get_offset_of_maxActive_3(),
	TweenManager_t374091826_StaticFields::get_offset_of_maxTweeners_4(),
	TweenManager_t374091826_StaticFields::get_offset_of_maxSequences_5(),
	TweenManager_t374091826_StaticFields::get_offset_of_hasActiveTweens_6(),
	TweenManager_t374091826_StaticFields::get_offset_of_hasActiveDefaultTweens_7(),
	TweenManager_t374091826_StaticFields::get_offset_of_hasActiveLateTweens_8(),
	TweenManager_t374091826_StaticFields::get_offset_of_hasActiveFixedTweens_9(),
	TweenManager_t374091826_StaticFields::get_offset_of_hasActiveManualTweens_10(),
	TweenManager_t374091826_StaticFields::get_offset_of_totActiveTweens_11(),
	TweenManager_t374091826_StaticFields::get_offset_of_totActiveDefaultTweens_12(),
	TweenManager_t374091826_StaticFields::get_offset_of_totActiveLateTweens_13(),
	TweenManager_t374091826_StaticFields::get_offset_of_totActiveFixedTweens_14(),
	TweenManager_t374091826_StaticFields::get_offset_of_totActiveManualTweens_15(),
	TweenManager_t374091826_StaticFields::get_offset_of_totActiveTweeners_16(),
	TweenManager_t374091826_StaticFields::get_offset_of_totActiveSequences_17(),
	TweenManager_t374091826_StaticFields::get_offset_of_totPooledTweeners_18(),
	TweenManager_t374091826_StaticFields::get_offset_of_totPooledSequences_19(),
	TweenManager_t374091826_StaticFields::get_offset_of_totTweeners_20(),
	TweenManager_t374091826_StaticFields::get_offset_of_totSequences_21(),
	TweenManager_t374091826_StaticFields::get_offset_of_isUpdateLoop_22(),
	TweenManager_t374091826_StaticFields::get_offset_of__activeTweens_23(),
	TweenManager_t374091826_StaticFields::get_offset_of__pooledTweeners_24(),
	TweenManager_t374091826_StaticFields::get_offset_of__PooledSequences_25(),
	TweenManager_t374091826_StaticFields::get_offset_of__KillList_26(),
	TweenManager_t374091826_StaticFields::get_offset_of__maxActiveLookupId_27(),
	TweenManager_t374091826_StaticFields::get_offset_of__requiresActiveReorganization_28(),
	TweenManager_t374091826_StaticFields::get_offset_of__reorganizeFromId_29(),
	TweenManager_t374091826_StaticFields::get_offset_of__minPooledTweenerId_30(),
	TweenManager_t374091826_StaticFields::get_offset_of__maxPooledTweenerId_31(),
	TweenManager_t374091826_StaticFields::get_offset_of__despawnAllCalledFromUpdateLoopCallback_32(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2391 = { sizeof (CapacityIncreaseMode_t828064684)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2391[4] = 
{
	CapacityIncreaseMode_t828064684::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2392 = { sizeof (Utils_t849021263), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2393 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2393[8] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2394 = { sizeof (FilterType_t3228157936)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2394[6] = 
{
	FilterType_t3228157936::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2395 = { sizeof (OperationType_t2152057980)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2395[14] = 
{
	OperationType_t2152057980::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2396 = { sizeof (SpecialStartupMode_t1644068939)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2396[6] = 
{
	SpecialStartupMode_t1644068939::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2397 = { sizeof (UpdateNotice_t1001625488)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2397[3] = 
{
	UpdateNotice_t1001625488::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2398 = { sizeof (UpdateMode_t1164472539)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2398[4] = 
{
	UpdateMode_t1164472539::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2399 = { sizeof (Bounce_t852251727), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
