
#import "AppDelegate.h"

extern "C"
{
    void _ReturnToNative()
    {
        NSLog(@"_ReturnToNative press");
        AppDelegate *delegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
        [delegate hideUnityWindow];
    }
}
