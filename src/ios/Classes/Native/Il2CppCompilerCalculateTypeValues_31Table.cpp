﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// System.AsyncCallback
struct AsyncCallback_t3962456242;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t2736202052;
// System.Collections.Generic.List`1<System.Collections.Hashtable>
struct List_1_t3325964508;
// System.Collections.Generic.List`1<TriLib.Extras.BoneRelationship>
struct List_1_t780838325;
// System.Collections.Hashtable
struct Hashtable_t1853889766;
// System.DelegateData
struct DelegateData_t1677132599;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Single[]
struct SingleU5BU5D_t1444911251;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1281789340;
// System.Void
struct Void_t1185182177;
// TriLib.AssetLoaderOptions
struct AssetLoaderOptions_t2366605288;
// TriLib.Extras.AvatarLoader
struct AvatarLoader_t2953159321;
// TriLib.Extras.BoneRelationshipList
struct BoneRelationshipList_t2048306945;
// UnityEngine.AudioSource
struct AudioSource_t3935305588;
// UnityEngine.Color[0...,0...]
struct ColorU5B0___U2C0___U5D_t941916414;
// UnityEngine.GUIStyle
struct GUIStyle_t3956901511;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// UnityEngine.Material
struct Material_t340375123;
// UnityEngine.Rect[]
struct RectU5BU5D_t2936723554;
// UnityEngine.RuntimeAnimatorController
struct RuntimeAnimatorController_t2933699135;
// UnityEngine.Texture2D
struct Texture2D_t3840446185;
// UnityEngine.Transform
struct Transform_t3600365921;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t1457185986;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1718750761;
// Vuforia.TrackableBehaviour
struct TrackableBehaviour_t1113559212;
// iTween
struct iTween_t770867771;
// iTween/ApplyTween
struct ApplyTween_t3327999347;
// iTween/CRSpline
struct CRSpline_t2815350084;
// iTween/EasingFunction
struct EasingFunction_t2767217938;




#ifndef U3CMODULEU3E_T692745556_H
#define U3CMODULEU3E_T692745556_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745556 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745556_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef ASSIMPINTEROP_T74186816_H
#define ASSIMPINTEROP_T74186816_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.AssimpInterop
struct  AssimpInterop_t74186816  : public RuntimeObject
{
public:

public:
};

struct AssimpInterop_t74186816_StaticFields
{
public:
	// System.Boolean TriLib.AssimpInterop::Is32Bits
	bool ___Is32Bits_3;
	// System.Int32 TriLib.AssimpInterop::IntSize
	int32_t ___IntSize_4;

public:
	inline static int32_t get_offset_of_Is32Bits_3() { return static_cast<int32_t>(offsetof(AssimpInterop_t74186816_StaticFields, ___Is32Bits_3)); }
	inline bool get_Is32Bits_3() const { return ___Is32Bits_3; }
	inline bool* get_address_of_Is32Bits_3() { return &___Is32Bits_3; }
	inline void set_Is32Bits_3(bool value)
	{
		___Is32Bits_3 = value;
	}

	inline static int32_t get_offset_of_IntSize_4() { return static_cast<int32_t>(offsetof(AssimpInterop_t74186816_StaticFields, ___IntSize_4)); }
	inline int32_t get_IntSize_4() const { return ___IntSize_4; }
	inline int32_t* get_address_of_IntSize_4() { return &___IntSize_4; }
	inline void set_IntSize_4(int32_t value)
	{
		___IntSize_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASSIMPINTEROP_T74186816_H
#ifndef CAMERAEXTENSIONS_T808704216_H
#define CAMERAEXTENSIONS_T808704216_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.CameraExtensions
struct  CameraExtensions_t808704216  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERAEXTENSIONS_T808704216_H
#ifndef U3CSTARTU3EC__ANONSTOREY0_T1727939190_H
#define U3CSTARTU3EC__ANONSTOREY0_T1727939190_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.Extras.AvatarLoaderSample/<Start>c__AnonStorey0
struct  U3CStartU3Ec__AnonStorey0_t1727939190  : public RuntimeObject
{
public:
	// System.String TriLib.Extras.AvatarLoaderSample/<Start>c__AnonStorey0::supportedExtensions
	String_t* ___supportedExtensions_0;

public:
	inline static int32_t get_offset_of_supportedExtensions_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__AnonStorey0_t1727939190, ___supportedExtensions_0)); }
	inline String_t* get_supportedExtensions_0() const { return ___supportedExtensions_0; }
	inline String_t** get_address_of_supportedExtensions_0() { return &___supportedExtensions_0; }
	inline void set_supportedExtensions_0(String_t* value)
	{
		___supportedExtensions_0 = value;
		Il2CppCodeGenWriteBarrier((&___supportedExtensions_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTU3EC__ANONSTOREY0_T1727939190_H
#ifndef BONERELATIONSHIP_T3603730879_H
#define BONERELATIONSHIP_T3603730879_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.Extras.BoneRelationship
struct  BoneRelationship_t3603730879  : public RuntimeObject
{
public:
	// System.String TriLib.Extras.BoneRelationship::HumanBone
	String_t* ___HumanBone_0;
	// System.String TriLib.Extras.BoneRelationship::BoneName
	String_t* ___BoneName_1;
	// System.Boolean TriLib.Extras.BoneRelationship::Optional
	bool ___Optional_2;

public:
	inline static int32_t get_offset_of_HumanBone_0() { return static_cast<int32_t>(offsetof(BoneRelationship_t3603730879, ___HumanBone_0)); }
	inline String_t* get_HumanBone_0() const { return ___HumanBone_0; }
	inline String_t** get_address_of_HumanBone_0() { return &___HumanBone_0; }
	inline void set_HumanBone_0(String_t* value)
	{
		___HumanBone_0 = value;
		Il2CppCodeGenWriteBarrier((&___HumanBone_0), value);
	}

	inline static int32_t get_offset_of_BoneName_1() { return static_cast<int32_t>(offsetof(BoneRelationship_t3603730879, ___BoneName_1)); }
	inline String_t* get_BoneName_1() const { return ___BoneName_1; }
	inline String_t** get_address_of_BoneName_1() { return &___BoneName_1; }
	inline void set_BoneName_1(String_t* value)
	{
		___BoneName_1 = value;
		Il2CppCodeGenWriteBarrier((&___BoneName_1), value);
	}

	inline static int32_t get_offset_of_Optional_2() { return static_cast<int32_t>(offsetof(BoneRelationship_t3603730879, ___Optional_2)); }
	inline bool get_Optional_2() const { return ___Optional_2; }
	inline bool* get_address_of_Optional_2() { return &___Optional_2; }
	inline void set_Optional_2(bool value)
	{
		___Optional_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BONERELATIONSHIP_T3603730879_H
#ifndef BONERELATIONSHIPLIST_T2048306945_H
#define BONERELATIONSHIPLIST_T2048306945_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.Extras.BoneRelationshipList
struct  BoneRelationshipList_t2048306945  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<TriLib.Extras.BoneRelationship> TriLib.Extras.BoneRelationshipList::_relationships
	List_1_t780838325 * ____relationships_0;

public:
	inline static int32_t get_offset_of__relationships_0() { return static_cast<int32_t>(offsetof(BoneRelationshipList_t2048306945, ____relationships_0)); }
	inline List_1_t780838325 * get__relationships_0() const { return ____relationships_0; }
	inline List_1_t780838325 ** get_address_of__relationships_0() { return &____relationships_0; }
	inline void set__relationships_0(List_1_t780838325 * value)
	{
		____relationships_0 = value;
		Il2CppCodeGenWriteBarrier((&____relationships_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BONERELATIONSHIPLIST_T2048306945_H
#ifndef FILEUTILS_T3396084117_H
#define FILEUTILS_T3396084117_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.FileUtils
struct  FileUtils_t3396084117  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILEUTILS_T3396084117_H
#ifndef MATRIXEXTENSIONS_T886005937_H
#define MATRIXEXTENSIONS_T886005937_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.MatrixExtensions
struct  MatrixExtensions_t886005937  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATRIXEXTENSIONS_T886005937_H
#ifndef STRINGUTILS_T4119127951_H
#define STRINGUTILS_T4119127951_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.StringUtils
struct  StringUtils_t4119127951  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGUTILS_T4119127951_H
#ifndef TEXTURE2DUTILS_T607689861_H
#define TEXTURE2DUTILS_T607689861_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.Texture2DUtils
struct  Texture2DUtils_t607689861  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTURE2DUTILS_T607689861_H
#ifndef TRANSFORMEXTENSIONS_T817912662_H
#define TRANSFORMEXTENSIONS_T817912662_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.TransformExtensions
struct  TransformExtensions_t817912662  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORMEXTENSIONS_T817912662_H
#ifndef U3CSTARTU3EC__ITERATOR2_T2390838266_H
#define U3CSTARTU3EC__ITERATOR2_T2390838266_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// iTween/<Start>c__Iterator2
struct  U3CStartU3Ec__Iterator2_t2390838266  : public RuntimeObject
{
public:
	// iTween iTween/<Start>c__Iterator2::$this
	iTween_t770867771 * ___U24this_0;
	// System.Object iTween/<Start>c__Iterator2::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean iTween/<Start>c__Iterator2::$disposing
	bool ___U24disposing_2;
	// System.Int32 iTween/<Start>c__Iterator2::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator2_t2390838266, ___U24this_0)); }
	inline iTween_t770867771 * get_U24this_0() const { return ___U24this_0; }
	inline iTween_t770867771 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(iTween_t770867771 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator2_t2390838266, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator2_t2390838266, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator2_t2390838266, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTU3EC__ITERATOR2_T2390838266_H
#ifndef U3CTWEENDELAYU3EC__ITERATOR0_T2686771544_H
#define U3CTWEENDELAYU3EC__ITERATOR0_T2686771544_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// iTween/<TweenDelay>c__Iterator0
struct  U3CTweenDelayU3Ec__Iterator0_t2686771544  : public RuntimeObject
{
public:
	// iTween iTween/<TweenDelay>c__Iterator0::$this
	iTween_t770867771 * ___U24this_0;
	// System.Object iTween/<TweenDelay>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean iTween/<TweenDelay>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 iTween/<TweenDelay>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CTweenDelayU3Ec__Iterator0_t2686771544, ___U24this_0)); }
	inline iTween_t770867771 * get_U24this_0() const { return ___U24this_0; }
	inline iTween_t770867771 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(iTween_t770867771 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CTweenDelayU3Ec__Iterator0_t2686771544, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CTweenDelayU3Ec__Iterator0_t2686771544, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CTweenDelayU3Ec__Iterator0_t2686771544, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CTWEENDELAYU3EC__ITERATOR0_T2686771544_H
#ifndef U3CTWEENRESTARTU3EC__ITERATOR1_T1737386981_H
#define U3CTWEENRESTARTU3EC__ITERATOR1_T1737386981_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// iTween/<TweenRestart>c__Iterator1
struct  U3CTweenRestartU3Ec__Iterator1_t1737386981  : public RuntimeObject
{
public:
	// iTween iTween/<TweenRestart>c__Iterator1::$this
	iTween_t770867771 * ___U24this_0;
	// System.Object iTween/<TweenRestart>c__Iterator1::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean iTween/<TweenRestart>c__Iterator1::$disposing
	bool ___U24disposing_2;
	// System.Int32 iTween/<TweenRestart>c__Iterator1::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CTweenRestartU3Ec__Iterator1_t1737386981, ___U24this_0)); }
	inline iTween_t770867771 * get_U24this_0() const { return ___U24this_0; }
	inline iTween_t770867771 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(iTween_t770867771 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CTweenRestartU3Ec__Iterator1_t1737386981, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CTweenRestartU3Ec__Iterator1_t1737386981, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CTweenRestartU3Ec__Iterator1_t1737386981, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CTWEENRESTARTU3EC__ITERATOR1_T1737386981_H
#ifndef CRSPLINE_T2815350084_H
#define CRSPLINE_T2815350084_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// iTween/CRSpline
struct  CRSpline_t2815350084  : public RuntimeObject
{
public:
	// UnityEngine.Vector3[] iTween/CRSpline::pts
	Vector3U5BU5D_t1718750761* ___pts_0;

public:
	inline static int32_t get_offset_of_pts_0() { return static_cast<int32_t>(offsetof(CRSpline_t2815350084, ___pts_0)); }
	inline Vector3U5BU5D_t1718750761* get_pts_0() const { return ___pts_0; }
	inline Vector3U5BU5D_t1718750761** get_address_of_pts_0() { return &___pts_0; }
	inline void set_pts_0(Vector3U5BU5D_t1718750761* value)
	{
		___pts_0 = value;
		Il2CppCodeGenWriteBarrier((&___pts_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CRSPLINE_T2815350084_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef SINGLE_T1397266774_H
#define SINGLE_T1397266774_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_t1397266774 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_7;

public:
	inline static int32_t get_offset_of_m_value_7() { return static_cast<int32_t>(offsetof(Single_t1397266774, ___m_value_7)); }
	inline float get_m_value_7() const { return ___m_value_7; }
	inline float* get_address_of_m_value_7() { return &___m_value_7; }
	inline void set_m_value_7(float value)
	{
		___m_value_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_T1397266774_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef RECT_T2360479859_H
#define RECT_T2360479859_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rect
struct  Rect_t2360479859 
{
public:
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_XMin_0)); }
	inline float get_m_XMin_0() const { return ___m_XMin_0; }
	inline float* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(float value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_YMin_1)); }
	inline float get_m_YMin_1() const { return ___m_YMin_1; }
	inline float* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(float value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_Width_2)); }
	inline float get_m_Width_2() const { return ___m_Width_2; }
	inline float* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(float value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_Height_3)); }
	inline float get_m_Height_3() const { return ___m_Height_3; }
	inline float* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(float value)
	{
		___m_Height_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECT_T2360479859_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_5)); }
	inline Vector3_t3722313464  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_t3722313464  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_6)); }
	inline Vector3_t3722313464  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_t3722313464 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_t3722313464  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_7)); }
	inline Vector3_t3722313464  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_t3722313464 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_t3722313464  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_8)); }
	inline Vector3_t3722313464  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_t3722313464 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_t3722313464  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_9)); }
	inline Vector3_t3722313464  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_t3722313464 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_t3722313464  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_10)); }
	inline Vector3_t3722313464  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_t3722313464 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_t3722313464  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_11)); }
	inline Vector3_t3722313464  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_t3722313464  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_12)); }
	inline Vector3_t3722313464  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_t3722313464 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_t3722313464  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_8)); }
	inline DelegateData_t1677132599 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1677132599 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1677132599 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T1188392813_H
#ifndef ASSIMPPOSTPROCESSSTEPS_T3704598218_H
#define ASSIMPPOSTPROCESSSTEPS_T3704598218_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.AssimpPostProcessSteps
struct  AssimpPostProcessSteps_t3704598218 
{
public:
	// System.Int32 TriLib.AssimpPostProcessSteps::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AssimpPostProcessSteps_t3704598218, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASSIMPPOSTPROCESSSTEPS_T3704598218_H
#ifndef TEXTURECOMPRESSION_T1528154288_H
#define TEXTURECOMPRESSION_T1528154288_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.TextureCompression
struct  TextureCompression_t1528154288 
{
public:
	// System.Int32 TriLib.TextureCompression::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TextureCompression_t1528154288, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTURECOMPRESSION_T1528154288_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef SPACE_T654135784_H
#define SPACE_T654135784_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Space
struct  Space_t654135784 
{
public:
	// System.Int32 UnityEngine.Space::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Space_t654135784, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPACE_T654135784_H
#ifndef EASETYPE_T2573404410_H
#define EASETYPE_T2573404410_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// iTween/EaseType
struct  EaseType_t2573404410 
{
public:
	// System.Int32 iTween/EaseType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(EaseType_t2573404410, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EASETYPE_T2573404410_H
#ifndef LOOPTYPE_T369612249_H
#define LOOPTYPE_T369612249_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// iTween/LoopType
struct  LoopType_t369612249 
{
public:
	// System.Int32 iTween/LoopType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(LoopType_t369612249, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOOPTYPE_T369612249_H
#ifndef NAMEDVALUECOLOR_T1091574706_H
#define NAMEDVALUECOLOR_T1091574706_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// iTween/NamedValueColor
struct  NamedValueColor_t1091574706 
{
public:
	// System.Int32 iTween/NamedValueColor::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(NamedValueColor_t1091574706, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAMEDVALUECOLOR_T1091574706_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___prev_9)); }
	inline MulticastDelegate_t * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___kpm_next_10)); }
	inline MulticastDelegate_t * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T_H
#ifndef ASSIMPPROCESSPRESET_T3828703936_H
#define ASSIMPPROCESSPRESET_T3828703936_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.AssimpProcessPreset
struct  AssimpProcessPreset_t3828703936  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASSIMPPROCESSPRESET_T3828703936_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef DEFAULTS_T3148213711_H
#define DEFAULTS_T3148213711_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// iTween/Defaults
struct  Defaults_t3148213711  : public RuntimeObject
{
public:

public:
};

struct Defaults_t3148213711_StaticFields
{
public:
	// System.Single iTween/Defaults::time
	float ___time_0;
	// System.Single iTween/Defaults::delay
	float ___delay_1;
	// iTween/NamedValueColor iTween/Defaults::namedColorValue
	int32_t ___namedColorValue_2;
	// iTween/LoopType iTween/Defaults::loopType
	int32_t ___loopType_3;
	// iTween/EaseType iTween/Defaults::easeType
	int32_t ___easeType_4;
	// System.Single iTween/Defaults::lookSpeed
	float ___lookSpeed_5;
	// System.Boolean iTween/Defaults::isLocal
	bool ___isLocal_6;
	// UnityEngine.Space iTween/Defaults::space
	int32_t ___space_7;
	// System.Boolean iTween/Defaults::orientToPath
	bool ___orientToPath_8;
	// UnityEngine.Color iTween/Defaults::color
	Color_t2555686324  ___color_9;
	// System.Single iTween/Defaults::updateTimePercentage
	float ___updateTimePercentage_10;
	// System.Single iTween/Defaults::updateTime
	float ___updateTime_11;
	// System.Single iTween/Defaults::lookAhead
	float ___lookAhead_12;
	// System.Boolean iTween/Defaults::useRealTime
	bool ___useRealTime_13;
	// UnityEngine.Vector3 iTween/Defaults::up
	Vector3_t3722313464  ___up_14;

public:
	inline static int32_t get_offset_of_time_0() { return static_cast<int32_t>(offsetof(Defaults_t3148213711_StaticFields, ___time_0)); }
	inline float get_time_0() const { return ___time_0; }
	inline float* get_address_of_time_0() { return &___time_0; }
	inline void set_time_0(float value)
	{
		___time_0 = value;
	}

	inline static int32_t get_offset_of_delay_1() { return static_cast<int32_t>(offsetof(Defaults_t3148213711_StaticFields, ___delay_1)); }
	inline float get_delay_1() const { return ___delay_1; }
	inline float* get_address_of_delay_1() { return &___delay_1; }
	inline void set_delay_1(float value)
	{
		___delay_1 = value;
	}

	inline static int32_t get_offset_of_namedColorValue_2() { return static_cast<int32_t>(offsetof(Defaults_t3148213711_StaticFields, ___namedColorValue_2)); }
	inline int32_t get_namedColorValue_2() const { return ___namedColorValue_2; }
	inline int32_t* get_address_of_namedColorValue_2() { return &___namedColorValue_2; }
	inline void set_namedColorValue_2(int32_t value)
	{
		___namedColorValue_2 = value;
	}

	inline static int32_t get_offset_of_loopType_3() { return static_cast<int32_t>(offsetof(Defaults_t3148213711_StaticFields, ___loopType_3)); }
	inline int32_t get_loopType_3() const { return ___loopType_3; }
	inline int32_t* get_address_of_loopType_3() { return &___loopType_3; }
	inline void set_loopType_3(int32_t value)
	{
		___loopType_3 = value;
	}

	inline static int32_t get_offset_of_easeType_4() { return static_cast<int32_t>(offsetof(Defaults_t3148213711_StaticFields, ___easeType_4)); }
	inline int32_t get_easeType_4() const { return ___easeType_4; }
	inline int32_t* get_address_of_easeType_4() { return &___easeType_4; }
	inline void set_easeType_4(int32_t value)
	{
		___easeType_4 = value;
	}

	inline static int32_t get_offset_of_lookSpeed_5() { return static_cast<int32_t>(offsetof(Defaults_t3148213711_StaticFields, ___lookSpeed_5)); }
	inline float get_lookSpeed_5() const { return ___lookSpeed_5; }
	inline float* get_address_of_lookSpeed_5() { return &___lookSpeed_5; }
	inline void set_lookSpeed_5(float value)
	{
		___lookSpeed_5 = value;
	}

	inline static int32_t get_offset_of_isLocal_6() { return static_cast<int32_t>(offsetof(Defaults_t3148213711_StaticFields, ___isLocal_6)); }
	inline bool get_isLocal_6() const { return ___isLocal_6; }
	inline bool* get_address_of_isLocal_6() { return &___isLocal_6; }
	inline void set_isLocal_6(bool value)
	{
		___isLocal_6 = value;
	}

	inline static int32_t get_offset_of_space_7() { return static_cast<int32_t>(offsetof(Defaults_t3148213711_StaticFields, ___space_7)); }
	inline int32_t get_space_7() const { return ___space_7; }
	inline int32_t* get_address_of_space_7() { return &___space_7; }
	inline void set_space_7(int32_t value)
	{
		___space_7 = value;
	}

	inline static int32_t get_offset_of_orientToPath_8() { return static_cast<int32_t>(offsetof(Defaults_t3148213711_StaticFields, ___orientToPath_8)); }
	inline bool get_orientToPath_8() const { return ___orientToPath_8; }
	inline bool* get_address_of_orientToPath_8() { return &___orientToPath_8; }
	inline void set_orientToPath_8(bool value)
	{
		___orientToPath_8 = value;
	}

	inline static int32_t get_offset_of_color_9() { return static_cast<int32_t>(offsetof(Defaults_t3148213711_StaticFields, ___color_9)); }
	inline Color_t2555686324  get_color_9() const { return ___color_9; }
	inline Color_t2555686324 * get_address_of_color_9() { return &___color_9; }
	inline void set_color_9(Color_t2555686324  value)
	{
		___color_9 = value;
	}

	inline static int32_t get_offset_of_updateTimePercentage_10() { return static_cast<int32_t>(offsetof(Defaults_t3148213711_StaticFields, ___updateTimePercentage_10)); }
	inline float get_updateTimePercentage_10() const { return ___updateTimePercentage_10; }
	inline float* get_address_of_updateTimePercentage_10() { return &___updateTimePercentage_10; }
	inline void set_updateTimePercentage_10(float value)
	{
		___updateTimePercentage_10 = value;
	}

	inline static int32_t get_offset_of_updateTime_11() { return static_cast<int32_t>(offsetof(Defaults_t3148213711_StaticFields, ___updateTime_11)); }
	inline float get_updateTime_11() const { return ___updateTime_11; }
	inline float* get_address_of_updateTime_11() { return &___updateTime_11; }
	inline void set_updateTime_11(float value)
	{
		___updateTime_11 = value;
	}

	inline static int32_t get_offset_of_lookAhead_12() { return static_cast<int32_t>(offsetof(Defaults_t3148213711_StaticFields, ___lookAhead_12)); }
	inline float get_lookAhead_12() const { return ___lookAhead_12; }
	inline float* get_address_of_lookAhead_12() { return &___lookAhead_12; }
	inline void set_lookAhead_12(float value)
	{
		___lookAhead_12 = value;
	}

	inline static int32_t get_offset_of_useRealTime_13() { return static_cast<int32_t>(offsetof(Defaults_t3148213711_StaticFields, ___useRealTime_13)); }
	inline bool get_useRealTime_13() const { return ___useRealTime_13; }
	inline bool* get_address_of_useRealTime_13() { return &___useRealTime_13; }
	inline void set_useRealTime_13(bool value)
	{
		___useRealTime_13 = value;
	}

	inline static int32_t get_offset_of_up_14() { return static_cast<int32_t>(offsetof(Defaults_t3148213711_StaticFields, ___up_14)); }
	inline Vector3_t3722313464  get_up_14() const { return ___up_14; }
	inline Vector3_t3722313464 * get_address_of_up_14() { return &___up_14; }
	inline void set_up_14(Vector3_t3722313464  value)
	{
		___up_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTS_T3148213711_H
#ifndef TEXTURELOADHANDLE_T2346000395_H
#define TEXTURELOADHANDLE_T2346000395_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.TextureLoadHandle
struct  TextureLoadHandle_t2346000395  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTURELOADHANDLE_T2346000395_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef APPLYTWEEN_T3327999347_H
#define APPLYTWEEN_T3327999347_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// iTween/ApplyTween
struct  ApplyTween_t3327999347  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APPLYTWEEN_T3327999347_H
#ifndef EASINGFUNCTION_T2767217938_H
#define EASINGFUNCTION_T2767217938_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// iTween/EasingFunction
struct  EasingFunction_t2767217938  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EASINGFUNCTION_T2767217938_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef DEFAULTTRACKABLEEVENTHANDLER_T1588957063_H
#define DEFAULTTRACKABLEEVENTHANDLER_T1588957063_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DefaultTrackableEventHandler
struct  DefaultTrackableEventHandler_t1588957063  : public MonoBehaviour_t3962482529
{
public:
	// Vuforia.TrackableBehaviour DefaultTrackableEventHandler::mTrackableBehaviour
	TrackableBehaviour_t1113559212 * ___mTrackableBehaviour_4;

public:
	inline static int32_t get_offset_of_mTrackableBehaviour_4() { return static_cast<int32_t>(offsetof(DefaultTrackableEventHandler_t1588957063, ___mTrackableBehaviour_4)); }
	inline TrackableBehaviour_t1113559212 * get_mTrackableBehaviour_4() const { return ___mTrackableBehaviour_4; }
	inline TrackableBehaviour_t1113559212 ** get_address_of_mTrackableBehaviour_4() { return &___mTrackableBehaviour_4; }
	inline void set_mTrackableBehaviour_4(TrackableBehaviour_t1113559212 * value)
	{
		___mTrackableBehaviour_4 = value;
		Il2CppCodeGenWriteBarrier((&___mTrackableBehaviour_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTTRACKABLEEVENTHANDLER_T1588957063_H
#ifndef MOVESAMPLE_T3412539464_H
#define MOVESAMPLE_T3412539464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoveSample
struct  MoveSample_t3412539464  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOVESAMPLE_T3412539464_H
#ifndef ROTATESAMPLE_T3002381122_H
#define ROTATESAMPLE_T3002381122_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RotateSample
struct  RotateSample_t3002381122  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROTATESAMPLE_T3002381122_H
#ifndef SAMPLEINFO_T3693012684_H
#define SAMPLEINFO_T3693012684_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SampleInfo
struct  SampleInfo_t3693012684  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SAMPLEINFO_T3693012684_H
#ifndef AVATARLOADER_T2953159321_H
#define AVATARLOADER_T2953159321_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.Extras.AvatarLoader
struct  AvatarLoader_t2953159321  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject TriLib.Extras.AvatarLoader::CurrentAvatar
	GameObject_t1113636619 * ___CurrentAvatar_4;
	// UnityEngine.RuntimeAnimatorController TriLib.Extras.AvatarLoader::RuntimeAnimatorController
	RuntimeAnimatorController_t2933699135 * ___RuntimeAnimatorController_5;
	// System.Single TriLib.Extras.AvatarLoader::ArmStretch
	float ___ArmStretch_6;
	// System.Single TriLib.Extras.AvatarLoader::FeetSpacing
	float ___FeetSpacing_7;
	// System.Boolean TriLib.Extras.AvatarLoader::HasTranslationDof
	bool ___HasTranslationDof_8;
	// System.Single TriLib.Extras.AvatarLoader::LegStretch
	float ___LegStretch_9;
	// System.Single TriLib.Extras.AvatarLoader::LowerArmTwist
	float ___LowerArmTwist_10;
	// System.Single TriLib.Extras.AvatarLoader::LowerLegTwist
	float ___LowerLegTwist_11;
	// System.Single TriLib.Extras.AvatarLoader::UpperArmTwist
	float ___UpperArmTwist_12;
	// System.Single TriLib.Extras.AvatarLoader::UpperLegTwist
	float ___UpperLegTwist_13;
	// System.Single TriLib.Extras.AvatarLoader::Scale
	float ___Scale_14;
	// System.Single TriLib.Extras.AvatarLoader::HeightOffset
	float ___HeightOffset_15;
	// TriLib.Extras.BoneRelationshipList TriLib.Extras.AvatarLoader::CustomBoneNames
	BoneRelationshipList_t2048306945 * ___CustomBoneNames_16;
	// TriLib.AssetLoaderOptions TriLib.Extras.AvatarLoader::_loaderOptions
	AssetLoaderOptions_t2366605288 * ____loaderOptions_19;

public:
	inline static int32_t get_offset_of_CurrentAvatar_4() { return static_cast<int32_t>(offsetof(AvatarLoader_t2953159321, ___CurrentAvatar_4)); }
	inline GameObject_t1113636619 * get_CurrentAvatar_4() const { return ___CurrentAvatar_4; }
	inline GameObject_t1113636619 ** get_address_of_CurrentAvatar_4() { return &___CurrentAvatar_4; }
	inline void set_CurrentAvatar_4(GameObject_t1113636619 * value)
	{
		___CurrentAvatar_4 = value;
		Il2CppCodeGenWriteBarrier((&___CurrentAvatar_4), value);
	}

	inline static int32_t get_offset_of_RuntimeAnimatorController_5() { return static_cast<int32_t>(offsetof(AvatarLoader_t2953159321, ___RuntimeAnimatorController_5)); }
	inline RuntimeAnimatorController_t2933699135 * get_RuntimeAnimatorController_5() const { return ___RuntimeAnimatorController_5; }
	inline RuntimeAnimatorController_t2933699135 ** get_address_of_RuntimeAnimatorController_5() { return &___RuntimeAnimatorController_5; }
	inline void set_RuntimeAnimatorController_5(RuntimeAnimatorController_t2933699135 * value)
	{
		___RuntimeAnimatorController_5 = value;
		Il2CppCodeGenWriteBarrier((&___RuntimeAnimatorController_5), value);
	}

	inline static int32_t get_offset_of_ArmStretch_6() { return static_cast<int32_t>(offsetof(AvatarLoader_t2953159321, ___ArmStretch_6)); }
	inline float get_ArmStretch_6() const { return ___ArmStretch_6; }
	inline float* get_address_of_ArmStretch_6() { return &___ArmStretch_6; }
	inline void set_ArmStretch_6(float value)
	{
		___ArmStretch_6 = value;
	}

	inline static int32_t get_offset_of_FeetSpacing_7() { return static_cast<int32_t>(offsetof(AvatarLoader_t2953159321, ___FeetSpacing_7)); }
	inline float get_FeetSpacing_7() const { return ___FeetSpacing_7; }
	inline float* get_address_of_FeetSpacing_7() { return &___FeetSpacing_7; }
	inline void set_FeetSpacing_7(float value)
	{
		___FeetSpacing_7 = value;
	}

	inline static int32_t get_offset_of_HasTranslationDof_8() { return static_cast<int32_t>(offsetof(AvatarLoader_t2953159321, ___HasTranslationDof_8)); }
	inline bool get_HasTranslationDof_8() const { return ___HasTranslationDof_8; }
	inline bool* get_address_of_HasTranslationDof_8() { return &___HasTranslationDof_8; }
	inline void set_HasTranslationDof_8(bool value)
	{
		___HasTranslationDof_8 = value;
	}

	inline static int32_t get_offset_of_LegStretch_9() { return static_cast<int32_t>(offsetof(AvatarLoader_t2953159321, ___LegStretch_9)); }
	inline float get_LegStretch_9() const { return ___LegStretch_9; }
	inline float* get_address_of_LegStretch_9() { return &___LegStretch_9; }
	inline void set_LegStretch_9(float value)
	{
		___LegStretch_9 = value;
	}

	inline static int32_t get_offset_of_LowerArmTwist_10() { return static_cast<int32_t>(offsetof(AvatarLoader_t2953159321, ___LowerArmTwist_10)); }
	inline float get_LowerArmTwist_10() const { return ___LowerArmTwist_10; }
	inline float* get_address_of_LowerArmTwist_10() { return &___LowerArmTwist_10; }
	inline void set_LowerArmTwist_10(float value)
	{
		___LowerArmTwist_10 = value;
	}

	inline static int32_t get_offset_of_LowerLegTwist_11() { return static_cast<int32_t>(offsetof(AvatarLoader_t2953159321, ___LowerLegTwist_11)); }
	inline float get_LowerLegTwist_11() const { return ___LowerLegTwist_11; }
	inline float* get_address_of_LowerLegTwist_11() { return &___LowerLegTwist_11; }
	inline void set_LowerLegTwist_11(float value)
	{
		___LowerLegTwist_11 = value;
	}

	inline static int32_t get_offset_of_UpperArmTwist_12() { return static_cast<int32_t>(offsetof(AvatarLoader_t2953159321, ___UpperArmTwist_12)); }
	inline float get_UpperArmTwist_12() const { return ___UpperArmTwist_12; }
	inline float* get_address_of_UpperArmTwist_12() { return &___UpperArmTwist_12; }
	inline void set_UpperArmTwist_12(float value)
	{
		___UpperArmTwist_12 = value;
	}

	inline static int32_t get_offset_of_UpperLegTwist_13() { return static_cast<int32_t>(offsetof(AvatarLoader_t2953159321, ___UpperLegTwist_13)); }
	inline float get_UpperLegTwist_13() const { return ___UpperLegTwist_13; }
	inline float* get_address_of_UpperLegTwist_13() { return &___UpperLegTwist_13; }
	inline void set_UpperLegTwist_13(float value)
	{
		___UpperLegTwist_13 = value;
	}

	inline static int32_t get_offset_of_Scale_14() { return static_cast<int32_t>(offsetof(AvatarLoader_t2953159321, ___Scale_14)); }
	inline float get_Scale_14() const { return ___Scale_14; }
	inline float* get_address_of_Scale_14() { return &___Scale_14; }
	inline void set_Scale_14(float value)
	{
		___Scale_14 = value;
	}

	inline static int32_t get_offset_of_HeightOffset_15() { return static_cast<int32_t>(offsetof(AvatarLoader_t2953159321, ___HeightOffset_15)); }
	inline float get_HeightOffset_15() const { return ___HeightOffset_15; }
	inline float* get_address_of_HeightOffset_15() { return &___HeightOffset_15; }
	inline void set_HeightOffset_15(float value)
	{
		___HeightOffset_15 = value;
	}

	inline static int32_t get_offset_of_CustomBoneNames_16() { return static_cast<int32_t>(offsetof(AvatarLoader_t2953159321, ___CustomBoneNames_16)); }
	inline BoneRelationshipList_t2048306945 * get_CustomBoneNames_16() const { return ___CustomBoneNames_16; }
	inline BoneRelationshipList_t2048306945 ** get_address_of_CustomBoneNames_16() { return &___CustomBoneNames_16; }
	inline void set_CustomBoneNames_16(BoneRelationshipList_t2048306945 * value)
	{
		___CustomBoneNames_16 = value;
		Il2CppCodeGenWriteBarrier((&___CustomBoneNames_16), value);
	}

	inline static int32_t get_offset_of__loaderOptions_19() { return static_cast<int32_t>(offsetof(AvatarLoader_t2953159321, ____loaderOptions_19)); }
	inline AssetLoaderOptions_t2366605288 * get__loaderOptions_19() const { return ____loaderOptions_19; }
	inline AssetLoaderOptions_t2366605288 ** get_address_of__loaderOptions_19() { return &____loaderOptions_19; }
	inline void set__loaderOptions_19(AssetLoaderOptions_t2366605288 * value)
	{
		____loaderOptions_19 = value;
		Il2CppCodeGenWriteBarrier((&____loaderOptions_19), value);
	}
};

struct AvatarLoader_t2953159321_StaticFields
{
public:
	// TriLib.Extras.BoneRelationshipList TriLib.Extras.AvatarLoader::BipedBoneNames
	BoneRelationshipList_t2048306945 * ___BipedBoneNames_17;
	// TriLib.Extras.BoneRelationshipList TriLib.Extras.AvatarLoader::MixamoBoneNames
	BoneRelationshipList_t2048306945 * ___MixamoBoneNames_18;

public:
	inline static int32_t get_offset_of_BipedBoneNames_17() { return static_cast<int32_t>(offsetof(AvatarLoader_t2953159321_StaticFields, ___BipedBoneNames_17)); }
	inline BoneRelationshipList_t2048306945 * get_BipedBoneNames_17() const { return ___BipedBoneNames_17; }
	inline BoneRelationshipList_t2048306945 ** get_address_of_BipedBoneNames_17() { return &___BipedBoneNames_17; }
	inline void set_BipedBoneNames_17(BoneRelationshipList_t2048306945 * value)
	{
		___BipedBoneNames_17 = value;
		Il2CppCodeGenWriteBarrier((&___BipedBoneNames_17), value);
	}

	inline static int32_t get_offset_of_MixamoBoneNames_18() { return static_cast<int32_t>(offsetof(AvatarLoader_t2953159321_StaticFields, ___MixamoBoneNames_18)); }
	inline BoneRelationshipList_t2048306945 * get_MixamoBoneNames_18() const { return ___MixamoBoneNames_18; }
	inline BoneRelationshipList_t2048306945 ** get_address_of_MixamoBoneNames_18() { return &___MixamoBoneNames_18; }
	inline void set_MixamoBoneNames_18(BoneRelationshipList_t2048306945 * value)
	{
		___MixamoBoneNames_18 = value;
		Il2CppCodeGenWriteBarrier((&___MixamoBoneNames_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AVATARLOADER_T2953159321_H
#ifndef AVATARLOADERSAMPLE_T509248141_H
#define AVATARLOADERSAMPLE_T509248141_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.Extras.AvatarLoaderSample
struct  AvatarLoaderSample_t509248141  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject TriLib.Extras.AvatarLoaderSample::FreeLookCamPrefab
	GameObject_t1113636619 * ___FreeLookCamPrefab_4;
	// UnityEngine.GameObject TriLib.Extras.AvatarLoaderSample::ThirdPersonControllerPrefab
	GameObject_t1113636619 * ___ThirdPersonControllerPrefab_5;
	// UnityEngine.GameObject TriLib.Extras.AvatarLoaderSample::ActiveCameraGameObject
	GameObject_t1113636619 * ___ActiveCameraGameObject_6;
	// System.String TriLib.Extras.AvatarLoaderSample::ModelsDirectory
	String_t* ___ModelsDirectory_7;
	// System.String[] TriLib.Extras.AvatarLoaderSample::_files
	StringU5BU5D_t1281789340* ____files_8;
	// UnityEngine.Rect TriLib.Extras.AvatarLoaderSample::_windowRect
	Rect_t2360479859  ____windowRect_9;
	// UnityEngine.Vector3 TriLib.Extras.AvatarLoaderSample::_scrollPosition
	Vector3_t3722313464  ____scrollPosition_10;
	// TriLib.Extras.AvatarLoader TriLib.Extras.AvatarLoaderSample::_avatarLoader
	AvatarLoader_t2953159321 * ____avatarLoader_11;

public:
	inline static int32_t get_offset_of_FreeLookCamPrefab_4() { return static_cast<int32_t>(offsetof(AvatarLoaderSample_t509248141, ___FreeLookCamPrefab_4)); }
	inline GameObject_t1113636619 * get_FreeLookCamPrefab_4() const { return ___FreeLookCamPrefab_4; }
	inline GameObject_t1113636619 ** get_address_of_FreeLookCamPrefab_4() { return &___FreeLookCamPrefab_4; }
	inline void set_FreeLookCamPrefab_4(GameObject_t1113636619 * value)
	{
		___FreeLookCamPrefab_4 = value;
		Il2CppCodeGenWriteBarrier((&___FreeLookCamPrefab_4), value);
	}

	inline static int32_t get_offset_of_ThirdPersonControllerPrefab_5() { return static_cast<int32_t>(offsetof(AvatarLoaderSample_t509248141, ___ThirdPersonControllerPrefab_5)); }
	inline GameObject_t1113636619 * get_ThirdPersonControllerPrefab_5() const { return ___ThirdPersonControllerPrefab_5; }
	inline GameObject_t1113636619 ** get_address_of_ThirdPersonControllerPrefab_5() { return &___ThirdPersonControllerPrefab_5; }
	inline void set_ThirdPersonControllerPrefab_5(GameObject_t1113636619 * value)
	{
		___ThirdPersonControllerPrefab_5 = value;
		Il2CppCodeGenWriteBarrier((&___ThirdPersonControllerPrefab_5), value);
	}

	inline static int32_t get_offset_of_ActiveCameraGameObject_6() { return static_cast<int32_t>(offsetof(AvatarLoaderSample_t509248141, ___ActiveCameraGameObject_6)); }
	inline GameObject_t1113636619 * get_ActiveCameraGameObject_6() const { return ___ActiveCameraGameObject_6; }
	inline GameObject_t1113636619 ** get_address_of_ActiveCameraGameObject_6() { return &___ActiveCameraGameObject_6; }
	inline void set_ActiveCameraGameObject_6(GameObject_t1113636619 * value)
	{
		___ActiveCameraGameObject_6 = value;
		Il2CppCodeGenWriteBarrier((&___ActiveCameraGameObject_6), value);
	}

	inline static int32_t get_offset_of_ModelsDirectory_7() { return static_cast<int32_t>(offsetof(AvatarLoaderSample_t509248141, ___ModelsDirectory_7)); }
	inline String_t* get_ModelsDirectory_7() const { return ___ModelsDirectory_7; }
	inline String_t** get_address_of_ModelsDirectory_7() { return &___ModelsDirectory_7; }
	inline void set_ModelsDirectory_7(String_t* value)
	{
		___ModelsDirectory_7 = value;
		Il2CppCodeGenWriteBarrier((&___ModelsDirectory_7), value);
	}

	inline static int32_t get_offset_of__files_8() { return static_cast<int32_t>(offsetof(AvatarLoaderSample_t509248141, ____files_8)); }
	inline StringU5BU5D_t1281789340* get__files_8() const { return ____files_8; }
	inline StringU5BU5D_t1281789340** get_address_of__files_8() { return &____files_8; }
	inline void set__files_8(StringU5BU5D_t1281789340* value)
	{
		____files_8 = value;
		Il2CppCodeGenWriteBarrier((&____files_8), value);
	}

	inline static int32_t get_offset_of__windowRect_9() { return static_cast<int32_t>(offsetof(AvatarLoaderSample_t509248141, ____windowRect_9)); }
	inline Rect_t2360479859  get__windowRect_9() const { return ____windowRect_9; }
	inline Rect_t2360479859 * get_address_of__windowRect_9() { return &____windowRect_9; }
	inline void set__windowRect_9(Rect_t2360479859  value)
	{
		____windowRect_9 = value;
	}

	inline static int32_t get_offset_of__scrollPosition_10() { return static_cast<int32_t>(offsetof(AvatarLoaderSample_t509248141, ____scrollPosition_10)); }
	inline Vector3_t3722313464  get__scrollPosition_10() const { return ____scrollPosition_10; }
	inline Vector3_t3722313464 * get_address_of__scrollPosition_10() { return &____scrollPosition_10; }
	inline void set__scrollPosition_10(Vector3_t3722313464  value)
	{
		____scrollPosition_10 = value;
	}

	inline static int32_t get_offset_of__avatarLoader_11() { return static_cast<int32_t>(offsetof(AvatarLoaderSample_t509248141, ____avatarLoader_11)); }
	inline AvatarLoader_t2953159321 * get__avatarLoader_11() const { return ____avatarLoader_11; }
	inline AvatarLoader_t2953159321 ** get_address_of__avatarLoader_11() { return &____avatarLoader_11; }
	inline void set__avatarLoader_11(AvatarLoader_t2953159321 * value)
	{
		____avatarLoader_11 = value;
		Il2CppCodeGenWriteBarrier((&____avatarLoader_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AVATARLOADERSAMPLE_T509248141_H
#ifndef VUFORIAMONOBEHAVIOUR_T1150221792_H
#define VUFORIAMONOBEHAVIOUR_T1150221792_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VuforiaMonoBehaviour
struct  VuforiaMonoBehaviour_t1150221792  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VUFORIAMONOBEHAVIOUR_T1150221792_H
#ifndef ITWEEN_T770867771_H
#define ITWEEN_T770867771_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// iTween
struct  iTween_t770867771  : public MonoBehaviour_t3962482529
{
public:
	// System.String iTween::id
	String_t* ___id_5;
	// System.String iTween::type
	String_t* ___type_6;
	// System.String iTween::method
	String_t* ___method_7;
	// iTween/EaseType iTween::easeType
	int32_t ___easeType_8;
	// System.Single iTween::time
	float ___time_9;
	// System.Single iTween::delay
	float ___delay_10;
	// iTween/LoopType iTween::loopType
	int32_t ___loopType_11;
	// System.Boolean iTween::isRunning
	bool ___isRunning_12;
	// System.Boolean iTween::isPaused
	bool ___isPaused_13;
	// System.String iTween::_name
	String_t* ____name_14;
	// System.Single iTween::runningTime
	float ___runningTime_15;
	// System.Single iTween::percentage
	float ___percentage_16;
	// System.Single iTween::delayStarted
	float ___delayStarted_17;
	// System.Boolean iTween::kinematic
	bool ___kinematic_18;
	// System.Boolean iTween::isLocal
	bool ___isLocal_19;
	// System.Boolean iTween::loop
	bool ___loop_20;
	// System.Boolean iTween::reverse
	bool ___reverse_21;
	// System.Boolean iTween::wasPaused
	bool ___wasPaused_22;
	// System.Boolean iTween::physics
	bool ___physics_23;
	// System.Collections.Hashtable iTween::tweenArguments
	Hashtable_t1853889766 * ___tweenArguments_24;
	// UnityEngine.Space iTween::space
	int32_t ___space_25;
	// iTween/EasingFunction iTween::ease
	EasingFunction_t2767217938 * ___ease_26;
	// iTween/ApplyTween iTween::apply
	ApplyTween_t3327999347 * ___apply_27;
	// UnityEngine.AudioSource iTween::audioSource
	AudioSource_t3935305588 * ___audioSource_28;
	// UnityEngine.Vector3[] iTween::vector3s
	Vector3U5BU5D_t1718750761* ___vector3s_29;
	// UnityEngine.Vector2[] iTween::vector2s
	Vector2U5BU5D_t1457185986* ___vector2s_30;
	// UnityEngine.Color[0...,0...] iTween::colors
	ColorU5B0___U2C0___U5D_t941916414* ___colors_31;
	// System.Single[] iTween::floats
	SingleU5BU5D_t1444911251* ___floats_32;
	// UnityEngine.Rect[] iTween::rects
	RectU5BU5D_t2936723554* ___rects_33;
	// iTween/CRSpline iTween::path
	CRSpline_t2815350084 * ___path_34;
	// UnityEngine.Vector3 iTween::preUpdate
	Vector3_t3722313464  ___preUpdate_35;
	// UnityEngine.Vector3 iTween::postUpdate
	Vector3_t3722313464  ___postUpdate_36;
	// iTween/NamedValueColor iTween::namedcolorvalue
	int32_t ___namedcolorvalue_37;
	// System.Single iTween::lastRealTime
	float ___lastRealTime_38;
	// System.Boolean iTween::useRealTime
	bool ___useRealTime_39;
	// UnityEngine.Transform iTween::thisTransform
	Transform_t3600365921 * ___thisTransform_40;

public:
	inline static int32_t get_offset_of_id_5() { return static_cast<int32_t>(offsetof(iTween_t770867771, ___id_5)); }
	inline String_t* get_id_5() const { return ___id_5; }
	inline String_t** get_address_of_id_5() { return &___id_5; }
	inline void set_id_5(String_t* value)
	{
		___id_5 = value;
		Il2CppCodeGenWriteBarrier((&___id_5), value);
	}

	inline static int32_t get_offset_of_type_6() { return static_cast<int32_t>(offsetof(iTween_t770867771, ___type_6)); }
	inline String_t* get_type_6() const { return ___type_6; }
	inline String_t** get_address_of_type_6() { return &___type_6; }
	inline void set_type_6(String_t* value)
	{
		___type_6 = value;
		Il2CppCodeGenWriteBarrier((&___type_6), value);
	}

	inline static int32_t get_offset_of_method_7() { return static_cast<int32_t>(offsetof(iTween_t770867771, ___method_7)); }
	inline String_t* get_method_7() const { return ___method_7; }
	inline String_t** get_address_of_method_7() { return &___method_7; }
	inline void set_method_7(String_t* value)
	{
		___method_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_7), value);
	}

	inline static int32_t get_offset_of_easeType_8() { return static_cast<int32_t>(offsetof(iTween_t770867771, ___easeType_8)); }
	inline int32_t get_easeType_8() const { return ___easeType_8; }
	inline int32_t* get_address_of_easeType_8() { return &___easeType_8; }
	inline void set_easeType_8(int32_t value)
	{
		___easeType_8 = value;
	}

	inline static int32_t get_offset_of_time_9() { return static_cast<int32_t>(offsetof(iTween_t770867771, ___time_9)); }
	inline float get_time_9() const { return ___time_9; }
	inline float* get_address_of_time_9() { return &___time_9; }
	inline void set_time_9(float value)
	{
		___time_9 = value;
	}

	inline static int32_t get_offset_of_delay_10() { return static_cast<int32_t>(offsetof(iTween_t770867771, ___delay_10)); }
	inline float get_delay_10() const { return ___delay_10; }
	inline float* get_address_of_delay_10() { return &___delay_10; }
	inline void set_delay_10(float value)
	{
		___delay_10 = value;
	}

	inline static int32_t get_offset_of_loopType_11() { return static_cast<int32_t>(offsetof(iTween_t770867771, ___loopType_11)); }
	inline int32_t get_loopType_11() const { return ___loopType_11; }
	inline int32_t* get_address_of_loopType_11() { return &___loopType_11; }
	inline void set_loopType_11(int32_t value)
	{
		___loopType_11 = value;
	}

	inline static int32_t get_offset_of_isRunning_12() { return static_cast<int32_t>(offsetof(iTween_t770867771, ___isRunning_12)); }
	inline bool get_isRunning_12() const { return ___isRunning_12; }
	inline bool* get_address_of_isRunning_12() { return &___isRunning_12; }
	inline void set_isRunning_12(bool value)
	{
		___isRunning_12 = value;
	}

	inline static int32_t get_offset_of_isPaused_13() { return static_cast<int32_t>(offsetof(iTween_t770867771, ___isPaused_13)); }
	inline bool get_isPaused_13() const { return ___isPaused_13; }
	inline bool* get_address_of_isPaused_13() { return &___isPaused_13; }
	inline void set_isPaused_13(bool value)
	{
		___isPaused_13 = value;
	}

	inline static int32_t get_offset_of__name_14() { return static_cast<int32_t>(offsetof(iTween_t770867771, ____name_14)); }
	inline String_t* get__name_14() const { return ____name_14; }
	inline String_t** get_address_of__name_14() { return &____name_14; }
	inline void set__name_14(String_t* value)
	{
		____name_14 = value;
		Il2CppCodeGenWriteBarrier((&____name_14), value);
	}

	inline static int32_t get_offset_of_runningTime_15() { return static_cast<int32_t>(offsetof(iTween_t770867771, ___runningTime_15)); }
	inline float get_runningTime_15() const { return ___runningTime_15; }
	inline float* get_address_of_runningTime_15() { return &___runningTime_15; }
	inline void set_runningTime_15(float value)
	{
		___runningTime_15 = value;
	}

	inline static int32_t get_offset_of_percentage_16() { return static_cast<int32_t>(offsetof(iTween_t770867771, ___percentage_16)); }
	inline float get_percentage_16() const { return ___percentage_16; }
	inline float* get_address_of_percentage_16() { return &___percentage_16; }
	inline void set_percentage_16(float value)
	{
		___percentage_16 = value;
	}

	inline static int32_t get_offset_of_delayStarted_17() { return static_cast<int32_t>(offsetof(iTween_t770867771, ___delayStarted_17)); }
	inline float get_delayStarted_17() const { return ___delayStarted_17; }
	inline float* get_address_of_delayStarted_17() { return &___delayStarted_17; }
	inline void set_delayStarted_17(float value)
	{
		___delayStarted_17 = value;
	}

	inline static int32_t get_offset_of_kinematic_18() { return static_cast<int32_t>(offsetof(iTween_t770867771, ___kinematic_18)); }
	inline bool get_kinematic_18() const { return ___kinematic_18; }
	inline bool* get_address_of_kinematic_18() { return &___kinematic_18; }
	inline void set_kinematic_18(bool value)
	{
		___kinematic_18 = value;
	}

	inline static int32_t get_offset_of_isLocal_19() { return static_cast<int32_t>(offsetof(iTween_t770867771, ___isLocal_19)); }
	inline bool get_isLocal_19() const { return ___isLocal_19; }
	inline bool* get_address_of_isLocal_19() { return &___isLocal_19; }
	inline void set_isLocal_19(bool value)
	{
		___isLocal_19 = value;
	}

	inline static int32_t get_offset_of_loop_20() { return static_cast<int32_t>(offsetof(iTween_t770867771, ___loop_20)); }
	inline bool get_loop_20() const { return ___loop_20; }
	inline bool* get_address_of_loop_20() { return &___loop_20; }
	inline void set_loop_20(bool value)
	{
		___loop_20 = value;
	}

	inline static int32_t get_offset_of_reverse_21() { return static_cast<int32_t>(offsetof(iTween_t770867771, ___reverse_21)); }
	inline bool get_reverse_21() const { return ___reverse_21; }
	inline bool* get_address_of_reverse_21() { return &___reverse_21; }
	inline void set_reverse_21(bool value)
	{
		___reverse_21 = value;
	}

	inline static int32_t get_offset_of_wasPaused_22() { return static_cast<int32_t>(offsetof(iTween_t770867771, ___wasPaused_22)); }
	inline bool get_wasPaused_22() const { return ___wasPaused_22; }
	inline bool* get_address_of_wasPaused_22() { return &___wasPaused_22; }
	inline void set_wasPaused_22(bool value)
	{
		___wasPaused_22 = value;
	}

	inline static int32_t get_offset_of_physics_23() { return static_cast<int32_t>(offsetof(iTween_t770867771, ___physics_23)); }
	inline bool get_physics_23() const { return ___physics_23; }
	inline bool* get_address_of_physics_23() { return &___physics_23; }
	inline void set_physics_23(bool value)
	{
		___physics_23 = value;
	}

	inline static int32_t get_offset_of_tweenArguments_24() { return static_cast<int32_t>(offsetof(iTween_t770867771, ___tweenArguments_24)); }
	inline Hashtable_t1853889766 * get_tweenArguments_24() const { return ___tweenArguments_24; }
	inline Hashtable_t1853889766 ** get_address_of_tweenArguments_24() { return &___tweenArguments_24; }
	inline void set_tweenArguments_24(Hashtable_t1853889766 * value)
	{
		___tweenArguments_24 = value;
		Il2CppCodeGenWriteBarrier((&___tweenArguments_24), value);
	}

	inline static int32_t get_offset_of_space_25() { return static_cast<int32_t>(offsetof(iTween_t770867771, ___space_25)); }
	inline int32_t get_space_25() const { return ___space_25; }
	inline int32_t* get_address_of_space_25() { return &___space_25; }
	inline void set_space_25(int32_t value)
	{
		___space_25 = value;
	}

	inline static int32_t get_offset_of_ease_26() { return static_cast<int32_t>(offsetof(iTween_t770867771, ___ease_26)); }
	inline EasingFunction_t2767217938 * get_ease_26() const { return ___ease_26; }
	inline EasingFunction_t2767217938 ** get_address_of_ease_26() { return &___ease_26; }
	inline void set_ease_26(EasingFunction_t2767217938 * value)
	{
		___ease_26 = value;
		Il2CppCodeGenWriteBarrier((&___ease_26), value);
	}

	inline static int32_t get_offset_of_apply_27() { return static_cast<int32_t>(offsetof(iTween_t770867771, ___apply_27)); }
	inline ApplyTween_t3327999347 * get_apply_27() const { return ___apply_27; }
	inline ApplyTween_t3327999347 ** get_address_of_apply_27() { return &___apply_27; }
	inline void set_apply_27(ApplyTween_t3327999347 * value)
	{
		___apply_27 = value;
		Il2CppCodeGenWriteBarrier((&___apply_27), value);
	}

	inline static int32_t get_offset_of_audioSource_28() { return static_cast<int32_t>(offsetof(iTween_t770867771, ___audioSource_28)); }
	inline AudioSource_t3935305588 * get_audioSource_28() const { return ___audioSource_28; }
	inline AudioSource_t3935305588 ** get_address_of_audioSource_28() { return &___audioSource_28; }
	inline void set_audioSource_28(AudioSource_t3935305588 * value)
	{
		___audioSource_28 = value;
		Il2CppCodeGenWriteBarrier((&___audioSource_28), value);
	}

	inline static int32_t get_offset_of_vector3s_29() { return static_cast<int32_t>(offsetof(iTween_t770867771, ___vector3s_29)); }
	inline Vector3U5BU5D_t1718750761* get_vector3s_29() const { return ___vector3s_29; }
	inline Vector3U5BU5D_t1718750761** get_address_of_vector3s_29() { return &___vector3s_29; }
	inline void set_vector3s_29(Vector3U5BU5D_t1718750761* value)
	{
		___vector3s_29 = value;
		Il2CppCodeGenWriteBarrier((&___vector3s_29), value);
	}

	inline static int32_t get_offset_of_vector2s_30() { return static_cast<int32_t>(offsetof(iTween_t770867771, ___vector2s_30)); }
	inline Vector2U5BU5D_t1457185986* get_vector2s_30() const { return ___vector2s_30; }
	inline Vector2U5BU5D_t1457185986** get_address_of_vector2s_30() { return &___vector2s_30; }
	inline void set_vector2s_30(Vector2U5BU5D_t1457185986* value)
	{
		___vector2s_30 = value;
		Il2CppCodeGenWriteBarrier((&___vector2s_30), value);
	}

	inline static int32_t get_offset_of_colors_31() { return static_cast<int32_t>(offsetof(iTween_t770867771, ___colors_31)); }
	inline ColorU5B0___U2C0___U5D_t941916414* get_colors_31() const { return ___colors_31; }
	inline ColorU5B0___U2C0___U5D_t941916414** get_address_of_colors_31() { return &___colors_31; }
	inline void set_colors_31(ColorU5B0___U2C0___U5D_t941916414* value)
	{
		___colors_31 = value;
		Il2CppCodeGenWriteBarrier((&___colors_31), value);
	}

	inline static int32_t get_offset_of_floats_32() { return static_cast<int32_t>(offsetof(iTween_t770867771, ___floats_32)); }
	inline SingleU5BU5D_t1444911251* get_floats_32() const { return ___floats_32; }
	inline SingleU5BU5D_t1444911251** get_address_of_floats_32() { return &___floats_32; }
	inline void set_floats_32(SingleU5BU5D_t1444911251* value)
	{
		___floats_32 = value;
		Il2CppCodeGenWriteBarrier((&___floats_32), value);
	}

	inline static int32_t get_offset_of_rects_33() { return static_cast<int32_t>(offsetof(iTween_t770867771, ___rects_33)); }
	inline RectU5BU5D_t2936723554* get_rects_33() const { return ___rects_33; }
	inline RectU5BU5D_t2936723554** get_address_of_rects_33() { return &___rects_33; }
	inline void set_rects_33(RectU5BU5D_t2936723554* value)
	{
		___rects_33 = value;
		Il2CppCodeGenWriteBarrier((&___rects_33), value);
	}

	inline static int32_t get_offset_of_path_34() { return static_cast<int32_t>(offsetof(iTween_t770867771, ___path_34)); }
	inline CRSpline_t2815350084 * get_path_34() const { return ___path_34; }
	inline CRSpline_t2815350084 ** get_address_of_path_34() { return &___path_34; }
	inline void set_path_34(CRSpline_t2815350084 * value)
	{
		___path_34 = value;
		Il2CppCodeGenWriteBarrier((&___path_34), value);
	}

	inline static int32_t get_offset_of_preUpdate_35() { return static_cast<int32_t>(offsetof(iTween_t770867771, ___preUpdate_35)); }
	inline Vector3_t3722313464  get_preUpdate_35() const { return ___preUpdate_35; }
	inline Vector3_t3722313464 * get_address_of_preUpdate_35() { return &___preUpdate_35; }
	inline void set_preUpdate_35(Vector3_t3722313464  value)
	{
		___preUpdate_35 = value;
	}

	inline static int32_t get_offset_of_postUpdate_36() { return static_cast<int32_t>(offsetof(iTween_t770867771, ___postUpdate_36)); }
	inline Vector3_t3722313464  get_postUpdate_36() const { return ___postUpdate_36; }
	inline Vector3_t3722313464 * get_address_of_postUpdate_36() { return &___postUpdate_36; }
	inline void set_postUpdate_36(Vector3_t3722313464  value)
	{
		___postUpdate_36 = value;
	}

	inline static int32_t get_offset_of_namedcolorvalue_37() { return static_cast<int32_t>(offsetof(iTween_t770867771, ___namedcolorvalue_37)); }
	inline int32_t get_namedcolorvalue_37() const { return ___namedcolorvalue_37; }
	inline int32_t* get_address_of_namedcolorvalue_37() { return &___namedcolorvalue_37; }
	inline void set_namedcolorvalue_37(int32_t value)
	{
		___namedcolorvalue_37 = value;
	}

	inline static int32_t get_offset_of_lastRealTime_38() { return static_cast<int32_t>(offsetof(iTween_t770867771, ___lastRealTime_38)); }
	inline float get_lastRealTime_38() const { return ___lastRealTime_38; }
	inline float* get_address_of_lastRealTime_38() { return &___lastRealTime_38; }
	inline void set_lastRealTime_38(float value)
	{
		___lastRealTime_38 = value;
	}

	inline static int32_t get_offset_of_useRealTime_39() { return static_cast<int32_t>(offsetof(iTween_t770867771, ___useRealTime_39)); }
	inline bool get_useRealTime_39() const { return ___useRealTime_39; }
	inline bool* get_address_of_useRealTime_39() { return &___useRealTime_39; }
	inline void set_useRealTime_39(bool value)
	{
		___useRealTime_39 = value;
	}

	inline static int32_t get_offset_of_thisTransform_40() { return static_cast<int32_t>(offsetof(iTween_t770867771, ___thisTransform_40)); }
	inline Transform_t3600365921 * get_thisTransform_40() const { return ___thisTransform_40; }
	inline Transform_t3600365921 ** get_address_of_thisTransform_40() { return &___thisTransform_40; }
	inline void set_thisTransform_40(Transform_t3600365921 * value)
	{
		___thisTransform_40 = value;
		Il2CppCodeGenWriteBarrier((&___thisTransform_40), value);
	}
};

struct iTween_t770867771_StaticFields
{
public:
	// System.Collections.Generic.List`1<System.Collections.Hashtable> iTween::tweens
	List_1_t3325964508 * ___tweens_4;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> iTween::<>f__switch$map0
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map0_41;

public:
	inline static int32_t get_offset_of_tweens_4() { return static_cast<int32_t>(offsetof(iTween_t770867771_StaticFields, ___tweens_4)); }
	inline List_1_t3325964508 * get_tweens_4() const { return ___tweens_4; }
	inline List_1_t3325964508 ** get_address_of_tweens_4() { return &___tweens_4; }
	inline void set_tweens_4(List_1_t3325964508 * value)
	{
		___tweens_4 = value;
		Il2CppCodeGenWriteBarrier((&___tweens_4), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map0_41() { return static_cast<int32_t>(offsetof(iTween_t770867771_StaticFields, ___U3CU3Ef__switchU24map0_41)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map0_41() const { return ___U3CU3Ef__switchU24map0_41; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map0_41() { return &___U3CU3Ef__switchU24map0_41; }
	inline void set_U3CU3Ef__switchU24map0_41(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map0_41 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map0_41), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ITWEEN_T770867771_H
#ifndef DEFAULTINITIALIZATIONERRORHANDLER_T3109936861_H
#define DEFAULTINITIALIZATIONERRORHANDLER_T3109936861_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DefaultInitializationErrorHandler
struct  DefaultInitializationErrorHandler_t3109936861  : public VuforiaMonoBehaviour_t1150221792
{
public:
	// System.String DefaultInitializationErrorHandler::mErrorText
	String_t* ___mErrorText_4;
	// System.Boolean DefaultInitializationErrorHandler::mErrorOccurred
	bool ___mErrorOccurred_5;
	// UnityEngine.GUIStyle DefaultInitializationErrorHandler::bodyStyle
	GUIStyle_t3956901511 * ___bodyStyle_7;
	// UnityEngine.GUIStyle DefaultInitializationErrorHandler::headerStyle
	GUIStyle_t3956901511 * ___headerStyle_8;
	// UnityEngine.GUIStyle DefaultInitializationErrorHandler::footerStyle
	GUIStyle_t3956901511 * ___footerStyle_9;
	// UnityEngine.Texture2D DefaultInitializationErrorHandler::bodyTexture
	Texture2D_t3840446185 * ___bodyTexture_10;
	// UnityEngine.Texture2D DefaultInitializationErrorHandler::headerTexture
	Texture2D_t3840446185 * ___headerTexture_11;
	// UnityEngine.Texture2D DefaultInitializationErrorHandler::footerTexture
	Texture2D_t3840446185 * ___footerTexture_12;

public:
	inline static int32_t get_offset_of_mErrorText_4() { return static_cast<int32_t>(offsetof(DefaultInitializationErrorHandler_t3109936861, ___mErrorText_4)); }
	inline String_t* get_mErrorText_4() const { return ___mErrorText_4; }
	inline String_t** get_address_of_mErrorText_4() { return &___mErrorText_4; }
	inline void set_mErrorText_4(String_t* value)
	{
		___mErrorText_4 = value;
		Il2CppCodeGenWriteBarrier((&___mErrorText_4), value);
	}

	inline static int32_t get_offset_of_mErrorOccurred_5() { return static_cast<int32_t>(offsetof(DefaultInitializationErrorHandler_t3109936861, ___mErrorOccurred_5)); }
	inline bool get_mErrorOccurred_5() const { return ___mErrorOccurred_5; }
	inline bool* get_address_of_mErrorOccurred_5() { return &___mErrorOccurred_5; }
	inline void set_mErrorOccurred_5(bool value)
	{
		___mErrorOccurred_5 = value;
	}

	inline static int32_t get_offset_of_bodyStyle_7() { return static_cast<int32_t>(offsetof(DefaultInitializationErrorHandler_t3109936861, ___bodyStyle_7)); }
	inline GUIStyle_t3956901511 * get_bodyStyle_7() const { return ___bodyStyle_7; }
	inline GUIStyle_t3956901511 ** get_address_of_bodyStyle_7() { return &___bodyStyle_7; }
	inline void set_bodyStyle_7(GUIStyle_t3956901511 * value)
	{
		___bodyStyle_7 = value;
		Il2CppCodeGenWriteBarrier((&___bodyStyle_7), value);
	}

	inline static int32_t get_offset_of_headerStyle_8() { return static_cast<int32_t>(offsetof(DefaultInitializationErrorHandler_t3109936861, ___headerStyle_8)); }
	inline GUIStyle_t3956901511 * get_headerStyle_8() const { return ___headerStyle_8; }
	inline GUIStyle_t3956901511 ** get_address_of_headerStyle_8() { return &___headerStyle_8; }
	inline void set_headerStyle_8(GUIStyle_t3956901511 * value)
	{
		___headerStyle_8 = value;
		Il2CppCodeGenWriteBarrier((&___headerStyle_8), value);
	}

	inline static int32_t get_offset_of_footerStyle_9() { return static_cast<int32_t>(offsetof(DefaultInitializationErrorHandler_t3109936861, ___footerStyle_9)); }
	inline GUIStyle_t3956901511 * get_footerStyle_9() const { return ___footerStyle_9; }
	inline GUIStyle_t3956901511 ** get_address_of_footerStyle_9() { return &___footerStyle_9; }
	inline void set_footerStyle_9(GUIStyle_t3956901511 * value)
	{
		___footerStyle_9 = value;
		Il2CppCodeGenWriteBarrier((&___footerStyle_9), value);
	}

	inline static int32_t get_offset_of_bodyTexture_10() { return static_cast<int32_t>(offsetof(DefaultInitializationErrorHandler_t3109936861, ___bodyTexture_10)); }
	inline Texture2D_t3840446185 * get_bodyTexture_10() const { return ___bodyTexture_10; }
	inline Texture2D_t3840446185 ** get_address_of_bodyTexture_10() { return &___bodyTexture_10; }
	inline void set_bodyTexture_10(Texture2D_t3840446185 * value)
	{
		___bodyTexture_10 = value;
		Il2CppCodeGenWriteBarrier((&___bodyTexture_10), value);
	}

	inline static int32_t get_offset_of_headerTexture_11() { return static_cast<int32_t>(offsetof(DefaultInitializationErrorHandler_t3109936861, ___headerTexture_11)); }
	inline Texture2D_t3840446185 * get_headerTexture_11() const { return ___headerTexture_11; }
	inline Texture2D_t3840446185 ** get_address_of_headerTexture_11() { return &___headerTexture_11; }
	inline void set_headerTexture_11(Texture2D_t3840446185 * value)
	{
		___headerTexture_11 = value;
		Il2CppCodeGenWriteBarrier((&___headerTexture_11), value);
	}

	inline static int32_t get_offset_of_footerTexture_12() { return static_cast<int32_t>(offsetof(DefaultInitializationErrorHandler_t3109936861, ___footerTexture_12)); }
	inline Texture2D_t3840446185 * get_footerTexture_12() const { return ___footerTexture_12; }
	inline Texture2D_t3840446185 ** get_address_of_footerTexture_12() { return &___footerTexture_12; }
	inline void set_footerTexture_12(Texture2D_t3840446185 * value)
	{
		___footerTexture_12 = value;
		Il2CppCodeGenWriteBarrier((&___footerTexture_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTINITIALIZATIONERRORHANDLER_T3109936861_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3100 = { sizeof (AssimpPostProcessSteps_t3704598218)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3100[27] = 
{
	AssimpPostProcessSteps_t3704598218::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3101 = { sizeof (AssimpProcessPreset_t3828703936), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3101[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3102 = { sizeof (AssimpInterop_t74186816), -1, sizeof(AssimpInterop_t74186816_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3102[5] = 
{
	0,
	0,
	0,
	AssimpInterop_t74186816_StaticFields::get_offset_of_Is32Bits_3(),
	AssimpInterop_t74186816_StaticFields::get_offset_of_IntSize_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3103 = { sizeof (CameraExtensions_t808704216), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3104 = { sizeof (FileUtils_t3396084117), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3105 = { sizeof (MatrixExtensions_t886005937), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3106 = { sizeof (StringUtils_t4119127951), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3107 = { sizeof (TextureCompression_t1528154288)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3107[4] = 
{
	TextureCompression_t1528154288::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3108 = { sizeof (TextureLoadHandle_t2346000395), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3109 = { sizeof (Texture2DUtils_t607689861), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3110 = { sizeof (TransformExtensions_t817912662), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3111 = { sizeof (AvatarLoaderSample_t509248141), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3111[8] = 
{
	AvatarLoaderSample_t509248141::get_offset_of_FreeLookCamPrefab_4(),
	AvatarLoaderSample_t509248141::get_offset_of_ThirdPersonControllerPrefab_5(),
	AvatarLoaderSample_t509248141::get_offset_of_ActiveCameraGameObject_6(),
	AvatarLoaderSample_t509248141::get_offset_of_ModelsDirectory_7(),
	AvatarLoaderSample_t509248141::get_offset_of__files_8(),
	AvatarLoaderSample_t509248141::get_offset_of__windowRect_9(),
	AvatarLoaderSample_t509248141::get_offset_of__scrollPosition_10(),
	AvatarLoaderSample_t509248141::get_offset_of__avatarLoader_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3112 = { sizeof (U3CStartU3Ec__AnonStorey0_t1727939190), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3112[1] = 
{
	U3CStartU3Ec__AnonStorey0_t1727939190::get_offset_of_supportedExtensions_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3113 = { sizeof (AvatarLoader_t2953159321), -1, sizeof(AvatarLoader_t2953159321_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3113[16] = 
{
	AvatarLoader_t2953159321::get_offset_of_CurrentAvatar_4(),
	AvatarLoader_t2953159321::get_offset_of_RuntimeAnimatorController_5(),
	AvatarLoader_t2953159321::get_offset_of_ArmStretch_6(),
	AvatarLoader_t2953159321::get_offset_of_FeetSpacing_7(),
	AvatarLoader_t2953159321::get_offset_of_HasTranslationDof_8(),
	AvatarLoader_t2953159321::get_offset_of_LegStretch_9(),
	AvatarLoader_t2953159321::get_offset_of_LowerArmTwist_10(),
	AvatarLoader_t2953159321::get_offset_of_LowerLegTwist_11(),
	AvatarLoader_t2953159321::get_offset_of_UpperArmTwist_12(),
	AvatarLoader_t2953159321::get_offset_of_UpperLegTwist_13(),
	AvatarLoader_t2953159321::get_offset_of_Scale_14(),
	AvatarLoader_t2953159321::get_offset_of_HeightOffset_15(),
	AvatarLoader_t2953159321::get_offset_of_CustomBoneNames_16(),
	AvatarLoader_t2953159321_StaticFields::get_offset_of_BipedBoneNames_17(),
	AvatarLoader_t2953159321_StaticFields::get_offset_of_MixamoBoneNames_18(),
	AvatarLoader_t2953159321::get_offset_of__loaderOptions_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3114 = { sizeof (BoneRelationship_t3603730879), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3114[3] = 
{
	BoneRelationship_t3603730879::get_offset_of_HumanBone_0(),
	BoneRelationship_t3603730879::get_offset_of_BoneName_1(),
	BoneRelationship_t3603730879::get_offset_of_Optional_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3115 = { sizeof (BoneRelationshipList_t2048306945), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3115[1] = 
{
	BoneRelationshipList_t2048306945::get_offset_of__relationships_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3116 = { sizeof (DefaultInitializationErrorHandler_t3109936861), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3116[9] = 
{
	DefaultInitializationErrorHandler_t3109936861::get_offset_of_mErrorText_4(),
	DefaultInitializationErrorHandler_t3109936861::get_offset_of_mErrorOccurred_5(),
	0,
	DefaultInitializationErrorHandler_t3109936861::get_offset_of_bodyStyle_7(),
	DefaultInitializationErrorHandler_t3109936861::get_offset_of_headerStyle_8(),
	DefaultInitializationErrorHandler_t3109936861::get_offset_of_footerStyle_9(),
	DefaultInitializationErrorHandler_t3109936861::get_offset_of_bodyTexture_10(),
	DefaultInitializationErrorHandler_t3109936861::get_offset_of_headerTexture_11(),
	DefaultInitializationErrorHandler_t3109936861::get_offset_of_footerTexture_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3117 = { sizeof (DefaultTrackableEventHandler_t1588957063), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3117[1] = 
{
	DefaultTrackableEventHandler_t1588957063::get_offset_of_mTrackableBehaviour_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3118 = { sizeof (U3CModuleU3E_t692745556), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3119 = { sizeof (iTween_t770867771), -1, sizeof(iTween_t770867771_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3119[38] = 
{
	iTween_t770867771_StaticFields::get_offset_of_tweens_4(),
	iTween_t770867771::get_offset_of_id_5(),
	iTween_t770867771::get_offset_of_type_6(),
	iTween_t770867771::get_offset_of_method_7(),
	iTween_t770867771::get_offset_of_easeType_8(),
	iTween_t770867771::get_offset_of_time_9(),
	iTween_t770867771::get_offset_of_delay_10(),
	iTween_t770867771::get_offset_of_loopType_11(),
	iTween_t770867771::get_offset_of_isRunning_12(),
	iTween_t770867771::get_offset_of_isPaused_13(),
	iTween_t770867771::get_offset_of__name_14(),
	iTween_t770867771::get_offset_of_runningTime_15(),
	iTween_t770867771::get_offset_of_percentage_16(),
	iTween_t770867771::get_offset_of_delayStarted_17(),
	iTween_t770867771::get_offset_of_kinematic_18(),
	iTween_t770867771::get_offset_of_isLocal_19(),
	iTween_t770867771::get_offset_of_loop_20(),
	iTween_t770867771::get_offset_of_reverse_21(),
	iTween_t770867771::get_offset_of_wasPaused_22(),
	iTween_t770867771::get_offset_of_physics_23(),
	iTween_t770867771::get_offset_of_tweenArguments_24(),
	iTween_t770867771::get_offset_of_space_25(),
	iTween_t770867771::get_offset_of_ease_26(),
	iTween_t770867771::get_offset_of_apply_27(),
	iTween_t770867771::get_offset_of_audioSource_28(),
	iTween_t770867771::get_offset_of_vector3s_29(),
	iTween_t770867771::get_offset_of_vector2s_30(),
	iTween_t770867771::get_offset_of_colors_31(),
	iTween_t770867771::get_offset_of_floats_32(),
	iTween_t770867771::get_offset_of_rects_33(),
	iTween_t770867771::get_offset_of_path_34(),
	iTween_t770867771::get_offset_of_preUpdate_35(),
	iTween_t770867771::get_offset_of_postUpdate_36(),
	iTween_t770867771::get_offset_of_namedcolorvalue_37(),
	iTween_t770867771::get_offset_of_lastRealTime_38(),
	iTween_t770867771::get_offset_of_useRealTime_39(),
	iTween_t770867771::get_offset_of_thisTransform_40(),
	iTween_t770867771_StaticFields::get_offset_of_U3CU3Ef__switchU24map0_41(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3120 = { sizeof (EasingFunction_t2767217938), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3121 = { sizeof (ApplyTween_t3327999347), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3122 = { sizeof (EaseType_t2573404410)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3122[34] = 
{
	EaseType_t2573404410::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3123 = { sizeof (LoopType_t369612249)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3123[4] = 
{
	LoopType_t369612249::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3124 = { sizeof (NamedValueColor_t1091574706)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3124[5] = 
{
	NamedValueColor_t1091574706::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3125 = { sizeof (Defaults_t3148213711), -1, sizeof(Defaults_t3148213711_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3125[15] = 
{
	Defaults_t3148213711_StaticFields::get_offset_of_time_0(),
	Defaults_t3148213711_StaticFields::get_offset_of_delay_1(),
	Defaults_t3148213711_StaticFields::get_offset_of_namedColorValue_2(),
	Defaults_t3148213711_StaticFields::get_offset_of_loopType_3(),
	Defaults_t3148213711_StaticFields::get_offset_of_easeType_4(),
	Defaults_t3148213711_StaticFields::get_offset_of_lookSpeed_5(),
	Defaults_t3148213711_StaticFields::get_offset_of_isLocal_6(),
	Defaults_t3148213711_StaticFields::get_offset_of_space_7(),
	Defaults_t3148213711_StaticFields::get_offset_of_orientToPath_8(),
	Defaults_t3148213711_StaticFields::get_offset_of_color_9(),
	Defaults_t3148213711_StaticFields::get_offset_of_updateTimePercentage_10(),
	Defaults_t3148213711_StaticFields::get_offset_of_updateTime_11(),
	Defaults_t3148213711_StaticFields::get_offset_of_lookAhead_12(),
	Defaults_t3148213711_StaticFields::get_offset_of_useRealTime_13(),
	Defaults_t3148213711_StaticFields::get_offset_of_up_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3126 = { sizeof (CRSpline_t2815350084), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3126[1] = 
{
	CRSpline_t2815350084::get_offset_of_pts_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3127 = { sizeof (U3CTweenDelayU3Ec__Iterator0_t2686771544), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3127[4] = 
{
	U3CTweenDelayU3Ec__Iterator0_t2686771544::get_offset_of_U24this_0(),
	U3CTweenDelayU3Ec__Iterator0_t2686771544::get_offset_of_U24current_1(),
	U3CTweenDelayU3Ec__Iterator0_t2686771544::get_offset_of_U24disposing_2(),
	U3CTweenDelayU3Ec__Iterator0_t2686771544::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3128 = { sizeof (U3CTweenRestartU3Ec__Iterator1_t1737386981), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3128[4] = 
{
	U3CTweenRestartU3Ec__Iterator1_t1737386981::get_offset_of_U24this_0(),
	U3CTweenRestartU3Ec__Iterator1_t1737386981::get_offset_of_U24current_1(),
	U3CTweenRestartU3Ec__Iterator1_t1737386981::get_offset_of_U24disposing_2(),
	U3CTweenRestartU3Ec__Iterator1_t1737386981::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3129 = { sizeof (U3CStartU3Ec__Iterator2_t2390838266), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3129[4] = 
{
	U3CStartU3Ec__Iterator2_t2390838266::get_offset_of_U24this_0(),
	U3CStartU3Ec__Iterator2_t2390838266::get_offset_of_U24current_1(),
	U3CStartU3Ec__Iterator2_t2390838266::get_offset_of_U24disposing_2(),
	U3CStartU3Ec__Iterator2_t2390838266::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3130 = { sizeof (MoveSample_t3412539464), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3131 = { sizeof (RotateSample_t3002381122), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3132 = { sizeof (SampleInfo_t3693012684), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
