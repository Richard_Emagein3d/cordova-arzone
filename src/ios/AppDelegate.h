
#import <Cordova/CDVViewController.h>
#import <Cordova/CDVAppDelegate.h>

#import "UnityAppController.h"

@interface AppDelegate : CDVAppDelegate {}

@property (strong, nonatomic) UIWindow *unityWindow;

@property (strong, nonatomic) UnityAppController * unityController;

@property (readwrite, assign) BOOL initialized;
@property (readwrite, assign) BOOL unityInitialized;


@property (readwrite, strong) UIApplication* m_application;
@property (readwrite, strong) NSDictionary* m_options;

- (void)showUnityWindow;
- (void)hideUnityWindow;
- (void)shouldAttachRenderDelegate;


@end

