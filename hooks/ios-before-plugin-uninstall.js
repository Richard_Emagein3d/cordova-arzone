#!/usr/bin/env node

var path = require('path');
var fs = require('fs');
var xml2js = require('./xml2js');

function loadConfigXMLDoc(filePath) {
    var json = "";
    try {
        var fileData = fs.readFileSync(filePath, 'ascii');
        var parser = new xml2js.Parser();
        parser.parseString(fileData.substring(0, fileData.length), function (err, result)
            { json = result; });
        return json;
    } catch (ex) {
        console.log(ex)
    }
}

function IOSBeforePluginUninstall(context) {
	var projDir;		// Path to project
	var pluginDir;		// Path to MFP plugin
    //var platformDir;	// Path to platform

    var rootdir = context.opts.projectRoot;

    var configXMLPath = "config.xml";
    var rawJSON = loadConfigXMLDoc(configXMLPath);
    var name = rawJSON.widget.name[0];

    pluginDir = path.join(rootdir,'plugins',context.opts.plugin.id,'src','ios')

    console.log(pluginDir);

    platformDir = path.join(rootdir,'platforms','ios');

    var cdvAppDelegateFile;		// Path to app's AppDelegate.m
    var cdvAppDelegateFileBackup;	// Path to AppDelegate.m backup

    var cdvConfigFile;		// Path to app's build.xconfig
    var cdvConfigFileBackup;	// Path to build.xconfig backup

    var cdvPCHFile;		// Path to app's build.xconfig
    var cdvPCHFileBackup;	// Path to build.xconfig backup
    
    // Restore AppDelegate.m to original from backup  ====================
    cdvAppDelegateFile = path.join(platformDir, name, 'Classes','AppDelegate.m');
    cdvAppDelegateFileBackup = cdvAppDelegateFile + '.bak';

    console.log("rolling back AppDelegate.m");
    if (fs.existsSync(cdvAppDelegateFileBackup)) {
        if (fs.existsSync(cdvAppDelegateFile))
        {
            fs.unlinkSync(cdvAppDelegateFile);
        }
        fs.renameSync(cdvAppDelegateFileBackup, cdvAppDelegateFile);
    } else
    console.log('Backup File Not Fund: ' + cdvAppDelegateFileBackup);


    // Restore AppDelegate.h to original from backup  ====================
    cdvAppDelegateFile = path.join(platformDir, name, 'Classes','AppDelegate.h');
    cdvAppDelegateFileBackup = cdvAppDelegateFile + '.bak';
    console.log("rolling back AppDelegate.h");
    if (fs.existsSync(cdvAppDelegateFileBackup)) {
        if (fs.existsSync(cdvAppDelegateFile))
        {
            fs.unlinkSync(cdvAppDelegateFile);
        }
        fs.renameSync(cdvAppDelegateFileBackup, cdvAppDelegateFile);
    } else
    console.log('Backup File Not Fund: ' + cdvAppDelegateFileBackup);


    // Restore build.xcconfig to original from backup  ====================
    cdvConfigFile = path.join(platformDir, 'cordova','build.xcconfig');
    cdvConfigFileBackup = cdvConfigFile + '.bak';

    console.log("rolling back build.xcconfig");
    if (fs.existsSync(cdvConfigFileBackup)) {
        if (fs.existsSync(cdvConfigFile))
        {
            fs.unlinkSync(cdvConfigFile);
        }
        fs.renameSync(cdvConfigFileBackup, cdvConfigFile);
    } else
    console.log('Backup File Not Fund: ' + cdvConfigFileBackup);


    var src = path.join(platformDir, name, name+'-Prefix.pch');
    var dest = path.join(platformDir,  'CordovaLib',name, name+'-Prefix.pch');

    // Restore {MyProjectName}-Prefix.pch to original from backup  ====================
    pluginPCHFile = path.join(pluginDir, 'prefix.pch');
    cdvPCHFile = src;
    cdvPCHFileBackup = cdvPCHFile + '.bak';

    console.log("rolling back prefix-header.pch");
    if (fs.existsSync(cdvPCHFileBackup))
    {
        if (fs.existsSync(cdvPCHFile))
        {
            fs.unlinkSync(cdvPCHFile);
        }
        if (fs.existsSync(dest))
        {
            fs.unlinkSync(dest);
        }
        fs.renameSync(cdvPCHFileBackup, cdvPCHFile);
    } else
    console.log('Backup File Not Fund: ' + cdvPCHFileBackup);

    if (fs.existsSync(path.join(platformDir, 'CordovaLib',name)))
        fs.rmdirSync(path.join(platformDir, 'CordovaLib',name));

}

module.exports = IOSBeforePluginUninstall;
