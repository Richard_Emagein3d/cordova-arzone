
#import "AppDelegate.h"
#import "MainViewController.h"

#import "VuforiaRenderDelegate.h"
#import "ARzone.h"

#include "RegisterMonoModules.h"
#include "RegisterFeatures.h"


void VuforiaRenderEvent(int marker);

@implementation AppDelegate


- (void)shouldAttachRenderDelegate
{
    
    NSLog(@"should attach render delgate");
    if( self.initialized )
    {
        self.unityController.renderDelegate = [[VuforiaRenderDelegate alloc] init];
        UnityRegisterRenderingPlugin(NULL, &VuforiaRenderEvent);
        
        /*#if UNITY_VERSION>434
         
         UnityRegisterRenderingPlugin(NULL, &VuforiaRenderEvent);
         
         #endif*/
    }
}

- (UIWindow *)unityWindow {
    return UnityGetMainWindow();
}


void UnityInitTrampoline();

- (void)showUnityWindow {
    
    if (!self.initialized)
    {
        ARzone * AR = [[ARzone alloc] init];
        [AR initUnity];
        
        self.unityController = [[UnityAppController alloc] init];
        [self.unityController application:self.m_application didFinishLaunchingWithOptions:self.m_options];
        
        //self.m_waitingMessage = nil;
        //self.m_application = nil;
        //self.m_options = nil;
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onAppWillTerminateCallback:)
                                                     name:UIApplicationWillTerminateNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onAppWillResignActiveCallback:)
                                                     name:UIApplicationWillResignActiveNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onAppDidBecomeActiveCallback:)
                                                     name:UIApplicationDidBecomeActiveNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onAppWillEnterForegroundCallback:)
                                                     name:UIApplicationWillEnterForegroundNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onAppDidEnterBackgroundCallback:)
                                                     name:UIApplicationDidEnterBackgroundNotification object:nil];
        
        self.initialized = YES;
    }
    
    UnityPause(0);
    
    [self onAppDidBecomeActiveCallback:nil];
    [self.unityWindow makeKeyAndVisible];
    
}

- (void)hideUnityWindow
{
    UnityPause(1);
    [self.window makeKeyAndVisible];
}

- (BOOL)application:(UIApplication*)application didFinishLaunchingWithOptions:(NSDictionary*)launchOptions
{
    self.m_application = application;
    self.m_options = launchOptions;
    
    self.viewController = [[MainViewController alloc] init];
    return [super application:application didFinishLaunchingWithOptions:launchOptions];
}

- (void)onAppWillTerminateCallback:(NSNotification*)notification
{
    [self.unityController applicationWillTerminate:[UIApplication sharedApplication]];
    [self.window makeKeyAndVisible];
}

- (void)onAppWillResignActiveCallback:(NSNotification*)notification
{
    [self.unityController applicationWillResignActive:[UIApplication sharedApplication]];
    [self.window makeKeyAndVisible];
}

- (void)onAppWillEnterForegroundCallback:(NSNotification*)notification
{
    [self.unityController applicationWillEnterForeground:[UIApplication sharedApplication]];
    [self.window makeKeyAndVisible];
}

- (void)onAppDidBecomeActiveCallback:(NSNotification*)notification
{
    [self.unityController applicationDidBecomeActive:[UIApplication sharedApplication]];
    [self.window makeKeyAndVisible];
}

- (void)onAppDidEnterBackgroundCallback:(NSNotification*)notification
{
    [self.unityController applicationDidEnterBackground:[UIApplication sharedApplication]];
    [self.window makeKeyAndVisible];
}

-(void)applicationWillResignActive:(UIApplication *)application{
    [self.unityController applicationWillResignActive:application];
    [self.window makeKeyAndVisible];
}

-(void)applicationDidEnterBackground:(UIApplication *)application{
    [self.unityController applicationDidEnterBackground:application];
    [self.window makeKeyAndVisible];
}

-(void)applicationWillEnterForeground:(UIApplication *)application{
    [self.unityController applicationWillEnterForeground:application];
    [self.window makeKeyAndVisible];
}

-(void)applicationDidBecomeActive:(UIApplication *)application{
    [self.unityController applicationDidBecomeActive:application];
    [self.window makeKeyAndVisible];
}
- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    [self.unityController applicationWillTerminate:application];
    [self.window makeKeyAndVisible];
    
}



@end

