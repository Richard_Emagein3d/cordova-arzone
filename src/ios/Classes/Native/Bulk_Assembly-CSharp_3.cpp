﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"

template <typename T1, typename T2, typename T3, typename T4>
struct VirtActionInvoker4
{
	typedef void (*Action)(void*, T1, T2, T3, T4, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1, T2 p2, T3 p3, T4 p4)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, p4, invokeData.method);
	}
};
template <typename T1, typename T2, typename T3>
struct VirtActionInvoker3
{
	typedef void (*Action)(void*, T1, T2, T3, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1, T2 p2, T3 p3)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, invokeData.method);
	}
};
template <typename R>
struct VirtFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1, typename T2, typename T3, typename T4>
struct GenericVirtActionInvoker4
{
	typedef void (*Action)(void*, T1, T2, T3, T4, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1, T2 p2, T3 p3, T4 p4)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_virtual_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, p4, invokeData.method);
	}
};
template <typename T1, typename T2, typename T3>
struct GenericVirtActionInvoker3
{
	typedef void (*Action)(void*, T1, T2, T3, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1, T2 p2, T3 p3)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_virtual_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, invokeData.method);
	}
};
template <typename T1, typename T2, typename T3, typename T4>
struct InterfaceActionInvoker4
{
	typedef void (*Action)(void*, T1, T2, T3, T4, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1, T2 p2, T3 p3, T4 p4)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, p4, invokeData.method);
	}
};
template <typename T1, typename T2, typename T3>
struct InterfaceActionInvoker3
{
	typedef void (*Action)(void*, T1, T2, T3, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1, T2 p2, T3 p3)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, invokeData.method);
	}
};
template <typename R>
struct InterfaceFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
struct InterfaceActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1, typename T2, typename T3, typename T4>
struct GenericInterfaceActionInvoker4
{
	typedef void (*Action)(void*, T1, T2, T3, T4, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1, T2 p2, T3 p3, T4 p4)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_interface_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, p4, invokeData.method);
	}
};
template <typename T1, typename T2, typename T3>
struct GenericInterfaceActionInvoker3
{
	typedef void (*Action)(void*, T1, T2, T3, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1, T2 p2, T3 p3)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_interface_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, invokeData.method);
	}
};

// AnimationsManager
struct AnimationsManager_t2281084567;
// MarkerData
struct MarkerData_t1252592219;
// MarkerManager
struct MarkerManager_t1031803141;
// Page
struct Page_t2586579457;
// PageTransition
struct PageTransition_t3158442731;
// Page[]
struct PageU5BU5D_t2014447836;
// Pixel
struct Pixel_t2713956838;
// Pixel[]
struct PixelU5BU5D_t462994371;
// System.Action`1<UnityEngine.AsyncOperation>
struct Action_1_t1617499438;
// System.Action`1<UnityEngine.U2D.SpriteAtlas>
struct Action_1_t819399007;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// System.Byte[]
struct ByteU5BU5D_t4116647657;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t2736202052;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t1632706988;
// System.Collections.Generic.List`1<Pixel>
struct List_1_t4186031580;
// System.Collections.Generic.List`1<Pixel>[]
struct List_1U5BU5D_t3658578741;
// System.Collections.Generic.List`1<System.Byte[]>
struct List_1_t1293755103;
// System.Collections.Generic.List`1<System.Collections.Generic.List`1<Pixel>>
struct List_1_t1363139026;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t257213610;
// System.Collections.Generic.List`1<System.String>
struct List_1_t3319525431;
// System.Collections.Generic.List`1<UnityEngine.CanvasGroup>
struct List_1_t1260619206;
// System.Collections.Generic.List`1<UnityEngine.UI.Image>
struct List_1_t4142344393;
// System.Collections.Generic.List`1<UnityEngine.UI.Selectable>
struct List_1_t427135887;
// System.Collections.IDictionary
struct IDictionary_t1363984059;
// System.Collections.IEnumerator
struct IEnumerator_t1853284238;
// System.DelegateData
struct DelegateData_t1677132599;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.IntPtr[]
struct IntPtrU5BU5D_t4013366056;
// System.NotSupportedException
struct NotSupportedException_t1314879016;
// System.Object[]
struct ObjectU5BU5D_t2843939325;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1281789340;
// System.Uri
struct Uri_t100236324;
// System.Void
struct Void_t1185182177;
// TMPro.TextMeshProUGUI
struct TextMeshProUGUI_t529313277;
// TriLib.TextureLoadHandle
struct TextureLoadHandle_t2346000395;
// TutoManager
struct TutoManager_t4000134877;
// UIManager
struct UIManager_t1042050227;
// UnityEngine.Animator
struct Animator_t434523843;
// UnityEngine.AssetBundle
struct AssetBundle_t1153907252;
// UnityEngine.Canvas
struct Canvas_t3310196443;
// UnityEngine.CanvasRenderer
struct CanvasRenderer_t2598313366;
// UnityEngine.Color[]
struct ColorU5BU5D_t941916413;
// UnityEngine.Component
struct Component_t1923634451;
// UnityEngine.Coroutine
struct Coroutine_t3829159415;
// UnityEngine.Event
struct Event_t2956885303;
// UnityEngine.Events.UnityAction
struct UnityAction_t3245792599;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// UnityEngine.Material
struct Material_t340375123;
// UnityEngine.Mesh
struct Mesh_t3648964284;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t3962482529;
// UnityEngine.Networking.CertificateHandler
struct CertificateHandler_t2739891000;
// UnityEngine.Networking.DownloadHandler
struct DownloadHandler_t2937767557;
// UnityEngine.Networking.UnityWebRequest
struct UnityWebRequest_t463507806;
// UnityEngine.Networking.UnityWebRequestAsyncOperation
struct UnityWebRequestAsyncOperation_t3852015985;
// UnityEngine.Networking.UploadHandler
struct UploadHandler_t2993558019;
// UnityEngine.Object
struct Object_t631007953;
// UnityEngine.RectTransform
struct RectTransform_t3704657025;
// UnityEngine.RectTransform/ReapplyDrivenProperties
struct ReapplyDrivenProperties_t1258266594;
// UnityEngine.Renderer
struct Renderer_t2627027031;
// UnityEngine.Renderer[]
struct RendererU5BU5D_t3210418286;
// UnityEngine.Sprite
struct Sprite_t280657092;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_t3235626157;
// UnityEngine.TextGenerator
struct TextGenerator_t3211863866;
// UnityEngine.Texture
struct Texture_t3661962703;
// UnityEngine.Texture2D
struct Texture2D_t3840446185;
// UnityEngine.TouchScreenKeyboard
struct TouchScreenKeyboard_t731888065;
// UnityEngine.Transform
struct Transform_t3600365921;
// UnityEngine.UI.AnimationTriggers
struct AnimationTriggers_t2532145056;
// UnityEngine.UI.Button
struct Button_t4055032469;
// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>
struct TweenRunner_1_t3055525458;
// UnityEngine.UI.Graphic
struct Graphic_t1660335611;
// UnityEngine.UI.Image
struct Image_t2670269651;
// UnityEngine.UI.InputField
struct InputField_t3762917431;
// UnityEngine.UI.InputField/OnChangeEvent
struct OnChangeEvent_t467195904;
// UnityEngine.UI.InputField/OnValidateInput
struct OnValidateInput_t2355412304;
// UnityEngine.UI.InputField/SubmitEvent
struct SubmitEvent_t648412432;
// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent
struct CullStateChangedEvent_t3661388177;
// UnityEngine.UI.RawImage
struct RawImage_t3182918964;
// UnityEngine.UI.RectMask2D
struct RectMask2D_t3474889437;
// UnityEngine.UI.Selectable
struct Selectable_t3250028441;
// UnityEngine.UI.Slider
struct Slider_t3903728902;
// UnityEngine.UI.Text
struct Text_t1901882714;
// UnityEngine.UI.VertexHelper
struct VertexHelper_t2453304189;
// UnityEngine.UIVertex[]
struct UIVertexU5BU5D_t1981460040;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t1457185986;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1718750761;
// UnityEngine.Video.VideoPlayer
struct VideoPlayer_t1683042537;
// UnityEngine.Video.VideoPlayer/ErrorEventHandler
struct ErrorEventHandler_t3211687919;
// UnityEngine.Video.VideoPlayer/EventHandler
struct EventHandler_t3436254912;
// UnityEngine.Video.VideoPlayer/FrameReadyEventHandler
struct FrameReadyEventHandler_t3848515759;
// UnityEngine.Video.VideoPlayer/TimeEventHandler
struct TimeEventHandler_t445758600;
// UnityEngine.WWW
struct WWW_t3688466362;
// UnityEngine.WWWForm
struct WWWForm_t4064702195;
// Vuforia.CloudRecoBehaviour
struct CloudRecoBehaviour_t431762792;
// Vuforia.TrackableBehaviour
struct TrackableBehaviour_t1113559212;
// ZoomObject
struct ZoomObject_t2903076344;
// animationImage
struct animationImage_t1423068421;
// baseSelector
struct baseSelector_t1489849420;
// captureAndSendScreen
struct captureAndSendScreen_t3196082656;
// captureAndSendScreen/<IMPL_sendCapture>c__Iterator0
struct U3CIMPL_sendCaptureU3Ec__Iterator0_t1292966789;
// startUpLANG
struct startUpLANG_t1517666504;
// startUpLANG/<GetLanguageJSON>c__Iterator0
struct U3CGetLanguageJSONU3Ec__Iterator0_t3550365120;
// testImage
struct testImage_t2491645257;
// testPlayer
struct testPlayer_t3643834628;
// testRescale
struct testRescale_t3860525213;

extern RuntimeClass* ContentManager2_t2980555998_il2cpp_TypeInfo_var;
extern RuntimeClass* Debug_t3317548046_il2cpp_TypeInfo_var;
extern RuntimeClass* GameObject_t1113636619_il2cpp_TypeInfo_var;
extern RuntimeClass* IDisposable_t3640265483_il2cpp_TypeInfo_var;
extern RuntimeClass* IEnumerator_t1853284238_il2cpp_TypeInfo_var;
extern RuntimeClass* Input_t1431474628_il2cpp_TypeInfo_var;
extern RuntimeClass* Int32_t2950945753_il2cpp_TypeInfo_var;
extern RuntimeClass* List_1_t4186031580_il2cpp_TypeInfo_var;
extern RuntimeClass* MarkerManager_t1031803141_il2cpp_TypeInfo_var;
extern RuntimeClass* Mathf_t3464937446_il2cpp_TypeInfo_var;
extern RuntimeClass* NotSupportedException_t1314879016_il2cpp_TypeInfo_var;
extern RuntimeClass* ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var;
extern RuntimeClass* Object_t631007953_il2cpp_TypeInfo_var;
extern RuntimeClass* Pixel_t2713956838_il2cpp_TypeInfo_var;
extern RuntimeClass* Single_t1397266774_il2cpp_TypeInfo_var;
extern RuntimeClass* StringU5BU5D_t1281789340_il2cpp_TypeInfo_var;
extern RuntimeClass* String_t_il2cpp_TypeInfo_var;
extern RuntimeClass* Texture2D_t3840446185_il2cpp_TypeInfo_var;
extern RuntimeClass* TextureFormat_t2701165832_il2cpp_TypeInfo_var;
extern RuntimeClass* Transform_t3600365921_il2cpp_TypeInfo_var;
extern RuntimeClass* U3CGetLanguageJSONU3Ec__Iterator0_t3550365120_il2cpp_TypeInfo_var;
extern RuntimeClass* U3CIMPL_sendCaptureU3Ec__Iterator0_t1292966789_il2cpp_TypeInfo_var;
extern RuntimeClass* Vector2_t2156229523_il2cpp_TypeInfo_var;
extern RuntimeClass* Vector3_t3722313464_il2cpp_TypeInfo_var;
extern RuntimeClass* WWWForm_t4064702195_il2cpp_TypeInfo_var;
extern RuntimeClass* WWW_t3688466362_il2cpp_TypeInfo_var;
extern String_t* _stringLiteral10537912;
extern String_t* _stringLiteral1107120589;
extern String_t* _stringLiteral1222789648;
extern String_t* _stringLiteral1392271312;
extern String_t* _stringLiteral1512728895;
extern String_t* _stringLiteral1615931692;
extern String_t* _stringLiteral1629333464;
extern String_t* _stringLiteral1645758142;
extern String_t* _stringLiteral1645954750;
extern String_t* _stringLiteral1884250374;
extern String_t* _stringLiteral2045074213;
extern String_t* _stringLiteral2078343567;
extern String_t* _stringLiteral207938285;
extern String_t* _stringLiteral2092972393;
extern String_t* _stringLiteral2123297983;
extern String_t* _stringLiteral2526170751;
extern String_t* _stringLiteral2595570117;
extern String_t* _stringLiteral2599195452;
extern String_t* _stringLiteral2733530345;
extern String_t* _stringLiteral274399943;
extern String_t* _stringLiteral2793514143;
extern String_t* _stringLiteral2929551439;
extern String_t* _stringLiteral2998704445;
extern String_t* _stringLiteral3017359863;
extern String_t* _stringLiteral3074750744;
extern String_t* _stringLiteral3086763974;
extern String_t* _stringLiteral3129749765;
extern String_t* _stringLiteral3287972590;
extern String_t* _stringLiteral330296735;
extern String_t* _stringLiteral3357073080;
extern String_t* _stringLiteral336663310;
extern String_t* _stringLiteral3452614529;
extern String_t* _stringLiteral3495511934;
extern String_t* _stringLiteral3698539982;
extern String_t* _stringLiteral4247501208;
extern String_t* _stringLiteral4275918539;
extern String_t* _stringLiteral589637722;
extern String_t* _stringLiteral609727728;
extern String_t* _stringLiteral688097028;
extern String_t* _stringLiteral79347;
extern const RuntimeMethod* Component_GetComponent_TisRectTransform_t3704657025_m3396022872_RuntimeMethod_var;
extern const RuntimeMethod* Component_GetComponent_TisSpriteRenderer_t3235626157_m2253180502_RuntimeMethod_var;
extern const RuntimeMethod* Component_GetComponentsInChildren_TisRenderer_t2627027031_m2677127880_RuntimeMethod_var;
extern const RuntimeMethod* GameObject_AddComponent_TisSpriteRenderer_t3235626157_m1939192903_RuntimeMethod_var;
extern const RuntimeMethod* GameObject_GetComponent_TisAnimator_t434523843_m440019408_RuntimeMethod_var;
extern const RuntimeMethod* GameObject_GetComponent_TisRenderer_t2627027031_m1619941042_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Add_m1787940857_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Add_m530383596_RuntimeMethod_var;
extern const RuntimeMethod* List_1_RemoveAt_m1835037992_RuntimeMethod_var;
extern const RuntimeMethod* List_1__ctor_m1545936499_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Count_m334549794_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Item_m1974214454_RuntimeMethod_var;
extern const RuntimeMethod* U3CGetLanguageJSONU3Ec__Iterator0_Reset_m2398464953_RuntimeMethod_var;
extern const RuntimeMethod* U3CIMPL_sendCaptureU3Ec__Iterator0_Reset_m3322412218_RuntimeMethod_var;
extern const uint32_t TransformExtensions_DestroyChildren_m3219731900_MetadataUsageId;
extern const uint32_t TransformExtensions_EncapsulateBounds_m1814600766_MetadataUsageId;
extern const uint32_t TransformExtensions_FindDeepChild_m311219388_MetadataUsageId;
extern const uint32_t TutoManager_backToMenu_m2565265301_MetadataUsageId;
extern const uint32_t TutoManager_launchAR_AUTO_m862194551_MetadataUsageId;
extern const uint32_t U3CGetLanguageJSONU3Ec__Iterator0_MoveNext_m1986941187_MetadataUsageId;
extern const uint32_t U3CGetLanguageJSONU3Ec__Iterator0_Reset_m2398464953_MetadataUsageId;
extern const uint32_t U3CIMPL_sendCaptureU3Ec__Iterator0_MoveNext_m200448671_MetadataUsageId;
extern const uint32_t U3CIMPL_sendCaptureU3Ec__Iterator0_Reset_m3322412218_MetadataUsageId;
extern const uint32_t UIManager_OnClickMenu_m710568917_MetadataUsageId;
extern const uint32_t UIManager_OnCreditClicked_m1737799703_MetadataUsageId;
extern const uint32_t UIManager_OnTutoClicked_m3350042246_MetadataUsageId;
extern const uint32_t UIManager_OnTutoClosed_m3625715984_MetadataUsageId;
extern const uint32_t UIManager_Start_m1211894305_MetadataUsageId;
extern const uint32_t UIManager_Update_m1085197836_MetadataUsageId;
extern const uint32_t ZoomObject_Update_m3507186912_MetadataUsageId;
extern const uint32_t animationImage_AnimationOuverture_m2423226750_MetadataUsageId;
extern const uint32_t animationImage_DemarerAnimation_m273565184_MetadataUsageId;
extern const uint32_t animationImage_Start_m2640814523_MetadataUsageId;
extern const uint32_t animationImage_awake_m3575752642_MetadataUsageId;
extern const uint32_t baseSelector_getText_m3897978743_MetadataUsageId;
extern const uint32_t captureAndSendScreen_IMPL_sendCapture_m127267877_MetadataUsageId;
extern const uint32_t captureAndSendScreen_Start_m1022736917_MetadataUsageId;
extern const uint32_t captureAndSendScreen__ctor_m2626167397_MetadataUsageId;
extern const uint32_t captureAndSendScreen_sendCapture_m3694921046_MetadataUsageId;
extern const uint32_t startUpLANG_GetLanguageJSON_m3198424110_MetadataUsageId;
extern const uint32_t startUpLANG_Start_m1865577903_MetadataUsageId;
extern const uint32_t testImage_clickImage_m58836525_MetadataUsageId;
extern const uint32_t testRescale_Start_m3448785195_MetadataUsageId;
struct CertificateHandler_t2739891000_marshaled_com;
struct DownloadHandler_t2937767557_marshaled_com;
struct UnityWebRequest_t463507806_marshaled_com;
struct UnityWebRequest_t463507806_marshaled_pinvoke;
struct UploadHandler_t2993558019_marshaled_com;

struct ByteU5BU5D_t4116647657;
struct ObjectU5BU5D_t2843939325;
struct StringU5BU5D_t1281789340;
struct ColorU5BU5D_t941916413;
struct RendererU5BU5D_t3210418286;


#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef MARKERMANAGER_T1031803141_H
#define MARKERMANAGER_T1031803141_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MarkerManager
struct  MarkerManager_t1031803141  : public RuntimeObject
{
public:
	// System.Int32 MarkerManager::layerCount
	int32_t ___layerCount_0;
	// MarkerData MarkerManager::marker
	MarkerData_t1252592219 * ___marker_1;

public:
	inline static int32_t get_offset_of_layerCount_0() { return static_cast<int32_t>(offsetof(MarkerManager_t1031803141, ___layerCount_0)); }
	inline int32_t get_layerCount_0() const { return ___layerCount_0; }
	inline int32_t* get_address_of_layerCount_0() { return &___layerCount_0; }
	inline void set_layerCount_0(int32_t value)
	{
		___layerCount_0 = value;
	}

	inline static int32_t get_offset_of_marker_1() { return static_cast<int32_t>(offsetof(MarkerManager_t1031803141, ___marker_1)); }
	inline MarkerData_t1252592219 * get_marker_1() const { return ___marker_1; }
	inline MarkerData_t1252592219 ** get_address_of_marker_1() { return &___marker_1; }
	inline void set_marker_1(MarkerData_t1252592219 * value)
	{
		___marker_1 = value;
		Il2CppCodeGenWriteBarrier((&___marker_1), value);
	}
};

struct MarkerManager_t1031803141_StaticFields
{
public:
	// MarkerManager MarkerManager::_Instance
	MarkerManager_t1031803141 * ____Instance_2;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> MarkerManager::<>f__switch$map0
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map0_3;

public:
	inline static int32_t get_offset_of__Instance_2() { return static_cast<int32_t>(offsetof(MarkerManager_t1031803141_StaticFields, ____Instance_2)); }
	inline MarkerManager_t1031803141 * get__Instance_2() const { return ____Instance_2; }
	inline MarkerManager_t1031803141 ** get_address_of__Instance_2() { return &____Instance_2; }
	inline void set__Instance_2(MarkerManager_t1031803141 * value)
	{
		____Instance_2 = value;
		Il2CppCodeGenWriteBarrier((&____Instance_2), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map0_3() { return static_cast<int32_t>(offsetof(MarkerManager_t1031803141_StaticFields, ___U3CU3Ef__switchU24map0_3)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map0_3() const { return ___U3CU3Ef__switchU24map0_3; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map0_3() { return &___U3CU3Ef__switchU24map0_3; }
	inline void set_U3CU3Ef__switchU24map0_3(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map0_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MARKERMANAGER_T1031803141_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef LIST_1_T4186031580_H
#define LIST_1_T4186031580_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<Pixel>
struct  List_1_t4186031580  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	PixelU5BU5D_t462994371* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t4186031580, ____items_1)); }
	inline PixelU5BU5D_t462994371* get__items_1() const { return ____items_1; }
	inline PixelU5BU5D_t462994371** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(PixelU5BU5D_t462994371* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t4186031580, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t4186031580, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t4186031580_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	PixelU5BU5D_t462994371* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t4186031580_StaticFields, ___EmptyArray_4)); }
	inline PixelU5BU5D_t462994371* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline PixelU5BU5D_t462994371** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(PixelU5BU5D_t462994371* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T4186031580_H
#ifndef LIST_1_T1363139026_H
#define LIST_1_T1363139026_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<System.Collections.Generic.List`1<Pixel>>
struct  List_1_t1363139026  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	List_1U5BU5D_t3658578741* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t1363139026, ____items_1)); }
	inline List_1U5BU5D_t3658578741* get__items_1() const { return ____items_1; }
	inline List_1U5BU5D_t3658578741** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(List_1U5BU5D_t3658578741* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t1363139026, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t1363139026, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t1363139026_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	List_1U5BU5D_t3658578741* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t1363139026_StaticFields, ___EmptyArray_4)); }
	inline List_1U5BU5D_t3658578741* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline List_1U5BU5D_t3658578741** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(List_1U5BU5D_t3658578741* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T1363139026_H
#ifndef EXCEPTION_T_H
#define EXCEPTION_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.IntPtr[] System.Exception::trace_ips
	IntPtrU5BU5D_t4013366056* ___trace_ips_0;
	// System.Exception System.Exception::inner_exception
	Exception_t * ___inner_exception_1;
	// System.String System.Exception::message
	String_t* ___message_2;
	// System.String System.Exception::help_link
	String_t* ___help_link_3;
	// System.String System.Exception::class_name
	String_t* ___class_name_4;
	// System.String System.Exception::stack_trace
	String_t* ___stack_trace_5;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_6;
	// System.Int32 System.Exception::remote_stack_index
	int32_t ___remote_stack_index_7;
	// System.Int32 System.Exception::hresult
	int32_t ___hresult_8;
	// System.String System.Exception::source
	String_t* ___source_9;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_10;

public:
	inline static int32_t get_offset_of_trace_ips_0() { return static_cast<int32_t>(offsetof(Exception_t, ___trace_ips_0)); }
	inline IntPtrU5BU5D_t4013366056* get_trace_ips_0() const { return ___trace_ips_0; }
	inline IntPtrU5BU5D_t4013366056** get_address_of_trace_ips_0() { return &___trace_ips_0; }
	inline void set_trace_ips_0(IntPtrU5BU5D_t4013366056* value)
	{
		___trace_ips_0 = value;
		Il2CppCodeGenWriteBarrier((&___trace_ips_0), value);
	}

	inline static int32_t get_offset_of_inner_exception_1() { return static_cast<int32_t>(offsetof(Exception_t, ___inner_exception_1)); }
	inline Exception_t * get_inner_exception_1() const { return ___inner_exception_1; }
	inline Exception_t ** get_address_of_inner_exception_1() { return &___inner_exception_1; }
	inline void set_inner_exception_1(Exception_t * value)
	{
		___inner_exception_1 = value;
		Il2CppCodeGenWriteBarrier((&___inner_exception_1), value);
	}

	inline static int32_t get_offset_of_message_2() { return static_cast<int32_t>(offsetof(Exception_t, ___message_2)); }
	inline String_t* get_message_2() const { return ___message_2; }
	inline String_t** get_address_of_message_2() { return &___message_2; }
	inline void set_message_2(String_t* value)
	{
		___message_2 = value;
		Il2CppCodeGenWriteBarrier((&___message_2), value);
	}

	inline static int32_t get_offset_of_help_link_3() { return static_cast<int32_t>(offsetof(Exception_t, ___help_link_3)); }
	inline String_t* get_help_link_3() const { return ___help_link_3; }
	inline String_t** get_address_of_help_link_3() { return &___help_link_3; }
	inline void set_help_link_3(String_t* value)
	{
		___help_link_3 = value;
		Il2CppCodeGenWriteBarrier((&___help_link_3), value);
	}

	inline static int32_t get_offset_of_class_name_4() { return static_cast<int32_t>(offsetof(Exception_t, ___class_name_4)); }
	inline String_t* get_class_name_4() const { return ___class_name_4; }
	inline String_t** get_address_of_class_name_4() { return &___class_name_4; }
	inline void set_class_name_4(String_t* value)
	{
		___class_name_4 = value;
		Il2CppCodeGenWriteBarrier((&___class_name_4), value);
	}

	inline static int32_t get_offset_of_stack_trace_5() { return static_cast<int32_t>(offsetof(Exception_t, ___stack_trace_5)); }
	inline String_t* get_stack_trace_5() const { return ___stack_trace_5; }
	inline String_t** get_address_of_stack_trace_5() { return &___stack_trace_5; }
	inline void set_stack_trace_5(String_t* value)
	{
		___stack_trace_5 = value;
		Il2CppCodeGenWriteBarrier((&___stack_trace_5), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_6() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_6)); }
	inline String_t* get__remoteStackTraceString_6() const { return ____remoteStackTraceString_6; }
	inline String_t** get_address_of__remoteStackTraceString_6() { return &____remoteStackTraceString_6; }
	inline void set__remoteStackTraceString_6(String_t* value)
	{
		____remoteStackTraceString_6 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_6), value);
	}

	inline static int32_t get_offset_of_remote_stack_index_7() { return static_cast<int32_t>(offsetof(Exception_t, ___remote_stack_index_7)); }
	inline int32_t get_remote_stack_index_7() const { return ___remote_stack_index_7; }
	inline int32_t* get_address_of_remote_stack_index_7() { return &___remote_stack_index_7; }
	inline void set_remote_stack_index_7(int32_t value)
	{
		___remote_stack_index_7 = value;
	}

	inline static int32_t get_offset_of_hresult_8() { return static_cast<int32_t>(offsetof(Exception_t, ___hresult_8)); }
	inline int32_t get_hresult_8() const { return ___hresult_8; }
	inline int32_t* get_address_of_hresult_8() { return &___hresult_8; }
	inline void set_hresult_8(int32_t value)
	{
		___hresult_8 = value;
	}

	inline static int32_t get_offset_of_source_9() { return static_cast<int32_t>(offsetof(Exception_t, ___source_9)); }
	inline String_t* get_source_9() const { return ___source_9; }
	inline String_t** get_address_of_source_9() { return &___source_9; }
	inline void set_source_9(String_t* value)
	{
		___source_9 = value;
		Il2CppCodeGenWriteBarrier((&___source_9), value);
	}

	inline static int32_t get_offset_of__data_10() { return static_cast<int32_t>(offsetof(Exception_t, ____data_10)); }
	inline RuntimeObject* get__data_10() const { return ____data_10; }
	inline RuntimeObject** get_address_of__data_10() { return &____data_10; }
	inline void set__data_10(RuntimeObject* value)
	{
		____data_10 = value;
		Il2CppCodeGenWriteBarrier((&____data_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCEPTION_T_H
#ifndef STRING_T_H
#define STRING_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::length
	int32_t ___length_0;
	// System.Char System.String::start_char
	Il2CppChar ___start_char_1;

public:
	inline static int32_t get_offset_of_length_0() { return static_cast<int32_t>(offsetof(String_t, ___length_0)); }
	inline int32_t get_length_0() const { return ___length_0; }
	inline int32_t* get_address_of_length_0() { return &___length_0; }
	inline void set_length_0(int32_t value)
	{
		___length_0 = value;
	}

	inline static int32_t get_offset_of_start_char_1() { return static_cast<int32_t>(offsetof(String_t, ___start_char_1)); }
	inline Il2CppChar get_start_char_1() const { return ___start_char_1; }
	inline Il2CppChar* get_address_of_start_char_1() { return &___start_char_1; }
	inline void set_start_char_1(Il2CppChar value)
	{
		___start_char_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_2;
	// System.Char[] System.String::WhiteChars
	CharU5BU5D_t3528271667* ___WhiteChars_3;

public:
	inline static int32_t get_offset_of_Empty_2() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_2)); }
	inline String_t* get_Empty_2() const { return ___Empty_2; }
	inline String_t** get_address_of_Empty_2() { return &___Empty_2; }
	inline void set_Empty_2(String_t* value)
	{
		___Empty_2 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_2), value);
	}

	inline static int32_t get_offset_of_WhiteChars_3() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___WhiteChars_3)); }
	inline CharU5BU5D_t3528271667* get_WhiteChars_3() const { return ___WhiteChars_3; }
	inline CharU5BU5D_t3528271667** get_address_of_WhiteChars_3() { return &___WhiteChars_3; }
	inline void set_WhiteChars_3(CharU5BU5D_t3528271667* value)
	{
		___WhiteChars_3 = value;
		Il2CppCodeGenWriteBarrier((&___WhiteChars_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRING_T_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef TRANSFORMEXTENSIONS_T817912662_H
#define TRANSFORMEXTENSIONS_T817912662_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.TransformExtensions
struct  TransformExtensions_t817912662  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORMEXTENSIONS_T817912662_H
#ifndef CUSTOMYIELDINSTRUCTION_T1895667560_H
#define CUSTOMYIELDINSTRUCTION_T1895667560_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.CustomYieldInstruction
struct  CustomYieldInstruction_t1895667560  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMYIELDINSTRUCTION_T1895667560_H
#ifndef WWWFORM_T4064702195_H
#define WWWFORM_T4064702195_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.WWWForm
struct  WWWForm_t4064702195  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<System.Byte[]> UnityEngine.WWWForm::formData
	List_1_t1293755103 * ___formData_0;
	// System.Collections.Generic.List`1<System.String> UnityEngine.WWWForm::fieldNames
	List_1_t3319525431 * ___fieldNames_1;
	// System.Collections.Generic.List`1<System.String> UnityEngine.WWWForm::fileNames
	List_1_t3319525431 * ___fileNames_2;
	// System.Collections.Generic.List`1<System.String> UnityEngine.WWWForm::types
	List_1_t3319525431 * ___types_3;
	// System.Byte[] UnityEngine.WWWForm::boundary
	ByteU5BU5D_t4116647657* ___boundary_4;
	// System.Boolean UnityEngine.WWWForm::containsFiles
	bool ___containsFiles_5;

public:
	inline static int32_t get_offset_of_formData_0() { return static_cast<int32_t>(offsetof(WWWForm_t4064702195, ___formData_0)); }
	inline List_1_t1293755103 * get_formData_0() const { return ___formData_0; }
	inline List_1_t1293755103 ** get_address_of_formData_0() { return &___formData_0; }
	inline void set_formData_0(List_1_t1293755103 * value)
	{
		___formData_0 = value;
		Il2CppCodeGenWriteBarrier((&___formData_0), value);
	}

	inline static int32_t get_offset_of_fieldNames_1() { return static_cast<int32_t>(offsetof(WWWForm_t4064702195, ___fieldNames_1)); }
	inline List_1_t3319525431 * get_fieldNames_1() const { return ___fieldNames_1; }
	inline List_1_t3319525431 ** get_address_of_fieldNames_1() { return &___fieldNames_1; }
	inline void set_fieldNames_1(List_1_t3319525431 * value)
	{
		___fieldNames_1 = value;
		Il2CppCodeGenWriteBarrier((&___fieldNames_1), value);
	}

	inline static int32_t get_offset_of_fileNames_2() { return static_cast<int32_t>(offsetof(WWWForm_t4064702195, ___fileNames_2)); }
	inline List_1_t3319525431 * get_fileNames_2() const { return ___fileNames_2; }
	inline List_1_t3319525431 ** get_address_of_fileNames_2() { return &___fileNames_2; }
	inline void set_fileNames_2(List_1_t3319525431 * value)
	{
		___fileNames_2 = value;
		Il2CppCodeGenWriteBarrier((&___fileNames_2), value);
	}

	inline static int32_t get_offset_of_types_3() { return static_cast<int32_t>(offsetof(WWWForm_t4064702195, ___types_3)); }
	inline List_1_t3319525431 * get_types_3() const { return ___types_3; }
	inline List_1_t3319525431 ** get_address_of_types_3() { return &___types_3; }
	inline void set_types_3(List_1_t3319525431 * value)
	{
		___types_3 = value;
		Il2CppCodeGenWriteBarrier((&___types_3), value);
	}

	inline static int32_t get_offset_of_boundary_4() { return static_cast<int32_t>(offsetof(WWWForm_t4064702195, ___boundary_4)); }
	inline ByteU5BU5D_t4116647657* get_boundary_4() const { return ___boundary_4; }
	inline ByteU5BU5D_t4116647657** get_address_of_boundary_4() { return &___boundary_4; }
	inline void set_boundary_4(ByteU5BU5D_t4116647657* value)
	{
		___boundary_4 = value;
		Il2CppCodeGenWriteBarrier((&___boundary_4), value);
	}

	inline static int32_t get_offset_of_containsFiles_5() { return static_cast<int32_t>(offsetof(WWWForm_t4064702195, ___containsFiles_5)); }
	inline bool get_containsFiles_5() const { return ___containsFiles_5; }
	inline bool* get_address_of_containsFiles_5() { return &___containsFiles_5; }
	inline void set_containsFiles_5(bool value)
	{
		___containsFiles_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WWWFORM_T4064702195_H
#ifndef YIELDINSTRUCTION_T403091072_H
#define YIELDINSTRUCTION_T403091072_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.YieldInstruction
struct  YieldInstruction_t403091072  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_t403091072_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_t403091072_marshaled_com
{
};
#endif // YIELDINSTRUCTION_T403091072_H
#ifndef U3CIMPL_SENDCAPTUREU3EC__ITERATOR0_T1292966789_H
#define U3CIMPL_SENDCAPTUREU3EC__ITERATOR0_T1292966789_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// captureAndSendScreen/<IMPL_sendCapture>c__Iterator0
struct  U3CIMPL_sendCaptureU3Ec__Iterator0_t1292966789  : public RuntimeObject
{
public:
	// System.Int32 captureAndSendScreen/<IMPL_sendCapture>c__Iterator0::<width>__0
	int32_t ___U3CwidthU3E__0_0;
	// System.Int32 captureAndSendScreen/<IMPL_sendCapture>c__Iterator0::<height>__0
	int32_t ___U3CheightU3E__0_1;
	// UnityEngine.Renderer captureAndSendScreen/<IMPL_sendCapture>c__Iterator0::<rendererBack>__0
	Renderer_t2627027031 * ___U3CrendererBackU3E__0_2;
	// UnityEngine.Texture2D captureAndSendScreen/<IMPL_sendCapture>c__Iterator0::<tex>__0
	Texture2D_t3840446185 * ___U3CtexU3E__0_3;
	// System.Byte[] captureAndSendScreen/<IMPL_sendCapture>c__Iterator0::<bytes>__0
	ByteU5BU5D_t4116647657* ___U3CbytesU3E__0_4;
	// UnityEngine.WWWForm captureAndSendScreen/<IMPL_sendCapture>c__Iterator0::<form>__0
	WWWForm_t4064702195 * ___U3CformU3E__0_5;
	// UnityEngine.Networking.UnityWebRequest captureAndSendScreen/<IMPL_sendCapture>c__Iterator0::<w>__0
	UnityWebRequest_t463507806 * ___U3CwU3E__0_6;
	// captureAndSendScreen captureAndSendScreen/<IMPL_sendCapture>c__Iterator0::$this
	captureAndSendScreen_t3196082656 * ___U24this_7;
	// System.Object captureAndSendScreen/<IMPL_sendCapture>c__Iterator0::$current
	RuntimeObject * ___U24current_8;
	// System.Boolean captureAndSendScreen/<IMPL_sendCapture>c__Iterator0::$disposing
	bool ___U24disposing_9;
	// System.Int32 captureAndSendScreen/<IMPL_sendCapture>c__Iterator0::$PC
	int32_t ___U24PC_10;

public:
	inline static int32_t get_offset_of_U3CwidthU3E__0_0() { return static_cast<int32_t>(offsetof(U3CIMPL_sendCaptureU3Ec__Iterator0_t1292966789, ___U3CwidthU3E__0_0)); }
	inline int32_t get_U3CwidthU3E__0_0() const { return ___U3CwidthU3E__0_0; }
	inline int32_t* get_address_of_U3CwidthU3E__0_0() { return &___U3CwidthU3E__0_0; }
	inline void set_U3CwidthU3E__0_0(int32_t value)
	{
		___U3CwidthU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CheightU3E__0_1() { return static_cast<int32_t>(offsetof(U3CIMPL_sendCaptureU3Ec__Iterator0_t1292966789, ___U3CheightU3E__0_1)); }
	inline int32_t get_U3CheightU3E__0_1() const { return ___U3CheightU3E__0_1; }
	inline int32_t* get_address_of_U3CheightU3E__0_1() { return &___U3CheightU3E__0_1; }
	inline void set_U3CheightU3E__0_1(int32_t value)
	{
		___U3CheightU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U3CrendererBackU3E__0_2() { return static_cast<int32_t>(offsetof(U3CIMPL_sendCaptureU3Ec__Iterator0_t1292966789, ___U3CrendererBackU3E__0_2)); }
	inline Renderer_t2627027031 * get_U3CrendererBackU3E__0_2() const { return ___U3CrendererBackU3E__0_2; }
	inline Renderer_t2627027031 ** get_address_of_U3CrendererBackU3E__0_2() { return &___U3CrendererBackU3E__0_2; }
	inline void set_U3CrendererBackU3E__0_2(Renderer_t2627027031 * value)
	{
		___U3CrendererBackU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrendererBackU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U3CtexU3E__0_3() { return static_cast<int32_t>(offsetof(U3CIMPL_sendCaptureU3Ec__Iterator0_t1292966789, ___U3CtexU3E__0_3)); }
	inline Texture2D_t3840446185 * get_U3CtexU3E__0_3() const { return ___U3CtexU3E__0_3; }
	inline Texture2D_t3840446185 ** get_address_of_U3CtexU3E__0_3() { return &___U3CtexU3E__0_3; }
	inline void set_U3CtexU3E__0_3(Texture2D_t3840446185 * value)
	{
		___U3CtexU3E__0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtexU3E__0_3), value);
	}

	inline static int32_t get_offset_of_U3CbytesU3E__0_4() { return static_cast<int32_t>(offsetof(U3CIMPL_sendCaptureU3Ec__Iterator0_t1292966789, ___U3CbytesU3E__0_4)); }
	inline ByteU5BU5D_t4116647657* get_U3CbytesU3E__0_4() const { return ___U3CbytesU3E__0_4; }
	inline ByteU5BU5D_t4116647657** get_address_of_U3CbytesU3E__0_4() { return &___U3CbytesU3E__0_4; }
	inline void set_U3CbytesU3E__0_4(ByteU5BU5D_t4116647657* value)
	{
		___U3CbytesU3E__0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CbytesU3E__0_4), value);
	}

	inline static int32_t get_offset_of_U3CformU3E__0_5() { return static_cast<int32_t>(offsetof(U3CIMPL_sendCaptureU3Ec__Iterator0_t1292966789, ___U3CformU3E__0_5)); }
	inline WWWForm_t4064702195 * get_U3CformU3E__0_5() const { return ___U3CformU3E__0_5; }
	inline WWWForm_t4064702195 ** get_address_of_U3CformU3E__0_5() { return &___U3CformU3E__0_5; }
	inline void set_U3CformU3E__0_5(WWWForm_t4064702195 * value)
	{
		___U3CformU3E__0_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CformU3E__0_5), value);
	}

	inline static int32_t get_offset_of_U3CwU3E__0_6() { return static_cast<int32_t>(offsetof(U3CIMPL_sendCaptureU3Ec__Iterator0_t1292966789, ___U3CwU3E__0_6)); }
	inline UnityWebRequest_t463507806 * get_U3CwU3E__0_6() const { return ___U3CwU3E__0_6; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CwU3E__0_6() { return &___U3CwU3E__0_6; }
	inline void set_U3CwU3E__0_6(UnityWebRequest_t463507806 * value)
	{
		___U3CwU3E__0_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwU3E__0_6), value);
	}

	inline static int32_t get_offset_of_U24this_7() { return static_cast<int32_t>(offsetof(U3CIMPL_sendCaptureU3Ec__Iterator0_t1292966789, ___U24this_7)); }
	inline captureAndSendScreen_t3196082656 * get_U24this_7() const { return ___U24this_7; }
	inline captureAndSendScreen_t3196082656 ** get_address_of_U24this_7() { return &___U24this_7; }
	inline void set_U24this_7(captureAndSendScreen_t3196082656 * value)
	{
		___U24this_7 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_7), value);
	}

	inline static int32_t get_offset_of_U24current_8() { return static_cast<int32_t>(offsetof(U3CIMPL_sendCaptureU3Ec__Iterator0_t1292966789, ___U24current_8)); }
	inline RuntimeObject * get_U24current_8() const { return ___U24current_8; }
	inline RuntimeObject ** get_address_of_U24current_8() { return &___U24current_8; }
	inline void set_U24current_8(RuntimeObject * value)
	{
		___U24current_8 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_8), value);
	}

	inline static int32_t get_offset_of_U24disposing_9() { return static_cast<int32_t>(offsetof(U3CIMPL_sendCaptureU3Ec__Iterator0_t1292966789, ___U24disposing_9)); }
	inline bool get_U24disposing_9() const { return ___U24disposing_9; }
	inline bool* get_address_of_U24disposing_9() { return &___U24disposing_9; }
	inline void set_U24disposing_9(bool value)
	{
		___U24disposing_9 = value;
	}

	inline static int32_t get_offset_of_U24PC_10() { return static_cast<int32_t>(offsetof(U3CIMPL_sendCaptureU3Ec__Iterator0_t1292966789, ___U24PC_10)); }
	inline int32_t get_U24PC_10() const { return ___U24PC_10; }
	inline int32_t* get_address_of_U24PC_10() { return &___U24PC_10; }
	inline void set_U24PC_10(int32_t value)
	{
		___U24PC_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CIMPL_SENDCAPTUREU3EC__ITERATOR0_T1292966789_H
#ifndef U3CGETLANGUAGEJSONU3EC__ITERATOR0_T3550365120_H
#define U3CGETLANGUAGEJSONU3EC__ITERATOR0_T3550365120_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// startUpLANG/<GetLanguageJSON>c__Iterator0
struct  U3CGetLanguageJSONU3Ec__Iterator0_t3550365120  : public RuntimeObject
{
public:
	// System.String startUpLANG/<GetLanguageJSON>c__Iterator0::<url>__0
	String_t* ___U3CurlU3E__0_0;
	// UnityEngine.WWW startUpLANG/<GetLanguageJSON>c__Iterator0::<www>__0
	WWW_t3688466362 * ___U3CwwwU3E__0_1;
	// MarkerManager startUpLANG/<GetLanguageJSON>c__Iterator0::<manager>__0
	MarkerManager_t1031803141 * ___U3CmanagerU3E__0_2;
	// startUpLANG startUpLANG/<GetLanguageJSON>c__Iterator0::$this
	startUpLANG_t1517666504 * ___U24this_3;
	// System.Object startUpLANG/<GetLanguageJSON>c__Iterator0::$current
	RuntimeObject * ___U24current_4;
	// System.Boolean startUpLANG/<GetLanguageJSON>c__Iterator0::$disposing
	bool ___U24disposing_5;
	// System.Int32 startUpLANG/<GetLanguageJSON>c__Iterator0::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_U3CurlU3E__0_0() { return static_cast<int32_t>(offsetof(U3CGetLanguageJSONU3Ec__Iterator0_t3550365120, ___U3CurlU3E__0_0)); }
	inline String_t* get_U3CurlU3E__0_0() const { return ___U3CurlU3E__0_0; }
	inline String_t** get_address_of_U3CurlU3E__0_0() { return &___U3CurlU3E__0_0; }
	inline void set_U3CurlU3E__0_0(String_t* value)
	{
		___U3CurlU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CurlU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E__0_1() { return static_cast<int32_t>(offsetof(U3CGetLanguageJSONU3Ec__Iterator0_t3550365120, ___U3CwwwU3E__0_1)); }
	inline WWW_t3688466362 * get_U3CwwwU3E__0_1() const { return ___U3CwwwU3E__0_1; }
	inline WWW_t3688466362 ** get_address_of_U3CwwwU3E__0_1() { return &___U3CwwwU3E__0_1; }
	inline void set_U3CwwwU3E__0_1(WWW_t3688466362 * value)
	{
		___U3CwwwU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwwwU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U3CmanagerU3E__0_2() { return static_cast<int32_t>(offsetof(U3CGetLanguageJSONU3Ec__Iterator0_t3550365120, ___U3CmanagerU3E__0_2)); }
	inline MarkerManager_t1031803141 * get_U3CmanagerU3E__0_2() const { return ___U3CmanagerU3E__0_2; }
	inline MarkerManager_t1031803141 ** get_address_of_U3CmanagerU3E__0_2() { return &___U3CmanagerU3E__0_2; }
	inline void set_U3CmanagerU3E__0_2(MarkerManager_t1031803141 * value)
	{
		___U3CmanagerU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmanagerU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CGetLanguageJSONU3Ec__Iterator0_t3550365120, ___U24this_3)); }
	inline startUpLANG_t1517666504 * get_U24this_3() const { return ___U24this_3; }
	inline startUpLANG_t1517666504 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(startUpLANG_t1517666504 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CGetLanguageJSONU3Ec__Iterator0_t3550365120, ___U24current_4)); }
	inline RuntimeObject * get_U24current_4() const { return ___U24current_4; }
	inline RuntimeObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(RuntimeObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CGetLanguageJSONU3Ec__Iterator0_t3550365120, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CGetLanguageJSONU3Ec__Iterator0_t3550365120, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETLANGUAGEJSONU3EC__ITERATOR0_T3550365120_H
#ifndef BOOLEAN_T97287965_H
#define BOOLEAN_T97287965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t97287965 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Boolean_t97287965, ___m_value_2)); }
	inline bool get_m_value_2() const { return ___m_value_2; }
	inline bool* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(bool value)
	{
		___m_value_2 = value;
	}
};

struct Boolean_t97287965_StaticFields
{
public:
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_0;
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_1;

public:
	inline static int32_t get_offset_of_FalseString_0() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___FalseString_0)); }
	inline String_t* get_FalseString_0() const { return ___FalseString_0; }
	inline String_t** get_address_of_FalseString_0() { return &___FalseString_0; }
	inline void set_FalseString_0(String_t* value)
	{
		___FalseString_0 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_0), value);
	}

	inline static int32_t get_offset_of_TrueString_1() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___TrueString_1)); }
	inline String_t* get_TrueString_1() const { return ___TrueString_1; }
	inline String_t** get_address_of_TrueString_1() { return &___TrueString_1; }
	inline void set_TrueString_1(String_t* value)
	{
		___TrueString_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T97287965_H
#ifndef BYTE_T1134296376_H
#define BYTE_T1134296376_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Byte
struct  Byte_t1134296376 
{
public:
	// System.Byte System.Byte::m_value
	uint8_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Byte_t1134296376, ___m_value_2)); }
	inline uint8_t get_m_value_2() const { return ___m_value_2; }
	inline uint8_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(uint8_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BYTE_T1134296376_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef INT32_T2950945753_H
#define INT32_T2950945753_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t2950945753 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int32_t2950945753, ___m_value_2)); }
	inline int32_t get_m_value_2() const { return ___m_value_2; }
	inline int32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T2950945753_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef SINGLE_T1397266774_H
#define SINGLE_T1397266774_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_t1397266774 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_7;

public:
	inline static int32_t get_offset_of_m_value_7() { return static_cast<int32_t>(offsetof(Single_t1397266774, ___m_value_7)); }
	inline float get_m_value_7() const { return ___m_value_7; }
	inline float* get_address_of_m_value_7() { return &___m_value_7; }
	inline void set_m_value_7(float value)
	{
		___m_value_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_T1397266774_H
#ifndef SYSTEMEXCEPTION_T176217640_H
#define SYSTEMEXCEPTION_T176217640_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.SystemException
struct  SystemException_t176217640  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMEXCEPTION_T176217640_H
#ifndef UINT32_T2560061978_H
#define UINT32_T2560061978_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UInt32
struct  UInt32_t2560061978 
{
public:
	// System.UInt32 System.UInt32::m_value
	uint32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(UInt32_t2560061978, ___m_value_2)); }
	inline uint32_t get_m_value_2() const { return ___m_value_2; }
	inline uint32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(uint32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UINT32_T2560061978_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef MATRIX4X4_T1817901843_H
#define MATRIX4X4_T1817901843_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Matrix4x4
struct  Matrix4x4_t1817901843 
{
public:
	// System.Single UnityEngine.Matrix4x4::m00
	float ___m00_0;
	// System.Single UnityEngine.Matrix4x4::m10
	float ___m10_1;
	// System.Single UnityEngine.Matrix4x4::m20
	float ___m20_2;
	// System.Single UnityEngine.Matrix4x4::m30
	float ___m30_3;
	// System.Single UnityEngine.Matrix4x4::m01
	float ___m01_4;
	// System.Single UnityEngine.Matrix4x4::m11
	float ___m11_5;
	// System.Single UnityEngine.Matrix4x4::m21
	float ___m21_6;
	// System.Single UnityEngine.Matrix4x4::m31
	float ___m31_7;
	// System.Single UnityEngine.Matrix4x4::m02
	float ___m02_8;
	// System.Single UnityEngine.Matrix4x4::m12
	float ___m12_9;
	// System.Single UnityEngine.Matrix4x4::m22
	float ___m22_10;
	// System.Single UnityEngine.Matrix4x4::m32
	float ___m32_11;
	// System.Single UnityEngine.Matrix4x4::m03
	float ___m03_12;
	// System.Single UnityEngine.Matrix4x4::m13
	float ___m13_13;
	// System.Single UnityEngine.Matrix4x4::m23
	float ___m23_14;
	// System.Single UnityEngine.Matrix4x4::m33
	float ___m33_15;

public:
	inline static int32_t get_offset_of_m00_0() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m00_0)); }
	inline float get_m00_0() const { return ___m00_0; }
	inline float* get_address_of_m00_0() { return &___m00_0; }
	inline void set_m00_0(float value)
	{
		___m00_0 = value;
	}

	inline static int32_t get_offset_of_m10_1() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m10_1)); }
	inline float get_m10_1() const { return ___m10_1; }
	inline float* get_address_of_m10_1() { return &___m10_1; }
	inline void set_m10_1(float value)
	{
		___m10_1 = value;
	}

	inline static int32_t get_offset_of_m20_2() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m20_2)); }
	inline float get_m20_2() const { return ___m20_2; }
	inline float* get_address_of_m20_2() { return &___m20_2; }
	inline void set_m20_2(float value)
	{
		___m20_2 = value;
	}

	inline static int32_t get_offset_of_m30_3() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m30_3)); }
	inline float get_m30_3() const { return ___m30_3; }
	inline float* get_address_of_m30_3() { return &___m30_3; }
	inline void set_m30_3(float value)
	{
		___m30_3 = value;
	}

	inline static int32_t get_offset_of_m01_4() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m01_4)); }
	inline float get_m01_4() const { return ___m01_4; }
	inline float* get_address_of_m01_4() { return &___m01_4; }
	inline void set_m01_4(float value)
	{
		___m01_4 = value;
	}

	inline static int32_t get_offset_of_m11_5() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m11_5)); }
	inline float get_m11_5() const { return ___m11_5; }
	inline float* get_address_of_m11_5() { return &___m11_5; }
	inline void set_m11_5(float value)
	{
		___m11_5 = value;
	}

	inline static int32_t get_offset_of_m21_6() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m21_6)); }
	inline float get_m21_6() const { return ___m21_6; }
	inline float* get_address_of_m21_6() { return &___m21_6; }
	inline void set_m21_6(float value)
	{
		___m21_6 = value;
	}

	inline static int32_t get_offset_of_m31_7() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m31_7)); }
	inline float get_m31_7() const { return ___m31_7; }
	inline float* get_address_of_m31_7() { return &___m31_7; }
	inline void set_m31_7(float value)
	{
		___m31_7 = value;
	}

	inline static int32_t get_offset_of_m02_8() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m02_8)); }
	inline float get_m02_8() const { return ___m02_8; }
	inline float* get_address_of_m02_8() { return &___m02_8; }
	inline void set_m02_8(float value)
	{
		___m02_8 = value;
	}

	inline static int32_t get_offset_of_m12_9() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m12_9)); }
	inline float get_m12_9() const { return ___m12_9; }
	inline float* get_address_of_m12_9() { return &___m12_9; }
	inline void set_m12_9(float value)
	{
		___m12_9 = value;
	}

	inline static int32_t get_offset_of_m22_10() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m22_10)); }
	inline float get_m22_10() const { return ___m22_10; }
	inline float* get_address_of_m22_10() { return &___m22_10; }
	inline void set_m22_10(float value)
	{
		___m22_10 = value;
	}

	inline static int32_t get_offset_of_m32_11() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m32_11)); }
	inline float get_m32_11() const { return ___m32_11; }
	inline float* get_address_of_m32_11() { return &___m32_11; }
	inline void set_m32_11(float value)
	{
		___m32_11 = value;
	}

	inline static int32_t get_offset_of_m03_12() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m03_12)); }
	inline float get_m03_12() const { return ___m03_12; }
	inline float* get_address_of_m03_12() { return &___m03_12; }
	inline void set_m03_12(float value)
	{
		___m03_12 = value;
	}

	inline static int32_t get_offset_of_m13_13() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m13_13)); }
	inline float get_m13_13() const { return ___m13_13; }
	inline float* get_address_of_m13_13() { return &___m13_13; }
	inline void set_m13_13(float value)
	{
		___m13_13 = value;
	}

	inline static int32_t get_offset_of_m23_14() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m23_14)); }
	inline float get_m23_14() const { return ___m23_14; }
	inline float* get_address_of_m23_14() { return &___m23_14; }
	inline void set_m23_14(float value)
	{
		___m23_14 = value;
	}

	inline static int32_t get_offset_of_m33_15() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m33_15)); }
	inline float get_m33_15() const { return ___m33_15; }
	inline float* get_address_of_m33_15() { return &___m33_15; }
	inline void set_m33_15(float value)
	{
		___m33_15 = value;
	}
};

struct Matrix4x4_t1817901843_StaticFields
{
public:
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::zeroMatrix
	Matrix4x4_t1817901843  ___zeroMatrix_16;
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::identityMatrix
	Matrix4x4_t1817901843  ___identityMatrix_17;

public:
	inline static int32_t get_offset_of_zeroMatrix_16() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843_StaticFields, ___zeroMatrix_16)); }
	inline Matrix4x4_t1817901843  get_zeroMatrix_16() const { return ___zeroMatrix_16; }
	inline Matrix4x4_t1817901843 * get_address_of_zeroMatrix_16() { return &___zeroMatrix_16; }
	inline void set_zeroMatrix_16(Matrix4x4_t1817901843  value)
	{
		___zeroMatrix_16 = value;
	}

	inline static int32_t get_offset_of_identityMatrix_17() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843_StaticFields, ___identityMatrix_17)); }
	inline Matrix4x4_t1817901843  get_identityMatrix_17() const { return ___identityMatrix_17; }
	inline Matrix4x4_t1817901843 * get_address_of_identityMatrix_17() { return &___identityMatrix_17; }
	inline void set_identityMatrix_17(Matrix4x4_t1817901843  value)
	{
		___identityMatrix_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATRIX4X4_T1817901843_H
#ifndef QUATERNION_T2301928331_H
#define QUATERNION_T2301928331_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t2301928331 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t2301928331_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t2301928331  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t2301928331  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t2301928331 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t2301928331  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T2301928331_H
#ifndef RECT_T2360479859_H
#define RECT_T2360479859_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rect
struct  Rect_t2360479859 
{
public:
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_XMin_0)); }
	inline float get_m_XMin_0() const { return ___m_XMin_0; }
	inline float* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(float value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_YMin_1)); }
	inline float get_m_YMin_1() const { return ___m_YMin_1; }
	inline float* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(float value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_Width_2)); }
	inline float get_m_Width_2() const { return ___m_Width_2; }
	inline float* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(float value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_Height_3)); }
	inline float get_m_Height_3() const { return ___m_Height_3; }
	inline float* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(float value)
	{
		___m_Height_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECT_T2360479859_H
#ifndef SPRITESTATE_T1362986479_H
#define SPRITESTATE_T1362986479_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.SpriteState
struct  SpriteState_t1362986479 
{
public:
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_HighlightedSprite
	Sprite_t280657092 * ___m_HighlightedSprite_0;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_PressedSprite
	Sprite_t280657092 * ___m_PressedSprite_1;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_DisabledSprite
	Sprite_t280657092 * ___m_DisabledSprite_2;

public:
	inline static int32_t get_offset_of_m_HighlightedSprite_0() { return static_cast<int32_t>(offsetof(SpriteState_t1362986479, ___m_HighlightedSprite_0)); }
	inline Sprite_t280657092 * get_m_HighlightedSprite_0() const { return ___m_HighlightedSprite_0; }
	inline Sprite_t280657092 ** get_address_of_m_HighlightedSprite_0() { return &___m_HighlightedSprite_0; }
	inline void set_m_HighlightedSprite_0(Sprite_t280657092 * value)
	{
		___m_HighlightedSprite_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_HighlightedSprite_0), value);
	}

	inline static int32_t get_offset_of_m_PressedSprite_1() { return static_cast<int32_t>(offsetof(SpriteState_t1362986479, ___m_PressedSprite_1)); }
	inline Sprite_t280657092 * get_m_PressedSprite_1() const { return ___m_PressedSprite_1; }
	inline Sprite_t280657092 ** get_address_of_m_PressedSprite_1() { return &___m_PressedSprite_1; }
	inline void set_m_PressedSprite_1(Sprite_t280657092 * value)
	{
		___m_PressedSprite_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PressedSprite_1), value);
	}

	inline static int32_t get_offset_of_m_DisabledSprite_2() { return static_cast<int32_t>(offsetof(SpriteState_t1362986479, ___m_DisabledSprite_2)); }
	inline Sprite_t280657092 * get_m_DisabledSprite_2() const { return ___m_DisabledSprite_2; }
	inline Sprite_t280657092 ** get_address_of_m_DisabledSprite_2() { return &___m_DisabledSprite_2; }
	inline void set_m_DisabledSprite_2(Sprite_t280657092 * value)
	{
		___m_DisabledSprite_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_DisabledSprite_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.UI.SpriteState
struct SpriteState_t1362986479_marshaled_pinvoke
{
	Sprite_t280657092 * ___m_HighlightedSprite_0;
	Sprite_t280657092 * ___m_PressedSprite_1;
	Sprite_t280657092 * ___m_DisabledSprite_2;
};
// Native definition for COM marshalling of UnityEngine.UI.SpriteState
struct SpriteState_t1362986479_marshaled_com
{
	Sprite_t280657092 * ___m_HighlightedSprite_0;
	Sprite_t280657092 * ___m_PressedSprite_1;
	Sprite_t280657092 * ___m_DisabledSprite_2;
};
#endif // SPRITESTATE_T1362986479_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_5)); }
	inline Vector3_t3722313464  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_t3722313464  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_6)); }
	inline Vector3_t3722313464  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_t3722313464 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_t3722313464  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_7)); }
	inline Vector3_t3722313464  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_t3722313464 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_t3722313464  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_8)); }
	inline Vector3_t3722313464  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_t3722313464 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_t3722313464  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_9)); }
	inline Vector3_t3722313464  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_t3722313464 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_t3722313464  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_10)); }
	inline Vector3_t3722313464  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_t3722313464 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_t3722313464  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_11)); }
	inline Vector3_t3722313464  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_t3722313464  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_12)); }
	inline Vector3_t3722313464  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_t3722313464 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_t3722313464  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef PIXEL_T2713956838_H
#define PIXEL_T2713956838_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pixel
struct  Pixel_t2713956838  : public RuntimeObject
{
public:
	// System.Int32 Pixel::i
	int32_t ___i_0;
	// System.Int32 Pixel::j
	int32_t ___j_1;
	// UnityEngine.Color Pixel::couleur
	Color_t2555686324  ___couleur_2;

public:
	inline static int32_t get_offset_of_i_0() { return static_cast<int32_t>(offsetof(Pixel_t2713956838, ___i_0)); }
	inline int32_t get_i_0() const { return ___i_0; }
	inline int32_t* get_address_of_i_0() { return &___i_0; }
	inline void set_i_0(int32_t value)
	{
		___i_0 = value;
	}

	inline static int32_t get_offset_of_j_1() { return static_cast<int32_t>(offsetof(Pixel_t2713956838, ___j_1)); }
	inline int32_t get_j_1() const { return ___j_1; }
	inline int32_t* get_address_of_j_1() { return &___j_1; }
	inline void set_j_1(int32_t value)
	{
		___j_1 = value;
	}

	inline static int32_t get_offset_of_couleur_2() { return static_cast<int32_t>(offsetof(Pixel_t2713956838, ___couleur_2)); }
	inline Color_t2555686324  get_couleur_2() const { return ___couleur_2; }
	inline Color_t2555686324 * get_address_of_couleur_2() { return &___couleur_2; }
	inline void set_couleur_2(Color_t2555686324  value)
	{
		___couleur_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PIXEL_T2713956838_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_8)); }
	inline DelegateData_t1677132599 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1677132599 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1677132599 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T1188392813_H
#ifndef NOTSUPPORTEDEXCEPTION_T1314879016_H
#define NOTSUPPORTEDEXCEPTION_T1314879016_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.NotSupportedException
struct  NotSupportedException_t1314879016  : public SystemException_t176217640
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOTSUPPORTEDEXCEPTION_T1314879016_H
#ifndef TEXTURECOMPRESSION_T1528154288_H
#define TEXTURECOMPRESSION_T1528154288_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.TextureCompression
struct  TextureCompression_t1528154288 
{
public:
	// System.Int32 TriLib.TextureCompression::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TextureCompression_t1528154288, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTURECOMPRESSION_T1528154288_H
#ifndef ASYNCOPERATION_T1445031843_H
#define ASYNCOPERATION_T1445031843_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AsyncOperation
struct  AsyncOperation_t1445031843  : public YieldInstruction_t403091072
{
public:
	// System.IntPtr UnityEngine.AsyncOperation::m_Ptr
	intptr_t ___m_Ptr_0;
	// System.Action`1<UnityEngine.AsyncOperation> UnityEngine.AsyncOperation::m_completeCallback
	Action_1_t1617499438 * ___m_completeCallback_1;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(AsyncOperation_t1445031843, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}

	inline static int32_t get_offset_of_m_completeCallback_1() { return static_cast<int32_t>(offsetof(AsyncOperation_t1445031843, ___m_completeCallback_1)); }
	inline Action_1_t1617499438 * get_m_completeCallback_1() const { return ___m_completeCallback_1; }
	inline Action_1_t1617499438 ** get_address_of_m_completeCallback_1() { return &___m_completeCallback_1; }
	inline void set_m_completeCallback_1(Action_1_t1617499438 * value)
	{
		___m_completeCallback_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_completeCallback_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.AsyncOperation
struct AsyncOperation_t1445031843_marshaled_pinvoke : public YieldInstruction_t403091072_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
	Il2CppMethodPointer ___m_completeCallback_1;
};
// Native definition for COM marshalling of UnityEngine.AsyncOperation
struct AsyncOperation_t1445031843_marshaled_com : public YieldInstruction_t403091072_marshaled_com
{
	intptr_t ___m_Ptr_0;
	Il2CppMethodPointer ___m_completeCallback_1;
};
#endif // ASYNCOPERATION_T1445031843_H
#ifndef BOUNDS_T2266837910_H
#define BOUNDS_T2266837910_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Bounds
struct  Bounds_t2266837910 
{
public:
	// UnityEngine.Vector3 UnityEngine.Bounds::m_Center
	Vector3_t3722313464  ___m_Center_0;
	// UnityEngine.Vector3 UnityEngine.Bounds::m_Extents
	Vector3_t3722313464  ___m_Extents_1;

public:
	inline static int32_t get_offset_of_m_Center_0() { return static_cast<int32_t>(offsetof(Bounds_t2266837910, ___m_Center_0)); }
	inline Vector3_t3722313464  get_m_Center_0() const { return ___m_Center_0; }
	inline Vector3_t3722313464 * get_address_of_m_Center_0() { return &___m_Center_0; }
	inline void set_m_Center_0(Vector3_t3722313464  value)
	{
		___m_Center_0 = value;
	}

	inline static int32_t get_offset_of_m_Extents_1() { return static_cast<int32_t>(offsetof(Bounds_t2266837910, ___m_Extents_1)); }
	inline Vector3_t3722313464  get_m_Extents_1() const { return ___m_Extents_1; }
	inline Vector3_t3722313464 * get_address_of_m_Extents_1() { return &___m_Extents_1; }
	inline void set_m_Extents_1(Vector3_t3722313464  value)
	{
		___m_Extents_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOUNDS_T2266837910_H
#ifndef COROUTINE_T3829159415_H
#define COROUTINE_T3829159415_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Coroutine
struct  Coroutine_t3829159415  : public YieldInstruction_t403091072
{
public:
	// System.IntPtr UnityEngine.Coroutine::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(Coroutine_t3829159415, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Coroutine
struct Coroutine_t3829159415_marshaled_pinvoke : public YieldInstruction_t403091072_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.Coroutine
struct Coroutine_t3829159415_marshaled_com : public YieldInstruction_t403091072_marshaled_com
{
	intptr_t ___m_Ptr_0;
};
#endif // COROUTINE_T3829159415_H
#ifndef DRIVENTRANSFORMPROPERTIES_T3813433528_H
#define DRIVENTRANSFORMPROPERTIES_T3813433528_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.DrivenTransformProperties
struct  DrivenTransformProperties_t3813433528 
{
public:
	// System.Int32 UnityEngine.DrivenTransformProperties::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DrivenTransformProperties_t3813433528, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRIVENTRANSFORMPROPERTIES_T3813433528_H
#ifndef KEYCODE_T2599294277_H
#define KEYCODE_T2599294277_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.KeyCode
struct  KeyCode_t2599294277 
{
public:
	// System.Int32 UnityEngine.KeyCode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(KeyCode_t2599294277, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYCODE_T2599294277_H
#ifndef CERTIFICATEHANDLER_T2739891000_H
#define CERTIFICATEHANDLER_T2739891000_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.CertificateHandler
struct  CertificateHandler_t2739891000  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Networking.CertificateHandler::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(CertificateHandler_t2739891000, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Networking.CertificateHandler
struct CertificateHandler_t2739891000_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.Networking.CertificateHandler
struct CertificateHandler_t2739891000_marshaled_com
{
	intptr_t ___m_Ptr_0;
};
#endif // CERTIFICATEHANDLER_T2739891000_H
#ifndef DOWNLOADHANDLER_T2937767557_H
#define DOWNLOADHANDLER_T2937767557_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.DownloadHandler
struct  DownloadHandler_t2937767557  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Networking.DownloadHandler::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(DownloadHandler_t2937767557, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Networking.DownloadHandler
struct DownloadHandler_t2937767557_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.Networking.DownloadHandler
struct DownloadHandler_t2937767557_marshaled_com
{
	intptr_t ___m_Ptr_0;
};
#endif // DOWNLOADHANDLER_T2937767557_H
#ifndef UPLOADHANDLER_T2993558019_H
#define UPLOADHANDLER_T2993558019_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.UploadHandler
struct  UploadHandler_t2993558019  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Networking.UploadHandler::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(UploadHandler_t2993558019, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Networking.UploadHandler
struct UploadHandler_t2993558019_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.Networking.UploadHandler
struct UploadHandler_t2993558019_marshaled_com
{
	intptr_t ___m_Ptr_0;
};
#endif // UPLOADHANDLER_T2993558019_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef RUNTIMEPLATFORM_T4159857903_H
#define RUNTIMEPLATFORM_T4159857903_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RuntimePlatform
struct  RuntimePlatform_t4159857903 
{
public:
	// System.Int32 UnityEngine.RuntimePlatform::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(RuntimePlatform_t4159857903, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEPLATFORM_T4159857903_H
#ifndef TEXTUREFORMAT_T2701165832_H
#define TEXTUREFORMAT_T2701165832_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TextureFormat
struct  TextureFormat_t2701165832 
{
public:
	// System.Int32 UnityEngine.TextureFormat::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TextureFormat_t2701165832, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTUREFORMAT_T2701165832_H
#ifndef THREADPRIORITY_T1774350854_H
#define THREADPRIORITY_T1774350854_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ThreadPriority
struct  ThreadPriority_t1774350854 
{
public:
	// System.Int32 UnityEngine.ThreadPriority::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ThreadPriority_t1774350854, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // THREADPRIORITY_T1774350854_H
#ifndef TOUCHPHASE_T72348083_H
#define TOUCHPHASE_T72348083_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TouchPhase
struct  TouchPhase_t72348083 
{
public:
	// System.Int32 UnityEngine.TouchPhase::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TouchPhase_t72348083, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHPHASE_T72348083_H
#ifndef TOUCHSCREENKEYBOARDTYPE_T1530597702_H
#define TOUCHSCREENKEYBOARDTYPE_T1530597702_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TouchScreenKeyboardType
struct  TouchScreenKeyboardType_t1530597702 
{
public:
	// System.Int32 UnityEngine.TouchScreenKeyboardType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TouchScreenKeyboardType_t1530597702, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHSCREENKEYBOARDTYPE_T1530597702_H
#ifndef TOUCHTYPE_T2034578258_H
#define TOUCHTYPE_T2034578258_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TouchType
struct  TouchType_t2034578258 
{
public:
	// System.Int32 UnityEngine.TouchType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TouchType_t2034578258, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHTYPE_T2034578258_H
#ifndef COLORBLOCK_T2139031574_H
#define COLORBLOCK_T2139031574_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ColorBlock
struct  ColorBlock_t2139031574 
{
public:
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_NormalColor
	Color_t2555686324  ___m_NormalColor_0;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_HighlightedColor
	Color_t2555686324  ___m_HighlightedColor_1;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_PressedColor
	Color_t2555686324  ___m_PressedColor_2;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_DisabledColor
	Color_t2555686324  ___m_DisabledColor_3;
	// System.Single UnityEngine.UI.ColorBlock::m_ColorMultiplier
	float ___m_ColorMultiplier_4;
	// System.Single UnityEngine.UI.ColorBlock::m_FadeDuration
	float ___m_FadeDuration_5;

public:
	inline static int32_t get_offset_of_m_NormalColor_0() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_NormalColor_0)); }
	inline Color_t2555686324  get_m_NormalColor_0() const { return ___m_NormalColor_0; }
	inline Color_t2555686324 * get_address_of_m_NormalColor_0() { return &___m_NormalColor_0; }
	inline void set_m_NormalColor_0(Color_t2555686324  value)
	{
		___m_NormalColor_0 = value;
	}

	inline static int32_t get_offset_of_m_HighlightedColor_1() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_HighlightedColor_1)); }
	inline Color_t2555686324  get_m_HighlightedColor_1() const { return ___m_HighlightedColor_1; }
	inline Color_t2555686324 * get_address_of_m_HighlightedColor_1() { return &___m_HighlightedColor_1; }
	inline void set_m_HighlightedColor_1(Color_t2555686324  value)
	{
		___m_HighlightedColor_1 = value;
	}

	inline static int32_t get_offset_of_m_PressedColor_2() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_PressedColor_2)); }
	inline Color_t2555686324  get_m_PressedColor_2() const { return ___m_PressedColor_2; }
	inline Color_t2555686324 * get_address_of_m_PressedColor_2() { return &___m_PressedColor_2; }
	inline void set_m_PressedColor_2(Color_t2555686324  value)
	{
		___m_PressedColor_2 = value;
	}

	inline static int32_t get_offset_of_m_DisabledColor_3() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_DisabledColor_3)); }
	inline Color_t2555686324  get_m_DisabledColor_3() const { return ___m_DisabledColor_3; }
	inline Color_t2555686324 * get_address_of_m_DisabledColor_3() { return &___m_DisabledColor_3; }
	inline void set_m_DisabledColor_3(Color_t2555686324  value)
	{
		___m_DisabledColor_3 = value;
	}

	inline static int32_t get_offset_of_m_ColorMultiplier_4() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_ColorMultiplier_4)); }
	inline float get_m_ColorMultiplier_4() const { return ___m_ColorMultiplier_4; }
	inline float* get_address_of_m_ColorMultiplier_4() { return &___m_ColorMultiplier_4; }
	inline void set_m_ColorMultiplier_4(float value)
	{
		___m_ColorMultiplier_4 = value;
	}

	inline static int32_t get_offset_of_m_FadeDuration_5() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_FadeDuration_5)); }
	inline float get_m_FadeDuration_5() const { return ___m_FadeDuration_5; }
	inline float* get_address_of_m_FadeDuration_5() { return &___m_FadeDuration_5; }
	inline void set_m_FadeDuration_5(float value)
	{
		___m_FadeDuration_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORBLOCK_T2139031574_H
#ifndef FILLMETHOD_T1167457570_H
#define FILLMETHOD_T1167457570_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Image/FillMethod
struct  FillMethod_t1167457570 
{
public:
	// System.Int32 UnityEngine.UI.Image/FillMethod::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FillMethod_t1167457570, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILLMETHOD_T1167457570_H
#ifndef TYPE_T1152881528_H
#define TYPE_T1152881528_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Image/Type
struct  Type_t1152881528 
{
public:
	// System.Int32 UnityEngine.UI.Image/Type::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Type_t1152881528, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE_T1152881528_H
#ifndef CHARACTERVALIDATION_T4051914437_H
#define CHARACTERVALIDATION_T4051914437_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.InputField/CharacterValidation
struct  CharacterValidation_t4051914437 
{
public:
	// System.Int32 UnityEngine.UI.InputField/CharacterValidation::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CharacterValidation_t4051914437, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHARACTERVALIDATION_T4051914437_H
#ifndef CONTENTTYPE_T1787303396_H
#define CONTENTTYPE_T1787303396_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.InputField/ContentType
struct  ContentType_t1787303396 
{
public:
	// System.Int32 UnityEngine.UI.InputField/ContentType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ContentType_t1787303396, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTENTTYPE_T1787303396_H
#ifndef INPUTTYPE_T1770400679_H
#define INPUTTYPE_T1770400679_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.InputField/InputType
struct  InputType_t1770400679 
{
public:
	// System.Int32 UnityEngine.UI.InputField/InputType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(InputType_t1770400679, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUTTYPE_T1770400679_H
#ifndef LINETYPE_T4214648469_H
#define LINETYPE_T4214648469_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.InputField/LineType
struct  LineType_t4214648469 
{
public:
	// System.Int32 UnityEngine.UI.InputField/LineType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(LineType_t4214648469, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINETYPE_T4214648469_H
#ifndef MODE_T1066900953_H
#define MODE_T1066900953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Navigation/Mode
struct  Mode_t1066900953 
{
public:
	// System.Int32 UnityEngine.UI.Navigation/Mode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Mode_t1066900953, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODE_T1066900953_H
#ifndef SELECTIONSTATE_T2656606514_H
#define SELECTIONSTATE_T2656606514_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Selectable/SelectionState
struct  SelectionState_t2656606514 
{
public:
	// System.Int32 UnityEngine.UI.Selectable/SelectionState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SelectionState_t2656606514, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTIONSTATE_T2656606514_H
#ifndef TRANSITION_T1769908631_H
#define TRANSITION_T1769908631_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Selectable/Transition
struct  Transition_t1769908631 
{
public:
	// System.Int32 UnityEngine.UI.Selectable/Transition::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Transition_t1769908631, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSITION_T1769908631_H
#ifndef PIXEL_FORMAT_T3209881435_H
#define PIXEL_FORMAT_T3209881435_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.Image/PIXEL_FORMAT
struct  PIXEL_FORMAT_t3209881435 
{
public:
	// System.Int32 Vuforia.Image/PIXEL_FORMAT::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PIXEL_FORMAT_t3209881435, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PIXEL_FORMAT_T3209881435_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___prev_9)); }
	inline MulticastDelegate_t * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___kpm_next_10)); }
	inline MulticastDelegate_t * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef GAMEOBJECT_T1113636619_H
#define GAMEOBJECT_T1113636619_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GameObject
struct  GameObject_t1113636619  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEOBJECT_T1113636619_H
#ifndef MATERIAL_T340375123_H
#define MATERIAL_T340375123_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Material
struct  Material_t340375123  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATERIAL_T340375123_H
#ifndef UNITYWEBREQUEST_T463507806_H
#define UNITYWEBREQUEST_T463507806_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.UnityWebRequest
struct  UnityWebRequest_t463507806  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Networking.UnityWebRequest::m_Ptr
	intptr_t ___m_Ptr_0;
	// UnityEngine.Networking.DownloadHandler UnityEngine.Networking.UnityWebRequest::m_DownloadHandler
	DownloadHandler_t2937767557 * ___m_DownloadHandler_1;
	// UnityEngine.Networking.UploadHandler UnityEngine.Networking.UnityWebRequest::m_UploadHandler
	UploadHandler_t2993558019 * ___m_UploadHandler_2;
	// UnityEngine.Networking.CertificateHandler UnityEngine.Networking.UnityWebRequest::m_CertificateHandler
	CertificateHandler_t2739891000 * ___m_CertificateHandler_3;
	// System.Uri UnityEngine.Networking.UnityWebRequest::m_Uri
	Uri_t100236324 * ___m_Uri_4;
	// System.Boolean UnityEngine.Networking.UnityWebRequest::<disposeCertificateHandlerOnDispose>k__BackingField
	bool ___U3CdisposeCertificateHandlerOnDisposeU3Ek__BackingField_11;
	// System.Boolean UnityEngine.Networking.UnityWebRequest::<disposeDownloadHandlerOnDispose>k__BackingField
	bool ___U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_12;
	// System.Boolean UnityEngine.Networking.UnityWebRequest::<disposeUploadHandlerOnDispose>k__BackingField
	bool ___U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_13;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(UnityWebRequest_t463507806, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}

	inline static int32_t get_offset_of_m_DownloadHandler_1() { return static_cast<int32_t>(offsetof(UnityWebRequest_t463507806, ___m_DownloadHandler_1)); }
	inline DownloadHandler_t2937767557 * get_m_DownloadHandler_1() const { return ___m_DownloadHandler_1; }
	inline DownloadHandler_t2937767557 ** get_address_of_m_DownloadHandler_1() { return &___m_DownloadHandler_1; }
	inline void set_m_DownloadHandler_1(DownloadHandler_t2937767557 * value)
	{
		___m_DownloadHandler_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_DownloadHandler_1), value);
	}

	inline static int32_t get_offset_of_m_UploadHandler_2() { return static_cast<int32_t>(offsetof(UnityWebRequest_t463507806, ___m_UploadHandler_2)); }
	inline UploadHandler_t2993558019 * get_m_UploadHandler_2() const { return ___m_UploadHandler_2; }
	inline UploadHandler_t2993558019 ** get_address_of_m_UploadHandler_2() { return &___m_UploadHandler_2; }
	inline void set_m_UploadHandler_2(UploadHandler_t2993558019 * value)
	{
		___m_UploadHandler_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_UploadHandler_2), value);
	}

	inline static int32_t get_offset_of_m_CertificateHandler_3() { return static_cast<int32_t>(offsetof(UnityWebRequest_t463507806, ___m_CertificateHandler_3)); }
	inline CertificateHandler_t2739891000 * get_m_CertificateHandler_3() const { return ___m_CertificateHandler_3; }
	inline CertificateHandler_t2739891000 ** get_address_of_m_CertificateHandler_3() { return &___m_CertificateHandler_3; }
	inline void set_m_CertificateHandler_3(CertificateHandler_t2739891000 * value)
	{
		___m_CertificateHandler_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_CertificateHandler_3), value);
	}

	inline static int32_t get_offset_of_m_Uri_4() { return static_cast<int32_t>(offsetof(UnityWebRequest_t463507806, ___m_Uri_4)); }
	inline Uri_t100236324 * get_m_Uri_4() const { return ___m_Uri_4; }
	inline Uri_t100236324 ** get_address_of_m_Uri_4() { return &___m_Uri_4; }
	inline void set_m_Uri_4(Uri_t100236324 * value)
	{
		___m_Uri_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Uri_4), value);
	}

	inline static int32_t get_offset_of_U3CdisposeCertificateHandlerOnDisposeU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(UnityWebRequest_t463507806, ___U3CdisposeCertificateHandlerOnDisposeU3Ek__BackingField_11)); }
	inline bool get_U3CdisposeCertificateHandlerOnDisposeU3Ek__BackingField_11() const { return ___U3CdisposeCertificateHandlerOnDisposeU3Ek__BackingField_11; }
	inline bool* get_address_of_U3CdisposeCertificateHandlerOnDisposeU3Ek__BackingField_11() { return &___U3CdisposeCertificateHandlerOnDisposeU3Ek__BackingField_11; }
	inline void set_U3CdisposeCertificateHandlerOnDisposeU3Ek__BackingField_11(bool value)
	{
		___U3CdisposeCertificateHandlerOnDisposeU3Ek__BackingField_11 = value;
	}

	inline static int32_t get_offset_of_U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(UnityWebRequest_t463507806, ___U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_12)); }
	inline bool get_U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_12() const { return ___U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_12; }
	inline bool* get_address_of_U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_12() { return &___U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_12; }
	inline void set_U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_12(bool value)
	{
		___U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_12 = value;
	}

	inline static int32_t get_offset_of_U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(UnityWebRequest_t463507806, ___U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_13)); }
	inline bool get_U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_13() const { return ___U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_13; }
	inline bool* get_address_of_U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_13() { return &___U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_13; }
	inline void set_U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_13(bool value)
	{
		___U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Networking.UnityWebRequest
struct UnityWebRequest_t463507806_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
	DownloadHandler_t2937767557_marshaled_pinvoke ___m_DownloadHandler_1;
	UploadHandler_t2993558019_marshaled_pinvoke ___m_UploadHandler_2;
	CertificateHandler_t2739891000_marshaled_pinvoke ___m_CertificateHandler_3;
	Uri_t100236324 * ___m_Uri_4;
	int32_t ___U3CdisposeCertificateHandlerOnDisposeU3Ek__BackingField_11;
	int32_t ___U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_12;
	int32_t ___U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_13;
};
// Native definition for COM marshalling of UnityEngine.Networking.UnityWebRequest
struct UnityWebRequest_t463507806_marshaled_com
{
	intptr_t ___m_Ptr_0;
	DownloadHandler_t2937767557_marshaled_com* ___m_DownloadHandler_1;
	UploadHandler_t2993558019_marshaled_com* ___m_UploadHandler_2;
	CertificateHandler_t2739891000_marshaled_com* ___m_CertificateHandler_3;
	Uri_t100236324 * ___m_Uri_4;
	int32_t ___U3CdisposeCertificateHandlerOnDisposeU3Ek__BackingField_11;
	int32_t ___U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_12;
	int32_t ___U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_13;
};
#endif // UNITYWEBREQUEST_T463507806_H
#ifndef UNITYWEBREQUESTASYNCOPERATION_T3852015985_H
#define UNITYWEBREQUESTASYNCOPERATION_T3852015985_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.UnityWebRequestAsyncOperation
struct  UnityWebRequestAsyncOperation_t3852015985  : public AsyncOperation_t1445031843
{
public:
	// UnityEngine.Networking.UnityWebRequest UnityEngine.Networking.UnityWebRequestAsyncOperation::<webRequest>k__BackingField
	UnityWebRequest_t463507806 * ___U3CwebRequestU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CwebRequestU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(UnityWebRequestAsyncOperation_t3852015985, ___U3CwebRequestU3Ek__BackingField_2)); }
	inline UnityWebRequest_t463507806 * get_U3CwebRequestU3Ek__BackingField_2() const { return ___U3CwebRequestU3Ek__BackingField_2; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CwebRequestU3Ek__BackingField_2() { return &___U3CwebRequestU3Ek__BackingField_2; }
	inline void set_U3CwebRequestU3Ek__BackingField_2(UnityWebRequest_t463507806 * value)
	{
		___U3CwebRequestU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwebRequestU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Networking.UnityWebRequestAsyncOperation
struct UnityWebRequestAsyncOperation_t3852015985_marshaled_pinvoke : public AsyncOperation_t1445031843_marshaled_pinvoke
{
	UnityWebRequest_t463507806_marshaled_pinvoke* ___U3CwebRequestU3Ek__BackingField_2;
};
// Native definition for COM marshalling of UnityEngine.Networking.UnityWebRequestAsyncOperation
struct UnityWebRequestAsyncOperation_t3852015985_marshaled_com : public AsyncOperation_t1445031843_marshaled_com
{
	UnityWebRequest_t463507806_marshaled_com* ___U3CwebRequestU3Ek__BackingField_2;
};
#endif // UNITYWEBREQUESTASYNCOPERATION_T3852015985_H
#ifndef SPRITE_T280657092_H
#define SPRITE_T280657092_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Sprite
struct  Sprite_t280657092  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPRITE_T280657092_H
#ifndef TEXTURE_T3661962703_H
#define TEXTURE_T3661962703_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Texture
struct  Texture_t3661962703  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTURE_T3661962703_H
#ifndef TOUCH_T1921856868_H
#define TOUCH_T1921856868_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Touch
struct  Touch_t1921856868 
{
public:
	// System.Int32 UnityEngine.Touch::m_FingerId
	int32_t ___m_FingerId_0;
	// UnityEngine.Vector2 UnityEngine.Touch::m_Position
	Vector2_t2156229523  ___m_Position_1;
	// UnityEngine.Vector2 UnityEngine.Touch::m_RawPosition
	Vector2_t2156229523  ___m_RawPosition_2;
	// UnityEngine.Vector2 UnityEngine.Touch::m_PositionDelta
	Vector2_t2156229523  ___m_PositionDelta_3;
	// System.Single UnityEngine.Touch::m_TimeDelta
	float ___m_TimeDelta_4;
	// System.Int32 UnityEngine.Touch::m_TapCount
	int32_t ___m_TapCount_5;
	// UnityEngine.TouchPhase UnityEngine.Touch::m_Phase
	int32_t ___m_Phase_6;
	// UnityEngine.TouchType UnityEngine.Touch::m_Type
	int32_t ___m_Type_7;
	// System.Single UnityEngine.Touch::m_Pressure
	float ___m_Pressure_8;
	// System.Single UnityEngine.Touch::m_maximumPossiblePressure
	float ___m_maximumPossiblePressure_9;
	// System.Single UnityEngine.Touch::m_Radius
	float ___m_Radius_10;
	// System.Single UnityEngine.Touch::m_RadiusVariance
	float ___m_RadiusVariance_11;
	// System.Single UnityEngine.Touch::m_AltitudeAngle
	float ___m_AltitudeAngle_12;
	// System.Single UnityEngine.Touch::m_AzimuthAngle
	float ___m_AzimuthAngle_13;

public:
	inline static int32_t get_offset_of_m_FingerId_0() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_FingerId_0)); }
	inline int32_t get_m_FingerId_0() const { return ___m_FingerId_0; }
	inline int32_t* get_address_of_m_FingerId_0() { return &___m_FingerId_0; }
	inline void set_m_FingerId_0(int32_t value)
	{
		___m_FingerId_0 = value;
	}

	inline static int32_t get_offset_of_m_Position_1() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_Position_1)); }
	inline Vector2_t2156229523  get_m_Position_1() const { return ___m_Position_1; }
	inline Vector2_t2156229523 * get_address_of_m_Position_1() { return &___m_Position_1; }
	inline void set_m_Position_1(Vector2_t2156229523  value)
	{
		___m_Position_1 = value;
	}

	inline static int32_t get_offset_of_m_RawPosition_2() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_RawPosition_2)); }
	inline Vector2_t2156229523  get_m_RawPosition_2() const { return ___m_RawPosition_2; }
	inline Vector2_t2156229523 * get_address_of_m_RawPosition_2() { return &___m_RawPosition_2; }
	inline void set_m_RawPosition_2(Vector2_t2156229523  value)
	{
		___m_RawPosition_2 = value;
	}

	inline static int32_t get_offset_of_m_PositionDelta_3() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_PositionDelta_3)); }
	inline Vector2_t2156229523  get_m_PositionDelta_3() const { return ___m_PositionDelta_3; }
	inline Vector2_t2156229523 * get_address_of_m_PositionDelta_3() { return &___m_PositionDelta_3; }
	inline void set_m_PositionDelta_3(Vector2_t2156229523  value)
	{
		___m_PositionDelta_3 = value;
	}

	inline static int32_t get_offset_of_m_TimeDelta_4() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_TimeDelta_4)); }
	inline float get_m_TimeDelta_4() const { return ___m_TimeDelta_4; }
	inline float* get_address_of_m_TimeDelta_4() { return &___m_TimeDelta_4; }
	inline void set_m_TimeDelta_4(float value)
	{
		___m_TimeDelta_4 = value;
	}

	inline static int32_t get_offset_of_m_TapCount_5() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_TapCount_5)); }
	inline int32_t get_m_TapCount_5() const { return ___m_TapCount_5; }
	inline int32_t* get_address_of_m_TapCount_5() { return &___m_TapCount_5; }
	inline void set_m_TapCount_5(int32_t value)
	{
		___m_TapCount_5 = value;
	}

	inline static int32_t get_offset_of_m_Phase_6() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_Phase_6)); }
	inline int32_t get_m_Phase_6() const { return ___m_Phase_6; }
	inline int32_t* get_address_of_m_Phase_6() { return &___m_Phase_6; }
	inline void set_m_Phase_6(int32_t value)
	{
		___m_Phase_6 = value;
	}

	inline static int32_t get_offset_of_m_Type_7() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_Type_7)); }
	inline int32_t get_m_Type_7() const { return ___m_Type_7; }
	inline int32_t* get_address_of_m_Type_7() { return &___m_Type_7; }
	inline void set_m_Type_7(int32_t value)
	{
		___m_Type_7 = value;
	}

	inline static int32_t get_offset_of_m_Pressure_8() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_Pressure_8)); }
	inline float get_m_Pressure_8() const { return ___m_Pressure_8; }
	inline float* get_address_of_m_Pressure_8() { return &___m_Pressure_8; }
	inline void set_m_Pressure_8(float value)
	{
		___m_Pressure_8 = value;
	}

	inline static int32_t get_offset_of_m_maximumPossiblePressure_9() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_maximumPossiblePressure_9)); }
	inline float get_m_maximumPossiblePressure_9() const { return ___m_maximumPossiblePressure_9; }
	inline float* get_address_of_m_maximumPossiblePressure_9() { return &___m_maximumPossiblePressure_9; }
	inline void set_m_maximumPossiblePressure_9(float value)
	{
		___m_maximumPossiblePressure_9 = value;
	}

	inline static int32_t get_offset_of_m_Radius_10() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_Radius_10)); }
	inline float get_m_Radius_10() const { return ___m_Radius_10; }
	inline float* get_address_of_m_Radius_10() { return &___m_Radius_10; }
	inline void set_m_Radius_10(float value)
	{
		___m_Radius_10 = value;
	}

	inline static int32_t get_offset_of_m_RadiusVariance_11() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_RadiusVariance_11)); }
	inline float get_m_RadiusVariance_11() const { return ___m_RadiusVariance_11; }
	inline float* get_address_of_m_RadiusVariance_11() { return &___m_RadiusVariance_11; }
	inline void set_m_RadiusVariance_11(float value)
	{
		___m_RadiusVariance_11 = value;
	}

	inline static int32_t get_offset_of_m_AltitudeAngle_12() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_AltitudeAngle_12)); }
	inline float get_m_AltitudeAngle_12() const { return ___m_AltitudeAngle_12; }
	inline float* get_address_of_m_AltitudeAngle_12() { return &___m_AltitudeAngle_12; }
	inline void set_m_AltitudeAngle_12(float value)
	{
		___m_AltitudeAngle_12 = value;
	}

	inline static int32_t get_offset_of_m_AzimuthAngle_13() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_AzimuthAngle_13)); }
	inline float get_m_AzimuthAngle_13() const { return ___m_AzimuthAngle_13; }
	inline float* get_address_of_m_AzimuthAngle_13() { return &___m_AzimuthAngle_13; }
	inline void set_m_AzimuthAngle_13(float value)
	{
		___m_AzimuthAngle_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCH_T1921856868_H
#ifndef NAVIGATION_T3049316579_H
#define NAVIGATION_T3049316579_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Navigation
struct  Navigation_t3049316579 
{
public:
	// UnityEngine.UI.Navigation/Mode UnityEngine.UI.Navigation::m_Mode
	int32_t ___m_Mode_0;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnUp
	Selectable_t3250028441 * ___m_SelectOnUp_1;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnDown
	Selectable_t3250028441 * ___m_SelectOnDown_2;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnLeft
	Selectable_t3250028441 * ___m_SelectOnLeft_3;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnRight
	Selectable_t3250028441 * ___m_SelectOnRight_4;

public:
	inline static int32_t get_offset_of_m_Mode_0() { return static_cast<int32_t>(offsetof(Navigation_t3049316579, ___m_Mode_0)); }
	inline int32_t get_m_Mode_0() const { return ___m_Mode_0; }
	inline int32_t* get_address_of_m_Mode_0() { return &___m_Mode_0; }
	inline void set_m_Mode_0(int32_t value)
	{
		___m_Mode_0 = value;
	}

	inline static int32_t get_offset_of_m_SelectOnUp_1() { return static_cast<int32_t>(offsetof(Navigation_t3049316579, ___m_SelectOnUp_1)); }
	inline Selectable_t3250028441 * get_m_SelectOnUp_1() const { return ___m_SelectOnUp_1; }
	inline Selectable_t3250028441 ** get_address_of_m_SelectOnUp_1() { return &___m_SelectOnUp_1; }
	inline void set_m_SelectOnUp_1(Selectable_t3250028441 * value)
	{
		___m_SelectOnUp_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnUp_1), value);
	}

	inline static int32_t get_offset_of_m_SelectOnDown_2() { return static_cast<int32_t>(offsetof(Navigation_t3049316579, ___m_SelectOnDown_2)); }
	inline Selectable_t3250028441 * get_m_SelectOnDown_2() const { return ___m_SelectOnDown_2; }
	inline Selectable_t3250028441 ** get_address_of_m_SelectOnDown_2() { return &___m_SelectOnDown_2; }
	inline void set_m_SelectOnDown_2(Selectable_t3250028441 * value)
	{
		___m_SelectOnDown_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnDown_2), value);
	}

	inline static int32_t get_offset_of_m_SelectOnLeft_3() { return static_cast<int32_t>(offsetof(Navigation_t3049316579, ___m_SelectOnLeft_3)); }
	inline Selectable_t3250028441 * get_m_SelectOnLeft_3() const { return ___m_SelectOnLeft_3; }
	inline Selectable_t3250028441 ** get_address_of_m_SelectOnLeft_3() { return &___m_SelectOnLeft_3; }
	inline void set_m_SelectOnLeft_3(Selectable_t3250028441 * value)
	{
		___m_SelectOnLeft_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnLeft_3), value);
	}

	inline static int32_t get_offset_of_m_SelectOnRight_4() { return static_cast<int32_t>(offsetof(Navigation_t3049316579, ___m_SelectOnRight_4)); }
	inline Selectable_t3250028441 * get_m_SelectOnRight_4() const { return ___m_SelectOnRight_4; }
	inline Selectable_t3250028441 ** get_address_of_m_SelectOnRight_4() { return &___m_SelectOnRight_4; }
	inline void set_m_SelectOnRight_4(Selectable_t3250028441 * value)
	{
		___m_SelectOnRight_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnRight_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.UI.Navigation
struct Navigation_t3049316579_marshaled_pinvoke
{
	int32_t ___m_Mode_0;
	Selectable_t3250028441 * ___m_SelectOnUp_1;
	Selectable_t3250028441 * ___m_SelectOnDown_2;
	Selectable_t3250028441 * ___m_SelectOnLeft_3;
	Selectable_t3250028441 * ___m_SelectOnRight_4;
};
// Native definition for COM marshalling of UnityEngine.UI.Navigation
struct Navigation_t3049316579_marshaled_com
{
	int32_t ___m_Mode_0;
	Selectable_t3250028441 * ___m_SelectOnUp_1;
	Selectable_t3250028441 * ___m_SelectOnDown_2;
	Selectable_t3250028441 * ___m_SelectOnLeft_3;
	Selectable_t3250028441 * ___m_SelectOnRight_4;
};
#endif // NAVIGATION_T3049316579_H
#ifndef WWW_T3688466362_H
#define WWW_T3688466362_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.WWW
struct  WWW_t3688466362  : public CustomYieldInstruction_t1895667560
{
public:
	// UnityEngine.ThreadPriority UnityEngine.WWW::<threadPriority>k__BackingField
	int32_t ___U3CthreadPriorityU3Ek__BackingField_0;
	// UnityEngine.Networking.UnityWebRequest UnityEngine.WWW::_uwr
	UnityWebRequest_t463507806 * ____uwr_1;
	// UnityEngine.AssetBundle UnityEngine.WWW::_assetBundle
	AssetBundle_t1153907252 * ____assetBundle_2;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> UnityEngine.WWW::_responseHeaders
	Dictionary_2_t1632706988 * ____responseHeaders_3;

public:
	inline static int32_t get_offset_of_U3CthreadPriorityU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(WWW_t3688466362, ___U3CthreadPriorityU3Ek__BackingField_0)); }
	inline int32_t get_U3CthreadPriorityU3Ek__BackingField_0() const { return ___U3CthreadPriorityU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CthreadPriorityU3Ek__BackingField_0() { return &___U3CthreadPriorityU3Ek__BackingField_0; }
	inline void set_U3CthreadPriorityU3Ek__BackingField_0(int32_t value)
	{
		___U3CthreadPriorityU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of__uwr_1() { return static_cast<int32_t>(offsetof(WWW_t3688466362, ____uwr_1)); }
	inline UnityWebRequest_t463507806 * get__uwr_1() const { return ____uwr_1; }
	inline UnityWebRequest_t463507806 ** get_address_of__uwr_1() { return &____uwr_1; }
	inline void set__uwr_1(UnityWebRequest_t463507806 * value)
	{
		____uwr_1 = value;
		Il2CppCodeGenWriteBarrier((&____uwr_1), value);
	}

	inline static int32_t get_offset_of__assetBundle_2() { return static_cast<int32_t>(offsetof(WWW_t3688466362, ____assetBundle_2)); }
	inline AssetBundle_t1153907252 * get__assetBundle_2() const { return ____assetBundle_2; }
	inline AssetBundle_t1153907252 ** get_address_of__assetBundle_2() { return &____assetBundle_2; }
	inline void set__assetBundle_2(AssetBundle_t1153907252 * value)
	{
		____assetBundle_2 = value;
		Il2CppCodeGenWriteBarrier((&____assetBundle_2), value);
	}

	inline static int32_t get_offset_of__responseHeaders_3() { return static_cast<int32_t>(offsetof(WWW_t3688466362, ____responseHeaders_3)); }
	inline Dictionary_2_t1632706988 * get__responseHeaders_3() const { return ____responseHeaders_3; }
	inline Dictionary_2_t1632706988 ** get_address_of__responseHeaders_3() { return &____responseHeaders_3; }
	inline void set__responseHeaders_3(Dictionary_2_t1632706988 * value)
	{
		____responseHeaders_3 = value;
		Il2CppCodeGenWriteBarrier((&____responseHeaders_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WWW_T3688466362_H
#ifndef ASYNCCALLBACK_T3962456242_H
#define ASYNCCALLBACK_T3962456242_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.AsyncCallback
struct  AsyncCallback_t3962456242  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYNCCALLBACK_T3962456242_H
#ifndef TEXTURELOADHANDLE_T2346000395_H
#define TEXTURELOADHANDLE_T2346000395_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.TextureLoadHandle
struct  TextureLoadHandle_t2346000395  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTURELOADHANDLE_T2346000395_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef RENDERER_T2627027031_H
#define RENDERER_T2627027031_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Renderer
struct  Renderer_t2627027031  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERER_T2627027031_H
#ifndef TEXTURE2D_T3840446185_H
#define TEXTURE2D_T3840446185_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Texture2D
struct  Texture2D_t3840446185  : public Texture_t3661962703
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTURE2D_T3840446185_H
#ifndef TRANSFORM_T3600365921_H
#define TRANSFORM_T3600365921_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Transform
struct  Transform_t3600365921  : public Component_t1923634451
{
public:
	// System.Int32 UnityEngine.Transform::<hierarchyCount>k__BackingField
	int32_t ___U3ChierarchyCountU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3ChierarchyCountU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Transform_t3600365921, ___U3ChierarchyCountU3Ek__BackingField_4)); }
	inline int32_t get_U3ChierarchyCountU3Ek__BackingField_4() const { return ___U3ChierarchyCountU3Ek__BackingField_4; }
	inline int32_t* get_address_of_U3ChierarchyCountU3Ek__BackingField_4() { return &___U3ChierarchyCountU3Ek__BackingField_4; }
	inline void set_U3ChierarchyCountU3Ek__BackingField_4(int32_t value)
	{
		___U3ChierarchyCountU3Ek__BackingField_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORM_T3600365921_H
#ifndef ANIMATOR_T434523843_H
#define ANIMATOR_T434523843_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Animator
struct  Animator_t434523843  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATOR_T434523843_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef RECTTRANSFORM_T3704657025_H
#define RECTTRANSFORM_T3704657025_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RectTransform
struct  RectTransform_t3704657025  : public Transform_t3600365921
{
public:
	// UnityEngine.Object UnityEngine.RectTransform::<drivenByObject>k__BackingField
	Object_t631007953 * ___U3CdrivenByObjectU3Ek__BackingField_6;
	// UnityEngine.DrivenTransformProperties UnityEngine.RectTransform::<drivenProperties>k__BackingField
	int32_t ___U3CdrivenPropertiesU3Ek__BackingField_7;

public:
	inline static int32_t get_offset_of_U3CdrivenByObjectU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(RectTransform_t3704657025, ___U3CdrivenByObjectU3Ek__BackingField_6)); }
	inline Object_t631007953 * get_U3CdrivenByObjectU3Ek__BackingField_6() const { return ___U3CdrivenByObjectU3Ek__BackingField_6; }
	inline Object_t631007953 ** get_address_of_U3CdrivenByObjectU3Ek__BackingField_6() { return &___U3CdrivenByObjectU3Ek__BackingField_6; }
	inline void set_U3CdrivenByObjectU3Ek__BackingField_6(Object_t631007953 * value)
	{
		___U3CdrivenByObjectU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CdrivenByObjectU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3CdrivenPropertiesU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(RectTransform_t3704657025, ___U3CdrivenPropertiesU3Ek__BackingField_7)); }
	inline int32_t get_U3CdrivenPropertiesU3Ek__BackingField_7() const { return ___U3CdrivenPropertiesU3Ek__BackingField_7; }
	inline int32_t* get_address_of_U3CdrivenPropertiesU3Ek__BackingField_7() { return &___U3CdrivenPropertiesU3Ek__BackingField_7; }
	inline void set_U3CdrivenPropertiesU3Ek__BackingField_7(int32_t value)
	{
		___U3CdrivenPropertiesU3Ek__BackingField_7 = value;
	}
};

struct RectTransform_t3704657025_StaticFields
{
public:
	// UnityEngine.RectTransform/ReapplyDrivenProperties UnityEngine.RectTransform::reapplyDrivenProperties
	ReapplyDrivenProperties_t1258266594 * ___reapplyDrivenProperties_5;

public:
	inline static int32_t get_offset_of_reapplyDrivenProperties_5() { return static_cast<int32_t>(offsetof(RectTransform_t3704657025_StaticFields, ___reapplyDrivenProperties_5)); }
	inline ReapplyDrivenProperties_t1258266594 * get_reapplyDrivenProperties_5() const { return ___reapplyDrivenProperties_5; }
	inline ReapplyDrivenProperties_t1258266594 ** get_address_of_reapplyDrivenProperties_5() { return &___reapplyDrivenProperties_5; }
	inline void set_reapplyDrivenProperties_5(ReapplyDrivenProperties_t1258266594 * value)
	{
		___reapplyDrivenProperties_5 = value;
		Il2CppCodeGenWriteBarrier((&___reapplyDrivenProperties_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECTTRANSFORM_T3704657025_H
#ifndef SPRITERENDERER_T3235626157_H
#define SPRITERENDERER_T3235626157_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SpriteRenderer
struct  SpriteRenderer_t3235626157  : public Renderer_t2627027031
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPRITERENDERER_T3235626157_H
#ifndef VIDEOPLAYER_T1683042537_H
#define VIDEOPLAYER_T1683042537_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Video.VideoPlayer
struct  VideoPlayer_t1683042537  : public Behaviour_t1437897464
{
public:
	// UnityEngine.Video.VideoPlayer/EventHandler UnityEngine.Video.VideoPlayer::prepareCompleted
	EventHandler_t3436254912 * ___prepareCompleted_4;
	// UnityEngine.Video.VideoPlayer/EventHandler UnityEngine.Video.VideoPlayer::loopPointReached
	EventHandler_t3436254912 * ___loopPointReached_5;
	// UnityEngine.Video.VideoPlayer/EventHandler UnityEngine.Video.VideoPlayer::started
	EventHandler_t3436254912 * ___started_6;
	// UnityEngine.Video.VideoPlayer/EventHandler UnityEngine.Video.VideoPlayer::frameDropped
	EventHandler_t3436254912 * ___frameDropped_7;
	// UnityEngine.Video.VideoPlayer/ErrorEventHandler UnityEngine.Video.VideoPlayer::errorReceived
	ErrorEventHandler_t3211687919 * ___errorReceived_8;
	// UnityEngine.Video.VideoPlayer/EventHandler UnityEngine.Video.VideoPlayer::seekCompleted
	EventHandler_t3436254912 * ___seekCompleted_9;
	// UnityEngine.Video.VideoPlayer/TimeEventHandler UnityEngine.Video.VideoPlayer::clockResyncOccurred
	TimeEventHandler_t445758600 * ___clockResyncOccurred_10;
	// UnityEngine.Video.VideoPlayer/FrameReadyEventHandler UnityEngine.Video.VideoPlayer::frameReady
	FrameReadyEventHandler_t3848515759 * ___frameReady_11;

public:
	inline static int32_t get_offset_of_prepareCompleted_4() { return static_cast<int32_t>(offsetof(VideoPlayer_t1683042537, ___prepareCompleted_4)); }
	inline EventHandler_t3436254912 * get_prepareCompleted_4() const { return ___prepareCompleted_4; }
	inline EventHandler_t3436254912 ** get_address_of_prepareCompleted_4() { return &___prepareCompleted_4; }
	inline void set_prepareCompleted_4(EventHandler_t3436254912 * value)
	{
		___prepareCompleted_4 = value;
		Il2CppCodeGenWriteBarrier((&___prepareCompleted_4), value);
	}

	inline static int32_t get_offset_of_loopPointReached_5() { return static_cast<int32_t>(offsetof(VideoPlayer_t1683042537, ___loopPointReached_5)); }
	inline EventHandler_t3436254912 * get_loopPointReached_5() const { return ___loopPointReached_5; }
	inline EventHandler_t3436254912 ** get_address_of_loopPointReached_5() { return &___loopPointReached_5; }
	inline void set_loopPointReached_5(EventHandler_t3436254912 * value)
	{
		___loopPointReached_5 = value;
		Il2CppCodeGenWriteBarrier((&___loopPointReached_5), value);
	}

	inline static int32_t get_offset_of_started_6() { return static_cast<int32_t>(offsetof(VideoPlayer_t1683042537, ___started_6)); }
	inline EventHandler_t3436254912 * get_started_6() const { return ___started_6; }
	inline EventHandler_t3436254912 ** get_address_of_started_6() { return &___started_6; }
	inline void set_started_6(EventHandler_t3436254912 * value)
	{
		___started_6 = value;
		Il2CppCodeGenWriteBarrier((&___started_6), value);
	}

	inline static int32_t get_offset_of_frameDropped_7() { return static_cast<int32_t>(offsetof(VideoPlayer_t1683042537, ___frameDropped_7)); }
	inline EventHandler_t3436254912 * get_frameDropped_7() const { return ___frameDropped_7; }
	inline EventHandler_t3436254912 ** get_address_of_frameDropped_7() { return &___frameDropped_7; }
	inline void set_frameDropped_7(EventHandler_t3436254912 * value)
	{
		___frameDropped_7 = value;
		Il2CppCodeGenWriteBarrier((&___frameDropped_7), value);
	}

	inline static int32_t get_offset_of_errorReceived_8() { return static_cast<int32_t>(offsetof(VideoPlayer_t1683042537, ___errorReceived_8)); }
	inline ErrorEventHandler_t3211687919 * get_errorReceived_8() const { return ___errorReceived_8; }
	inline ErrorEventHandler_t3211687919 ** get_address_of_errorReceived_8() { return &___errorReceived_8; }
	inline void set_errorReceived_8(ErrorEventHandler_t3211687919 * value)
	{
		___errorReceived_8 = value;
		Il2CppCodeGenWriteBarrier((&___errorReceived_8), value);
	}

	inline static int32_t get_offset_of_seekCompleted_9() { return static_cast<int32_t>(offsetof(VideoPlayer_t1683042537, ___seekCompleted_9)); }
	inline EventHandler_t3436254912 * get_seekCompleted_9() const { return ___seekCompleted_9; }
	inline EventHandler_t3436254912 ** get_address_of_seekCompleted_9() { return &___seekCompleted_9; }
	inline void set_seekCompleted_9(EventHandler_t3436254912 * value)
	{
		___seekCompleted_9 = value;
		Il2CppCodeGenWriteBarrier((&___seekCompleted_9), value);
	}

	inline static int32_t get_offset_of_clockResyncOccurred_10() { return static_cast<int32_t>(offsetof(VideoPlayer_t1683042537, ___clockResyncOccurred_10)); }
	inline TimeEventHandler_t445758600 * get_clockResyncOccurred_10() const { return ___clockResyncOccurred_10; }
	inline TimeEventHandler_t445758600 ** get_address_of_clockResyncOccurred_10() { return &___clockResyncOccurred_10; }
	inline void set_clockResyncOccurred_10(TimeEventHandler_t445758600 * value)
	{
		___clockResyncOccurred_10 = value;
		Il2CppCodeGenWriteBarrier((&___clockResyncOccurred_10), value);
	}

	inline static int32_t get_offset_of_frameReady_11() { return static_cast<int32_t>(offsetof(VideoPlayer_t1683042537, ___frameReady_11)); }
	inline FrameReadyEventHandler_t3848515759 * get_frameReady_11() const { return ___frameReady_11; }
	inline FrameReadyEventHandler_t3848515759 ** get_address_of_frameReady_11() { return &___frameReady_11; }
	inline void set_frameReady_11(FrameReadyEventHandler_t3848515759 * value)
	{
		___frameReady_11 = value;
		Il2CppCodeGenWriteBarrier((&___frameReady_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIDEOPLAYER_T1683042537_H
#ifndef CONTENTMANAGER2_T2980555998_H
#define CONTENTMANAGER2_T2980555998_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ContentManager2
struct  ContentManager2_t2980555998  : public MonoBehaviour_t3962482529
{
public:
	// captureAndSendScreen ContentManager2::captureSend
	captureAndSendScreen_t3196082656 * ___captureSend_5;
	// UnityEngine.GameObject ContentManager2::AugmentationObject
	GameObject_t1113636619 * ___AugmentationObject_6;
	// AnimationsManager ContentManager2::AnimationsManager
	AnimationsManager_t2281084567 * ___AnimationsManager_7;
	// UnityEngine.GameObject ContentManager2::calendarWidget
	GameObject_t1113636619 * ___calendarWidget_8;
	// System.Boolean ContentManager2::isMarkerShown
	bool ___isMarkerShown_9;
	// System.String ContentManager2::UserGUID
	String_t* ___UserGUID_10;
	// System.String ContentManager2::appName
	String_t* ___appName_11;
	// UnityEngine.GameObject ContentManager2::markerWidget
	GameObject_t1113636619 * ___markerWidget_12;
	// UnityEngine.GameObject ContentManager2::actionWidget
	GameObject_t1113636619 * ___actionWidget_13;
	// UnityEngine.GameObject ContentManager2::jeuWidget
	GameObject_t1113636619 * ___jeuWidget_14;
	// UnityEngine.GameObject ContentManager2::imageWidget
	GameObject_t1113636619 * ___imageWidget_15;
	// UnityEngine.GameObject ContentManager2::videoWidget
	GameObject_t1113636619 * ___videoWidget_16;
	// UnityEngine.GameObject ContentManager2::audioWidget
	GameObject_t1113636619 * ___audioWidget_17;
	// UnityEngine.GameObject ContentManager2::chromaKeyWidget
	GameObject_t1113636619 * ___chromaKeyWidget_18;
	// UnityEngine.GameObject ContentManager2::videoPlayerPortrait
	GameObject_t1113636619 * ___videoPlayerPortrait_19;
	// UnityEngine.UI.RawImage ContentManager2::videoPlayerPortrait_RAW
	RawImage_t3182918964 * ___videoPlayerPortrait_RAW_20;
	// UnityEngine.GameObject ContentManager2::videoPlayerLandscape
	GameObject_t1113636619 * ___videoPlayerLandscape_21;
	// UnityEngine.UI.RawImage ContentManager2::videoPlayerLandscape_RAW
	RawImage_t3182918964 * ___videoPlayerLandscape_RAW_22;
	// UnityEngine.GameObject ContentManager2::fullscreenVideoPlayer
	GameObject_t1113636619 * ___fullscreenVideoPlayer_23;
	// UnityEngine.GameObject ContentManager2::objectWidget
	GameObject_t1113636619 * ___objectWidget_24;
	// UnityEngine.UI.RawImage ContentManager2::rawImgPortrait
	RawImage_t3182918964 * ___rawImgPortrait_25;
	// UnityEngine.UI.RawImage ContentManager2::rawImgLandscape
	RawImage_t3182918964 * ___rawImgLandscape_26;
	// UnityEngine.GameObject ContentManager2::panelBack
	GameObject_t1113636619 * ___panelBack_27;
	// UnityEngine.GameObject ContentManager2::CameraOBJContainer
	GameObject_t1113636619 * ___CameraOBJContainer_28;
	// UnityEngine.GameObject ContentManager2::FS_OBJ_container
	GameObject_t1113636619 * ___FS_OBJ_container_29;
	// UnityEngine.GameObject ContentManager2::closeButton
	GameObject_t1113636619 * ___closeButton_30;
	// UnityEngine.GameObject ContentManager2::backButtonBackMarker
	GameObject_t1113636619 * ___backButtonBackMarker_31;
	// UnityEngine.GameObject ContentManager2::backMarkerON
	GameObject_t1113636619 * ___backMarkerON_32;
	// UnityEngine.GameObject ContentManager2::backMarkerOFF
	GameObject_t1113636619 * ___backMarkerOFF_33;
	// System.Boolean ContentManager2::isShowingBackMarker
	bool ___isShowingBackMarker_34;
	// UnityEngine.GameObject ContentManager2::sphereCloseUp
	GameObject_t1113636619 * ___sphereCloseUp_35;
	// UnityEngine.GameObject ContentManager2::cubeEtalon
	GameObject_t1113636619 * ___cubeEtalon_36;
	// UnityEngine.GameObject ContentManager2::closeMarkerButton
	GameObject_t1113636619 * ___closeMarkerButton_37;
	// UnityEngine.UI.Slider ContentManager2::sliderZoom
	Slider_t3903728902 * ___sliderZoom_38;
	// UnityEngine.GameObject ContentManager2::cancelButton
	GameObject_t1113636619 * ___cancelButton_39;
	// UnityEngine.GameObject ContentManager2::fullscreen3DObject
	GameObject_t1113636619 * ___fullscreen3DObject_40;
	// UnityEngine.GameObject ContentManager2::emailPanel
	GameObject_t1113636619 * ___emailPanel_41;
	// TMPro.TextMeshProUGUI ContentManager2::emailPanel_mail
	TextMeshProUGUI_t529313277 * ___emailPanel_mail_42;
	// TMPro.TextMeshProUGUI ContentManager2::emailPanel_text
	TextMeshProUGUI_t529313277 * ___emailPanel_text_43;
	// UnityEngine.UI.Button ContentManager2::emailPanel_button
	Button_t4055032469 * ___emailPanel_button_44;
	// System.String ContentManager2::screenShotURL
	String_t* ___screenShotURL_45;
	// UnityEngine.GameObject ContentManager2::nodeTex
	GameObject_t1113636619 * ___nodeTex_46;
	// Vuforia.Image/PIXEL_FORMAT ContentManager2::mPixelFormat
	int32_t ___mPixelFormat_47;
	// System.Boolean ContentManager2::mFormatRegistered
	bool ___mFormatRegistered_48;
	// Vuforia.CloudRecoBehaviour ContentManager2::mCloudRecoBehaviour
	CloudRecoBehaviour_t431762792 * ___mCloudRecoBehaviour_49;
	// Vuforia.TrackableBehaviour ContentManager2::mTrackableBehaviour
	TrackableBehaviour_t1113559212 * ___mTrackableBehaviour_50;
	// System.Boolean ContentManager2::mIsShowingMarkerData
	bool ___mIsShowingMarkerData_51;
	// System.String ContentManager2::currentMarker
	String_t* ___currentMarker_52;
	// System.Boolean ContentManager2::isTracking3D
	bool ___isTracking3D_53;
	// UnityEngine.GameObject ContentManager2::pano_regles
	GameObject_t1113636619 * ___pano_regles_54;
	// UnityEngine.GameObject ContentManager2::pano_data
	GameObject_t1113636619 * ___pano_data_55;
	// UnityEngine.GameObject ContentManager2::pano_error
	GameObject_t1113636619 * ___pano_error_56;
	// UnityEngine.GameObject ContentManager2::pano_gagne
	GameObject_t1113636619 * ___pano_gagne_57;

public:
	inline static int32_t get_offset_of_captureSend_5() { return static_cast<int32_t>(offsetof(ContentManager2_t2980555998, ___captureSend_5)); }
	inline captureAndSendScreen_t3196082656 * get_captureSend_5() const { return ___captureSend_5; }
	inline captureAndSendScreen_t3196082656 ** get_address_of_captureSend_5() { return &___captureSend_5; }
	inline void set_captureSend_5(captureAndSendScreen_t3196082656 * value)
	{
		___captureSend_5 = value;
		Il2CppCodeGenWriteBarrier((&___captureSend_5), value);
	}

	inline static int32_t get_offset_of_AugmentationObject_6() { return static_cast<int32_t>(offsetof(ContentManager2_t2980555998, ___AugmentationObject_6)); }
	inline GameObject_t1113636619 * get_AugmentationObject_6() const { return ___AugmentationObject_6; }
	inline GameObject_t1113636619 ** get_address_of_AugmentationObject_6() { return &___AugmentationObject_6; }
	inline void set_AugmentationObject_6(GameObject_t1113636619 * value)
	{
		___AugmentationObject_6 = value;
		Il2CppCodeGenWriteBarrier((&___AugmentationObject_6), value);
	}

	inline static int32_t get_offset_of_AnimationsManager_7() { return static_cast<int32_t>(offsetof(ContentManager2_t2980555998, ___AnimationsManager_7)); }
	inline AnimationsManager_t2281084567 * get_AnimationsManager_7() const { return ___AnimationsManager_7; }
	inline AnimationsManager_t2281084567 ** get_address_of_AnimationsManager_7() { return &___AnimationsManager_7; }
	inline void set_AnimationsManager_7(AnimationsManager_t2281084567 * value)
	{
		___AnimationsManager_7 = value;
		Il2CppCodeGenWriteBarrier((&___AnimationsManager_7), value);
	}

	inline static int32_t get_offset_of_calendarWidget_8() { return static_cast<int32_t>(offsetof(ContentManager2_t2980555998, ___calendarWidget_8)); }
	inline GameObject_t1113636619 * get_calendarWidget_8() const { return ___calendarWidget_8; }
	inline GameObject_t1113636619 ** get_address_of_calendarWidget_8() { return &___calendarWidget_8; }
	inline void set_calendarWidget_8(GameObject_t1113636619 * value)
	{
		___calendarWidget_8 = value;
		Il2CppCodeGenWriteBarrier((&___calendarWidget_8), value);
	}

	inline static int32_t get_offset_of_isMarkerShown_9() { return static_cast<int32_t>(offsetof(ContentManager2_t2980555998, ___isMarkerShown_9)); }
	inline bool get_isMarkerShown_9() const { return ___isMarkerShown_9; }
	inline bool* get_address_of_isMarkerShown_9() { return &___isMarkerShown_9; }
	inline void set_isMarkerShown_9(bool value)
	{
		___isMarkerShown_9 = value;
	}

	inline static int32_t get_offset_of_UserGUID_10() { return static_cast<int32_t>(offsetof(ContentManager2_t2980555998, ___UserGUID_10)); }
	inline String_t* get_UserGUID_10() const { return ___UserGUID_10; }
	inline String_t** get_address_of_UserGUID_10() { return &___UserGUID_10; }
	inline void set_UserGUID_10(String_t* value)
	{
		___UserGUID_10 = value;
		Il2CppCodeGenWriteBarrier((&___UserGUID_10), value);
	}

	inline static int32_t get_offset_of_appName_11() { return static_cast<int32_t>(offsetof(ContentManager2_t2980555998, ___appName_11)); }
	inline String_t* get_appName_11() const { return ___appName_11; }
	inline String_t** get_address_of_appName_11() { return &___appName_11; }
	inline void set_appName_11(String_t* value)
	{
		___appName_11 = value;
		Il2CppCodeGenWriteBarrier((&___appName_11), value);
	}

	inline static int32_t get_offset_of_markerWidget_12() { return static_cast<int32_t>(offsetof(ContentManager2_t2980555998, ___markerWidget_12)); }
	inline GameObject_t1113636619 * get_markerWidget_12() const { return ___markerWidget_12; }
	inline GameObject_t1113636619 ** get_address_of_markerWidget_12() { return &___markerWidget_12; }
	inline void set_markerWidget_12(GameObject_t1113636619 * value)
	{
		___markerWidget_12 = value;
		Il2CppCodeGenWriteBarrier((&___markerWidget_12), value);
	}

	inline static int32_t get_offset_of_actionWidget_13() { return static_cast<int32_t>(offsetof(ContentManager2_t2980555998, ___actionWidget_13)); }
	inline GameObject_t1113636619 * get_actionWidget_13() const { return ___actionWidget_13; }
	inline GameObject_t1113636619 ** get_address_of_actionWidget_13() { return &___actionWidget_13; }
	inline void set_actionWidget_13(GameObject_t1113636619 * value)
	{
		___actionWidget_13 = value;
		Il2CppCodeGenWriteBarrier((&___actionWidget_13), value);
	}

	inline static int32_t get_offset_of_jeuWidget_14() { return static_cast<int32_t>(offsetof(ContentManager2_t2980555998, ___jeuWidget_14)); }
	inline GameObject_t1113636619 * get_jeuWidget_14() const { return ___jeuWidget_14; }
	inline GameObject_t1113636619 ** get_address_of_jeuWidget_14() { return &___jeuWidget_14; }
	inline void set_jeuWidget_14(GameObject_t1113636619 * value)
	{
		___jeuWidget_14 = value;
		Il2CppCodeGenWriteBarrier((&___jeuWidget_14), value);
	}

	inline static int32_t get_offset_of_imageWidget_15() { return static_cast<int32_t>(offsetof(ContentManager2_t2980555998, ___imageWidget_15)); }
	inline GameObject_t1113636619 * get_imageWidget_15() const { return ___imageWidget_15; }
	inline GameObject_t1113636619 ** get_address_of_imageWidget_15() { return &___imageWidget_15; }
	inline void set_imageWidget_15(GameObject_t1113636619 * value)
	{
		___imageWidget_15 = value;
		Il2CppCodeGenWriteBarrier((&___imageWidget_15), value);
	}

	inline static int32_t get_offset_of_videoWidget_16() { return static_cast<int32_t>(offsetof(ContentManager2_t2980555998, ___videoWidget_16)); }
	inline GameObject_t1113636619 * get_videoWidget_16() const { return ___videoWidget_16; }
	inline GameObject_t1113636619 ** get_address_of_videoWidget_16() { return &___videoWidget_16; }
	inline void set_videoWidget_16(GameObject_t1113636619 * value)
	{
		___videoWidget_16 = value;
		Il2CppCodeGenWriteBarrier((&___videoWidget_16), value);
	}

	inline static int32_t get_offset_of_audioWidget_17() { return static_cast<int32_t>(offsetof(ContentManager2_t2980555998, ___audioWidget_17)); }
	inline GameObject_t1113636619 * get_audioWidget_17() const { return ___audioWidget_17; }
	inline GameObject_t1113636619 ** get_address_of_audioWidget_17() { return &___audioWidget_17; }
	inline void set_audioWidget_17(GameObject_t1113636619 * value)
	{
		___audioWidget_17 = value;
		Il2CppCodeGenWriteBarrier((&___audioWidget_17), value);
	}

	inline static int32_t get_offset_of_chromaKeyWidget_18() { return static_cast<int32_t>(offsetof(ContentManager2_t2980555998, ___chromaKeyWidget_18)); }
	inline GameObject_t1113636619 * get_chromaKeyWidget_18() const { return ___chromaKeyWidget_18; }
	inline GameObject_t1113636619 ** get_address_of_chromaKeyWidget_18() { return &___chromaKeyWidget_18; }
	inline void set_chromaKeyWidget_18(GameObject_t1113636619 * value)
	{
		___chromaKeyWidget_18 = value;
		Il2CppCodeGenWriteBarrier((&___chromaKeyWidget_18), value);
	}

	inline static int32_t get_offset_of_videoPlayerPortrait_19() { return static_cast<int32_t>(offsetof(ContentManager2_t2980555998, ___videoPlayerPortrait_19)); }
	inline GameObject_t1113636619 * get_videoPlayerPortrait_19() const { return ___videoPlayerPortrait_19; }
	inline GameObject_t1113636619 ** get_address_of_videoPlayerPortrait_19() { return &___videoPlayerPortrait_19; }
	inline void set_videoPlayerPortrait_19(GameObject_t1113636619 * value)
	{
		___videoPlayerPortrait_19 = value;
		Il2CppCodeGenWriteBarrier((&___videoPlayerPortrait_19), value);
	}

	inline static int32_t get_offset_of_videoPlayerPortrait_RAW_20() { return static_cast<int32_t>(offsetof(ContentManager2_t2980555998, ___videoPlayerPortrait_RAW_20)); }
	inline RawImage_t3182918964 * get_videoPlayerPortrait_RAW_20() const { return ___videoPlayerPortrait_RAW_20; }
	inline RawImage_t3182918964 ** get_address_of_videoPlayerPortrait_RAW_20() { return &___videoPlayerPortrait_RAW_20; }
	inline void set_videoPlayerPortrait_RAW_20(RawImage_t3182918964 * value)
	{
		___videoPlayerPortrait_RAW_20 = value;
		Il2CppCodeGenWriteBarrier((&___videoPlayerPortrait_RAW_20), value);
	}

	inline static int32_t get_offset_of_videoPlayerLandscape_21() { return static_cast<int32_t>(offsetof(ContentManager2_t2980555998, ___videoPlayerLandscape_21)); }
	inline GameObject_t1113636619 * get_videoPlayerLandscape_21() const { return ___videoPlayerLandscape_21; }
	inline GameObject_t1113636619 ** get_address_of_videoPlayerLandscape_21() { return &___videoPlayerLandscape_21; }
	inline void set_videoPlayerLandscape_21(GameObject_t1113636619 * value)
	{
		___videoPlayerLandscape_21 = value;
		Il2CppCodeGenWriteBarrier((&___videoPlayerLandscape_21), value);
	}

	inline static int32_t get_offset_of_videoPlayerLandscape_RAW_22() { return static_cast<int32_t>(offsetof(ContentManager2_t2980555998, ___videoPlayerLandscape_RAW_22)); }
	inline RawImage_t3182918964 * get_videoPlayerLandscape_RAW_22() const { return ___videoPlayerLandscape_RAW_22; }
	inline RawImage_t3182918964 ** get_address_of_videoPlayerLandscape_RAW_22() { return &___videoPlayerLandscape_RAW_22; }
	inline void set_videoPlayerLandscape_RAW_22(RawImage_t3182918964 * value)
	{
		___videoPlayerLandscape_RAW_22 = value;
		Il2CppCodeGenWriteBarrier((&___videoPlayerLandscape_RAW_22), value);
	}

	inline static int32_t get_offset_of_fullscreenVideoPlayer_23() { return static_cast<int32_t>(offsetof(ContentManager2_t2980555998, ___fullscreenVideoPlayer_23)); }
	inline GameObject_t1113636619 * get_fullscreenVideoPlayer_23() const { return ___fullscreenVideoPlayer_23; }
	inline GameObject_t1113636619 ** get_address_of_fullscreenVideoPlayer_23() { return &___fullscreenVideoPlayer_23; }
	inline void set_fullscreenVideoPlayer_23(GameObject_t1113636619 * value)
	{
		___fullscreenVideoPlayer_23 = value;
		Il2CppCodeGenWriteBarrier((&___fullscreenVideoPlayer_23), value);
	}

	inline static int32_t get_offset_of_objectWidget_24() { return static_cast<int32_t>(offsetof(ContentManager2_t2980555998, ___objectWidget_24)); }
	inline GameObject_t1113636619 * get_objectWidget_24() const { return ___objectWidget_24; }
	inline GameObject_t1113636619 ** get_address_of_objectWidget_24() { return &___objectWidget_24; }
	inline void set_objectWidget_24(GameObject_t1113636619 * value)
	{
		___objectWidget_24 = value;
		Il2CppCodeGenWriteBarrier((&___objectWidget_24), value);
	}

	inline static int32_t get_offset_of_rawImgPortrait_25() { return static_cast<int32_t>(offsetof(ContentManager2_t2980555998, ___rawImgPortrait_25)); }
	inline RawImage_t3182918964 * get_rawImgPortrait_25() const { return ___rawImgPortrait_25; }
	inline RawImage_t3182918964 ** get_address_of_rawImgPortrait_25() { return &___rawImgPortrait_25; }
	inline void set_rawImgPortrait_25(RawImage_t3182918964 * value)
	{
		___rawImgPortrait_25 = value;
		Il2CppCodeGenWriteBarrier((&___rawImgPortrait_25), value);
	}

	inline static int32_t get_offset_of_rawImgLandscape_26() { return static_cast<int32_t>(offsetof(ContentManager2_t2980555998, ___rawImgLandscape_26)); }
	inline RawImage_t3182918964 * get_rawImgLandscape_26() const { return ___rawImgLandscape_26; }
	inline RawImage_t3182918964 ** get_address_of_rawImgLandscape_26() { return &___rawImgLandscape_26; }
	inline void set_rawImgLandscape_26(RawImage_t3182918964 * value)
	{
		___rawImgLandscape_26 = value;
		Il2CppCodeGenWriteBarrier((&___rawImgLandscape_26), value);
	}

	inline static int32_t get_offset_of_panelBack_27() { return static_cast<int32_t>(offsetof(ContentManager2_t2980555998, ___panelBack_27)); }
	inline GameObject_t1113636619 * get_panelBack_27() const { return ___panelBack_27; }
	inline GameObject_t1113636619 ** get_address_of_panelBack_27() { return &___panelBack_27; }
	inline void set_panelBack_27(GameObject_t1113636619 * value)
	{
		___panelBack_27 = value;
		Il2CppCodeGenWriteBarrier((&___panelBack_27), value);
	}

	inline static int32_t get_offset_of_CameraOBJContainer_28() { return static_cast<int32_t>(offsetof(ContentManager2_t2980555998, ___CameraOBJContainer_28)); }
	inline GameObject_t1113636619 * get_CameraOBJContainer_28() const { return ___CameraOBJContainer_28; }
	inline GameObject_t1113636619 ** get_address_of_CameraOBJContainer_28() { return &___CameraOBJContainer_28; }
	inline void set_CameraOBJContainer_28(GameObject_t1113636619 * value)
	{
		___CameraOBJContainer_28 = value;
		Il2CppCodeGenWriteBarrier((&___CameraOBJContainer_28), value);
	}

	inline static int32_t get_offset_of_FS_OBJ_container_29() { return static_cast<int32_t>(offsetof(ContentManager2_t2980555998, ___FS_OBJ_container_29)); }
	inline GameObject_t1113636619 * get_FS_OBJ_container_29() const { return ___FS_OBJ_container_29; }
	inline GameObject_t1113636619 ** get_address_of_FS_OBJ_container_29() { return &___FS_OBJ_container_29; }
	inline void set_FS_OBJ_container_29(GameObject_t1113636619 * value)
	{
		___FS_OBJ_container_29 = value;
		Il2CppCodeGenWriteBarrier((&___FS_OBJ_container_29), value);
	}

	inline static int32_t get_offset_of_closeButton_30() { return static_cast<int32_t>(offsetof(ContentManager2_t2980555998, ___closeButton_30)); }
	inline GameObject_t1113636619 * get_closeButton_30() const { return ___closeButton_30; }
	inline GameObject_t1113636619 ** get_address_of_closeButton_30() { return &___closeButton_30; }
	inline void set_closeButton_30(GameObject_t1113636619 * value)
	{
		___closeButton_30 = value;
		Il2CppCodeGenWriteBarrier((&___closeButton_30), value);
	}

	inline static int32_t get_offset_of_backButtonBackMarker_31() { return static_cast<int32_t>(offsetof(ContentManager2_t2980555998, ___backButtonBackMarker_31)); }
	inline GameObject_t1113636619 * get_backButtonBackMarker_31() const { return ___backButtonBackMarker_31; }
	inline GameObject_t1113636619 ** get_address_of_backButtonBackMarker_31() { return &___backButtonBackMarker_31; }
	inline void set_backButtonBackMarker_31(GameObject_t1113636619 * value)
	{
		___backButtonBackMarker_31 = value;
		Il2CppCodeGenWriteBarrier((&___backButtonBackMarker_31), value);
	}

	inline static int32_t get_offset_of_backMarkerON_32() { return static_cast<int32_t>(offsetof(ContentManager2_t2980555998, ___backMarkerON_32)); }
	inline GameObject_t1113636619 * get_backMarkerON_32() const { return ___backMarkerON_32; }
	inline GameObject_t1113636619 ** get_address_of_backMarkerON_32() { return &___backMarkerON_32; }
	inline void set_backMarkerON_32(GameObject_t1113636619 * value)
	{
		___backMarkerON_32 = value;
		Il2CppCodeGenWriteBarrier((&___backMarkerON_32), value);
	}

	inline static int32_t get_offset_of_backMarkerOFF_33() { return static_cast<int32_t>(offsetof(ContentManager2_t2980555998, ___backMarkerOFF_33)); }
	inline GameObject_t1113636619 * get_backMarkerOFF_33() const { return ___backMarkerOFF_33; }
	inline GameObject_t1113636619 ** get_address_of_backMarkerOFF_33() { return &___backMarkerOFF_33; }
	inline void set_backMarkerOFF_33(GameObject_t1113636619 * value)
	{
		___backMarkerOFF_33 = value;
		Il2CppCodeGenWriteBarrier((&___backMarkerOFF_33), value);
	}

	inline static int32_t get_offset_of_isShowingBackMarker_34() { return static_cast<int32_t>(offsetof(ContentManager2_t2980555998, ___isShowingBackMarker_34)); }
	inline bool get_isShowingBackMarker_34() const { return ___isShowingBackMarker_34; }
	inline bool* get_address_of_isShowingBackMarker_34() { return &___isShowingBackMarker_34; }
	inline void set_isShowingBackMarker_34(bool value)
	{
		___isShowingBackMarker_34 = value;
	}

	inline static int32_t get_offset_of_sphereCloseUp_35() { return static_cast<int32_t>(offsetof(ContentManager2_t2980555998, ___sphereCloseUp_35)); }
	inline GameObject_t1113636619 * get_sphereCloseUp_35() const { return ___sphereCloseUp_35; }
	inline GameObject_t1113636619 ** get_address_of_sphereCloseUp_35() { return &___sphereCloseUp_35; }
	inline void set_sphereCloseUp_35(GameObject_t1113636619 * value)
	{
		___sphereCloseUp_35 = value;
		Il2CppCodeGenWriteBarrier((&___sphereCloseUp_35), value);
	}

	inline static int32_t get_offset_of_cubeEtalon_36() { return static_cast<int32_t>(offsetof(ContentManager2_t2980555998, ___cubeEtalon_36)); }
	inline GameObject_t1113636619 * get_cubeEtalon_36() const { return ___cubeEtalon_36; }
	inline GameObject_t1113636619 ** get_address_of_cubeEtalon_36() { return &___cubeEtalon_36; }
	inline void set_cubeEtalon_36(GameObject_t1113636619 * value)
	{
		___cubeEtalon_36 = value;
		Il2CppCodeGenWriteBarrier((&___cubeEtalon_36), value);
	}

	inline static int32_t get_offset_of_closeMarkerButton_37() { return static_cast<int32_t>(offsetof(ContentManager2_t2980555998, ___closeMarkerButton_37)); }
	inline GameObject_t1113636619 * get_closeMarkerButton_37() const { return ___closeMarkerButton_37; }
	inline GameObject_t1113636619 ** get_address_of_closeMarkerButton_37() { return &___closeMarkerButton_37; }
	inline void set_closeMarkerButton_37(GameObject_t1113636619 * value)
	{
		___closeMarkerButton_37 = value;
		Il2CppCodeGenWriteBarrier((&___closeMarkerButton_37), value);
	}

	inline static int32_t get_offset_of_sliderZoom_38() { return static_cast<int32_t>(offsetof(ContentManager2_t2980555998, ___sliderZoom_38)); }
	inline Slider_t3903728902 * get_sliderZoom_38() const { return ___sliderZoom_38; }
	inline Slider_t3903728902 ** get_address_of_sliderZoom_38() { return &___sliderZoom_38; }
	inline void set_sliderZoom_38(Slider_t3903728902 * value)
	{
		___sliderZoom_38 = value;
		Il2CppCodeGenWriteBarrier((&___sliderZoom_38), value);
	}

	inline static int32_t get_offset_of_cancelButton_39() { return static_cast<int32_t>(offsetof(ContentManager2_t2980555998, ___cancelButton_39)); }
	inline GameObject_t1113636619 * get_cancelButton_39() const { return ___cancelButton_39; }
	inline GameObject_t1113636619 ** get_address_of_cancelButton_39() { return &___cancelButton_39; }
	inline void set_cancelButton_39(GameObject_t1113636619 * value)
	{
		___cancelButton_39 = value;
		Il2CppCodeGenWriteBarrier((&___cancelButton_39), value);
	}

	inline static int32_t get_offset_of_fullscreen3DObject_40() { return static_cast<int32_t>(offsetof(ContentManager2_t2980555998, ___fullscreen3DObject_40)); }
	inline GameObject_t1113636619 * get_fullscreen3DObject_40() const { return ___fullscreen3DObject_40; }
	inline GameObject_t1113636619 ** get_address_of_fullscreen3DObject_40() { return &___fullscreen3DObject_40; }
	inline void set_fullscreen3DObject_40(GameObject_t1113636619 * value)
	{
		___fullscreen3DObject_40 = value;
		Il2CppCodeGenWriteBarrier((&___fullscreen3DObject_40), value);
	}

	inline static int32_t get_offset_of_emailPanel_41() { return static_cast<int32_t>(offsetof(ContentManager2_t2980555998, ___emailPanel_41)); }
	inline GameObject_t1113636619 * get_emailPanel_41() const { return ___emailPanel_41; }
	inline GameObject_t1113636619 ** get_address_of_emailPanel_41() { return &___emailPanel_41; }
	inline void set_emailPanel_41(GameObject_t1113636619 * value)
	{
		___emailPanel_41 = value;
		Il2CppCodeGenWriteBarrier((&___emailPanel_41), value);
	}

	inline static int32_t get_offset_of_emailPanel_mail_42() { return static_cast<int32_t>(offsetof(ContentManager2_t2980555998, ___emailPanel_mail_42)); }
	inline TextMeshProUGUI_t529313277 * get_emailPanel_mail_42() const { return ___emailPanel_mail_42; }
	inline TextMeshProUGUI_t529313277 ** get_address_of_emailPanel_mail_42() { return &___emailPanel_mail_42; }
	inline void set_emailPanel_mail_42(TextMeshProUGUI_t529313277 * value)
	{
		___emailPanel_mail_42 = value;
		Il2CppCodeGenWriteBarrier((&___emailPanel_mail_42), value);
	}

	inline static int32_t get_offset_of_emailPanel_text_43() { return static_cast<int32_t>(offsetof(ContentManager2_t2980555998, ___emailPanel_text_43)); }
	inline TextMeshProUGUI_t529313277 * get_emailPanel_text_43() const { return ___emailPanel_text_43; }
	inline TextMeshProUGUI_t529313277 ** get_address_of_emailPanel_text_43() { return &___emailPanel_text_43; }
	inline void set_emailPanel_text_43(TextMeshProUGUI_t529313277 * value)
	{
		___emailPanel_text_43 = value;
		Il2CppCodeGenWriteBarrier((&___emailPanel_text_43), value);
	}

	inline static int32_t get_offset_of_emailPanel_button_44() { return static_cast<int32_t>(offsetof(ContentManager2_t2980555998, ___emailPanel_button_44)); }
	inline Button_t4055032469 * get_emailPanel_button_44() const { return ___emailPanel_button_44; }
	inline Button_t4055032469 ** get_address_of_emailPanel_button_44() { return &___emailPanel_button_44; }
	inline void set_emailPanel_button_44(Button_t4055032469 * value)
	{
		___emailPanel_button_44 = value;
		Il2CppCodeGenWriteBarrier((&___emailPanel_button_44), value);
	}

	inline static int32_t get_offset_of_screenShotURL_45() { return static_cast<int32_t>(offsetof(ContentManager2_t2980555998, ___screenShotURL_45)); }
	inline String_t* get_screenShotURL_45() const { return ___screenShotURL_45; }
	inline String_t** get_address_of_screenShotURL_45() { return &___screenShotURL_45; }
	inline void set_screenShotURL_45(String_t* value)
	{
		___screenShotURL_45 = value;
		Il2CppCodeGenWriteBarrier((&___screenShotURL_45), value);
	}

	inline static int32_t get_offset_of_nodeTex_46() { return static_cast<int32_t>(offsetof(ContentManager2_t2980555998, ___nodeTex_46)); }
	inline GameObject_t1113636619 * get_nodeTex_46() const { return ___nodeTex_46; }
	inline GameObject_t1113636619 ** get_address_of_nodeTex_46() { return &___nodeTex_46; }
	inline void set_nodeTex_46(GameObject_t1113636619 * value)
	{
		___nodeTex_46 = value;
		Il2CppCodeGenWriteBarrier((&___nodeTex_46), value);
	}

	inline static int32_t get_offset_of_mPixelFormat_47() { return static_cast<int32_t>(offsetof(ContentManager2_t2980555998, ___mPixelFormat_47)); }
	inline int32_t get_mPixelFormat_47() const { return ___mPixelFormat_47; }
	inline int32_t* get_address_of_mPixelFormat_47() { return &___mPixelFormat_47; }
	inline void set_mPixelFormat_47(int32_t value)
	{
		___mPixelFormat_47 = value;
	}

	inline static int32_t get_offset_of_mFormatRegistered_48() { return static_cast<int32_t>(offsetof(ContentManager2_t2980555998, ___mFormatRegistered_48)); }
	inline bool get_mFormatRegistered_48() const { return ___mFormatRegistered_48; }
	inline bool* get_address_of_mFormatRegistered_48() { return &___mFormatRegistered_48; }
	inline void set_mFormatRegistered_48(bool value)
	{
		___mFormatRegistered_48 = value;
	}

	inline static int32_t get_offset_of_mCloudRecoBehaviour_49() { return static_cast<int32_t>(offsetof(ContentManager2_t2980555998, ___mCloudRecoBehaviour_49)); }
	inline CloudRecoBehaviour_t431762792 * get_mCloudRecoBehaviour_49() const { return ___mCloudRecoBehaviour_49; }
	inline CloudRecoBehaviour_t431762792 ** get_address_of_mCloudRecoBehaviour_49() { return &___mCloudRecoBehaviour_49; }
	inline void set_mCloudRecoBehaviour_49(CloudRecoBehaviour_t431762792 * value)
	{
		___mCloudRecoBehaviour_49 = value;
		Il2CppCodeGenWriteBarrier((&___mCloudRecoBehaviour_49), value);
	}

	inline static int32_t get_offset_of_mTrackableBehaviour_50() { return static_cast<int32_t>(offsetof(ContentManager2_t2980555998, ___mTrackableBehaviour_50)); }
	inline TrackableBehaviour_t1113559212 * get_mTrackableBehaviour_50() const { return ___mTrackableBehaviour_50; }
	inline TrackableBehaviour_t1113559212 ** get_address_of_mTrackableBehaviour_50() { return &___mTrackableBehaviour_50; }
	inline void set_mTrackableBehaviour_50(TrackableBehaviour_t1113559212 * value)
	{
		___mTrackableBehaviour_50 = value;
		Il2CppCodeGenWriteBarrier((&___mTrackableBehaviour_50), value);
	}

	inline static int32_t get_offset_of_mIsShowingMarkerData_51() { return static_cast<int32_t>(offsetof(ContentManager2_t2980555998, ___mIsShowingMarkerData_51)); }
	inline bool get_mIsShowingMarkerData_51() const { return ___mIsShowingMarkerData_51; }
	inline bool* get_address_of_mIsShowingMarkerData_51() { return &___mIsShowingMarkerData_51; }
	inline void set_mIsShowingMarkerData_51(bool value)
	{
		___mIsShowingMarkerData_51 = value;
	}

	inline static int32_t get_offset_of_currentMarker_52() { return static_cast<int32_t>(offsetof(ContentManager2_t2980555998, ___currentMarker_52)); }
	inline String_t* get_currentMarker_52() const { return ___currentMarker_52; }
	inline String_t** get_address_of_currentMarker_52() { return &___currentMarker_52; }
	inline void set_currentMarker_52(String_t* value)
	{
		___currentMarker_52 = value;
		Il2CppCodeGenWriteBarrier((&___currentMarker_52), value);
	}

	inline static int32_t get_offset_of_isTracking3D_53() { return static_cast<int32_t>(offsetof(ContentManager2_t2980555998, ___isTracking3D_53)); }
	inline bool get_isTracking3D_53() const { return ___isTracking3D_53; }
	inline bool* get_address_of_isTracking3D_53() { return &___isTracking3D_53; }
	inline void set_isTracking3D_53(bool value)
	{
		___isTracking3D_53 = value;
	}

	inline static int32_t get_offset_of_pano_regles_54() { return static_cast<int32_t>(offsetof(ContentManager2_t2980555998, ___pano_regles_54)); }
	inline GameObject_t1113636619 * get_pano_regles_54() const { return ___pano_regles_54; }
	inline GameObject_t1113636619 ** get_address_of_pano_regles_54() { return &___pano_regles_54; }
	inline void set_pano_regles_54(GameObject_t1113636619 * value)
	{
		___pano_regles_54 = value;
		Il2CppCodeGenWriteBarrier((&___pano_regles_54), value);
	}

	inline static int32_t get_offset_of_pano_data_55() { return static_cast<int32_t>(offsetof(ContentManager2_t2980555998, ___pano_data_55)); }
	inline GameObject_t1113636619 * get_pano_data_55() const { return ___pano_data_55; }
	inline GameObject_t1113636619 ** get_address_of_pano_data_55() { return &___pano_data_55; }
	inline void set_pano_data_55(GameObject_t1113636619 * value)
	{
		___pano_data_55 = value;
		Il2CppCodeGenWriteBarrier((&___pano_data_55), value);
	}

	inline static int32_t get_offset_of_pano_error_56() { return static_cast<int32_t>(offsetof(ContentManager2_t2980555998, ___pano_error_56)); }
	inline GameObject_t1113636619 * get_pano_error_56() const { return ___pano_error_56; }
	inline GameObject_t1113636619 ** get_address_of_pano_error_56() { return &___pano_error_56; }
	inline void set_pano_error_56(GameObject_t1113636619 * value)
	{
		___pano_error_56 = value;
		Il2CppCodeGenWriteBarrier((&___pano_error_56), value);
	}

	inline static int32_t get_offset_of_pano_gagne_57() { return static_cast<int32_t>(offsetof(ContentManager2_t2980555998, ___pano_gagne_57)); }
	inline GameObject_t1113636619 * get_pano_gagne_57() const { return ___pano_gagne_57; }
	inline GameObject_t1113636619 ** get_address_of_pano_gagne_57() { return &___pano_gagne_57; }
	inline void set_pano_gagne_57(GameObject_t1113636619 * value)
	{
		___pano_gagne_57 = value;
		Il2CppCodeGenWriteBarrier((&___pano_gagne_57), value);
	}
};

struct ContentManager2_t2980555998_StaticFields
{
public:
	// System.String ContentManager2::baseName
	String_t* ___baseName_4;

public:
	inline static int32_t get_offset_of_baseName_4() { return static_cast<int32_t>(offsetof(ContentManager2_t2980555998_StaticFields, ___baseName_4)); }
	inline String_t* get_baseName_4() const { return ___baseName_4; }
	inline String_t** get_address_of_baseName_4() { return &___baseName_4; }
	inline void set_baseName_4(String_t* value)
	{
		___baseName_4 = value;
		Il2CppCodeGenWriteBarrier((&___baseName_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTENTMANAGER2_T2980555998_H
#ifndef PAGETRANSITION_T3158442731_H
#define PAGETRANSITION_T3158442731_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PageTransition
struct  PageTransition_t3158442731  : public MonoBehaviour_t3962482529
{
public:
	// Page[] PageTransition::pages
	PageU5BU5D_t2014447836* ___pages_4;
	// UnityEngine.RectTransform PageTransition::anchorTop
	RectTransform_t3704657025 * ___anchorTop_5;
	// UnityEngine.RectTransform PageTransition::anchorBottom
	RectTransform_t3704657025 * ___anchorBottom_6;
	// UnityEngine.RectTransform PageTransition::anchorLeft
	RectTransform_t3704657025 * ___anchorLeft_7;
	// UnityEngine.RectTransform PageTransition::anchorRight
	RectTransform_t3704657025 * ___anchorRight_8;
	// Page PageTransition::CurrentPage
	Page_t2586579457 * ___CurrentPage_9;
	// System.Boolean PageTransition::isTransiting
	bool ___isTransiting_10;

public:
	inline static int32_t get_offset_of_pages_4() { return static_cast<int32_t>(offsetof(PageTransition_t3158442731, ___pages_4)); }
	inline PageU5BU5D_t2014447836* get_pages_4() const { return ___pages_4; }
	inline PageU5BU5D_t2014447836** get_address_of_pages_4() { return &___pages_4; }
	inline void set_pages_4(PageU5BU5D_t2014447836* value)
	{
		___pages_4 = value;
		Il2CppCodeGenWriteBarrier((&___pages_4), value);
	}

	inline static int32_t get_offset_of_anchorTop_5() { return static_cast<int32_t>(offsetof(PageTransition_t3158442731, ___anchorTop_5)); }
	inline RectTransform_t3704657025 * get_anchorTop_5() const { return ___anchorTop_5; }
	inline RectTransform_t3704657025 ** get_address_of_anchorTop_5() { return &___anchorTop_5; }
	inline void set_anchorTop_5(RectTransform_t3704657025 * value)
	{
		___anchorTop_5 = value;
		Il2CppCodeGenWriteBarrier((&___anchorTop_5), value);
	}

	inline static int32_t get_offset_of_anchorBottom_6() { return static_cast<int32_t>(offsetof(PageTransition_t3158442731, ___anchorBottom_6)); }
	inline RectTransform_t3704657025 * get_anchorBottom_6() const { return ___anchorBottom_6; }
	inline RectTransform_t3704657025 ** get_address_of_anchorBottom_6() { return &___anchorBottom_6; }
	inline void set_anchorBottom_6(RectTransform_t3704657025 * value)
	{
		___anchorBottom_6 = value;
		Il2CppCodeGenWriteBarrier((&___anchorBottom_6), value);
	}

	inline static int32_t get_offset_of_anchorLeft_7() { return static_cast<int32_t>(offsetof(PageTransition_t3158442731, ___anchorLeft_7)); }
	inline RectTransform_t3704657025 * get_anchorLeft_7() const { return ___anchorLeft_7; }
	inline RectTransform_t3704657025 ** get_address_of_anchorLeft_7() { return &___anchorLeft_7; }
	inline void set_anchorLeft_7(RectTransform_t3704657025 * value)
	{
		___anchorLeft_7 = value;
		Il2CppCodeGenWriteBarrier((&___anchorLeft_7), value);
	}

	inline static int32_t get_offset_of_anchorRight_8() { return static_cast<int32_t>(offsetof(PageTransition_t3158442731, ___anchorRight_8)); }
	inline RectTransform_t3704657025 * get_anchorRight_8() const { return ___anchorRight_8; }
	inline RectTransform_t3704657025 ** get_address_of_anchorRight_8() { return &___anchorRight_8; }
	inline void set_anchorRight_8(RectTransform_t3704657025 * value)
	{
		___anchorRight_8 = value;
		Il2CppCodeGenWriteBarrier((&___anchorRight_8), value);
	}

	inline static int32_t get_offset_of_CurrentPage_9() { return static_cast<int32_t>(offsetof(PageTransition_t3158442731, ___CurrentPage_9)); }
	inline Page_t2586579457 * get_CurrentPage_9() const { return ___CurrentPage_9; }
	inline Page_t2586579457 ** get_address_of_CurrentPage_9() { return &___CurrentPage_9; }
	inline void set_CurrentPage_9(Page_t2586579457 * value)
	{
		___CurrentPage_9 = value;
		Il2CppCodeGenWriteBarrier((&___CurrentPage_9), value);
	}

	inline static int32_t get_offset_of_isTransiting_10() { return static_cast<int32_t>(offsetof(PageTransition_t3158442731, ___isTransiting_10)); }
	inline bool get_isTransiting_10() const { return ___isTransiting_10; }
	inline bool* get_address_of_isTransiting_10() { return &___isTransiting_10; }
	inline void set_isTransiting_10(bool value)
	{
		___isTransiting_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PAGETRANSITION_T3158442731_H
#ifndef TUTOMANAGER_T4000134877_H
#define TUTOMANAGER_T4000134877_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TutoManager
struct  TutoManager_t4000134877  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject TutoManager::demo1
	GameObject_t1113636619 * ___demo1_4;
	// UnityEngine.GameObject TutoManager::demo2
	GameObject_t1113636619 * ___demo2_5;
	// UnityEngine.GameObject TutoManager::demo3
	GameObject_t1113636619 * ___demo3_6;
	// System.String TutoManager::url
	String_t* ___url_7;

public:
	inline static int32_t get_offset_of_demo1_4() { return static_cast<int32_t>(offsetof(TutoManager_t4000134877, ___demo1_4)); }
	inline GameObject_t1113636619 * get_demo1_4() const { return ___demo1_4; }
	inline GameObject_t1113636619 ** get_address_of_demo1_4() { return &___demo1_4; }
	inline void set_demo1_4(GameObject_t1113636619 * value)
	{
		___demo1_4 = value;
		Il2CppCodeGenWriteBarrier((&___demo1_4), value);
	}

	inline static int32_t get_offset_of_demo2_5() { return static_cast<int32_t>(offsetof(TutoManager_t4000134877, ___demo2_5)); }
	inline GameObject_t1113636619 * get_demo2_5() const { return ___demo2_5; }
	inline GameObject_t1113636619 ** get_address_of_demo2_5() { return &___demo2_5; }
	inline void set_demo2_5(GameObject_t1113636619 * value)
	{
		___demo2_5 = value;
		Il2CppCodeGenWriteBarrier((&___demo2_5), value);
	}

	inline static int32_t get_offset_of_demo3_6() { return static_cast<int32_t>(offsetof(TutoManager_t4000134877, ___demo3_6)); }
	inline GameObject_t1113636619 * get_demo3_6() const { return ___demo3_6; }
	inline GameObject_t1113636619 ** get_address_of_demo3_6() { return &___demo3_6; }
	inline void set_demo3_6(GameObject_t1113636619 * value)
	{
		___demo3_6 = value;
		Il2CppCodeGenWriteBarrier((&___demo3_6), value);
	}

	inline static int32_t get_offset_of_url_7() { return static_cast<int32_t>(offsetof(TutoManager_t4000134877, ___url_7)); }
	inline String_t* get_url_7() const { return ___url_7; }
	inline String_t** get_address_of_url_7() { return &___url_7; }
	inline void set_url_7(String_t* value)
	{
		___url_7 = value;
		Il2CppCodeGenWriteBarrier((&___url_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TUTOMANAGER_T4000134877_H
#ifndef UIMANAGER_T1042050227_H
#define UIMANAGER_T1042050227_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIManager
struct  UIManager_t1042050227  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject UIManager::tuto
	GameObject_t1113636619 * ___tuto_4;
	// UnityEngine.GameObject UIManager::tutoButton
	GameObject_t1113636619 * ___tutoButton_5;
	// UnityEngine.GameObject UIManager::credit
	GameObject_t1113636619 * ___credit_6;
	// UnityEngine.GameObject UIManager::creditPanel
	GameObject_t1113636619 * ___creditPanel_7;
	// UnityEngine.GameObject UIManager::Menu
	GameObject_t1113636619 * ___Menu_8;
	// UnityEngine.GameObject UIManager::mainPanel
	GameObject_t1113636619 * ___mainPanel_9;
	// PageTransition UIManager::pageTransition
	PageTransition_t3158442731 * ___pageTransition_10;
	// System.Boolean UIManager::panelOpen
	bool ___panelOpen_11;
	// System.Boolean UIManager::creditOpen
	bool ___creditOpen_12;

public:
	inline static int32_t get_offset_of_tuto_4() { return static_cast<int32_t>(offsetof(UIManager_t1042050227, ___tuto_4)); }
	inline GameObject_t1113636619 * get_tuto_4() const { return ___tuto_4; }
	inline GameObject_t1113636619 ** get_address_of_tuto_4() { return &___tuto_4; }
	inline void set_tuto_4(GameObject_t1113636619 * value)
	{
		___tuto_4 = value;
		Il2CppCodeGenWriteBarrier((&___tuto_4), value);
	}

	inline static int32_t get_offset_of_tutoButton_5() { return static_cast<int32_t>(offsetof(UIManager_t1042050227, ___tutoButton_5)); }
	inline GameObject_t1113636619 * get_tutoButton_5() const { return ___tutoButton_5; }
	inline GameObject_t1113636619 ** get_address_of_tutoButton_5() { return &___tutoButton_5; }
	inline void set_tutoButton_5(GameObject_t1113636619 * value)
	{
		___tutoButton_5 = value;
		Il2CppCodeGenWriteBarrier((&___tutoButton_5), value);
	}

	inline static int32_t get_offset_of_credit_6() { return static_cast<int32_t>(offsetof(UIManager_t1042050227, ___credit_6)); }
	inline GameObject_t1113636619 * get_credit_6() const { return ___credit_6; }
	inline GameObject_t1113636619 ** get_address_of_credit_6() { return &___credit_6; }
	inline void set_credit_6(GameObject_t1113636619 * value)
	{
		___credit_6 = value;
		Il2CppCodeGenWriteBarrier((&___credit_6), value);
	}

	inline static int32_t get_offset_of_creditPanel_7() { return static_cast<int32_t>(offsetof(UIManager_t1042050227, ___creditPanel_7)); }
	inline GameObject_t1113636619 * get_creditPanel_7() const { return ___creditPanel_7; }
	inline GameObject_t1113636619 ** get_address_of_creditPanel_7() { return &___creditPanel_7; }
	inline void set_creditPanel_7(GameObject_t1113636619 * value)
	{
		___creditPanel_7 = value;
		Il2CppCodeGenWriteBarrier((&___creditPanel_7), value);
	}

	inline static int32_t get_offset_of_Menu_8() { return static_cast<int32_t>(offsetof(UIManager_t1042050227, ___Menu_8)); }
	inline GameObject_t1113636619 * get_Menu_8() const { return ___Menu_8; }
	inline GameObject_t1113636619 ** get_address_of_Menu_8() { return &___Menu_8; }
	inline void set_Menu_8(GameObject_t1113636619 * value)
	{
		___Menu_8 = value;
		Il2CppCodeGenWriteBarrier((&___Menu_8), value);
	}

	inline static int32_t get_offset_of_mainPanel_9() { return static_cast<int32_t>(offsetof(UIManager_t1042050227, ___mainPanel_9)); }
	inline GameObject_t1113636619 * get_mainPanel_9() const { return ___mainPanel_9; }
	inline GameObject_t1113636619 ** get_address_of_mainPanel_9() { return &___mainPanel_9; }
	inline void set_mainPanel_9(GameObject_t1113636619 * value)
	{
		___mainPanel_9 = value;
		Il2CppCodeGenWriteBarrier((&___mainPanel_9), value);
	}

	inline static int32_t get_offset_of_pageTransition_10() { return static_cast<int32_t>(offsetof(UIManager_t1042050227, ___pageTransition_10)); }
	inline PageTransition_t3158442731 * get_pageTransition_10() const { return ___pageTransition_10; }
	inline PageTransition_t3158442731 ** get_address_of_pageTransition_10() { return &___pageTransition_10; }
	inline void set_pageTransition_10(PageTransition_t3158442731 * value)
	{
		___pageTransition_10 = value;
		Il2CppCodeGenWriteBarrier((&___pageTransition_10), value);
	}

	inline static int32_t get_offset_of_panelOpen_11() { return static_cast<int32_t>(offsetof(UIManager_t1042050227, ___panelOpen_11)); }
	inline bool get_panelOpen_11() const { return ___panelOpen_11; }
	inline bool* get_address_of_panelOpen_11() { return &___panelOpen_11; }
	inline void set_panelOpen_11(bool value)
	{
		___panelOpen_11 = value;
	}

	inline static int32_t get_offset_of_creditOpen_12() { return static_cast<int32_t>(offsetof(UIManager_t1042050227, ___creditOpen_12)); }
	inline bool get_creditOpen_12() const { return ___creditOpen_12; }
	inline bool* get_address_of_creditOpen_12() { return &___creditOpen_12; }
	inline void set_creditOpen_12(bool value)
	{
		___creditOpen_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIMANAGER_T1042050227_H
#ifndef UIBEHAVIOUR_T3495933518_H
#define UIBEHAVIOUR_T3495933518_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_t3495933518  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBEHAVIOUR_T3495933518_H
#ifndef ZOOMOBJECT_T2903076344_H
#define ZOOMOBJECT_T2903076344_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZoomObject
struct  ZoomObject_t2903076344  : public MonoBehaviour_t3962482529
{
public:
	// System.Single ZoomObject::maxZoomIn
	float ___maxZoomIn_4;
	// System.Single ZoomObject::maxZoomOut
	float ___maxZoomOut_5;
	// UnityEngine.Transform ZoomObject::target
	Transform_t3600365921 * ___target_6;
	// UnityEngine.Vector3 ZoomObject::originalZoom
	Vector3_t3722313464  ___originalZoom_7;
	// System.Boolean ZoomObject::zoomIn
	bool ___zoomIn_8;
	// System.Single ZoomObject::delta
	float ___delta_9;
	// System.Single ZoomObject::zoom
	float ___zoom_10;
	// System.Boolean ZoomObject::isZooming
	bool ___isZooming_11;

public:
	inline static int32_t get_offset_of_maxZoomIn_4() { return static_cast<int32_t>(offsetof(ZoomObject_t2903076344, ___maxZoomIn_4)); }
	inline float get_maxZoomIn_4() const { return ___maxZoomIn_4; }
	inline float* get_address_of_maxZoomIn_4() { return &___maxZoomIn_4; }
	inline void set_maxZoomIn_4(float value)
	{
		___maxZoomIn_4 = value;
	}

	inline static int32_t get_offset_of_maxZoomOut_5() { return static_cast<int32_t>(offsetof(ZoomObject_t2903076344, ___maxZoomOut_5)); }
	inline float get_maxZoomOut_5() const { return ___maxZoomOut_5; }
	inline float* get_address_of_maxZoomOut_5() { return &___maxZoomOut_5; }
	inline void set_maxZoomOut_5(float value)
	{
		___maxZoomOut_5 = value;
	}

	inline static int32_t get_offset_of_target_6() { return static_cast<int32_t>(offsetof(ZoomObject_t2903076344, ___target_6)); }
	inline Transform_t3600365921 * get_target_6() const { return ___target_6; }
	inline Transform_t3600365921 ** get_address_of_target_6() { return &___target_6; }
	inline void set_target_6(Transform_t3600365921 * value)
	{
		___target_6 = value;
		Il2CppCodeGenWriteBarrier((&___target_6), value);
	}

	inline static int32_t get_offset_of_originalZoom_7() { return static_cast<int32_t>(offsetof(ZoomObject_t2903076344, ___originalZoom_7)); }
	inline Vector3_t3722313464  get_originalZoom_7() const { return ___originalZoom_7; }
	inline Vector3_t3722313464 * get_address_of_originalZoom_7() { return &___originalZoom_7; }
	inline void set_originalZoom_7(Vector3_t3722313464  value)
	{
		___originalZoom_7 = value;
	}

	inline static int32_t get_offset_of_zoomIn_8() { return static_cast<int32_t>(offsetof(ZoomObject_t2903076344, ___zoomIn_8)); }
	inline bool get_zoomIn_8() const { return ___zoomIn_8; }
	inline bool* get_address_of_zoomIn_8() { return &___zoomIn_8; }
	inline void set_zoomIn_8(bool value)
	{
		___zoomIn_8 = value;
	}

	inline static int32_t get_offset_of_delta_9() { return static_cast<int32_t>(offsetof(ZoomObject_t2903076344, ___delta_9)); }
	inline float get_delta_9() const { return ___delta_9; }
	inline float* get_address_of_delta_9() { return &___delta_9; }
	inline void set_delta_9(float value)
	{
		___delta_9 = value;
	}

	inline static int32_t get_offset_of_zoom_10() { return static_cast<int32_t>(offsetof(ZoomObject_t2903076344, ___zoom_10)); }
	inline float get_zoom_10() const { return ___zoom_10; }
	inline float* get_address_of_zoom_10() { return &___zoom_10; }
	inline void set_zoom_10(float value)
	{
		___zoom_10 = value;
	}

	inline static int32_t get_offset_of_isZooming_11() { return static_cast<int32_t>(offsetof(ZoomObject_t2903076344, ___isZooming_11)); }
	inline bool get_isZooming_11() const { return ___isZooming_11; }
	inline bool* get_address_of_isZooming_11() { return &___isZooming_11; }
	inline void set_isZooming_11(bool value)
	{
		___isZooming_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ZOOMOBJECT_T2903076344_H
#ifndef ANIMATIONIMAGE_T1423068421_H
#define ANIMATIONIMAGE_T1423068421_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// animationImage
struct  animationImage_t1423068421  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Texture2D animationImage::tex
	Texture2D_t3840446185 * ___tex_4;
	// UnityEngine.Sprite animationImage::mySprite
	Sprite_t280657092 * ___mySprite_5;
	// UnityEngine.Sprite animationImage::mySpriteResult
	Sprite_t280657092 * ___mySpriteResult_6;
	// UnityEngine.SpriteRenderer animationImage::sr
	SpriteRenderer_t3235626157 * ___sr_7;
	// UnityEngine.SpriteRenderer animationImage::srResult
	SpriteRenderer_t3235626157 * ___srResult_8;
	// UnityEngine.GameObject animationImage::newSpriteObj
	GameObject_t1113636619 * ___newSpriteObj_9;
	// UnityEngine.GameObject animationImage::newSpriteResult
	GameObject_t1113636619 * ___newSpriteResult_10;
	// UnityEngine.Color animationImage::C
	Color_t2555686324  ___C_11;
	// System.Single animationImage::pixelByUnity
	float ___pixelByUnity_12;
	// System.Collections.Generic.List`1<Pixel> animationImage::listePixels
	List_1_t4186031580 * ___listePixels_13;
	// System.Collections.Generic.List`1<System.Collections.Generic.List`1<Pixel>> animationImage::listeColPixels
	List_1_t1363139026 * ___listeColPixels_14;
	// System.Int32 animationImage::ChoixAnimation
	int32_t ___ChoixAnimation_15;
	// System.Boolean animationImage::animationFinie
	bool ___animationFinie_16;
	// System.Boolean animationImage::affichageFini
	bool ___affichageFini_17;
	// System.Int32 animationImage::numPix
	int32_t ___numPix_18;
	// System.Int32 animationImage::nbPix
	int32_t ___nbPix_19;
	// System.Int32 animationImage::nbAfficher
	int32_t ___nbAfficher_20;

public:
	inline static int32_t get_offset_of_tex_4() { return static_cast<int32_t>(offsetof(animationImage_t1423068421, ___tex_4)); }
	inline Texture2D_t3840446185 * get_tex_4() const { return ___tex_4; }
	inline Texture2D_t3840446185 ** get_address_of_tex_4() { return &___tex_4; }
	inline void set_tex_4(Texture2D_t3840446185 * value)
	{
		___tex_4 = value;
		Il2CppCodeGenWriteBarrier((&___tex_4), value);
	}

	inline static int32_t get_offset_of_mySprite_5() { return static_cast<int32_t>(offsetof(animationImage_t1423068421, ___mySprite_5)); }
	inline Sprite_t280657092 * get_mySprite_5() const { return ___mySprite_5; }
	inline Sprite_t280657092 ** get_address_of_mySprite_5() { return &___mySprite_5; }
	inline void set_mySprite_5(Sprite_t280657092 * value)
	{
		___mySprite_5 = value;
		Il2CppCodeGenWriteBarrier((&___mySprite_5), value);
	}

	inline static int32_t get_offset_of_mySpriteResult_6() { return static_cast<int32_t>(offsetof(animationImage_t1423068421, ___mySpriteResult_6)); }
	inline Sprite_t280657092 * get_mySpriteResult_6() const { return ___mySpriteResult_6; }
	inline Sprite_t280657092 ** get_address_of_mySpriteResult_6() { return &___mySpriteResult_6; }
	inline void set_mySpriteResult_6(Sprite_t280657092 * value)
	{
		___mySpriteResult_6 = value;
		Il2CppCodeGenWriteBarrier((&___mySpriteResult_6), value);
	}

	inline static int32_t get_offset_of_sr_7() { return static_cast<int32_t>(offsetof(animationImage_t1423068421, ___sr_7)); }
	inline SpriteRenderer_t3235626157 * get_sr_7() const { return ___sr_7; }
	inline SpriteRenderer_t3235626157 ** get_address_of_sr_7() { return &___sr_7; }
	inline void set_sr_7(SpriteRenderer_t3235626157 * value)
	{
		___sr_7 = value;
		Il2CppCodeGenWriteBarrier((&___sr_7), value);
	}

	inline static int32_t get_offset_of_srResult_8() { return static_cast<int32_t>(offsetof(animationImage_t1423068421, ___srResult_8)); }
	inline SpriteRenderer_t3235626157 * get_srResult_8() const { return ___srResult_8; }
	inline SpriteRenderer_t3235626157 ** get_address_of_srResult_8() { return &___srResult_8; }
	inline void set_srResult_8(SpriteRenderer_t3235626157 * value)
	{
		___srResult_8 = value;
		Il2CppCodeGenWriteBarrier((&___srResult_8), value);
	}

	inline static int32_t get_offset_of_newSpriteObj_9() { return static_cast<int32_t>(offsetof(animationImage_t1423068421, ___newSpriteObj_9)); }
	inline GameObject_t1113636619 * get_newSpriteObj_9() const { return ___newSpriteObj_9; }
	inline GameObject_t1113636619 ** get_address_of_newSpriteObj_9() { return &___newSpriteObj_9; }
	inline void set_newSpriteObj_9(GameObject_t1113636619 * value)
	{
		___newSpriteObj_9 = value;
		Il2CppCodeGenWriteBarrier((&___newSpriteObj_9), value);
	}

	inline static int32_t get_offset_of_newSpriteResult_10() { return static_cast<int32_t>(offsetof(animationImage_t1423068421, ___newSpriteResult_10)); }
	inline GameObject_t1113636619 * get_newSpriteResult_10() const { return ___newSpriteResult_10; }
	inline GameObject_t1113636619 ** get_address_of_newSpriteResult_10() { return &___newSpriteResult_10; }
	inline void set_newSpriteResult_10(GameObject_t1113636619 * value)
	{
		___newSpriteResult_10 = value;
		Il2CppCodeGenWriteBarrier((&___newSpriteResult_10), value);
	}

	inline static int32_t get_offset_of_C_11() { return static_cast<int32_t>(offsetof(animationImage_t1423068421, ___C_11)); }
	inline Color_t2555686324  get_C_11() const { return ___C_11; }
	inline Color_t2555686324 * get_address_of_C_11() { return &___C_11; }
	inline void set_C_11(Color_t2555686324  value)
	{
		___C_11 = value;
	}

	inline static int32_t get_offset_of_pixelByUnity_12() { return static_cast<int32_t>(offsetof(animationImage_t1423068421, ___pixelByUnity_12)); }
	inline float get_pixelByUnity_12() const { return ___pixelByUnity_12; }
	inline float* get_address_of_pixelByUnity_12() { return &___pixelByUnity_12; }
	inline void set_pixelByUnity_12(float value)
	{
		___pixelByUnity_12 = value;
	}

	inline static int32_t get_offset_of_listePixels_13() { return static_cast<int32_t>(offsetof(animationImage_t1423068421, ___listePixels_13)); }
	inline List_1_t4186031580 * get_listePixels_13() const { return ___listePixels_13; }
	inline List_1_t4186031580 ** get_address_of_listePixels_13() { return &___listePixels_13; }
	inline void set_listePixels_13(List_1_t4186031580 * value)
	{
		___listePixels_13 = value;
		Il2CppCodeGenWriteBarrier((&___listePixels_13), value);
	}

	inline static int32_t get_offset_of_listeColPixels_14() { return static_cast<int32_t>(offsetof(animationImage_t1423068421, ___listeColPixels_14)); }
	inline List_1_t1363139026 * get_listeColPixels_14() const { return ___listeColPixels_14; }
	inline List_1_t1363139026 ** get_address_of_listeColPixels_14() { return &___listeColPixels_14; }
	inline void set_listeColPixels_14(List_1_t1363139026 * value)
	{
		___listeColPixels_14 = value;
		Il2CppCodeGenWriteBarrier((&___listeColPixels_14), value);
	}

	inline static int32_t get_offset_of_ChoixAnimation_15() { return static_cast<int32_t>(offsetof(animationImage_t1423068421, ___ChoixAnimation_15)); }
	inline int32_t get_ChoixAnimation_15() const { return ___ChoixAnimation_15; }
	inline int32_t* get_address_of_ChoixAnimation_15() { return &___ChoixAnimation_15; }
	inline void set_ChoixAnimation_15(int32_t value)
	{
		___ChoixAnimation_15 = value;
	}

	inline static int32_t get_offset_of_animationFinie_16() { return static_cast<int32_t>(offsetof(animationImage_t1423068421, ___animationFinie_16)); }
	inline bool get_animationFinie_16() const { return ___animationFinie_16; }
	inline bool* get_address_of_animationFinie_16() { return &___animationFinie_16; }
	inline void set_animationFinie_16(bool value)
	{
		___animationFinie_16 = value;
	}

	inline static int32_t get_offset_of_affichageFini_17() { return static_cast<int32_t>(offsetof(animationImage_t1423068421, ___affichageFini_17)); }
	inline bool get_affichageFini_17() const { return ___affichageFini_17; }
	inline bool* get_address_of_affichageFini_17() { return &___affichageFini_17; }
	inline void set_affichageFini_17(bool value)
	{
		___affichageFini_17 = value;
	}

	inline static int32_t get_offset_of_numPix_18() { return static_cast<int32_t>(offsetof(animationImage_t1423068421, ___numPix_18)); }
	inline int32_t get_numPix_18() const { return ___numPix_18; }
	inline int32_t* get_address_of_numPix_18() { return &___numPix_18; }
	inline void set_numPix_18(int32_t value)
	{
		___numPix_18 = value;
	}

	inline static int32_t get_offset_of_nbPix_19() { return static_cast<int32_t>(offsetof(animationImage_t1423068421, ___nbPix_19)); }
	inline int32_t get_nbPix_19() const { return ___nbPix_19; }
	inline int32_t* get_address_of_nbPix_19() { return &___nbPix_19; }
	inline void set_nbPix_19(int32_t value)
	{
		___nbPix_19 = value;
	}

	inline static int32_t get_offset_of_nbAfficher_20() { return static_cast<int32_t>(offsetof(animationImage_t1423068421, ___nbAfficher_20)); }
	inline int32_t get_nbAfficher_20() const { return ___nbAfficher_20; }
	inline int32_t* get_address_of_nbAfficher_20() { return &___nbAfficher_20; }
	inline void set_nbAfficher_20(int32_t value)
	{
		___nbAfficher_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATIONIMAGE_T1423068421_H
#ifndef BASESELECTOR_T1489849420_H
#define BASESELECTOR_T1489849420_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// baseSelector
struct  baseSelector_t1489849420  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.InputField baseSelector::inputField
	InputField_t3762917431 * ___inputField_4;

public:
	inline static int32_t get_offset_of_inputField_4() { return static_cast<int32_t>(offsetof(baseSelector_t1489849420, ___inputField_4)); }
	inline InputField_t3762917431 * get_inputField_4() const { return ___inputField_4; }
	inline InputField_t3762917431 ** get_address_of_inputField_4() { return &___inputField_4; }
	inline void set_inputField_4(InputField_t3762917431 * value)
	{
		___inputField_4 = value;
		Il2CppCodeGenWriteBarrier((&___inputField_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASESELECTOR_T1489849420_H
#ifndef CAPTUREANDSENDSCREEN_T3196082656_H
#define CAPTUREANDSENDSCREEN_T3196082656_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// captureAndSendScreen
struct  captureAndSendScreen_t3196082656  : public MonoBehaviour_t3962482529
{
public:
	// System.String captureAndSendScreen::screenShotURL
	String_t* ___screenShotURL_4;
	// System.String captureAndSendScreen::UDID_User
	String_t* ___UDID_User_5;
	// System.String captureAndSendScreen::INTERFACE
	String_t* ___INTERFACE_6;
	// UnityEngine.GameObject captureAndSendScreen::nodeTex
	GameObject_t1113636619 * ___nodeTex_7;

public:
	inline static int32_t get_offset_of_screenShotURL_4() { return static_cast<int32_t>(offsetof(captureAndSendScreen_t3196082656, ___screenShotURL_4)); }
	inline String_t* get_screenShotURL_4() const { return ___screenShotURL_4; }
	inline String_t** get_address_of_screenShotURL_4() { return &___screenShotURL_4; }
	inline void set_screenShotURL_4(String_t* value)
	{
		___screenShotURL_4 = value;
		Il2CppCodeGenWriteBarrier((&___screenShotURL_4), value);
	}

	inline static int32_t get_offset_of_UDID_User_5() { return static_cast<int32_t>(offsetof(captureAndSendScreen_t3196082656, ___UDID_User_5)); }
	inline String_t* get_UDID_User_5() const { return ___UDID_User_5; }
	inline String_t** get_address_of_UDID_User_5() { return &___UDID_User_5; }
	inline void set_UDID_User_5(String_t* value)
	{
		___UDID_User_5 = value;
		Il2CppCodeGenWriteBarrier((&___UDID_User_5), value);
	}

	inline static int32_t get_offset_of_INTERFACE_6() { return static_cast<int32_t>(offsetof(captureAndSendScreen_t3196082656, ___INTERFACE_6)); }
	inline String_t* get_INTERFACE_6() const { return ___INTERFACE_6; }
	inline String_t** get_address_of_INTERFACE_6() { return &___INTERFACE_6; }
	inline void set_INTERFACE_6(String_t* value)
	{
		___INTERFACE_6 = value;
		Il2CppCodeGenWriteBarrier((&___INTERFACE_6), value);
	}

	inline static int32_t get_offset_of_nodeTex_7() { return static_cast<int32_t>(offsetof(captureAndSendScreen_t3196082656, ___nodeTex_7)); }
	inline GameObject_t1113636619 * get_nodeTex_7() const { return ___nodeTex_7; }
	inline GameObject_t1113636619 ** get_address_of_nodeTex_7() { return &___nodeTex_7; }
	inline void set_nodeTex_7(GameObject_t1113636619 * value)
	{
		___nodeTex_7 = value;
		Il2CppCodeGenWriteBarrier((&___nodeTex_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAPTUREANDSENDSCREEN_T3196082656_H
#ifndef STARTUPLANG_T1517666504_H
#define STARTUPLANG_T1517666504_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// startUpLANG
struct  startUpLANG_t1517666504  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 startUpLANG::selectedLanguageID
	int32_t ___selectedLanguageID_4;

public:
	inline static int32_t get_offset_of_selectedLanguageID_4() { return static_cast<int32_t>(offsetof(startUpLANG_t1517666504, ___selectedLanguageID_4)); }
	inline int32_t get_selectedLanguageID_4() const { return ___selectedLanguageID_4; }
	inline int32_t* get_address_of_selectedLanguageID_4() { return &___selectedLanguageID_4; }
	inline void set_selectedLanguageID_4(int32_t value)
	{
		___selectedLanguageID_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STARTUPLANG_T1517666504_H
#ifndef TESTIMAGE_T2491645257_H
#define TESTIMAGE_T2491645257_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// testImage
struct  testImage_t2491645257  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject testImage::plane
	GameObject_t1113636619 * ___plane_4;
	// UnityEngine.UI.Image testImage::rawImage
	Image_t2670269651 * ___rawImage_5;
	// UnityEngine.UI.RawImage testImage::rawImageLandscape
	RawImage_t3182918964 * ___rawImageLandscape_6;

public:
	inline static int32_t get_offset_of_plane_4() { return static_cast<int32_t>(offsetof(testImage_t2491645257, ___plane_4)); }
	inline GameObject_t1113636619 * get_plane_4() const { return ___plane_4; }
	inline GameObject_t1113636619 ** get_address_of_plane_4() { return &___plane_4; }
	inline void set_plane_4(GameObject_t1113636619 * value)
	{
		___plane_4 = value;
		Il2CppCodeGenWriteBarrier((&___plane_4), value);
	}

	inline static int32_t get_offset_of_rawImage_5() { return static_cast<int32_t>(offsetof(testImage_t2491645257, ___rawImage_5)); }
	inline Image_t2670269651 * get_rawImage_5() const { return ___rawImage_5; }
	inline Image_t2670269651 ** get_address_of_rawImage_5() { return &___rawImage_5; }
	inline void set_rawImage_5(Image_t2670269651 * value)
	{
		___rawImage_5 = value;
		Il2CppCodeGenWriteBarrier((&___rawImage_5), value);
	}

	inline static int32_t get_offset_of_rawImageLandscape_6() { return static_cast<int32_t>(offsetof(testImage_t2491645257, ___rawImageLandscape_6)); }
	inline RawImage_t3182918964 * get_rawImageLandscape_6() const { return ___rawImageLandscape_6; }
	inline RawImage_t3182918964 ** get_address_of_rawImageLandscape_6() { return &___rawImageLandscape_6; }
	inline void set_rawImageLandscape_6(RawImage_t3182918964 * value)
	{
		___rawImageLandscape_6 = value;
		Il2CppCodeGenWriteBarrier((&___rawImageLandscape_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TESTIMAGE_T2491645257_H
#ifndef TESTPLAYER_T3643834628_H
#define TESTPLAYER_T3643834628_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// testPlayer
struct  testPlayer_t3643834628  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Video.VideoPlayer testPlayer::videoPlayer
	VideoPlayer_t1683042537 * ___videoPlayer_4;

public:
	inline static int32_t get_offset_of_videoPlayer_4() { return static_cast<int32_t>(offsetof(testPlayer_t3643834628, ___videoPlayer_4)); }
	inline VideoPlayer_t1683042537 * get_videoPlayer_4() const { return ___videoPlayer_4; }
	inline VideoPlayer_t1683042537 ** get_address_of_videoPlayer_4() { return &___videoPlayer_4; }
	inline void set_videoPlayer_4(VideoPlayer_t1683042537 * value)
	{
		___videoPlayer_4 = value;
		Il2CppCodeGenWriteBarrier((&___videoPlayer_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TESTPLAYER_T3643834628_H
#ifndef TESTRESCALE_T3860525213_H
#define TESTRESCALE_T3860525213_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// testRescale
struct  testRescale_t3860525213  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Bounds testRescale::combinedBounds
	Bounds_t2266837910  ___combinedBounds_4;

public:
	inline static int32_t get_offset_of_combinedBounds_4() { return static_cast<int32_t>(offsetof(testRescale_t3860525213, ___combinedBounds_4)); }
	inline Bounds_t2266837910  get_combinedBounds_4() const { return ___combinedBounds_4; }
	inline Bounds_t2266837910 * get_address_of_combinedBounds_4() { return &___combinedBounds_4; }
	inline void set_combinedBounds_4(Bounds_t2266837910  value)
	{
		___combinedBounds_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TESTRESCALE_T3860525213_H
#ifndef GRAPHIC_T1660335611_H
#define GRAPHIC_T1660335611_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Graphic
struct  Graphic_t1660335611  : public UIBehaviour_t3495933518
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::m_Material
	Material_t340375123 * ___m_Material_6;
	// UnityEngine.Color UnityEngine.UI.Graphic::m_Color
	Color_t2555686324  ___m_Color_7;
	// System.Boolean UnityEngine.UI.Graphic::m_RaycastTarget
	bool ___m_RaycastTarget_8;
	// UnityEngine.RectTransform UnityEngine.UI.Graphic::m_RectTransform
	RectTransform_t3704657025 * ___m_RectTransform_9;
	// UnityEngine.CanvasRenderer UnityEngine.UI.Graphic::m_CanvasRenderer
	CanvasRenderer_t2598313366 * ___m_CanvasRenderer_10;
	// UnityEngine.Canvas UnityEngine.UI.Graphic::m_Canvas
	Canvas_t3310196443 * ___m_Canvas_11;
	// System.Boolean UnityEngine.UI.Graphic::m_VertsDirty
	bool ___m_VertsDirty_12;
	// System.Boolean UnityEngine.UI.Graphic::m_MaterialDirty
	bool ___m_MaterialDirty_13;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyLayoutCallback
	UnityAction_t3245792599 * ___m_OnDirtyLayoutCallback_14;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyVertsCallback
	UnityAction_t3245792599 * ___m_OnDirtyVertsCallback_15;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyMaterialCallback
	UnityAction_t3245792599 * ___m_OnDirtyMaterialCallback_16;
	// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween> UnityEngine.UI.Graphic::m_ColorTweenRunner
	TweenRunner_1_t3055525458 * ___m_ColorTweenRunner_19;
	// System.Boolean UnityEngine.UI.Graphic::<useLegacyMeshGeneration>k__BackingField
	bool ___U3CuseLegacyMeshGenerationU3Ek__BackingField_20;

public:
	inline static int32_t get_offset_of_m_Material_6() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_Material_6)); }
	inline Material_t340375123 * get_m_Material_6() const { return ___m_Material_6; }
	inline Material_t340375123 ** get_address_of_m_Material_6() { return &___m_Material_6; }
	inline void set_m_Material_6(Material_t340375123 * value)
	{
		___m_Material_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_Material_6), value);
	}

	inline static int32_t get_offset_of_m_Color_7() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_Color_7)); }
	inline Color_t2555686324  get_m_Color_7() const { return ___m_Color_7; }
	inline Color_t2555686324 * get_address_of_m_Color_7() { return &___m_Color_7; }
	inline void set_m_Color_7(Color_t2555686324  value)
	{
		___m_Color_7 = value;
	}

	inline static int32_t get_offset_of_m_RaycastTarget_8() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_RaycastTarget_8)); }
	inline bool get_m_RaycastTarget_8() const { return ___m_RaycastTarget_8; }
	inline bool* get_address_of_m_RaycastTarget_8() { return &___m_RaycastTarget_8; }
	inline void set_m_RaycastTarget_8(bool value)
	{
		___m_RaycastTarget_8 = value;
	}

	inline static int32_t get_offset_of_m_RectTransform_9() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_RectTransform_9)); }
	inline RectTransform_t3704657025 * get_m_RectTransform_9() const { return ___m_RectTransform_9; }
	inline RectTransform_t3704657025 ** get_address_of_m_RectTransform_9() { return &___m_RectTransform_9; }
	inline void set_m_RectTransform_9(RectTransform_t3704657025 * value)
	{
		___m_RectTransform_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_RectTransform_9), value);
	}

	inline static int32_t get_offset_of_m_CanvasRenderer_10() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_CanvasRenderer_10)); }
	inline CanvasRenderer_t2598313366 * get_m_CanvasRenderer_10() const { return ___m_CanvasRenderer_10; }
	inline CanvasRenderer_t2598313366 ** get_address_of_m_CanvasRenderer_10() { return &___m_CanvasRenderer_10; }
	inline void set_m_CanvasRenderer_10(CanvasRenderer_t2598313366 * value)
	{
		___m_CanvasRenderer_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_CanvasRenderer_10), value);
	}

	inline static int32_t get_offset_of_m_Canvas_11() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_Canvas_11)); }
	inline Canvas_t3310196443 * get_m_Canvas_11() const { return ___m_Canvas_11; }
	inline Canvas_t3310196443 ** get_address_of_m_Canvas_11() { return &___m_Canvas_11; }
	inline void set_m_Canvas_11(Canvas_t3310196443 * value)
	{
		___m_Canvas_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_Canvas_11), value);
	}

	inline static int32_t get_offset_of_m_VertsDirty_12() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_VertsDirty_12)); }
	inline bool get_m_VertsDirty_12() const { return ___m_VertsDirty_12; }
	inline bool* get_address_of_m_VertsDirty_12() { return &___m_VertsDirty_12; }
	inline void set_m_VertsDirty_12(bool value)
	{
		___m_VertsDirty_12 = value;
	}

	inline static int32_t get_offset_of_m_MaterialDirty_13() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_MaterialDirty_13)); }
	inline bool get_m_MaterialDirty_13() const { return ___m_MaterialDirty_13; }
	inline bool* get_address_of_m_MaterialDirty_13() { return &___m_MaterialDirty_13; }
	inline void set_m_MaterialDirty_13(bool value)
	{
		___m_MaterialDirty_13 = value;
	}

	inline static int32_t get_offset_of_m_OnDirtyLayoutCallback_14() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_OnDirtyLayoutCallback_14)); }
	inline UnityAction_t3245792599 * get_m_OnDirtyLayoutCallback_14() const { return ___m_OnDirtyLayoutCallback_14; }
	inline UnityAction_t3245792599 ** get_address_of_m_OnDirtyLayoutCallback_14() { return &___m_OnDirtyLayoutCallback_14; }
	inline void set_m_OnDirtyLayoutCallback_14(UnityAction_t3245792599 * value)
	{
		___m_OnDirtyLayoutCallback_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyLayoutCallback_14), value);
	}

	inline static int32_t get_offset_of_m_OnDirtyVertsCallback_15() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_OnDirtyVertsCallback_15)); }
	inline UnityAction_t3245792599 * get_m_OnDirtyVertsCallback_15() const { return ___m_OnDirtyVertsCallback_15; }
	inline UnityAction_t3245792599 ** get_address_of_m_OnDirtyVertsCallback_15() { return &___m_OnDirtyVertsCallback_15; }
	inline void set_m_OnDirtyVertsCallback_15(UnityAction_t3245792599 * value)
	{
		___m_OnDirtyVertsCallback_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyVertsCallback_15), value);
	}

	inline static int32_t get_offset_of_m_OnDirtyMaterialCallback_16() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_OnDirtyMaterialCallback_16)); }
	inline UnityAction_t3245792599 * get_m_OnDirtyMaterialCallback_16() const { return ___m_OnDirtyMaterialCallback_16; }
	inline UnityAction_t3245792599 ** get_address_of_m_OnDirtyMaterialCallback_16() { return &___m_OnDirtyMaterialCallback_16; }
	inline void set_m_OnDirtyMaterialCallback_16(UnityAction_t3245792599 * value)
	{
		___m_OnDirtyMaterialCallback_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyMaterialCallback_16), value);
	}

	inline static int32_t get_offset_of_m_ColorTweenRunner_19() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_ColorTweenRunner_19)); }
	inline TweenRunner_1_t3055525458 * get_m_ColorTweenRunner_19() const { return ___m_ColorTweenRunner_19; }
	inline TweenRunner_1_t3055525458 ** get_address_of_m_ColorTweenRunner_19() { return &___m_ColorTweenRunner_19; }
	inline void set_m_ColorTweenRunner_19(TweenRunner_1_t3055525458 * value)
	{
		___m_ColorTweenRunner_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_ColorTweenRunner_19), value);
	}

	inline static int32_t get_offset_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_20() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___U3CuseLegacyMeshGenerationU3Ek__BackingField_20)); }
	inline bool get_U3CuseLegacyMeshGenerationU3Ek__BackingField_20() const { return ___U3CuseLegacyMeshGenerationU3Ek__BackingField_20; }
	inline bool* get_address_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_20() { return &___U3CuseLegacyMeshGenerationU3Ek__BackingField_20; }
	inline void set_U3CuseLegacyMeshGenerationU3Ek__BackingField_20(bool value)
	{
		___U3CuseLegacyMeshGenerationU3Ek__BackingField_20 = value;
	}
};

struct Graphic_t1660335611_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::s_DefaultUI
	Material_t340375123 * ___s_DefaultUI_4;
	// UnityEngine.Texture2D UnityEngine.UI.Graphic::s_WhiteTexture
	Texture2D_t3840446185 * ___s_WhiteTexture_5;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::s_Mesh
	Mesh_t3648964284 * ___s_Mesh_17;
	// UnityEngine.UI.VertexHelper UnityEngine.UI.Graphic::s_VertexHelper
	VertexHelper_t2453304189 * ___s_VertexHelper_18;

public:
	inline static int32_t get_offset_of_s_DefaultUI_4() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_DefaultUI_4)); }
	inline Material_t340375123 * get_s_DefaultUI_4() const { return ___s_DefaultUI_4; }
	inline Material_t340375123 ** get_address_of_s_DefaultUI_4() { return &___s_DefaultUI_4; }
	inline void set_s_DefaultUI_4(Material_t340375123 * value)
	{
		___s_DefaultUI_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_DefaultUI_4), value);
	}

	inline static int32_t get_offset_of_s_WhiteTexture_5() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_WhiteTexture_5)); }
	inline Texture2D_t3840446185 * get_s_WhiteTexture_5() const { return ___s_WhiteTexture_5; }
	inline Texture2D_t3840446185 ** get_address_of_s_WhiteTexture_5() { return &___s_WhiteTexture_5; }
	inline void set_s_WhiteTexture_5(Texture2D_t3840446185 * value)
	{
		___s_WhiteTexture_5 = value;
		Il2CppCodeGenWriteBarrier((&___s_WhiteTexture_5), value);
	}

	inline static int32_t get_offset_of_s_Mesh_17() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_Mesh_17)); }
	inline Mesh_t3648964284 * get_s_Mesh_17() const { return ___s_Mesh_17; }
	inline Mesh_t3648964284 ** get_address_of_s_Mesh_17() { return &___s_Mesh_17; }
	inline void set_s_Mesh_17(Mesh_t3648964284 * value)
	{
		___s_Mesh_17 = value;
		Il2CppCodeGenWriteBarrier((&___s_Mesh_17), value);
	}

	inline static int32_t get_offset_of_s_VertexHelper_18() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_VertexHelper_18)); }
	inline VertexHelper_t2453304189 * get_s_VertexHelper_18() const { return ___s_VertexHelper_18; }
	inline VertexHelper_t2453304189 ** get_address_of_s_VertexHelper_18() { return &___s_VertexHelper_18; }
	inline void set_s_VertexHelper_18(VertexHelper_t2453304189 * value)
	{
		___s_VertexHelper_18 = value;
		Il2CppCodeGenWriteBarrier((&___s_VertexHelper_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRAPHIC_T1660335611_H
#ifndef SELECTABLE_T3250028441_H
#define SELECTABLE_T3250028441_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Selectable
struct  Selectable_t3250028441  : public UIBehaviour_t3495933518
{
public:
	// UnityEngine.UI.Navigation UnityEngine.UI.Selectable::m_Navigation
	Navigation_t3049316579  ___m_Navigation_5;
	// UnityEngine.UI.Selectable/Transition UnityEngine.UI.Selectable::m_Transition
	int32_t ___m_Transition_6;
	// UnityEngine.UI.ColorBlock UnityEngine.UI.Selectable::m_Colors
	ColorBlock_t2139031574  ___m_Colors_7;
	// UnityEngine.UI.SpriteState UnityEngine.UI.Selectable::m_SpriteState
	SpriteState_t1362986479  ___m_SpriteState_8;
	// UnityEngine.UI.AnimationTriggers UnityEngine.UI.Selectable::m_AnimationTriggers
	AnimationTriggers_t2532145056 * ___m_AnimationTriggers_9;
	// System.Boolean UnityEngine.UI.Selectable::m_Interactable
	bool ___m_Interactable_10;
	// UnityEngine.UI.Graphic UnityEngine.UI.Selectable::m_TargetGraphic
	Graphic_t1660335611 * ___m_TargetGraphic_11;
	// System.Boolean UnityEngine.UI.Selectable::m_GroupsAllowInteraction
	bool ___m_GroupsAllowInteraction_12;
	// UnityEngine.UI.Selectable/SelectionState UnityEngine.UI.Selectable::m_CurrentSelectionState
	int32_t ___m_CurrentSelectionState_13;
	// System.Boolean UnityEngine.UI.Selectable::<isPointerInside>k__BackingField
	bool ___U3CisPointerInsideU3Ek__BackingField_14;
	// System.Boolean UnityEngine.UI.Selectable::<isPointerDown>k__BackingField
	bool ___U3CisPointerDownU3Ek__BackingField_15;
	// System.Boolean UnityEngine.UI.Selectable::<hasSelection>k__BackingField
	bool ___U3ChasSelectionU3Ek__BackingField_16;
	// System.Collections.Generic.List`1<UnityEngine.CanvasGroup> UnityEngine.UI.Selectable::m_CanvasGroupCache
	List_1_t1260619206 * ___m_CanvasGroupCache_17;

public:
	inline static int32_t get_offset_of_m_Navigation_5() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_Navigation_5)); }
	inline Navigation_t3049316579  get_m_Navigation_5() const { return ___m_Navigation_5; }
	inline Navigation_t3049316579 * get_address_of_m_Navigation_5() { return &___m_Navigation_5; }
	inline void set_m_Navigation_5(Navigation_t3049316579  value)
	{
		___m_Navigation_5 = value;
	}

	inline static int32_t get_offset_of_m_Transition_6() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_Transition_6)); }
	inline int32_t get_m_Transition_6() const { return ___m_Transition_6; }
	inline int32_t* get_address_of_m_Transition_6() { return &___m_Transition_6; }
	inline void set_m_Transition_6(int32_t value)
	{
		___m_Transition_6 = value;
	}

	inline static int32_t get_offset_of_m_Colors_7() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_Colors_7)); }
	inline ColorBlock_t2139031574  get_m_Colors_7() const { return ___m_Colors_7; }
	inline ColorBlock_t2139031574 * get_address_of_m_Colors_7() { return &___m_Colors_7; }
	inline void set_m_Colors_7(ColorBlock_t2139031574  value)
	{
		___m_Colors_7 = value;
	}

	inline static int32_t get_offset_of_m_SpriteState_8() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_SpriteState_8)); }
	inline SpriteState_t1362986479  get_m_SpriteState_8() const { return ___m_SpriteState_8; }
	inline SpriteState_t1362986479 * get_address_of_m_SpriteState_8() { return &___m_SpriteState_8; }
	inline void set_m_SpriteState_8(SpriteState_t1362986479  value)
	{
		___m_SpriteState_8 = value;
	}

	inline static int32_t get_offset_of_m_AnimationTriggers_9() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_AnimationTriggers_9)); }
	inline AnimationTriggers_t2532145056 * get_m_AnimationTriggers_9() const { return ___m_AnimationTriggers_9; }
	inline AnimationTriggers_t2532145056 ** get_address_of_m_AnimationTriggers_9() { return &___m_AnimationTriggers_9; }
	inline void set_m_AnimationTriggers_9(AnimationTriggers_t2532145056 * value)
	{
		___m_AnimationTriggers_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_AnimationTriggers_9), value);
	}

	inline static int32_t get_offset_of_m_Interactable_10() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_Interactable_10)); }
	inline bool get_m_Interactable_10() const { return ___m_Interactable_10; }
	inline bool* get_address_of_m_Interactable_10() { return &___m_Interactable_10; }
	inline void set_m_Interactable_10(bool value)
	{
		___m_Interactable_10 = value;
	}

	inline static int32_t get_offset_of_m_TargetGraphic_11() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_TargetGraphic_11)); }
	inline Graphic_t1660335611 * get_m_TargetGraphic_11() const { return ___m_TargetGraphic_11; }
	inline Graphic_t1660335611 ** get_address_of_m_TargetGraphic_11() { return &___m_TargetGraphic_11; }
	inline void set_m_TargetGraphic_11(Graphic_t1660335611 * value)
	{
		___m_TargetGraphic_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_TargetGraphic_11), value);
	}

	inline static int32_t get_offset_of_m_GroupsAllowInteraction_12() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_GroupsAllowInteraction_12)); }
	inline bool get_m_GroupsAllowInteraction_12() const { return ___m_GroupsAllowInteraction_12; }
	inline bool* get_address_of_m_GroupsAllowInteraction_12() { return &___m_GroupsAllowInteraction_12; }
	inline void set_m_GroupsAllowInteraction_12(bool value)
	{
		___m_GroupsAllowInteraction_12 = value;
	}

	inline static int32_t get_offset_of_m_CurrentSelectionState_13() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_CurrentSelectionState_13)); }
	inline int32_t get_m_CurrentSelectionState_13() const { return ___m_CurrentSelectionState_13; }
	inline int32_t* get_address_of_m_CurrentSelectionState_13() { return &___m_CurrentSelectionState_13; }
	inline void set_m_CurrentSelectionState_13(int32_t value)
	{
		___m_CurrentSelectionState_13 = value;
	}

	inline static int32_t get_offset_of_U3CisPointerInsideU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___U3CisPointerInsideU3Ek__BackingField_14)); }
	inline bool get_U3CisPointerInsideU3Ek__BackingField_14() const { return ___U3CisPointerInsideU3Ek__BackingField_14; }
	inline bool* get_address_of_U3CisPointerInsideU3Ek__BackingField_14() { return &___U3CisPointerInsideU3Ek__BackingField_14; }
	inline void set_U3CisPointerInsideU3Ek__BackingField_14(bool value)
	{
		___U3CisPointerInsideU3Ek__BackingField_14 = value;
	}

	inline static int32_t get_offset_of_U3CisPointerDownU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___U3CisPointerDownU3Ek__BackingField_15)); }
	inline bool get_U3CisPointerDownU3Ek__BackingField_15() const { return ___U3CisPointerDownU3Ek__BackingField_15; }
	inline bool* get_address_of_U3CisPointerDownU3Ek__BackingField_15() { return &___U3CisPointerDownU3Ek__BackingField_15; }
	inline void set_U3CisPointerDownU3Ek__BackingField_15(bool value)
	{
		___U3CisPointerDownU3Ek__BackingField_15 = value;
	}

	inline static int32_t get_offset_of_U3ChasSelectionU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___U3ChasSelectionU3Ek__BackingField_16)); }
	inline bool get_U3ChasSelectionU3Ek__BackingField_16() const { return ___U3ChasSelectionU3Ek__BackingField_16; }
	inline bool* get_address_of_U3ChasSelectionU3Ek__BackingField_16() { return &___U3ChasSelectionU3Ek__BackingField_16; }
	inline void set_U3ChasSelectionU3Ek__BackingField_16(bool value)
	{
		___U3ChasSelectionU3Ek__BackingField_16 = value;
	}

	inline static int32_t get_offset_of_m_CanvasGroupCache_17() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_CanvasGroupCache_17)); }
	inline List_1_t1260619206 * get_m_CanvasGroupCache_17() const { return ___m_CanvasGroupCache_17; }
	inline List_1_t1260619206 ** get_address_of_m_CanvasGroupCache_17() { return &___m_CanvasGroupCache_17; }
	inline void set_m_CanvasGroupCache_17(List_1_t1260619206 * value)
	{
		___m_CanvasGroupCache_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_CanvasGroupCache_17), value);
	}
};

struct Selectable_t3250028441_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.UI.Selectable> UnityEngine.UI.Selectable::s_List
	List_1_t427135887 * ___s_List_4;

public:
	inline static int32_t get_offset_of_s_List_4() { return static_cast<int32_t>(offsetof(Selectable_t3250028441_StaticFields, ___s_List_4)); }
	inline List_1_t427135887 * get_s_List_4() const { return ___s_List_4; }
	inline List_1_t427135887 ** get_address_of_s_List_4() { return &___s_List_4; }
	inline void set_s_List_4(List_1_t427135887 * value)
	{
		___s_List_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_List_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTABLE_T3250028441_H
#ifndef INPUTFIELD_T3762917431_H
#define INPUTFIELD_T3762917431_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.InputField
struct  InputField_t3762917431  : public Selectable_t3250028441
{
public:
	// UnityEngine.TouchScreenKeyboard UnityEngine.UI.InputField::m_Keyboard
	TouchScreenKeyboard_t731888065 * ___m_Keyboard_18;
	// UnityEngine.UI.Text UnityEngine.UI.InputField::m_TextComponent
	Text_t1901882714 * ___m_TextComponent_20;
	// UnityEngine.UI.Graphic UnityEngine.UI.InputField::m_Placeholder
	Graphic_t1660335611 * ___m_Placeholder_21;
	// UnityEngine.UI.InputField/ContentType UnityEngine.UI.InputField::m_ContentType
	int32_t ___m_ContentType_22;
	// UnityEngine.UI.InputField/InputType UnityEngine.UI.InputField::m_InputType
	int32_t ___m_InputType_23;
	// System.Char UnityEngine.UI.InputField::m_AsteriskChar
	Il2CppChar ___m_AsteriskChar_24;
	// UnityEngine.TouchScreenKeyboardType UnityEngine.UI.InputField::m_KeyboardType
	int32_t ___m_KeyboardType_25;
	// UnityEngine.UI.InputField/LineType UnityEngine.UI.InputField::m_LineType
	int32_t ___m_LineType_26;
	// System.Boolean UnityEngine.UI.InputField::m_HideMobileInput
	bool ___m_HideMobileInput_27;
	// UnityEngine.UI.InputField/CharacterValidation UnityEngine.UI.InputField::m_CharacterValidation
	int32_t ___m_CharacterValidation_28;
	// System.Int32 UnityEngine.UI.InputField::m_CharacterLimit
	int32_t ___m_CharacterLimit_29;
	// UnityEngine.UI.InputField/SubmitEvent UnityEngine.UI.InputField::m_OnEndEdit
	SubmitEvent_t648412432 * ___m_OnEndEdit_30;
	// UnityEngine.UI.InputField/OnChangeEvent UnityEngine.UI.InputField::m_OnValueChanged
	OnChangeEvent_t467195904 * ___m_OnValueChanged_31;
	// UnityEngine.UI.InputField/OnValidateInput UnityEngine.UI.InputField::m_OnValidateInput
	OnValidateInput_t2355412304 * ___m_OnValidateInput_32;
	// UnityEngine.Color UnityEngine.UI.InputField::m_CaretColor
	Color_t2555686324  ___m_CaretColor_33;
	// System.Boolean UnityEngine.UI.InputField::m_CustomCaretColor
	bool ___m_CustomCaretColor_34;
	// UnityEngine.Color UnityEngine.UI.InputField::m_SelectionColor
	Color_t2555686324  ___m_SelectionColor_35;
	// System.String UnityEngine.UI.InputField::m_Text
	String_t* ___m_Text_36;
	// System.Single UnityEngine.UI.InputField::m_CaretBlinkRate
	float ___m_CaretBlinkRate_37;
	// System.Int32 UnityEngine.UI.InputField::m_CaretWidth
	int32_t ___m_CaretWidth_38;
	// System.Boolean UnityEngine.UI.InputField::m_ReadOnly
	bool ___m_ReadOnly_39;
	// System.Int32 UnityEngine.UI.InputField::m_CaretPosition
	int32_t ___m_CaretPosition_40;
	// System.Int32 UnityEngine.UI.InputField::m_CaretSelectPosition
	int32_t ___m_CaretSelectPosition_41;
	// UnityEngine.RectTransform UnityEngine.UI.InputField::caretRectTrans
	RectTransform_t3704657025 * ___caretRectTrans_42;
	// UnityEngine.UIVertex[] UnityEngine.UI.InputField::m_CursorVerts
	UIVertexU5BU5D_t1981460040* ___m_CursorVerts_43;
	// UnityEngine.TextGenerator UnityEngine.UI.InputField::m_InputTextCache
	TextGenerator_t3211863866 * ___m_InputTextCache_44;
	// UnityEngine.CanvasRenderer UnityEngine.UI.InputField::m_CachedInputRenderer
	CanvasRenderer_t2598313366 * ___m_CachedInputRenderer_45;
	// System.Boolean UnityEngine.UI.InputField::m_PreventFontCallback
	bool ___m_PreventFontCallback_46;
	// UnityEngine.Mesh UnityEngine.UI.InputField::m_Mesh
	Mesh_t3648964284 * ___m_Mesh_47;
	// System.Boolean UnityEngine.UI.InputField::m_AllowInput
	bool ___m_AllowInput_48;
	// System.Boolean UnityEngine.UI.InputField::m_ShouldActivateNextUpdate
	bool ___m_ShouldActivateNextUpdate_49;
	// System.Boolean UnityEngine.UI.InputField::m_UpdateDrag
	bool ___m_UpdateDrag_50;
	// System.Boolean UnityEngine.UI.InputField::m_DragPositionOutOfBounds
	bool ___m_DragPositionOutOfBounds_51;
	// System.Boolean UnityEngine.UI.InputField::m_CaretVisible
	bool ___m_CaretVisible_54;
	// UnityEngine.Coroutine UnityEngine.UI.InputField::m_BlinkCoroutine
	Coroutine_t3829159415 * ___m_BlinkCoroutine_55;
	// System.Single UnityEngine.UI.InputField::m_BlinkStartTime
	float ___m_BlinkStartTime_56;
	// System.Int32 UnityEngine.UI.InputField::m_DrawStart
	int32_t ___m_DrawStart_57;
	// System.Int32 UnityEngine.UI.InputField::m_DrawEnd
	int32_t ___m_DrawEnd_58;
	// UnityEngine.Coroutine UnityEngine.UI.InputField::m_DragCoroutine
	Coroutine_t3829159415 * ___m_DragCoroutine_59;
	// System.String UnityEngine.UI.InputField::m_OriginalText
	String_t* ___m_OriginalText_60;
	// System.Boolean UnityEngine.UI.InputField::m_WasCanceled
	bool ___m_WasCanceled_61;
	// System.Boolean UnityEngine.UI.InputField::m_HasDoneFocusTransition
	bool ___m_HasDoneFocusTransition_62;
	// UnityEngine.Event UnityEngine.UI.InputField::m_ProcessingEvent
	Event_t2956885303 * ___m_ProcessingEvent_64;

public:
	inline static int32_t get_offset_of_m_Keyboard_18() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_Keyboard_18)); }
	inline TouchScreenKeyboard_t731888065 * get_m_Keyboard_18() const { return ___m_Keyboard_18; }
	inline TouchScreenKeyboard_t731888065 ** get_address_of_m_Keyboard_18() { return &___m_Keyboard_18; }
	inline void set_m_Keyboard_18(TouchScreenKeyboard_t731888065 * value)
	{
		___m_Keyboard_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_Keyboard_18), value);
	}

	inline static int32_t get_offset_of_m_TextComponent_20() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_TextComponent_20)); }
	inline Text_t1901882714 * get_m_TextComponent_20() const { return ___m_TextComponent_20; }
	inline Text_t1901882714 ** get_address_of_m_TextComponent_20() { return &___m_TextComponent_20; }
	inline void set_m_TextComponent_20(Text_t1901882714 * value)
	{
		___m_TextComponent_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextComponent_20), value);
	}

	inline static int32_t get_offset_of_m_Placeholder_21() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_Placeholder_21)); }
	inline Graphic_t1660335611 * get_m_Placeholder_21() const { return ___m_Placeholder_21; }
	inline Graphic_t1660335611 ** get_address_of_m_Placeholder_21() { return &___m_Placeholder_21; }
	inline void set_m_Placeholder_21(Graphic_t1660335611 * value)
	{
		___m_Placeholder_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_Placeholder_21), value);
	}

	inline static int32_t get_offset_of_m_ContentType_22() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_ContentType_22)); }
	inline int32_t get_m_ContentType_22() const { return ___m_ContentType_22; }
	inline int32_t* get_address_of_m_ContentType_22() { return &___m_ContentType_22; }
	inline void set_m_ContentType_22(int32_t value)
	{
		___m_ContentType_22 = value;
	}

	inline static int32_t get_offset_of_m_InputType_23() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_InputType_23)); }
	inline int32_t get_m_InputType_23() const { return ___m_InputType_23; }
	inline int32_t* get_address_of_m_InputType_23() { return &___m_InputType_23; }
	inline void set_m_InputType_23(int32_t value)
	{
		___m_InputType_23 = value;
	}

	inline static int32_t get_offset_of_m_AsteriskChar_24() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_AsteriskChar_24)); }
	inline Il2CppChar get_m_AsteriskChar_24() const { return ___m_AsteriskChar_24; }
	inline Il2CppChar* get_address_of_m_AsteriskChar_24() { return &___m_AsteriskChar_24; }
	inline void set_m_AsteriskChar_24(Il2CppChar value)
	{
		___m_AsteriskChar_24 = value;
	}

	inline static int32_t get_offset_of_m_KeyboardType_25() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_KeyboardType_25)); }
	inline int32_t get_m_KeyboardType_25() const { return ___m_KeyboardType_25; }
	inline int32_t* get_address_of_m_KeyboardType_25() { return &___m_KeyboardType_25; }
	inline void set_m_KeyboardType_25(int32_t value)
	{
		___m_KeyboardType_25 = value;
	}

	inline static int32_t get_offset_of_m_LineType_26() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_LineType_26)); }
	inline int32_t get_m_LineType_26() const { return ___m_LineType_26; }
	inline int32_t* get_address_of_m_LineType_26() { return &___m_LineType_26; }
	inline void set_m_LineType_26(int32_t value)
	{
		___m_LineType_26 = value;
	}

	inline static int32_t get_offset_of_m_HideMobileInput_27() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_HideMobileInput_27)); }
	inline bool get_m_HideMobileInput_27() const { return ___m_HideMobileInput_27; }
	inline bool* get_address_of_m_HideMobileInput_27() { return &___m_HideMobileInput_27; }
	inline void set_m_HideMobileInput_27(bool value)
	{
		___m_HideMobileInput_27 = value;
	}

	inline static int32_t get_offset_of_m_CharacterValidation_28() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_CharacterValidation_28)); }
	inline int32_t get_m_CharacterValidation_28() const { return ___m_CharacterValidation_28; }
	inline int32_t* get_address_of_m_CharacterValidation_28() { return &___m_CharacterValidation_28; }
	inline void set_m_CharacterValidation_28(int32_t value)
	{
		___m_CharacterValidation_28 = value;
	}

	inline static int32_t get_offset_of_m_CharacterLimit_29() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_CharacterLimit_29)); }
	inline int32_t get_m_CharacterLimit_29() const { return ___m_CharacterLimit_29; }
	inline int32_t* get_address_of_m_CharacterLimit_29() { return &___m_CharacterLimit_29; }
	inline void set_m_CharacterLimit_29(int32_t value)
	{
		___m_CharacterLimit_29 = value;
	}

	inline static int32_t get_offset_of_m_OnEndEdit_30() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_OnEndEdit_30)); }
	inline SubmitEvent_t648412432 * get_m_OnEndEdit_30() const { return ___m_OnEndEdit_30; }
	inline SubmitEvent_t648412432 ** get_address_of_m_OnEndEdit_30() { return &___m_OnEndEdit_30; }
	inline void set_m_OnEndEdit_30(SubmitEvent_t648412432 * value)
	{
		___m_OnEndEdit_30 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnEndEdit_30), value);
	}

	inline static int32_t get_offset_of_m_OnValueChanged_31() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_OnValueChanged_31)); }
	inline OnChangeEvent_t467195904 * get_m_OnValueChanged_31() const { return ___m_OnValueChanged_31; }
	inline OnChangeEvent_t467195904 ** get_address_of_m_OnValueChanged_31() { return &___m_OnValueChanged_31; }
	inline void set_m_OnValueChanged_31(OnChangeEvent_t467195904 * value)
	{
		___m_OnValueChanged_31 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnValueChanged_31), value);
	}

	inline static int32_t get_offset_of_m_OnValidateInput_32() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_OnValidateInput_32)); }
	inline OnValidateInput_t2355412304 * get_m_OnValidateInput_32() const { return ___m_OnValidateInput_32; }
	inline OnValidateInput_t2355412304 ** get_address_of_m_OnValidateInput_32() { return &___m_OnValidateInput_32; }
	inline void set_m_OnValidateInput_32(OnValidateInput_t2355412304 * value)
	{
		___m_OnValidateInput_32 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnValidateInput_32), value);
	}

	inline static int32_t get_offset_of_m_CaretColor_33() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_CaretColor_33)); }
	inline Color_t2555686324  get_m_CaretColor_33() const { return ___m_CaretColor_33; }
	inline Color_t2555686324 * get_address_of_m_CaretColor_33() { return &___m_CaretColor_33; }
	inline void set_m_CaretColor_33(Color_t2555686324  value)
	{
		___m_CaretColor_33 = value;
	}

	inline static int32_t get_offset_of_m_CustomCaretColor_34() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_CustomCaretColor_34)); }
	inline bool get_m_CustomCaretColor_34() const { return ___m_CustomCaretColor_34; }
	inline bool* get_address_of_m_CustomCaretColor_34() { return &___m_CustomCaretColor_34; }
	inline void set_m_CustomCaretColor_34(bool value)
	{
		___m_CustomCaretColor_34 = value;
	}

	inline static int32_t get_offset_of_m_SelectionColor_35() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_SelectionColor_35)); }
	inline Color_t2555686324  get_m_SelectionColor_35() const { return ___m_SelectionColor_35; }
	inline Color_t2555686324 * get_address_of_m_SelectionColor_35() { return &___m_SelectionColor_35; }
	inline void set_m_SelectionColor_35(Color_t2555686324  value)
	{
		___m_SelectionColor_35 = value;
	}

	inline static int32_t get_offset_of_m_Text_36() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_Text_36)); }
	inline String_t* get_m_Text_36() const { return ___m_Text_36; }
	inline String_t** get_address_of_m_Text_36() { return &___m_Text_36; }
	inline void set_m_Text_36(String_t* value)
	{
		___m_Text_36 = value;
		Il2CppCodeGenWriteBarrier((&___m_Text_36), value);
	}

	inline static int32_t get_offset_of_m_CaretBlinkRate_37() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_CaretBlinkRate_37)); }
	inline float get_m_CaretBlinkRate_37() const { return ___m_CaretBlinkRate_37; }
	inline float* get_address_of_m_CaretBlinkRate_37() { return &___m_CaretBlinkRate_37; }
	inline void set_m_CaretBlinkRate_37(float value)
	{
		___m_CaretBlinkRate_37 = value;
	}

	inline static int32_t get_offset_of_m_CaretWidth_38() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_CaretWidth_38)); }
	inline int32_t get_m_CaretWidth_38() const { return ___m_CaretWidth_38; }
	inline int32_t* get_address_of_m_CaretWidth_38() { return &___m_CaretWidth_38; }
	inline void set_m_CaretWidth_38(int32_t value)
	{
		___m_CaretWidth_38 = value;
	}

	inline static int32_t get_offset_of_m_ReadOnly_39() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_ReadOnly_39)); }
	inline bool get_m_ReadOnly_39() const { return ___m_ReadOnly_39; }
	inline bool* get_address_of_m_ReadOnly_39() { return &___m_ReadOnly_39; }
	inline void set_m_ReadOnly_39(bool value)
	{
		___m_ReadOnly_39 = value;
	}

	inline static int32_t get_offset_of_m_CaretPosition_40() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_CaretPosition_40)); }
	inline int32_t get_m_CaretPosition_40() const { return ___m_CaretPosition_40; }
	inline int32_t* get_address_of_m_CaretPosition_40() { return &___m_CaretPosition_40; }
	inline void set_m_CaretPosition_40(int32_t value)
	{
		___m_CaretPosition_40 = value;
	}

	inline static int32_t get_offset_of_m_CaretSelectPosition_41() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_CaretSelectPosition_41)); }
	inline int32_t get_m_CaretSelectPosition_41() const { return ___m_CaretSelectPosition_41; }
	inline int32_t* get_address_of_m_CaretSelectPosition_41() { return &___m_CaretSelectPosition_41; }
	inline void set_m_CaretSelectPosition_41(int32_t value)
	{
		___m_CaretSelectPosition_41 = value;
	}

	inline static int32_t get_offset_of_caretRectTrans_42() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___caretRectTrans_42)); }
	inline RectTransform_t3704657025 * get_caretRectTrans_42() const { return ___caretRectTrans_42; }
	inline RectTransform_t3704657025 ** get_address_of_caretRectTrans_42() { return &___caretRectTrans_42; }
	inline void set_caretRectTrans_42(RectTransform_t3704657025 * value)
	{
		___caretRectTrans_42 = value;
		Il2CppCodeGenWriteBarrier((&___caretRectTrans_42), value);
	}

	inline static int32_t get_offset_of_m_CursorVerts_43() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_CursorVerts_43)); }
	inline UIVertexU5BU5D_t1981460040* get_m_CursorVerts_43() const { return ___m_CursorVerts_43; }
	inline UIVertexU5BU5D_t1981460040** get_address_of_m_CursorVerts_43() { return &___m_CursorVerts_43; }
	inline void set_m_CursorVerts_43(UIVertexU5BU5D_t1981460040* value)
	{
		___m_CursorVerts_43 = value;
		Il2CppCodeGenWriteBarrier((&___m_CursorVerts_43), value);
	}

	inline static int32_t get_offset_of_m_InputTextCache_44() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_InputTextCache_44)); }
	inline TextGenerator_t3211863866 * get_m_InputTextCache_44() const { return ___m_InputTextCache_44; }
	inline TextGenerator_t3211863866 ** get_address_of_m_InputTextCache_44() { return &___m_InputTextCache_44; }
	inline void set_m_InputTextCache_44(TextGenerator_t3211863866 * value)
	{
		___m_InputTextCache_44 = value;
		Il2CppCodeGenWriteBarrier((&___m_InputTextCache_44), value);
	}

	inline static int32_t get_offset_of_m_CachedInputRenderer_45() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_CachedInputRenderer_45)); }
	inline CanvasRenderer_t2598313366 * get_m_CachedInputRenderer_45() const { return ___m_CachedInputRenderer_45; }
	inline CanvasRenderer_t2598313366 ** get_address_of_m_CachedInputRenderer_45() { return &___m_CachedInputRenderer_45; }
	inline void set_m_CachedInputRenderer_45(CanvasRenderer_t2598313366 * value)
	{
		___m_CachedInputRenderer_45 = value;
		Il2CppCodeGenWriteBarrier((&___m_CachedInputRenderer_45), value);
	}

	inline static int32_t get_offset_of_m_PreventFontCallback_46() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_PreventFontCallback_46)); }
	inline bool get_m_PreventFontCallback_46() const { return ___m_PreventFontCallback_46; }
	inline bool* get_address_of_m_PreventFontCallback_46() { return &___m_PreventFontCallback_46; }
	inline void set_m_PreventFontCallback_46(bool value)
	{
		___m_PreventFontCallback_46 = value;
	}

	inline static int32_t get_offset_of_m_Mesh_47() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_Mesh_47)); }
	inline Mesh_t3648964284 * get_m_Mesh_47() const { return ___m_Mesh_47; }
	inline Mesh_t3648964284 ** get_address_of_m_Mesh_47() { return &___m_Mesh_47; }
	inline void set_m_Mesh_47(Mesh_t3648964284 * value)
	{
		___m_Mesh_47 = value;
		Il2CppCodeGenWriteBarrier((&___m_Mesh_47), value);
	}

	inline static int32_t get_offset_of_m_AllowInput_48() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_AllowInput_48)); }
	inline bool get_m_AllowInput_48() const { return ___m_AllowInput_48; }
	inline bool* get_address_of_m_AllowInput_48() { return &___m_AllowInput_48; }
	inline void set_m_AllowInput_48(bool value)
	{
		___m_AllowInput_48 = value;
	}

	inline static int32_t get_offset_of_m_ShouldActivateNextUpdate_49() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_ShouldActivateNextUpdate_49)); }
	inline bool get_m_ShouldActivateNextUpdate_49() const { return ___m_ShouldActivateNextUpdate_49; }
	inline bool* get_address_of_m_ShouldActivateNextUpdate_49() { return &___m_ShouldActivateNextUpdate_49; }
	inline void set_m_ShouldActivateNextUpdate_49(bool value)
	{
		___m_ShouldActivateNextUpdate_49 = value;
	}

	inline static int32_t get_offset_of_m_UpdateDrag_50() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_UpdateDrag_50)); }
	inline bool get_m_UpdateDrag_50() const { return ___m_UpdateDrag_50; }
	inline bool* get_address_of_m_UpdateDrag_50() { return &___m_UpdateDrag_50; }
	inline void set_m_UpdateDrag_50(bool value)
	{
		___m_UpdateDrag_50 = value;
	}

	inline static int32_t get_offset_of_m_DragPositionOutOfBounds_51() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_DragPositionOutOfBounds_51)); }
	inline bool get_m_DragPositionOutOfBounds_51() const { return ___m_DragPositionOutOfBounds_51; }
	inline bool* get_address_of_m_DragPositionOutOfBounds_51() { return &___m_DragPositionOutOfBounds_51; }
	inline void set_m_DragPositionOutOfBounds_51(bool value)
	{
		___m_DragPositionOutOfBounds_51 = value;
	}

	inline static int32_t get_offset_of_m_CaretVisible_54() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_CaretVisible_54)); }
	inline bool get_m_CaretVisible_54() const { return ___m_CaretVisible_54; }
	inline bool* get_address_of_m_CaretVisible_54() { return &___m_CaretVisible_54; }
	inline void set_m_CaretVisible_54(bool value)
	{
		___m_CaretVisible_54 = value;
	}

	inline static int32_t get_offset_of_m_BlinkCoroutine_55() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_BlinkCoroutine_55)); }
	inline Coroutine_t3829159415 * get_m_BlinkCoroutine_55() const { return ___m_BlinkCoroutine_55; }
	inline Coroutine_t3829159415 ** get_address_of_m_BlinkCoroutine_55() { return &___m_BlinkCoroutine_55; }
	inline void set_m_BlinkCoroutine_55(Coroutine_t3829159415 * value)
	{
		___m_BlinkCoroutine_55 = value;
		Il2CppCodeGenWriteBarrier((&___m_BlinkCoroutine_55), value);
	}

	inline static int32_t get_offset_of_m_BlinkStartTime_56() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_BlinkStartTime_56)); }
	inline float get_m_BlinkStartTime_56() const { return ___m_BlinkStartTime_56; }
	inline float* get_address_of_m_BlinkStartTime_56() { return &___m_BlinkStartTime_56; }
	inline void set_m_BlinkStartTime_56(float value)
	{
		___m_BlinkStartTime_56 = value;
	}

	inline static int32_t get_offset_of_m_DrawStart_57() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_DrawStart_57)); }
	inline int32_t get_m_DrawStart_57() const { return ___m_DrawStart_57; }
	inline int32_t* get_address_of_m_DrawStart_57() { return &___m_DrawStart_57; }
	inline void set_m_DrawStart_57(int32_t value)
	{
		___m_DrawStart_57 = value;
	}

	inline static int32_t get_offset_of_m_DrawEnd_58() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_DrawEnd_58)); }
	inline int32_t get_m_DrawEnd_58() const { return ___m_DrawEnd_58; }
	inline int32_t* get_address_of_m_DrawEnd_58() { return &___m_DrawEnd_58; }
	inline void set_m_DrawEnd_58(int32_t value)
	{
		___m_DrawEnd_58 = value;
	}

	inline static int32_t get_offset_of_m_DragCoroutine_59() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_DragCoroutine_59)); }
	inline Coroutine_t3829159415 * get_m_DragCoroutine_59() const { return ___m_DragCoroutine_59; }
	inline Coroutine_t3829159415 ** get_address_of_m_DragCoroutine_59() { return &___m_DragCoroutine_59; }
	inline void set_m_DragCoroutine_59(Coroutine_t3829159415 * value)
	{
		___m_DragCoroutine_59 = value;
		Il2CppCodeGenWriteBarrier((&___m_DragCoroutine_59), value);
	}

	inline static int32_t get_offset_of_m_OriginalText_60() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_OriginalText_60)); }
	inline String_t* get_m_OriginalText_60() const { return ___m_OriginalText_60; }
	inline String_t** get_address_of_m_OriginalText_60() { return &___m_OriginalText_60; }
	inline void set_m_OriginalText_60(String_t* value)
	{
		___m_OriginalText_60 = value;
		Il2CppCodeGenWriteBarrier((&___m_OriginalText_60), value);
	}

	inline static int32_t get_offset_of_m_WasCanceled_61() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_WasCanceled_61)); }
	inline bool get_m_WasCanceled_61() const { return ___m_WasCanceled_61; }
	inline bool* get_address_of_m_WasCanceled_61() { return &___m_WasCanceled_61; }
	inline void set_m_WasCanceled_61(bool value)
	{
		___m_WasCanceled_61 = value;
	}

	inline static int32_t get_offset_of_m_HasDoneFocusTransition_62() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_HasDoneFocusTransition_62)); }
	inline bool get_m_HasDoneFocusTransition_62() const { return ___m_HasDoneFocusTransition_62; }
	inline bool* get_address_of_m_HasDoneFocusTransition_62() { return &___m_HasDoneFocusTransition_62; }
	inline void set_m_HasDoneFocusTransition_62(bool value)
	{
		___m_HasDoneFocusTransition_62 = value;
	}

	inline static int32_t get_offset_of_m_ProcessingEvent_64() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_ProcessingEvent_64)); }
	inline Event_t2956885303 * get_m_ProcessingEvent_64() const { return ___m_ProcessingEvent_64; }
	inline Event_t2956885303 ** get_address_of_m_ProcessingEvent_64() { return &___m_ProcessingEvent_64; }
	inline void set_m_ProcessingEvent_64(Event_t2956885303 * value)
	{
		___m_ProcessingEvent_64 = value;
		Il2CppCodeGenWriteBarrier((&___m_ProcessingEvent_64), value);
	}
};

struct InputField_t3762917431_StaticFields
{
public:
	// System.Char[] UnityEngine.UI.InputField::kSeparators
	CharU5BU5D_t3528271667* ___kSeparators_19;

public:
	inline static int32_t get_offset_of_kSeparators_19() { return static_cast<int32_t>(offsetof(InputField_t3762917431_StaticFields, ___kSeparators_19)); }
	inline CharU5BU5D_t3528271667* get_kSeparators_19() const { return ___kSeparators_19; }
	inline CharU5BU5D_t3528271667** get_address_of_kSeparators_19() { return &___kSeparators_19; }
	inline void set_kSeparators_19(CharU5BU5D_t3528271667* value)
	{
		___kSeparators_19 = value;
		Il2CppCodeGenWriteBarrier((&___kSeparators_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUTFIELD_T3762917431_H
#ifndef MASKABLEGRAPHIC_T3839221559_H
#define MASKABLEGRAPHIC_T3839221559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.MaskableGraphic
struct  MaskableGraphic_t3839221559  : public Graphic_t1660335611
{
public:
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculateStencil
	bool ___m_ShouldRecalculateStencil_21;
	// UnityEngine.Material UnityEngine.UI.MaskableGraphic::m_MaskMaterial
	Material_t340375123 * ___m_MaskMaterial_22;
	// UnityEngine.UI.RectMask2D UnityEngine.UI.MaskableGraphic::m_ParentMask
	RectMask2D_t3474889437 * ___m_ParentMask_23;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_Maskable
	bool ___m_Maskable_24;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IncludeForMasking
	bool ___m_IncludeForMasking_25;
	// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent UnityEngine.UI.MaskableGraphic::m_OnCullStateChanged
	CullStateChangedEvent_t3661388177 * ___m_OnCullStateChanged_26;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculate
	bool ___m_ShouldRecalculate_27;
	// System.Int32 UnityEngine.UI.MaskableGraphic::m_StencilValue
	int32_t ___m_StencilValue_28;
	// UnityEngine.Vector3[] UnityEngine.UI.MaskableGraphic::m_Corners
	Vector3U5BU5D_t1718750761* ___m_Corners_29;

public:
	inline static int32_t get_offset_of_m_ShouldRecalculateStencil_21() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_ShouldRecalculateStencil_21)); }
	inline bool get_m_ShouldRecalculateStencil_21() const { return ___m_ShouldRecalculateStencil_21; }
	inline bool* get_address_of_m_ShouldRecalculateStencil_21() { return &___m_ShouldRecalculateStencil_21; }
	inline void set_m_ShouldRecalculateStencil_21(bool value)
	{
		___m_ShouldRecalculateStencil_21 = value;
	}

	inline static int32_t get_offset_of_m_MaskMaterial_22() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_MaskMaterial_22)); }
	inline Material_t340375123 * get_m_MaskMaterial_22() const { return ___m_MaskMaterial_22; }
	inline Material_t340375123 ** get_address_of_m_MaskMaterial_22() { return &___m_MaskMaterial_22; }
	inline void set_m_MaskMaterial_22(Material_t340375123 * value)
	{
		___m_MaskMaterial_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_MaskMaterial_22), value);
	}

	inline static int32_t get_offset_of_m_ParentMask_23() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_ParentMask_23)); }
	inline RectMask2D_t3474889437 * get_m_ParentMask_23() const { return ___m_ParentMask_23; }
	inline RectMask2D_t3474889437 ** get_address_of_m_ParentMask_23() { return &___m_ParentMask_23; }
	inline void set_m_ParentMask_23(RectMask2D_t3474889437 * value)
	{
		___m_ParentMask_23 = value;
		Il2CppCodeGenWriteBarrier((&___m_ParentMask_23), value);
	}

	inline static int32_t get_offset_of_m_Maskable_24() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_Maskable_24)); }
	inline bool get_m_Maskable_24() const { return ___m_Maskable_24; }
	inline bool* get_address_of_m_Maskable_24() { return &___m_Maskable_24; }
	inline void set_m_Maskable_24(bool value)
	{
		___m_Maskable_24 = value;
	}

	inline static int32_t get_offset_of_m_IncludeForMasking_25() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_IncludeForMasking_25)); }
	inline bool get_m_IncludeForMasking_25() const { return ___m_IncludeForMasking_25; }
	inline bool* get_address_of_m_IncludeForMasking_25() { return &___m_IncludeForMasking_25; }
	inline void set_m_IncludeForMasking_25(bool value)
	{
		___m_IncludeForMasking_25 = value;
	}

	inline static int32_t get_offset_of_m_OnCullStateChanged_26() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_OnCullStateChanged_26)); }
	inline CullStateChangedEvent_t3661388177 * get_m_OnCullStateChanged_26() const { return ___m_OnCullStateChanged_26; }
	inline CullStateChangedEvent_t3661388177 ** get_address_of_m_OnCullStateChanged_26() { return &___m_OnCullStateChanged_26; }
	inline void set_m_OnCullStateChanged_26(CullStateChangedEvent_t3661388177 * value)
	{
		___m_OnCullStateChanged_26 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnCullStateChanged_26), value);
	}

	inline static int32_t get_offset_of_m_ShouldRecalculate_27() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_ShouldRecalculate_27)); }
	inline bool get_m_ShouldRecalculate_27() const { return ___m_ShouldRecalculate_27; }
	inline bool* get_address_of_m_ShouldRecalculate_27() { return &___m_ShouldRecalculate_27; }
	inline void set_m_ShouldRecalculate_27(bool value)
	{
		___m_ShouldRecalculate_27 = value;
	}

	inline static int32_t get_offset_of_m_StencilValue_28() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_StencilValue_28)); }
	inline int32_t get_m_StencilValue_28() const { return ___m_StencilValue_28; }
	inline int32_t* get_address_of_m_StencilValue_28() { return &___m_StencilValue_28; }
	inline void set_m_StencilValue_28(int32_t value)
	{
		___m_StencilValue_28 = value;
	}

	inline static int32_t get_offset_of_m_Corners_29() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_Corners_29)); }
	inline Vector3U5BU5D_t1718750761* get_m_Corners_29() const { return ___m_Corners_29; }
	inline Vector3U5BU5D_t1718750761** get_address_of_m_Corners_29() { return &___m_Corners_29; }
	inline void set_m_Corners_29(Vector3U5BU5D_t1718750761* value)
	{
		___m_Corners_29 = value;
		Il2CppCodeGenWriteBarrier((&___m_Corners_29), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MASKABLEGRAPHIC_T3839221559_H
#ifndef IMAGE_T2670269651_H
#define IMAGE_T2670269651_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Image
struct  Image_t2670269651  : public MaskableGraphic_t3839221559
{
public:
	// UnityEngine.Sprite UnityEngine.UI.Image::m_Sprite
	Sprite_t280657092 * ___m_Sprite_31;
	// UnityEngine.Sprite UnityEngine.UI.Image::m_OverrideSprite
	Sprite_t280657092 * ___m_OverrideSprite_32;
	// UnityEngine.UI.Image/Type UnityEngine.UI.Image::m_Type
	int32_t ___m_Type_33;
	// System.Boolean UnityEngine.UI.Image::m_PreserveAspect
	bool ___m_PreserveAspect_34;
	// System.Boolean UnityEngine.UI.Image::m_FillCenter
	bool ___m_FillCenter_35;
	// UnityEngine.UI.Image/FillMethod UnityEngine.UI.Image::m_FillMethod
	int32_t ___m_FillMethod_36;
	// System.Single UnityEngine.UI.Image::m_FillAmount
	float ___m_FillAmount_37;
	// System.Boolean UnityEngine.UI.Image::m_FillClockwise
	bool ___m_FillClockwise_38;
	// System.Int32 UnityEngine.UI.Image::m_FillOrigin
	int32_t ___m_FillOrigin_39;
	// System.Single UnityEngine.UI.Image::m_AlphaHitTestMinimumThreshold
	float ___m_AlphaHitTestMinimumThreshold_40;
	// System.Boolean UnityEngine.UI.Image::m_Tracked
	bool ___m_Tracked_41;

public:
	inline static int32_t get_offset_of_m_Sprite_31() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_Sprite_31)); }
	inline Sprite_t280657092 * get_m_Sprite_31() const { return ___m_Sprite_31; }
	inline Sprite_t280657092 ** get_address_of_m_Sprite_31() { return &___m_Sprite_31; }
	inline void set_m_Sprite_31(Sprite_t280657092 * value)
	{
		___m_Sprite_31 = value;
		Il2CppCodeGenWriteBarrier((&___m_Sprite_31), value);
	}

	inline static int32_t get_offset_of_m_OverrideSprite_32() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_OverrideSprite_32)); }
	inline Sprite_t280657092 * get_m_OverrideSprite_32() const { return ___m_OverrideSprite_32; }
	inline Sprite_t280657092 ** get_address_of_m_OverrideSprite_32() { return &___m_OverrideSprite_32; }
	inline void set_m_OverrideSprite_32(Sprite_t280657092 * value)
	{
		___m_OverrideSprite_32 = value;
		Il2CppCodeGenWriteBarrier((&___m_OverrideSprite_32), value);
	}

	inline static int32_t get_offset_of_m_Type_33() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_Type_33)); }
	inline int32_t get_m_Type_33() const { return ___m_Type_33; }
	inline int32_t* get_address_of_m_Type_33() { return &___m_Type_33; }
	inline void set_m_Type_33(int32_t value)
	{
		___m_Type_33 = value;
	}

	inline static int32_t get_offset_of_m_PreserveAspect_34() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_PreserveAspect_34)); }
	inline bool get_m_PreserveAspect_34() const { return ___m_PreserveAspect_34; }
	inline bool* get_address_of_m_PreserveAspect_34() { return &___m_PreserveAspect_34; }
	inline void set_m_PreserveAspect_34(bool value)
	{
		___m_PreserveAspect_34 = value;
	}

	inline static int32_t get_offset_of_m_FillCenter_35() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_FillCenter_35)); }
	inline bool get_m_FillCenter_35() const { return ___m_FillCenter_35; }
	inline bool* get_address_of_m_FillCenter_35() { return &___m_FillCenter_35; }
	inline void set_m_FillCenter_35(bool value)
	{
		___m_FillCenter_35 = value;
	}

	inline static int32_t get_offset_of_m_FillMethod_36() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_FillMethod_36)); }
	inline int32_t get_m_FillMethod_36() const { return ___m_FillMethod_36; }
	inline int32_t* get_address_of_m_FillMethod_36() { return &___m_FillMethod_36; }
	inline void set_m_FillMethod_36(int32_t value)
	{
		___m_FillMethod_36 = value;
	}

	inline static int32_t get_offset_of_m_FillAmount_37() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_FillAmount_37)); }
	inline float get_m_FillAmount_37() const { return ___m_FillAmount_37; }
	inline float* get_address_of_m_FillAmount_37() { return &___m_FillAmount_37; }
	inline void set_m_FillAmount_37(float value)
	{
		___m_FillAmount_37 = value;
	}

	inline static int32_t get_offset_of_m_FillClockwise_38() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_FillClockwise_38)); }
	inline bool get_m_FillClockwise_38() const { return ___m_FillClockwise_38; }
	inline bool* get_address_of_m_FillClockwise_38() { return &___m_FillClockwise_38; }
	inline void set_m_FillClockwise_38(bool value)
	{
		___m_FillClockwise_38 = value;
	}

	inline static int32_t get_offset_of_m_FillOrigin_39() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_FillOrigin_39)); }
	inline int32_t get_m_FillOrigin_39() const { return ___m_FillOrigin_39; }
	inline int32_t* get_address_of_m_FillOrigin_39() { return &___m_FillOrigin_39; }
	inline void set_m_FillOrigin_39(int32_t value)
	{
		___m_FillOrigin_39 = value;
	}

	inline static int32_t get_offset_of_m_AlphaHitTestMinimumThreshold_40() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_AlphaHitTestMinimumThreshold_40)); }
	inline float get_m_AlphaHitTestMinimumThreshold_40() const { return ___m_AlphaHitTestMinimumThreshold_40; }
	inline float* get_address_of_m_AlphaHitTestMinimumThreshold_40() { return &___m_AlphaHitTestMinimumThreshold_40; }
	inline void set_m_AlphaHitTestMinimumThreshold_40(float value)
	{
		___m_AlphaHitTestMinimumThreshold_40 = value;
	}

	inline static int32_t get_offset_of_m_Tracked_41() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_Tracked_41)); }
	inline bool get_m_Tracked_41() const { return ___m_Tracked_41; }
	inline bool* get_address_of_m_Tracked_41() { return &___m_Tracked_41; }
	inline void set_m_Tracked_41(bool value)
	{
		___m_Tracked_41 = value;
	}
};

struct Image_t2670269651_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Image::s_ETC1DefaultUI
	Material_t340375123 * ___s_ETC1DefaultUI_30;
	// UnityEngine.Vector2[] UnityEngine.UI.Image::s_VertScratch
	Vector2U5BU5D_t1457185986* ___s_VertScratch_42;
	// UnityEngine.Vector2[] UnityEngine.UI.Image::s_UVScratch
	Vector2U5BU5D_t1457185986* ___s_UVScratch_43;
	// UnityEngine.Vector3[] UnityEngine.UI.Image::s_Xy
	Vector3U5BU5D_t1718750761* ___s_Xy_44;
	// UnityEngine.Vector3[] UnityEngine.UI.Image::s_Uv
	Vector3U5BU5D_t1718750761* ___s_Uv_45;
	// System.Collections.Generic.List`1<UnityEngine.UI.Image> UnityEngine.UI.Image::m_TrackedTexturelessImages
	List_1_t4142344393 * ___m_TrackedTexturelessImages_46;
	// System.Boolean UnityEngine.UI.Image::s_Initialized
	bool ___s_Initialized_47;
	// System.Action`1<UnityEngine.U2D.SpriteAtlas> UnityEngine.UI.Image::<>f__mg$cache0
	Action_1_t819399007 * ___U3CU3Ef__mgU24cache0_48;

public:
	inline static int32_t get_offset_of_s_ETC1DefaultUI_30() { return static_cast<int32_t>(offsetof(Image_t2670269651_StaticFields, ___s_ETC1DefaultUI_30)); }
	inline Material_t340375123 * get_s_ETC1DefaultUI_30() const { return ___s_ETC1DefaultUI_30; }
	inline Material_t340375123 ** get_address_of_s_ETC1DefaultUI_30() { return &___s_ETC1DefaultUI_30; }
	inline void set_s_ETC1DefaultUI_30(Material_t340375123 * value)
	{
		___s_ETC1DefaultUI_30 = value;
		Il2CppCodeGenWriteBarrier((&___s_ETC1DefaultUI_30), value);
	}

	inline static int32_t get_offset_of_s_VertScratch_42() { return static_cast<int32_t>(offsetof(Image_t2670269651_StaticFields, ___s_VertScratch_42)); }
	inline Vector2U5BU5D_t1457185986* get_s_VertScratch_42() const { return ___s_VertScratch_42; }
	inline Vector2U5BU5D_t1457185986** get_address_of_s_VertScratch_42() { return &___s_VertScratch_42; }
	inline void set_s_VertScratch_42(Vector2U5BU5D_t1457185986* value)
	{
		___s_VertScratch_42 = value;
		Il2CppCodeGenWriteBarrier((&___s_VertScratch_42), value);
	}

	inline static int32_t get_offset_of_s_UVScratch_43() { return static_cast<int32_t>(offsetof(Image_t2670269651_StaticFields, ___s_UVScratch_43)); }
	inline Vector2U5BU5D_t1457185986* get_s_UVScratch_43() const { return ___s_UVScratch_43; }
	inline Vector2U5BU5D_t1457185986** get_address_of_s_UVScratch_43() { return &___s_UVScratch_43; }
	inline void set_s_UVScratch_43(Vector2U5BU5D_t1457185986* value)
	{
		___s_UVScratch_43 = value;
		Il2CppCodeGenWriteBarrier((&___s_UVScratch_43), value);
	}

	inline static int32_t get_offset_of_s_Xy_44() { return static_cast<int32_t>(offsetof(Image_t2670269651_StaticFields, ___s_Xy_44)); }
	inline Vector3U5BU5D_t1718750761* get_s_Xy_44() const { return ___s_Xy_44; }
	inline Vector3U5BU5D_t1718750761** get_address_of_s_Xy_44() { return &___s_Xy_44; }
	inline void set_s_Xy_44(Vector3U5BU5D_t1718750761* value)
	{
		___s_Xy_44 = value;
		Il2CppCodeGenWriteBarrier((&___s_Xy_44), value);
	}

	inline static int32_t get_offset_of_s_Uv_45() { return static_cast<int32_t>(offsetof(Image_t2670269651_StaticFields, ___s_Uv_45)); }
	inline Vector3U5BU5D_t1718750761* get_s_Uv_45() const { return ___s_Uv_45; }
	inline Vector3U5BU5D_t1718750761** get_address_of_s_Uv_45() { return &___s_Uv_45; }
	inline void set_s_Uv_45(Vector3U5BU5D_t1718750761* value)
	{
		___s_Uv_45 = value;
		Il2CppCodeGenWriteBarrier((&___s_Uv_45), value);
	}

	inline static int32_t get_offset_of_m_TrackedTexturelessImages_46() { return static_cast<int32_t>(offsetof(Image_t2670269651_StaticFields, ___m_TrackedTexturelessImages_46)); }
	inline List_1_t4142344393 * get_m_TrackedTexturelessImages_46() const { return ___m_TrackedTexturelessImages_46; }
	inline List_1_t4142344393 ** get_address_of_m_TrackedTexturelessImages_46() { return &___m_TrackedTexturelessImages_46; }
	inline void set_m_TrackedTexturelessImages_46(List_1_t4142344393 * value)
	{
		___m_TrackedTexturelessImages_46 = value;
		Il2CppCodeGenWriteBarrier((&___m_TrackedTexturelessImages_46), value);
	}

	inline static int32_t get_offset_of_s_Initialized_47() { return static_cast<int32_t>(offsetof(Image_t2670269651_StaticFields, ___s_Initialized_47)); }
	inline bool get_s_Initialized_47() const { return ___s_Initialized_47; }
	inline bool* get_address_of_s_Initialized_47() { return &___s_Initialized_47; }
	inline void set_s_Initialized_47(bool value)
	{
		___s_Initialized_47 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_48() { return static_cast<int32_t>(offsetof(Image_t2670269651_StaticFields, ___U3CU3Ef__mgU24cache0_48)); }
	inline Action_1_t819399007 * get_U3CU3Ef__mgU24cache0_48() const { return ___U3CU3Ef__mgU24cache0_48; }
	inline Action_1_t819399007 ** get_address_of_U3CU3Ef__mgU24cache0_48() { return &___U3CU3Ef__mgU24cache0_48; }
	inline void set_U3CU3Ef__mgU24cache0_48(Action_1_t819399007 * value)
	{
		___U3CU3Ef__mgU24cache0_48 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_48), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMAGE_T2670269651_H
#ifndef RAWIMAGE_T3182918964_H
#define RAWIMAGE_T3182918964_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.RawImage
struct  RawImage_t3182918964  : public MaskableGraphic_t3839221559
{
public:
	// UnityEngine.Texture UnityEngine.UI.RawImage::m_Texture
	Texture_t3661962703 * ___m_Texture_30;
	// UnityEngine.Rect UnityEngine.UI.RawImage::m_UVRect
	Rect_t2360479859  ___m_UVRect_31;

public:
	inline static int32_t get_offset_of_m_Texture_30() { return static_cast<int32_t>(offsetof(RawImage_t3182918964, ___m_Texture_30)); }
	inline Texture_t3661962703 * get_m_Texture_30() const { return ___m_Texture_30; }
	inline Texture_t3661962703 ** get_address_of_m_Texture_30() { return &___m_Texture_30; }
	inline void set_m_Texture_30(Texture_t3661962703 * value)
	{
		___m_Texture_30 = value;
		Il2CppCodeGenWriteBarrier((&___m_Texture_30), value);
	}

	inline static int32_t get_offset_of_m_UVRect_31() { return static_cast<int32_t>(offsetof(RawImage_t3182918964, ___m_UVRect_31)); }
	inline Rect_t2360479859  get_m_UVRect_31() const { return ___m_UVRect_31; }
	inline Rect_t2360479859 * get_address_of_m_UVRect_31() { return &___m_UVRect_31; }
	inline void set_m_UVRect_31(Rect_t2360479859  value)
	{
		___m_UVRect_31 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAWIMAGE_T3182918964_H
// UnityEngine.Renderer[]
struct RendererU5BU5D_t3210418286  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Renderer_t2627027031 * m_Items[1];

public:
	inline Renderer_t2627027031 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Renderer_t2627027031 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Renderer_t2627027031 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Renderer_t2627027031 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Renderer_t2627027031 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Renderer_t2627027031 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Object[]
struct ObjectU5BU5D_t2843939325  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject * m_Items[1];

public:
	inline RuntimeObject * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline RuntimeObject * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.Color[]
struct ColorU5BU5D_t941916413  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Color_t2555686324  m_Items[1];

public:
	inline Color_t2555686324  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Color_t2555686324 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Color_t2555686324  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Color_t2555686324  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Color_t2555686324 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Color_t2555686324  value)
	{
		m_Items[index] = value;
	}
};
// System.String[]
struct StringU5BU5D_t1281789340  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) String_t* m_Items[1];

public:
	inline String_t* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline String_t** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, String_t* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline String_t* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline String_t** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, String_t* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Byte[]
struct ByteU5BU5D_t4116647657  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) uint8_t m_Items[1];

public:
	inline uint8_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline uint8_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, uint8_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline uint8_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline uint8_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, uint8_t value)
	{
		m_Items[index] = value;
	}
};


// !!0[] UnityEngine.Component::GetComponentsInChildren<System.Object>()
extern "C" IL2CPP_METHOD_ATTR ObjectU5BU5D_t2843939325* Component_GetComponentsInChildren_TisRuntimeObject_m3953365647_gshared (Component_t1923634451 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * GameObject_GetComponent_TisRuntimeObject_m2049753423_gshared (GameObject_t1113636619 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * Component_GetComponent_TisRuntimeObject_m2906321015_gshared (Component_t1923634451 * __this, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count()
extern "C" IL2CPP_METHOD_ATTR int32_t List_1_get_Count_m2934127733_gshared (List_1_t257213610 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<System.Object>::get_Item(System.Int32)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * List_1_get_Item_m2287542950_gshared (List_1_t257213610 * __this, int32_t p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::RemoveAt(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void List_1_RemoveAt_m2730968292_gshared (List_1_t257213610 * __this, int32_t p0, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::AddComponent<System.Object>()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * GameObject_AddComponent_TisRuntimeObject_m3469369570_gshared (GameObject_t1113636619 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
extern "C" IL2CPP_METHOD_ATTR void List_1__ctor_m2321703786_gshared (List_1_t257213610 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Add(!0)
extern "C" IL2CPP_METHOD_ATTR void List_1_Add_m3338814081_gshared (List_1_t257213610 * __this, RuntimeObject * p0, const RuntimeMethod* method);

// System.Void TriLib.TextureLoadHandle::Invoke(System.String,UnityEngine.Material,System.String,UnityEngine.Texture2D)
extern "C" IL2CPP_METHOD_ATTR void TextureLoadHandle_Invoke_m1791263530 (TextureLoadHandle_t2346000395 * __this, String_t* ___sourcePath0, Material_t340375123 * ___material1, String_t* ___propertyName2, Texture2D_t3840446185 * ___texture3, const RuntimeMethod* method);
// UnityEngine.Vector3 TriLib.MatrixExtensions::ExtractScale(UnityEngine.Matrix4x4)
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  MatrixExtensions_ExtractScale_m3504230185 (RuntimeObject * __this /* static, unused */, Matrix4x4_t1817901843  ___matrix0, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_localScale(UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR void Transform_set_localScale_m3053443106 (Transform_t3600365921 * __this, Vector3_t3722313464  p0, const RuntimeMethod* method);
// UnityEngine.Quaternion TriLib.MatrixExtensions::ExtractRotation(UnityEngine.Matrix4x4)
extern "C" IL2CPP_METHOD_ATTR Quaternion_t2301928331  MatrixExtensions_ExtractRotation_m3933191427 (RuntimeObject * __this /* static, unused */, Matrix4x4_t1817901843  ___matrix0, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_localRotation(UnityEngine.Quaternion)
extern "C" IL2CPP_METHOD_ATTR void Transform_set_localRotation_m19445462 (Transform_t3600365921 * __this, Quaternion_t2301928331  p0, const RuntimeMethod* method);
// UnityEngine.Vector3 TriLib.MatrixExtensions::ExtractPosition(UnityEngine.Matrix4x4)
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  MatrixExtensions_ExtractPosition_m3825606610 (RuntimeObject * __this /* static, unused */, Matrix4x4_t1817901843  ___matrix0, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_localPosition(UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR void Transform_set_localPosition_m4128471975 (Transform_t3600365921 * __this, Vector3_t3722313464  p0, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_rotation(UnityEngine.Quaternion)
extern "C" IL2CPP_METHOD_ATTR void Transform_set_rotation_m3524318132 (Transform_t3600365921 * __this, Quaternion_t2301928331  p0, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_position(UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR void Transform_set_position_m3387557959 (Transform_t3600365921 * __this, Vector3_t3722313464  p0, const RuntimeMethod* method);
// !!0[] UnityEngine.Component::GetComponentsInChildren<UnityEngine.Renderer>()
inline RendererU5BU5D_t3210418286* Component_GetComponentsInChildren_TisRenderer_t2627027031_m2677127880 (Component_t1923634451 * __this, const RuntimeMethod* method)
{
	return ((  RendererU5BU5D_t3210418286* (*) (Component_t1923634451 *, const RuntimeMethod*))Component_GetComponentsInChildren_TisRuntimeObject_m3953365647_gshared)(__this, method);
}
// UnityEngine.Bounds UnityEngine.Renderer::get_bounds()
extern "C" IL2CPP_METHOD_ATTR Bounds_t2266837910  Renderer_get_bounds_m1803204000 (Renderer_t2627027031 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Bounds::Encapsulate(UnityEngine.Bounds)
extern "C" IL2CPP_METHOD_ATTR void Bounds_Encapsulate_m1263362003 (Bounds_t2266837910 * __this, Bounds_t2266837910  p0, const RuntimeMethod* method);
// System.String UnityEngine.Object::get_name()
extern "C" IL2CPP_METHOD_ATTR String_t* Object_get_name_m4211327027 (Object_t631007953 * __this, const RuntimeMethod* method);
// System.Boolean System.String::op_Equality(System.String,System.String)
extern "C" IL2CPP_METHOD_ATTR bool String_op_Equality_m920492651 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, const RuntimeMethod* method);
// System.Boolean System.String::EndsWith(System.String)
extern "C" IL2CPP_METHOD_ATTR bool String_EndsWith_m1901926500 (String_t* __this, String_t* p0, const RuntimeMethod* method);
// System.Collections.IEnumerator UnityEngine.Transform::GetEnumerator()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* Transform_GetEnumerator_m2717073726 (Transform_t3600365921 * __this, const RuntimeMethod* method);
// UnityEngine.Transform TriLib.TransformExtensions::FindDeepChild(UnityEngine.Transform,System.String,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR Transform_t3600365921 * TransformExtensions_FindDeepChild_m311219388 (RuntimeObject * __this /* static, unused */, Transform_t3600365921 * ___transform0, String_t* ___name1, bool ___endsWith2, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
extern "C" IL2CPP_METHOD_ATTR bool Object_op_Inequality_m4071470834 (RuntimeObject * __this /* static, unused */, Object_t631007953 * p0, Object_t631007953 * p1, const RuntimeMethod* method);
// System.Int32 UnityEngine.Transform::get_childCount()
extern "C" IL2CPP_METHOD_ATTR int32_t Transform_get_childCount_m3145433196 (Transform_t3600365921 * __this, const RuntimeMethod* method);
// UnityEngine.Transform UnityEngine.Transform::GetChild(System.Int32)
extern "C" IL2CPP_METHOD_ATTR Transform_t3600365921 * Transform_GetChild_m1092972975 (Transform_t3600365921 * __this, int32_t p0, const RuntimeMethod* method);
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
extern "C" IL2CPP_METHOD_ATTR GameObject_t1113636619 * Component_get_gameObject_m442555142 (Component_t1923634451 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Object::DestroyImmediate(UnityEngine.Object)
extern "C" IL2CPP_METHOD_ATTR void Object_DestroyImmediate_m3193525861 (RuntimeObject * __this /* static, unused */, Object_t631007953 * p0, const RuntimeMethod* method);
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object)
extern "C" IL2CPP_METHOD_ATTR void Object_Destroy_m565254235 (RuntimeObject * __this /* static, unused */, Object_t631007953 * p0, const RuntimeMethod* method);
// System.Void UnityEngine.MonoBehaviour::.ctor()
extern "C" IL2CPP_METHOD_ATTR void MonoBehaviour__ctor_m1579109191 (MonoBehaviour_t3962482529 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.GameObject::SetActive(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void GameObject_SetActive_m796801857 (GameObject_t1113636619 * __this, bool p0, const RuntimeMethod* method);
// System.Void UnityEngine.SceneManagement.SceneManager::LoadScene(System.String)
extern "C" IL2CPP_METHOD_ATTR void SceneManager_LoadScene_m1758133949 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method);
// System.Void UnityEngine.Application::OpenURL(System.String)
extern "C" IL2CPP_METHOD_ATTR void Application_OpenURL_m738341736 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Animator>()
inline Animator_t434523843 * GameObject_GetComponent_TisAnimator_t434523843_m440019408 (GameObject_t1113636619 * __this, const RuntimeMethod* method)
{
	return ((  Animator_t434523843 * (*) (GameObject_t1113636619 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m2049753423_gshared)(__this, method);
}
// System.Void UnityEngine.Animator::set_speed(System.Single)
extern "C" IL2CPP_METHOD_ATTR void Animator_set_speed_m1181320995 (Animator_t434523843 * __this, float p0, const RuntimeMethod* method);
// System.Int32 UnityEngine.PlayerPrefs::GetInt(System.String,System.Int32)
extern "C" IL2CPP_METHOD_ATTR int32_t PlayerPrefs_GetInt_m1299643124 (RuntimeObject * __this /* static, unused */, String_t* p0, int32_t p1, const RuntimeMethod* method);
// System.Void UnityEngine.PlayerPrefs::SetInt(System.String,System.Int32)
extern "C" IL2CPP_METHOD_ATTR void PlayerPrefs_SetInt_m2842000469 (RuntimeObject * __this /* static, unused */, String_t* p0, int32_t p1, const RuntimeMethod* method);
// System.Void UnityEngine.PlayerPrefs::Save()
extern "C" IL2CPP_METHOD_ATTR void PlayerPrefs_Save_m2701929462 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Void UnityEngine.MonoBehaviour::Invoke(System.String,System.Single)
extern "C" IL2CPP_METHOD_ATTR void MonoBehaviour_Invoke_m4227543964 (MonoBehaviour_t3962482529 * __this, String_t* p0, float p1, const RuntimeMethod* method);
// System.Void UnityEngine.Animator::Play(System.String)
extern "C" IL2CPP_METHOD_ATTR void Animator_Play_m1697843332 (Animator_t434523843 * __this, String_t* p0, const RuntimeMethod* method);
// System.Void PageTransition::Init()
extern "C" IL2CPP_METHOD_ATTR void PageTransition_Init_m631843422 (PageTransition_t3158442731 * __this, const RuntimeMethod* method);
// System.Void UIManager::OnTutoClosed()
extern "C" IL2CPP_METHOD_ATTR void UIManager_OnTutoClosed_m3625715984 (UIManager_t1042050227 * __this, const RuntimeMethod* method);
// System.Void UIManager::_ReturnToNative()
extern "C" IL2CPP_METHOD_ATTR void UIManager__ReturnToNative_m374797032 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// UnityEngine.RuntimePlatform UnityEngine.Application::get_platform()
extern "C" IL2CPP_METHOD_ATTR int32_t Application_get_platform_m2150679437 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Boolean UnityEngine.Input::GetKey(UnityEngine.KeyCode)
extern "C" IL2CPP_METHOD_ATTR bool Input_GetKey_m3736388334 (RuntimeObject * __this /* static, unused */, int32_t p0, const RuntimeMethod* method);
// System.Void UIManager::OnQuit()
extern "C" IL2CPP_METHOD_ATTR void UIManager_OnQuit_m447715357 (UIManager_t1042050227 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::get_localScale()
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  Transform_get_localScale_m129152068 (Transform_t3600365921 * __this, const RuntimeMethod* method);
// System.Int32 UnityEngine.Input::get_touchCount()
extern "C" IL2CPP_METHOD_ATTR int32_t Input_get_touchCount_m3403849067 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// UnityEngine.Touch UnityEngine.Input::GetTouch(System.Int32)
extern "C" IL2CPP_METHOD_ATTR Touch_t1921856868  Input_GetTouch_m2192712756 (RuntimeObject * __this /* static, unused */, int32_t p0, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.Touch::get_deltaPosition()
extern "C" IL2CPP_METHOD_ATTR Vector2_t2156229523  Touch_get_deltaPosition_m2389653382 (Touch_t1921856868 * __this, const RuntimeMethod* method);
// System.String System.String::Concat(System.Object,System.Object)
extern "C" IL2CPP_METHOD_ATTR String_t* String_Concat_m904156431 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, RuntimeObject * p1, const RuntimeMethod* method);
// System.Void UnityEngine.Debug::Log(System.Object)
extern "C" IL2CPP_METHOD_ATTR void Debug_Log_m4051431634 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.Touch::get_position()
extern "C" IL2CPP_METHOD_ATTR Vector2_t2156229523  Touch_get_position_m3109777936 (Touch_t1921856868 * __this, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.Vector2::op_Subtraction(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C" IL2CPP_METHOD_ATTR Vector2_t2156229523  Vector2_op_Subtraction_m73004381 (RuntimeObject * __this /* static, unused */, Vector2_t2156229523  p0, Vector2_t2156229523  p1, const RuntimeMethod* method);
// System.Single UnityEngine.Vector2::get_magnitude()
extern "C" IL2CPP_METHOD_ATTR float Vector2_get_magnitude_m2752892833 (Vector2_t2156229523 * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Time::get_deltaTime()
extern "C" IL2CPP_METHOD_ATTR float Time_get_deltaTime_m372706562 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Single UnityEngine.Mathf::Clamp(System.Single,System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR float Mathf_Clamp_m3350697880 (RuntimeObject * __this /* static, unused */, float p0, float p1, float p2, const RuntimeMethod* method);
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR void Vector3__ctor_m3353183577 (Vector3_t3722313464 * __this, float p0, float p1, float p2, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<UnityEngine.SpriteRenderer>()
inline SpriteRenderer_t3235626157 * Component_GetComponent_TisSpriteRenderer_t3235626157_m2253180502 (Component_t1923634451 * __this, const RuntimeMethod* method)
{
	return ((  SpriteRenderer_t3235626157 * (*) (Component_t1923634451 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2906321015_gshared)(__this, method);
}
// System.Void UnityEngine.Renderer::set_enabled(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void Renderer_set_enabled_m1727253150 (Renderer_t2627027031 * __this, bool p0, const RuntimeMethod* method);
// System.Void UnityEngine.Color::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR void Color__ctor_m2943235014 (Color_t2555686324 * __this, float p0, float p1, float p2, float p3, const RuntimeMethod* method);
// System.Void animationImage::DemarerAnimation()
extern "C" IL2CPP_METHOD_ATTR void animationImage_DemarerAnimation_m273565184 (animationImage_t1423068421 * __this, const RuntimeMethod* method);
// System.Void animationImage::AnimationOuverture(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void animationImage_AnimationOuverture_m2423226750 (animationImage_t1423068421 * __this, int32_t ___choix0, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<Pixel>::get_Count()
inline int32_t List_1_get_Count_m334549794 (List_1_t4186031580 * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_t4186031580 *, const RuntimeMethod*))List_1_get_Count_m2934127733_gshared)(__this, method);
}
// System.Int32 UnityEngine.Random::Range(System.Int32,System.Int32)
extern "C" IL2CPP_METHOD_ATTR int32_t Random_Range_m4054026115 (RuntimeObject * __this /* static, unused */, int32_t p0, int32_t p1, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<Pixel>::get_Item(System.Int32)
inline Pixel_t2713956838 * List_1_get_Item_m1974214454 (List_1_t4186031580 * __this, int32_t p0, const RuntimeMethod* method)
{
	return ((  Pixel_t2713956838 * (*) (List_1_t4186031580 *, int32_t, const RuntimeMethod*))List_1_get_Item_m2287542950_gshared)(__this, p0, method);
}
// System.Void UnityEngine.Texture2D::SetPixel(System.Int32,System.Int32,UnityEngine.Color)
extern "C" IL2CPP_METHOD_ATTR void Texture2D_SetPixel_m2984741184 (Texture2D_t3840446185 * __this, int32_t p0, int32_t p1, Color_t2555686324  p2, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<Pixel>::RemoveAt(System.Int32)
inline void List_1_RemoveAt_m1835037992 (List_1_t4186031580 * __this, int32_t p0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t4186031580 *, int32_t, const RuntimeMethod*))List_1_RemoveAt_m2730968292_gshared)(__this, p0, method);
}
// System.Int32 UnityEngine.Mathf::Min(System.Int32,System.Int32)
extern "C" IL2CPP_METHOD_ATTR int32_t Mathf_Min_m18103608 (RuntimeObject * __this /* static, unused */, int32_t p0, int32_t p1, const RuntimeMethod* method);
// System.Void UnityEngine.Texture2D::Apply()
extern "C" IL2CPP_METHOD_ATTR void Texture2D_Apply_m2271746283 (Texture2D_t3840446185 * __this, const RuntimeMethod* method);
// System.Void animationImage::randomisationverticale()
extern "C" IL2CPP_METHOD_ATTR void animationImage_randomisationverticale_m1761082774 (animationImage_t1423068421 * __this, const RuntimeMethod* method);
// UnityEngine.Color UnityEngine.Texture2D::GetPixel(System.Int32,System.Int32)
extern "C" IL2CPP_METHOD_ATTR Color_t2555686324  Texture2D_GetPixel_m1195410881 (Texture2D_t3840446185 * __this, int32_t p0, int32_t p1, const RuntimeMethod* method);
// System.Void UnityEngine.GameObject::.ctor()
extern "C" IL2CPP_METHOD_ATTR void GameObject__ctor_m3707688467 (GameObject_t1113636619 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Object::set_name(System.String)
extern "C" IL2CPP_METHOD_ATTR void Object_set_name_m291480324 (Object_t631007953 * __this, String_t* p0, const RuntimeMethod* method);
// UnityEngine.Transform UnityEngine.GameObject::get_transform()
extern "C" IL2CPP_METHOD_ATTR Transform_t3600365921 * GameObject_get_transform_m1369836730 (GameObject_t1113636619 * __this, const RuntimeMethod* method);
// UnityEngine.Transform UnityEngine.Component::get_transform()
extern "C" IL2CPP_METHOD_ATTR Transform_t3600365921 * Component_get_transform_m3162698980 (Component_t1923634451 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_parent(UnityEngine.Transform)
extern "C" IL2CPP_METHOD_ATTR void Transform_set_parent_m786917804 (Transform_t3600365921 * __this, Transform_t3600365921 * p0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::get_position()
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  Transform_get_position_m36019626 (Transform_t3600365921 * __this, const RuntimeMethod* method);
// UnityEngine.Quaternion UnityEngine.Transform::get_rotation()
extern "C" IL2CPP_METHOD_ATTR Quaternion_t2301928331  Transform_get_rotation_m3502953881 (Transform_t3600365921 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
extern "C" IL2CPP_METHOD_ATTR bool Object_op_Equality_m1810815630 (RuntimeObject * __this /* static, unused */, Object_t631007953 * p0, Object_t631007953 * p1, const RuntimeMethod* method);
// System.Void UnityEngine.MonoBehaviour::print(System.Object)
extern "C" IL2CPP_METHOD_ATTR void MonoBehaviour_print_m330341231 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, const RuntimeMethod* method);
// UnityEngine.TextureFormat UnityEngine.Texture2D::get_format()
extern "C" IL2CPP_METHOD_ATTR int32_t Texture2D_get_format_m2371899271 (Texture2D_t3840446185 * __this, const RuntimeMethod* method);
// System.String System.String::Concat(System.Object[])
extern "C" IL2CPP_METHOD_ATTR String_t* String_Concat_m2971454694 (RuntimeObject * __this /* static, unused */, ObjectU5BU5D_t2843939325* p0, const RuntimeMethod* method);
// System.Void UnityEngine.Texture2D::.ctor(System.Int32,System.Int32,UnityEngine.TextureFormat,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void Texture2D__ctor_m2862217990 (Texture2D_t3840446185 * __this, int32_t p0, int32_t p1, int32_t p2, bool p3, const RuntimeMethod* method);
// UnityEngine.Color[] UnityEngine.Texture2D::GetPixels()
extern "C" IL2CPP_METHOD_ATTR ColorU5BU5D_t941916413* Texture2D_GetPixels_m2081641574 (Texture2D_t3840446185 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Texture2D::SetPixels(UnityEngine.Color[])
extern "C" IL2CPP_METHOD_ATTR void Texture2D_SetPixels_m3008871897 (Texture2D_t3840446185 * __this, ColorU5BU5D_t941916413* p0, const RuntimeMethod* method);
// System.Void UnityEngine.Rect::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR void Rect__ctor_m2614021312 (Rect_t2360479859 * __this, float p0, float p1, float p2, float p3, const RuntimeMethod* method);
// System.Void UnityEngine.Vector2::.ctor(System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR void Vector2__ctor_m3970636864 (Vector2_t2156229523 * __this, float p0, float p1, const RuntimeMethod* method);
// UnityEngine.Sprite UnityEngine.Sprite::Create(UnityEngine.Texture2D,UnityEngine.Rect,UnityEngine.Vector2,System.Single)
extern "C" IL2CPP_METHOD_ATTR Sprite_t280657092 * Sprite_Create_m3350529538 (RuntimeObject * __this /* static, unused */, Texture2D_t3840446185 * p0, Rect_t2360479859  p1, Vector2_t2156229523  p2, float p3, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::AddComponent<UnityEngine.SpriteRenderer>()
inline SpriteRenderer_t3235626157 * GameObject_AddComponent_TisSpriteRenderer_t3235626157_m1939192903 (GameObject_t1113636619 * __this, const RuntimeMethod* method)
{
	return ((  SpriteRenderer_t3235626157 * (*) (GameObject_t1113636619 *, const RuntimeMethod*))GameObject_AddComponent_TisRuntimeObject_m3469369570_gshared)(__this, method);
}
// System.Void UnityEngine.SpriteRenderer::set_sprite(UnityEngine.Sprite)
extern "C" IL2CPP_METHOD_ATTR void SpriteRenderer_set_sprite_m1286893786 (SpriteRenderer_t3235626157 * __this, Sprite_t280657092 * p0, const RuntimeMethod* method);
// System.Void UnityEngine.SpriteRenderer::set_color(UnityEngine.Color)
extern "C" IL2CPP_METHOD_ATTR void SpriteRenderer_set_color_m3056777566 (SpriteRenderer_t3235626157 * __this, Color_t2555686324  p0, const RuntimeMethod* method);
// System.Void UnityEngine.Renderer::set_sortingOrder(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void Renderer_set_sortingOrder_m549573253 (Renderer_t2627027031 * __this, int32_t p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<Pixel>::.ctor()
inline void List_1__ctor_m1545936499 (List_1_t4186031580 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t4186031580 *, const RuntimeMethod*))List_1__ctor_m2321703786_gshared)(__this, method);
}
// System.Void Pixel::.ctor(System.Int32,System.Int32,UnityEngine.Color)
extern "C" IL2CPP_METHOD_ATTR void Pixel__ctor_m3546726819 (Pixel_t2713956838 * __this, int32_t ___i10, int32_t ___j11, Color_t2555686324  ___coul2, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<Pixel>::Add(!0)
inline void List_1_Add_m1787940857 (List_1_t4186031580 * __this, Pixel_t2713956838 * p0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t4186031580 *, Pixel_t2713956838 *, const RuntimeMethod*))List_1_Add_m3338814081_gshared)(__this, p0, method);
}
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.List`1<Pixel>>::Add(!0)
inline void List_1_Add_m530383596 (List_1_t1363139026 * __this, List_1_t4186031580 * p0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t1363139026 *, List_1_t4186031580 *, const RuntimeMethod*))List_1_Add_m3338814081_gshared)(__this, p0, method);
}
// System.String UnityEngine.UI.InputField::get_text()
extern "C" IL2CPP_METHOD_ATTR String_t* InputField_get_text_m3534748202 (InputField_t3762917431 * __this, const RuntimeMethod* method);
// UnityEngine.Transform UnityEngine.Transform::Find(System.String)
extern "C" IL2CPP_METHOD_ATTR Transform_t3600365921 * Transform_Find_m1729760951 (Transform_t3600365921 * __this, String_t* p0, const RuntimeMethod* method);
// UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine(System.String)
extern "C" IL2CPP_METHOD_ATTR Coroutine_t3829159415 * MonoBehaviour_StartCoroutine_m2618285814 (MonoBehaviour_t3962482529 * __this, String_t* p0, const RuntimeMethod* method);
// System.Void captureAndSendScreen/<IMPL_sendCapture>c__Iterator0::.ctor()
extern "C" IL2CPP_METHOD_ATTR void U3CIMPL_sendCaptureU3Ec__Iterator0__ctor_m2744192186 (U3CIMPL_sendCaptureU3Ec__Iterator0_t1292966789 * __this, const RuntimeMethod* method);
// System.Void System.Object::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Object__ctor_m297566312 (RuntimeObject * __this, const RuntimeMethod* method);
// System.Int32 UnityEngine.Screen::get_width()
extern "C" IL2CPP_METHOD_ATTR int32_t Screen_get_width_m345039817 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Int32 UnityEngine.Screen::get_height()
extern "C" IL2CPP_METHOD_ATTR int32_t Screen_get_height_m1623532518 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.String System.String::Concat(System.String[])
extern "C" IL2CPP_METHOD_ATTR String_t* String_Concat_m1809518182 (RuntimeObject * __this /* static, unused */, StringU5BU5D_t1281789340* p0, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Renderer>()
inline Renderer_t2627027031 * GameObject_GetComponent_TisRenderer_t2627027031_m1619941042 (GameObject_t1113636619 * __this, const RuntimeMethod* method)
{
	return ((  Renderer_t2627027031 * (*) (GameObject_t1113636619 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m2049753423_gshared)(__this, method);
}
// UnityEngine.Material UnityEngine.Renderer::get_material()
extern "C" IL2CPP_METHOD_ATTR Material_t340375123 * Renderer_get_material_m4171603682 (Renderer_t2627027031 * __this, const RuntimeMethod* method);
// UnityEngine.Texture UnityEngine.Material::get_mainTexture()
extern "C" IL2CPP_METHOD_ATTR Texture_t3661962703 * Material_get_mainTexture_m692510677 (Material_t340375123 * __this, const RuntimeMethod* method);
// System.Byte[] UnityEngine.ImageConversion::EncodeToPNG(UnityEngine.Texture2D)
extern "C" IL2CPP_METHOD_ATTR ByteU5BU5D_t4116647657* ImageConversion_EncodeToPNG_m2292631531 (RuntimeObject * __this /* static, unused */, Texture2D_t3840446185 * p0, const RuntimeMethod* method);
// System.Void UnityEngine.WWWForm::.ctor()
extern "C" IL2CPP_METHOD_ATTR void WWWForm__ctor_m2465700452 (WWWForm_t4064702195 * __this, const RuntimeMethod* method);
// System.Int32 UnityEngine.Time::get_frameCount()
extern "C" IL2CPP_METHOD_ATTR int32_t Time_get_frameCount_m1220035214 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.String System.Int32::ToString()
extern "C" IL2CPP_METHOD_ATTR String_t* Int32_ToString_m141394615 (int32_t* __this, const RuntimeMethod* method);
// System.Void UnityEngine.WWWForm::AddField(System.String,System.String)
extern "C" IL2CPP_METHOD_ATTR void WWWForm_AddField_m2357902982 (WWWForm_t4064702195 * __this, String_t* p0, String_t* p1, const RuntimeMethod* method);
// System.Void UnityEngine.WWWForm::AddBinaryData(System.String,System.Byte[],System.String,System.String)
extern "C" IL2CPP_METHOD_ATTR void WWWForm_AddBinaryData_m1593257607 (WWWForm_t4064702195 * __this, String_t* p0, ByteU5BU5D_t4116647657* p1, String_t* p2, String_t* p3, const RuntimeMethod* method);
// UnityEngine.Networking.UnityWebRequest UnityEngine.Networking.UnityWebRequest::Post(System.String,UnityEngine.WWWForm)
extern "C" IL2CPP_METHOD_ATTR UnityWebRequest_t463507806 * UnityWebRequest_Post_m4193475377 (RuntimeObject * __this /* static, unused */, String_t* p0, WWWForm_t4064702195 * p1, const RuntimeMethod* method);
// UnityEngine.Networking.UnityWebRequestAsyncOperation UnityEngine.Networking.UnityWebRequest::SendWebRequest()
extern "C" IL2CPP_METHOD_ATTR UnityWebRequestAsyncOperation_t3852015985 * UnityWebRequest_SendWebRequest_m489860187 (UnityWebRequest_t463507806 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Networking.UnityWebRequest::get_isNetworkError()
extern "C" IL2CPP_METHOD_ATTR bool UnityWebRequest_get_isNetworkError_m1231611882 (UnityWebRequest_t463507806 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Networking.UnityWebRequest::get_isHttpError()
extern "C" IL2CPP_METHOD_ATTR bool UnityWebRequest_get_isHttpError_m797082501 (UnityWebRequest_t463507806 * __this, const RuntimeMethod* method);
// System.String UnityEngine.Networking.UnityWebRequest::get_error()
extern "C" IL2CPP_METHOD_ATTR String_t* UnityWebRequest_get_error_m1613086199 (UnityWebRequest_t463507806 * __this, const RuntimeMethod* method);
// System.Void System.NotSupportedException::.ctor()
extern "C" IL2CPP_METHOD_ATTR void NotSupportedException__ctor_m2730133172 (NotSupportedException_t1314879016 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.PlayerPrefs::HasKey(System.String)
extern "C" IL2CPP_METHOD_ATTR bool PlayerPrefs_HasKey_m2794939670 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method);
// System.Int32 UnityEngine.PlayerPrefs::GetInt(System.String)
extern "C" IL2CPP_METHOD_ATTR int32_t PlayerPrefs_GetInt_m3797620966 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method);
// System.Collections.IEnumerator startUpLANG::GetLanguageJSON()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* startUpLANG_GetLanguageJSON_m3198424110 (startUpLANG_t1517666504 * __this, const RuntimeMethod* method);
// UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine(System.Collections.IEnumerator)
extern "C" IL2CPP_METHOD_ATTR Coroutine_t3829159415 * MonoBehaviour_StartCoroutine_m3411253000 (MonoBehaviour_t3962482529 * __this, RuntimeObject* p0, const RuntimeMethod* method);
// System.Void startUpLANG/<GetLanguageJSON>c__Iterator0::.ctor()
extern "C" IL2CPP_METHOD_ATTR void U3CGetLanguageJSONU3Ec__Iterator0__ctor_m1391803005 (U3CGetLanguageJSONU3Ec__Iterator0_t3550365120 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.WWW::.ctor(System.String)
extern "C" IL2CPP_METHOD_ATTR void WWW__ctor_m2915079343 (WWW_t3688466362 * __this, String_t* p0, const RuntimeMethod* method);
// MarkerManager MarkerManager::GetInstance()
extern "C" IL2CPP_METHOD_ATTR MarkerManager_t1031803141 * MarkerManager_GetInstance_m3344970980 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.String UnityEngine.WWW::get_text()
extern "C" IL2CPP_METHOD_ATTR String_t* WWW_get_text_m898164367 (WWW_t3688466362 * __this, const RuntimeMethod* method);
// System.Int32 MarkerManager::parse(System.String)
extern "C" IL2CPP_METHOD_ATTR int32_t MarkerManager_parse_m1894428919 (MarkerManager_t1031803141 * __this, String_t* ___str0, const RuntimeMethod* method);
// UnityEngine.Sprite UnityEngine.Sprite::Create(UnityEngine.Texture2D,UnityEngine.Rect,UnityEngine.Vector2)
extern "C" IL2CPP_METHOD_ATTR Sprite_t280657092 * Sprite_Create_m803022218 (RuntimeObject * __this /* static, unused */, Texture2D_t3840446185 * p0, Rect_t2360479859  p1, Vector2_t2156229523  p2, const RuntimeMethod* method);
// System.Void UnityEngine.UI.Image::set_sprite(UnityEngine.Sprite)
extern "C" IL2CPP_METHOD_ATTR void Image_set_sprite_m2369174689 (Image_t2670269651 * __this, Sprite_t280657092 * p0, const RuntimeMethod* method);
// System.Void UnityEngine.UI.RawImage::set_texture(UnityEngine.Texture)
extern "C" IL2CPP_METHOD_ATTR void RawImage_set_texture_m415027901 (RawImage_t3182918964 * __this, Texture_t3661962703 * p0, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<UnityEngine.RectTransform>()
inline RectTransform_t3704657025 * Component_GetComponent_TisRectTransform_t3704657025_m3396022872 (Component_t1923634451 * __this, const RuntimeMethod* method)
{
	return ((  RectTransform_t3704657025 * (*) (Component_t1923634451 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2906321015_gshared)(__this, method);
}
// System.Void UnityEngine.RectTransform::set_sizeDelta(UnityEngine.Vector2)
extern "C" IL2CPP_METHOD_ATTR void RectTransform_set_sizeDelta_m3462269772 (RectTransform_t3704657025 * __this, Vector2_t2156229523  p0, const RuntimeMethod* method);
// System.Void UnityEngine.Video.VideoPlayer::Play()
extern "C" IL2CPP_METHOD_ATTR void VideoPlayer_Play_m1850501814 (VideoPlayer_t1683042537 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Gizmos::set_color(UnityEngine.Color)
extern "C" IL2CPP_METHOD_ATTR void Gizmos_set_color_m3399737545 (RuntimeObject * __this /* static, unused */, Color_t2555686324  p0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Bounds::get_center()
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  Bounds_get_center_m1418449258 (Bounds_t2266837910 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Gizmos::DrawCube(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR void Gizmos_DrawCube_m530322281 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, Vector3_t3722313464  p1, const RuntimeMethod* method);
// System.String UnityEngine.Bounds::ToString()
extern "C" IL2CPP_METHOD_ATTR String_t* Bounds_ToString_m4208690781 (Bounds_t2266837910 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Bounds::get_extents()
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  Bounds_get_extents_m1304537151 (Bounds_t2266837910 * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Vector3::get_magnitude()
extern "C" IL2CPP_METHOD_ATTR float Vector3_get_magnitude_m27958459 (Vector3_t3722313464 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_Subtraction(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  Vector3_op_Subtraction_m3073674971 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, Vector3_t3722313464  p1, const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TriLib.TextureLoadHandle::.ctor(System.Object,System.IntPtr)
extern "C" IL2CPP_METHOD_ATTR void TextureLoadHandle__ctor_m1301794767 (TextureLoadHandle_t2346000395 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void TriLib.TextureLoadHandle::Invoke(System.String,UnityEngine.Material,System.String,UnityEngine.Texture2D)
extern "C" IL2CPP_METHOD_ATTR void TextureLoadHandle_Invoke_m1791263530 (TextureLoadHandle_t2346000395 * __this, String_t* ___sourcePath0, Material_t340375123 * ___material1, String_t* ___propertyName2, Texture2D_t3840446185 * ___texture3, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		TextureLoadHandle_Invoke_m1791263530((TextureLoadHandle_t2346000395 *)__this->get_prev_9(), ___sourcePath0, ___material1, ___propertyName2, ___texture3, method);
	}
	Il2CppMethodPointer targetMethodPointer = __this->get_method_ptr_0();
	RuntimeMethod* targetMethod = (RuntimeMethod*)(__this->get_method_3());
	RuntimeObject* targetThis = __this->get_m_target_2();
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
	bool ___methodIsStatic = MethodIsStatic(targetMethod);
	if (___methodIsStatic)
	{
		if (il2cpp_codegen_method_parameter_count(targetMethod) == 4)
		{
			// open
			{
				typedef void (*FunctionPointerType) (RuntimeObject *, String_t*, Material_t340375123 *, String_t*, Texture2D_t3840446185 *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(NULL, ___sourcePath0, ___material1, ___propertyName2, ___texture3, targetMethod);
			}
		}
		else
		{
			// closed
			{
				typedef void (*FunctionPointerType) (RuntimeObject *, void*, String_t*, Material_t340375123 *, String_t*, Texture2D_t3840446185 *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(NULL, targetThis, ___sourcePath0, ___material1, ___propertyName2, ___texture3, targetMethod);
			}
		}
	}
	else
	{
		if (il2cpp_codegen_method_parameter_count(targetMethod) == 4)
		{
			// closed
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						GenericInterfaceActionInvoker4< String_t*, Material_t340375123 *, String_t*, Texture2D_t3840446185 * >::Invoke(targetMethod, targetThis, ___sourcePath0, ___material1, ___propertyName2, ___texture3);
					else
						GenericVirtActionInvoker4< String_t*, Material_t340375123 *, String_t*, Texture2D_t3840446185 * >::Invoke(targetMethod, targetThis, ___sourcePath0, ___material1, ___propertyName2, ___texture3);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						InterfaceActionInvoker4< String_t*, Material_t340375123 *, String_t*, Texture2D_t3840446185 * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___sourcePath0, ___material1, ___propertyName2, ___texture3);
					else
						VirtActionInvoker4< String_t*, Material_t340375123 *, String_t*, Texture2D_t3840446185 * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___sourcePath0, ___material1, ___propertyName2, ___texture3);
				}
			}
			else
			{
				typedef void (*FunctionPointerType) (void*, String_t*, Material_t340375123 *, String_t*, Texture2D_t3840446185 *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetThis, ___sourcePath0, ___material1, ___propertyName2, ___texture3, targetMethod);
			}
		}
		else
		{
			// open
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						GenericInterfaceActionInvoker3< Material_t340375123 *, String_t*, Texture2D_t3840446185 * >::Invoke(targetMethod, ___sourcePath0, ___material1, ___propertyName2, ___texture3);
					else
						GenericVirtActionInvoker3< Material_t340375123 *, String_t*, Texture2D_t3840446185 * >::Invoke(targetMethod, ___sourcePath0, ___material1, ___propertyName2, ___texture3);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						InterfaceActionInvoker3< Material_t340375123 *, String_t*, Texture2D_t3840446185 * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), ___sourcePath0, ___material1, ___propertyName2, ___texture3);
					else
						VirtActionInvoker3< Material_t340375123 *, String_t*, Texture2D_t3840446185 * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), ___sourcePath0, ___material1, ___propertyName2, ___texture3);
				}
			}
			else
			{
				typedef void (*FunctionPointerType) (String_t*, Material_t340375123 *, String_t*, Texture2D_t3840446185 *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(___sourcePath0, ___material1, ___propertyName2, ___texture3, targetMethod);
			}
		}
	}
}
// System.IAsyncResult TriLib.TextureLoadHandle::BeginInvoke(System.String,UnityEngine.Material,System.String,UnityEngine.Texture2D,System.AsyncCallback,System.Object)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* TextureLoadHandle_BeginInvoke_m2200083141 (TextureLoadHandle_t2346000395 * __this, String_t* ___sourcePath0, Material_t340375123 * ___material1, String_t* ___propertyName2, Texture2D_t3840446185 * ___texture3, AsyncCallback_t3962456242 * ___callback4, RuntimeObject * ___object5, const RuntimeMethod* method)
{
	void *__d_args[5] = {0};
	__d_args[0] = ___sourcePath0;
	__d_args[1] = ___material1;
	__d_args[2] = ___propertyName2;
	__d_args[3] = ___texture3;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback4, (RuntimeObject*)___object5);
}
// System.Void TriLib.TextureLoadHandle::EndInvoke(System.IAsyncResult)
extern "C" IL2CPP_METHOD_ATTR void TextureLoadHandle_EndInvoke_m1823782744 (TextureLoadHandle_t2346000395 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TriLib.TransformExtensions::LoadMatrix(UnityEngine.Transform,UnityEngine.Matrix4x4,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void TransformExtensions_LoadMatrix_m191976773 (RuntimeObject * __this /* static, unused */, Transform_t3600365921 * ___transform0, Matrix4x4_t1817901843  ___matrix1, bool ___local2, const RuntimeMethod* method)
{
	{
		bool L_0 = ___local2;
		if (!L_0)
		{
			goto IL_002f;
		}
	}
	{
		Transform_t3600365921 * L_1 = ___transform0;
		Matrix4x4_t1817901843  L_2 = ___matrix1;
		Vector3_t3722313464  L_3 = MatrixExtensions_ExtractScale_m3504230185(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		NullCheck(L_1);
		Transform_set_localScale_m3053443106(L_1, L_3, /*hidden argument*/NULL);
		Transform_t3600365921 * L_4 = ___transform0;
		Matrix4x4_t1817901843  L_5 = ___matrix1;
		Quaternion_t2301928331  L_6 = MatrixExtensions_ExtractRotation_m3933191427(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		NullCheck(L_4);
		Transform_set_localRotation_m19445462(L_4, L_6, /*hidden argument*/NULL);
		Transform_t3600365921 * L_7 = ___transform0;
		Matrix4x4_t1817901843  L_8 = ___matrix1;
		Vector3_t3722313464  L_9 = MatrixExtensions_ExtractPosition_m3825606610(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		NullCheck(L_7);
		Transform_set_localPosition_m4128471975(L_7, L_9, /*hidden argument*/NULL);
		goto IL_0047;
	}

IL_002f:
	{
		Transform_t3600365921 * L_10 = ___transform0;
		Matrix4x4_t1817901843  L_11 = ___matrix1;
		Quaternion_t2301928331  L_12 = MatrixExtensions_ExtractRotation_m3933191427(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		NullCheck(L_10);
		Transform_set_rotation_m3524318132(L_10, L_12, /*hidden argument*/NULL);
		Transform_t3600365921 * L_13 = ___transform0;
		Matrix4x4_t1817901843  L_14 = ___matrix1;
		Vector3_t3722313464  L_15 = MatrixExtensions_ExtractPosition_m3825606610(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		NullCheck(L_13);
		Transform_set_position_m3387557959(L_13, L_15, /*hidden argument*/NULL);
	}

IL_0047:
	{
		return;
	}
}
// UnityEngine.Bounds TriLib.TransformExtensions::EncapsulateBounds(UnityEngine.Transform)
extern "C" IL2CPP_METHOD_ATTR Bounds_t2266837910  TransformExtensions_EncapsulateBounds_m1814600766 (RuntimeObject * __this /* static, unused */, Transform_t3600365921 * ___transform0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TransformExtensions_EncapsulateBounds_m1814600766_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Bounds_t2266837910  V_0;
	memset(&V_0, 0, sizeof(V_0));
	RendererU5BU5D_t3210418286* V_1 = NULL;
	Renderer_t2627027031 * V_2 = NULL;
	RendererU5BU5D_t3210418286* V_3 = NULL;
	int32_t V_4 = 0;
	{
		il2cpp_codegen_initobj((&V_0), sizeof(Bounds_t2266837910 ));
		Transform_t3600365921 * L_0 = ___transform0;
		NullCheck(L_0);
		RendererU5BU5D_t3210418286* L_1 = Component_GetComponentsInChildren_TisRenderer_t2627027031_m2677127880(L_0, /*hidden argument*/Component_GetComponentsInChildren_TisRenderer_t2627027031_m2677127880_RuntimeMethod_var);
		V_1 = L_1;
		RendererU5BU5D_t3210418286* L_2 = V_1;
		if (!L_2)
		{
			goto IL_0041;
		}
	}
	{
		RendererU5BU5D_t3210418286* L_3 = V_1;
		V_3 = L_3;
		V_4 = 0;
		goto IL_0037;
	}

IL_001f:
	{
		RendererU5BU5D_t3210418286* L_4 = V_3;
		int32_t L_5 = V_4;
		NullCheck(L_4);
		int32_t L_6 = L_5;
		Renderer_t2627027031 * L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		V_2 = L_7;
		Renderer_t2627027031 * L_8 = V_2;
		NullCheck(L_8);
		Bounds_t2266837910  L_9 = Renderer_get_bounds_m1803204000(L_8, /*hidden argument*/NULL);
		Bounds_Encapsulate_m1263362003((Bounds_t2266837910 *)(&V_0), L_9, /*hidden argument*/NULL);
		int32_t L_10 = V_4;
		V_4 = ((int32_t)il2cpp_codegen_add((int32_t)L_10, (int32_t)1));
	}

IL_0037:
	{
		int32_t L_11 = V_4;
		RendererU5BU5D_t3210418286* L_12 = V_3;
		NullCheck(L_12);
		if ((((int32_t)L_11) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_12)->max_length)))))))
		{
			goto IL_001f;
		}
	}

IL_0041:
	{
		Bounds_t2266837910  L_13 = V_0;
		return L_13;
	}
}
// UnityEngine.Transform TriLib.TransformExtensions::FindDeepChild(UnityEngine.Transform,System.String,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR Transform_t3600365921 * TransformExtensions_FindDeepChild_m311219388 (RuntimeObject * __this /* static, unused */, Transform_t3600365921 * ___transform0, String_t* ___name1, bool ___endsWith2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TransformExtensions_FindDeepChild_m311219388_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Transform_t3600365921 * V_0 = NULL;
	RuntimeObject* V_1 = NULL;
	Transform_t3600365921 * V_2 = NULL;
	Transform_t3600365921 * V_3 = NULL;
	RuntimeObject* V_4 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = -1;
	NO_UNUSED_WARNING (__leave_target);
	bool G_B3_0 = false;
	{
		bool L_0 = ___endsWith2;
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		Transform_t3600365921 * L_1 = ___transform0;
		NullCheck(L_1);
		String_t* L_2 = Object_get_name_m4211327027(L_1, /*hidden argument*/NULL);
		String_t* L_3 = ___name1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_4 = String_op_Equality_m920492651(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		G_B3_0 = L_4;
		goto IL_0023;
	}

IL_0017:
	{
		Transform_t3600365921 * L_5 = ___transform0;
		NullCheck(L_5);
		String_t* L_6 = Object_get_name_m4211327027(L_5, /*hidden argument*/NULL);
		String_t* L_7 = ___name1;
		NullCheck(L_6);
		bool L_8 = String_EndsWith_m1901926500(L_6, L_7, /*hidden argument*/NULL);
		G_B3_0 = L_8;
	}

IL_0023:
	{
		if (!G_B3_0)
		{
			goto IL_002a;
		}
	}
	{
		Transform_t3600365921 * L_9 = ___transform0;
		return L_9;
	}

IL_002a:
	{
		Transform_t3600365921 * L_10 = ___transform0;
		NullCheck(L_10);
		RuntimeObject* L_11 = Transform_GetEnumerator_m2717073726(L_10, /*hidden argument*/NULL);
		V_1 = L_11;
	}

IL_0031:
	try
	{ // begin try (depth: 1)
		{
			goto IL_005e;
		}

IL_0036:
		{
			RuntimeObject* L_12 = V_1;
			NullCheck(L_12);
			RuntimeObject * L_13 = InterfaceFuncInvoker0< RuntimeObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t1853284238_il2cpp_TypeInfo_var, L_12);
			V_0 = ((Transform_t3600365921 *)CastclassClass((RuntimeObject*)L_13, Transform_t3600365921_il2cpp_TypeInfo_var));
			Transform_t3600365921 * L_14 = V_0;
			String_t* L_15 = ___name1;
			Transform_t3600365921 * L_16 = TransformExtensions_FindDeepChild_m311219388(NULL /*static, unused*/, L_14, L_15, (bool)0, /*hidden argument*/NULL);
			V_2 = L_16;
			Transform_t3600365921 * L_17 = V_2;
			IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
			bool L_18 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_17, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
			if (!L_18)
			{
				goto IL_005e;
			}
		}

IL_0057:
		{
			Transform_t3600365921 * L_19 = V_2;
			V_3 = L_19;
			IL2CPP_LEAVE(0x86, FINALLY_006e);
		}

IL_005e:
		{
			RuntimeObject* L_20 = V_1;
			NullCheck(L_20);
			bool L_21 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1853284238_il2cpp_TypeInfo_var, L_20);
			if (L_21)
			{
				goto IL_0036;
			}
		}

IL_0069:
		{
			IL2CPP_LEAVE(0x84, FINALLY_006e);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_006e;
	}

FINALLY_006e:
	{ // begin finally (depth: 1)
		{
			RuntimeObject* L_22 = V_1;
			RuntimeObject* L_23 = ((RuntimeObject*)IsInst((RuntimeObject*)L_22, IDisposable_t3640265483_il2cpp_TypeInfo_var));
			V_4 = L_23;
			if (!L_23)
			{
				goto IL_0083;
			}
		}

IL_007c:
		{
			RuntimeObject* L_24 = V_4;
			NullCheck(L_24);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t3640265483_il2cpp_TypeInfo_var, L_24);
		}

IL_0083:
		{
			IL2CPP_END_FINALLY(110)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(110)
	{
		IL2CPP_JUMP_TBL(0x86, IL_0086)
		IL2CPP_JUMP_TBL(0x84, IL_0084)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0084:
	{
		return (Transform_t3600365921 *)NULL;
	}

IL_0086:
	{
		Transform_t3600365921 * L_25 = V_3;
		return L_25;
	}
}
// System.Void TriLib.TransformExtensions::DestroyChildren(UnityEngine.Transform,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void TransformExtensions_DestroyChildren_m3219731900 (RuntimeObject * __this /* static, unused */, Transform_t3600365921 * ___transform0, bool ___destroyImmediate1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TransformExtensions_DestroyChildren_m3219731900_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Transform_t3600365921 * V_1 = NULL;
	{
		Transform_t3600365921 * L_0 = ___transform0;
		NullCheck(L_0);
		int32_t L_1 = Transform_get_childCount_m3145433196(L_0, /*hidden argument*/NULL);
		V_0 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_1, (int32_t)1));
		goto IL_003b;
	}

IL_000e:
	{
		Transform_t3600365921 * L_2 = ___transform0;
		int32_t L_3 = V_0;
		NullCheck(L_2);
		Transform_t3600365921 * L_4 = Transform_GetChild_m1092972975(L_2, L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		bool L_5 = ___destroyImmediate1;
		if (!L_5)
		{
			goto IL_002c;
		}
	}
	{
		Transform_t3600365921 * L_6 = V_1;
		NullCheck(L_6);
		GameObject_t1113636619 * L_7 = Component_get_gameObject_m442555142(L_6, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_DestroyImmediate_m3193525861(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		goto IL_0037;
	}

IL_002c:
	{
		Transform_t3600365921 * L_8 = V_1;
		NullCheck(L_8);
		GameObject_t1113636619 * L_9 = Component_get_gameObject_m442555142(L_8, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_Destroy_m565254235(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
	}

IL_0037:
	{
		int32_t L_10 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_10, (int32_t)1));
	}

IL_003b:
	{
		int32_t L_11 = V_0;
		if ((((int32_t)L_11) >= ((int32_t)0)))
		{
			goto IL_000e;
		}
	}
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TutoManager::.ctor()
extern "C" IL2CPP_METHOD_ATTR void TutoManager__ctor_m3835269636 (TutoManager_t4000134877 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TutoManager::Start()
extern "C" IL2CPP_METHOD_ATTR void TutoManager_Start_m2913216960 (TutoManager_t4000134877 * __this, const RuntimeMethod* method)
{
	{
		GameObject_t1113636619 * L_0 = __this->get_demo1_4();
		NullCheck(L_0);
		GameObject_SetActive_m796801857(L_0, (bool)1, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_1 = __this->get_demo2_5();
		NullCheck(L_1);
		GameObject_SetActive_m796801857(L_1, (bool)0, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_2 = __this->get_demo3_6();
		NullCheck(L_2);
		GameObject_SetActive_m796801857(L_2, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TutoManager::backToMenu()
extern "C" IL2CPP_METHOD_ATTR void TutoManager_backToMenu_m2565265301 (TutoManager_t4000134877 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TutoManager_backToMenu_m2565265301_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		SceneManager_LoadScene_m1758133949(NULL /*static, unused*/, _stringLiteral2526170751, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TutoManager::gotoDemo2()
extern "C" IL2CPP_METHOD_ATTR void TutoManager_gotoDemo2_m886691602 (TutoManager_t4000134877 * __this, const RuntimeMethod* method)
{
	{
		GameObject_t1113636619 * L_0 = __this->get_demo1_4();
		NullCheck(L_0);
		GameObject_SetActive_m796801857(L_0, (bool)0, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_1 = __this->get_demo2_5();
		NullCheck(L_1);
		GameObject_SetActive_m796801857(L_1, (bool)1, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_2 = __this->get_demo3_6();
		NullCheck(L_2);
		GameObject_SetActive_m796801857(L_2, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TutoManager::gotoDemo3()
extern "C" IL2CPP_METHOD_ATTR void TutoManager_gotoDemo3_m886626066 (TutoManager_t4000134877 * __this, const RuntimeMethod* method)
{
	{
		GameObject_t1113636619 * L_0 = __this->get_demo1_4();
		NullCheck(L_0);
		GameObject_SetActive_m796801857(L_0, (bool)0, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_1 = __this->get_demo2_5();
		NullCheck(L_1);
		GameObject_SetActive_m796801857(L_1, (bool)0, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_2 = __this->get_demo3_6();
		NullCheck(L_2);
		GameObject_SetActive_m796801857(L_2, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TutoManager::launchAR_AUTO()
extern "C" IL2CPP_METHOD_ATTR void TutoManager_launchAR_AUTO_m862194551 (TutoManager_t4000134877 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TutoManager_launchAR_AUTO_m862194551_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		SceneManager_LoadScene_m1758133949(NULL /*static, unused*/, _stringLiteral1884250374, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TutoManager::launchURL()
extern "C" IL2CPP_METHOD_ATTR void TutoManager_launchURL_m2867535317 (TutoManager_t4000134877 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_url_7();
		Application_OpenURL_m738341736(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UIManager::.ctor()
extern "C" IL2CPP_METHOD_ATTR void UIManager__ctor_m873742767 (UIManager_t1042050227 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
extern "C" void DEFAULT_CALL _ReturnToNative();
// System.Void UIManager::_ReturnToNative()
extern "C" IL2CPP_METHOD_ATTR void UIManager__ReturnToNative_m374797032 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_ReturnToNative)();

}
// System.Void UIManager::Start()
extern "C" IL2CPP_METHOD_ATTR void UIManager_Start_m1211894305 (UIManager_t1042050227 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIManager_Start_m1211894305_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		GameObject_t1113636619 * L_0 = __this->get_credit_6();
		NullCheck(L_0);
		Animator_t434523843 * L_1 = GameObject_GetComponent_TisAnimator_t434523843_m440019408(L_0, /*hidden argument*/GameObject_GetComponent_TisAnimator_t434523843_m440019408_RuntimeMethod_var);
		NullCheck(L_1);
		Animator_set_speed_m1181320995(L_1, (0.0f), /*hidden argument*/NULL);
		GameObject_t1113636619 * L_2 = __this->get_tutoButton_5();
		NullCheck(L_2);
		Animator_t434523843 * L_3 = GameObject_GetComponent_TisAnimator_t434523843_m440019408(L_2, /*hidden argument*/GameObject_GetComponent_TisAnimator_t434523843_m440019408_RuntimeMethod_var);
		NullCheck(L_3);
		Animator_set_speed_m1181320995(L_3, (0.0f), /*hidden argument*/NULL);
		GameObject_t1113636619 * L_4 = __this->get_tuto_4();
		NullCheck(L_4);
		GameObject_SetActive_m796801857(L_4, (bool)0, /*hidden argument*/NULL);
		int32_t L_5 = PlayerPrefs_GetInt_m1299643124(NULL /*static, unused*/, _stringLiteral1512728895, 0, /*hidden argument*/NULL);
		V_0 = L_5;
		int32_t L_6 = V_0;
		if (L_6)
		{
			goto IL_0078;
		}
	}
	{
		PlayerPrefs_SetInt_m2842000469(NULL /*static, unused*/, _stringLiteral1512728895, 1, /*hidden argument*/NULL);
		PlayerPrefs_Save_m2701929462(NULL /*static, unused*/, /*hidden argument*/NULL);
		MonoBehaviour_Invoke_m4227543964(__this, _stringLiteral10537912, (1.5f), /*hidden argument*/NULL);
		MonoBehaviour_Invoke_m4227543964(__this, _stringLiteral1615931692, (3.0f), /*hidden argument*/NULL);
	}

IL_0078:
	{
		return;
	}
}
// System.Void UIManager::OnClickMenu()
extern "C" IL2CPP_METHOD_ATTR void UIManager_OnClickMenu_m710568917 (UIManager_t1042050227 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIManager_OnClickMenu_m710568917_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_panelOpen_11();
		if (!L_0)
		{
			goto IL_0041;
		}
	}
	{
		GameObject_t1113636619 * L_1 = __this->get_Menu_8();
		NullCheck(L_1);
		Animator_t434523843 * L_2 = GameObject_GetComponent_TisAnimator_t434523843_m440019408(L_1, /*hidden argument*/GameObject_GetComponent_TisAnimator_t434523843_m440019408_RuntimeMethod_var);
		NullCheck(L_2);
		Animator_Play_m1697843332(L_2, _stringLiteral2929551439, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_3 = __this->get_mainPanel_9();
		NullCheck(L_3);
		Animator_t434523843 * L_4 = GameObject_GetComponent_TisAnimator_t434523843_m440019408(L_3, /*hidden argument*/GameObject_GetComponent_TisAnimator_t434523843_m440019408_RuntimeMethod_var);
		NullCheck(L_4);
		Animator_Play_m1697843332(L_4, _stringLiteral3495511934, /*hidden argument*/NULL);
		__this->set_panelOpen_11((bool)0);
		goto IL_0072;
	}

IL_0041:
	{
		GameObject_t1113636619 * L_5 = __this->get_Menu_8();
		NullCheck(L_5);
		Animator_t434523843 * L_6 = GameObject_GetComponent_TisAnimator_t434523843_m440019408(L_5, /*hidden argument*/GameObject_GetComponent_TisAnimator_t434523843_m440019408_RuntimeMethod_var);
		NullCheck(L_6);
		Animator_Play_m1697843332(L_6, _stringLiteral688097028, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_7 = __this->get_mainPanel_9();
		NullCheck(L_7);
		Animator_t434523843 * L_8 = GameObject_GetComponent_TisAnimator_t434523843_m440019408(L_7, /*hidden argument*/GameObject_GetComponent_TisAnimator_t434523843_m440019408_RuntimeMethod_var);
		NullCheck(L_8);
		Animator_Play_m1697843332(L_8, _stringLiteral589637722, /*hidden argument*/NULL);
		__this->set_panelOpen_11((bool)1);
	}

IL_0072:
	{
		return;
	}
}
// System.Void UIManager::OnTutoClicked()
extern "C" IL2CPP_METHOD_ATTR void UIManager_OnTutoClicked_m3350042246 (UIManager_t1042050227 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIManager_OnTutoClicked_m3350042246_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_creditOpen_12();
		if (!L_0)
		{
			goto IL_003c;
		}
	}
	{
		GameObject_t1113636619 * L_1 = __this->get_credit_6();
		NullCheck(L_1);
		Animator_t434523843 * L_2 = GameObject_GetComponent_TisAnimator_t434523843_m440019408(L_1, /*hidden argument*/GameObject_GetComponent_TisAnimator_t434523843_m440019408_RuntimeMethod_var);
		NullCheck(L_2);
		Animator_set_speed_m1181320995(L_2, (0.0f), /*hidden argument*/NULL);
		GameObject_t1113636619 * L_3 = __this->get_creditPanel_7();
		NullCheck(L_3);
		Animator_t434523843 * L_4 = GameObject_GetComponent_TisAnimator_t434523843_m440019408(L_3, /*hidden argument*/GameObject_GetComponent_TisAnimator_t434523843_m440019408_RuntimeMethod_var);
		NullCheck(L_4);
		Animator_Play_m1697843332(L_4, _stringLiteral3129749765, /*hidden argument*/NULL);
		__this->set_creditOpen_12((bool)0);
	}

IL_003c:
	{
		GameObject_t1113636619 * L_5 = __this->get_tuto_4();
		NullCheck(L_5);
		GameObject_SetActive_m796801857(L_5, (bool)1, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_6 = __this->get_tutoButton_5();
		NullCheck(L_6);
		Animator_t434523843 * L_7 = GameObject_GetComponent_TisAnimator_t434523843_m440019408(L_6, /*hidden argument*/GameObject_GetComponent_TisAnimator_t434523843_m440019408_RuntimeMethod_var);
		NullCheck(L_7);
		Animator_set_speed_m1181320995(L_7, (1.0f), /*hidden argument*/NULL);
		PageTransition_t3158442731 * L_8 = __this->get_pageTransition_10();
		NullCheck(L_8);
		PageTransition_Init_m631843422(L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UIManager::OnTutoClosed()
extern "C" IL2CPP_METHOD_ATTR void UIManager_OnTutoClosed_m3625715984 (UIManager_t1042050227 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIManager_OnTutoClosed_m3625715984_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1113636619 * L_0 = __this->get_tutoButton_5();
		NullCheck(L_0);
		Animator_t434523843 * L_1 = GameObject_GetComponent_TisAnimator_t434523843_m440019408(L_0, /*hidden argument*/GameObject_GetComponent_TisAnimator_t434523843_m440019408_RuntimeMethod_var);
		NullCheck(L_1);
		Animator_set_speed_m1181320995(L_1, (0.0f), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UIManager::OnCreditClicked()
extern "C" IL2CPP_METHOD_ATTR void UIManager_OnCreditClicked_m1737799703 (UIManager_t1042050227 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIManager_OnCreditClicked_m1737799703_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_creditOpen_12();
		if (!L_0)
		{
			goto IL_0041;
		}
	}
	{
		GameObject_t1113636619 * L_1 = __this->get_credit_6();
		NullCheck(L_1);
		Animator_t434523843 * L_2 = GameObject_GetComponent_TisAnimator_t434523843_m440019408(L_1, /*hidden argument*/GameObject_GetComponent_TisAnimator_t434523843_m440019408_RuntimeMethod_var);
		NullCheck(L_2);
		Animator_set_speed_m1181320995(L_2, (0.0f), /*hidden argument*/NULL);
		GameObject_t1113636619 * L_3 = __this->get_creditPanel_7();
		NullCheck(L_3);
		Animator_t434523843 * L_4 = GameObject_GetComponent_TisAnimator_t434523843_m440019408(L_3, /*hidden argument*/GameObject_GetComponent_TisAnimator_t434523843_m440019408_RuntimeMethod_var);
		NullCheck(L_4);
		Animator_Play_m1697843332(L_4, _stringLiteral3129749765, /*hidden argument*/NULL);
		__this->set_creditOpen_12((bool)0);
		goto IL_0084;
	}

IL_0041:
	{
		GameObject_t1113636619 * L_5 = __this->get_tuto_4();
		NullCheck(L_5);
		GameObject_SetActive_m796801857(L_5, (bool)0, /*hidden argument*/NULL);
		UIManager_OnTutoClosed_m3625715984(__this, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_6 = __this->get_credit_6();
		NullCheck(L_6);
		Animator_t434523843 * L_7 = GameObject_GetComponent_TisAnimator_t434523843_m440019408(L_6, /*hidden argument*/GameObject_GetComponent_TisAnimator_t434523843_m440019408_RuntimeMethod_var);
		NullCheck(L_7);
		Animator_set_speed_m1181320995(L_7, (1.0f), /*hidden argument*/NULL);
		GameObject_t1113636619 * L_8 = __this->get_creditPanel_7();
		NullCheck(L_8);
		Animator_t434523843 * L_9 = GameObject_GetComponent_TisAnimator_t434523843_m440019408(L_8, /*hidden argument*/GameObject_GetComponent_TisAnimator_t434523843_m440019408_RuntimeMethod_var);
		NullCheck(L_9);
		Animator_Play_m1697843332(L_9, _stringLiteral79347, /*hidden argument*/NULL);
		__this->set_creditOpen_12((bool)1);
	}

IL_0084:
	{
		return;
	}
}
// System.Void UIManager::OnQuit()
extern "C" IL2CPP_METHOD_ATTR void UIManager_OnQuit_m447715357 (UIManager_t1042050227 * __this, const RuntimeMethod* method)
{
	{
		UIManager__ReturnToNative_m374797032(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UIManager::Update()
extern "C" IL2CPP_METHOD_ATTR void UIManager_Update_m1085197836 (UIManager_t1042050227 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIManager_Update_m1085197836_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = Application_get_platform_m2150679437(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)11)))))
		{
			goto IL_001e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		bool L_1 = Input_GetKey_m3736388334(NULL /*static, unused*/, ((int32_t)27), /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001e;
		}
	}
	{
		UIManager_OnQuit_m447715357(__this, /*hidden argument*/NULL);
	}

IL_001e:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ZoomObject::.ctor()
extern "C" IL2CPP_METHOD_ATTR void ZoomObject__ctor_m1819632538 (ZoomObject_t2903076344 * __this, const RuntimeMethod* method)
{
	{
		__this->set_zoom_10((1.0f));
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ZoomObject::Awake()
extern "C" IL2CPP_METHOD_ATTR void ZoomObject_Awake_m1123788873 (ZoomObject_t2903076344 * __this, const RuntimeMethod* method)
{
	{
		Transform_t3600365921 * L_0 = __this->get_target_6();
		NullCheck(L_0);
		Vector3_t3722313464  L_1 = Transform_get_localScale_m129152068(L_0, /*hidden argument*/NULL);
		__this->set_originalZoom_7(L_1);
		Vector3_t3722313464 * L_2 = __this->get_address_of_originalZoom_7();
		float L_3 = L_2->get_x_2();
		__this->set_maxZoomIn_4(L_3);
		return;
	}
}
// System.Void ZoomObject::Reset()
extern "C" IL2CPP_METHOD_ATTR void ZoomObject_Reset_m3333110601 (ZoomObject_t2903076344 * __this, const RuntimeMethod* method)
{
	{
		float L_0 = __this->get_maxZoomIn_4();
		__this->set_zoom_10(L_0);
		__this->set_delta_9((0.0f));
		Transform_t3600365921 * L_1 = __this->get_target_6();
		Vector3_t3722313464  L_2 = __this->get_originalZoom_7();
		NullCheck(L_1);
		Transform_set_localScale_m3053443106(L_1, L_2, /*hidden argument*/NULL);
		__this->set_isZooming_11((bool)0);
		return;
	}
}
// System.Void ZoomObject::Update()
extern "C" IL2CPP_METHOD_ATTR void ZoomObject_Update_m3507186912 (ZoomObject_t2903076344 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ZoomObject_Update_m3507186912_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Touch_t1921856868  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Touch_t1921856868  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Touch_t1921856868  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Touch_t1921856868  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector2_t2156229523  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Vector2_t2156229523  V_5;
	memset(&V_5, 0, sizeof(V_5));
	float V_6 = 0.0f;
	Vector2_t2156229523  V_7;
	memset(&V_7, 0, sizeof(V_7));
	float V_8 = 0.0f;
	Vector2_t2156229523  V_9;
	memset(&V_9, 0, sizeof(V_9));
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		int32_t L_0 = Input_get_touchCount_m3403849067(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)2))))
		{
			goto IL_0154;
		}
	}
	{
		bool L_1 = __this->get_isZooming_11();
		if (L_1)
		{
			goto IL_001d;
		}
	}
	{
		__this->set_isZooming_11((bool)1);
	}

IL_001d:
	{
		bool L_2 = __this->get_isZooming_11();
		if (!L_2)
		{
			goto IL_014f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		Touch_t1921856868  L_3 = Input_GetTouch_m2192712756(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_0 = L_3;
		Vector2_t2156229523  L_4 = Touch_get_deltaPosition_m2389653382((Touch_t1921856868 *)(&V_0), /*hidden argument*/NULL);
		Vector2_t2156229523  L_5 = L_4;
		RuntimeObject * L_6 = Box(Vector2_t2156229523_il2cpp_TypeInfo_var, &L_5);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = String_Concat_m904156431(NULL /*static, unused*/, _stringLiteral3017359863, L_6, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		Touch_t1921856868  L_8 = Input_GetTouch_m2192712756(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		V_1 = L_8;
		Vector2_t2156229523  L_9 = Touch_get_deltaPosition_m2389653382((Touch_t1921856868 *)(&V_1), /*hidden argument*/NULL);
		Vector2_t2156229523  L_10 = L_9;
		RuntimeObject * L_11 = Box(Vector2_t2156229523_il2cpp_TypeInfo_var, &L_10);
		String_t* L_12 = String_Concat_m904156431(NULL /*static, unused*/, _stringLiteral3698539982, L_11, /*hidden argument*/NULL);
		Debug_Log_m4051431634(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		Touch_t1921856868  L_13 = Input_GetTouch_m2192712756(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_2 = L_13;
		Touch_t1921856868  L_14 = Input_GetTouch_m2192712756(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		V_3 = L_14;
		Vector2_t2156229523  L_15 = Touch_get_position_m3109777936((Touch_t1921856868 *)(&V_2), /*hidden argument*/NULL);
		Vector2_t2156229523  L_16 = Touch_get_deltaPosition_m2389653382((Touch_t1921856868 *)(&V_2), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_17 = Vector2_op_Subtraction_m73004381(NULL /*static, unused*/, L_15, L_16, /*hidden argument*/NULL);
		V_4 = L_17;
		Vector2_t2156229523  L_18 = Touch_get_position_m3109777936((Touch_t1921856868 *)(&V_3), /*hidden argument*/NULL);
		Vector2_t2156229523  L_19 = Touch_get_deltaPosition_m2389653382((Touch_t1921856868 *)(&V_3), /*hidden argument*/NULL);
		Vector2_t2156229523  L_20 = Vector2_op_Subtraction_m73004381(NULL /*static, unused*/, L_18, L_19, /*hidden argument*/NULL);
		V_5 = L_20;
		Vector2_t2156229523  L_21 = V_4;
		Vector2_t2156229523  L_22 = V_5;
		Vector2_t2156229523  L_23 = Vector2_op_Subtraction_m73004381(NULL /*static, unused*/, L_21, L_22, /*hidden argument*/NULL);
		V_7 = L_23;
		float L_24 = Vector2_get_magnitude_m2752892833((Vector2_t2156229523 *)(&V_7), /*hidden argument*/NULL);
		V_6 = L_24;
		Vector2_t2156229523  L_25 = Touch_get_position_m3109777936((Touch_t1921856868 *)(&V_2), /*hidden argument*/NULL);
		Vector2_t2156229523  L_26 = Touch_get_position_m3109777936((Touch_t1921856868 *)(&V_3), /*hidden argument*/NULL);
		Vector2_t2156229523  L_27 = Vector2_op_Subtraction_m73004381(NULL /*static, unused*/, L_25, L_26, /*hidden argument*/NULL);
		V_9 = L_27;
		float L_28 = Vector2_get_magnitude_m2752892833((Vector2_t2156229523 *)(&V_9), /*hidden argument*/NULL);
		V_8 = L_28;
		float L_29 = V_6;
		float L_30 = V_8;
		__this->set_delta_9(((float)il2cpp_codegen_subtract((float)L_29, (float)L_30)));
		float L_31 = __this->get_delta_9();
		if ((((float)L_31) == ((float)(0.0f))))
		{
			goto IL_014f;
		}
	}
	{
		float L_32 = __this->get_zoom_10();
		float L_33 = __this->get_delta_9();
		float L_34 = Time_get_deltaTime_m372706562(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_zoom_10(((float)il2cpp_codegen_subtract((float)L_32, (float)((float)((float)((float)il2cpp_codegen_multiply((float)L_33, (float)L_34))/(float)(5.0f))))));
		float L_35 = __this->get_zoom_10();
		float L_36 = __this->get_maxZoomIn_4();
		float L_37 = __this->get_maxZoomOut_5();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_38 = Mathf_Clamp_m3350697880(NULL /*static, unused*/, L_35, L_36, L_37, /*hidden argument*/NULL);
		__this->set_zoom_10(L_38);
		Transform_t3600365921 * L_39 = __this->get_target_6();
		float L_40 = __this->get_zoom_10();
		float L_41 = __this->get_zoom_10();
		float L_42 = __this->get_zoom_10();
		Vector3_t3722313464  L_43;
		memset(&L_43, 0, sizeof(L_43));
		Vector3__ctor_m3353183577((&L_43), L_40, L_41, L_42, /*hidden argument*/NULL);
		NullCheck(L_39);
		Transform_set_localScale_m3053443106(L_39, L_43, /*hidden argument*/NULL);
	}

IL_014f:
	{
		goto IL_015b;
	}

IL_0154:
	{
		__this->set_isZooming_11((bool)0);
	}

IL_015b:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void animationImage::.ctor()
extern "C" IL2CPP_METHOD_ATTR void animationImage__ctor_m2784108477 (animationImage_t1423068421 * __this, const RuntimeMethod* method)
{
	{
		__this->set_pixelByUnity_12((100.0f));
		__this->set_nbAfficher_20(1);
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void animationImage::awake()
extern "C" IL2CPP_METHOD_ATTR void animationImage_awake_m3575752642 (animationImage_t1423068421 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (animationImage_awake_m3575752642_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		SpriteRenderer_t3235626157 * L_0 = Component_GetComponent_TisSpriteRenderer_t3235626157_m2253180502(__this, /*hidden argument*/Component_GetComponent_TisSpriteRenderer_t3235626157_m2253180502_RuntimeMethod_var);
		NullCheck(L_0);
		Renderer_set_enabled_m1727253150(L_0, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void animationImage::Start()
extern "C" IL2CPP_METHOD_ATTR void animationImage_Start_m2640814523 (animationImage_t1423068421 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (animationImage_Start_m2640814523_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		SpriteRenderer_t3235626157 * L_0 = Component_GetComponent_TisSpriteRenderer_t3235626157_m2253180502(__this, /*hidden argument*/Component_GetComponent_TisSpriteRenderer_t3235626157_m2253180502_RuntimeMethod_var);
		NullCheck(L_0);
		Renderer_set_enabled_m1727253150(L_0, (bool)0, /*hidden argument*/NULL);
		Color_t2555686324  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Color__ctor_m2943235014((&L_1), (0.0f), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		__this->set_C_11(L_1);
		__this->set_animationFinie_16((bool)0);
		__this->set_affichageFini_17((bool)0);
		animationImage_DemarerAnimation_m273565184(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void animationImage::Update()
extern "C" IL2CPP_METHOD_ATTR void animationImage_Update_m2509067233 (animationImage_t1423068421 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_affichageFini_17();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		int32_t L_1 = __this->get_ChoixAnimation_15();
		animationImage_AnimationOuverture_m2423226750(__this, L_1, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void animationImage::AnimationOuverture(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void animationImage_AnimationOuverture_m2423226750 (animationImage_t1423068421 * __this, int32_t ___choix0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (animationImage_AnimationOuverture_m2423226750_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	{
		bool L_0 = __this->get_animationFinie_16();
		if (L_0)
		{
			goto IL_02d3;
		}
	}
	{
		int32_t L_1 = ___choix0;
		switch (((int32_t)il2cpp_codegen_subtract((int32_t)L_1, (int32_t)1)))
		{
			case 0:
			{
				goto IL_0028;
			}
			case 1:
			{
				goto IL_011f;
			}
			case 2:
			{
				goto IL_01be;
			}
			case 3:
			{
				goto IL_02c3;
			}
		}
	}
	{
		goto IL_02ce;
	}

IL_0028:
	{
		int32_t L_2 = __this->get_nbPix_19();
		List_1_t4186031580 * L_3 = __this->get_listePixels_13();
		NullCheck(L_3);
		int32_t L_4 = List_1_get_Count_m334549794(L_3, /*hidden argument*/List_1_get_Count_m334549794_RuntimeMethod_var);
		__this->set_nbAfficher_20(((int32_t)il2cpp_codegen_add((int32_t)(((int32_t)((int32_t)((float)((float)(((float)((float)((int32_t)il2cpp_codegen_subtract((int32_t)L_2, (int32_t)L_4)))))/(float)(10.0f)))))), (int32_t)2)));
		List_1_t4186031580 * L_5 = __this->get_listePixels_13();
		NullCheck(L_5);
		int32_t L_6 = List_1_get_Count_m334549794(L_5, /*hidden argument*/List_1_get_Count_m334549794_RuntimeMethod_var);
		if ((((int32_t)L_6) <= ((int32_t)0)))
		{
			goto IL_0107;
		}
	}
	{
		V_0 = 0;
		goto IL_00db;
	}

IL_0062:
	{
		List_1_t4186031580 * L_7 = __this->get_listePixels_13();
		NullCheck(L_7);
		int32_t L_8 = List_1_get_Count_m334549794(L_7, /*hidden argument*/List_1_get_Count_m334549794_RuntimeMethod_var);
		int32_t L_9 = Random_Range_m4054026115(NULL /*static, unused*/, 0, L_8, /*hidden argument*/NULL);
		__this->set_numPix_18(L_9);
		Texture2D_t3840446185 * L_10 = __this->get_tex_4();
		List_1_t4186031580 * L_11 = __this->get_listePixels_13();
		int32_t L_12 = __this->get_numPix_18();
		NullCheck(L_11);
		Pixel_t2713956838 * L_13 = List_1_get_Item_m1974214454(L_11, L_12, /*hidden argument*/List_1_get_Item_m1974214454_RuntimeMethod_var);
		NullCheck(L_13);
		int32_t L_14 = L_13->get_i_0();
		List_1_t4186031580 * L_15 = __this->get_listePixels_13();
		int32_t L_16 = __this->get_numPix_18();
		NullCheck(L_15);
		Pixel_t2713956838 * L_17 = List_1_get_Item_m1974214454(L_15, L_16, /*hidden argument*/List_1_get_Item_m1974214454_RuntimeMethod_var);
		NullCheck(L_17);
		int32_t L_18 = L_17->get_j_1();
		List_1_t4186031580 * L_19 = __this->get_listePixels_13();
		int32_t L_20 = __this->get_numPix_18();
		NullCheck(L_19);
		Pixel_t2713956838 * L_21 = List_1_get_Item_m1974214454(L_19, L_20, /*hidden argument*/List_1_get_Item_m1974214454_RuntimeMethod_var);
		NullCheck(L_21);
		Color_t2555686324  L_22 = L_21->get_couleur_2();
		NullCheck(L_10);
		Texture2D_SetPixel_m2984741184(L_10, L_14, L_18, L_22, /*hidden argument*/NULL);
		List_1_t4186031580 * L_23 = __this->get_listePixels_13();
		int32_t L_24 = __this->get_numPix_18();
		NullCheck(L_23);
		List_1_RemoveAt_m1835037992(L_23, L_24, /*hidden argument*/List_1_RemoveAt_m1835037992_RuntimeMethod_var);
		int32_t L_25 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_25, (int32_t)1));
	}

IL_00db:
	{
		int32_t L_26 = V_0;
		List_1_t4186031580 * L_27 = __this->get_listePixels_13();
		NullCheck(L_27);
		int32_t L_28 = List_1_get_Count_m334549794(L_27, /*hidden argument*/List_1_get_Count_m334549794_RuntimeMethod_var);
		int32_t L_29 = __this->get_nbAfficher_20();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		int32_t L_30 = Mathf_Min_m18103608(NULL /*static, unused*/, L_28, L_29, /*hidden argument*/NULL);
		if ((((int32_t)L_26) < ((int32_t)L_30)))
		{
			goto IL_0062;
		}
	}
	{
		Texture2D_t3840446185 * L_31 = __this->get_tex_4();
		NullCheck(L_31);
		Texture2D_Apply_m2271746283(L_31, /*hidden argument*/NULL);
		goto IL_011a;
	}

IL_0107:
	{
		SpriteRenderer_t3235626157 * L_32 = Component_GetComponent_TisSpriteRenderer_t3235626157_m2253180502(__this, /*hidden argument*/Component_GetComponent_TisSpriteRenderer_t3235626157_m2253180502_RuntimeMethod_var);
		NullCheck(L_32);
		Renderer_set_enabled_m1727253150(L_32, (bool)1, /*hidden argument*/NULL);
		__this->set_animationFinie_16((bool)1);
	}

IL_011a:
	{
		goto IL_02ce;
	}

IL_011f:
	{
		List_1_t4186031580 * L_33 = __this->get_listePixels_13();
		NullCheck(L_33);
		int32_t L_34 = List_1_get_Count_m334549794(L_33, /*hidden argument*/List_1_get_Count_m334549794_RuntimeMethod_var);
		if ((((int32_t)L_34) <= ((int32_t)0)))
		{
			goto IL_01a6;
		}
	}
	{
		V_1 = 0;
		goto IL_0185;
	}

IL_0137:
	{
		Texture2D_t3840446185 * L_35 = __this->get_tex_4();
		List_1_t4186031580 * L_36 = __this->get_listePixels_13();
		NullCheck(L_36);
		Pixel_t2713956838 * L_37 = List_1_get_Item_m1974214454(L_36, 0, /*hidden argument*/List_1_get_Item_m1974214454_RuntimeMethod_var);
		NullCheck(L_37);
		int32_t L_38 = L_37->get_i_0();
		List_1_t4186031580 * L_39 = __this->get_listePixels_13();
		NullCheck(L_39);
		Pixel_t2713956838 * L_40 = List_1_get_Item_m1974214454(L_39, 0, /*hidden argument*/List_1_get_Item_m1974214454_RuntimeMethod_var);
		NullCheck(L_40);
		int32_t L_41 = L_40->get_j_1();
		List_1_t4186031580 * L_42 = __this->get_listePixels_13();
		NullCheck(L_42);
		Pixel_t2713956838 * L_43 = List_1_get_Item_m1974214454(L_42, 0, /*hidden argument*/List_1_get_Item_m1974214454_RuntimeMethod_var);
		NullCheck(L_43);
		Color_t2555686324  L_44 = L_43->get_couleur_2();
		NullCheck(L_35);
		Texture2D_SetPixel_m2984741184(L_35, L_38, L_41, L_44, /*hidden argument*/NULL);
		List_1_t4186031580 * L_45 = __this->get_listePixels_13();
		NullCheck(L_45);
		List_1_RemoveAt_m1835037992(L_45, 0, /*hidden argument*/List_1_RemoveAt_m1835037992_RuntimeMethod_var);
		int32_t L_46 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_46, (int32_t)1));
	}

IL_0185:
	{
		int32_t L_47 = V_1;
		Texture2D_t3840446185 * L_48 = __this->get_tex_4();
		NullCheck(L_48);
		int32_t L_49 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.Texture::get_height() */, L_48);
		if ((((int32_t)L_47) < ((int32_t)L_49)))
		{
			goto IL_0137;
		}
	}
	{
		Texture2D_t3840446185 * L_50 = __this->get_tex_4();
		NullCheck(L_50);
		Texture2D_Apply_m2271746283(L_50, /*hidden argument*/NULL);
		goto IL_01b9;
	}

IL_01a6:
	{
		SpriteRenderer_t3235626157 * L_51 = Component_GetComponent_TisSpriteRenderer_t3235626157_m2253180502(__this, /*hidden argument*/Component_GetComponent_TisSpriteRenderer_t3235626157_m2253180502_RuntimeMethod_var);
		NullCheck(L_51);
		Renderer_set_enabled_m1727253150(L_51, (bool)1, /*hidden argument*/NULL);
		__this->set_animationFinie_16((bool)1);
	}

IL_01b9:
	{
		goto IL_02ce;
	}

IL_01be:
	{
		List_1_t4186031580 * L_52 = __this->get_listePixels_13();
		NullCheck(L_52);
		int32_t L_53 = List_1_get_Count_m334549794(L_52, /*hidden argument*/List_1_get_Count_m334549794_RuntimeMethod_var);
		if ((((int32_t)L_53) <= ((int32_t)0)))
		{
			goto IL_02ab;
		}
	}
	{
		V_2 = 0;
		goto IL_0224;
	}

IL_01d6:
	{
		Texture2D_t3840446185 * L_54 = __this->get_tex_4();
		List_1_t4186031580 * L_55 = __this->get_listePixels_13();
		NullCheck(L_55);
		Pixel_t2713956838 * L_56 = List_1_get_Item_m1974214454(L_55, 0, /*hidden argument*/List_1_get_Item_m1974214454_RuntimeMethod_var);
		NullCheck(L_56);
		int32_t L_57 = L_56->get_i_0();
		List_1_t4186031580 * L_58 = __this->get_listePixels_13();
		NullCheck(L_58);
		Pixel_t2713956838 * L_59 = List_1_get_Item_m1974214454(L_58, 0, /*hidden argument*/List_1_get_Item_m1974214454_RuntimeMethod_var);
		NullCheck(L_59);
		int32_t L_60 = L_59->get_j_1();
		List_1_t4186031580 * L_61 = __this->get_listePixels_13();
		NullCheck(L_61);
		Pixel_t2713956838 * L_62 = List_1_get_Item_m1974214454(L_61, 0, /*hidden argument*/List_1_get_Item_m1974214454_RuntimeMethod_var);
		NullCheck(L_62);
		Color_t2555686324  L_63 = L_62->get_couleur_2();
		NullCheck(L_54);
		Texture2D_SetPixel_m2984741184(L_54, L_57, L_60, L_63, /*hidden argument*/NULL);
		List_1_t4186031580 * L_64 = __this->get_listePixels_13();
		NullCheck(L_64);
		List_1_RemoveAt_m1835037992(L_64, 0, /*hidden argument*/List_1_RemoveAt_m1835037992_RuntimeMethod_var);
		int32_t L_65 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_65, (int32_t)1));
	}

IL_0224:
	{
		int32_t L_66 = V_2;
		Texture2D_t3840446185 * L_67 = __this->get_tex_4();
		NullCheck(L_67);
		int32_t L_68 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.Texture::get_height() */, L_67);
		if ((((int32_t)L_66) < ((int32_t)L_68)))
		{
			goto IL_01d6;
		}
	}
	{
		V_3 = 0;
		goto IL_028a;
	}

IL_023c:
	{
		Texture2D_t3840446185 * L_69 = __this->get_tex_4();
		List_1_t4186031580 * L_70 = __this->get_listePixels_13();
		NullCheck(L_70);
		Pixel_t2713956838 * L_71 = List_1_get_Item_m1974214454(L_70, 0, /*hidden argument*/List_1_get_Item_m1974214454_RuntimeMethod_var);
		NullCheck(L_71);
		int32_t L_72 = L_71->get_i_0();
		List_1_t4186031580 * L_73 = __this->get_listePixels_13();
		NullCheck(L_73);
		Pixel_t2713956838 * L_74 = List_1_get_Item_m1974214454(L_73, 0, /*hidden argument*/List_1_get_Item_m1974214454_RuntimeMethod_var);
		NullCheck(L_74);
		int32_t L_75 = L_74->get_j_1();
		List_1_t4186031580 * L_76 = __this->get_listePixels_13();
		NullCheck(L_76);
		Pixel_t2713956838 * L_77 = List_1_get_Item_m1974214454(L_76, 0, /*hidden argument*/List_1_get_Item_m1974214454_RuntimeMethod_var);
		NullCheck(L_77);
		Color_t2555686324  L_78 = L_77->get_couleur_2();
		NullCheck(L_69);
		Texture2D_SetPixel_m2984741184(L_69, L_72, L_75, L_78, /*hidden argument*/NULL);
		List_1_t4186031580 * L_79 = __this->get_listePixels_13();
		NullCheck(L_79);
		List_1_RemoveAt_m1835037992(L_79, 0, /*hidden argument*/List_1_RemoveAt_m1835037992_RuntimeMethod_var);
		int32_t L_80 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add((int32_t)L_80, (int32_t)1));
	}

IL_028a:
	{
		int32_t L_81 = V_3;
		Texture2D_t3840446185 * L_82 = __this->get_tex_4();
		NullCheck(L_82);
		int32_t L_83 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.Texture::get_height() */, L_82);
		if ((((int32_t)L_81) < ((int32_t)L_83)))
		{
			goto IL_023c;
		}
	}
	{
		Texture2D_t3840446185 * L_84 = __this->get_tex_4();
		NullCheck(L_84);
		Texture2D_Apply_m2271746283(L_84, /*hidden argument*/NULL);
		goto IL_02be;
	}

IL_02ab:
	{
		SpriteRenderer_t3235626157 * L_85 = Component_GetComponent_TisSpriteRenderer_t3235626157_m2253180502(__this, /*hidden argument*/Component_GetComponent_TisSpriteRenderer_t3235626157_m2253180502_RuntimeMethod_var);
		NullCheck(L_85);
		Renderer_set_enabled_m1727253150(L_85, (bool)1, /*hidden argument*/NULL);
		__this->set_animationFinie_16((bool)1);
	}

IL_02be:
	{
		goto IL_02ce;
	}

IL_02c3:
	{
		animationImage_randomisationverticale_m1761082774(__this, /*hidden argument*/NULL);
		goto IL_02ce;
	}

IL_02ce:
	{
		goto IL_037b;
	}

IL_02d3:
	{
		V_4 = 0;
		goto IL_0342;
	}

IL_02db:
	{
		V_5 = 0;
		goto IL_032a;
	}

IL_02e3:
	{
		Texture2D_t3840446185 * L_86 = __this->get_tex_4();
		int32_t L_87 = V_4;
		int32_t L_88 = V_5;
		NullCheck(L_86);
		Color_t2555686324  L_89 = Texture2D_GetPixel_m1195410881(L_86, L_87, L_88, /*hidden argument*/NULL);
		__this->set_C_11(L_89);
		Color_t2555686324 * L_90 = __this->get_address_of_C_11();
		Color_t2555686324 * L_91 = L_90;
		float L_92 = L_91->get_a_3();
		L_91->set_a_3(((float)il2cpp_codegen_subtract((float)L_92, (float)(0.1f))));
		Texture2D_t3840446185 * L_93 = __this->get_tex_4();
		int32_t L_94 = V_4;
		int32_t L_95 = V_5;
		Color_t2555686324  L_96 = __this->get_C_11();
		NullCheck(L_93);
		Texture2D_SetPixel_m2984741184(L_93, L_94, L_95, L_96, /*hidden argument*/NULL);
		int32_t L_97 = V_5;
		V_5 = ((int32_t)il2cpp_codegen_add((int32_t)L_97, (int32_t)1));
	}

IL_032a:
	{
		int32_t L_98 = V_5;
		Texture2D_t3840446185 * L_99 = __this->get_tex_4();
		NullCheck(L_99);
		int32_t L_100 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.Texture::get_height() */, L_99);
		if ((((int32_t)L_98) < ((int32_t)L_100)))
		{
			goto IL_02e3;
		}
	}
	{
		int32_t L_101 = V_4;
		V_4 = ((int32_t)il2cpp_codegen_add((int32_t)L_101, (int32_t)1));
	}

IL_0342:
	{
		int32_t L_102 = V_4;
		Texture2D_t3840446185 * L_103 = __this->get_tex_4();
		NullCheck(L_103);
		int32_t L_104 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_103);
		if ((((int32_t)L_102) < ((int32_t)L_104)))
		{
			goto IL_02db;
		}
	}
	{
		Texture2D_t3840446185 * L_105 = __this->get_tex_4();
		NullCheck(L_105);
		Texture2D_Apply_m2271746283(L_105, /*hidden argument*/NULL);
		Color_t2555686324 * L_106 = __this->get_address_of_C_11();
		float L_107 = L_106->get_a_3();
		if ((!(((float)L_107) <= ((float)(0.0f)))))
		{
			goto IL_037b;
		}
	}
	{
		__this->set_affichageFini_17((bool)1);
	}

IL_037b:
	{
		return;
	}
}
// System.Void animationImage::DemarerAnimation()
extern "C" IL2CPP_METHOD_ATTR void animationImage_DemarerAnimation_m273565184 (animationImage_t1423068421 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (animationImage_DemarerAnimation_m273565184_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Texture2D_t3840446185 * V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	List_1_t4186031580 * V_6 = NULL;
	int32_t V_7 = 0;
	int32_t V_8 = 0;
	int32_t V_9 = 0;
	int32_t V_10 = 0;
	int32_t V_11 = 0;
	int32_t V_12 = 0;
	{
		GameObject_t1113636619 * L_0 = (GameObject_t1113636619 *)il2cpp_codegen_object_new(GameObject_t1113636619_il2cpp_TypeInfo_var);
		GameObject__ctor_m3707688467(L_0, /*hidden argument*/NULL);
		__this->set_newSpriteObj_9(L_0);
		GameObject_t1113636619 * L_1 = __this->get_newSpriteObj_9();
		NullCheck(L_1);
		Object_set_name_m291480324(L_1, _stringLiteral330296735, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_2 = __this->get_newSpriteObj_9();
		NullCheck(L_2);
		Transform_t3600365921 * L_3 = GameObject_get_transform_m1369836730(L_2, /*hidden argument*/NULL);
		Transform_t3600365921 * L_4 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		Transform_set_parent_m786917804(L_3, L_4, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_5 = __this->get_newSpriteObj_9();
		NullCheck(L_5);
		Transform_t3600365921 * L_6 = GameObject_get_transform_m1369836730(L_5, /*hidden argument*/NULL);
		Transform_t3600365921 * L_7 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		Vector3_t3722313464  L_8 = Transform_get_position_m36019626(L_7, /*hidden argument*/NULL);
		NullCheck(L_6);
		Transform_set_position_m3387557959(L_6, L_8, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_9 = __this->get_newSpriteObj_9();
		NullCheck(L_9);
		Transform_t3600365921 * L_10 = GameObject_get_transform_m1369836730(L_9, /*hidden argument*/NULL);
		Transform_t3600365921 * L_11 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_11);
		Quaternion_t2301928331  L_12 = Transform_get_rotation_m3502953881(L_11, /*hidden argument*/NULL);
		NullCheck(L_10);
		Transform_set_rotation_m3524318132(L_10, L_12, /*hidden argument*/NULL);
		Texture2D_t3840446185 * L_13 = __this->get_tex_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_14 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_13, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_0082;
		}
	}
	{
		MonoBehaviour_print_m330341231(NULL /*static, unused*/, _stringLiteral1392271312, /*hidden argument*/NULL);
	}

IL_0082:
	{
		ObjectU5BU5D_t2843939325* L_15 = (ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)4);
		ObjectU5BU5D_t2843939325* L_16 = L_15;
		NullCheck(L_16);
		ArrayElementTypeCheck (L_16, _stringLiteral2599195452);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)_stringLiteral2599195452);
		ObjectU5BU5D_t2843939325* L_17 = L_16;
		Texture2D_t3840446185 * L_18 = __this->get_tex_4();
		NullCheck(L_18);
		String_t* L_19 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_18);
		NullCheck(L_17);
		ArrayElementTypeCheck (L_17, L_19);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_19);
		ObjectU5BU5D_t2843939325* L_20 = L_17;
		NullCheck(L_20);
		ArrayElementTypeCheck (L_20, _stringLiteral3074750744);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)_stringLiteral3074750744);
		ObjectU5BU5D_t2843939325* L_21 = L_20;
		Texture2D_t3840446185 * L_22 = __this->get_tex_4();
		NullCheck(L_22);
		int32_t L_23 = Texture2D_get_format_m2371899271(L_22, /*hidden argument*/NULL);
		int32_t L_24 = L_23;
		RuntimeObject * L_25 = Box(TextureFormat_t2701165832_il2cpp_TypeInfo_var, &L_24);
		NullCheck(L_21);
		ArrayElementTypeCheck (L_21, L_25);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_25);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_26 = String_Concat_m2971454694(NULL /*static, unused*/, L_21, /*hidden argument*/NULL);
		MonoBehaviour_print_m330341231(NULL /*static, unused*/, L_26, /*hidden argument*/NULL);
		Texture2D_t3840446185 * L_27 = __this->get_tex_4();
		NullCheck(L_27);
		int32_t L_28 = Texture2D_get_format_m2371899271(L_27, /*hidden argument*/NULL);
		if ((((int32_t)L_28) == ((int32_t)4)))
		{
			goto IL_0110;
		}
	}
	{
		Texture2D_t3840446185 * L_29 = __this->get_tex_4();
		NullCheck(L_29);
		int32_t L_30 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_29);
		Texture2D_t3840446185 * L_31 = __this->get_tex_4();
		NullCheck(L_31);
		int32_t L_32 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.Texture::get_height() */, L_31);
		Texture2D_t3840446185 * L_33 = (Texture2D_t3840446185 *)il2cpp_codegen_object_new(Texture2D_t3840446185_il2cpp_TypeInfo_var);
		Texture2D__ctor_m2862217990(L_33, L_30, L_32, 4, (bool)0, /*hidden argument*/NULL);
		V_0 = L_33;
		Texture2D_t3840446185 * L_34 = V_0;
		Texture2D_t3840446185 * L_35 = __this->get_tex_4();
		NullCheck(L_35);
		ColorU5BU5D_t941916413* L_36 = Texture2D_GetPixels_m2081641574(L_35, /*hidden argument*/NULL);
		NullCheck(L_34);
		Texture2D_SetPixels_m3008871897(L_34, L_36, /*hidden argument*/NULL);
		Texture2D_t3840446185 * L_37 = V_0;
		NullCheck(L_37);
		Texture2D_Apply_m2271746283(L_37, /*hidden argument*/NULL);
		Texture2D_t3840446185 * L_38 = V_0;
		__this->set_tex_4(L_38);
	}

IL_0110:
	{
		Texture2D_t3840446185 * L_39 = __this->get_tex_4();
		Texture2D_t3840446185 * L_40 = __this->get_tex_4();
		NullCheck(L_40);
		int32_t L_41 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_40);
		Texture2D_t3840446185 * L_42 = __this->get_tex_4();
		NullCheck(L_42);
		int32_t L_43 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.Texture::get_height() */, L_42);
		Rect_t2360479859  L_44;
		memset(&L_44, 0, sizeof(L_44));
		Rect__ctor_m2614021312((&L_44), (0.0f), (0.0f), (((float)((float)L_41))), (((float)((float)L_43))), /*hidden argument*/NULL);
		Vector2_t2156229523  L_45;
		memset(&L_45, 0, sizeof(L_45));
		Vector2__ctor_m3970636864((&L_45), (0.5f), (0.5f), /*hidden argument*/NULL);
		float L_46 = __this->get_pixelByUnity_12();
		Sprite_t280657092 * L_47 = Sprite_Create_m3350529538(NULL /*static, unused*/, L_39, L_44, L_45, ((float)((float)L_46/(float)(10.0f))), /*hidden argument*/NULL);
		__this->set_mySprite_5(L_47);
		GameObject_t1113636619 * L_48 = __this->get_newSpriteObj_9();
		NullCheck(L_48);
		SpriteRenderer_t3235626157 * L_49 = GameObject_AddComponent_TisSpriteRenderer_t3235626157_m1939192903(L_48, /*hidden argument*/GameObject_AddComponent_TisSpriteRenderer_t3235626157_m1939192903_RuntimeMethod_var);
		__this->set_sr_7(L_49);
		SpriteRenderer_t3235626157 * L_50 = __this->get_sr_7();
		Sprite_t280657092 * L_51 = __this->get_mySprite_5();
		NullCheck(L_50);
		SpriteRenderer_set_sprite_m1286893786(L_50, L_51, /*hidden argument*/NULL);
		SpriteRenderer_t3235626157 * L_52 = __this->get_sr_7();
		Color_t2555686324  L_53;
		memset(&L_53, 0, sizeof(L_53));
		Color__ctor_m2943235014((&L_53), (1.0f), (1.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_52);
		SpriteRenderer_set_color_m3056777566(L_52, L_53, /*hidden argument*/NULL);
		SpriteRenderer_t3235626157 * L_54 = __this->get_sr_7();
		NullCheck(L_54);
		Renderer_set_sortingOrder_m549573253(L_54, 1, /*hidden argument*/NULL);
		List_1_t4186031580 * L_55 = (List_1_t4186031580 *)il2cpp_codegen_object_new(List_1_t4186031580_il2cpp_TypeInfo_var);
		List_1__ctor_m1545936499(L_55, /*hidden argument*/List_1__ctor_m1545936499_RuntimeMethod_var);
		__this->set_listePixels_13(L_55);
		int32_t L_56 = __this->get_ChoixAnimation_15();
		V_1 = L_56;
		int32_t L_57 = V_1;
		if ((((int32_t)L_57) == ((int32_t)3)))
		{
			goto IL_01da;
		}
	}
	{
		int32_t L_58 = V_1;
		if ((((int32_t)L_58) == ((int32_t)4)))
		{
			goto IL_0277;
		}
	}
	{
		goto IL_02ef;
	}

IL_01da:
	{
		V_2 = 1;
		goto IL_0220;
	}

IL_01e1:
	{
		V_3 = 0;
		goto IL_020b;
	}

IL_01e8:
	{
		List_1_t4186031580 * L_59 = __this->get_listePixels_13();
		int32_t L_60 = V_3;
		int32_t L_61 = V_2;
		Texture2D_t3840446185 * L_62 = __this->get_tex_4();
		int32_t L_63 = V_3;
		int32_t L_64 = V_2;
		NullCheck(L_62);
		Color_t2555686324  L_65 = Texture2D_GetPixel_m1195410881(L_62, L_63, L_64, /*hidden argument*/NULL);
		Pixel_t2713956838 * L_66 = (Pixel_t2713956838 *)il2cpp_codegen_object_new(Pixel_t2713956838_il2cpp_TypeInfo_var);
		Pixel__ctor_m3546726819(L_66, L_60, L_61, L_65, /*hidden argument*/NULL);
		NullCheck(L_59);
		List_1_Add_m1787940857(L_59, L_66, /*hidden argument*/List_1_Add_m1787940857_RuntimeMethod_var);
		int32_t L_67 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add((int32_t)L_67, (int32_t)1));
	}

IL_020b:
	{
		int32_t L_68 = V_3;
		Texture2D_t3840446185 * L_69 = __this->get_tex_4();
		NullCheck(L_69);
		int32_t L_70 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_69);
		if ((((int32_t)L_68) < ((int32_t)L_70)))
		{
			goto IL_01e8;
		}
	}
	{
		int32_t L_71 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_71, (int32_t)1));
	}

IL_0220:
	{
		int32_t L_72 = V_2;
		Texture2D_t3840446185 * L_73 = __this->get_tex_4();
		NullCheck(L_73);
		int32_t L_74 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.Texture::get_height() */, L_73);
		if ((((int32_t)L_72) < ((int32_t)L_74)))
		{
			goto IL_01e1;
		}
	}
	{
		V_4 = 0;
		goto IL_0260;
	}

IL_0239:
	{
		List_1_t4186031580 * L_75 = __this->get_listePixels_13();
		int32_t L_76 = V_4;
		Texture2D_t3840446185 * L_77 = __this->get_tex_4();
		int32_t L_78 = V_4;
		NullCheck(L_77);
		Color_t2555686324  L_79 = Texture2D_GetPixel_m1195410881(L_77, L_78, 0, /*hidden argument*/NULL);
		Pixel_t2713956838 * L_80 = (Pixel_t2713956838 *)il2cpp_codegen_object_new(Pixel_t2713956838_il2cpp_TypeInfo_var);
		Pixel__ctor_m3546726819(L_80, L_76, 0, L_79, /*hidden argument*/NULL);
		NullCheck(L_75);
		List_1_Add_m1787940857(L_75, L_80, /*hidden argument*/List_1_Add_m1787940857_RuntimeMethod_var);
		int32_t L_81 = V_4;
		V_4 = ((int32_t)il2cpp_codegen_add((int32_t)L_81, (int32_t)1));
	}

IL_0260:
	{
		int32_t L_82 = V_4;
		Texture2D_t3840446185 * L_83 = __this->get_tex_4();
		NullCheck(L_83);
		int32_t L_84 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_83);
		if ((((int32_t)L_82) < ((int32_t)L_84)))
		{
			goto IL_0239;
		}
	}
	{
		goto IL_0398;
	}

IL_0277:
	{
		V_5 = 0;
		goto IL_02d8;
	}

IL_027f:
	{
		List_1_t4186031580 * L_85 = (List_1_t4186031580 *)il2cpp_codegen_object_new(List_1_t4186031580_il2cpp_TypeInfo_var);
		List_1__ctor_m1545936499(L_85, /*hidden argument*/List_1__ctor_m1545936499_RuntimeMethod_var);
		V_6 = L_85;
		V_7 = 0;
		goto IL_02b3;
	}

IL_028e:
	{
		List_1_t4186031580 * L_86 = V_6;
		int32_t L_87 = V_7;
		int32_t L_88 = V_5;
		Texture2D_t3840446185 * L_89 = __this->get_tex_4();
		int32_t L_90 = V_7;
		int32_t L_91 = V_5;
		NullCheck(L_89);
		Color_t2555686324  L_92 = Texture2D_GetPixel_m1195410881(L_89, L_90, L_91, /*hidden argument*/NULL);
		Pixel_t2713956838 * L_93 = (Pixel_t2713956838 *)il2cpp_codegen_object_new(Pixel_t2713956838_il2cpp_TypeInfo_var);
		Pixel__ctor_m3546726819(L_93, L_87, L_88, L_92, /*hidden argument*/NULL);
		NullCheck(L_86);
		List_1_Add_m1787940857(L_86, L_93, /*hidden argument*/List_1_Add_m1787940857_RuntimeMethod_var);
		int32_t L_94 = V_7;
		V_7 = ((int32_t)il2cpp_codegen_add((int32_t)L_94, (int32_t)1));
	}

IL_02b3:
	{
		int32_t L_95 = V_7;
		Texture2D_t3840446185 * L_96 = __this->get_tex_4();
		NullCheck(L_96);
		int32_t L_97 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_96);
		if ((((int32_t)L_95) < ((int32_t)L_97)))
		{
			goto IL_028e;
		}
	}
	{
		List_1_t1363139026 * L_98 = __this->get_listeColPixels_14();
		List_1_t4186031580 * L_99 = V_6;
		NullCheck(L_98);
		List_1_Add_m530383596(L_98, L_99, /*hidden argument*/List_1_Add_m530383596_RuntimeMethod_var);
		int32_t L_100 = V_5;
		V_5 = ((int32_t)il2cpp_codegen_add((int32_t)L_100, (int32_t)1));
	}

IL_02d8:
	{
		int32_t L_101 = V_5;
		Texture2D_t3840446185 * L_102 = __this->get_tex_4();
		NullCheck(L_102);
		int32_t L_103 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.Texture::get_height() */, L_102);
		if ((((int32_t)L_101) < ((int32_t)L_103)))
		{
			goto IL_027f;
		}
	}
	{
		goto IL_0398;
	}

IL_02ef:
	{
		V_8 = 1;
		goto IL_0340;
	}

IL_02f7:
	{
		V_9 = 0;
		goto IL_0328;
	}

IL_02ff:
	{
		List_1_t4186031580 * L_104 = __this->get_listePixels_13();
		int32_t L_105 = V_8;
		int32_t L_106 = V_9;
		Texture2D_t3840446185 * L_107 = __this->get_tex_4();
		int32_t L_108 = V_8;
		int32_t L_109 = V_9;
		NullCheck(L_107);
		Color_t2555686324  L_110 = Texture2D_GetPixel_m1195410881(L_107, L_108, L_109, /*hidden argument*/NULL);
		Pixel_t2713956838 * L_111 = (Pixel_t2713956838 *)il2cpp_codegen_object_new(Pixel_t2713956838_il2cpp_TypeInfo_var);
		Pixel__ctor_m3546726819(L_111, L_105, L_106, L_110, /*hidden argument*/NULL);
		NullCheck(L_104);
		List_1_Add_m1787940857(L_104, L_111, /*hidden argument*/List_1_Add_m1787940857_RuntimeMethod_var);
		int32_t L_112 = V_9;
		V_9 = ((int32_t)il2cpp_codegen_add((int32_t)L_112, (int32_t)1));
	}

IL_0328:
	{
		int32_t L_113 = V_9;
		Texture2D_t3840446185 * L_114 = __this->get_tex_4();
		NullCheck(L_114);
		int32_t L_115 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.Texture::get_height() */, L_114);
		if ((((int32_t)L_113) < ((int32_t)L_115)))
		{
			goto IL_02ff;
		}
	}
	{
		int32_t L_116 = V_8;
		V_8 = ((int32_t)il2cpp_codegen_add((int32_t)L_116, (int32_t)1));
	}

IL_0340:
	{
		int32_t L_117 = V_8;
		Texture2D_t3840446185 * L_118 = __this->get_tex_4();
		NullCheck(L_118);
		int32_t L_119 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_118);
		if ((((int32_t)L_117) < ((int32_t)L_119)))
		{
			goto IL_02f7;
		}
	}
	{
		V_10 = 0;
		goto IL_0381;
	}

IL_035a:
	{
		List_1_t4186031580 * L_120 = __this->get_listePixels_13();
		int32_t L_121 = V_10;
		Texture2D_t3840446185 * L_122 = __this->get_tex_4();
		int32_t L_123 = V_10;
		NullCheck(L_122);
		Color_t2555686324  L_124 = Texture2D_GetPixel_m1195410881(L_122, 0, L_123, /*hidden argument*/NULL);
		Pixel_t2713956838 * L_125 = (Pixel_t2713956838 *)il2cpp_codegen_object_new(Pixel_t2713956838_il2cpp_TypeInfo_var);
		Pixel__ctor_m3546726819(L_125, 0, L_121, L_124, /*hidden argument*/NULL);
		NullCheck(L_120);
		List_1_Add_m1787940857(L_120, L_125, /*hidden argument*/List_1_Add_m1787940857_RuntimeMethod_var);
		int32_t L_126 = V_10;
		V_10 = ((int32_t)il2cpp_codegen_add((int32_t)L_126, (int32_t)1));
	}

IL_0381:
	{
		int32_t L_127 = V_10;
		Texture2D_t3840446185 * L_128 = __this->get_tex_4();
		NullCheck(L_128);
		int32_t L_129 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.Texture::get_height() */, L_128);
		if ((((int32_t)L_127) < ((int32_t)L_129)))
		{
			goto IL_035a;
		}
	}
	{
		goto IL_0398;
	}

IL_0398:
	{
		V_11 = 0;
		goto IL_03db;
	}

IL_03a0:
	{
		V_12 = 0;
		goto IL_03c3;
	}

IL_03a8:
	{
		Texture2D_t3840446185 * L_130 = __this->get_tex_4();
		int32_t L_131 = V_11;
		int32_t L_132 = V_12;
		Color_t2555686324  L_133 = __this->get_C_11();
		NullCheck(L_130);
		Texture2D_SetPixel_m2984741184(L_130, L_131, L_132, L_133, /*hidden argument*/NULL);
		int32_t L_134 = V_12;
		V_12 = ((int32_t)il2cpp_codegen_add((int32_t)L_134, (int32_t)1));
	}

IL_03c3:
	{
		int32_t L_135 = V_12;
		Texture2D_t3840446185 * L_136 = __this->get_tex_4();
		NullCheck(L_136);
		int32_t L_137 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.Texture::get_height() */, L_136);
		if ((((int32_t)L_135) < ((int32_t)L_137)))
		{
			goto IL_03a8;
		}
	}
	{
		int32_t L_138 = V_11;
		V_11 = ((int32_t)il2cpp_codegen_add((int32_t)L_138, (int32_t)1));
	}

IL_03db:
	{
		int32_t L_139 = V_11;
		Texture2D_t3840446185 * L_140 = __this->get_tex_4();
		NullCheck(L_140);
		int32_t L_141 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_140);
		if ((((int32_t)L_139) < ((int32_t)L_141)))
		{
			goto IL_03a0;
		}
	}
	{
		Texture2D_t3840446185 * L_142 = __this->get_tex_4();
		NullCheck(L_142);
		Texture2D_Apply_m2271746283(L_142, /*hidden argument*/NULL);
		List_1_t4186031580 * L_143 = __this->get_listePixels_13();
		NullCheck(L_143);
		int32_t L_144 = List_1_get_Count_m334549794(L_143, /*hidden argument*/List_1_get_Count_m334549794_RuntimeMethod_var);
		__this->set_nbPix_19(L_144);
		return;
	}
}
// System.Void animationImage::randomisationverticale()
extern "C" IL2CPP_METHOD_ATTR void animationImage_randomisationverticale_m1761082774 (animationImage_t1423068421 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void animationImage::OnDestroy()
extern "C" IL2CPP_METHOD_ATTR void animationImage_OnDestroy_m4135289325 (animationImage_t1423068421 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void baseSelector::.ctor()
extern "C" IL2CPP_METHOD_ATTR void baseSelector__ctor_m3158696087 (baseSelector_t1489849420 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void baseSelector::Start()
extern "C" IL2CPP_METHOD_ATTR void baseSelector_Start_m2366842259 (baseSelector_t1489849420 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void baseSelector::getText()
extern "C" IL2CPP_METHOD_ATTR void baseSelector_getText_m3897978743 (baseSelector_t1489849420 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (baseSelector_getText_m3897978743_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		InputField_t3762917431 * L_0 = __this->get_inputField_4();
		NullCheck(L_0);
		String_t* L_1 = InputField_get_text_m3534748202(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(ContentManager2_t2980555998_il2cpp_TypeInfo_var);
		((ContentManager2_t2980555998_StaticFields*)il2cpp_codegen_static_fields_for(ContentManager2_t2980555998_il2cpp_TypeInfo_var))->set_baseName_4(L_1);
		SceneManager_LoadScene_m1758133949(NULL /*static, unused*/, _stringLiteral1222789648, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void captureAndSendScreen::.ctor()
extern "C" IL2CPP_METHOD_ATTR void captureAndSendScreen__ctor_m2626167397 (captureAndSendScreen_t3196082656 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (captureAndSendScreen__ctor_m2626167397_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_screenShotURL_4(_stringLiteral2123297983);
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void captureAndSendScreen::Start()
extern "C" IL2CPP_METHOD_ATTR void captureAndSendScreen_Start_m1022736917 (captureAndSendScreen_t3196082656 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (captureAndSendScreen_Start_m1022736917_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Transform_t3600365921 * L_0 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_t3600365921 * L_1 = Transform_Find_m1729760951(L_0, _stringLiteral336663310, /*hidden argument*/NULL);
		NullCheck(L_1);
		GameObject_t1113636619 * L_2 = Component_get_gameObject_m442555142(L_1, /*hidden argument*/NULL);
		__this->set_nodeTex_7(L_2);
		return;
	}
}
// System.Void captureAndSendScreen::sendCapture()
extern "C" IL2CPP_METHOD_ATTR void captureAndSendScreen_sendCapture_m3694921046 (captureAndSendScreen_t3196082656 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (captureAndSendScreen_sendCapture_m3694921046_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, _stringLiteral274399943, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2618285814(__this, _stringLiteral4275918539, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator captureAndSendScreen::IMPL_sendCapture()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* captureAndSendScreen_IMPL_sendCapture_m127267877 (captureAndSendScreen_t3196082656 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (captureAndSendScreen_IMPL_sendCapture_m127267877_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CIMPL_sendCaptureU3Ec__Iterator0_t1292966789 * V_0 = NULL;
	{
		U3CIMPL_sendCaptureU3Ec__Iterator0_t1292966789 * L_0 = (U3CIMPL_sendCaptureU3Ec__Iterator0_t1292966789 *)il2cpp_codegen_object_new(U3CIMPL_sendCaptureU3Ec__Iterator0_t1292966789_il2cpp_TypeInfo_var);
		U3CIMPL_sendCaptureU3Ec__Iterator0__ctor_m2744192186(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CIMPL_sendCaptureU3Ec__Iterator0_t1292966789 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U24this_7(__this);
		U3CIMPL_sendCaptureU3Ec__Iterator0_t1292966789 * L_2 = V_0;
		return L_2;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void captureAndSendScreen/<IMPL_sendCapture>c__Iterator0::.ctor()
extern "C" IL2CPP_METHOD_ATTR void U3CIMPL_sendCaptureU3Ec__Iterator0__ctor_m2744192186 (U3CIMPL_sendCaptureU3Ec__Iterator0_t1292966789 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean captureAndSendScreen/<IMPL_sendCapture>c__Iterator0::MoveNext()
extern "C" IL2CPP_METHOD_ATTR bool U3CIMPL_sendCaptureU3Ec__Iterator0_MoveNext_m200448671 (U3CIMPL_sendCaptureU3Ec__Iterator0_t1292966789 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CIMPL_sendCaptureU3Ec__Iterator0_MoveNext_m200448671_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = __this->get_U24PC_10();
		V_0 = L_0;
		__this->set_U24PC_10((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0021;
			}
			case 1:
			{
				goto IL_01ac;
			}
		}
	}
	{
		goto IL_01f2;
	}

IL_0021:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, _stringLiteral2595570117, /*hidden argument*/NULL);
		int32_t L_2 = Screen_get_width_m345039817(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_U3CwidthU3E__0_0(L_2);
		int32_t L_3 = Screen_get_height_m1623532518(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_U3CheightU3E__0_1(L_3);
		captureAndSendScreen_t3196082656 * L_4 = __this->get_U24this_7();
		StringU5BU5D_t1281789340* L_5 = (StringU5BU5D_t1281789340*)SZArrayNew(StringU5BU5D_t1281789340_il2cpp_TypeInfo_var, (uint32_t)5);
		StringU5BU5D_t1281789340* L_6 = L_5;
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, _stringLiteral3086763974);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral3086763974);
		StringU5BU5D_t1281789340* L_7 = L_6;
		captureAndSendScreen_t3196082656 * L_8 = __this->get_U24this_7();
		NullCheck(L_8);
		String_t* L_9 = L_8->get_UDID_User_5();
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, L_9);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_9);
		StringU5BU5D_t1281789340* L_10 = L_7;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, _stringLiteral3452614529);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral3452614529);
		StringU5BU5D_t1281789340* L_11 = L_10;
		captureAndSendScreen_t3196082656 * L_12 = __this->get_U24this_7();
		NullCheck(L_12);
		String_t* L_13 = L_12->get_INTERFACE_6();
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_13);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)L_13);
		StringU5BU5D_t1281789340* L_14 = L_11;
		NullCheck(L_14);
		ArrayElementTypeCheck (L_14, _stringLiteral4247501208);
		(L_14)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral4247501208);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_15 = String_Concat_m1809518182(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		NullCheck(L_4);
		L_4->set_screenShotURL_4(L_15);
		captureAndSendScreen_t3196082656 * L_16 = __this->get_U24this_7();
		NullCheck(L_16);
		GameObject_t1113636619 * L_17 = L_16->get_nodeTex_7();
		NullCheck(L_17);
		Renderer_t2627027031 * L_18 = GameObject_GetComponent_TisRenderer_t2627027031_m1619941042(L_17, /*hidden argument*/GameObject_GetComponent_TisRenderer_t2627027031_m1619941042_RuntimeMethod_var);
		__this->set_U3CrendererBackU3E__0_2(L_18);
		Renderer_t2627027031 * L_19 = __this->get_U3CrendererBackU3E__0_2();
		NullCheck(L_19);
		Material_t340375123 * L_20 = Renderer_get_material_m4171603682(L_19, /*hidden argument*/NULL);
		NullCheck(L_20);
		Texture_t3661962703 * L_21 = Material_get_mainTexture_m692510677(L_20, /*hidden argument*/NULL);
		__this->set_U3CtexU3E__0_3(((Texture2D_t3840446185 *)IsInstSealed((RuntimeObject*)L_21, Texture2D_t3840446185_il2cpp_TypeInfo_var)));
		Texture2D_t3840446185 * L_22 = __this->get_U3CtexU3E__0_3();
		NullCheck(L_22);
		int32_t L_23 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_22);
		int32_t L_24 = L_23;
		RuntimeObject * L_25 = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &L_24);
		String_t* L_26 = String_Concat_m904156431(NULL /*static, unused*/, _stringLiteral3357073080, L_25, /*hidden argument*/NULL);
		Debug_Log_m4051431634(NULL /*static, unused*/, L_26, /*hidden argument*/NULL);
		Texture2D_t3840446185 * L_27 = __this->get_U3CtexU3E__0_3();
		NullCheck(L_27);
		int32_t L_28 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.Texture::get_height() */, L_27);
		int32_t L_29 = L_28;
		RuntimeObject * L_30 = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &L_29);
		String_t* L_31 = String_Concat_m904156431(NULL /*static, unused*/, _stringLiteral2078343567, L_30, /*hidden argument*/NULL);
		Debug_Log_m4051431634(NULL /*static, unused*/, L_31, /*hidden argument*/NULL);
		Texture2D_t3840446185 * L_32 = __this->get_U3CtexU3E__0_3();
		ByteU5BU5D_t4116647657* L_33 = ImageConversion_EncodeToPNG_m2292631531(NULL /*static, unused*/, L_32, /*hidden argument*/NULL);
		__this->set_U3CbytesU3E__0_4(L_33);
		ByteU5BU5D_t4116647657* L_34 = __this->get_U3CbytesU3E__0_4();
		NullCheck(L_34);
		int32_t L_35 = (((int32_t)((int32_t)(((RuntimeArray *)L_34)->max_length))));
		RuntimeObject * L_36 = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &L_35);
		MonoBehaviour_print_m330341231(NULL /*static, unused*/, L_36, /*hidden argument*/NULL);
		WWWForm_t4064702195 * L_37 = (WWWForm_t4064702195 *)il2cpp_codegen_object_new(WWWForm_t4064702195_il2cpp_TypeInfo_var);
		WWWForm__ctor_m2465700452(L_37, /*hidden argument*/NULL);
		__this->set_U3CformU3E__0_5(L_37);
		WWWForm_t4064702195 * L_38 = __this->get_U3CformU3E__0_5();
		int32_t L_39 = Time_get_frameCount_m1220035214(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_39;
		String_t* L_40 = Int32_ToString_m141394615((int32_t*)(&V_1), /*hidden argument*/NULL);
		NullCheck(L_38);
		WWWForm_AddField_m2357902982(L_38, _stringLiteral207938285, L_40, /*hidden argument*/NULL);
		WWWForm_t4064702195 * L_41 = __this->get_U3CformU3E__0_5();
		ByteU5BU5D_t4116647657* L_42 = __this->get_U3CbytesU3E__0_4();
		NullCheck(L_41);
		WWWForm_AddBinaryData_m1593257607(L_41, _stringLiteral1629333464, L_42, _stringLiteral3287972590, _stringLiteral2045074213, /*hidden argument*/NULL);
		captureAndSendScreen_t3196082656 * L_43 = __this->get_U24this_7();
		NullCheck(L_43);
		String_t* L_44 = L_43->get_screenShotURL_4();
		WWWForm_t4064702195 * L_45 = __this->get_U3CformU3E__0_5();
		UnityWebRequest_t463507806 * L_46 = UnityWebRequest_Post_m4193475377(NULL /*static, unused*/, L_44, L_45, /*hidden argument*/NULL);
		__this->set_U3CwU3E__0_6(L_46);
		UnityWebRequest_t463507806 * L_47 = __this->get_U3CwU3E__0_6();
		NullCheck(L_47);
		UnityWebRequestAsyncOperation_t3852015985 * L_48 = UnityWebRequest_SendWebRequest_m489860187(L_47, /*hidden argument*/NULL);
		__this->set_U24current_8(L_48);
		bool L_49 = __this->get_U24disposing_9();
		if (L_49)
		{
			goto IL_01a7;
		}
	}
	{
		__this->set_U24PC_10(1);
	}

IL_01a7:
	{
		goto IL_01f4;
	}

IL_01ac:
	{
		UnityWebRequest_t463507806 * L_50 = __this->get_U3CwU3E__0_6();
		NullCheck(L_50);
		bool L_51 = UnityWebRequest_get_isNetworkError_m1231611882(L_50, /*hidden argument*/NULL);
		if (L_51)
		{
			goto IL_01cc;
		}
	}
	{
		UnityWebRequest_t463507806 * L_52 = __this->get_U3CwU3E__0_6();
		NullCheck(L_52);
		bool L_53 = UnityWebRequest_get_isHttpError_m797082501(L_52, /*hidden argument*/NULL);
		if (!L_53)
		{
			goto IL_01e1;
		}
	}

IL_01cc:
	{
		UnityWebRequest_t463507806 * L_54 = __this->get_U3CwU3E__0_6();
		NullCheck(L_54);
		String_t* L_55 = UnityWebRequest_get_error_m1613086199(L_54, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, L_55, /*hidden argument*/NULL);
		goto IL_01eb;
	}

IL_01e1:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, _stringLiteral2998704445, /*hidden argument*/NULL);
	}

IL_01eb:
	{
		__this->set_U24PC_10((-1));
	}

IL_01f2:
	{
		return (bool)0;
	}

IL_01f4:
	{
		return (bool)1;
	}
}
// System.Object captureAndSendScreen/<IMPL_sendCapture>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * U3CIMPL_sendCaptureU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m388835112 (U3CIMPL_sendCaptureU3Ec__Iterator0_t1292966789 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_8();
		return L_0;
	}
}
// System.Object captureAndSendScreen/<IMPL_sendCapture>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * U3CIMPL_sendCaptureU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m3272242346 (U3CIMPL_sendCaptureU3Ec__Iterator0_t1292966789 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_8();
		return L_0;
	}
}
// System.Void captureAndSendScreen/<IMPL_sendCapture>c__Iterator0::Dispose()
extern "C" IL2CPP_METHOD_ATTR void U3CIMPL_sendCaptureU3Ec__Iterator0_Dispose_m3169466563 (U3CIMPL_sendCaptureU3Ec__Iterator0_t1292966789 * __this, const RuntimeMethod* method)
{
	{
		__this->set_U24disposing_9((bool)1);
		__this->set_U24PC_10((-1));
		return;
	}
}
// System.Void captureAndSendScreen/<IMPL_sendCapture>c__Iterator0::Reset()
extern "C" IL2CPP_METHOD_ATTR void U3CIMPL_sendCaptureU3Ec__Iterator0_Reset_m3322412218 (U3CIMPL_sendCaptureU3Ec__Iterator0_t1292966789 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CIMPL_sendCaptureU3Ec__Iterator0_Reset_m3322412218_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1314879016 * L_0 = (NotSupportedException_t1314879016 *)il2cpp_codegen_object_new(NotSupportedException_t1314879016_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2730133172(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, NULL, U3CIMPL_sendCaptureU3Ec__Iterator0_Reset_m3322412218_RuntimeMethod_var);
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void startUpLANG::.ctor()
extern "C" IL2CPP_METHOD_ATTR void startUpLANG__ctor_m1896023275 (startUpLANG_t1517666504 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void startUpLANG::Start()
extern "C" IL2CPP_METHOD_ATTR void startUpLANG_Start_m1865577903 (startUpLANG_t1517666504 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (startUpLANG_Start_m1865577903_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = PlayerPrefs_HasKey_m2794939670(NULL /*static, unused*/, _stringLiteral2733530345, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_002c;
		}
	}
	{
		int32_t L_1 = PlayerPrefs_GetInt_m3797620966(NULL /*static, unused*/, _stringLiteral2733530345, /*hidden argument*/NULL);
		__this->set_selectedLanguageID_4(L_1);
		RuntimeObject* L_2 = startUpLANG_GetLanguageJSON_m3198424110(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m3411253000(__this, L_2, /*hidden argument*/NULL);
	}

IL_002c:
	{
		return;
	}
}
// System.Collections.IEnumerator startUpLANG::GetLanguageJSON()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* startUpLANG_GetLanguageJSON_m3198424110 (startUpLANG_t1517666504 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (startUpLANG_GetLanguageJSON_m3198424110_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CGetLanguageJSONU3Ec__Iterator0_t3550365120 * V_0 = NULL;
	{
		U3CGetLanguageJSONU3Ec__Iterator0_t3550365120 * L_0 = (U3CGetLanguageJSONU3Ec__Iterator0_t3550365120 *)il2cpp_codegen_object_new(U3CGetLanguageJSONU3Ec__Iterator0_t3550365120_il2cpp_TypeInfo_var);
		U3CGetLanguageJSONU3Ec__Iterator0__ctor_m1391803005(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CGetLanguageJSONU3Ec__Iterator0_t3550365120 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U24this_3(__this);
		U3CGetLanguageJSONU3Ec__Iterator0_t3550365120 * L_2 = V_0;
		return L_2;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void startUpLANG/<GetLanguageJSON>c__Iterator0::.ctor()
extern "C" IL2CPP_METHOD_ATTR void U3CGetLanguageJSONU3Ec__Iterator0__ctor_m1391803005 (U3CGetLanguageJSONU3Ec__Iterator0_t3550365120 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean startUpLANG/<GetLanguageJSON>c__Iterator0::MoveNext()
extern "C" IL2CPP_METHOD_ATTR bool U3CGetLanguageJSONU3Ec__Iterator0_MoveNext_m1986941187 (U3CGetLanguageJSONU3Ec__Iterator0_t3550365120 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CGetLanguageJSONU3Ec__Iterator0_MoveNext_m1986941187_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_6();
		V_0 = L_0;
		__this->set_U24PC_6((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0021;
			}
			case 1:
			{
				goto IL_0072;
			}
		}
	}
	{
		goto IL_00a5;
	}

IL_0021:
	{
		startUpLANG_t1517666504 * L_2 = __this->get_U24this_3();
		NullCheck(L_2);
		int32_t L_3 = L_2->get_selectedLanguageID_4();
		int32_t L_4 = L_3;
		RuntimeObject * L_5 = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &L_4);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Concat_m904156431(NULL /*static, unused*/, _stringLiteral2092972393, L_5, /*hidden argument*/NULL);
		__this->set_U3CurlU3E__0_0(L_6);
		String_t* L_7 = __this->get_U3CurlU3E__0_0();
		WWW_t3688466362 * L_8 = (WWW_t3688466362 *)il2cpp_codegen_object_new(WWW_t3688466362_il2cpp_TypeInfo_var);
		WWW__ctor_m2915079343(L_8, L_7, /*hidden argument*/NULL);
		__this->set_U3CwwwU3E__0_1(L_8);
		WWW_t3688466362 * L_9 = __this->get_U3CwwwU3E__0_1();
		__this->set_U24current_4(L_9);
		bool L_10 = __this->get_U24disposing_5();
		if (L_10)
		{
			goto IL_006d;
		}
	}
	{
		__this->set_U24PC_6(1);
	}

IL_006d:
	{
		goto IL_00a7;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MarkerManager_t1031803141_il2cpp_TypeInfo_var);
		MarkerManager_t1031803141 * L_11 = MarkerManager_GetInstance_m3344970980(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_U3CmanagerU3E__0_2(L_11);
		MarkerManager_t1031803141 * L_12 = __this->get_U3CmanagerU3E__0_2();
		WWW_t3688466362 * L_13 = __this->get_U3CwwwU3E__0_1();
		NullCheck(L_13);
		String_t* L_14 = WWW_get_text_m898164367(L_13, /*hidden argument*/NULL);
		NullCheck(L_12);
		MarkerManager_parse_m1894428919(L_12, L_14, /*hidden argument*/NULL);
		SceneManager_LoadScene_m1758133949(NULL /*static, unused*/, _stringLiteral2793514143, /*hidden argument*/NULL);
		__this->set_U24PC_6((-1));
	}

IL_00a5:
	{
		return (bool)0;
	}

IL_00a7:
	{
		return (bool)1;
	}
}
// System.Object startUpLANG/<GetLanguageJSON>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * U3CGetLanguageJSONU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m385277307 (U3CGetLanguageJSONU3Ec__Iterator0_t3550365120 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_4();
		return L_0;
	}
}
// System.Object startUpLANG/<GetLanguageJSON>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * U3CGetLanguageJSONU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m2311424098 (U3CGetLanguageJSONU3Ec__Iterator0_t3550365120 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_4();
		return L_0;
	}
}
// System.Void startUpLANG/<GetLanguageJSON>c__Iterator0::Dispose()
extern "C" IL2CPP_METHOD_ATTR void U3CGetLanguageJSONU3Ec__Iterator0_Dispose_m2945560746 (U3CGetLanguageJSONU3Ec__Iterator0_t3550365120 * __this, const RuntimeMethod* method)
{
	{
		__this->set_U24disposing_5((bool)1);
		__this->set_U24PC_6((-1));
		return;
	}
}
// System.Void startUpLANG/<GetLanguageJSON>c__Iterator0::Reset()
extern "C" IL2CPP_METHOD_ATTR void U3CGetLanguageJSONU3Ec__Iterator0_Reset_m2398464953 (U3CGetLanguageJSONU3Ec__Iterator0_t3550365120 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CGetLanguageJSONU3Ec__Iterator0_Reset_m2398464953_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1314879016 * L_0 = (NotSupportedException_t1314879016 *)il2cpp_codegen_object_new(NotSupportedException_t1314879016_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2730133172(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, NULL, U3CGetLanguageJSONU3Ec__Iterator0_Reset_m2398464953_RuntimeMethod_var);
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void testImage::.ctor()
extern "C" IL2CPP_METHOD_ATTR void testImage__ctor_m2908843561 (testImage_t2491645257 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void testImage::Start()
extern "C" IL2CPP_METHOD_ATTR void testImage_Start_m1287184537 (testImage_t2491645257 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void testImage::Update()
extern "C" IL2CPP_METHOD_ATTR void testImage_Update_m3312027097 (testImage_t2491645257 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void testImage::clickImage()
extern "C" IL2CPP_METHOD_ATTR void testImage_clickImage_m58836525 (testImage_t2491645257 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (testImage_clickImage_m58836525_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Renderer_t2627027031 * V_0 = NULL;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	float V_6 = 0.0f;
	float V_7 = 0.0f;
	float V_8 = 0.0f;
	float V_9 = 0.0f;
	float V_10 = 0.0f;
	float V_11 = 0.0f;
	float V_12 = 0.0f;
	float V_13 = 0.0f;
	float V_14 = 0.0f;
	float V_15 = 0.0f;
	float V_16 = 0.0f;
	float V_17 = 0.0f;
	{
		Image_t2670269651 * L_0 = __this->get_rawImage_5();
		NullCheck(L_0);
		GameObject_t1113636619 * L_1 = Component_get_gameObject_m442555142(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		GameObject_SetActive_m796801857(L_1, (bool)1, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_2 = __this->get_plane_4();
		NullCheck(L_2);
		Renderer_t2627027031 * L_3 = GameObject_GetComponent_TisRenderer_t2627027031_m1619941042(L_2, /*hidden argument*/GameObject_GetComponent_TisRenderer_t2627027031_m1619941042_RuntimeMethod_var);
		V_0 = L_3;
		Image_t2670269651 * L_4 = __this->get_rawImage_5();
		Renderer_t2627027031 * L_5 = V_0;
		NullCheck(L_5);
		Material_t340375123 * L_6 = Renderer_get_material_m4171603682(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		Texture_t3661962703 * L_7 = Material_get_mainTexture_m692510677(L_6, /*hidden argument*/NULL);
		Renderer_t2627027031 * L_8 = V_0;
		NullCheck(L_8);
		Material_t340375123 * L_9 = Renderer_get_material_m4171603682(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		Texture_t3661962703 * L_10 = Material_get_mainTexture_m692510677(L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		int32_t L_11 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_10);
		Renderer_t2627027031 * L_12 = V_0;
		NullCheck(L_12);
		Material_t340375123 * L_13 = Renderer_get_material_m4171603682(L_12, /*hidden argument*/NULL);
		NullCheck(L_13);
		Texture_t3661962703 * L_14 = Material_get_mainTexture_m692510677(L_13, /*hidden argument*/NULL);
		NullCheck(L_14);
		int32_t L_15 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.Texture::get_height() */, L_14);
		Rect_t2360479859  L_16;
		memset(&L_16, 0, sizeof(L_16));
		Rect__ctor_m2614021312((&L_16), (0.0f), (0.0f), (((float)((float)L_11))), (((float)((float)L_15))), /*hidden argument*/NULL);
		Vector2_t2156229523  L_17;
		memset(&L_17, 0, sizeof(L_17));
		Vector2__ctor_m3970636864((&L_17), (0.0f), (0.0f), /*hidden argument*/NULL);
		Sprite_t280657092 * L_18 = Sprite_Create_m803022218(NULL /*static, unused*/, ((Texture2D_t3840446185 *)IsInstSealed((RuntimeObject*)L_7, Texture2D_t3840446185_il2cpp_TypeInfo_var)), L_16, L_17, /*hidden argument*/NULL);
		NullCheck(L_4);
		Image_set_sprite_m2369174689(L_4, L_18, /*hidden argument*/NULL);
		RawImage_t3182918964 * L_19 = __this->get_rawImageLandscape_6();
		Renderer_t2627027031 * L_20 = V_0;
		NullCheck(L_20);
		Material_t340375123 * L_21 = Renderer_get_material_m4171603682(L_20, /*hidden argument*/NULL);
		NullCheck(L_21);
		Texture_t3661962703 * L_22 = Material_get_mainTexture_m692510677(L_21, /*hidden argument*/NULL);
		NullCheck(L_19);
		RawImage_set_texture_m415027901(L_19, L_22, /*hidden argument*/NULL);
		int32_t L_23 = Screen_get_width_m345039817(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_24 = Screen_get_height_m1623532518(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = ((float)((float)(((float)((float)L_23)))/(float)(((float)((float)L_24)))));
		Renderer_t2627027031 * L_25 = V_0;
		NullCheck(L_25);
		Material_t340375123 * L_26 = Renderer_get_material_m4171603682(L_25, /*hidden argument*/NULL);
		NullCheck(L_26);
		Texture_t3661962703 * L_27 = Material_get_mainTexture_m692510677(L_26, /*hidden argument*/NULL);
		NullCheck(L_27);
		int32_t L_28 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_27);
		V_2 = (((float)((float)L_28)));
		Renderer_t2627027031 * L_29 = V_0;
		NullCheck(L_29);
		Material_t340375123 * L_30 = Renderer_get_material_m4171603682(L_29, /*hidden argument*/NULL);
		NullCheck(L_30);
		Texture_t3661962703 * L_31 = Material_get_mainTexture_m692510677(L_30, /*hidden argument*/NULL);
		NullCheck(L_31);
		int32_t L_32 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.Texture::get_height() */, L_31);
		V_3 = (((float)((float)L_32)));
		float L_33 = V_2;
		float L_34 = V_3;
		V_4 = ((float)((float)L_33/(float)L_34));
		V_5 = (800.0f);
		float L_35 = V_1;
		V_6 = ((float)((float)(800.0f)/(float)L_35));
		float L_36 = V_5;
		float L_37 = L_36;
		RuntimeObject * L_38 = Box(Single_t1397266774_il2cpp_TypeInfo_var, &L_37);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_39 = String_Concat_m904156431(NULL /*static, unused*/, _stringLiteral1107120589, L_38, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, L_39, /*hidden argument*/NULL);
		float L_40 = V_6;
		float L_41 = L_40;
		RuntimeObject * L_42 = Box(Single_t1397266774_il2cpp_TypeInfo_var, &L_41);
		String_t* L_43 = String_Concat_m904156431(NULL /*static, unused*/, _stringLiteral609727728, L_42, /*hidden argument*/NULL);
		Debug_Log_m4051431634(NULL /*static, unused*/, L_43, /*hidden argument*/NULL);
		float L_44 = V_5;
		float L_45 = V_6;
		V_7 = ((float)((float)L_44/(float)L_45));
		V_8 = (0.0f);
		V_9 = (0.0f);
		float L_46 = V_4;
		float L_47 = V_7;
		if ((!(((float)L_46) > ((float)L_47))))
		{
			goto IL_013a;
		}
	}
	{
		V_8 = (800.0f);
		float L_48 = V_4;
		V_9 = ((float)((float)(800.0f)/(float)L_48));
		goto IL_0145;
	}

IL_013a:
	{
		float L_49 = V_6;
		float L_50 = V_4;
		V_8 = ((float)il2cpp_codegen_multiply((float)L_49, (float)L_50));
		float L_51 = V_6;
		V_9 = L_51;
	}

IL_0145:
	{
		Image_t2670269651 * L_52 = __this->get_rawImage_5();
		NullCheck(L_52);
		RectTransform_t3704657025 * L_53 = Component_GetComponent_TisRectTransform_t3704657025_m3396022872(L_52, /*hidden argument*/Component_GetComponent_TisRectTransform_t3704657025_m3396022872_RuntimeMethod_var);
		float L_54 = V_8;
		float L_55 = V_9;
		Vector2_t2156229523  L_56;
		memset(&L_56, 0, sizeof(L_56));
		Vector2__ctor_m3970636864((&L_56), L_54, L_55, /*hidden argument*/NULL);
		NullCheck(L_53);
		RectTransform_set_sizeDelta_m3462269772(L_53, L_56, /*hidden argument*/NULL);
		Renderer_t2627027031 * L_57 = V_0;
		NullCheck(L_57);
		Material_t340375123 * L_58 = Renderer_get_material_m4171603682(L_57, /*hidden argument*/NULL);
		NullCheck(L_58);
		Texture_t3661962703 * L_59 = Material_get_mainTexture_m692510677(L_58, /*hidden argument*/NULL);
		NullCheck(L_59);
		int32_t L_60 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.Texture::get_height() */, L_59);
		V_10 = (((float)((float)L_60)));
		Renderer_t2627027031 * L_61 = V_0;
		NullCheck(L_61);
		Material_t340375123 * L_62 = Renderer_get_material_m4171603682(L_61, /*hidden argument*/NULL);
		NullCheck(L_62);
		Texture_t3661962703 * L_63 = Material_get_mainTexture_m692510677(L_62, /*hidden argument*/NULL);
		NullCheck(L_63);
		int32_t L_64 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_63);
		V_11 = (((float)((float)L_64)));
		float L_65 = V_10;
		float L_66 = V_11;
		V_12 = ((float)((float)L_65/(float)L_66));
		V_13 = (800.0f);
		float L_67 = V_1;
		V_14 = ((float)((float)(800.0f)/(float)L_67));
		float L_68 = V_13;
		float L_69 = V_14;
		V_15 = ((float)((float)L_68/(float)L_69));
		V_16 = (0.0f);
		V_17 = (0.0f);
		float L_70 = V_12;
		float L_71 = V_15;
		if ((!(((float)L_70) > ((float)L_71))))
		{
			goto IL_01d9;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, _stringLiteral1645758142, /*hidden argument*/NULL);
		float L_72 = V_12;
		V_16 = ((float)((float)(800.0f)/(float)L_72));
		V_17 = (800.0f);
		goto IL_01ee;
	}

IL_01d9:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, _stringLiteral1645954750, /*hidden argument*/NULL);
		float L_73 = V_14;
		V_16 = L_73;
		float L_74 = V_14;
		float L_75 = V_12;
		V_17 = ((float)il2cpp_codegen_multiply((float)L_74, (float)L_75));
	}

IL_01ee:
	{
		RawImage_t3182918964 * L_76 = __this->get_rawImageLandscape_6();
		NullCheck(L_76);
		RectTransform_t3704657025 * L_77 = Component_GetComponent_TisRectTransform_t3704657025_m3396022872(L_76, /*hidden argument*/Component_GetComponent_TisRectTransform_t3704657025_m3396022872_RuntimeMethod_var);
		float L_78 = V_16;
		float L_79 = V_17;
		Vector2_t2156229523  L_80;
		memset(&L_80, 0, sizeof(L_80));
		Vector2__ctor_m3970636864((&L_80), L_78, L_79, /*hidden argument*/NULL);
		NullCheck(L_77);
		RectTransform_set_sizeDelta_m3462269772(L_77, L_80, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void testPlayer::.ctor()
extern "C" IL2CPP_METHOD_ATTR void testPlayer__ctor_m2359107716 (testPlayer_t3643834628 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void testPlayer::Start()
extern "C" IL2CPP_METHOD_ATTR void testPlayer_Start_m1113046612 (testPlayer_t3643834628 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void testPlayer::Update()
extern "C" IL2CPP_METHOD_ATTR void testPlayer_Update_m3739037858 (testPlayer_t3643834628 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void testPlayer::playVideo()
extern "C" IL2CPP_METHOD_ATTR void testPlayer_playVideo_m2498393805 (testPlayer_t3643834628 * __this, const RuntimeMethod* method)
{
	{
		VideoPlayer_t1683042537 * L_0 = __this->get_videoPlayer_4();
		NullCheck(L_0);
		VideoPlayer_Play_m1850501814(L_0, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void testRescale::.ctor()
extern "C" IL2CPP_METHOD_ATTR void testRescale__ctor_m3308984367 (testRescale_t3860525213 * __this, const RuntimeMethod* method)
{
	Bounds_t2266837910  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		il2cpp_codegen_initobj((&V_0), sizeof(Bounds_t2266837910 ));
		Bounds_t2266837910  L_0 = V_0;
		__this->set_combinedBounds_4(L_0);
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void testRescale::OnDrawGizmos()
extern "C" IL2CPP_METHOD_ATTR void testRescale_OnDrawGizmos_m950397092 (testRescale_t3860525213 * __this, const RuntimeMethod* method)
{
	{
		Color_t2555686324  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Color__ctor_m2943235014((&L_0), (1.0f), (0.0f), (0.0f), (0.5f), /*hidden argument*/NULL);
		Gizmos_set_color_m3399737545(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		Bounds_t2266837910 * L_1 = __this->get_address_of_combinedBounds_4();
		Vector3_t3722313464  L_2 = Bounds_get_center_m1418449258((Bounds_t2266837910 *)L_1, /*hidden argument*/NULL);
		Vector3_t3722313464  L_3;
		memset(&L_3, 0, sizeof(L_3));
		Vector3__ctor_m3353183577((&L_3), (100.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		Gizmos_DrawCube_m530322281(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		Bounds_t2266837910 * L_4 = __this->get_address_of_combinedBounds_4();
		Vector3_t3722313464  L_5 = Bounds_get_center_m1418449258((Bounds_t2266837910 *)L_4, /*hidden argument*/NULL);
		Vector3_t3722313464  L_6;
		memset(&L_6, 0, sizeof(L_6));
		Vector3__ctor_m3353183577((&L_6), (1.0f), (100.0f), (1.0f), /*hidden argument*/NULL);
		Gizmos_DrawCube_m530322281(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		Bounds_t2266837910 * L_7 = __this->get_address_of_combinedBounds_4();
		Vector3_t3722313464  L_8 = Bounds_get_center_m1418449258((Bounds_t2266837910 *)L_7, /*hidden argument*/NULL);
		Vector3_t3722313464  L_9;
		memset(&L_9, 0, sizeof(L_9));
		Vector3__ctor_m3353183577((&L_9), (1.0f), (1.0f), (100.0f), /*hidden argument*/NULL);
		Gizmos_DrawCube_m530322281(NULL /*static, unused*/, L_8, L_9, /*hidden argument*/NULL);
		return;
	}
}
// System.Void testRescale::Start()
extern "C" IL2CPP_METHOD_ATTR void testRescale_Start_m3448785195 (testRescale_t3860525213 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (testRescale_Start_m3448785195_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RendererU5BU5D_t3210418286* V_0 = NULL;
	Renderer_t2627027031 * V_1 = NULL;
	RendererU5BU5D_t3210418286* V_2 = NULL;
	int32_t V_3 = 0;
	Bounds_t2266837910  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Vector3_t3722313464  V_5;
	memset(&V_5, 0, sizeof(V_5));
	float V_6 = 0.0f;
	Vector3_t3722313464  V_7;
	memset(&V_7, 0, sizeof(V_7));
	Renderer_t2627027031 * V_8 = NULL;
	RendererU5BU5D_t3210418286* V_9 = NULL;
	int32_t V_10 = 0;
	Vector3_t3722313464  V_11;
	memset(&V_11, 0, sizeof(V_11));
	{
		RendererU5BU5D_t3210418286* L_0 = Component_GetComponentsInChildren_TisRenderer_t2627027031_m2677127880(__this, /*hidden argument*/Component_GetComponentsInChildren_TisRenderer_t2627027031_m2677127880_RuntimeMethod_var);
		V_0 = L_0;
		RendererU5BU5D_t3210418286* L_1 = V_0;
		NullCheck(L_1);
		int32_t L_2 = 0;
		Renderer_t2627027031 * L_3 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		NullCheck(L_3);
		Bounds_t2266837910  L_4 = Renderer_get_bounds_m1803204000(L_3, /*hidden argument*/NULL);
		__this->set_combinedBounds_4(L_4);
		RendererU5BU5D_t3210418286* L_5 = V_0;
		V_2 = L_5;
		V_3 = 0;
		goto IL_0051;
	}

IL_001e:
	{
		RendererU5BU5D_t3210418286* L_6 = V_2;
		int32_t L_7 = V_3;
		NullCheck(L_6);
		int32_t L_8 = L_7;
		Renderer_t2627027031 * L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		V_1 = L_9;
		Bounds_t2266837910 * L_10 = __this->get_address_of_combinedBounds_4();
		Renderer_t2627027031 * L_11 = V_1;
		NullCheck(L_11);
		Bounds_t2266837910  L_12 = Renderer_get_bounds_m1803204000(L_11, /*hidden argument*/NULL);
		Bounds_Encapsulate_m1263362003((Bounds_t2266837910 *)L_10, L_12, /*hidden argument*/NULL);
		Renderer_t2627027031 * L_13 = V_1;
		NullCheck(L_13);
		Bounds_t2266837910  L_14 = Renderer_get_bounds_m1803204000(L_13, /*hidden argument*/NULL);
		V_4 = L_14;
		String_t* L_15 = Bounds_ToString_m4208690781((Bounds_t2266837910 *)(&V_4), /*hidden argument*/NULL);
		MonoBehaviour_print_m330341231(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		int32_t L_16 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add((int32_t)L_16, (int32_t)1));
	}

IL_0051:
	{
		int32_t L_17 = V_3;
		RendererU5BU5D_t3210418286* L_18 = V_2;
		NullCheck(L_18);
		if ((((int32_t)L_17) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_18)->max_length)))))))
		{
			goto IL_001e;
		}
	}
	{
		Bounds_t2266837910 * L_19 = __this->get_address_of_combinedBounds_4();
		String_t* L_20 = Bounds_ToString_m4208690781((Bounds_t2266837910 *)L_19, /*hidden argument*/NULL);
		MonoBehaviour_print_m330341231(NULL /*static, unused*/, L_20, /*hidden argument*/NULL);
		Bounds_t2266837910 * L_21 = __this->get_address_of_combinedBounds_4();
		Vector3_t3722313464  L_22 = Bounds_get_extents_m1304537151((Bounds_t2266837910 *)L_21, /*hidden argument*/NULL);
		V_5 = L_22;
		(&V_5)->set_y_3((0.0f));
		Bounds_t2266837910 * L_23 = __this->get_address_of_combinedBounds_4();
		Vector3_t3722313464  L_24 = Bounds_get_extents_m1304537151((Bounds_t2266837910 *)L_23, /*hidden argument*/NULL);
		V_7 = L_24;
		float L_25 = Vector3_get_magnitude_m27958459((Vector3_t3722313464 *)(&V_7), /*hidden argument*/NULL);
		V_6 = ((float)((float)(10.0f)/(float)L_25));
		RendererU5BU5D_t3210418286* L_26 = V_0;
		V_9 = L_26;
		V_10 = 0;
		goto IL_0107;
	}

IL_00b0:
	{
		RendererU5BU5D_t3210418286* L_27 = V_9;
		int32_t L_28 = V_10;
		NullCheck(L_27);
		int32_t L_29 = L_28;
		Renderer_t2627027031 * L_30 = (L_27)->GetAt(static_cast<il2cpp_array_size_t>(L_29));
		V_8 = L_30;
		Renderer_t2627027031 * L_31 = V_8;
		NullCheck(L_31);
		Transform_t3600365921 * L_32 = Component_get_transform_m3162698980(L_31, /*hidden argument*/NULL);
		Transform_t3600365921 * L_33 = L_32;
		NullCheck(L_33);
		Vector3_t3722313464  L_34 = Transform_get_position_m36019626(L_33, /*hidden argument*/NULL);
		Bounds_t2266837910 * L_35 = __this->get_address_of_combinedBounds_4();
		Vector3_t3722313464  L_36 = Bounds_get_center_m1418449258((Bounds_t2266837910 *)L_35, /*hidden argument*/NULL);
		Bounds_t2266837910 * L_37 = __this->get_address_of_combinedBounds_4();
		Vector3_t3722313464  L_38 = Bounds_get_extents_m1304537151((Bounds_t2266837910 *)L_37, /*hidden argument*/NULL);
		V_11 = L_38;
		float L_39 = (&V_11)->get_y_3();
		Vector3_t3722313464  L_40;
		memset(&L_40, 0, sizeof(L_40));
		Vector3__ctor_m3353183577((&L_40), (0.0f), L_39, (0.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_41 = Vector3_op_Subtraction_m3073674971(NULL /*static, unused*/, L_36, L_40, /*hidden argument*/NULL);
		Vector3_t3722313464  L_42 = Vector3_op_Subtraction_m3073674971(NULL /*static, unused*/, L_34, L_41, /*hidden argument*/NULL);
		NullCheck(L_33);
		Transform_set_position_m3387557959(L_33, L_42, /*hidden argument*/NULL);
		int32_t L_43 = V_10;
		V_10 = ((int32_t)il2cpp_codegen_add((int32_t)L_43, (int32_t)1));
	}

IL_0107:
	{
		int32_t L_44 = V_10;
		RendererU5BU5D_t3210418286* L_45 = V_9;
		NullCheck(L_45);
		if ((((int32_t)L_44) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_45)->max_length)))))))
		{
			goto IL_00b0;
		}
	}
	{
		Transform_t3600365921 * L_46 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		float L_47 = V_6;
		float L_48 = V_6;
		float L_49 = V_6;
		Vector3_t3722313464  L_50;
		memset(&L_50, 0, sizeof(L_50));
		Vector3__ctor_m3353183577((&L_50), L_47, L_48, L_49, /*hidden argument*/NULL);
		NullCheck(L_46);
		Transform_set_localScale_m3053443106(L_46, L_50, /*hidden argument*/NULL);
		return;
	}
}
// System.Void testRescale::Update()
extern "C" IL2CPP_METHOD_ATTR void testRescale_Update_m1630457380 (testRescale_t3860525213 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
