﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// ActionContent
struct ActionContent_t2142700484;
// AnimationsManager
struct AnimationsManager_t2281084567;
// BookData
struct BookData_t1076903227;
// BookInformationParser
struct BookInformationParser_t151156397;
// CameraSettings
struct CameraSettings_t3152619780;
// ContentManager
struct ContentManager_t211916338;
// ContentManager2
struct ContentManager2_t2980555998;
// GrattageManager
struct GrattageManager_t1333860694;
// GrattageScript
struct GrattageScript_t3667968469;
// MenuAnimator
struct MenuAnimator_t2112910832;
// MenuOptions
struct MenuOptions_t1951716431;
// ScanLine
struct ScanLine_t269422218;
// StarsRatingControl
struct StarsRatingControl_t657076381;
// System.Action
struct Action_t1264377477;
// System.Action`1<System.Boolean>
struct Action_1_t269755560;
// System.Byte[]
struct ByteU5BU5D_t4116647657;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.ImageTarget>
struct Dictionary_2_t2595729825;
// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VirtualButtonBehaviour>
struct Dictionary_2_t325039782;
// System.Collections.Generic.Dictionary`2<System.String,Vuforia.WebCamProfile/ProfileData>
struct Dictionary_2_t3304648224;
// System.Collections.Generic.Dictionary`2<System.Type,System.Func`2<System.Type,Vuforia.Tracker>>
struct Dictionary_2_t1322931057;
// System.Collections.Generic.Dictionary`2<System.Type,System.Func`2<Vuforia.Tracker,System.Boolean>>
struct Dictionary_2_t2058017892;
// System.Collections.Generic.Dictionary`2<System.Type,Vuforia.Tracker>
struct Dictionary_2_t858966067;
// System.Collections.Generic.HashSet`1<UnityEngine.MeshRenderer>
struct HashSet_1_t3446926030;
// System.Collections.Generic.LinkedList`1<Vuforia.VuforiaManager/TrackableIdPair>
struct LinkedList_1_t3066996466;
// System.Collections.Generic.List`1<Pixel>
struct List_1_t4186031580;
// System.Collections.Generic.List`1<System.Collections.Generic.List`1<Pixel>>
struct List_1_t1363139026;
// System.Collections.Generic.List`1<System.Type>
struct List_1_t3956019502;
// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct List_1_t3628304265;
// System.Collections.Generic.List`1<Vuforia.DataSet>
struct List_1_t463142320;
// System.Collections.Generic.List`1<Vuforia.ITrackableEventHandler>
struct List_1_t2968050330;
// System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>
struct List_1_t2728888017;
// System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>
struct List_1_t905170877;
// System.Collections.Generic.List`1<Vuforia.IVirtualButtonEventHandler>
struct List_1_t365750880;
// System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>
struct List_1_t619090059;
// System.Func`2<System.Type,Vuforia.Tracker>
struct Func_2_t3173551289;
// System.Func`2<Vuforia.Tracker,System.Boolean>
struct Func_2_t3908638124;
// System.Func`3<System.String,Vuforia.WebCamProfile/ProfileData,Vuforia.IWebCamTexAdaptor>
struct Func_3_t3440825513;
// System.String
struct String_t;
// System.Void
struct Void_t1185182177;
// TMPro.TextMeshPro
struct TextMeshPro_t2393593166;
// TMPro.TextMeshProUGUI
struct TextMeshProUGUI_t529313277;
// TrackableSettings
struct TrackableSettings_t2862243993;
// UnityEngine.Camera
struct Camera_t4157153871;
// UnityEngine.Canvas
struct Canvas_t3310196443;
// UnityEngine.Collider
struct Collider_t1773347010;
// UnityEngine.Color32[]
struct Color32U5BU5D_t3850468773;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3328599146;
// UnityEngine.Material
struct Material_t340375123;
// UnityEngine.RaycastHit[]
struct RaycastHitU5BU5D_t1690781147;
// UnityEngine.Renderer
struct Renderer_t2627027031;
// UnityEngine.Shader
struct Shader_t4151988712;
// UnityEngine.Sprite
struct Sprite_t280657092;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_t3235626157;
// UnityEngine.TextMesh
struct TextMesh_t1536577757;
// UnityEngine.Texture
struct Texture_t3661962703;
// UnityEngine.Texture2D
struct Texture2D_t3840446185;
// UnityEngine.Transform
struct Transform_t3600365921;
// UnityEngine.UI.Button
struct Button_t4055032469;
// UnityEngine.UI.Image
struct Image_t2670269651;
// UnityEngine.UI.RawImage
struct RawImage_t3182918964;
// UnityEngine.UI.Slider
struct Slider_t3903728902;
// UnityEngine.UI.Text
struct Text_t1901882714;
// UnityEngine.WWW
struct WWW_t3688466362;
// Vuforia.BackgroundPlaneBehaviour
struct BackgroundPlaneBehaviour_t3333547397;
// Vuforia.CameraCalibrationComparer
struct CameraCalibrationComparer_t2990055837;
// Vuforia.CloudRecoBehaviour
struct CloudRecoBehaviour_t431762792;
// Vuforia.DataSet
struct DataSet_t3286034874;
// Vuforia.DigitalEyewearARController
struct DigitalEyewearARController_t1054226036;
// Vuforia.ICameraConfiguration
struct ICameraConfiguration_t283990539;
// Vuforia.IHoloLensApiAbstraction
struct IHoloLensApiAbstraction_t3268373165;
// Vuforia.ITrackerManager
struct ITrackerManager_t607206903;
// Vuforia.ImageTarget
struct ImageTarget_t3707016494;
// Vuforia.ImageTargetBehaviour
struct ImageTargetBehaviour_t2200418350;
// Vuforia.ImageTargetBuilder
struct ImageTargetBuilder_t2430893908;
// Vuforia.LateLatchingManager
struct LateLatchingManager_t3198550161;
// Vuforia.MultiTarget
struct MultiTarget_t2016089265;
// Vuforia.ObjectTracker
struct ObjectTracker_t4177997237;
// Vuforia.StateManager
struct StateManager_t1982749557;
// Vuforia.TargetFinder
struct TargetFinder_t2439332195;
// Vuforia.Trackable
struct Trackable_t2451999991;
// Vuforia.TrackableBehaviour
struct TrackableBehaviour_t1113559212;
// Vuforia.TrackerData/TrackableResultData[]
struct TrackableResultDataU5BU5D_t4273811049;
// Vuforia.TrackerData/VuMarkTargetData[]
struct VuMarkTargetDataU5BU5D_t4015091482;
// Vuforia.TrackerData/VuMarkTargetResultData[]
struct VuMarkTargetResultDataU5BU5D_t2157423781;
// Vuforia.VideoBackgroundManager
struct VideoBackgroundManager_t2198727358;
// Vuforia.VirtualButton
struct VirtualButton_t386166510;
// Vuforia.VuMarkBehaviour
struct VuMarkBehaviour_t1178230459;
// Vuforia.VuforiaARController
struct VuforiaARController_t1876945237;
// Vuforia.VuforiaBehaviour
struct VuforiaBehaviour_t2151848540;
// Vuforia.WebCam
struct WebCam_t2427002488;
// Vuforia.WorldCenterTrackableBehaviour
struct WorldCenterTrackableBehaviour_t632567575;




#ifndef U3CMODULEU3E_T692745555_H
#define U3CMODULEU3E_T692745555_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745555 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745555_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CLOADIMAGEU3EC__ITERATOR0_T987217000_H
#define U3CLOADIMAGEU3EC__ITERATOR0_T987217000_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ActionContent/<loadImage>c__Iterator0
struct  U3CloadImageU3Ec__Iterator0_t987217000  : public RuntimeObject
{
public:
	// System.String ActionContent/<loadImage>c__Iterator0::url
	String_t* ___url_0;
	// UnityEngine.WWW ActionContent/<loadImage>c__Iterator0::<www>__0
	WWW_t3688466362 * ___U3CwwwU3E__0_1;
	// UnityEngine.Renderer ActionContent/<loadImage>c__Iterator0::<rd>__0
	Renderer_t2627027031 * ___U3CrdU3E__0_2;
	// System.Single ActionContent/<loadImage>c__Iterator0::posX
	float ___posX_3;
	// System.Single ActionContent/<loadImage>c__Iterator0::posY
	float ___posY_4;
	// System.Single ActionContent/<loadImage>c__Iterator0::layer
	float ___layer_5;
	// System.Single ActionContent/<loadImage>c__Iterator0::sizeX
	float ___sizeX_6;
	// System.Single ActionContent/<loadImage>c__Iterator0::sizeY
	float ___sizeY_7;
	// System.Single ActionContent/<loadImage>c__Iterator0::rotation
	float ___rotation_8;
	// ActionContent ActionContent/<loadImage>c__Iterator0::$this
	ActionContent_t2142700484 * ___U24this_9;
	// System.Object ActionContent/<loadImage>c__Iterator0::$current
	RuntimeObject * ___U24current_10;
	// System.Boolean ActionContent/<loadImage>c__Iterator0::$disposing
	bool ___U24disposing_11;
	// System.Int32 ActionContent/<loadImage>c__Iterator0::$PC
	int32_t ___U24PC_12;

public:
	inline static int32_t get_offset_of_url_0() { return static_cast<int32_t>(offsetof(U3CloadImageU3Ec__Iterator0_t987217000, ___url_0)); }
	inline String_t* get_url_0() const { return ___url_0; }
	inline String_t** get_address_of_url_0() { return &___url_0; }
	inline void set_url_0(String_t* value)
	{
		___url_0 = value;
		Il2CppCodeGenWriteBarrier((&___url_0), value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E__0_1() { return static_cast<int32_t>(offsetof(U3CloadImageU3Ec__Iterator0_t987217000, ___U3CwwwU3E__0_1)); }
	inline WWW_t3688466362 * get_U3CwwwU3E__0_1() const { return ___U3CwwwU3E__0_1; }
	inline WWW_t3688466362 ** get_address_of_U3CwwwU3E__0_1() { return &___U3CwwwU3E__0_1; }
	inline void set_U3CwwwU3E__0_1(WWW_t3688466362 * value)
	{
		___U3CwwwU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwwwU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U3CrdU3E__0_2() { return static_cast<int32_t>(offsetof(U3CloadImageU3Ec__Iterator0_t987217000, ___U3CrdU3E__0_2)); }
	inline Renderer_t2627027031 * get_U3CrdU3E__0_2() const { return ___U3CrdU3E__0_2; }
	inline Renderer_t2627027031 ** get_address_of_U3CrdU3E__0_2() { return &___U3CrdU3E__0_2; }
	inline void set_U3CrdU3E__0_2(Renderer_t2627027031 * value)
	{
		___U3CrdU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrdU3E__0_2), value);
	}

	inline static int32_t get_offset_of_posX_3() { return static_cast<int32_t>(offsetof(U3CloadImageU3Ec__Iterator0_t987217000, ___posX_3)); }
	inline float get_posX_3() const { return ___posX_3; }
	inline float* get_address_of_posX_3() { return &___posX_3; }
	inline void set_posX_3(float value)
	{
		___posX_3 = value;
	}

	inline static int32_t get_offset_of_posY_4() { return static_cast<int32_t>(offsetof(U3CloadImageU3Ec__Iterator0_t987217000, ___posY_4)); }
	inline float get_posY_4() const { return ___posY_4; }
	inline float* get_address_of_posY_4() { return &___posY_4; }
	inline void set_posY_4(float value)
	{
		___posY_4 = value;
	}

	inline static int32_t get_offset_of_layer_5() { return static_cast<int32_t>(offsetof(U3CloadImageU3Ec__Iterator0_t987217000, ___layer_5)); }
	inline float get_layer_5() const { return ___layer_5; }
	inline float* get_address_of_layer_5() { return &___layer_5; }
	inline void set_layer_5(float value)
	{
		___layer_5 = value;
	}

	inline static int32_t get_offset_of_sizeX_6() { return static_cast<int32_t>(offsetof(U3CloadImageU3Ec__Iterator0_t987217000, ___sizeX_6)); }
	inline float get_sizeX_6() const { return ___sizeX_6; }
	inline float* get_address_of_sizeX_6() { return &___sizeX_6; }
	inline void set_sizeX_6(float value)
	{
		___sizeX_6 = value;
	}

	inline static int32_t get_offset_of_sizeY_7() { return static_cast<int32_t>(offsetof(U3CloadImageU3Ec__Iterator0_t987217000, ___sizeY_7)); }
	inline float get_sizeY_7() const { return ___sizeY_7; }
	inline float* get_address_of_sizeY_7() { return &___sizeY_7; }
	inline void set_sizeY_7(float value)
	{
		___sizeY_7 = value;
	}

	inline static int32_t get_offset_of_rotation_8() { return static_cast<int32_t>(offsetof(U3CloadImageU3Ec__Iterator0_t987217000, ___rotation_8)); }
	inline float get_rotation_8() const { return ___rotation_8; }
	inline float* get_address_of_rotation_8() { return &___rotation_8; }
	inline void set_rotation_8(float value)
	{
		___rotation_8 = value;
	}

	inline static int32_t get_offset_of_U24this_9() { return static_cast<int32_t>(offsetof(U3CloadImageU3Ec__Iterator0_t987217000, ___U24this_9)); }
	inline ActionContent_t2142700484 * get_U24this_9() const { return ___U24this_9; }
	inline ActionContent_t2142700484 ** get_address_of_U24this_9() { return &___U24this_9; }
	inline void set_U24this_9(ActionContent_t2142700484 * value)
	{
		___U24this_9 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_9), value);
	}

	inline static int32_t get_offset_of_U24current_10() { return static_cast<int32_t>(offsetof(U3CloadImageU3Ec__Iterator0_t987217000, ___U24current_10)); }
	inline RuntimeObject * get_U24current_10() const { return ___U24current_10; }
	inline RuntimeObject ** get_address_of_U24current_10() { return &___U24current_10; }
	inline void set_U24current_10(RuntimeObject * value)
	{
		___U24current_10 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_10), value);
	}

	inline static int32_t get_offset_of_U24disposing_11() { return static_cast<int32_t>(offsetof(U3CloadImageU3Ec__Iterator0_t987217000, ___U24disposing_11)); }
	inline bool get_U24disposing_11() const { return ___U24disposing_11; }
	inline bool* get_address_of_U24disposing_11() { return &___U24disposing_11; }
	inline void set_U24disposing_11(bool value)
	{
		___U24disposing_11 = value;
	}

	inline static int32_t get_offset_of_U24PC_12() { return static_cast<int32_t>(offsetof(U3CloadImageU3Ec__Iterator0_t987217000, ___U24PC_12)); }
	inline int32_t get_U24PC_12() const { return ___U24PC_12; }
	inline int32_t* get_address_of_U24PC_12() { return &___U24PC_12; }
	inline void set_U24PC_12(int32_t value)
	{
		___U24PC_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADIMAGEU3EC__ITERATOR0_T987217000_H
#ifndef U3CLOADNEXTSCENEAFTERU3EC__ITERATOR0_T994227170_H
#define U3CLOADNEXTSCENEAFTERU3EC__ITERATOR0_T994227170_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AsyncSceneLoader/<LoadNextSceneAfter>c__Iterator0
struct  U3CLoadNextSceneAfterU3Ec__Iterator0_t994227170  : public RuntimeObject
{
public:
	// System.Single AsyncSceneLoader/<LoadNextSceneAfter>c__Iterator0::seconds
	float ___seconds_0;
	// System.Object AsyncSceneLoader/<LoadNextSceneAfter>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean AsyncSceneLoader/<LoadNextSceneAfter>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 AsyncSceneLoader/<LoadNextSceneAfter>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_seconds_0() { return static_cast<int32_t>(offsetof(U3CLoadNextSceneAfterU3Ec__Iterator0_t994227170, ___seconds_0)); }
	inline float get_seconds_0() const { return ___seconds_0; }
	inline float* get_address_of_seconds_0() { return &___seconds_0; }
	inline void set_seconds_0(float value)
	{
		___seconds_0 = value;
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CLoadNextSceneAfterU3Ec__Iterator0_t994227170, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CLoadNextSceneAfterU3Ec__Iterator0_t994227170, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CLoadNextSceneAfterU3Ec__Iterator0_t994227170, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADNEXTSCENEAFTERU3EC__ITERATOR0_T994227170_H
#ifndef BOOKDATA_T1076903227_H
#define BOOKDATA_T1076903227_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BookData
struct  BookData_t1076903227  : public RuntimeObject
{
public:
	// System.String BookData::<BookTitle>k__BackingField
	String_t* ___U3CBookTitleU3Ek__BackingField_0;
	// System.String BookData::<BookAuthor>k__BackingField
	String_t* ___U3CBookAuthorU3Ek__BackingField_1;
	// System.Int32 BookData::<BookRating>k__BackingField
	int32_t ___U3CBookRatingU3Ek__BackingField_2;
	// System.Int32 BookData::<BookOverallRating>k__BackingField
	int32_t ___U3CBookOverallRatingU3Ek__BackingField_3;
	// System.Single BookData::<BookRegularPrice>k__BackingField
	float ___U3CBookRegularPriceU3Ek__BackingField_4;
	// System.Single BookData::<BookYourPrice>k__BackingField
	float ___U3CBookYourPriceU3Ek__BackingField_5;
	// System.String BookData::<BookThumbUrl>k__BackingField
	String_t* ___U3CBookThumbUrlU3Ek__BackingField_6;
	// System.String BookData::<BookDetailUrl>k__BackingField
	String_t* ___U3CBookDetailUrlU3Ek__BackingField_7;

public:
	inline static int32_t get_offset_of_U3CBookTitleU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(BookData_t1076903227, ___U3CBookTitleU3Ek__BackingField_0)); }
	inline String_t* get_U3CBookTitleU3Ek__BackingField_0() const { return ___U3CBookTitleU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CBookTitleU3Ek__BackingField_0() { return &___U3CBookTitleU3Ek__BackingField_0; }
	inline void set_U3CBookTitleU3Ek__BackingField_0(String_t* value)
	{
		___U3CBookTitleU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CBookTitleU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CBookAuthorU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(BookData_t1076903227, ___U3CBookAuthorU3Ek__BackingField_1)); }
	inline String_t* get_U3CBookAuthorU3Ek__BackingField_1() const { return ___U3CBookAuthorU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CBookAuthorU3Ek__BackingField_1() { return &___U3CBookAuthorU3Ek__BackingField_1; }
	inline void set_U3CBookAuthorU3Ek__BackingField_1(String_t* value)
	{
		___U3CBookAuthorU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CBookAuthorU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CBookRatingU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(BookData_t1076903227, ___U3CBookRatingU3Ek__BackingField_2)); }
	inline int32_t get_U3CBookRatingU3Ek__BackingField_2() const { return ___U3CBookRatingU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CBookRatingU3Ek__BackingField_2() { return &___U3CBookRatingU3Ek__BackingField_2; }
	inline void set_U3CBookRatingU3Ek__BackingField_2(int32_t value)
	{
		___U3CBookRatingU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CBookOverallRatingU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(BookData_t1076903227, ___U3CBookOverallRatingU3Ek__BackingField_3)); }
	inline int32_t get_U3CBookOverallRatingU3Ek__BackingField_3() const { return ___U3CBookOverallRatingU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CBookOverallRatingU3Ek__BackingField_3() { return &___U3CBookOverallRatingU3Ek__BackingField_3; }
	inline void set_U3CBookOverallRatingU3Ek__BackingField_3(int32_t value)
	{
		___U3CBookOverallRatingU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CBookRegularPriceU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(BookData_t1076903227, ___U3CBookRegularPriceU3Ek__BackingField_4)); }
	inline float get_U3CBookRegularPriceU3Ek__BackingField_4() const { return ___U3CBookRegularPriceU3Ek__BackingField_4; }
	inline float* get_address_of_U3CBookRegularPriceU3Ek__BackingField_4() { return &___U3CBookRegularPriceU3Ek__BackingField_4; }
	inline void set_U3CBookRegularPriceU3Ek__BackingField_4(float value)
	{
		___U3CBookRegularPriceU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CBookYourPriceU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(BookData_t1076903227, ___U3CBookYourPriceU3Ek__BackingField_5)); }
	inline float get_U3CBookYourPriceU3Ek__BackingField_5() const { return ___U3CBookYourPriceU3Ek__BackingField_5; }
	inline float* get_address_of_U3CBookYourPriceU3Ek__BackingField_5() { return &___U3CBookYourPriceU3Ek__BackingField_5; }
	inline void set_U3CBookYourPriceU3Ek__BackingField_5(float value)
	{
		___U3CBookYourPriceU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CBookThumbUrlU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(BookData_t1076903227, ___U3CBookThumbUrlU3Ek__BackingField_6)); }
	inline String_t* get_U3CBookThumbUrlU3Ek__BackingField_6() const { return ___U3CBookThumbUrlU3Ek__BackingField_6; }
	inline String_t** get_address_of_U3CBookThumbUrlU3Ek__BackingField_6() { return &___U3CBookThumbUrlU3Ek__BackingField_6; }
	inline void set_U3CBookThumbUrlU3Ek__BackingField_6(String_t* value)
	{
		___U3CBookThumbUrlU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CBookThumbUrlU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3CBookDetailUrlU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(BookData_t1076903227, ___U3CBookDetailUrlU3Ek__BackingField_7)); }
	inline String_t* get_U3CBookDetailUrlU3Ek__BackingField_7() const { return ___U3CBookDetailUrlU3Ek__BackingField_7; }
	inline String_t** get_address_of_U3CBookDetailUrlU3Ek__BackingField_7() { return &___U3CBookDetailUrlU3Ek__BackingField_7; }
	inline void set_U3CBookDetailUrlU3Ek__BackingField_7(String_t* value)
	{
		___U3CBookDetailUrlU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CBookDetailUrlU3Ek__BackingField_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOKDATA_T1076903227_H
#ifndef BOOKINFORMATIONPARSER_T151156397_H
#define BOOKINFORMATIONPARSER_T151156397_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BookInformationParser
struct  BookInformationParser_t151156397  : public RuntimeObject
{
public:
	// UnityEngine.GameObject BookInformationParser::mBookObject
	GameObject_t1113636619 * ___mBookObject_0;
	// UnityEngine.TextMesh BookInformationParser::mBookTitle
	TextMesh_t1536577757 * ___mBookTitle_1;
	// UnityEngine.TextMesh BookInformationParser::mBookAuthor
	TextMesh_t1536577757 * ___mBookAuthor_2;
	// UnityEngine.TextMesh BookInformationParser::mBookRegularPrice
	TextMesh_t1536577757 * ___mBookRegularPrice_3;
	// UnityEngine.TextMesh BookInformationParser::mBookOverallRatings
	TextMesh_t1536577757 * ___mBookOverallRatings_4;
	// StarsRatingControl BookInformationParser::mBookStarsRating
	StarsRatingControl_t657076381 * ___mBookStarsRating_5;
	// UnityEngine.TextMesh BookInformationParser::mBookYourPrice
	TextMesh_t1536577757 * ___mBookYourPrice_6;
	// UnityEngine.GameObject BookInformationParser::mBookThumb
	GameObject_t1113636619 * ___mBookThumb_7;

public:
	inline static int32_t get_offset_of_mBookObject_0() { return static_cast<int32_t>(offsetof(BookInformationParser_t151156397, ___mBookObject_0)); }
	inline GameObject_t1113636619 * get_mBookObject_0() const { return ___mBookObject_0; }
	inline GameObject_t1113636619 ** get_address_of_mBookObject_0() { return &___mBookObject_0; }
	inline void set_mBookObject_0(GameObject_t1113636619 * value)
	{
		___mBookObject_0 = value;
		Il2CppCodeGenWriteBarrier((&___mBookObject_0), value);
	}

	inline static int32_t get_offset_of_mBookTitle_1() { return static_cast<int32_t>(offsetof(BookInformationParser_t151156397, ___mBookTitle_1)); }
	inline TextMesh_t1536577757 * get_mBookTitle_1() const { return ___mBookTitle_1; }
	inline TextMesh_t1536577757 ** get_address_of_mBookTitle_1() { return &___mBookTitle_1; }
	inline void set_mBookTitle_1(TextMesh_t1536577757 * value)
	{
		___mBookTitle_1 = value;
		Il2CppCodeGenWriteBarrier((&___mBookTitle_1), value);
	}

	inline static int32_t get_offset_of_mBookAuthor_2() { return static_cast<int32_t>(offsetof(BookInformationParser_t151156397, ___mBookAuthor_2)); }
	inline TextMesh_t1536577757 * get_mBookAuthor_2() const { return ___mBookAuthor_2; }
	inline TextMesh_t1536577757 ** get_address_of_mBookAuthor_2() { return &___mBookAuthor_2; }
	inline void set_mBookAuthor_2(TextMesh_t1536577757 * value)
	{
		___mBookAuthor_2 = value;
		Il2CppCodeGenWriteBarrier((&___mBookAuthor_2), value);
	}

	inline static int32_t get_offset_of_mBookRegularPrice_3() { return static_cast<int32_t>(offsetof(BookInformationParser_t151156397, ___mBookRegularPrice_3)); }
	inline TextMesh_t1536577757 * get_mBookRegularPrice_3() const { return ___mBookRegularPrice_3; }
	inline TextMesh_t1536577757 ** get_address_of_mBookRegularPrice_3() { return &___mBookRegularPrice_3; }
	inline void set_mBookRegularPrice_3(TextMesh_t1536577757 * value)
	{
		___mBookRegularPrice_3 = value;
		Il2CppCodeGenWriteBarrier((&___mBookRegularPrice_3), value);
	}

	inline static int32_t get_offset_of_mBookOverallRatings_4() { return static_cast<int32_t>(offsetof(BookInformationParser_t151156397, ___mBookOverallRatings_4)); }
	inline TextMesh_t1536577757 * get_mBookOverallRatings_4() const { return ___mBookOverallRatings_4; }
	inline TextMesh_t1536577757 ** get_address_of_mBookOverallRatings_4() { return &___mBookOverallRatings_4; }
	inline void set_mBookOverallRatings_4(TextMesh_t1536577757 * value)
	{
		___mBookOverallRatings_4 = value;
		Il2CppCodeGenWriteBarrier((&___mBookOverallRatings_4), value);
	}

	inline static int32_t get_offset_of_mBookStarsRating_5() { return static_cast<int32_t>(offsetof(BookInformationParser_t151156397, ___mBookStarsRating_5)); }
	inline StarsRatingControl_t657076381 * get_mBookStarsRating_5() const { return ___mBookStarsRating_5; }
	inline StarsRatingControl_t657076381 ** get_address_of_mBookStarsRating_5() { return &___mBookStarsRating_5; }
	inline void set_mBookStarsRating_5(StarsRatingControl_t657076381 * value)
	{
		___mBookStarsRating_5 = value;
		Il2CppCodeGenWriteBarrier((&___mBookStarsRating_5), value);
	}

	inline static int32_t get_offset_of_mBookYourPrice_6() { return static_cast<int32_t>(offsetof(BookInformationParser_t151156397, ___mBookYourPrice_6)); }
	inline TextMesh_t1536577757 * get_mBookYourPrice_6() const { return ___mBookYourPrice_6; }
	inline TextMesh_t1536577757 ** get_address_of_mBookYourPrice_6() { return &___mBookYourPrice_6; }
	inline void set_mBookYourPrice_6(TextMesh_t1536577757 * value)
	{
		___mBookYourPrice_6 = value;
		Il2CppCodeGenWriteBarrier((&___mBookYourPrice_6), value);
	}

	inline static int32_t get_offset_of_mBookThumb_7() { return static_cast<int32_t>(offsetof(BookInformationParser_t151156397, ___mBookThumb_7)); }
	inline GameObject_t1113636619 * get_mBookThumb_7() const { return ___mBookThumb_7; }
	inline GameObject_t1113636619 ** get_address_of_mBookThumb_7() { return &___mBookThumb_7; }
	inline void set_mBookThumb_7(GameObject_t1113636619 * value)
	{
		___mBookThumb_7 = value;
		Il2CppCodeGenWriteBarrier((&___mBookThumb_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOKINFORMATIONPARSER_T151156397_H
#ifndef U3CRESTOREORIGINALFOCUSMODEU3EC__ITERATOR0_T2912012229_H
#define U3CRESTOREORIGINALFOCUSMODEU3EC__ITERATOR0_T2912012229_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraSettings/<RestoreOriginalFocusMode>c__Iterator0
struct  U3CRestoreOriginalFocusModeU3Ec__Iterator0_t2912012229  : public RuntimeObject
{
public:
	// CameraSettings CameraSettings/<RestoreOriginalFocusMode>c__Iterator0::$this
	CameraSettings_t3152619780 * ___U24this_0;
	// System.Object CameraSettings/<RestoreOriginalFocusMode>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean CameraSettings/<RestoreOriginalFocusMode>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 CameraSettings/<RestoreOriginalFocusMode>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CRestoreOriginalFocusModeU3Ec__Iterator0_t2912012229, ___U24this_0)); }
	inline CameraSettings_t3152619780 * get_U24this_0() const { return ___U24this_0; }
	inline CameraSettings_t3152619780 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(CameraSettings_t3152619780 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CRestoreOriginalFocusModeU3Ec__Iterator0_t2912012229, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CRestoreOriginalFocusModeU3Ec__Iterator0_t2912012229, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CRestoreOriginalFocusModeU3Ec__Iterator0_t2912012229, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CRESTOREORIGINALFOCUSMODEU3EC__ITERATOR0_T2912012229_H
#ifndef U3CLOADJSONBOOKDATAU3EC__ITERATOR0_T2579545968_H
#define U3CLOADJSONBOOKDATAU3EC__ITERATOR0_T2579545968_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ContentManager/<LoadJSONBookData>c__Iterator0
struct  U3CLoadJSONBookDataU3Ec__Iterator0_t2579545968  : public RuntimeObject
{
public:
	// System.String ContentManager/<LoadJSONBookData>c__Iterator0::jsonBookUrl
	String_t* ___jsonBookUrl_0;
	// System.String ContentManager/<LoadJSONBookData>c__Iterator0::<fullBookURL>__0
	String_t* ___U3CfullBookURLU3E__0_1;
	// ContentManager ContentManager/<LoadJSONBookData>c__Iterator0::$this
	ContentManager_t211916338 * ___U24this_2;
	// System.Object ContentManager/<LoadJSONBookData>c__Iterator0::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean ContentManager/<LoadJSONBookData>c__Iterator0::$disposing
	bool ___U24disposing_4;
	// System.Int32 ContentManager/<LoadJSONBookData>c__Iterator0::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_jsonBookUrl_0() { return static_cast<int32_t>(offsetof(U3CLoadJSONBookDataU3Ec__Iterator0_t2579545968, ___jsonBookUrl_0)); }
	inline String_t* get_jsonBookUrl_0() const { return ___jsonBookUrl_0; }
	inline String_t** get_address_of_jsonBookUrl_0() { return &___jsonBookUrl_0; }
	inline void set_jsonBookUrl_0(String_t* value)
	{
		___jsonBookUrl_0 = value;
		Il2CppCodeGenWriteBarrier((&___jsonBookUrl_0), value);
	}

	inline static int32_t get_offset_of_U3CfullBookURLU3E__0_1() { return static_cast<int32_t>(offsetof(U3CLoadJSONBookDataU3Ec__Iterator0_t2579545968, ___U3CfullBookURLU3E__0_1)); }
	inline String_t* get_U3CfullBookURLU3E__0_1() const { return ___U3CfullBookURLU3E__0_1; }
	inline String_t** get_address_of_U3CfullBookURLU3E__0_1() { return &___U3CfullBookURLU3E__0_1; }
	inline void set_U3CfullBookURLU3E__0_1(String_t* value)
	{
		___U3CfullBookURLU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CfullBookURLU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CLoadJSONBookDataU3Ec__Iterator0_t2579545968, ___U24this_2)); }
	inline ContentManager_t211916338 * get_U24this_2() const { return ___U24this_2; }
	inline ContentManager_t211916338 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(ContentManager_t211916338 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CLoadJSONBookDataU3Ec__Iterator0_t2579545968, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CLoadJSONBookDataU3Ec__Iterator0_t2579545968, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CLoadJSONBookDataU3Ec__Iterator0_t2579545968, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADJSONBOOKDATAU3EC__ITERATOR0_T2579545968_H
#ifndef U3CSETUPURL_IMPU3EC__ITERATOR0_T1720507757_H
#define U3CSETUPURL_IMPU3EC__ITERATOR0_T1720507757_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GrattageManager/<setupURL_IMP>c__Iterator0
struct  U3CsetupURL_IMPU3Ec__Iterator0_t1720507757  : public RuntimeObject
{
public:
	// UnityEngine.WWW GrattageManager/<setupURL_IMP>c__Iterator0::<wwwFront>__0
	WWW_t3688466362 * ___U3CwwwFrontU3E__0_0;
	// UnityEngine.WWW GrattageManager/<setupURL_IMP>c__Iterator0::<wwwBack>__0
	WWW_t3688466362 * ___U3CwwwBackU3E__0_1;
	// GrattageManager GrattageManager/<setupURL_IMP>c__Iterator0::$this
	GrattageManager_t1333860694 * ___U24this_2;
	// System.Object GrattageManager/<setupURL_IMP>c__Iterator0::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean GrattageManager/<setupURL_IMP>c__Iterator0::$disposing
	bool ___U24disposing_4;
	// System.Int32 GrattageManager/<setupURL_IMP>c__Iterator0::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U3CwwwFrontU3E__0_0() { return static_cast<int32_t>(offsetof(U3CsetupURL_IMPU3Ec__Iterator0_t1720507757, ___U3CwwwFrontU3E__0_0)); }
	inline WWW_t3688466362 * get_U3CwwwFrontU3E__0_0() const { return ___U3CwwwFrontU3E__0_0; }
	inline WWW_t3688466362 ** get_address_of_U3CwwwFrontU3E__0_0() { return &___U3CwwwFrontU3E__0_0; }
	inline void set_U3CwwwFrontU3E__0_0(WWW_t3688466362 * value)
	{
		___U3CwwwFrontU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwwwFrontU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3CwwwBackU3E__0_1() { return static_cast<int32_t>(offsetof(U3CsetupURL_IMPU3Ec__Iterator0_t1720507757, ___U3CwwwBackU3E__0_1)); }
	inline WWW_t3688466362 * get_U3CwwwBackU3E__0_1() const { return ___U3CwwwBackU3E__0_1; }
	inline WWW_t3688466362 ** get_address_of_U3CwwwBackU3E__0_1() { return &___U3CwwwBackU3E__0_1; }
	inline void set_U3CwwwBackU3E__0_1(WWW_t3688466362 * value)
	{
		___U3CwwwBackU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwwwBackU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CsetupURL_IMPU3Ec__Iterator0_t1720507757, ___U24this_2)); }
	inline GrattageManager_t1333860694 * get_U24this_2() const { return ___U24this_2; }
	inline GrattageManager_t1333860694 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(GrattageManager_t1333860694 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CsetupURL_IMPU3Ec__Iterator0_t1720507757, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CsetupURL_IMPU3Ec__Iterator0_t1720507757, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CsetupURL_IMPU3Ec__Iterator0_t1720507757, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSETUPURL_IMPU3EC__ITERATOR0_T1720507757_H
#ifndef U3CSETUPURL_IMPU3EC__ITERATOR0_T540476453_H
#define U3CSETUPURL_IMPU3EC__ITERATOR0_T540476453_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GrattageScript/<setupURL_IMP>c__Iterator0
struct  U3CsetupURL_IMPU3Ec__Iterator0_t540476453  : public RuntimeObject
{
public:
	// System.Single GrattageScript/<setupURL_IMP>c__Iterator0::posX
	float ___posX_0;
	// System.Single GrattageScript/<setupURL_IMP>c__Iterator0::posY
	float ___posY_1;
	// System.Single GrattageScript/<setupURL_IMP>c__Iterator0::layer
	float ___layer_2;
	// System.Single GrattageScript/<setupURL_IMP>c__Iterator0::sizeX
	float ___sizeX_3;
	// System.Single GrattageScript/<setupURL_IMP>c__Iterator0::sizeY
	float ___sizeY_4;
	// UnityEngine.WWW GrattageScript/<setupURL_IMP>c__Iterator0::<wwwFront>__0
	WWW_t3688466362 * ___U3CwwwFrontU3E__0_5;
	// UnityEngine.WWW GrattageScript/<setupURL_IMP>c__Iterator0::<wwwBack>__0
	WWW_t3688466362 * ___U3CwwwBackU3E__0_6;
	// GrattageScript GrattageScript/<setupURL_IMP>c__Iterator0::$this
	GrattageScript_t3667968469 * ___U24this_7;
	// System.Object GrattageScript/<setupURL_IMP>c__Iterator0::$current
	RuntimeObject * ___U24current_8;
	// System.Boolean GrattageScript/<setupURL_IMP>c__Iterator0::$disposing
	bool ___U24disposing_9;
	// System.Int32 GrattageScript/<setupURL_IMP>c__Iterator0::$PC
	int32_t ___U24PC_10;

public:
	inline static int32_t get_offset_of_posX_0() { return static_cast<int32_t>(offsetof(U3CsetupURL_IMPU3Ec__Iterator0_t540476453, ___posX_0)); }
	inline float get_posX_0() const { return ___posX_0; }
	inline float* get_address_of_posX_0() { return &___posX_0; }
	inline void set_posX_0(float value)
	{
		___posX_0 = value;
	}

	inline static int32_t get_offset_of_posY_1() { return static_cast<int32_t>(offsetof(U3CsetupURL_IMPU3Ec__Iterator0_t540476453, ___posY_1)); }
	inline float get_posY_1() const { return ___posY_1; }
	inline float* get_address_of_posY_1() { return &___posY_1; }
	inline void set_posY_1(float value)
	{
		___posY_1 = value;
	}

	inline static int32_t get_offset_of_layer_2() { return static_cast<int32_t>(offsetof(U3CsetupURL_IMPU3Ec__Iterator0_t540476453, ___layer_2)); }
	inline float get_layer_2() const { return ___layer_2; }
	inline float* get_address_of_layer_2() { return &___layer_2; }
	inline void set_layer_2(float value)
	{
		___layer_2 = value;
	}

	inline static int32_t get_offset_of_sizeX_3() { return static_cast<int32_t>(offsetof(U3CsetupURL_IMPU3Ec__Iterator0_t540476453, ___sizeX_3)); }
	inline float get_sizeX_3() const { return ___sizeX_3; }
	inline float* get_address_of_sizeX_3() { return &___sizeX_3; }
	inline void set_sizeX_3(float value)
	{
		___sizeX_3 = value;
	}

	inline static int32_t get_offset_of_sizeY_4() { return static_cast<int32_t>(offsetof(U3CsetupURL_IMPU3Ec__Iterator0_t540476453, ___sizeY_4)); }
	inline float get_sizeY_4() const { return ___sizeY_4; }
	inline float* get_address_of_sizeY_4() { return &___sizeY_4; }
	inline void set_sizeY_4(float value)
	{
		___sizeY_4 = value;
	}

	inline static int32_t get_offset_of_U3CwwwFrontU3E__0_5() { return static_cast<int32_t>(offsetof(U3CsetupURL_IMPU3Ec__Iterator0_t540476453, ___U3CwwwFrontU3E__0_5)); }
	inline WWW_t3688466362 * get_U3CwwwFrontU3E__0_5() const { return ___U3CwwwFrontU3E__0_5; }
	inline WWW_t3688466362 ** get_address_of_U3CwwwFrontU3E__0_5() { return &___U3CwwwFrontU3E__0_5; }
	inline void set_U3CwwwFrontU3E__0_5(WWW_t3688466362 * value)
	{
		___U3CwwwFrontU3E__0_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwwwFrontU3E__0_5), value);
	}

	inline static int32_t get_offset_of_U3CwwwBackU3E__0_6() { return static_cast<int32_t>(offsetof(U3CsetupURL_IMPU3Ec__Iterator0_t540476453, ___U3CwwwBackU3E__0_6)); }
	inline WWW_t3688466362 * get_U3CwwwBackU3E__0_6() const { return ___U3CwwwBackU3E__0_6; }
	inline WWW_t3688466362 ** get_address_of_U3CwwwBackU3E__0_6() { return &___U3CwwwBackU3E__0_6; }
	inline void set_U3CwwwBackU3E__0_6(WWW_t3688466362 * value)
	{
		___U3CwwwBackU3E__0_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwwwBackU3E__0_6), value);
	}

	inline static int32_t get_offset_of_U24this_7() { return static_cast<int32_t>(offsetof(U3CsetupURL_IMPU3Ec__Iterator0_t540476453, ___U24this_7)); }
	inline GrattageScript_t3667968469 * get_U24this_7() const { return ___U24this_7; }
	inline GrattageScript_t3667968469 ** get_address_of_U24this_7() { return &___U24this_7; }
	inline void set_U24this_7(GrattageScript_t3667968469 * value)
	{
		___U24this_7 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_7), value);
	}

	inline static int32_t get_offset_of_U24current_8() { return static_cast<int32_t>(offsetof(U3CsetupURL_IMPU3Ec__Iterator0_t540476453, ___U24current_8)); }
	inline RuntimeObject * get_U24current_8() const { return ___U24current_8; }
	inline RuntimeObject ** get_address_of_U24current_8() { return &___U24current_8; }
	inline void set_U24current_8(RuntimeObject * value)
	{
		___U24current_8 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_8), value);
	}

	inline static int32_t get_offset_of_U24disposing_9() { return static_cast<int32_t>(offsetof(U3CsetupURL_IMPU3Ec__Iterator0_t540476453, ___U24disposing_9)); }
	inline bool get_U24disposing_9() const { return ___U24disposing_9; }
	inline bool* get_address_of_U24disposing_9() { return &___U24disposing_9; }
	inline void set_U24disposing_9(bool value)
	{
		___U24disposing_9 = value;
	}

	inline static int32_t get_offset_of_U24PC_10() { return static_cast<int32_t>(offsetof(U3CsetupURL_IMPU3Ec__Iterator0_t540476453, ___U24PC_10)); }
	inline int32_t get_U24PC_10() const { return ___U24PC_10; }
	inline int32_t* get_address_of_U24PC_10() { return &___U24PC_10; }
	inline void set_U24PC_10(int32_t value)
	{
		___U24PC_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSETUPURL_IMPU3EC__ITERATOR0_T540476453_H
#ifndef JSONPARSER_T75429706_H
#define JSONPARSER_T75429706_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// JSONParser
struct  JSONParser_t75429706  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONPARSER_T75429706_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef ARCONTROLLER_T116632334_H
#define ARCONTROLLER_T116632334_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.ARController
struct  ARController_t116632334  : public RuntimeObject
{
public:
	// Vuforia.VuforiaBehaviour Vuforia.ARController::mVuforiaBehaviour
	VuforiaBehaviour_t2151848540 * ___mVuforiaBehaviour_0;

public:
	inline static int32_t get_offset_of_mVuforiaBehaviour_0() { return static_cast<int32_t>(offsetof(ARController_t116632334, ___mVuforiaBehaviour_0)); }
	inline VuforiaBehaviour_t2151848540 * get_mVuforiaBehaviour_0() const { return ___mVuforiaBehaviour_0; }
	inline VuforiaBehaviour_t2151848540 ** get_address_of_mVuforiaBehaviour_0() { return &___mVuforiaBehaviour_0; }
	inline void set_mVuforiaBehaviour_0(VuforiaBehaviour_t2151848540 * value)
	{
		___mVuforiaBehaviour_0 = value;
		Il2CppCodeGenWriteBarrier((&___mVuforiaBehaviour_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARCONTROLLER_T116632334_H
#ifndef EYEWEARCALIBRATIONPROFILEMANAGER_T947793426_H
#define EYEWEARCALIBRATIONPROFILEMANAGER_T947793426_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.EyewearCalibrationProfileManager
struct  EyewearCalibrationProfileManager_t947793426  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EYEWEARCALIBRATIONPROFILEMANAGER_T947793426_H
#ifndef EYEWEARUSERCALIBRATOR_T2926839199_H
#define EYEWEARUSERCALIBRATOR_T2926839199_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.EyewearUserCalibrator
struct  EyewearUserCalibrator_t2926839199  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EYEWEARUSERCALIBRATOR_T2926839199_H
#ifndef TRACKER_T2709586299_H
#define TRACKER_T2709586299_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.Tracker
struct  Tracker_t2709586299  : public RuntimeObject
{
public:
	// System.Boolean Vuforia.Tracker::<IsActive>k__BackingField
	bool ___U3CIsActiveU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CIsActiveU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Tracker_t2709586299, ___U3CIsActiveU3Ek__BackingField_0)); }
	inline bool get_U3CIsActiveU3Ek__BackingField_0() const { return ___U3CIsActiveU3Ek__BackingField_0; }
	inline bool* get_address_of_U3CIsActiveU3Ek__BackingField_0() { return &___U3CIsActiveU3Ek__BackingField_0; }
	inline void set_U3CIsActiveU3Ek__BackingField_0(bool value)
	{
		___U3CIsActiveU3Ek__BackingField_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKER_T2709586299_H
#ifndef TRACKERMANAGER_T1703337244_H
#define TRACKERMANAGER_T1703337244_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.TrackerManager
struct  TrackerManager_t1703337244  : public RuntimeObject
{
public:
	// Vuforia.StateManager Vuforia.TrackerManager::mStateManager
	StateManager_t1982749557 * ___mStateManager_1;
	// System.Collections.Generic.Dictionary`2<System.Type,Vuforia.Tracker> Vuforia.TrackerManager::mTrackers
	Dictionary_2_t858966067 * ___mTrackers_2;
	// System.Collections.Generic.Dictionary`2<System.Type,System.Func`2<System.Type,Vuforia.Tracker>> Vuforia.TrackerManager::mTrackerCreators
	Dictionary_2_t1322931057 * ___mTrackerCreators_3;
	// System.Collections.Generic.Dictionary`2<System.Type,System.Func`2<Vuforia.Tracker,System.Boolean>> Vuforia.TrackerManager::mTrackerNativeDeinitializers
	Dictionary_2_t2058017892 * ___mTrackerNativeDeinitializers_4;

public:
	inline static int32_t get_offset_of_mStateManager_1() { return static_cast<int32_t>(offsetof(TrackerManager_t1703337244, ___mStateManager_1)); }
	inline StateManager_t1982749557 * get_mStateManager_1() const { return ___mStateManager_1; }
	inline StateManager_t1982749557 ** get_address_of_mStateManager_1() { return &___mStateManager_1; }
	inline void set_mStateManager_1(StateManager_t1982749557 * value)
	{
		___mStateManager_1 = value;
		Il2CppCodeGenWriteBarrier((&___mStateManager_1), value);
	}

	inline static int32_t get_offset_of_mTrackers_2() { return static_cast<int32_t>(offsetof(TrackerManager_t1703337244, ___mTrackers_2)); }
	inline Dictionary_2_t858966067 * get_mTrackers_2() const { return ___mTrackers_2; }
	inline Dictionary_2_t858966067 ** get_address_of_mTrackers_2() { return &___mTrackers_2; }
	inline void set_mTrackers_2(Dictionary_2_t858966067 * value)
	{
		___mTrackers_2 = value;
		Il2CppCodeGenWriteBarrier((&___mTrackers_2), value);
	}

	inline static int32_t get_offset_of_mTrackerCreators_3() { return static_cast<int32_t>(offsetof(TrackerManager_t1703337244, ___mTrackerCreators_3)); }
	inline Dictionary_2_t1322931057 * get_mTrackerCreators_3() const { return ___mTrackerCreators_3; }
	inline Dictionary_2_t1322931057 ** get_address_of_mTrackerCreators_3() { return &___mTrackerCreators_3; }
	inline void set_mTrackerCreators_3(Dictionary_2_t1322931057 * value)
	{
		___mTrackerCreators_3 = value;
		Il2CppCodeGenWriteBarrier((&___mTrackerCreators_3), value);
	}

	inline static int32_t get_offset_of_mTrackerNativeDeinitializers_4() { return static_cast<int32_t>(offsetof(TrackerManager_t1703337244, ___mTrackerNativeDeinitializers_4)); }
	inline Dictionary_2_t2058017892 * get_mTrackerNativeDeinitializers_4() const { return ___mTrackerNativeDeinitializers_4; }
	inline Dictionary_2_t2058017892 ** get_address_of_mTrackerNativeDeinitializers_4() { return &___mTrackerNativeDeinitializers_4; }
	inline void set_mTrackerNativeDeinitializers_4(Dictionary_2_t2058017892 * value)
	{
		___mTrackerNativeDeinitializers_4 = value;
		Il2CppCodeGenWriteBarrier((&___mTrackerNativeDeinitializers_4), value);
	}
};

struct TrackerManager_t1703337244_StaticFields
{
public:
	// Vuforia.ITrackerManager Vuforia.TrackerManager::mInstance
	RuntimeObject* ___mInstance_0;

public:
	inline static int32_t get_offset_of_mInstance_0() { return static_cast<int32_t>(offsetof(TrackerManager_t1703337244_StaticFields, ___mInstance_0)); }
	inline RuntimeObject* get_mInstance_0() const { return ___mInstance_0; }
	inline RuntimeObject** get_address_of_mInstance_0() { return &___mInstance_0; }
	inline void set_mInstance_0(RuntimeObject* value)
	{
		___mInstance_0 = value;
		Il2CppCodeGenWriteBarrier((&___mInstance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKERMANAGER_T1703337244_H
#ifndef U3CU3EC_T1451390621_H
#define U3CU3EC_T1451390621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.TrackerManager/<>c
struct  U3CU3Ec_t1451390621  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t1451390621_StaticFields
{
public:
	// Vuforia.TrackerManager/<>c Vuforia.TrackerManager/<>c::<>9
	U3CU3Ec_t1451390621 * ___U3CU3E9_0;
	// System.Func`2<System.Type,Vuforia.Tracker> Vuforia.TrackerManager/<>c::<>9__8_0
	Func_2_t3173551289 * ___U3CU3E9__8_0_1;
	// System.Func`2<System.Type,Vuforia.Tracker> Vuforia.TrackerManager/<>c::<>9__8_1
	Func_2_t3173551289 * ___U3CU3E9__8_1_2;
	// System.Func`2<Vuforia.Tracker,System.Boolean> Vuforia.TrackerManager/<>c::<>9__8_2
	Func_2_t3908638124 * ___U3CU3E9__8_2_3;
	// System.Func`2<Vuforia.Tracker,System.Boolean> Vuforia.TrackerManager/<>c::<>9__8_3
	Func_2_t3908638124 * ___U3CU3E9__8_3_4;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1451390621_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t1451390621 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t1451390621 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t1451390621 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__8_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1451390621_StaticFields, ___U3CU3E9__8_0_1)); }
	inline Func_2_t3173551289 * get_U3CU3E9__8_0_1() const { return ___U3CU3E9__8_0_1; }
	inline Func_2_t3173551289 ** get_address_of_U3CU3E9__8_0_1() { return &___U3CU3E9__8_0_1; }
	inline void set_U3CU3E9__8_0_1(Func_2_t3173551289 * value)
	{
		___U3CU3E9__8_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__8_0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__8_1_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1451390621_StaticFields, ___U3CU3E9__8_1_2)); }
	inline Func_2_t3173551289 * get_U3CU3E9__8_1_2() const { return ___U3CU3E9__8_1_2; }
	inline Func_2_t3173551289 ** get_address_of_U3CU3E9__8_1_2() { return &___U3CU3E9__8_1_2; }
	inline void set_U3CU3E9__8_1_2(Func_2_t3173551289 * value)
	{
		___U3CU3E9__8_1_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__8_1_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__8_2_3() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1451390621_StaticFields, ___U3CU3E9__8_2_3)); }
	inline Func_2_t3908638124 * get_U3CU3E9__8_2_3() const { return ___U3CU3E9__8_2_3; }
	inline Func_2_t3908638124 ** get_address_of_U3CU3E9__8_2_3() { return &___U3CU3E9__8_2_3; }
	inline void set_U3CU3E9__8_2_3(Func_2_t3908638124 * value)
	{
		___U3CU3E9__8_2_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__8_2_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__8_3_4() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1451390621_StaticFields, ___U3CU3E9__8_3_4)); }
	inline Func_2_t3908638124 * get_U3CU3E9__8_3_4() const { return ___U3CU3E9__8_3_4; }
	inline Func_2_t3908638124 ** get_address_of_U3CU3E9__8_3_4() { return &___U3CU3E9__8_3_4; }
	inline void set_U3CU3E9__8_3_4(Func_2_t3908638124 * value)
	{
		___U3CU3E9__8_3_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__8_3_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T1451390621_H
#ifndef GLOBALVARS_T2485087241_H
#define GLOBALVARS_T2485087241_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaRuntimeUtilities/GlobalVars
struct  GlobalVars_t2485087241  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLOBALVARS_T2485087241_H
#ifndef VUFORIAUNITY_T1788908542_H
#define VUFORIAUNITY_T1788908542_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaUnity
struct  VuforiaUnity_t1788908542  : public RuntimeObject
{
public:

public:
};

struct VuforiaUnity_t1788908542_StaticFields
{
public:
	// Vuforia.IHoloLensApiAbstraction Vuforia.VuforiaUnity::mHoloLensApiAbstraction
	RuntimeObject* ___mHoloLensApiAbstraction_0;
	// System.Boolean Vuforia.VuforiaUnity::mRendererDirty
	bool ___mRendererDirty_2;
	// System.Int32 Vuforia.VuforiaUnity::mWrapperType
	int32_t ___mWrapperType_3;

public:
	inline static int32_t get_offset_of_mHoloLensApiAbstraction_0() { return static_cast<int32_t>(offsetof(VuforiaUnity_t1788908542_StaticFields, ___mHoloLensApiAbstraction_0)); }
	inline RuntimeObject* get_mHoloLensApiAbstraction_0() const { return ___mHoloLensApiAbstraction_0; }
	inline RuntimeObject** get_address_of_mHoloLensApiAbstraction_0() { return &___mHoloLensApiAbstraction_0; }
	inline void set_mHoloLensApiAbstraction_0(RuntimeObject* value)
	{
		___mHoloLensApiAbstraction_0 = value;
		Il2CppCodeGenWriteBarrier((&___mHoloLensApiAbstraction_0), value);
	}

	inline static int32_t get_offset_of_mRendererDirty_2() { return static_cast<int32_t>(offsetof(VuforiaUnity_t1788908542_StaticFields, ___mRendererDirty_2)); }
	inline bool get_mRendererDirty_2() const { return ___mRendererDirty_2; }
	inline bool* get_address_of_mRendererDirty_2() { return &___mRendererDirty_2; }
	inline void set_mRendererDirty_2(bool value)
	{
		___mRendererDirty_2 = value;
	}

	inline static int32_t get_offset_of_mWrapperType_3() { return static_cast<int32_t>(offsetof(VuforiaUnity_t1788908542_StaticFields, ___mWrapperType_3)); }
	inline int32_t get_mWrapperType_3() const { return ___mWrapperType_3; }
	inline int32_t* get_address_of_mWrapperType_3() { return &___mWrapperType_3; }
	inline void set_mWrapperType_3(int32_t value)
	{
		___mWrapperType_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VUFORIAUNITY_T1788908542_H
#ifndef U3CU3EC_T3582055403_H
#define U3CU3EC_T3582055403_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.WebCamARController/<>c
struct  U3CU3Ec_t3582055403  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t3582055403_StaticFields
{
public:
	// Vuforia.WebCamARController/<>c Vuforia.WebCamARController/<>c::<>9
	U3CU3Ec_t3582055403 * ___U3CU3E9_0;
	// System.Func`3<System.String,Vuforia.WebCamProfile/ProfileData,Vuforia.IWebCamTexAdaptor> Vuforia.WebCamARController/<>c::<>9__7_0
	Func_3_t3440825513 * ___U3CU3E9__7_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t3582055403_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t3582055403 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t3582055403 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t3582055403 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__7_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t3582055403_StaticFields, ___U3CU3E9__7_0_1)); }
	inline Func_3_t3440825513 * get_U3CU3E9__7_0_1() const { return ___U3CU3E9__7_0_1; }
	inline Func_3_t3440825513 ** get_address_of_U3CU3E9__7_0_1() { return &___U3CU3E9__7_0_1; }
	inline void set_U3CU3E9__7_0_1(Func_3_t3440825513 * value)
	{
		___U3CU3E9__7_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__7_0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T3582055403_H
#ifndef __STATICARRAYINITTYPESIZEU3D24_T3517759979_H
#define __STATICARRAYINITTYPESIZEU3D24_T3517759979_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24
struct  __StaticArrayInitTypeSizeU3D24_t3517759979 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D24_t3517759979__padding[24];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D24_T3517759979_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef MATRIX4X4_T1817901843_H
#define MATRIX4X4_T1817901843_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Matrix4x4
struct  Matrix4x4_t1817901843 
{
public:
	// System.Single UnityEngine.Matrix4x4::m00
	float ___m00_0;
	// System.Single UnityEngine.Matrix4x4::m10
	float ___m10_1;
	// System.Single UnityEngine.Matrix4x4::m20
	float ___m20_2;
	// System.Single UnityEngine.Matrix4x4::m30
	float ___m30_3;
	// System.Single UnityEngine.Matrix4x4::m01
	float ___m01_4;
	// System.Single UnityEngine.Matrix4x4::m11
	float ___m11_5;
	// System.Single UnityEngine.Matrix4x4::m21
	float ___m21_6;
	// System.Single UnityEngine.Matrix4x4::m31
	float ___m31_7;
	// System.Single UnityEngine.Matrix4x4::m02
	float ___m02_8;
	// System.Single UnityEngine.Matrix4x4::m12
	float ___m12_9;
	// System.Single UnityEngine.Matrix4x4::m22
	float ___m22_10;
	// System.Single UnityEngine.Matrix4x4::m32
	float ___m32_11;
	// System.Single UnityEngine.Matrix4x4::m03
	float ___m03_12;
	// System.Single UnityEngine.Matrix4x4::m13
	float ___m13_13;
	// System.Single UnityEngine.Matrix4x4::m23
	float ___m23_14;
	// System.Single UnityEngine.Matrix4x4::m33
	float ___m33_15;

public:
	inline static int32_t get_offset_of_m00_0() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m00_0)); }
	inline float get_m00_0() const { return ___m00_0; }
	inline float* get_address_of_m00_0() { return &___m00_0; }
	inline void set_m00_0(float value)
	{
		___m00_0 = value;
	}

	inline static int32_t get_offset_of_m10_1() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m10_1)); }
	inline float get_m10_1() const { return ___m10_1; }
	inline float* get_address_of_m10_1() { return &___m10_1; }
	inline void set_m10_1(float value)
	{
		___m10_1 = value;
	}

	inline static int32_t get_offset_of_m20_2() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m20_2)); }
	inline float get_m20_2() const { return ___m20_2; }
	inline float* get_address_of_m20_2() { return &___m20_2; }
	inline void set_m20_2(float value)
	{
		___m20_2 = value;
	}

	inline static int32_t get_offset_of_m30_3() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m30_3)); }
	inline float get_m30_3() const { return ___m30_3; }
	inline float* get_address_of_m30_3() { return &___m30_3; }
	inline void set_m30_3(float value)
	{
		___m30_3 = value;
	}

	inline static int32_t get_offset_of_m01_4() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m01_4)); }
	inline float get_m01_4() const { return ___m01_4; }
	inline float* get_address_of_m01_4() { return &___m01_4; }
	inline void set_m01_4(float value)
	{
		___m01_4 = value;
	}

	inline static int32_t get_offset_of_m11_5() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m11_5)); }
	inline float get_m11_5() const { return ___m11_5; }
	inline float* get_address_of_m11_5() { return &___m11_5; }
	inline void set_m11_5(float value)
	{
		___m11_5 = value;
	}

	inline static int32_t get_offset_of_m21_6() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m21_6)); }
	inline float get_m21_6() const { return ___m21_6; }
	inline float* get_address_of_m21_6() { return &___m21_6; }
	inline void set_m21_6(float value)
	{
		___m21_6 = value;
	}

	inline static int32_t get_offset_of_m31_7() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m31_7)); }
	inline float get_m31_7() const { return ___m31_7; }
	inline float* get_address_of_m31_7() { return &___m31_7; }
	inline void set_m31_7(float value)
	{
		___m31_7 = value;
	}

	inline static int32_t get_offset_of_m02_8() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m02_8)); }
	inline float get_m02_8() const { return ___m02_8; }
	inline float* get_address_of_m02_8() { return &___m02_8; }
	inline void set_m02_8(float value)
	{
		___m02_8 = value;
	}

	inline static int32_t get_offset_of_m12_9() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m12_9)); }
	inline float get_m12_9() const { return ___m12_9; }
	inline float* get_address_of_m12_9() { return &___m12_9; }
	inline void set_m12_9(float value)
	{
		___m12_9 = value;
	}

	inline static int32_t get_offset_of_m22_10() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m22_10)); }
	inline float get_m22_10() const { return ___m22_10; }
	inline float* get_address_of_m22_10() { return &___m22_10; }
	inline void set_m22_10(float value)
	{
		___m22_10 = value;
	}

	inline static int32_t get_offset_of_m32_11() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m32_11)); }
	inline float get_m32_11() const { return ___m32_11; }
	inline float* get_address_of_m32_11() { return &___m32_11; }
	inline void set_m32_11(float value)
	{
		___m32_11 = value;
	}

	inline static int32_t get_offset_of_m03_12() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m03_12)); }
	inline float get_m03_12() const { return ___m03_12; }
	inline float* get_address_of_m03_12() { return &___m03_12; }
	inline void set_m03_12(float value)
	{
		___m03_12 = value;
	}

	inline static int32_t get_offset_of_m13_13() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m13_13)); }
	inline float get_m13_13() const { return ___m13_13; }
	inline float* get_address_of_m13_13() { return &___m13_13; }
	inline void set_m13_13(float value)
	{
		___m13_13 = value;
	}

	inline static int32_t get_offset_of_m23_14() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m23_14)); }
	inline float get_m23_14() const { return ___m23_14; }
	inline float* get_address_of_m23_14() { return &___m23_14; }
	inline void set_m23_14(float value)
	{
		___m23_14 = value;
	}

	inline static int32_t get_offset_of_m33_15() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m33_15)); }
	inline float get_m33_15() const { return ___m33_15; }
	inline float* get_address_of_m33_15() { return &___m33_15; }
	inline void set_m33_15(float value)
	{
		___m33_15 = value;
	}
};

struct Matrix4x4_t1817901843_StaticFields
{
public:
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::zeroMatrix
	Matrix4x4_t1817901843  ___zeroMatrix_16;
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::identityMatrix
	Matrix4x4_t1817901843  ___identityMatrix_17;

public:
	inline static int32_t get_offset_of_zeroMatrix_16() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843_StaticFields, ___zeroMatrix_16)); }
	inline Matrix4x4_t1817901843  get_zeroMatrix_16() const { return ___zeroMatrix_16; }
	inline Matrix4x4_t1817901843 * get_address_of_zeroMatrix_16() { return &___zeroMatrix_16; }
	inline void set_zeroMatrix_16(Matrix4x4_t1817901843  value)
	{
		___zeroMatrix_16 = value;
	}

	inline static int32_t get_offset_of_identityMatrix_17() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843_StaticFields, ___identityMatrix_17)); }
	inline Matrix4x4_t1817901843  get_identityMatrix_17() const { return ___identityMatrix_17; }
	inline Matrix4x4_t1817901843 * get_address_of_identityMatrix_17() { return &___identityMatrix_17; }
	inline void set_identityMatrix_17(Matrix4x4_t1817901843  value)
	{
		___identityMatrix_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATRIX4X4_T1817901843_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_5)); }
	inline Vector3_t3722313464  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_t3722313464  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_6)); }
	inline Vector3_t3722313464  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_t3722313464 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_t3722313464  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_7)); }
	inline Vector3_t3722313464  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_t3722313464 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_t3722313464  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_8)); }
	inline Vector3_t3722313464  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_t3722313464 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_t3722313464  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_9)); }
	inline Vector3_t3722313464  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_t3722313464 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_t3722313464  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_10)); }
	inline Vector3_t3722313464  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_t3722313464 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_t3722313464  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_11)); }
	inline Vector3_t3722313464  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_t3722313464  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_12)); }
	inline Vector3_t3722313464  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_t3722313464 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_t3722313464  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef VECTOR4_T3319028937_H
#define VECTOR4_T3319028937_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector4
struct  Vector4_t3319028937 
{
public:
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}

	inline static int32_t get_offset_of_w_4() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___w_4)); }
	inline float get_w_4() const { return ___w_4; }
	inline float* get_address_of_w_4() { return &___w_4; }
	inline void set_w_4(float value)
	{
		___w_4 = value;
	}
};

struct Vector4_t3319028937_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_t3319028937  ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_t3319028937  ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_t3319028937  ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_t3319028937  ___negativeInfinityVector_8;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___zeroVector_5)); }
	inline Vector4_t3319028937  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector4_t3319028937 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector4_t3319028937  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___oneVector_6)); }
	inline Vector4_t3319028937  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector4_t3319028937 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector4_t3319028937  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_7() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___positiveInfinityVector_7)); }
	inline Vector4_t3319028937  get_positiveInfinityVector_7() const { return ___positiveInfinityVector_7; }
	inline Vector4_t3319028937 * get_address_of_positiveInfinityVector_7() { return &___positiveInfinityVector_7; }
	inline void set_positiveInfinityVector_7(Vector4_t3319028937  value)
	{
		___positiveInfinityVector_7 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___negativeInfinityVector_8)); }
	inline Vector4_t3319028937  get_negativeInfinityVector_8() const { return ___negativeInfinityVector_8; }
	inline Vector4_t3319028937 * get_address_of_negativeInfinityVector_8() { return &___negativeInfinityVector_8; }
	inline void set_negativeInfinityVector_8(Vector4_t3319028937  value)
	{
		___negativeInfinityVector_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR4_T3319028937_H
#ifndef OBJECTTRACKER_T4177997237_H
#define OBJECTTRACKER_T4177997237_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.ObjectTracker
struct  ObjectTracker_t4177997237  : public Tracker_t2709586299
{
public:
	// System.Collections.Generic.List`1<Vuforia.DataSet> Vuforia.ObjectTracker::mActiveDataSets
	List_1_t463142320 * ___mActiveDataSets_1;
	// System.Collections.Generic.List`1<Vuforia.DataSet> Vuforia.ObjectTracker::mDataSets
	List_1_t463142320 * ___mDataSets_2;
	// Vuforia.ImageTargetBuilder Vuforia.ObjectTracker::mImageTargetBuilder
	ImageTargetBuilder_t2430893908 * ___mImageTargetBuilder_3;
	// Vuforia.TargetFinder Vuforia.ObjectTracker::mTargetFinder
	TargetFinder_t2439332195 * ___mTargetFinder_4;

public:
	inline static int32_t get_offset_of_mActiveDataSets_1() { return static_cast<int32_t>(offsetof(ObjectTracker_t4177997237, ___mActiveDataSets_1)); }
	inline List_1_t463142320 * get_mActiveDataSets_1() const { return ___mActiveDataSets_1; }
	inline List_1_t463142320 ** get_address_of_mActiveDataSets_1() { return &___mActiveDataSets_1; }
	inline void set_mActiveDataSets_1(List_1_t463142320 * value)
	{
		___mActiveDataSets_1 = value;
		Il2CppCodeGenWriteBarrier((&___mActiveDataSets_1), value);
	}

	inline static int32_t get_offset_of_mDataSets_2() { return static_cast<int32_t>(offsetof(ObjectTracker_t4177997237, ___mDataSets_2)); }
	inline List_1_t463142320 * get_mDataSets_2() const { return ___mDataSets_2; }
	inline List_1_t463142320 ** get_address_of_mDataSets_2() { return &___mDataSets_2; }
	inline void set_mDataSets_2(List_1_t463142320 * value)
	{
		___mDataSets_2 = value;
		Il2CppCodeGenWriteBarrier((&___mDataSets_2), value);
	}

	inline static int32_t get_offset_of_mImageTargetBuilder_3() { return static_cast<int32_t>(offsetof(ObjectTracker_t4177997237, ___mImageTargetBuilder_3)); }
	inline ImageTargetBuilder_t2430893908 * get_mImageTargetBuilder_3() const { return ___mImageTargetBuilder_3; }
	inline ImageTargetBuilder_t2430893908 ** get_address_of_mImageTargetBuilder_3() { return &___mImageTargetBuilder_3; }
	inline void set_mImageTargetBuilder_3(ImageTargetBuilder_t2430893908 * value)
	{
		___mImageTargetBuilder_3 = value;
		Il2CppCodeGenWriteBarrier((&___mImageTargetBuilder_3), value);
	}

	inline static int32_t get_offset_of_mTargetFinder_4() { return static_cast<int32_t>(offsetof(ObjectTracker_t4177997237, ___mTargetFinder_4)); }
	inline TargetFinder_t2439332195 * get_mTargetFinder_4() const { return ___mTargetFinder_4; }
	inline TargetFinder_t2439332195 ** get_address_of_mTargetFinder_4() { return &___mTargetFinder_4; }
	inline void set_mTargetFinder_4(TargetFinder_t2439332195 * value)
	{
		___mTargetFinder_4 = value;
		Il2CppCodeGenWriteBarrier((&___mTargetFinder_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTTRACKER_T4177997237_H
#ifndef RECTANGLEDATA_T1039179782_H
#define RECTANGLEDATA_T1039179782_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.RectangleData
#pragma pack(push, tp, 1)
struct  RectangleData_t1039179782 
{
public:
	// System.Single Vuforia.RectangleData::leftTopX
	float ___leftTopX_0;
	// System.Single Vuforia.RectangleData::leftTopY
	float ___leftTopY_1;
	// System.Single Vuforia.RectangleData::rightBottomX
	float ___rightBottomX_2;
	// System.Single Vuforia.RectangleData::rightBottomY
	float ___rightBottomY_3;

public:
	inline static int32_t get_offset_of_leftTopX_0() { return static_cast<int32_t>(offsetof(RectangleData_t1039179782, ___leftTopX_0)); }
	inline float get_leftTopX_0() const { return ___leftTopX_0; }
	inline float* get_address_of_leftTopX_0() { return &___leftTopX_0; }
	inline void set_leftTopX_0(float value)
	{
		___leftTopX_0 = value;
	}

	inline static int32_t get_offset_of_leftTopY_1() { return static_cast<int32_t>(offsetof(RectangleData_t1039179782, ___leftTopY_1)); }
	inline float get_leftTopY_1() const { return ___leftTopY_1; }
	inline float* get_address_of_leftTopY_1() { return &___leftTopY_1; }
	inline void set_leftTopY_1(float value)
	{
		___leftTopY_1 = value;
	}

	inline static int32_t get_offset_of_rightBottomX_2() { return static_cast<int32_t>(offsetof(RectangleData_t1039179782, ___rightBottomX_2)); }
	inline float get_rightBottomX_2() const { return ___rightBottomX_2; }
	inline float* get_address_of_rightBottomX_2() { return &___rightBottomX_2; }
	inline void set_rightBottomX_2(float value)
	{
		___rightBottomX_2 = value;
	}

	inline static int32_t get_offset_of_rightBottomY_3() { return static_cast<int32_t>(offsetof(RectangleData_t1039179782, ___rightBottomY_3)); }
	inline float get_rightBottomY_3() const { return ___rightBottomY_3; }
	inline float* get_address_of_rightBottomY_3() { return &___rightBottomY_3; }
	inline void set_rightBottomY_3(float value)
	{
		___rightBottomY_3 = value;
	}
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECTANGLEDATA_T1039179782_H
#ifndef SIMPLETARGETDATA_T4194873257_H
#define SIMPLETARGETDATA_T4194873257_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.SimpleTargetData
#pragma pack(push, tp, 1)
struct  SimpleTargetData_t4194873257 
{
public:
	// System.Int32 Vuforia.SimpleTargetData::id
	int32_t ___id_0;
	// System.Int32 Vuforia.SimpleTargetData::unused
	int32_t ___unused_1;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(SimpleTargetData_t4194873257, ___id_0)); }
	inline int32_t get_id_0() const { return ___id_0; }
	inline int32_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int32_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_unused_1() { return static_cast<int32_t>(offsetof(SimpleTargetData_t4194873257, ___unused_1)); }
	inline int32_t get_unused_1() const { return ___unused_1; }
	inline int32_t* get_address_of_unused_1() { return &___unused_1; }
	inline void set_unused_1(int32_t value)
	{
		___unused_1 = value;
	}
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIMPLETARGETDATA_T4194873257_H
#ifndef TARGETFINDERSTATE_T3286805956_H
#define TARGETFINDERSTATE_T3286805956_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.TargetFinder/TargetFinderState
#pragma pack(push, tp, 1)
struct  TargetFinderState_t3286805956 
{
public:
	// System.Int32 Vuforia.TargetFinder/TargetFinderState::IsRequesting
	int32_t ___IsRequesting_0;
	// System.Int32 Vuforia.TargetFinder/TargetFinderState::UpdateState
	int32_t ___UpdateState_1;
	// System.Int32 Vuforia.TargetFinder/TargetFinderState::ResultCount
	int32_t ___ResultCount_2;
	// System.Int32 Vuforia.TargetFinder/TargetFinderState::unused
	int32_t ___unused_3;

public:
	inline static int32_t get_offset_of_IsRequesting_0() { return static_cast<int32_t>(offsetof(TargetFinderState_t3286805956, ___IsRequesting_0)); }
	inline int32_t get_IsRequesting_0() const { return ___IsRequesting_0; }
	inline int32_t* get_address_of_IsRequesting_0() { return &___IsRequesting_0; }
	inline void set_IsRequesting_0(int32_t value)
	{
		___IsRequesting_0 = value;
	}

	inline static int32_t get_offset_of_UpdateState_1() { return static_cast<int32_t>(offsetof(TargetFinderState_t3286805956, ___UpdateState_1)); }
	inline int32_t get_UpdateState_1() const { return ___UpdateState_1; }
	inline int32_t* get_address_of_UpdateState_1() { return &___UpdateState_1; }
	inline void set_UpdateState_1(int32_t value)
	{
		___UpdateState_1 = value;
	}

	inline static int32_t get_offset_of_ResultCount_2() { return static_cast<int32_t>(offsetof(TargetFinderState_t3286805956, ___ResultCount_2)); }
	inline int32_t get_ResultCount_2() const { return ___ResultCount_2; }
	inline int32_t* get_address_of_ResultCount_2() { return &___ResultCount_2; }
	inline void set_ResultCount_2(int32_t value)
	{
		___ResultCount_2 = value;
	}

	inline static int32_t get_offset_of_unused_3() { return static_cast<int32_t>(offsetof(TargetFinderState_t3286805956, ___unused_3)); }
	inline int32_t get_unused_3() const { return ___unused_3; }
	inline int32_t* get_address_of_unused_3() { return &___unused_3; }
	inline void set_unused_3(int32_t value)
	{
		___unused_3 = value;
	}
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TARGETFINDERSTATE_T3286805956_H
#ifndef VUFORIAMACROS_T2044285728_H
#define VUFORIAMACROS_T2044285728_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaMacros
struct  VuforiaMacros_t2044285728 
{
public:
	union
	{
		struct
		{
		};
		uint8_t VuforiaMacros_t2044285728__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VUFORIAMACROS_T2044285728_H
#ifndef AUTOROTATIONSTATE_T2150317116_H
#define AUTOROTATIONSTATE_T2150317116_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaManager/AutoRotationState
struct  AutoRotationState_t2150317116 
{
public:
	// System.Boolean Vuforia.VuforiaManager/AutoRotationState::setOnPause
	bool ___setOnPause_0;
	// System.Boolean Vuforia.VuforiaManager/AutoRotationState::autorotateToPortrait
	bool ___autorotateToPortrait_1;
	// System.Boolean Vuforia.VuforiaManager/AutoRotationState::autorotateToPortraitUpsideDown
	bool ___autorotateToPortraitUpsideDown_2;
	// System.Boolean Vuforia.VuforiaManager/AutoRotationState::autorotateToLandscapeLeft
	bool ___autorotateToLandscapeLeft_3;
	// System.Boolean Vuforia.VuforiaManager/AutoRotationState::autorotateToLandscapeRight
	bool ___autorotateToLandscapeRight_4;

public:
	inline static int32_t get_offset_of_setOnPause_0() { return static_cast<int32_t>(offsetof(AutoRotationState_t2150317116, ___setOnPause_0)); }
	inline bool get_setOnPause_0() const { return ___setOnPause_0; }
	inline bool* get_address_of_setOnPause_0() { return &___setOnPause_0; }
	inline void set_setOnPause_0(bool value)
	{
		___setOnPause_0 = value;
	}

	inline static int32_t get_offset_of_autorotateToPortrait_1() { return static_cast<int32_t>(offsetof(AutoRotationState_t2150317116, ___autorotateToPortrait_1)); }
	inline bool get_autorotateToPortrait_1() const { return ___autorotateToPortrait_1; }
	inline bool* get_address_of_autorotateToPortrait_1() { return &___autorotateToPortrait_1; }
	inline void set_autorotateToPortrait_1(bool value)
	{
		___autorotateToPortrait_1 = value;
	}

	inline static int32_t get_offset_of_autorotateToPortraitUpsideDown_2() { return static_cast<int32_t>(offsetof(AutoRotationState_t2150317116, ___autorotateToPortraitUpsideDown_2)); }
	inline bool get_autorotateToPortraitUpsideDown_2() const { return ___autorotateToPortraitUpsideDown_2; }
	inline bool* get_address_of_autorotateToPortraitUpsideDown_2() { return &___autorotateToPortraitUpsideDown_2; }
	inline void set_autorotateToPortraitUpsideDown_2(bool value)
	{
		___autorotateToPortraitUpsideDown_2 = value;
	}

	inline static int32_t get_offset_of_autorotateToLandscapeLeft_3() { return static_cast<int32_t>(offsetof(AutoRotationState_t2150317116, ___autorotateToLandscapeLeft_3)); }
	inline bool get_autorotateToLandscapeLeft_3() const { return ___autorotateToLandscapeLeft_3; }
	inline bool* get_address_of_autorotateToLandscapeLeft_3() { return &___autorotateToLandscapeLeft_3; }
	inline void set_autorotateToLandscapeLeft_3(bool value)
	{
		___autorotateToLandscapeLeft_3 = value;
	}

	inline static int32_t get_offset_of_autorotateToLandscapeRight_4() { return static_cast<int32_t>(offsetof(AutoRotationState_t2150317116, ___autorotateToLandscapeRight_4)); }
	inline bool get_autorotateToLandscapeRight_4() const { return ___autorotateToLandscapeRight_4; }
	inline bool* get_address_of_autorotateToLandscapeRight_4() { return &___autorotateToLandscapeRight_4; }
	inline void set_autorotateToLandscapeRight_4(bool value)
	{
		___autorotateToLandscapeRight_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Vuforia.VuforiaManager/AutoRotationState
struct AutoRotationState_t2150317116_marshaled_pinvoke
{
	int32_t ___setOnPause_0;
	int32_t ___autorotateToPortrait_1;
	int32_t ___autorotateToPortraitUpsideDown_2;
	int32_t ___autorotateToLandscapeLeft_3;
	int32_t ___autorotateToLandscapeRight_4;
};
// Native definition for COM marshalling of Vuforia.VuforiaManager/AutoRotationState
struct AutoRotationState_t2150317116_marshaled_com
{
	int32_t ___setOnPause_0;
	int32_t ___autorotateToPortrait_1;
	int32_t ___autorotateToPortraitUpsideDown_2;
	int32_t ___autorotateToLandscapeLeft_3;
	int32_t ___autorotateToLandscapeRight_4;
};
#endif // AUTOROTATIONSTATE_T2150317116_H
#ifndef TRACKABLEIDPAIR_T4227350457_H
#define TRACKABLEIDPAIR_T4227350457_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaManager/TrackableIdPair
struct  TrackableIdPair_t4227350457 
{
public:
	// System.Int32 Vuforia.VuforiaManager/TrackableIdPair::TrackableId
	int32_t ___TrackableId_0;
	// System.Int32 Vuforia.VuforiaManager/TrackableIdPair::ResultId
	int32_t ___ResultId_1;

public:
	inline static int32_t get_offset_of_TrackableId_0() { return static_cast<int32_t>(offsetof(TrackableIdPair_t4227350457, ___TrackableId_0)); }
	inline int32_t get_TrackableId_0() const { return ___TrackableId_0; }
	inline int32_t* get_address_of_TrackableId_0() { return &___TrackableId_0; }
	inline void set_TrackableId_0(int32_t value)
	{
		___TrackableId_0 = value;
	}

	inline static int32_t get_offset_of_ResultId_1() { return static_cast<int32_t>(offsetof(TrackableIdPair_t4227350457, ___ResultId_1)); }
	inline int32_t get_ResultId_1() const { return ___ResultId_1; }
	inline int32_t* get_address_of_ResultId_1() { return &___ResultId_1; }
	inline void set_ResultId_1(int32_t value)
	{
		___ResultId_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKABLEIDPAIR_T4227350457_H
#ifndef VEC2I_T3527036565_H
#define VEC2I_T3527036565_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaRenderer/Vec2I
#pragma pack(push, tp, 1)
struct  Vec2I_t3527036565 
{
public:
	// System.Int32 Vuforia.VuforiaRenderer/Vec2I::x
	int32_t ___x_0;
	// System.Int32 Vuforia.VuforiaRenderer/Vec2I::y
	int32_t ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vec2I_t3527036565, ___x_0)); }
	inline int32_t get_x_0() const { return ___x_0; }
	inline int32_t* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(int32_t value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vec2I_t3527036565, ___y_1)); }
	inline int32_t get_y_1() const { return ___y_1; }
	inline int32_t* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(int32_t value)
	{
		___y_1 = value;
	}
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VEC2I_T3527036565_H
#ifndef WEBCAMARCONTROLLER_T3718642882_H
#define WEBCAMARCONTROLLER_T3718642882_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.WebCamARController
struct  WebCamARController_t3718642882  : public ARController_t116632334
{
public:
	// System.Int32 Vuforia.WebCamARController::RenderTextureLayer
	int32_t ___RenderTextureLayer_1;
	// System.String Vuforia.WebCamARController::mDeviceNameSetInEditor
	String_t* ___mDeviceNameSetInEditor_2;
	// System.Boolean Vuforia.WebCamARController::mFlipHorizontally
	bool ___mFlipHorizontally_3;
	// Vuforia.WebCam Vuforia.WebCamARController::mWebCamImpl
	WebCam_t2427002488 * ___mWebCamImpl_4;
	// System.Func`3<System.String,Vuforia.WebCamProfile/ProfileData,Vuforia.IWebCamTexAdaptor> Vuforia.WebCamARController::mWebCamTexAdaptorProvider
	Func_3_t3440825513 * ___mWebCamTexAdaptorProvider_5;

public:
	inline static int32_t get_offset_of_RenderTextureLayer_1() { return static_cast<int32_t>(offsetof(WebCamARController_t3718642882, ___RenderTextureLayer_1)); }
	inline int32_t get_RenderTextureLayer_1() const { return ___RenderTextureLayer_1; }
	inline int32_t* get_address_of_RenderTextureLayer_1() { return &___RenderTextureLayer_1; }
	inline void set_RenderTextureLayer_1(int32_t value)
	{
		___RenderTextureLayer_1 = value;
	}

	inline static int32_t get_offset_of_mDeviceNameSetInEditor_2() { return static_cast<int32_t>(offsetof(WebCamARController_t3718642882, ___mDeviceNameSetInEditor_2)); }
	inline String_t* get_mDeviceNameSetInEditor_2() const { return ___mDeviceNameSetInEditor_2; }
	inline String_t** get_address_of_mDeviceNameSetInEditor_2() { return &___mDeviceNameSetInEditor_2; }
	inline void set_mDeviceNameSetInEditor_2(String_t* value)
	{
		___mDeviceNameSetInEditor_2 = value;
		Il2CppCodeGenWriteBarrier((&___mDeviceNameSetInEditor_2), value);
	}

	inline static int32_t get_offset_of_mFlipHorizontally_3() { return static_cast<int32_t>(offsetof(WebCamARController_t3718642882, ___mFlipHorizontally_3)); }
	inline bool get_mFlipHorizontally_3() const { return ___mFlipHorizontally_3; }
	inline bool* get_address_of_mFlipHorizontally_3() { return &___mFlipHorizontally_3; }
	inline void set_mFlipHorizontally_3(bool value)
	{
		___mFlipHorizontally_3 = value;
	}

	inline static int32_t get_offset_of_mWebCamImpl_4() { return static_cast<int32_t>(offsetof(WebCamARController_t3718642882, ___mWebCamImpl_4)); }
	inline WebCam_t2427002488 * get_mWebCamImpl_4() const { return ___mWebCamImpl_4; }
	inline WebCam_t2427002488 ** get_address_of_mWebCamImpl_4() { return &___mWebCamImpl_4; }
	inline void set_mWebCamImpl_4(WebCam_t2427002488 * value)
	{
		___mWebCamImpl_4 = value;
		Il2CppCodeGenWriteBarrier((&___mWebCamImpl_4), value);
	}

	inline static int32_t get_offset_of_mWebCamTexAdaptorProvider_5() { return static_cast<int32_t>(offsetof(WebCamARController_t3718642882, ___mWebCamTexAdaptorProvider_5)); }
	inline Func_3_t3440825513 * get_mWebCamTexAdaptorProvider_5() const { return ___mWebCamTexAdaptorProvider_5; }
	inline Func_3_t3440825513 ** get_address_of_mWebCamTexAdaptorProvider_5() { return &___mWebCamTexAdaptorProvider_5; }
	inline void set_mWebCamTexAdaptorProvider_5(Func_3_t3440825513 * value)
	{
		___mWebCamTexAdaptorProvider_5 = value;
		Il2CppCodeGenWriteBarrier((&___mWebCamTexAdaptorProvider_5), value);
	}
};

struct WebCamARController_t3718642882_StaticFields
{
public:
	// Vuforia.WebCamARController Vuforia.WebCamARController::mInstance
	WebCamARController_t3718642882 * ___mInstance_6;
	// System.Object Vuforia.WebCamARController::mPadlock
	RuntimeObject * ___mPadlock_7;

public:
	inline static int32_t get_offset_of_mInstance_6() { return static_cast<int32_t>(offsetof(WebCamARController_t3718642882_StaticFields, ___mInstance_6)); }
	inline WebCamARController_t3718642882 * get_mInstance_6() const { return ___mInstance_6; }
	inline WebCamARController_t3718642882 ** get_address_of_mInstance_6() { return &___mInstance_6; }
	inline void set_mInstance_6(WebCamARController_t3718642882 * value)
	{
		___mInstance_6 = value;
		Il2CppCodeGenWriteBarrier((&___mInstance_6), value);
	}

	inline static int32_t get_offset_of_mPadlock_7() { return static_cast<int32_t>(offsetof(WebCamARController_t3718642882_StaticFields, ___mPadlock_7)); }
	inline RuntimeObject * get_mPadlock_7() const { return ___mPadlock_7; }
	inline RuntimeObject ** get_address_of_mPadlock_7() { return &___mPadlock_7; }
	inline void set_mPadlock_7(RuntimeObject * value)
	{
		___mPadlock_7 = value;
		Il2CppCodeGenWriteBarrier((&___mPadlock_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBCAMARCONTROLLER_T3718642882_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255368_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255368_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t3057255368  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t3057255368_StaticFields
{
public:
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::898C2022A0C02FCE602BF05E1C09BD48301606E5
	__StaticArrayInitTypeSizeU3D24_t3517759979  ___898C2022A0C02FCE602BF05E1C09BD48301606E5_0;

public:
	inline static int32_t get_offset_of_U3898C2022A0C02FCE602BF05E1C09BD48301606E5_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255368_StaticFields, ___898C2022A0C02FCE602BF05E1C09BD48301606E5_0)); }
	inline __StaticArrayInitTypeSizeU3D24_t3517759979  get_U3898C2022A0C02FCE602BF05E1C09BD48301606E5_0() const { return ___898C2022A0C02FCE602BF05E1C09BD48301606E5_0; }
	inline __StaticArrayInitTypeSizeU3D24_t3517759979 * get_address_of_U3898C2022A0C02FCE602BF05E1C09BD48301606E5_0() { return &___898C2022A0C02FCE602BF05E1C09BD48301606E5_0; }
	inline void set_U3898C2022A0C02FCE602BF05E1C09BD48301606E5_0(__StaticArrayInitTypeSizeU3D24_t3517759979  value)
	{
		___898C2022A0C02FCE602BF05E1C09BD48301606E5_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255368_H
#ifndef PIXEL_T2713956838_H
#define PIXEL_T2713956838_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pixel
struct  Pixel_t2713956838  : public RuntimeObject
{
public:
	// System.Int32 Pixel::i
	int32_t ___i_0;
	// System.Int32 Pixel::j
	int32_t ___j_1;
	// UnityEngine.Color Pixel::couleur
	Color_t2555686324  ___couleur_2;

public:
	inline static int32_t get_offset_of_i_0() { return static_cast<int32_t>(offsetof(Pixel_t2713956838, ___i_0)); }
	inline int32_t get_i_0() const { return ___i_0; }
	inline int32_t* get_address_of_i_0() { return &___i_0; }
	inline void set_i_0(int32_t value)
	{
		___i_0 = value;
	}

	inline static int32_t get_offset_of_j_1() { return static_cast<int32_t>(offsetof(Pixel_t2713956838, ___j_1)); }
	inline int32_t get_j_1() const { return ___j_1; }
	inline int32_t* get_address_of_j_1() { return &___j_1; }
	inline void set_j_1(int32_t value)
	{
		___j_1 = value;
	}

	inline static int32_t get_offset_of_couleur_2() { return static_cast<int32_t>(offsetof(Pixel_t2713956838, ___couleur_2)); }
	inline Color_t2555686324  get_couleur_2() const { return ___couleur_2; }
	inline Color_t2555686324 * get_address_of_couleur_2() { return &___couleur_2; }
	inline void set_couleur_2(Color_t2555686324  value)
	{
		___couleur_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PIXEL_T2713956838_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef PLANE_T1000493321_H
#define PLANE_T1000493321_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Plane
struct  Plane_t1000493321 
{
public:
	// UnityEngine.Vector3 UnityEngine.Plane::m_Normal
	Vector3_t3722313464  ___m_Normal_0;
	// System.Single UnityEngine.Plane::m_Distance
	float ___m_Distance_1;

public:
	inline static int32_t get_offset_of_m_Normal_0() { return static_cast<int32_t>(offsetof(Plane_t1000493321, ___m_Normal_0)); }
	inline Vector3_t3722313464  get_m_Normal_0() const { return ___m_Normal_0; }
	inline Vector3_t3722313464 * get_address_of_m_Normal_0() { return &___m_Normal_0; }
	inline void set_m_Normal_0(Vector3_t3722313464  value)
	{
		___m_Normal_0 = value;
	}

	inline static int32_t get_offset_of_m_Distance_1() { return static_cast<int32_t>(offsetof(Plane_t1000493321, ___m_Distance_1)); }
	inline float get_m_Distance_1() const { return ___m_Distance_1; }
	inline float* get_address_of_m_Distance_1() { return &___m_Distance_1; }
	inline void set_m_Distance_1(float value)
	{
		___m_Distance_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLANE_T1000493321_H
#ifndef RAY_T3785851493_H
#define RAY_T3785851493_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Ray
struct  Ray_t3785851493 
{
public:
	// UnityEngine.Vector3 UnityEngine.Ray::m_Origin
	Vector3_t3722313464  ___m_Origin_0;
	// UnityEngine.Vector3 UnityEngine.Ray::m_Direction
	Vector3_t3722313464  ___m_Direction_1;

public:
	inline static int32_t get_offset_of_m_Origin_0() { return static_cast<int32_t>(offsetof(Ray_t3785851493, ___m_Origin_0)); }
	inline Vector3_t3722313464  get_m_Origin_0() const { return ___m_Origin_0; }
	inline Vector3_t3722313464 * get_address_of_m_Origin_0() { return &___m_Origin_0; }
	inline void set_m_Origin_0(Vector3_t3722313464  value)
	{
		___m_Origin_0 = value;
	}

	inline static int32_t get_offset_of_m_Direction_1() { return static_cast<int32_t>(offsetof(Ray_t3785851493, ___m_Direction_1)); }
	inline Vector3_t3722313464  get_m_Direction_1() const { return ___m_Direction_1; }
	inline Vector3_t3722313464 * get_address_of_m_Direction_1() { return &___m_Direction_1; }
	inline void set_m_Direction_1(Vector3_t3722313464  value)
	{
		___m_Direction_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAY_T3785851493_H
#ifndef RAYCASTHIT_T1056001966_H
#define RAYCASTHIT_T1056001966_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RaycastHit
struct  RaycastHit_t1056001966 
{
public:
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Point
	Vector3_t3722313464  ___m_Point_0;
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Normal
	Vector3_t3722313464  ___m_Normal_1;
	// System.UInt32 UnityEngine.RaycastHit::m_FaceID
	uint32_t ___m_FaceID_2;
	// System.Single UnityEngine.RaycastHit::m_Distance
	float ___m_Distance_3;
	// UnityEngine.Vector2 UnityEngine.RaycastHit::m_UV
	Vector2_t2156229523  ___m_UV_4;
	// System.Int32 UnityEngine.RaycastHit::m_Collider
	int32_t ___m_Collider_5;

public:
	inline static int32_t get_offset_of_m_Point_0() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_Point_0)); }
	inline Vector3_t3722313464  get_m_Point_0() const { return ___m_Point_0; }
	inline Vector3_t3722313464 * get_address_of_m_Point_0() { return &___m_Point_0; }
	inline void set_m_Point_0(Vector3_t3722313464  value)
	{
		___m_Point_0 = value;
	}

	inline static int32_t get_offset_of_m_Normal_1() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_Normal_1)); }
	inline Vector3_t3722313464  get_m_Normal_1() const { return ___m_Normal_1; }
	inline Vector3_t3722313464 * get_address_of_m_Normal_1() { return &___m_Normal_1; }
	inline void set_m_Normal_1(Vector3_t3722313464  value)
	{
		___m_Normal_1 = value;
	}

	inline static int32_t get_offset_of_m_FaceID_2() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_FaceID_2)); }
	inline uint32_t get_m_FaceID_2() const { return ___m_FaceID_2; }
	inline uint32_t* get_address_of_m_FaceID_2() { return &___m_FaceID_2; }
	inline void set_m_FaceID_2(uint32_t value)
	{
		___m_FaceID_2 = value;
	}

	inline static int32_t get_offset_of_m_Distance_3() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_Distance_3)); }
	inline float get_m_Distance_3() const { return ___m_Distance_3; }
	inline float* get_address_of_m_Distance_3() { return &___m_Distance_3; }
	inline void set_m_Distance_3(float value)
	{
		___m_Distance_3 = value;
	}

	inline static int32_t get_offset_of_m_UV_4() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_UV_4)); }
	inline Vector2_t2156229523  get_m_UV_4() const { return ___m_UV_4; }
	inline Vector2_t2156229523 * get_address_of_m_UV_4() { return &___m_UV_4; }
	inline void set_m_UV_4(Vector2_t2156229523  value)
	{
		___m_UV_4 = value;
	}

	inline static int32_t get_offset_of_m_Collider_5() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_Collider_5)); }
	inline int32_t get_m_Collider_5() const { return ___m_Collider_5; }
	inline int32_t* get_address_of_m_Collider_5() { return &___m_Collider_5; }
	inline void set_m_Collider_5(int32_t value)
	{
		___m_Collider_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAYCASTHIT_T1056001966_H
#ifndef SCREENORIENTATION_T1705519499_H
#define SCREENORIENTATION_T1705519499_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScreenOrientation
struct  ScreenOrientation_t1705519499 
{
public:
	// System.Int32 UnityEngine.ScreenOrientation::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ScreenOrientation_t1705519499, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCREENORIENTATION_T1705519499_H
#ifndef CAMERADEVICEMODE_T2478715656_H
#define CAMERADEVICEMODE_T2478715656_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.CameraDevice/CameraDeviceMode
struct  CameraDeviceMode_t2478715656 
{
public:
	// System.Int32 Vuforia.CameraDevice/CameraDeviceMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CameraDeviceMode_t2478715656, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERADEVICEMODE_T2478715656_H
#ifndef CAMERADIRECTION_T637748435_H
#define CAMERADIRECTION_T637748435_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.CameraDevice/CameraDirection
struct  CameraDirection_t637748435 
{
public:
	// System.Int32 Vuforia.CameraDevice/CameraDirection::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CameraDirection_t637748435, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERADIRECTION_T637748435_H
#ifndef CLIPPING_MODE_T2655398006_H
#define CLIPPING_MODE_T2655398006_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.HideExcessAreaUtility/CLIPPING_MODE
struct  CLIPPING_MODE_t2655398006 
{
public:
	// System.Int32 Vuforia.HideExcessAreaUtility/CLIPPING_MODE::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CLIPPING_MODE_t2655398006, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLIPPING_MODE_T2655398006_H
#ifndef PIXEL_FORMAT_T3209881435_H
#define PIXEL_FORMAT_T3209881435_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.Image/PIXEL_FORMAT
struct  PIXEL_FORMAT_t3209881435 
{
public:
	// System.Int32 Vuforia.Image/PIXEL_FORMAT::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PIXEL_FORMAT_t3209881435, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PIXEL_FORMAT_T3209881435_H
#ifndef FRAMEQUALITY_T46289180_H
#define FRAMEQUALITY_T46289180_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.ImageTargetBuilder/FrameQuality
struct  FrameQuality_t46289180 
{
public:
	// System.Int32 Vuforia.ImageTargetBuilder/FrameQuality::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FrameQuality_t46289180, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FRAMEQUALITY_T46289180_H
#ifndef IMAGETARGETTYPE_T2834081427_H
#define IMAGETARGETTYPE_T2834081427_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.ImageTargetType
struct  ImageTargetType_t2834081427 
{
public:
	// System.Int32 Vuforia.ImageTargetType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ImageTargetType_t2834081427, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMAGETARGETTYPE_T2834081427_H
#ifndef TARGETFINDER_T2439332195_H
#define TARGETFINDER_T2439332195_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.TargetFinder
struct  TargetFinder_t2439332195  : public RuntimeObject
{
public:
	// System.IntPtr Vuforia.TargetFinder::mTargetFinderStatePtr
	intptr_t ___mTargetFinderStatePtr_0;
	// Vuforia.TargetFinder/TargetFinderState Vuforia.TargetFinder::mTargetFinderState
	TargetFinderState_t3286805956  ___mTargetFinderState_1;
	// System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult> Vuforia.TargetFinder::mNewResults
	List_1_t619090059 * ___mNewResults_2;
	// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.ImageTarget> Vuforia.TargetFinder::mImageTargets
	Dictionary_2_t2595729825 * ___mImageTargets_3;

public:
	inline static int32_t get_offset_of_mTargetFinderStatePtr_0() { return static_cast<int32_t>(offsetof(TargetFinder_t2439332195, ___mTargetFinderStatePtr_0)); }
	inline intptr_t get_mTargetFinderStatePtr_0() const { return ___mTargetFinderStatePtr_0; }
	inline intptr_t* get_address_of_mTargetFinderStatePtr_0() { return &___mTargetFinderStatePtr_0; }
	inline void set_mTargetFinderStatePtr_0(intptr_t value)
	{
		___mTargetFinderStatePtr_0 = value;
	}

	inline static int32_t get_offset_of_mTargetFinderState_1() { return static_cast<int32_t>(offsetof(TargetFinder_t2439332195, ___mTargetFinderState_1)); }
	inline TargetFinderState_t3286805956  get_mTargetFinderState_1() const { return ___mTargetFinderState_1; }
	inline TargetFinderState_t3286805956 * get_address_of_mTargetFinderState_1() { return &___mTargetFinderState_1; }
	inline void set_mTargetFinderState_1(TargetFinderState_t3286805956  value)
	{
		___mTargetFinderState_1 = value;
	}

	inline static int32_t get_offset_of_mNewResults_2() { return static_cast<int32_t>(offsetof(TargetFinder_t2439332195, ___mNewResults_2)); }
	inline List_1_t619090059 * get_mNewResults_2() const { return ___mNewResults_2; }
	inline List_1_t619090059 ** get_address_of_mNewResults_2() { return &___mNewResults_2; }
	inline void set_mNewResults_2(List_1_t619090059 * value)
	{
		___mNewResults_2 = value;
		Il2CppCodeGenWriteBarrier((&___mNewResults_2), value);
	}

	inline static int32_t get_offset_of_mImageTargets_3() { return static_cast<int32_t>(offsetof(TargetFinder_t2439332195, ___mImageTargets_3)); }
	inline Dictionary_2_t2595729825 * get_mImageTargets_3() const { return ___mImageTargets_3; }
	inline Dictionary_2_t2595729825 ** get_address_of_mImageTargets_3() { return &___mImageTargets_3; }
	inline void set_mImageTargets_3(Dictionary_2_t2595729825 * value)
	{
		___mImageTargets_3 = value;
		Il2CppCodeGenWriteBarrier((&___mImageTargets_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TARGETFINDER_T2439332195_H
#ifndef FILTERMODE_T1400485161_H
#define FILTERMODE_T1400485161_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.TargetFinder/FilterMode
struct  FilterMode_t1400485161 
{
public:
	// System.Int32 Vuforia.TargetFinder/FilterMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FilterMode_t1400485161, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILTERMODE_T1400485161_H
#ifndef INITSTATE_T538152685_H
#define INITSTATE_T538152685_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.TargetFinder/InitState
struct  InitState_t538152685 
{
public:
	// System.Int32 Vuforia.TargetFinder/InitState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(InitState_t538152685, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INITSTATE_T538152685_H
#ifndef INTERNALTARGETSEARCHRESULT_T3697474723_H
#define INTERNALTARGETSEARCHRESULT_T3697474723_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.TargetFinder/InternalTargetSearchResult
#pragma pack(push, tp, 1)
struct  InternalTargetSearchResult_t3697474723 
{
public:
	// System.IntPtr Vuforia.TargetFinder/InternalTargetSearchResult::TargetNamePtr
	intptr_t ___TargetNamePtr_0;
	// System.IntPtr Vuforia.TargetFinder/InternalTargetSearchResult::UniqueTargetIdPtr
	intptr_t ___UniqueTargetIdPtr_1;
	// System.IntPtr Vuforia.TargetFinder/InternalTargetSearchResult::MetaDataPtr
	intptr_t ___MetaDataPtr_2;
	// System.IntPtr Vuforia.TargetFinder/InternalTargetSearchResult::TargetSearchResultPtr
	intptr_t ___TargetSearchResultPtr_3;
	// System.Single Vuforia.TargetFinder/InternalTargetSearchResult::TargetSize
	float ___TargetSize_4;
	// System.Int32 Vuforia.TargetFinder/InternalTargetSearchResult::TrackingRating
	int32_t ___TrackingRating_5;

public:
	inline static int32_t get_offset_of_TargetNamePtr_0() { return static_cast<int32_t>(offsetof(InternalTargetSearchResult_t3697474723, ___TargetNamePtr_0)); }
	inline intptr_t get_TargetNamePtr_0() const { return ___TargetNamePtr_0; }
	inline intptr_t* get_address_of_TargetNamePtr_0() { return &___TargetNamePtr_0; }
	inline void set_TargetNamePtr_0(intptr_t value)
	{
		___TargetNamePtr_0 = value;
	}

	inline static int32_t get_offset_of_UniqueTargetIdPtr_1() { return static_cast<int32_t>(offsetof(InternalTargetSearchResult_t3697474723, ___UniqueTargetIdPtr_1)); }
	inline intptr_t get_UniqueTargetIdPtr_1() const { return ___UniqueTargetIdPtr_1; }
	inline intptr_t* get_address_of_UniqueTargetIdPtr_1() { return &___UniqueTargetIdPtr_1; }
	inline void set_UniqueTargetIdPtr_1(intptr_t value)
	{
		___UniqueTargetIdPtr_1 = value;
	}

	inline static int32_t get_offset_of_MetaDataPtr_2() { return static_cast<int32_t>(offsetof(InternalTargetSearchResult_t3697474723, ___MetaDataPtr_2)); }
	inline intptr_t get_MetaDataPtr_2() const { return ___MetaDataPtr_2; }
	inline intptr_t* get_address_of_MetaDataPtr_2() { return &___MetaDataPtr_2; }
	inline void set_MetaDataPtr_2(intptr_t value)
	{
		___MetaDataPtr_2 = value;
	}

	inline static int32_t get_offset_of_TargetSearchResultPtr_3() { return static_cast<int32_t>(offsetof(InternalTargetSearchResult_t3697474723, ___TargetSearchResultPtr_3)); }
	inline intptr_t get_TargetSearchResultPtr_3() const { return ___TargetSearchResultPtr_3; }
	inline intptr_t* get_address_of_TargetSearchResultPtr_3() { return &___TargetSearchResultPtr_3; }
	inline void set_TargetSearchResultPtr_3(intptr_t value)
	{
		___TargetSearchResultPtr_3 = value;
	}

	inline static int32_t get_offset_of_TargetSize_4() { return static_cast<int32_t>(offsetof(InternalTargetSearchResult_t3697474723, ___TargetSize_4)); }
	inline float get_TargetSize_4() const { return ___TargetSize_4; }
	inline float* get_address_of_TargetSize_4() { return &___TargetSize_4; }
	inline void set_TargetSize_4(float value)
	{
		___TargetSize_4 = value;
	}

	inline static int32_t get_offset_of_TrackingRating_5() { return static_cast<int32_t>(offsetof(InternalTargetSearchResult_t3697474723, ___TrackingRating_5)); }
	inline int32_t get_TrackingRating_5() const { return ___TrackingRating_5; }
	inline int32_t* get_address_of_TrackingRating_5() { return &___TrackingRating_5; }
	inline void set_TrackingRating_5(int32_t value)
	{
		___TrackingRating_5 = value;
	}
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERNALTARGETSEARCHRESULT_T3697474723_H
#ifndef TARGETSEARCHRESULT_T3441982613_H
#define TARGETSEARCHRESULT_T3441982613_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.TargetFinder/TargetSearchResult
struct  TargetSearchResult_t3441982613 
{
public:
	// System.String Vuforia.TargetFinder/TargetSearchResult::TargetName
	String_t* ___TargetName_0;
	// System.String Vuforia.TargetFinder/TargetSearchResult::UniqueTargetId
	String_t* ___UniqueTargetId_1;
	// System.Single Vuforia.TargetFinder/TargetSearchResult::TargetSize
	float ___TargetSize_2;
	// System.String Vuforia.TargetFinder/TargetSearchResult::MetaData
	String_t* ___MetaData_3;
	// System.Byte Vuforia.TargetFinder/TargetSearchResult::TrackingRating
	uint8_t ___TrackingRating_4;
	// System.IntPtr Vuforia.TargetFinder/TargetSearchResult::TargetSearchResultPtr
	intptr_t ___TargetSearchResultPtr_5;

public:
	inline static int32_t get_offset_of_TargetName_0() { return static_cast<int32_t>(offsetof(TargetSearchResult_t3441982613, ___TargetName_0)); }
	inline String_t* get_TargetName_0() const { return ___TargetName_0; }
	inline String_t** get_address_of_TargetName_0() { return &___TargetName_0; }
	inline void set_TargetName_0(String_t* value)
	{
		___TargetName_0 = value;
		Il2CppCodeGenWriteBarrier((&___TargetName_0), value);
	}

	inline static int32_t get_offset_of_UniqueTargetId_1() { return static_cast<int32_t>(offsetof(TargetSearchResult_t3441982613, ___UniqueTargetId_1)); }
	inline String_t* get_UniqueTargetId_1() const { return ___UniqueTargetId_1; }
	inline String_t** get_address_of_UniqueTargetId_1() { return &___UniqueTargetId_1; }
	inline void set_UniqueTargetId_1(String_t* value)
	{
		___UniqueTargetId_1 = value;
		Il2CppCodeGenWriteBarrier((&___UniqueTargetId_1), value);
	}

	inline static int32_t get_offset_of_TargetSize_2() { return static_cast<int32_t>(offsetof(TargetSearchResult_t3441982613, ___TargetSize_2)); }
	inline float get_TargetSize_2() const { return ___TargetSize_2; }
	inline float* get_address_of_TargetSize_2() { return &___TargetSize_2; }
	inline void set_TargetSize_2(float value)
	{
		___TargetSize_2 = value;
	}

	inline static int32_t get_offset_of_MetaData_3() { return static_cast<int32_t>(offsetof(TargetSearchResult_t3441982613, ___MetaData_3)); }
	inline String_t* get_MetaData_3() const { return ___MetaData_3; }
	inline String_t** get_address_of_MetaData_3() { return &___MetaData_3; }
	inline void set_MetaData_3(String_t* value)
	{
		___MetaData_3 = value;
		Il2CppCodeGenWriteBarrier((&___MetaData_3), value);
	}

	inline static int32_t get_offset_of_TrackingRating_4() { return static_cast<int32_t>(offsetof(TargetSearchResult_t3441982613, ___TrackingRating_4)); }
	inline uint8_t get_TrackingRating_4() const { return ___TrackingRating_4; }
	inline uint8_t* get_address_of_TrackingRating_4() { return &___TrackingRating_4; }
	inline void set_TrackingRating_4(uint8_t value)
	{
		___TrackingRating_4 = value;
	}

	inline static int32_t get_offset_of_TargetSearchResultPtr_5() { return static_cast<int32_t>(offsetof(TargetSearchResult_t3441982613, ___TargetSearchResultPtr_5)); }
	inline intptr_t get_TargetSearchResultPtr_5() const { return ___TargetSearchResultPtr_5; }
	inline intptr_t* get_address_of_TargetSearchResultPtr_5() { return &___TargetSearchResultPtr_5; }
	inline void set_TargetSearchResultPtr_5(intptr_t value)
	{
		___TargetSearchResultPtr_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Vuforia.TargetFinder/TargetSearchResult
struct TargetSearchResult_t3441982613_marshaled_pinvoke
{
	char* ___TargetName_0;
	char* ___UniqueTargetId_1;
	float ___TargetSize_2;
	char* ___MetaData_3;
	uint8_t ___TrackingRating_4;
	intptr_t ___TargetSearchResultPtr_5;
};
// Native definition for COM marshalling of Vuforia.TargetFinder/TargetSearchResult
struct TargetSearchResult_t3441982613_marshaled_com
{
	Il2CppChar* ___TargetName_0;
	Il2CppChar* ___UniqueTargetId_1;
	float ___TargetSize_2;
	Il2CppChar* ___MetaData_3;
	uint8_t ___TrackingRating_4;
	intptr_t ___TargetSearchResultPtr_5;
};
#endif // TARGETSEARCHRESULT_T3441982613_H
#ifndef UPDATESTATE_T1279515537_H
#define UPDATESTATE_T1279515537_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.TargetFinder/UpdateState
struct  UpdateState_t1279515537 
{
public:
	// System.Int32 Vuforia.TargetFinder/UpdateState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(UpdateState_t1279515537, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPDATESTATE_T1279515537_H
#ifndef COORDINATESYSTEM_T4035406609_H
#define COORDINATESYSTEM_T4035406609_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.TrackableBehaviour/CoordinateSystem
struct  CoordinateSystem_t4035406609 
{
public:
	// System.Int32 Vuforia.TrackableBehaviour/CoordinateSystem::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CoordinateSystem_t4035406609, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COORDINATESYSTEM_T4035406609_H
#ifndef STATUS_T1100905814_H
#define STATUS_T1100905814_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.TrackableBehaviour/Status
struct  Status_t1100905814 
{
public:
	// System.Int32 Vuforia.TrackableBehaviour/Status::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Status_t1100905814, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATUS_T1100905814_H
#ifndef STATUSINFO_T1633251416_H
#define STATUSINFO_T1633251416_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.TrackableBehaviour/StatusInfo
struct  StatusInfo_t1633251416 
{
public:
	// System.Int32 Vuforia.TrackableBehaviour/StatusInfo::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(StatusInfo_t1633251416, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATUSINFO_T1633251416_H
#ifndef TRACKABLESOURCE_T2567074243_H
#define TRACKABLESOURCE_T2567074243_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.TrackableSource
struct  TrackableSource_t2567074243  : public RuntimeObject
{
public:
	// System.IntPtr Vuforia.TrackableSource::<TrackableSourcePtr>k__BackingField
	intptr_t ___U3CTrackableSourcePtrU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CTrackableSourcePtrU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(TrackableSource_t2567074243, ___U3CTrackableSourcePtrU3Ek__BackingField_0)); }
	inline intptr_t get_U3CTrackableSourcePtrU3Ek__BackingField_0() const { return ___U3CTrackableSourcePtrU3Ek__BackingField_0; }
	inline intptr_t* get_address_of_U3CTrackableSourcePtrU3Ek__BackingField_0() { return &___U3CTrackableSourcePtrU3Ek__BackingField_0; }
	inline void set_U3CTrackableSourcePtrU3Ek__BackingField_0(intptr_t value)
	{
		___U3CTrackableSourcePtrU3Ek__BackingField_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKABLESOURCE_T2567074243_H
#ifndef FRAMESTATE_T2717258284_H
#define FRAMESTATE_T2717258284_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.TrackerData/FrameState
#pragma pack(push, tp, 1)
struct  FrameState_t2717258284 
{
public:
	// System.IntPtr Vuforia.TrackerData/FrameState::trackableDataArray
	intptr_t ___trackableDataArray_0;
	// System.IntPtr Vuforia.TrackerData/FrameState::vbDataArray
	intptr_t ___vbDataArray_1;
	// System.IntPtr Vuforia.TrackerData/FrameState::vuMarkResultArray
	intptr_t ___vuMarkResultArray_2;
	// System.IntPtr Vuforia.TrackerData/FrameState::newVuMarkDataArray
	intptr_t ___newVuMarkDataArray_3;
	// System.IntPtr Vuforia.TrackerData/FrameState::illuminationData
	intptr_t ___illuminationData_4;
	// System.Int32 Vuforia.TrackerData/FrameState::numTrackableResults
	int32_t ___numTrackableResults_5;
	// System.Int32 Vuforia.TrackerData/FrameState::numVirtualButtonResults
	int32_t ___numVirtualButtonResults_6;
	// System.Int32 Vuforia.TrackerData/FrameState::frameIndex
	int32_t ___frameIndex_7;
	// System.Int32 Vuforia.TrackerData/FrameState::numVuMarkResults
	int32_t ___numVuMarkResults_8;
	// System.Int32 Vuforia.TrackerData/FrameState::numNewVuMarks
	int32_t ___numNewVuMarks_9;
	// System.Int32 Vuforia.TrackerData/FrameState::deviceTrackableId
	int32_t ___deviceTrackableId_10;
	// System.Int32 Vuforia.TrackerData/FrameState::deviceTrackableStatusInfo
	int32_t ___deviceTrackableStatusInfo_11;
	// UnityEngine.Vector4 Vuforia.TrackerData/FrameState::minCameraCalibration
	Vector4_t3319028937  ___minCameraCalibration_12;

public:
	inline static int32_t get_offset_of_trackableDataArray_0() { return static_cast<int32_t>(offsetof(FrameState_t2717258284, ___trackableDataArray_0)); }
	inline intptr_t get_trackableDataArray_0() const { return ___trackableDataArray_0; }
	inline intptr_t* get_address_of_trackableDataArray_0() { return &___trackableDataArray_0; }
	inline void set_trackableDataArray_0(intptr_t value)
	{
		___trackableDataArray_0 = value;
	}

	inline static int32_t get_offset_of_vbDataArray_1() { return static_cast<int32_t>(offsetof(FrameState_t2717258284, ___vbDataArray_1)); }
	inline intptr_t get_vbDataArray_1() const { return ___vbDataArray_1; }
	inline intptr_t* get_address_of_vbDataArray_1() { return &___vbDataArray_1; }
	inline void set_vbDataArray_1(intptr_t value)
	{
		___vbDataArray_1 = value;
	}

	inline static int32_t get_offset_of_vuMarkResultArray_2() { return static_cast<int32_t>(offsetof(FrameState_t2717258284, ___vuMarkResultArray_2)); }
	inline intptr_t get_vuMarkResultArray_2() const { return ___vuMarkResultArray_2; }
	inline intptr_t* get_address_of_vuMarkResultArray_2() { return &___vuMarkResultArray_2; }
	inline void set_vuMarkResultArray_2(intptr_t value)
	{
		___vuMarkResultArray_2 = value;
	}

	inline static int32_t get_offset_of_newVuMarkDataArray_3() { return static_cast<int32_t>(offsetof(FrameState_t2717258284, ___newVuMarkDataArray_3)); }
	inline intptr_t get_newVuMarkDataArray_3() const { return ___newVuMarkDataArray_3; }
	inline intptr_t* get_address_of_newVuMarkDataArray_3() { return &___newVuMarkDataArray_3; }
	inline void set_newVuMarkDataArray_3(intptr_t value)
	{
		___newVuMarkDataArray_3 = value;
	}

	inline static int32_t get_offset_of_illuminationData_4() { return static_cast<int32_t>(offsetof(FrameState_t2717258284, ___illuminationData_4)); }
	inline intptr_t get_illuminationData_4() const { return ___illuminationData_4; }
	inline intptr_t* get_address_of_illuminationData_4() { return &___illuminationData_4; }
	inline void set_illuminationData_4(intptr_t value)
	{
		___illuminationData_4 = value;
	}

	inline static int32_t get_offset_of_numTrackableResults_5() { return static_cast<int32_t>(offsetof(FrameState_t2717258284, ___numTrackableResults_5)); }
	inline int32_t get_numTrackableResults_5() const { return ___numTrackableResults_5; }
	inline int32_t* get_address_of_numTrackableResults_5() { return &___numTrackableResults_5; }
	inline void set_numTrackableResults_5(int32_t value)
	{
		___numTrackableResults_5 = value;
	}

	inline static int32_t get_offset_of_numVirtualButtonResults_6() { return static_cast<int32_t>(offsetof(FrameState_t2717258284, ___numVirtualButtonResults_6)); }
	inline int32_t get_numVirtualButtonResults_6() const { return ___numVirtualButtonResults_6; }
	inline int32_t* get_address_of_numVirtualButtonResults_6() { return &___numVirtualButtonResults_6; }
	inline void set_numVirtualButtonResults_6(int32_t value)
	{
		___numVirtualButtonResults_6 = value;
	}

	inline static int32_t get_offset_of_frameIndex_7() { return static_cast<int32_t>(offsetof(FrameState_t2717258284, ___frameIndex_7)); }
	inline int32_t get_frameIndex_7() const { return ___frameIndex_7; }
	inline int32_t* get_address_of_frameIndex_7() { return &___frameIndex_7; }
	inline void set_frameIndex_7(int32_t value)
	{
		___frameIndex_7 = value;
	}

	inline static int32_t get_offset_of_numVuMarkResults_8() { return static_cast<int32_t>(offsetof(FrameState_t2717258284, ___numVuMarkResults_8)); }
	inline int32_t get_numVuMarkResults_8() const { return ___numVuMarkResults_8; }
	inline int32_t* get_address_of_numVuMarkResults_8() { return &___numVuMarkResults_8; }
	inline void set_numVuMarkResults_8(int32_t value)
	{
		___numVuMarkResults_8 = value;
	}

	inline static int32_t get_offset_of_numNewVuMarks_9() { return static_cast<int32_t>(offsetof(FrameState_t2717258284, ___numNewVuMarks_9)); }
	inline int32_t get_numNewVuMarks_9() const { return ___numNewVuMarks_9; }
	inline int32_t* get_address_of_numNewVuMarks_9() { return &___numNewVuMarks_9; }
	inline void set_numNewVuMarks_9(int32_t value)
	{
		___numNewVuMarks_9 = value;
	}

	inline static int32_t get_offset_of_deviceTrackableId_10() { return static_cast<int32_t>(offsetof(FrameState_t2717258284, ___deviceTrackableId_10)); }
	inline int32_t get_deviceTrackableId_10() const { return ___deviceTrackableId_10; }
	inline int32_t* get_address_of_deviceTrackableId_10() { return &___deviceTrackableId_10; }
	inline void set_deviceTrackableId_10(int32_t value)
	{
		___deviceTrackableId_10 = value;
	}

	inline static int32_t get_offset_of_deviceTrackableStatusInfo_11() { return static_cast<int32_t>(offsetof(FrameState_t2717258284, ___deviceTrackableStatusInfo_11)); }
	inline int32_t get_deviceTrackableStatusInfo_11() const { return ___deviceTrackableStatusInfo_11; }
	inline int32_t* get_address_of_deviceTrackableStatusInfo_11() { return &___deviceTrackableStatusInfo_11; }
	inline void set_deviceTrackableStatusInfo_11(int32_t value)
	{
		___deviceTrackableStatusInfo_11 = value;
	}

	inline static int32_t get_offset_of_minCameraCalibration_12() { return static_cast<int32_t>(offsetof(FrameState_t2717258284, ___minCameraCalibration_12)); }
	inline Vector4_t3319028937  get_minCameraCalibration_12() const { return ___minCameraCalibration_12; }
	inline Vector4_t3319028937 * get_address_of_minCameraCalibration_12() { return &___minCameraCalibration_12; }
	inline void set_minCameraCalibration_12(Vector4_t3319028937  value)
	{
		___minCameraCalibration_12 = value;
	}
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FRAMESTATE_T2717258284_H
#ifndef SENSITIVITY_T3045829715_H
#define SENSITIVITY_T3045829715_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VirtualButton/Sensitivity
struct  Sensitivity_t3045829715 
{
public:
	// System.Int32 Vuforia.VirtualButton/Sensitivity::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Sensitivity_t3045829715, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SENSITIVITY_T3045829715_H
#ifndef WORLDCENTERMODE_T3672819471_H
#define WORLDCENTERMODE_T3672819471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaARController/WorldCenterMode
struct  WorldCenterMode_t3672819471 
{
public:
	// System.Int32 Vuforia.VuforiaARController/WorldCenterMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(WorldCenterMode_t3672819471, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WORLDCENTERMODE_T3672819471_H
#ifndef U3CU3EC__DISPLAYCLASS73_0_T779439468_H
#define U3CU3EC__DISPLAYCLASS73_0_T779439468_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaManager/<>c__DisplayClass73_0
struct  U3CU3Ec__DisplayClass73_0_t779439468  : public RuntimeObject
{
public:
	// Vuforia.VuforiaManager/TrackableIdPair Vuforia.VuforiaManager/<>c__DisplayClass73_0::id
	TrackableIdPair_t4227350457  ___id_0;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass73_0_t779439468, ___id_0)); }
	inline TrackableIdPair_t4227350457  get_id_0() const { return ___id_0; }
	inline TrackableIdPair_t4227350457 * get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(TrackableIdPair_t4227350457  value)
	{
		___id_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS73_0_T779439468_H
#ifndef FPSHINT_T2906034572_H
#define FPSHINT_T2906034572_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaRenderer/FpsHint
struct  FpsHint_t2906034572 
{
public:
	// System.Int32 Vuforia.VuforiaRenderer/FpsHint::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FpsHint_t2906034572, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FPSHINT_T2906034572_H
#ifndef RENDEREVENT_T1863578599_H
#define RENDEREVENT_T1863578599_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaRenderer/RenderEvent
struct  RenderEvent_t1863578599 
{
public:
	// System.Int32 Vuforia.VuforiaRenderer/RenderEvent::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(RenderEvent_t1863578599, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDEREVENT_T1863578599_H
#ifndef RENDERERAPI_T402009282_H
#define RENDERERAPI_T402009282_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaRenderer/RendererAPI
struct  RendererAPI_t402009282 
{
public:
	// System.Int32 Vuforia.VuforiaRenderer/RendererAPI::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(RendererAPI_t402009282, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERERAPI_T402009282_H
#ifndef VIDEOBGCFGDATA_T994527297_H
#define VIDEOBGCFGDATA_T994527297_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaRenderer/VideoBGCfgData
#pragma pack(push, tp, 1)
struct  VideoBGCfgData_t994527297 
{
public:
	// Vuforia.VuforiaRenderer/Vec2I Vuforia.VuforiaRenderer/VideoBGCfgData::position
	Vec2I_t3527036565  ___position_0;
	// Vuforia.VuforiaRenderer/Vec2I Vuforia.VuforiaRenderer/VideoBGCfgData::size
	Vec2I_t3527036565  ___size_1;
	// System.Int32 Vuforia.VuforiaRenderer/VideoBGCfgData::enabled
	int32_t ___enabled_2;
	// System.Int32 Vuforia.VuforiaRenderer/VideoBGCfgData::reflectionInteger
	int32_t ___reflectionInteger_3;

public:
	inline static int32_t get_offset_of_position_0() { return static_cast<int32_t>(offsetof(VideoBGCfgData_t994527297, ___position_0)); }
	inline Vec2I_t3527036565  get_position_0() const { return ___position_0; }
	inline Vec2I_t3527036565 * get_address_of_position_0() { return &___position_0; }
	inline void set_position_0(Vec2I_t3527036565  value)
	{
		___position_0 = value;
	}

	inline static int32_t get_offset_of_size_1() { return static_cast<int32_t>(offsetof(VideoBGCfgData_t994527297, ___size_1)); }
	inline Vec2I_t3527036565  get_size_1() const { return ___size_1; }
	inline Vec2I_t3527036565 * get_address_of_size_1() { return &___size_1; }
	inline void set_size_1(Vec2I_t3527036565  value)
	{
		___size_1 = value;
	}

	inline static int32_t get_offset_of_enabled_2() { return static_cast<int32_t>(offsetof(VideoBGCfgData_t994527297, ___enabled_2)); }
	inline int32_t get_enabled_2() const { return ___enabled_2; }
	inline int32_t* get_address_of_enabled_2() { return &___enabled_2; }
	inline void set_enabled_2(int32_t value)
	{
		___enabled_2 = value;
	}

	inline static int32_t get_offset_of_reflectionInteger_3() { return static_cast<int32_t>(offsetof(VideoBGCfgData_t994527297, ___reflectionInteger_3)); }
	inline int32_t get_reflectionInteger_3() const { return ___reflectionInteger_3; }
	inline int32_t* get_address_of_reflectionInteger_3() { return &___reflectionInteger_3; }
	inline void set_reflectionInteger_3(int32_t value)
	{
		___reflectionInteger_3 = value;
	}
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIDEOBGCFGDATA_T994527297_H
#ifndef VIDEOBACKGROUNDREFLECTION_T736962841_H
#define VIDEOBACKGROUNDREFLECTION_T736962841_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaRenderer/VideoBackgroundReflection
struct  VideoBackgroundReflection_t736962841 
{
public:
	// System.Int32 Vuforia.VuforiaRenderer/VideoBackgroundReflection::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(VideoBackgroundReflection_t736962841, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIDEOBACKGROUNDREFLECTION_T736962841_H
#ifndef VIDEOTEXTUREINFO_T1805965052_H
#define VIDEOTEXTUREINFO_T1805965052_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaRenderer/VideoTextureInfo
#pragma pack(push, tp, 1)
struct  VideoTextureInfo_t1805965052 
{
public:
	// Vuforia.VuforiaRenderer/Vec2I Vuforia.VuforiaRenderer/VideoTextureInfo::textureSize
	Vec2I_t3527036565  ___textureSize_0;
	// Vuforia.VuforiaRenderer/Vec2I Vuforia.VuforiaRenderer/VideoTextureInfo::imageSize
	Vec2I_t3527036565  ___imageSize_1;

public:
	inline static int32_t get_offset_of_textureSize_0() { return static_cast<int32_t>(offsetof(VideoTextureInfo_t1805965052, ___textureSize_0)); }
	inline Vec2I_t3527036565  get_textureSize_0() const { return ___textureSize_0; }
	inline Vec2I_t3527036565 * get_address_of_textureSize_0() { return &___textureSize_0; }
	inline void set_textureSize_0(Vec2I_t3527036565  value)
	{
		___textureSize_0 = value;
	}

	inline static int32_t get_offset_of_imageSize_1() { return static_cast<int32_t>(offsetof(VideoTextureInfo_t1805965052, ___imageSize_1)); }
	inline Vec2I_t3527036565  get_imageSize_1() const { return ___imageSize_1; }
	inline Vec2I_t3527036565 * get_address_of_imageSize_1() { return &___imageSize_1; }
	inline void set_imageSize_1(Vec2I_t3527036565  value)
	{
		___imageSize_1 = value;
	}
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIDEOTEXTUREINFO_T1805965052_H
#ifndef INITIALIZABLEBOOL_T3274999204_H
#define INITIALIZABLEBOOL_T3274999204_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaRuntimeUtilities/InitializableBool
struct  InitializableBool_t3274999204 
{
public:
	// System.Int32 Vuforia.VuforiaRuntimeUtilities/InitializableBool::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(InitializableBool_t3274999204, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INITIALIZABLEBOOL_T3274999204_H
#ifndef INITERROR_T3420749710_H
#define INITERROR_T3420749710_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaUnity/InitError
struct  InitError_t3420749710 
{
public:
	// System.Int32 Vuforia.VuforiaUnity/InitError::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(InitError_t3420749710, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INITERROR_T3420749710_H
#ifndef STORAGETYPE_T857810839_H
#define STORAGETYPE_T857810839_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaUnity/StorageType
struct  StorageType_t857810839 
{
public:
	// System.Int32 Vuforia.VuforiaUnity/StorageType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(StorageType_t857810839, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STORAGETYPE_T857810839_H
#ifndef VUFORIAHINT_T545805519_H
#define VUFORIAHINT_T545805519_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaUnity/VuforiaHint
struct  VuforiaHint_t545805519 
{
public:
	// System.Int32 Vuforia.VuforiaUnity/VuforiaHint::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(VuforiaHint_t545805519, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VUFORIAHINT_T545805519_H
#ifndef PROFILEDATA_T3519391925_H
#define PROFILEDATA_T3519391925_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.WebCamProfile/ProfileData
struct  ProfileData_t3519391925 
{
public:
	// Vuforia.VuforiaRenderer/Vec2I Vuforia.WebCamProfile/ProfileData::RequestedTextureSize
	Vec2I_t3527036565  ___RequestedTextureSize_0;
	// Vuforia.VuforiaRenderer/Vec2I Vuforia.WebCamProfile/ProfileData::ResampledTextureSize
	Vec2I_t3527036565  ___ResampledTextureSize_1;
	// System.Int32 Vuforia.WebCamProfile/ProfileData::RequestedFPS
	int32_t ___RequestedFPS_2;

public:
	inline static int32_t get_offset_of_RequestedTextureSize_0() { return static_cast<int32_t>(offsetof(ProfileData_t3519391925, ___RequestedTextureSize_0)); }
	inline Vec2I_t3527036565  get_RequestedTextureSize_0() const { return ___RequestedTextureSize_0; }
	inline Vec2I_t3527036565 * get_address_of_RequestedTextureSize_0() { return &___RequestedTextureSize_0; }
	inline void set_RequestedTextureSize_0(Vec2I_t3527036565  value)
	{
		___RequestedTextureSize_0 = value;
	}

	inline static int32_t get_offset_of_ResampledTextureSize_1() { return static_cast<int32_t>(offsetof(ProfileData_t3519391925, ___ResampledTextureSize_1)); }
	inline Vec2I_t3527036565  get_ResampledTextureSize_1() const { return ___ResampledTextureSize_1; }
	inline Vec2I_t3527036565 * get_address_of_ResampledTextureSize_1() { return &___ResampledTextureSize_1; }
	inline void set_ResampledTextureSize_1(Vec2I_t3527036565  value)
	{
		___ResampledTextureSize_1 = value;
	}

	inline static int32_t get_offset_of_RequestedFPS_2() { return static_cast<int32_t>(offsetof(ProfileData_t3519391925, ___RequestedFPS_2)); }
	inline int32_t get_RequestedFPS_2() const { return ___RequestedFPS_2; }
	inline int32_t* get_address_of_RequestedFPS_2() { return &___RequestedFPS_2; }
	inline void set_RequestedFPS_2(int32_t value)
	{
		___RequestedFPS_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROFILEDATA_T3519391925_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef IMAGE_T745056343_H
#define IMAGE_T745056343_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.Image
struct  Image_t745056343  : public RuntimeObject
{
public:
	// System.Int32 Vuforia.Image::mWidth
	int32_t ___mWidth_0;
	// System.Int32 Vuforia.Image::mHeight
	int32_t ___mHeight_1;
	// System.Int32 Vuforia.Image::mStride
	int32_t ___mStride_2;
	// System.Int32 Vuforia.Image::mBufferWidth
	int32_t ___mBufferWidth_3;
	// System.Int32 Vuforia.Image::mBufferHeight
	int32_t ___mBufferHeight_4;
	// Vuforia.Image/PIXEL_FORMAT Vuforia.Image::mPixelFormat
	int32_t ___mPixelFormat_5;
	// System.Byte[] Vuforia.Image::mData
	ByteU5BU5D_t4116647657* ___mData_6;
	// System.IntPtr Vuforia.Image::mUnmanagedData
	intptr_t ___mUnmanagedData_7;
	// System.Boolean Vuforia.Image::mDataSet
	bool ___mDataSet_8;
	// UnityEngine.Color32[] Vuforia.Image::mPixel32
	Color32U5BU5D_t3850468773* ___mPixel32_9;

public:
	inline static int32_t get_offset_of_mWidth_0() { return static_cast<int32_t>(offsetof(Image_t745056343, ___mWidth_0)); }
	inline int32_t get_mWidth_0() const { return ___mWidth_0; }
	inline int32_t* get_address_of_mWidth_0() { return &___mWidth_0; }
	inline void set_mWidth_0(int32_t value)
	{
		___mWidth_0 = value;
	}

	inline static int32_t get_offset_of_mHeight_1() { return static_cast<int32_t>(offsetof(Image_t745056343, ___mHeight_1)); }
	inline int32_t get_mHeight_1() const { return ___mHeight_1; }
	inline int32_t* get_address_of_mHeight_1() { return &___mHeight_1; }
	inline void set_mHeight_1(int32_t value)
	{
		___mHeight_1 = value;
	}

	inline static int32_t get_offset_of_mStride_2() { return static_cast<int32_t>(offsetof(Image_t745056343, ___mStride_2)); }
	inline int32_t get_mStride_2() const { return ___mStride_2; }
	inline int32_t* get_address_of_mStride_2() { return &___mStride_2; }
	inline void set_mStride_2(int32_t value)
	{
		___mStride_2 = value;
	}

	inline static int32_t get_offset_of_mBufferWidth_3() { return static_cast<int32_t>(offsetof(Image_t745056343, ___mBufferWidth_3)); }
	inline int32_t get_mBufferWidth_3() const { return ___mBufferWidth_3; }
	inline int32_t* get_address_of_mBufferWidth_3() { return &___mBufferWidth_3; }
	inline void set_mBufferWidth_3(int32_t value)
	{
		___mBufferWidth_3 = value;
	}

	inline static int32_t get_offset_of_mBufferHeight_4() { return static_cast<int32_t>(offsetof(Image_t745056343, ___mBufferHeight_4)); }
	inline int32_t get_mBufferHeight_4() const { return ___mBufferHeight_4; }
	inline int32_t* get_address_of_mBufferHeight_4() { return &___mBufferHeight_4; }
	inline void set_mBufferHeight_4(int32_t value)
	{
		___mBufferHeight_4 = value;
	}

	inline static int32_t get_offset_of_mPixelFormat_5() { return static_cast<int32_t>(offsetof(Image_t745056343, ___mPixelFormat_5)); }
	inline int32_t get_mPixelFormat_5() const { return ___mPixelFormat_5; }
	inline int32_t* get_address_of_mPixelFormat_5() { return &___mPixelFormat_5; }
	inline void set_mPixelFormat_5(int32_t value)
	{
		___mPixelFormat_5 = value;
	}

	inline static int32_t get_offset_of_mData_6() { return static_cast<int32_t>(offsetof(Image_t745056343, ___mData_6)); }
	inline ByteU5BU5D_t4116647657* get_mData_6() const { return ___mData_6; }
	inline ByteU5BU5D_t4116647657** get_address_of_mData_6() { return &___mData_6; }
	inline void set_mData_6(ByteU5BU5D_t4116647657* value)
	{
		___mData_6 = value;
		Il2CppCodeGenWriteBarrier((&___mData_6), value);
	}

	inline static int32_t get_offset_of_mUnmanagedData_7() { return static_cast<int32_t>(offsetof(Image_t745056343, ___mUnmanagedData_7)); }
	inline intptr_t get_mUnmanagedData_7() const { return ___mUnmanagedData_7; }
	inline intptr_t* get_address_of_mUnmanagedData_7() { return &___mUnmanagedData_7; }
	inline void set_mUnmanagedData_7(intptr_t value)
	{
		___mUnmanagedData_7 = value;
	}

	inline static int32_t get_offset_of_mDataSet_8() { return static_cast<int32_t>(offsetof(Image_t745056343, ___mDataSet_8)); }
	inline bool get_mDataSet_8() const { return ___mDataSet_8; }
	inline bool* get_address_of_mDataSet_8() { return &___mDataSet_8; }
	inline void set_mDataSet_8(bool value)
	{
		___mDataSet_8 = value;
	}

	inline static int32_t get_offset_of_mPixel32_9() { return static_cast<int32_t>(offsetof(Image_t745056343, ___mPixel32_9)); }
	inline Color32U5BU5D_t3850468773* get_mPixel32_9() const { return ___mPixel32_9; }
	inline Color32U5BU5D_t3850468773** get_address_of_mPixel32_9() { return &___mPixel32_9; }
	inline void set_mPixel32_9(Color32U5BU5D_t3850468773* value)
	{
		___mPixel32_9 = value;
		Il2CppCodeGenWriteBarrier((&___mPixel32_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMAGE_T745056343_H
#ifndef SURFACEUTILITIES_T1841955943_H
#define SURFACEUTILITIES_T1841955943_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.SurfaceUtilities
struct  SurfaceUtilities_t1841955943  : public RuntimeObject
{
public:

public:
};

struct SurfaceUtilities_t1841955943_StaticFields
{
public:
	// UnityEngine.ScreenOrientation Vuforia.SurfaceUtilities::mScreenOrientation
	int32_t ___mScreenOrientation_0;

public:
	inline static int32_t get_offset_of_mScreenOrientation_0() { return static_cast<int32_t>(offsetof(SurfaceUtilities_t1841955943_StaticFields, ___mScreenOrientation_0)); }
	inline int32_t get_mScreenOrientation_0() const { return ___mScreenOrientation_0; }
	inline int32_t* get_address_of_mScreenOrientation_0() { return &___mScreenOrientation_0; }
	inline void set_mScreenOrientation_0(int32_t value)
	{
		___mScreenOrientation_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SURFACEUTILITIES_T1841955943_H
#ifndef VIDEOBACKGROUNDMANAGER_T2198727358_H
#define VIDEOBACKGROUNDMANAGER_T2198727358_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VideoBackgroundManager
struct  VideoBackgroundManager_t2198727358  : public ARController_t116632334
{
public:
	// Vuforia.HideExcessAreaUtility/CLIPPING_MODE Vuforia.VideoBackgroundManager::mClippingMode
	int32_t ___mClippingMode_1;
	// UnityEngine.Shader Vuforia.VideoBackgroundManager::mMatteShader
	Shader_t4151988712 * ___mMatteShader_2;
	// System.Boolean Vuforia.VideoBackgroundManager::mVideoBackgroundEnabled
	bool ___mVideoBackgroundEnabled_3;
	// UnityEngine.Texture Vuforia.VideoBackgroundManager::mTexture
	Texture_t3661962703 * ___mTexture_4;
	// System.Boolean Vuforia.VideoBackgroundManager::mVideoBgConfigChanged
	bool ___mVideoBgConfigChanged_5;
	// System.IntPtr Vuforia.VideoBackgroundManager::mNativeTexturePtr
	intptr_t ___mNativeTexturePtr_6;

public:
	inline static int32_t get_offset_of_mClippingMode_1() { return static_cast<int32_t>(offsetof(VideoBackgroundManager_t2198727358, ___mClippingMode_1)); }
	inline int32_t get_mClippingMode_1() const { return ___mClippingMode_1; }
	inline int32_t* get_address_of_mClippingMode_1() { return &___mClippingMode_1; }
	inline void set_mClippingMode_1(int32_t value)
	{
		___mClippingMode_1 = value;
	}

	inline static int32_t get_offset_of_mMatteShader_2() { return static_cast<int32_t>(offsetof(VideoBackgroundManager_t2198727358, ___mMatteShader_2)); }
	inline Shader_t4151988712 * get_mMatteShader_2() const { return ___mMatteShader_2; }
	inline Shader_t4151988712 ** get_address_of_mMatteShader_2() { return &___mMatteShader_2; }
	inline void set_mMatteShader_2(Shader_t4151988712 * value)
	{
		___mMatteShader_2 = value;
		Il2CppCodeGenWriteBarrier((&___mMatteShader_2), value);
	}

	inline static int32_t get_offset_of_mVideoBackgroundEnabled_3() { return static_cast<int32_t>(offsetof(VideoBackgroundManager_t2198727358, ___mVideoBackgroundEnabled_3)); }
	inline bool get_mVideoBackgroundEnabled_3() const { return ___mVideoBackgroundEnabled_3; }
	inline bool* get_address_of_mVideoBackgroundEnabled_3() { return &___mVideoBackgroundEnabled_3; }
	inline void set_mVideoBackgroundEnabled_3(bool value)
	{
		___mVideoBackgroundEnabled_3 = value;
	}

	inline static int32_t get_offset_of_mTexture_4() { return static_cast<int32_t>(offsetof(VideoBackgroundManager_t2198727358, ___mTexture_4)); }
	inline Texture_t3661962703 * get_mTexture_4() const { return ___mTexture_4; }
	inline Texture_t3661962703 ** get_address_of_mTexture_4() { return &___mTexture_4; }
	inline void set_mTexture_4(Texture_t3661962703 * value)
	{
		___mTexture_4 = value;
		Il2CppCodeGenWriteBarrier((&___mTexture_4), value);
	}

	inline static int32_t get_offset_of_mVideoBgConfigChanged_5() { return static_cast<int32_t>(offsetof(VideoBackgroundManager_t2198727358, ___mVideoBgConfigChanged_5)); }
	inline bool get_mVideoBgConfigChanged_5() const { return ___mVideoBgConfigChanged_5; }
	inline bool* get_address_of_mVideoBgConfigChanged_5() { return &___mVideoBgConfigChanged_5; }
	inline void set_mVideoBgConfigChanged_5(bool value)
	{
		___mVideoBgConfigChanged_5 = value;
	}

	inline static int32_t get_offset_of_mNativeTexturePtr_6() { return static_cast<int32_t>(offsetof(VideoBackgroundManager_t2198727358, ___mNativeTexturePtr_6)); }
	inline intptr_t get_mNativeTexturePtr_6() const { return ___mNativeTexturePtr_6; }
	inline intptr_t* get_address_of_mNativeTexturePtr_6() { return &___mNativeTexturePtr_6; }
	inline void set_mNativeTexturePtr_6(intptr_t value)
	{
		___mNativeTexturePtr_6 = value;
	}
};

struct VideoBackgroundManager_t2198727358_StaticFields
{
public:
	// Vuforia.VideoBackgroundManager Vuforia.VideoBackgroundManager::mInstance
	VideoBackgroundManager_t2198727358 * ___mInstance_7;
	// System.Object Vuforia.VideoBackgroundManager::mPadlock
	RuntimeObject * ___mPadlock_8;

public:
	inline static int32_t get_offset_of_mInstance_7() { return static_cast<int32_t>(offsetof(VideoBackgroundManager_t2198727358_StaticFields, ___mInstance_7)); }
	inline VideoBackgroundManager_t2198727358 * get_mInstance_7() const { return ___mInstance_7; }
	inline VideoBackgroundManager_t2198727358 ** get_address_of_mInstance_7() { return &___mInstance_7; }
	inline void set_mInstance_7(VideoBackgroundManager_t2198727358 * value)
	{
		___mInstance_7 = value;
		Il2CppCodeGenWriteBarrier((&___mInstance_7), value);
	}

	inline static int32_t get_offset_of_mPadlock_8() { return static_cast<int32_t>(offsetof(VideoBackgroundManager_t2198727358_StaticFields, ___mPadlock_8)); }
	inline RuntimeObject * get_mPadlock_8() const { return ___mPadlock_8; }
	inline RuntimeObject ** get_address_of_mPadlock_8() { return &___mPadlock_8; }
	inline void set_mPadlock_8(RuntimeObject * value)
	{
		___mPadlock_8 = value;
		Il2CppCodeGenWriteBarrier((&___mPadlock_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIDEOBACKGROUNDMANAGER_T2198727358_H
#ifndef VIRTUALBUTTON_T386166510_H
#define VIRTUALBUTTON_T386166510_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VirtualButton
struct  VirtualButton_t386166510  : public RuntimeObject
{
public:
	// System.String Vuforia.VirtualButton::mName
	String_t* ___mName_0;
	// System.Int32 Vuforia.VirtualButton::mID
	int32_t ___mID_1;
	// Vuforia.RectangleData Vuforia.VirtualButton::mArea
	RectangleData_t1039179782  ___mArea_2;
	// System.Boolean Vuforia.VirtualButton::mIsEnabled
	bool ___mIsEnabled_3;
	// Vuforia.ImageTarget Vuforia.VirtualButton::mParentImageTarget
	RuntimeObject* ___mParentImageTarget_4;
	// Vuforia.DataSet Vuforia.VirtualButton::mParentDataSet
	DataSet_t3286034874 * ___mParentDataSet_5;

public:
	inline static int32_t get_offset_of_mName_0() { return static_cast<int32_t>(offsetof(VirtualButton_t386166510, ___mName_0)); }
	inline String_t* get_mName_0() const { return ___mName_0; }
	inline String_t** get_address_of_mName_0() { return &___mName_0; }
	inline void set_mName_0(String_t* value)
	{
		___mName_0 = value;
		Il2CppCodeGenWriteBarrier((&___mName_0), value);
	}

	inline static int32_t get_offset_of_mID_1() { return static_cast<int32_t>(offsetof(VirtualButton_t386166510, ___mID_1)); }
	inline int32_t get_mID_1() const { return ___mID_1; }
	inline int32_t* get_address_of_mID_1() { return &___mID_1; }
	inline void set_mID_1(int32_t value)
	{
		___mID_1 = value;
	}

	inline static int32_t get_offset_of_mArea_2() { return static_cast<int32_t>(offsetof(VirtualButton_t386166510, ___mArea_2)); }
	inline RectangleData_t1039179782  get_mArea_2() const { return ___mArea_2; }
	inline RectangleData_t1039179782 * get_address_of_mArea_2() { return &___mArea_2; }
	inline void set_mArea_2(RectangleData_t1039179782  value)
	{
		___mArea_2 = value;
	}

	inline static int32_t get_offset_of_mIsEnabled_3() { return static_cast<int32_t>(offsetof(VirtualButton_t386166510, ___mIsEnabled_3)); }
	inline bool get_mIsEnabled_3() const { return ___mIsEnabled_3; }
	inline bool* get_address_of_mIsEnabled_3() { return &___mIsEnabled_3; }
	inline void set_mIsEnabled_3(bool value)
	{
		___mIsEnabled_3 = value;
	}

	inline static int32_t get_offset_of_mParentImageTarget_4() { return static_cast<int32_t>(offsetof(VirtualButton_t386166510, ___mParentImageTarget_4)); }
	inline RuntimeObject* get_mParentImageTarget_4() const { return ___mParentImageTarget_4; }
	inline RuntimeObject** get_address_of_mParentImageTarget_4() { return &___mParentImageTarget_4; }
	inline void set_mParentImageTarget_4(RuntimeObject* value)
	{
		___mParentImageTarget_4 = value;
		Il2CppCodeGenWriteBarrier((&___mParentImageTarget_4), value);
	}

	inline static int32_t get_offset_of_mParentDataSet_5() { return static_cast<int32_t>(offsetof(VirtualButton_t386166510, ___mParentDataSet_5)); }
	inline DataSet_t3286034874 * get_mParentDataSet_5() const { return ___mParentDataSet_5; }
	inline DataSet_t3286034874 ** get_address_of_mParentDataSet_5() { return &___mParentDataSet_5; }
	inline void set_mParentDataSet_5(DataSet_t3286034874 * value)
	{
		___mParentDataSet_5 = value;
		Il2CppCodeGenWriteBarrier((&___mParentDataSet_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIRTUALBUTTON_T386166510_H
#ifndef VUFORIAARCONTROLLER_T1876945237_H
#define VUFORIAARCONTROLLER_T1876945237_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaARController
struct  VuforiaARController_t1876945237  : public ARController_t116632334
{
public:
	// Vuforia.CameraDevice/CameraDeviceMode Vuforia.VuforiaARController::CameraDeviceModeSetting
	int32_t ___CameraDeviceModeSetting_1;
	// System.Int32 Vuforia.VuforiaARController::MaxSimultaneousImageTargets
	int32_t ___MaxSimultaneousImageTargets_2;
	// System.Int32 Vuforia.VuforiaARController::MaxSimultaneousObjectTargets
	int32_t ___MaxSimultaneousObjectTargets_3;
	// System.Boolean Vuforia.VuforiaARController::UseDelayedLoadingObjectTargets
	bool ___UseDelayedLoadingObjectTargets_4;
	// Vuforia.CameraDevice/CameraDirection Vuforia.VuforiaARController::CameraDirection
	int32_t ___CameraDirection_5;
	// Vuforia.VuforiaRenderer/VideoBackgroundReflection Vuforia.VuforiaARController::MirrorVideoBackground
	int32_t ___MirrorVideoBackground_6;
	// Vuforia.VuforiaARController/WorldCenterMode Vuforia.VuforiaARController::mWorldCenterMode
	int32_t ___mWorldCenterMode_7;
	// Vuforia.TrackableBehaviour Vuforia.VuforiaARController::mWorldCenter
	TrackableBehaviour_t1113559212 * ___mWorldCenter_8;
	// System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler> Vuforia.VuforiaARController::mVideoBgEventHandlers
	List_1_t905170877 * ___mVideoBgEventHandlers_9;
	// System.Action Vuforia.VuforiaARController::mOnVuforiaInitialized
	Action_t1264377477 * ___mOnVuforiaInitialized_10;
	// System.Action Vuforia.VuforiaARController::mOnVuforiaStarted
	Action_t1264377477 * ___mOnVuforiaStarted_11;
	// System.Action Vuforia.VuforiaARController::mOnVuforiaDeinitialized
	Action_t1264377477 * ___mOnVuforiaDeinitialized_12;
	// System.Action Vuforia.VuforiaARController::mOnTrackablesUpdated
	Action_t1264377477 * ___mOnTrackablesUpdated_13;
	// System.Action Vuforia.VuforiaARController::mRenderOnUpdate
	Action_t1264377477 * ___mRenderOnUpdate_14;
	// System.Action`1<System.Boolean> Vuforia.VuforiaARController::mOnPause
	Action_1_t269755560 * ___mOnPause_15;
	// System.Boolean Vuforia.VuforiaARController::mPaused
	bool ___mPaused_16;
	// System.Action Vuforia.VuforiaARController::mOnBackgroundTextureChanged
	Action_t1264377477 * ___mOnBackgroundTextureChanged_17;
	// System.Boolean Vuforia.VuforiaARController::mStartHasBeenInvoked
	bool ___mStartHasBeenInvoked_18;
	// System.Boolean Vuforia.VuforiaARController::mHasStarted
	bool ___mHasStarted_19;
	// Vuforia.ICameraConfiguration Vuforia.VuforiaARController::mCameraConfiguration
	RuntimeObject* ___mCameraConfiguration_20;
	// Vuforia.DigitalEyewearARController Vuforia.VuforiaARController::mEyewearBehaviour
	DigitalEyewearARController_t1054226036 * ___mEyewearBehaviour_21;
	// Vuforia.VideoBackgroundManager Vuforia.VuforiaARController::mVideoBackgroundMgr
	VideoBackgroundManager_t2198727358 * ___mVideoBackgroundMgr_22;
	// System.Boolean Vuforia.VuforiaARController::mCheckStopCamera
	bool ___mCheckStopCamera_23;
	// UnityEngine.Material Vuforia.VuforiaARController::mClearMaterial
	Material_t340375123 * ___mClearMaterial_24;
	// System.Boolean Vuforia.VuforiaARController::mMetalRendering
	bool ___mMetalRendering_25;
	// System.Boolean Vuforia.VuforiaARController::mHasStartedOnce
	bool ___mHasStartedOnce_26;
	// System.Boolean Vuforia.VuforiaARController::mWasEnabledBeforePause
	bool ___mWasEnabledBeforePause_27;
	// System.Boolean Vuforia.VuforiaARController::mObjectTrackerWasActiveBeforePause
	bool ___mObjectTrackerWasActiveBeforePause_28;
	// System.Boolean Vuforia.VuforiaARController::mObjectTrackerWasActiveBeforeDisabling
	bool ___mObjectTrackerWasActiveBeforeDisabling_29;
	// System.Int32 Vuforia.VuforiaARController::mLastUpdatedFrame
	int32_t ___mLastUpdatedFrame_30;
	// System.Collections.Generic.List`1<System.Type> Vuforia.VuforiaARController::mTrackersRequestedToDeinit
	List_1_t3956019502 * ___mTrackersRequestedToDeinit_31;

public:
	inline static int32_t get_offset_of_CameraDeviceModeSetting_1() { return static_cast<int32_t>(offsetof(VuforiaARController_t1876945237, ___CameraDeviceModeSetting_1)); }
	inline int32_t get_CameraDeviceModeSetting_1() const { return ___CameraDeviceModeSetting_1; }
	inline int32_t* get_address_of_CameraDeviceModeSetting_1() { return &___CameraDeviceModeSetting_1; }
	inline void set_CameraDeviceModeSetting_1(int32_t value)
	{
		___CameraDeviceModeSetting_1 = value;
	}

	inline static int32_t get_offset_of_MaxSimultaneousImageTargets_2() { return static_cast<int32_t>(offsetof(VuforiaARController_t1876945237, ___MaxSimultaneousImageTargets_2)); }
	inline int32_t get_MaxSimultaneousImageTargets_2() const { return ___MaxSimultaneousImageTargets_2; }
	inline int32_t* get_address_of_MaxSimultaneousImageTargets_2() { return &___MaxSimultaneousImageTargets_2; }
	inline void set_MaxSimultaneousImageTargets_2(int32_t value)
	{
		___MaxSimultaneousImageTargets_2 = value;
	}

	inline static int32_t get_offset_of_MaxSimultaneousObjectTargets_3() { return static_cast<int32_t>(offsetof(VuforiaARController_t1876945237, ___MaxSimultaneousObjectTargets_3)); }
	inline int32_t get_MaxSimultaneousObjectTargets_3() const { return ___MaxSimultaneousObjectTargets_3; }
	inline int32_t* get_address_of_MaxSimultaneousObjectTargets_3() { return &___MaxSimultaneousObjectTargets_3; }
	inline void set_MaxSimultaneousObjectTargets_3(int32_t value)
	{
		___MaxSimultaneousObjectTargets_3 = value;
	}

	inline static int32_t get_offset_of_UseDelayedLoadingObjectTargets_4() { return static_cast<int32_t>(offsetof(VuforiaARController_t1876945237, ___UseDelayedLoadingObjectTargets_4)); }
	inline bool get_UseDelayedLoadingObjectTargets_4() const { return ___UseDelayedLoadingObjectTargets_4; }
	inline bool* get_address_of_UseDelayedLoadingObjectTargets_4() { return &___UseDelayedLoadingObjectTargets_4; }
	inline void set_UseDelayedLoadingObjectTargets_4(bool value)
	{
		___UseDelayedLoadingObjectTargets_4 = value;
	}

	inline static int32_t get_offset_of_CameraDirection_5() { return static_cast<int32_t>(offsetof(VuforiaARController_t1876945237, ___CameraDirection_5)); }
	inline int32_t get_CameraDirection_5() const { return ___CameraDirection_5; }
	inline int32_t* get_address_of_CameraDirection_5() { return &___CameraDirection_5; }
	inline void set_CameraDirection_5(int32_t value)
	{
		___CameraDirection_5 = value;
	}

	inline static int32_t get_offset_of_MirrorVideoBackground_6() { return static_cast<int32_t>(offsetof(VuforiaARController_t1876945237, ___MirrorVideoBackground_6)); }
	inline int32_t get_MirrorVideoBackground_6() const { return ___MirrorVideoBackground_6; }
	inline int32_t* get_address_of_MirrorVideoBackground_6() { return &___MirrorVideoBackground_6; }
	inline void set_MirrorVideoBackground_6(int32_t value)
	{
		___MirrorVideoBackground_6 = value;
	}

	inline static int32_t get_offset_of_mWorldCenterMode_7() { return static_cast<int32_t>(offsetof(VuforiaARController_t1876945237, ___mWorldCenterMode_7)); }
	inline int32_t get_mWorldCenterMode_7() const { return ___mWorldCenterMode_7; }
	inline int32_t* get_address_of_mWorldCenterMode_7() { return &___mWorldCenterMode_7; }
	inline void set_mWorldCenterMode_7(int32_t value)
	{
		___mWorldCenterMode_7 = value;
	}

	inline static int32_t get_offset_of_mWorldCenter_8() { return static_cast<int32_t>(offsetof(VuforiaARController_t1876945237, ___mWorldCenter_8)); }
	inline TrackableBehaviour_t1113559212 * get_mWorldCenter_8() const { return ___mWorldCenter_8; }
	inline TrackableBehaviour_t1113559212 ** get_address_of_mWorldCenter_8() { return &___mWorldCenter_8; }
	inline void set_mWorldCenter_8(TrackableBehaviour_t1113559212 * value)
	{
		___mWorldCenter_8 = value;
		Il2CppCodeGenWriteBarrier((&___mWorldCenter_8), value);
	}

	inline static int32_t get_offset_of_mVideoBgEventHandlers_9() { return static_cast<int32_t>(offsetof(VuforiaARController_t1876945237, ___mVideoBgEventHandlers_9)); }
	inline List_1_t905170877 * get_mVideoBgEventHandlers_9() const { return ___mVideoBgEventHandlers_9; }
	inline List_1_t905170877 ** get_address_of_mVideoBgEventHandlers_9() { return &___mVideoBgEventHandlers_9; }
	inline void set_mVideoBgEventHandlers_9(List_1_t905170877 * value)
	{
		___mVideoBgEventHandlers_9 = value;
		Il2CppCodeGenWriteBarrier((&___mVideoBgEventHandlers_9), value);
	}

	inline static int32_t get_offset_of_mOnVuforiaInitialized_10() { return static_cast<int32_t>(offsetof(VuforiaARController_t1876945237, ___mOnVuforiaInitialized_10)); }
	inline Action_t1264377477 * get_mOnVuforiaInitialized_10() const { return ___mOnVuforiaInitialized_10; }
	inline Action_t1264377477 ** get_address_of_mOnVuforiaInitialized_10() { return &___mOnVuforiaInitialized_10; }
	inline void set_mOnVuforiaInitialized_10(Action_t1264377477 * value)
	{
		___mOnVuforiaInitialized_10 = value;
		Il2CppCodeGenWriteBarrier((&___mOnVuforiaInitialized_10), value);
	}

	inline static int32_t get_offset_of_mOnVuforiaStarted_11() { return static_cast<int32_t>(offsetof(VuforiaARController_t1876945237, ___mOnVuforiaStarted_11)); }
	inline Action_t1264377477 * get_mOnVuforiaStarted_11() const { return ___mOnVuforiaStarted_11; }
	inline Action_t1264377477 ** get_address_of_mOnVuforiaStarted_11() { return &___mOnVuforiaStarted_11; }
	inline void set_mOnVuforiaStarted_11(Action_t1264377477 * value)
	{
		___mOnVuforiaStarted_11 = value;
		Il2CppCodeGenWriteBarrier((&___mOnVuforiaStarted_11), value);
	}

	inline static int32_t get_offset_of_mOnVuforiaDeinitialized_12() { return static_cast<int32_t>(offsetof(VuforiaARController_t1876945237, ___mOnVuforiaDeinitialized_12)); }
	inline Action_t1264377477 * get_mOnVuforiaDeinitialized_12() const { return ___mOnVuforiaDeinitialized_12; }
	inline Action_t1264377477 ** get_address_of_mOnVuforiaDeinitialized_12() { return &___mOnVuforiaDeinitialized_12; }
	inline void set_mOnVuforiaDeinitialized_12(Action_t1264377477 * value)
	{
		___mOnVuforiaDeinitialized_12 = value;
		Il2CppCodeGenWriteBarrier((&___mOnVuforiaDeinitialized_12), value);
	}

	inline static int32_t get_offset_of_mOnTrackablesUpdated_13() { return static_cast<int32_t>(offsetof(VuforiaARController_t1876945237, ___mOnTrackablesUpdated_13)); }
	inline Action_t1264377477 * get_mOnTrackablesUpdated_13() const { return ___mOnTrackablesUpdated_13; }
	inline Action_t1264377477 ** get_address_of_mOnTrackablesUpdated_13() { return &___mOnTrackablesUpdated_13; }
	inline void set_mOnTrackablesUpdated_13(Action_t1264377477 * value)
	{
		___mOnTrackablesUpdated_13 = value;
		Il2CppCodeGenWriteBarrier((&___mOnTrackablesUpdated_13), value);
	}

	inline static int32_t get_offset_of_mRenderOnUpdate_14() { return static_cast<int32_t>(offsetof(VuforiaARController_t1876945237, ___mRenderOnUpdate_14)); }
	inline Action_t1264377477 * get_mRenderOnUpdate_14() const { return ___mRenderOnUpdate_14; }
	inline Action_t1264377477 ** get_address_of_mRenderOnUpdate_14() { return &___mRenderOnUpdate_14; }
	inline void set_mRenderOnUpdate_14(Action_t1264377477 * value)
	{
		___mRenderOnUpdate_14 = value;
		Il2CppCodeGenWriteBarrier((&___mRenderOnUpdate_14), value);
	}

	inline static int32_t get_offset_of_mOnPause_15() { return static_cast<int32_t>(offsetof(VuforiaARController_t1876945237, ___mOnPause_15)); }
	inline Action_1_t269755560 * get_mOnPause_15() const { return ___mOnPause_15; }
	inline Action_1_t269755560 ** get_address_of_mOnPause_15() { return &___mOnPause_15; }
	inline void set_mOnPause_15(Action_1_t269755560 * value)
	{
		___mOnPause_15 = value;
		Il2CppCodeGenWriteBarrier((&___mOnPause_15), value);
	}

	inline static int32_t get_offset_of_mPaused_16() { return static_cast<int32_t>(offsetof(VuforiaARController_t1876945237, ___mPaused_16)); }
	inline bool get_mPaused_16() const { return ___mPaused_16; }
	inline bool* get_address_of_mPaused_16() { return &___mPaused_16; }
	inline void set_mPaused_16(bool value)
	{
		___mPaused_16 = value;
	}

	inline static int32_t get_offset_of_mOnBackgroundTextureChanged_17() { return static_cast<int32_t>(offsetof(VuforiaARController_t1876945237, ___mOnBackgroundTextureChanged_17)); }
	inline Action_t1264377477 * get_mOnBackgroundTextureChanged_17() const { return ___mOnBackgroundTextureChanged_17; }
	inline Action_t1264377477 ** get_address_of_mOnBackgroundTextureChanged_17() { return &___mOnBackgroundTextureChanged_17; }
	inline void set_mOnBackgroundTextureChanged_17(Action_t1264377477 * value)
	{
		___mOnBackgroundTextureChanged_17 = value;
		Il2CppCodeGenWriteBarrier((&___mOnBackgroundTextureChanged_17), value);
	}

	inline static int32_t get_offset_of_mStartHasBeenInvoked_18() { return static_cast<int32_t>(offsetof(VuforiaARController_t1876945237, ___mStartHasBeenInvoked_18)); }
	inline bool get_mStartHasBeenInvoked_18() const { return ___mStartHasBeenInvoked_18; }
	inline bool* get_address_of_mStartHasBeenInvoked_18() { return &___mStartHasBeenInvoked_18; }
	inline void set_mStartHasBeenInvoked_18(bool value)
	{
		___mStartHasBeenInvoked_18 = value;
	}

	inline static int32_t get_offset_of_mHasStarted_19() { return static_cast<int32_t>(offsetof(VuforiaARController_t1876945237, ___mHasStarted_19)); }
	inline bool get_mHasStarted_19() const { return ___mHasStarted_19; }
	inline bool* get_address_of_mHasStarted_19() { return &___mHasStarted_19; }
	inline void set_mHasStarted_19(bool value)
	{
		___mHasStarted_19 = value;
	}

	inline static int32_t get_offset_of_mCameraConfiguration_20() { return static_cast<int32_t>(offsetof(VuforiaARController_t1876945237, ___mCameraConfiguration_20)); }
	inline RuntimeObject* get_mCameraConfiguration_20() const { return ___mCameraConfiguration_20; }
	inline RuntimeObject** get_address_of_mCameraConfiguration_20() { return &___mCameraConfiguration_20; }
	inline void set_mCameraConfiguration_20(RuntimeObject* value)
	{
		___mCameraConfiguration_20 = value;
		Il2CppCodeGenWriteBarrier((&___mCameraConfiguration_20), value);
	}

	inline static int32_t get_offset_of_mEyewearBehaviour_21() { return static_cast<int32_t>(offsetof(VuforiaARController_t1876945237, ___mEyewearBehaviour_21)); }
	inline DigitalEyewearARController_t1054226036 * get_mEyewearBehaviour_21() const { return ___mEyewearBehaviour_21; }
	inline DigitalEyewearARController_t1054226036 ** get_address_of_mEyewearBehaviour_21() { return &___mEyewearBehaviour_21; }
	inline void set_mEyewearBehaviour_21(DigitalEyewearARController_t1054226036 * value)
	{
		___mEyewearBehaviour_21 = value;
		Il2CppCodeGenWriteBarrier((&___mEyewearBehaviour_21), value);
	}

	inline static int32_t get_offset_of_mVideoBackgroundMgr_22() { return static_cast<int32_t>(offsetof(VuforiaARController_t1876945237, ___mVideoBackgroundMgr_22)); }
	inline VideoBackgroundManager_t2198727358 * get_mVideoBackgroundMgr_22() const { return ___mVideoBackgroundMgr_22; }
	inline VideoBackgroundManager_t2198727358 ** get_address_of_mVideoBackgroundMgr_22() { return &___mVideoBackgroundMgr_22; }
	inline void set_mVideoBackgroundMgr_22(VideoBackgroundManager_t2198727358 * value)
	{
		___mVideoBackgroundMgr_22 = value;
		Il2CppCodeGenWriteBarrier((&___mVideoBackgroundMgr_22), value);
	}

	inline static int32_t get_offset_of_mCheckStopCamera_23() { return static_cast<int32_t>(offsetof(VuforiaARController_t1876945237, ___mCheckStopCamera_23)); }
	inline bool get_mCheckStopCamera_23() const { return ___mCheckStopCamera_23; }
	inline bool* get_address_of_mCheckStopCamera_23() { return &___mCheckStopCamera_23; }
	inline void set_mCheckStopCamera_23(bool value)
	{
		___mCheckStopCamera_23 = value;
	}

	inline static int32_t get_offset_of_mClearMaterial_24() { return static_cast<int32_t>(offsetof(VuforiaARController_t1876945237, ___mClearMaterial_24)); }
	inline Material_t340375123 * get_mClearMaterial_24() const { return ___mClearMaterial_24; }
	inline Material_t340375123 ** get_address_of_mClearMaterial_24() { return &___mClearMaterial_24; }
	inline void set_mClearMaterial_24(Material_t340375123 * value)
	{
		___mClearMaterial_24 = value;
		Il2CppCodeGenWriteBarrier((&___mClearMaterial_24), value);
	}

	inline static int32_t get_offset_of_mMetalRendering_25() { return static_cast<int32_t>(offsetof(VuforiaARController_t1876945237, ___mMetalRendering_25)); }
	inline bool get_mMetalRendering_25() const { return ___mMetalRendering_25; }
	inline bool* get_address_of_mMetalRendering_25() { return &___mMetalRendering_25; }
	inline void set_mMetalRendering_25(bool value)
	{
		___mMetalRendering_25 = value;
	}

	inline static int32_t get_offset_of_mHasStartedOnce_26() { return static_cast<int32_t>(offsetof(VuforiaARController_t1876945237, ___mHasStartedOnce_26)); }
	inline bool get_mHasStartedOnce_26() const { return ___mHasStartedOnce_26; }
	inline bool* get_address_of_mHasStartedOnce_26() { return &___mHasStartedOnce_26; }
	inline void set_mHasStartedOnce_26(bool value)
	{
		___mHasStartedOnce_26 = value;
	}

	inline static int32_t get_offset_of_mWasEnabledBeforePause_27() { return static_cast<int32_t>(offsetof(VuforiaARController_t1876945237, ___mWasEnabledBeforePause_27)); }
	inline bool get_mWasEnabledBeforePause_27() const { return ___mWasEnabledBeforePause_27; }
	inline bool* get_address_of_mWasEnabledBeforePause_27() { return &___mWasEnabledBeforePause_27; }
	inline void set_mWasEnabledBeforePause_27(bool value)
	{
		___mWasEnabledBeforePause_27 = value;
	}

	inline static int32_t get_offset_of_mObjectTrackerWasActiveBeforePause_28() { return static_cast<int32_t>(offsetof(VuforiaARController_t1876945237, ___mObjectTrackerWasActiveBeforePause_28)); }
	inline bool get_mObjectTrackerWasActiveBeforePause_28() const { return ___mObjectTrackerWasActiveBeforePause_28; }
	inline bool* get_address_of_mObjectTrackerWasActiveBeforePause_28() { return &___mObjectTrackerWasActiveBeforePause_28; }
	inline void set_mObjectTrackerWasActiveBeforePause_28(bool value)
	{
		___mObjectTrackerWasActiveBeforePause_28 = value;
	}

	inline static int32_t get_offset_of_mObjectTrackerWasActiveBeforeDisabling_29() { return static_cast<int32_t>(offsetof(VuforiaARController_t1876945237, ___mObjectTrackerWasActiveBeforeDisabling_29)); }
	inline bool get_mObjectTrackerWasActiveBeforeDisabling_29() const { return ___mObjectTrackerWasActiveBeforeDisabling_29; }
	inline bool* get_address_of_mObjectTrackerWasActiveBeforeDisabling_29() { return &___mObjectTrackerWasActiveBeforeDisabling_29; }
	inline void set_mObjectTrackerWasActiveBeforeDisabling_29(bool value)
	{
		___mObjectTrackerWasActiveBeforeDisabling_29 = value;
	}

	inline static int32_t get_offset_of_mLastUpdatedFrame_30() { return static_cast<int32_t>(offsetof(VuforiaARController_t1876945237, ___mLastUpdatedFrame_30)); }
	inline int32_t get_mLastUpdatedFrame_30() const { return ___mLastUpdatedFrame_30; }
	inline int32_t* get_address_of_mLastUpdatedFrame_30() { return &___mLastUpdatedFrame_30; }
	inline void set_mLastUpdatedFrame_30(int32_t value)
	{
		___mLastUpdatedFrame_30 = value;
	}

	inline static int32_t get_offset_of_mTrackersRequestedToDeinit_31() { return static_cast<int32_t>(offsetof(VuforiaARController_t1876945237, ___mTrackersRequestedToDeinit_31)); }
	inline List_1_t3956019502 * get_mTrackersRequestedToDeinit_31() const { return ___mTrackersRequestedToDeinit_31; }
	inline List_1_t3956019502 ** get_address_of_mTrackersRequestedToDeinit_31() { return &___mTrackersRequestedToDeinit_31; }
	inline void set_mTrackersRequestedToDeinit_31(List_1_t3956019502 * value)
	{
		___mTrackersRequestedToDeinit_31 = value;
		Il2CppCodeGenWriteBarrier((&___mTrackersRequestedToDeinit_31), value);
	}
};

struct VuforiaARController_t1876945237_StaticFields
{
public:
	// Vuforia.VuforiaARController Vuforia.VuforiaARController::mInstance
	VuforiaARController_t1876945237 * ___mInstance_32;
	// System.Object Vuforia.VuforiaARController::mPadlock
	RuntimeObject * ___mPadlock_33;

public:
	inline static int32_t get_offset_of_mInstance_32() { return static_cast<int32_t>(offsetof(VuforiaARController_t1876945237_StaticFields, ___mInstance_32)); }
	inline VuforiaARController_t1876945237 * get_mInstance_32() const { return ___mInstance_32; }
	inline VuforiaARController_t1876945237 ** get_address_of_mInstance_32() { return &___mInstance_32; }
	inline void set_mInstance_32(VuforiaARController_t1876945237 * value)
	{
		___mInstance_32 = value;
		Il2CppCodeGenWriteBarrier((&___mInstance_32), value);
	}

	inline static int32_t get_offset_of_mPadlock_33() { return static_cast<int32_t>(offsetof(VuforiaARController_t1876945237_StaticFields, ___mPadlock_33)); }
	inline RuntimeObject * get_mPadlock_33() const { return ___mPadlock_33; }
	inline RuntimeObject ** get_address_of_mPadlock_33() { return &___mPadlock_33; }
	inline void set_mPadlock_33(RuntimeObject * value)
	{
		___mPadlock_33 = value;
		Il2CppCodeGenWriteBarrier((&___mPadlock_33), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VUFORIAARCONTROLLER_T1876945237_H
#ifndef VUFORIAMANAGER_T1653423889_H
#define VUFORIAMANAGER_T1653423889_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaManager
struct  VuforiaManager_t1653423889  : public RuntimeObject
{
public:
	// System.Boolean Vuforia.VuforiaManager::<VideoBackgroundTextureSet>k__BackingField
	bool ___U3CVideoBackgroundTextureSetU3Ek__BackingField_0;
	// Vuforia.VuforiaARController/WorldCenterMode Vuforia.VuforiaManager::mWorldCenterMode
	int32_t ___mWorldCenterMode_2;
	// Vuforia.WorldCenterTrackableBehaviour Vuforia.VuforiaManager::mWorldCenter
	RuntimeObject* ___mWorldCenter_3;
	// Vuforia.VuMarkBehaviour Vuforia.VuforiaManager::mVuMarkWorldCenter
	VuMarkBehaviour_t1178230459 * ___mVuMarkWorldCenter_4;
	// UnityEngine.Transform Vuforia.VuforiaManager::mARCameraTransform
	Transform_t3600365921 * ___mARCameraTransform_5;
	// UnityEngine.Transform Vuforia.VuforiaManager::mCentralAnchorPoint
	Transform_t3600365921 * ___mCentralAnchorPoint_6;
	// Vuforia.TrackerData/TrackableResultData[] Vuforia.VuforiaManager::mTrackableResultDataArray
	TrackableResultDataU5BU5D_t4273811049* ___mTrackableResultDataArray_7;
	// Vuforia.TrackerData/VuMarkTargetData[] Vuforia.VuforiaManager::mVuMarkDataArray
	VuMarkTargetDataU5BU5D_t4015091482* ___mVuMarkDataArray_8;
	// Vuforia.TrackerData/VuMarkTargetResultData[] Vuforia.VuforiaManager::mVuMarkResultDataArray
	VuMarkTargetResultDataU5BU5D_t2157423781* ___mVuMarkResultDataArray_9;
	// System.Collections.Generic.LinkedList`1<Vuforia.VuforiaManager/TrackableIdPair> Vuforia.VuforiaManager::mWCTrackableFoundQueue
	LinkedList_1_t3066996466 * ___mWCTrackableFoundQueue_10;
	// System.IntPtr Vuforia.VuforiaManager::mImageHeaderData
	intptr_t ___mImageHeaderData_11;
	// System.Int32 Vuforia.VuforiaManager::mNumImageHeaders
	int32_t ___mNumImageHeaders_12;
	// System.Int32 Vuforia.VuforiaManager::mInjectedFrameIdx
	int32_t ___mInjectedFrameIdx_13;
	// System.IntPtr Vuforia.VuforiaManager::mLastProcessedFrameStatePtr
	intptr_t ___mLastProcessedFrameStatePtr_14;
	// System.Boolean Vuforia.VuforiaManager::mInitialized
	bool ___mInitialized_15;
	// System.Boolean Vuforia.VuforiaManager::mPaused
	bool ___mPaused_16;
	// Vuforia.TrackerData/FrameState Vuforia.VuforiaManager::mFrameState
	FrameState_t2717258284  ___mFrameState_17;
	// Vuforia.VuforiaManager/AutoRotationState Vuforia.VuforiaManager::mAutoRotationState
	AutoRotationState_t2150317116  ___mAutoRotationState_18;
	// System.Boolean Vuforia.VuforiaManager::mVideoBackgroundNeedsRedrawing
	bool ___mVideoBackgroundNeedsRedrawing_19;
	// System.Int32 Vuforia.VuforiaManager::mDiscardStatesForRendering
	int32_t ___mDiscardStatesForRendering_20;
	// System.Int32 Vuforia.VuforiaManager::mLastFrameIdx
	int32_t ___mLastFrameIdx_21;
	// System.Boolean Vuforia.VuforiaManager::mIsSeeThroughDevice
	bool ___mIsSeeThroughDevice_22;
	// Vuforia.LateLatchingManager Vuforia.VuforiaManager::mLateLatchingManager
	LateLatchingManager_t3198550161 * ___mLateLatchingManager_23;
	// Vuforia.CameraCalibrationComparer Vuforia.VuforiaManager::mCameraCalibrationComparer
	CameraCalibrationComparer_t2990055837 * ___mCameraCalibrationComparer_24;

public:
	inline static int32_t get_offset_of_U3CVideoBackgroundTextureSetU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(VuforiaManager_t1653423889, ___U3CVideoBackgroundTextureSetU3Ek__BackingField_0)); }
	inline bool get_U3CVideoBackgroundTextureSetU3Ek__BackingField_0() const { return ___U3CVideoBackgroundTextureSetU3Ek__BackingField_0; }
	inline bool* get_address_of_U3CVideoBackgroundTextureSetU3Ek__BackingField_0() { return &___U3CVideoBackgroundTextureSetU3Ek__BackingField_0; }
	inline void set_U3CVideoBackgroundTextureSetU3Ek__BackingField_0(bool value)
	{
		___U3CVideoBackgroundTextureSetU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_mWorldCenterMode_2() { return static_cast<int32_t>(offsetof(VuforiaManager_t1653423889, ___mWorldCenterMode_2)); }
	inline int32_t get_mWorldCenterMode_2() const { return ___mWorldCenterMode_2; }
	inline int32_t* get_address_of_mWorldCenterMode_2() { return &___mWorldCenterMode_2; }
	inline void set_mWorldCenterMode_2(int32_t value)
	{
		___mWorldCenterMode_2 = value;
	}

	inline static int32_t get_offset_of_mWorldCenter_3() { return static_cast<int32_t>(offsetof(VuforiaManager_t1653423889, ___mWorldCenter_3)); }
	inline RuntimeObject* get_mWorldCenter_3() const { return ___mWorldCenter_3; }
	inline RuntimeObject** get_address_of_mWorldCenter_3() { return &___mWorldCenter_3; }
	inline void set_mWorldCenter_3(RuntimeObject* value)
	{
		___mWorldCenter_3 = value;
		Il2CppCodeGenWriteBarrier((&___mWorldCenter_3), value);
	}

	inline static int32_t get_offset_of_mVuMarkWorldCenter_4() { return static_cast<int32_t>(offsetof(VuforiaManager_t1653423889, ___mVuMarkWorldCenter_4)); }
	inline VuMarkBehaviour_t1178230459 * get_mVuMarkWorldCenter_4() const { return ___mVuMarkWorldCenter_4; }
	inline VuMarkBehaviour_t1178230459 ** get_address_of_mVuMarkWorldCenter_4() { return &___mVuMarkWorldCenter_4; }
	inline void set_mVuMarkWorldCenter_4(VuMarkBehaviour_t1178230459 * value)
	{
		___mVuMarkWorldCenter_4 = value;
		Il2CppCodeGenWriteBarrier((&___mVuMarkWorldCenter_4), value);
	}

	inline static int32_t get_offset_of_mARCameraTransform_5() { return static_cast<int32_t>(offsetof(VuforiaManager_t1653423889, ___mARCameraTransform_5)); }
	inline Transform_t3600365921 * get_mARCameraTransform_5() const { return ___mARCameraTransform_5; }
	inline Transform_t3600365921 ** get_address_of_mARCameraTransform_5() { return &___mARCameraTransform_5; }
	inline void set_mARCameraTransform_5(Transform_t3600365921 * value)
	{
		___mARCameraTransform_5 = value;
		Il2CppCodeGenWriteBarrier((&___mARCameraTransform_5), value);
	}

	inline static int32_t get_offset_of_mCentralAnchorPoint_6() { return static_cast<int32_t>(offsetof(VuforiaManager_t1653423889, ___mCentralAnchorPoint_6)); }
	inline Transform_t3600365921 * get_mCentralAnchorPoint_6() const { return ___mCentralAnchorPoint_6; }
	inline Transform_t3600365921 ** get_address_of_mCentralAnchorPoint_6() { return &___mCentralAnchorPoint_6; }
	inline void set_mCentralAnchorPoint_6(Transform_t3600365921 * value)
	{
		___mCentralAnchorPoint_6 = value;
		Il2CppCodeGenWriteBarrier((&___mCentralAnchorPoint_6), value);
	}

	inline static int32_t get_offset_of_mTrackableResultDataArray_7() { return static_cast<int32_t>(offsetof(VuforiaManager_t1653423889, ___mTrackableResultDataArray_7)); }
	inline TrackableResultDataU5BU5D_t4273811049* get_mTrackableResultDataArray_7() const { return ___mTrackableResultDataArray_7; }
	inline TrackableResultDataU5BU5D_t4273811049** get_address_of_mTrackableResultDataArray_7() { return &___mTrackableResultDataArray_7; }
	inline void set_mTrackableResultDataArray_7(TrackableResultDataU5BU5D_t4273811049* value)
	{
		___mTrackableResultDataArray_7 = value;
		Il2CppCodeGenWriteBarrier((&___mTrackableResultDataArray_7), value);
	}

	inline static int32_t get_offset_of_mVuMarkDataArray_8() { return static_cast<int32_t>(offsetof(VuforiaManager_t1653423889, ___mVuMarkDataArray_8)); }
	inline VuMarkTargetDataU5BU5D_t4015091482* get_mVuMarkDataArray_8() const { return ___mVuMarkDataArray_8; }
	inline VuMarkTargetDataU5BU5D_t4015091482** get_address_of_mVuMarkDataArray_8() { return &___mVuMarkDataArray_8; }
	inline void set_mVuMarkDataArray_8(VuMarkTargetDataU5BU5D_t4015091482* value)
	{
		___mVuMarkDataArray_8 = value;
		Il2CppCodeGenWriteBarrier((&___mVuMarkDataArray_8), value);
	}

	inline static int32_t get_offset_of_mVuMarkResultDataArray_9() { return static_cast<int32_t>(offsetof(VuforiaManager_t1653423889, ___mVuMarkResultDataArray_9)); }
	inline VuMarkTargetResultDataU5BU5D_t2157423781* get_mVuMarkResultDataArray_9() const { return ___mVuMarkResultDataArray_9; }
	inline VuMarkTargetResultDataU5BU5D_t2157423781** get_address_of_mVuMarkResultDataArray_9() { return &___mVuMarkResultDataArray_9; }
	inline void set_mVuMarkResultDataArray_9(VuMarkTargetResultDataU5BU5D_t2157423781* value)
	{
		___mVuMarkResultDataArray_9 = value;
		Il2CppCodeGenWriteBarrier((&___mVuMarkResultDataArray_9), value);
	}

	inline static int32_t get_offset_of_mWCTrackableFoundQueue_10() { return static_cast<int32_t>(offsetof(VuforiaManager_t1653423889, ___mWCTrackableFoundQueue_10)); }
	inline LinkedList_1_t3066996466 * get_mWCTrackableFoundQueue_10() const { return ___mWCTrackableFoundQueue_10; }
	inline LinkedList_1_t3066996466 ** get_address_of_mWCTrackableFoundQueue_10() { return &___mWCTrackableFoundQueue_10; }
	inline void set_mWCTrackableFoundQueue_10(LinkedList_1_t3066996466 * value)
	{
		___mWCTrackableFoundQueue_10 = value;
		Il2CppCodeGenWriteBarrier((&___mWCTrackableFoundQueue_10), value);
	}

	inline static int32_t get_offset_of_mImageHeaderData_11() { return static_cast<int32_t>(offsetof(VuforiaManager_t1653423889, ___mImageHeaderData_11)); }
	inline intptr_t get_mImageHeaderData_11() const { return ___mImageHeaderData_11; }
	inline intptr_t* get_address_of_mImageHeaderData_11() { return &___mImageHeaderData_11; }
	inline void set_mImageHeaderData_11(intptr_t value)
	{
		___mImageHeaderData_11 = value;
	}

	inline static int32_t get_offset_of_mNumImageHeaders_12() { return static_cast<int32_t>(offsetof(VuforiaManager_t1653423889, ___mNumImageHeaders_12)); }
	inline int32_t get_mNumImageHeaders_12() const { return ___mNumImageHeaders_12; }
	inline int32_t* get_address_of_mNumImageHeaders_12() { return &___mNumImageHeaders_12; }
	inline void set_mNumImageHeaders_12(int32_t value)
	{
		___mNumImageHeaders_12 = value;
	}

	inline static int32_t get_offset_of_mInjectedFrameIdx_13() { return static_cast<int32_t>(offsetof(VuforiaManager_t1653423889, ___mInjectedFrameIdx_13)); }
	inline int32_t get_mInjectedFrameIdx_13() const { return ___mInjectedFrameIdx_13; }
	inline int32_t* get_address_of_mInjectedFrameIdx_13() { return &___mInjectedFrameIdx_13; }
	inline void set_mInjectedFrameIdx_13(int32_t value)
	{
		___mInjectedFrameIdx_13 = value;
	}

	inline static int32_t get_offset_of_mLastProcessedFrameStatePtr_14() { return static_cast<int32_t>(offsetof(VuforiaManager_t1653423889, ___mLastProcessedFrameStatePtr_14)); }
	inline intptr_t get_mLastProcessedFrameStatePtr_14() const { return ___mLastProcessedFrameStatePtr_14; }
	inline intptr_t* get_address_of_mLastProcessedFrameStatePtr_14() { return &___mLastProcessedFrameStatePtr_14; }
	inline void set_mLastProcessedFrameStatePtr_14(intptr_t value)
	{
		___mLastProcessedFrameStatePtr_14 = value;
	}

	inline static int32_t get_offset_of_mInitialized_15() { return static_cast<int32_t>(offsetof(VuforiaManager_t1653423889, ___mInitialized_15)); }
	inline bool get_mInitialized_15() const { return ___mInitialized_15; }
	inline bool* get_address_of_mInitialized_15() { return &___mInitialized_15; }
	inline void set_mInitialized_15(bool value)
	{
		___mInitialized_15 = value;
	}

	inline static int32_t get_offset_of_mPaused_16() { return static_cast<int32_t>(offsetof(VuforiaManager_t1653423889, ___mPaused_16)); }
	inline bool get_mPaused_16() const { return ___mPaused_16; }
	inline bool* get_address_of_mPaused_16() { return &___mPaused_16; }
	inline void set_mPaused_16(bool value)
	{
		___mPaused_16 = value;
	}

	inline static int32_t get_offset_of_mFrameState_17() { return static_cast<int32_t>(offsetof(VuforiaManager_t1653423889, ___mFrameState_17)); }
	inline FrameState_t2717258284  get_mFrameState_17() const { return ___mFrameState_17; }
	inline FrameState_t2717258284 * get_address_of_mFrameState_17() { return &___mFrameState_17; }
	inline void set_mFrameState_17(FrameState_t2717258284  value)
	{
		___mFrameState_17 = value;
	}

	inline static int32_t get_offset_of_mAutoRotationState_18() { return static_cast<int32_t>(offsetof(VuforiaManager_t1653423889, ___mAutoRotationState_18)); }
	inline AutoRotationState_t2150317116  get_mAutoRotationState_18() const { return ___mAutoRotationState_18; }
	inline AutoRotationState_t2150317116 * get_address_of_mAutoRotationState_18() { return &___mAutoRotationState_18; }
	inline void set_mAutoRotationState_18(AutoRotationState_t2150317116  value)
	{
		___mAutoRotationState_18 = value;
	}

	inline static int32_t get_offset_of_mVideoBackgroundNeedsRedrawing_19() { return static_cast<int32_t>(offsetof(VuforiaManager_t1653423889, ___mVideoBackgroundNeedsRedrawing_19)); }
	inline bool get_mVideoBackgroundNeedsRedrawing_19() const { return ___mVideoBackgroundNeedsRedrawing_19; }
	inline bool* get_address_of_mVideoBackgroundNeedsRedrawing_19() { return &___mVideoBackgroundNeedsRedrawing_19; }
	inline void set_mVideoBackgroundNeedsRedrawing_19(bool value)
	{
		___mVideoBackgroundNeedsRedrawing_19 = value;
	}

	inline static int32_t get_offset_of_mDiscardStatesForRendering_20() { return static_cast<int32_t>(offsetof(VuforiaManager_t1653423889, ___mDiscardStatesForRendering_20)); }
	inline int32_t get_mDiscardStatesForRendering_20() const { return ___mDiscardStatesForRendering_20; }
	inline int32_t* get_address_of_mDiscardStatesForRendering_20() { return &___mDiscardStatesForRendering_20; }
	inline void set_mDiscardStatesForRendering_20(int32_t value)
	{
		___mDiscardStatesForRendering_20 = value;
	}

	inline static int32_t get_offset_of_mLastFrameIdx_21() { return static_cast<int32_t>(offsetof(VuforiaManager_t1653423889, ___mLastFrameIdx_21)); }
	inline int32_t get_mLastFrameIdx_21() const { return ___mLastFrameIdx_21; }
	inline int32_t* get_address_of_mLastFrameIdx_21() { return &___mLastFrameIdx_21; }
	inline void set_mLastFrameIdx_21(int32_t value)
	{
		___mLastFrameIdx_21 = value;
	}

	inline static int32_t get_offset_of_mIsSeeThroughDevice_22() { return static_cast<int32_t>(offsetof(VuforiaManager_t1653423889, ___mIsSeeThroughDevice_22)); }
	inline bool get_mIsSeeThroughDevice_22() const { return ___mIsSeeThroughDevice_22; }
	inline bool* get_address_of_mIsSeeThroughDevice_22() { return &___mIsSeeThroughDevice_22; }
	inline void set_mIsSeeThroughDevice_22(bool value)
	{
		___mIsSeeThroughDevice_22 = value;
	}

	inline static int32_t get_offset_of_mLateLatchingManager_23() { return static_cast<int32_t>(offsetof(VuforiaManager_t1653423889, ___mLateLatchingManager_23)); }
	inline LateLatchingManager_t3198550161 * get_mLateLatchingManager_23() const { return ___mLateLatchingManager_23; }
	inline LateLatchingManager_t3198550161 ** get_address_of_mLateLatchingManager_23() { return &___mLateLatchingManager_23; }
	inline void set_mLateLatchingManager_23(LateLatchingManager_t3198550161 * value)
	{
		___mLateLatchingManager_23 = value;
		Il2CppCodeGenWriteBarrier((&___mLateLatchingManager_23), value);
	}

	inline static int32_t get_offset_of_mCameraCalibrationComparer_24() { return static_cast<int32_t>(offsetof(VuforiaManager_t1653423889, ___mCameraCalibrationComparer_24)); }
	inline CameraCalibrationComparer_t2990055837 * get_mCameraCalibrationComparer_24() const { return ___mCameraCalibrationComparer_24; }
	inline CameraCalibrationComparer_t2990055837 ** get_address_of_mCameraCalibrationComparer_24() { return &___mCameraCalibrationComparer_24; }
	inline void set_mCameraCalibrationComparer_24(CameraCalibrationComparer_t2990055837 * value)
	{
		___mCameraCalibrationComparer_24 = value;
		Il2CppCodeGenWriteBarrier((&___mCameraCalibrationComparer_24), value);
	}
};

struct VuforiaManager_t1653423889_StaticFields
{
public:
	// Vuforia.VuforiaManager Vuforia.VuforiaManager::sInstance
	VuforiaManager_t1653423889 * ___sInstance_1;

public:
	inline static int32_t get_offset_of_sInstance_1() { return static_cast<int32_t>(offsetof(VuforiaManager_t1653423889_StaticFields, ___sInstance_1)); }
	inline VuforiaManager_t1653423889 * get_sInstance_1() const { return ___sInstance_1; }
	inline VuforiaManager_t1653423889 ** get_address_of_sInstance_1() { return &___sInstance_1; }
	inline void set_sInstance_1(VuforiaManager_t1653423889 * value)
	{
		___sInstance_1 = value;
		Il2CppCodeGenWriteBarrier((&___sInstance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VUFORIAMANAGER_T1653423889_H
#ifndef VUFORIARENDERER_T3433045970_H
#define VUFORIARENDERER_T3433045970_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaRenderer
struct  VuforiaRenderer_t3433045970  : public RuntimeObject
{
public:
	// Vuforia.VuforiaRenderer/VideoBGCfgData Vuforia.VuforiaRenderer::mVideoBGConfig
	VideoBGCfgData_t994527297  ___mVideoBGConfig_1;
	// System.Boolean Vuforia.VuforiaRenderer::mVideoBGConfigSet
	bool ___mVideoBGConfigSet_2;
	// UnityEngine.Texture Vuforia.VuforiaRenderer::mVideoBackgroundTexture
	Texture_t3661962703 * ___mVideoBackgroundTexture_3;
	// System.Boolean Vuforia.VuforiaRenderer::mBackgroundTextureHasChanged
	bool ___mBackgroundTextureHasChanged_4;
	// Vuforia.VuforiaRenderer/VideoBackgroundReflection Vuforia.VuforiaRenderer::mLastSetReflection
	int32_t ___mLastSetReflection_5;
	// System.IntPtr Vuforia.VuforiaRenderer::mNativeRenderingCallback
	intptr_t ___mNativeRenderingCallback_6;

public:
	inline static int32_t get_offset_of_mVideoBGConfig_1() { return static_cast<int32_t>(offsetof(VuforiaRenderer_t3433045970, ___mVideoBGConfig_1)); }
	inline VideoBGCfgData_t994527297  get_mVideoBGConfig_1() const { return ___mVideoBGConfig_1; }
	inline VideoBGCfgData_t994527297 * get_address_of_mVideoBGConfig_1() { return &___mVideoBGConfig_1; }
	inline void set_mVideoBGConfig_1(VideoBGCfgData_t994527297  value)
	{
		___mVideoBGConfig_1 = value;
	}

	inline static int32_t get_offset_of_mVideoBGConfigSet_2() { return static_cast<int32_t>(offsetof(VuforiaRenderer_t3433045970, ___mVideoBGConfigSet_2)); }
	inline bool get_mVideoBGConfigSet_2() const { return ___mVideoBGConfigSet_2; }
	inline bool* get_address_of_mVideoBGConfigSet_2() { return &___mVideoBGConfigSet_2; }
	inline void set_mVideoBGConfigSet_2(bool value)
	{
		___mVideoBGConfigSet_2 = value;
	}

	inline static int32_t get_offset_of_mVideoBackgroundTexture_3() { return static_cast<int32_t>(offsetof(VuforiaRenderer_t3433045970, ___mVideoBackgroundTexture_3)); }
	inline Texture_t3661962703 * get_mVideoBackgroundTexture_3() const { return ___mVideoBackgroundTexture_3; }
	inline Texture_t3661962703 ** get_address_of_mVideoBackgroundTexture_3() { return &___mVideoBackgroundTexture_3; }
	inline void set_mVideoBackgroundTexture_3(Texture_t3661962703 * value)
	{
		___mVideoBackgroundTexture_3 = value;
		Il2CppCodeGenWriteBarrier((&___mVideoBackgroundTexture_3), value);
	}

	inline static int32_t get_offset_of_mBackgroundTextureHasChanged_4() { return static_cast<int32_t>(offsetof(VuforiaRenderer_t3433045970, ___mBackgroundTextureHasChanged_4)); }
	inline bool get_mBackgroundTextureHasChanged_4() const { return ___mBackgroundTextureHasChanged_4; }
	inline bool* get_address_of_mBackgroundTextureHasChanged_4() { return &___mBackgroundTextureHasChanged_4; }
	inline void set_mBackgroundTextureHasChanged_4(bool value)
	{
		___mBackgroundTextureHasChanged_4 = value;
	}

	inline static int32_t get_offset_of_mLastSetReflection_5() { return static_cast<int32_t>(offsetof(VuforiaRenderer_t3433045970, ___mLastSetReflection_5)); }
	inline int32_t get_mLastSetReflection_5() const { return ___mLastSetReflection_5; }
	inline int32_t* get_address_of_mLastSetReflection_5() { return &___mLastSetReflection_5; }
	inline void set_mLastSetReflection_5(int32_t value)
	{
		___mLastSetReflection_5 = value;
	}

	inline static int32_t get_offset_of_mNativeRenderingCallback_6() { return static_cast<int32_t>(offsetof(VuforiaRenderer_t3433045970, ___mNativeRenderingCallback_6)); }
	inline intptr_t get_mNativeRenderingCallback_6() const { return ___mNativeRenderingCallback_6; }
	inline intptr_t* get_address_of_mNativeRenderingCallback_6() { return &___mNativeRenderingCallback_6; }
	inline void set_mNativeRenderingCallback_6(intptr_t value)
	{
		___mNativeRenderingCallback_6 = value;
	}
};

struct VuforiaRenderer_t3433045970_StaticFields
{
public:
	// Vuforia.VuforiaRenderer Vuforia.VuforiaRenderer::sInstance
	VuforiaRenderer_t3433045970 * ___sInstance_0;

public:
	inline static int32_t get_offset_of_sInstance_0() { return static_cast<int32_t>(offsetof(VuforiaRenderer_t3433045970_StaticFields, ___sInstance_0)); }
	inline VuforiaRenderer_t3433045970 * get_sInstance_0() const { return ___sInstance_0; }
	inline VuforiaRenderer_t3433045970 ** get_address_of_sInstance_0() { return &___sInstance_0; }
	inline void set_sInstance_0(VuforiaRenderer_t3433045970 * value)
	{
		___sInstance_0 = value;
		Il2CppCodeGenWriteBarrier((&___sInstance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VUFORIARENDERER_T3433045970_H
#ifndef VUFORIARUNTIMEUTILITIES_T399660591_H
#define VUFORIARUNTIMEUTILITIES_T399660591_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaRuntimeUtilities
struct  VuforiaRuntimeUtilities_t399660591  : public RuntimeObject
{
public:

public:
};

struct VuforiaRuntimeUtilities_t399660591_StaticFields
{
public:
	// Vuforia.VuforiaRuntimeUtilities/InitializableBool Vuforia.VuforiaRuntimeUtilities::sWebCamUsed
	int32_t ___sWebCamUsed_0;
	// Vuforia.VuforiaRuntimeUtilities/InitializableBool Vuforia.VuforiaRuntimeUtilities::sNativePluginSupport
	int32_t ___sNativePluginSupport_1;

public:
	inline static int32_t get_offset_of_sWebCamUsed_0() { return static_cast<int32_t>(offsetof(VuforiaRuntimeUtilities_t399660591_StaticFields, ___sWebCamUsed_0)); }
	inline int32_t get_sWebCamUsed_0() const { return ___sWebCamUsed_0; }
	inline int32_t* get_address_of_sWebCamUsed_0() { return &___sWebCamUsed_0; }
	inline void set_sWebCamUsed_0(int32_t value)
	{
		___sWebCamUsed_0 = value;
	}

	inline static int32_t get_offset_of_sNativePluginSupport_1() { return static_cast<int32_t>(offsetof(VuforiaRuntimeUtilities_t399660591_StaticFields, ___sNativePluginSupport_1)); }
	inline int32_t get_sNativePluginSupport_1() const { return ___sNativePluginSupport_1; }
	inline int32_t* get_address_of_sNativePluginSupport_1() { return &___sNativePluginSupport_1; }
	inline void set_sNativePluginSupport_1(int32_t value)
	{
		___sNativePluginSupport_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VUFORIARUNTIMEUTILITIES_T399660591_H
#ifndef PROFILECOLLECTION_T901995765_H
#define PROFILECOLLECTION_T901995765_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.WebCamProfile/ProfileCollection
struct  ProfileCollection_t901995765 
{
public:
	// Vuforia.WebCamProfile/ProfileData Vuforia.WebCamProfile/ProfileCollection::DefaultProfile
	ProfileData_t3519391925  ___DefaultProfile_0;
	// System.Collections.Generic.Dictionary`2<System.String,Vuforia.WebCamProfile/ProfileData> Vuforia.WebCamProfile/ProfileCollection::Profiles
	Dictionary_2_t3304648224 * ___Profiles_1;

public:
	inline static int32_t get_offset_of_DefaultProfile_0() { return static_cast<int32_t>(offsetof(ProfileCollection_t901995765, ___DefaultProfile_0)); }
	inline ProfileData_t3519391925  get_DefaultProfile_0() const { return ___DefaultProfile_0; }
	inline ProfileData_t3519391925 * get_address_of_DefaultProfile_0() { return &___DefaultProfile_0; }
	inline void set_DefaultProfile_0(ProfileData_t3519391925  value)
	{
		___DefaultProfile_0 = value;
	}

	inline static int32_t get_offset_of_Profiles_1() { return static_cast<int32_t>(offsetof(ProfileCollection_t901995765, ___Profiles_1)); }
	inline Dictionary_2_t3304648224 * get_Profiles_1() const { return ___Profiles_1; }
	inline Dictionary_2_t3304648224 ** get_address_of_Profiles_1() { return &___Profiles_1; }
	inline void set_Profiles_1(Dictionary_2_t3304648224 * value)
	{
		___Profiles_1 = value;
		Il2CppCodeGenWriteBarrier((&___Profiles_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Vuforia.WebCamProfile/ProfileCollection
struct ProfileCollection_t901995765_marshaled_pinvoke
{
	ProfileData_t3519391925  ___DefaultProfile_0;
	Dictionary_2_t3304648224 * ___Profiles_1;
};
// Native definition for COM marshalling of Vuforia.WebCamProfile/ProfileCollection
struct ProfileCollection_t901995765_marshaled_com
{
	ProfileData_t3519391925  ___DefaultProfile_0;
	Dictionary_2_t3304648224 * ___Profiles_1;
};
#endif // PROFILECOLLECTION_T901995765_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef ABOUTMANAGER_T2961629990_H
#define ABOUTMANAGER_T2961629990_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AboutManager
struct  AboutManager_t2961629990  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Text AboutManager::aboutText
	Text_t1901882714 * ___aboutText_4;

public:
	inline static int32_t get_offset_of_aboutText_4() { return static_cast<int32_t>(offsetof(AboutManager_t2961629990, ___aboutText_4)); }
	inline Text_t1901882714 * get_aboutText_4() const { return ___aboutText_4; }
	inline Text_t1901882714 ** get_address_of_aboutText_4() { return &___aboutText_4; }
	inline void set_aboutText_4(Text_t1901882714 * value)
	{
		___aboutText_4 = value;
		Il2CppCodeGenWriteBarrier((&___aboutText_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABOUTMANAGER_T2961629990_H
#ifndef ABOUTSCREEN_T2183797299_H
#define ABOUTSCREEN_T2183797299_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AboutScreen
struct  AboutScreen_t2183797299  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABOUTSCREEN_T2183797299_H
#ifndef ACTIONCONTENT_T2142700484_H
#define ACTIONCONTENT_T2142700484_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ActionContent
struct  ActionContent_t2142700484  : public MonoBehaviour_t3962482529
{
public:
	// System.String ActionContent::urlImage
	String_t* ___urlImage_4;
	// UnityEngine.GameObject ActionContent::plane
	GameObject_t1113636619 * ___plane_5;
	// UnityEngine.RaycastHit ActionContent::hit
	RaycastHit_t1056001966  ___hit_6;
	// UnityEngine.Ray ActionContent::ray
	Ray_t3785851493  ___ray_7;
	// UnityEngine.RaycastHit[] ActionContent::hitAllTemp
	RaycastHitU5BU5D_t1690781147* ___hitAllTemp_8;
	// UnityEngine.Collider ActionContent::collider
	Collider_t1773347010 * ___collider_9;
	// System.String ActionContent::action
	String_t* ___action_10;

public:
	inline static int32_t get_offset_of_urlImage_4() { return static_cast<int32_t>(offsetof(ActionContent_t2142700484, ___urlImage_4)); }
	inline String_t* get_urlImage_4() const { return ___urlImage_4; }
	inline String_t** get_address_of_urlImage_4() { return &___urlImage_4; }
	inline void set_urlImage_4(String_t* value)
	{
		___urlImage_4 = value;
		Il2CppCodeGenWriteBarrier((&___urlImage_4), value);
	}

	inline static int32_t get_offset_of_plane_5() { return static_cast<int32_t>(offsetof(ActionContent_t2142700484, ___plane_5)); }
	inline GameObject_t1113636619 * get_plane_5() const { return ___plane_5; }
	inline GameObject_t1113636619 ** get_address_of_plane_5() { return &___plane_5; }
	inline void set_plane_5(GameObject_t1113636619 * value)
	{
		___plane_5 = value;
		Il2CppCodeGenWriteBarrier((&___plane_5), value);
	}

	inline static int32_t get_offset_of_hit_6() { return static_cast<int32_t>(offsetof(ActionContent_t2142700484, ___hit_6)); }
	inline RaycastHit_t1056001966  get_hit_6() const { return ___hit_6; }
	inline RaycastHit_t1056001966 * get_address_of_hit_6() { return &___hit_6; }
	inline void set_hit_6(RaycastHit_t1056001966  value)
	{
		___hit_6 = value;
	}

	inline static int32_t get_offset_of_ray_7() { return static_cast<int32_t>(offsetof(ActionContent_t2142700484, ___ray_7)); }
	inline Ray_t3785851493  get_ray_7() const { return ___ray_7; }
	inline Ray_t3785851493 * get_address_of_ray_7() { return &___ray_7; }
	inline void set_ray_7(Ray_t3785851493  value)
	{
		___ray_7 = value;
	}

	inline static int32_t get_offset_of_hitAllTemp_8() { return static_cast<int32_t>(offsetof(ActionContent_t2142700484, ___hitAllTemp_8)); }
	inline RaycastHitU5BU5D_t1690781147* get_hitAllTemp_8() const { return ___hitAllTemp_8; }
	inline RaycastHitU5BU5D_t1690781147** get_address_of_hitAllTemp_8() { return &___hitAllTemp_8; }
	inline void set_hitAllTemp_8(RaycastHitU5BU5D_t1690781147* value)
	{
		___hitAllTemp_8 = value;
		Il2CppCodeGenWriteBarrier((&___hitAllTemp_8), value);
	}

	inline static int32_t get_offset_of_collider_9() { return static_cast<int32_t>(offsetof(ActionContent_t2142700484, ___collider_9)); }
	inline Collider_t1773347010 * get_collider_9() const { return ___collider_9; }
	inline Collider_t1773347010 ** get_address_of_collider_9() { return &___collider_9; }
	inline void set_collider_9(Collider_t1773347010 * value)
	{
		___collider_9 = value;
		Il2CppCodeGenWriteBarrier((&___collider_9), value);
	}

	inline static int32_t get_offset_of_action_10() { return static_cast<int32_t>(offsetof(ActionContent_t2142700484, ___action_10)); }
	inline String_t* get_action_10() const { return ___action_10; }
	inline String_t** get_address_of_action_10() { return &___action_10; }
	inline void set_action_10(String_t* value)
	{
		___action_10 = value;
		Il2CppCodeGenWriteBarrier((&___action_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTIONCONTENT_T2142700484_H
#ifndef ANIMATIONSMANAGER_T2281084567_H
#define ANIMATIONSMANAGER_T2281084567_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AnimationsManager
struct  AnimationsManager_t2281084567  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject AnimationsManager::OverlayPosition
	GameObject_t1113636619 * ___OverlayPosition_4;
	// UnityEngine.UI.Slider AnimationsManager::sliderZoom
	Slider_t3903728902 * ___sliderZoom_5;
	// ContentManager2 AnimationsManager::contentManager
	ContentManager2_t2980555998 * ___contentManager_6;
	// UnityEngine.Vector3 AnimationsManager::savedPositionCamera
	Vector3_t3722313464  ___savedPositionCamera_7;
	// UnityEngine.Vector3 AnimationsManager::savedPositionOverlay
	Vector3_t3722313464  ___savedPositionOverlay_8;
	// UnityEngine.Vector3 AnimationsManager::savedUpVector
	Vector3_t3722313464  ___savedUpVector_9;
	// UnityEngine.GameObject AnimationsManager::mAugmentationObject
	GameObject_t1113636619 * ___mAugmentationObject_10;
	// System.Boolean AnimationsManager::mIsTracking
	bool ___mIsTracking_11;
	// System.Boolean AnimationsManager::mDoAnimationTo2D
	bool ___mDoAnimationTo2D_12;
	// System.Boolean AnimationsManager::mDoAnimationTo3D
	bool ___mDoAnimationTo3D_13;
	// System.Boolean AnimationsManager::mIsShowingOverlay
	bool ___mIsShowingOverlay_14;

public:
	inline static int32_t get_offset_of_OverlayPosition_4() { return static_cast<int32_t>(offsetof(AnimationsManager_t2281084567, ___OverlayPosition_4)); }
	inline GameObject_t1113636619 * get_OverlayPosition_4() const { return ___OverlayPosition_4; }
	inline GameObject_t1113636619 ** get_address_of_OverlayPosition_4() { return &___OverlayPosition_4; }
	inline void set_OverlayPosition_4(GameObject_t1113636619 * value)
	{
		___OverlayPosition_4 = value;
		Il2CppCodeGenWriteBarrier((&___OverlayPosition_4), value);
	}

	inline static int32_t get_offset_of_sliderZoom_5() { return static_cast<int32_t>(offsetof(AnimationsManager_t2281084567, ___sliderZoom_5)); }
	inline Slider_t3903728902 * get_sliderZoom_5() const { return ___sliderZoom_5; }
	inline Slider_t3903728902 ** get_address_of_sliderZoom_5() { return &___sliderZoom_5; }
	inline void set_sliderZoom_5(Slider_t3903728902 * value)
	{
		___sliderZoom_5 = value;
		Il2CppCodeGenWriteBarrier((&___sliderZoom_5), value);
	}

	inline static int32_t get_offset_of_contentManager_6() { return static_cast<int32_t>(offsetof(AnimationsManager_t2281084567, ___contentManager_6)); }
	inline ContentManager2_t2980555998 * get_contentManager_6() const { return ___contentManager_6; }
	inline ContentManager2_t2980555998 ** get_address_of_contentManager_6() { return &___contentManager_6; }
	inline void set_contentManager_6(ContentManager2_t2980555998 * value)
	{
		___contentManager_6 = value;
		Il2CppCodeGenWriteBarrier((&___contentManager_6), value);
	}

	inline static int32_t get_offset_of_savedPositionCamera_7() { return static_cast<int32_t>(offsetof(AnimationsManager_t2281084567, ___savedPositionCamera_7)); }
	inline Vector3_t3722313464  get_savedPositionCamera_7() const { return ___savedPositionCamera_7; }
	inline Vector3_t3722313464 * get_address_of_savedPositionCamera_7() { return &___savedPositionCamera_7; }
	inline void set_savedPositionCamera_7(Vector3_t3722313464  value)
	{
		___savedPositionCamera_7 = value;
	}

	inline static int32_t get_offset_of_savedPositionOverlay_8() { return static_cast<int32_t>(offsetof(AnimationsManager_t2281084567, ___savedPositionOverlay_8)); }
	inline Vector3_t3722313464  get_savedPositionOverlay_8() const { return ___savedPositionOverlay_8; }
	inline Vector3_t3722313464 * get_address_of_savedPositionOverlay_8() { return &___savedPositionOverlay_8; }
	inline void set_savedPositionOverlay_8(Vector3_t3722313464  value)
	{
		___savedPositionOverlay_8 = value;
	}

	inline static int32_t get_offset_of_savedUpVector_9() { return static_cast<int32_t>(offsetof(AnimationsManager_t2281084567, ___savedUpVector_9)); }
	inline Vector3_t3722313464  get_savedUpVector_9() const { return ___savedUpVector_9; }
	inline Vector3_t3722313464 * get_address_of_savedUpVector_9() { return &___savedUpVector_9; }
	inline void set_savedUpVector_9(Vector3_t3722313464  value)
	{
		___savedUpVector_9 = value;
	}

	inline static int32_t get_offset_of_mAugmentationObject_10() { return static_cast<int32_t>(offsetof(AnimationsManager_t2281084567, ___mAugmentationObject_10)); }
	inline GameObject_t1113636619 * get_mAugmentationObject_10() const { return ___mAugmentationObject_10; }
	inline GameObject_t1113636619 ** get_address_of_mAugmentationObject_10() { return &___mAugmentationObject_10; }
	inline void set_mAugmentationObject_10(GameObject_t1113636619 * value)
	{
		___mAugmentationObject_10 = value;
		Il2CppCodeGenWriteBarrier((&___mAugmentationObject_10), value);
	}

	inline static int32_t get_offset_of_mIsTracking_11() { return static_cast<int32_t>(offsetof(AnimationsManager_t2281084567, ___mIsTracking_11)); }
	inline bool get_mIsTracking_11() const { return ___mIsTracking_11; }
	inline bool* get_address_of_mIsTracking_11() { return &___mIsTracking_11; }
	inline void set_mIsTracking_11(bool value)
	{
		___mIsTracking_11 = value;
	}

	inline static int32_t get_offset_of_mDoAnimationTo2D_12() { return static_cast<int32_t>(offsetof(AnimationsManager_t2281084567, ___mDoAnimationTo2D_12)); }
	inline bool get_mDoAnimationTo2D_12() const { return ___mDoAnimationTo2D_12; }
	inline bool* get_address_of_mDoAnimationTo2D_12() { return &___mDoAnimationTo2D_12; }
	inline void set_mDoAnimationTo2D_12(bool value)
	{
		___mDoAnimationTo2D_12 = value;
	}

	inline static int32_t get_offset_of_mDoAnimationTo3D_13() { return static_cast<int32_t>(offsetof(AnimationsManager_t2281084567, ___mDoAnimationTo3D_13)); }
	inline bool get_mDoAnimationTo3D_13() const { return ___mDoAnimationTo3D_13; }
	inline bool* get_address_of_mDoAnimationTo3D_13() { return &___mDoAnimationTo3D_13; }
	inline void set_mDoAnimationTo3D_13(bool value)
	{
		___mDoAnimationTo3D_13 = value;
	}

	inline static int32_t get_offset_of_mIsShowingOverlay_14() { return static_cast<int32_t>(offsetof(AnimationsManager_t2281084567, ___mIsShowingOverlay_14)); }
	inline bool get_mIsShowingOverlay_14() const { return ___mIsShowingOverlay_14; }
	inline bool* get_address_of_mIsShowingOverlay_14() { return &___mIsShowingOverlay_14; }
	inline void set_mIsShowingOverlay_14(bool value)
	{
		___mIsShowingOverlay_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATIONSMANAGER_T2281084567_H
#ifndef ASYNCSCENELOADER_T621267272_H
#define ASYNCSCENELOADER_T621267272_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AsyncSceneLoader
struct  AsyncSceneLoader_t621267272  : public MonoBehaviour_t3962482529
{
public:
	// System.Single AsyncSceneLoader::loadingDelay
	float ___loadingDelay_4;

public:
	inline static int32_t get_offset_of_loadingDelay_4() { return static_cast<int32_t>(offsetof(AsyncSceneLoader_t621267272, ___loadingDelay_4)); }
	inline float get_loadingDelay_4() const { return ___loadingDelay_4; }
	inline float* get_address_of_loadingDelay_4() { return &___loadingDelay_4; }
	inline void set_loadingDelay_4(float value)
	{
		___loadingDelay_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYNCSCENELOADER_T621267272_H
#ifndef BASEMARKERAPP_T2308075069_H
#define BASEMARKERAPP_T2308075069_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BaseMarkerApp
struct  BaseMarkerApp_t2308075069  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean BaseMarkerApp::isMarkedForDistroy
	bool ___isMarkedForDistroy_4;
	// System.String BaseMarkerApp::AppID
	String_t* ___AppID_5;

public:
	inline static int32_t get_offset_of_isMarkedForDistroy_4() { return static_cast<int32_t>(offsetof(BaseMarkerApp_t2308075069, ___isMarkedForDistroy_4)); }
	inline bool get_isMarkedForDistroy_4() const { return ___isMarkedForDistroy_4; }
	inline bool* get_address_of_isMarkedForDistroy_4() { return &___isMarkedForDistroy_4; }
	inline void set_isMarkedForDistroy_4(bool value)
	{
		___isMarkedForDistroy_4 = value;
	}

	inline static int32_t get_offset_of_AppID_5() { return static_cast<int32_t>(offsetof(BaseMarkerApp_t2308075069, ___AppID_5)); }
	inline String_t* get_AppID_5() const { return ___AppID_5; }
	inline String_t** get_address_of_AppID_5() { return &___AppID_5; }
	inline void set_AppID_5(String_t* value)
	{
		___AppID_5 = value;
		Il2CppCodeGenWriteBarrier((&___AppID_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEMARKERAPP_T2308075069_H
#ifndef BASICS_T653963782_H
#define BASICS_T653963782_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Basics
struct  Basics_t653963782  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Transform Basics::cubeA
	Transform_t3600365921 * ___cubeA_4;
	// UnityEngine.Transform Basics::cubeB
	Transform_t3600365921 * ___cubeB_5;

public:
	inline static int32_t get_offset_of_cubeA_4() { return static_cast<int32_t>(offsetof(Basics_t653963782, ___cubeA_4)); }
	inline Transform_t3600365921 * get_cubeA_4() const { return ___cubeA_4; }
	inline Transform_t3600365921 ** get_address_of_cubeA_4() { return &___cubeA_4; }
	inline void set_cubeA_4(Transform_t3600365921 * value)
	{
		___cubeA_4 = value;
		Il2CppCodeGenWriteBarrier((&___cubeA_4), value);
	}

	inline static int32_t get_offset_of_cubeB_5() { return static_cast<int32_t>(offsetof(Basics_t653963782, ___cubeB_5)); }
	inline Transform_t3600365921 * get_cubeB_5() const { return ___cubeB_5; }
	inline Transform_t3600365921 ** get_address_of_cubeB_5() { return &___cubeB_5; }
	inline void set_cubeB_5(Transform_t3600365921 * value)
	{
		___cubeB_5 = value;
		Il2CppCodeGenWriteBarrier((&___cubeB_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASICS_T653963782_H
#ifndef CAMERASETTINGS_T3152619780_H
#define CAMERASETTINGS_T3152619780_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraSettings
struct  CameraSettings_t3152619780  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean CameraSettings::mVuforiaStarted
	bool ___mVuforiaStarted_4;
	// System.Boolean CameraSettings::mAutofocusEnabled
	bool ___mAutofocusEnabled_5;
	// System.Boolean CameraSettings::mFlashTorchEnabled
	bool ___mFlashTorchEnabled_6;
	// Vuforia.CameraDevice/CameraDirection CameraSettings::mActiveDirection
	int32_t ___mActiveDirection_7;

public:
	inline static int32_t get_offset_of_mVuforiaStarted_4() { return static_cast<int32_t>(offsetof(CameraSettings_t3152619780, ___mVuforiaStarted_4)); }
	inline bool get_mVuforiaStarted_4() const { return ___mVuforiaStarted_4; }
	inline bool* get_address_of_mVuforiaStarted_4() { return &___mVuforiaStarted_4; }
	inline void set_mVuforiaStarted_4(bool value)
	{
		___mVuforiaStarted_4 = value;
	}

	inline static int32_t get_offset_of_mAutofocusEnabled_5() { return static_cast<int32_t>(offsetof(CameraSettings_t3152619780, ___mAutofocusEnabled_5)); }
	inline bool get_mAutofocusEnabled_5() const { return ___mAutofocusEnabled_5; }
	inline bool* get_address_of_mAutofocusEnabled_5() { return &___mAutofocusEnabled_5; }
	inline void set_mAutofocusEnabled_5(bool value)
	{
		___mAutofocusEnabled_5 = value;
	}

	inline static int32_t get_offset_of_mFlashTorchEnabled_6() { return static_cast<int32_t>(offsetof(CameraSettings_t3152619780, ___mFlashTorchEnabled_6)); }
	inline bool get_mFlashTorchEnabled_6() const { return ___mFlashTorchEnabled_6; }
	inline bool* get_address_of_mFlashTorchEnabled_6() { return &___mFlashTorchEnabled_6; }
	inline void set_mFlashTorchEnabled_6(bool value)
	{
		___mFlashTorchEnabled_6 = value;
	}

	inline static int32_t get_offset_of_mActiveDirection_7() { return static_cast<int32_t>(offsetof(CameraSettings_t3152619780, ___mActiveDirection_7)); }
	inline int32_t get_mActiveDirection_7() const { return ___mActiveDirection_7; }
	inline int32_t* get_address_of_mActiveDirection_7() { return &___mActiveDirection_7; }
	inline void set_mActiveDirection_7(int32_t value)
	{
		___mActiveDirection_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERASETTINGS_T3152619780_H
#ifndef CLOUDRECOEVENTHANDLER_T613522768_H
#define CLOUDRECOEVENTHANDLER_T613522768_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CloudRecoEventHandler
struct  CloudRecoEventHandler_t613522768  : public MonoBehaviour_t3962482529
{
public:
	// Vuforia.CloudRecoBehaviour CloudRecoEventHandler::mCloudRecoBehaviour
	CloudRecoBehaviour_t431762792 * ___mCloudRecoBehaviour_4;
	// Vuforia.ObjectTracker CloudRecoEventHandler::mObjectTracker
	ObjectTracker_t4177997237 * ___mObjectTracker_5;
	// ContentManager2 CloudRecoEventHandler::mContentManager
	ContentManager2_t2980555998 * ___mContentManager_6;
	// UnityEngine.GameObject CloudRecoEventHandler::mParentOfImageTargetTemplate
	GameObject_t1113636619 * ___mParentOfImageTargetTemplate_7;
	// System.Boolean CloudRecoEventHandler::mMustRestartApp
	bool ___mMustRestartApp_8;
	// System.String CloudRecoEventHandler::errorTitle
	String_t* ___errorTitle_9;
	// System.String CloudRecoEventHandler::errorMsg
	String_t* ___errorMsg_10;
	// Vuforia.ImageTargetBehaviour CloudRecoEventHandler::ImageTargetTemplate
	ImageTargetBehaviour_t2200418350 * ___ImageTargetTemplate_11;
	// ScanLine CloudRecoEventHandler::scanLine
	ScanLine_t269422218 * ___scanLine_12;
	// System.Boolean CloudRecoEventHandler::isTracking
	bool ___isTracking_13;

public:
	inline static int32_t get_offset_of_mCloudRecoBehaviour_4() { return static_cast<int32_t>(offsetof(CloudRecoEventHandler_t613522768, ___mCloudRecoBehaviour_4)); }
	inline CloudRecoBehaviour_t431762792 * get_mCloudRecoBehaviour_4() const { return ___mCloudRecoBehaviour_4; }
	inline CloudRecoBehaviour_t431762792 ** get_address_of_mCloudRecoBehaviour_4() { return &___mCloudRecoBehaviour_4; }
	inline void set_mCloudRecoBehaviour_4(CloudRecoBehaviour_t431762792 * value)
	{
		___mCloudRecoBehaviour_4 = value;
		Il2CppCodeGenWriteBarrier((&___mCloudRecoBehaviour_4), value);
	}

	inline static int32_t get_offset_of_mObjectTracker_5() { return static_cast<int32_t>(offsetof(CloudRecoEventHandler_t613522768, ___mObjectTracker_5)); }
	inline ObjectTracker_t4177997237 * get_mObjectTracker_5() const { return ___mObjectTracker_5; }
	inline ObjectTracker_t4177997237 ** get_address_of_mObjectTracker_5() { return &___mObjectTracker_5; }
	inline void set_mObjectTracker_5(ObjectTracker_t4177997237 * value)
	{
		___mObjectTracker_5 = value;
		Il2CppCodeGenWriteBarrier((&___mObjectTracker_5), value);
	}

	inline static int32_t get_offset_of_mContentManager_6() { return static_cast<int32_t>(offsetof(CloudRecoEventHandler_t613522768, ___mContentManager_6)); }
	inline ContentManager2_t2980555998 * get_mContentManager_6() const { return ___mContentManager_6; }
	inline ContentManager2_t2980555998 ** get_address_of_mContentManager_6() { return &___mContentManager_6; }
	inline void set_mContentManager_6(ContentManager2_t2980555998 * value)
	{
		___mContentManager_6 = value;
		Il2CppCodeGenWriteBarrier((&___mContentManager_6), value);
	}

	inline static int32_t get_offset_of_mParentOfImageTargetTemplate_7() { return static_cast<int32_t>(offsetof(CloudRecoEventHandler_t613522768, ___mParentOfImageTargetTemplate_7)); }
	inline GameObject_t1113636619 * get_mParentOfImageTargetTemplate_7() const { return ___mParentOfImageTargetTemplate_7; }
	inline GameObject_t1113636619 ** get_address_of_mParentOfImageTargetTemplate_7() { return &___mParentOfImageTargetTemplate_7; }
	inline void set_mParentOfImageTargetTemplate_7(GameObject_t1113636619 * value)
	{
		___mParentOfImageTargetTemplate_7 = value;
		Il2CppCodeGenWriteBarrier((&___mParentOfImageTargetTemplate_7), value);
	}

	inline static int32_t get_offset_of_mMustRestartApp_8() { return static_cast<int32_t>(offsetof(CloudRecoEventHandler_t613522768, ___mMustRestartApp_8)); }
	inline bool get_mMustRestartApp_8() const { return ___mMustRestartApp_8; }
	inline bool* get_address_of_mMustRestartApp_8() { return &___mMustRestartApp_8; }
	inline void set_mMustRestartApp_8(bool value)
	{
		___mMustRestartApp_8 = value;
	}

	inline static int32_t get_offset_of_errorTitle_9() { return static_cast<int32_t>(offsetof(CloudRecoEventHandler_t613522768, ___errorTitle_9)); }
	inline String_t* get_errorTitle_9() const { return ___errorTitle_9; }
	inline String_t** get_address_of_errorTitle_9() { return &___errorTitle_9; }
	inline void set_errorTitle_9(String_t* value)
	{
		___errorTitle_9 = value;
		Il2CppCodeGenWriteBarrier((&___errorTitle_9), value);
	}

	inline static int32_t get_offset_of_errorMsg_10() { return static_cast<int32_t>(offsetof(CloudRecoEventHandler_t613522768, ___errorMsg_10)); }
	inline String_t* get_errorMsg_10() const { return ___errorMsg_10; }
	inline String_t** get_address_of_errorMsg_10() { return &___errorMsg_10; }
	inline void set_errorMsg_10(String_t* value)
	{
		___errorMsg_10 = value;
		Il2CppCodeGenWriteBarrier((&___errorMsg_10), value);
	}

	inline static int32_t get_offset_of_ImageTargetTemplate_11() { return static_cast<int32_t>(offsetof(CloudRecoEventHandler_t613522768, ___ImageTargetTemplate_11)); }
	inline ImageTargetBehaviour_t2200418350 * get_ImageTargetTemplate_11() const { return ___ImageTargetTemplate_11; }
	inline ImageTargetBehaviour_t2200418350 ** get_address_of_ImageTargetTemplate_11() { return &___ImageTargetTemplate_11; }
	inline void set_ImageTargetTemplate_11(ImageTargetBehaviour_t2200418350 * value)
	{
		___ImageTargetTemplate_11 = value;
		Il2CppCodeGenWriteBarrier((&___ImageTargetTemplate_11), value);
	}

	inline static int32_t get_offset_of_scanLine_12() { return static_cast<int32_t>(offsetof(CloudRecoEventHandler_t613522768, ___scanLine_12)); }
	inline ScanLine_t269422218 * get_scanLine_12() const { return ___scanLine_12; }
	inline ScanLine_t269422218 ** get_address_of_scanLine_12() { return &___scanLine_12; }
	inline void set_scanLine_12(ScanLine_t269422218 * value)
	{
		___scanLine_12 = value;
		Il2CppCodeGenWriteBarrier((&___scanLine_12), value);
	}

	inline static int32_t get_offset_of_isTracking_13() { return static_cast<int32_t>(offsetof(CloudRecoEventHandler_t613522768, ___isTracking_13)); }
	inline bool get_isTracking_13() const { return ___isTracking_13; }
	inline bool* get_address_of_isTracking_13() { return &___isTracking_13; }
	inline void set_isTracking_13(bool value)
	{
		___isTracking_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLOUDRECOEVENTHANDLER_T613522768_H
#ifndef CONTENTMANAGER_T211916338_H
#define CONTENTMANAGER_T211916338_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ContentManager
struct  ContentManager_t211916338  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Image ContentManager::LoadingSpinnerBackground
	Image_t2670269651 * ___LoadingSpinnerBackground_4;
	// UnityEngine.UI.Image ContentManager::LoadingSpinnerImage
	Image_t2670269651 * ___LoadingSpinnerImage_5;
	// UnityEngine.UI.Button ContentManager::CancelButton
	Button_t4055032469 * ___CancelButton_6;
	// UnityEngine.GameObject ContentManager::AugmentationObject
	GameObject_t1113636619 * ___AugmentationObject_7;
	// AnimationsManager ContentManager::AnimationsManager
	AnimationsManager_t2281084567 * ___AnimationsManager_8;
	// System.String ContentManager::JsonServerUrl
	String_t* ___JsonServerUrl_9;
	// System.Boolean ContentManager::mIsShowingBookData
	bool ___mIsShowingBookData_10;
	// System.Boolean ContentManager::mIsLoadingBookData
	bool ___mIsLoadingBookData_11;
	// System.Boolean ContentManager::mIsLoadingBookThumb
	bool ___mIsLoadingBookThumb_12;
	// UnityEngine.WWW ContentManager::mJsonBookInfo
	WWW_t3688466362 * ___mJsonBookInfo_13;
	// UnityEngine.WWW ContentManager::mBookThumb
	WWW_t3688466362 * ___mBookThumb_14;
	// BookData ContentManager::mBookData
	BookData_t1076903227 * ___mBookData_15;
	// System.Boolean ContentManager::mIsBookThumbRequested
	bool ___mIsBookThumbRequested_16;
	// BookInformationParser ContentManager::mBookInformationParser
	BookInformationParser_t151156397 * ___mBookInformationParser_17;
	// System.Boolean ContentManager::mIsShowingMenu
	bool ___mIsShowingMenu_18;
	// Vuforia.CloudRecoBehaviour ContentManager::mCloudRecoBehaviour
	CloudRecoBehaviour_t431762792 * ___mCloudRecoBehaviour_19;
	// Vuforia.TrackableBehaviour ContentManager::mTrackableBehaviour
	TrackableBehaviour_t1113559212 * ___mTrackableBehaviour_20;

public:
	inline static int32_t get_offset_of_LoadingSpinnerBackground_4() { return static_cast<int32_t>(offsetof(ContentManager_t211916338, ___LoadingSpinnerBackground_4)); }
	inline Image_t2670269651 * get_LoadingSpinnerBackground_4() const { return ___LoadingSpinnerBackground_4; }
	inline Image_t2670269651 ** get_address_of_LoadingSpinnerBackground_4() { return &___LoadingSpinnerBackground_4; }
	inline void set_LoadingSpinnerBackground_4(Image_t2670269651 * value)
	{
		___LoadingSpinnerBackground_4 = value;
		Il2CppCodeGenWriteBarrier((&___LoadingSpinnerBackground_4), value);
	}

	inline static int32_t get_offset_of_LoadingSpinnerImage_5() { return static_cast<int32_t>(offsetof(ContentManager_t211916338, ___LoadingSpinnerImage_5)); }
	inline Image_t2670269651 * get_LoadingSpinnerImage_5() const { return ___LoadingSpinnerImage_5; }
	inline Image_t2670269651 ** get_address_of_LoadingSpinnerImage_5() { return &___LoadingSpinnerImage_5; }
	inline void set_LoadingSpinnerImage_5(Image_t2670269651 * value)
	{
		___LoadingSpinnerImage_5 = value;
		Il2CppCodeGenWriteBarrier((&___LoadingSpinnerImage_5), value);
	}

	inline static int32_t get_offset_of_CancelButton_6() { return static_cast<int32_t>(offsetof(ContentManager_t211916338, ___CancelButton_6)); }
	inline Button_t4055032469 * get_CancelButton_6() const { return ___CancelButton_6; }
	inline Button_t4055032469 ** get_address_of_CancelButton_6() { return &___CancelButton_6; }
	inline void set_CancelButton_6(Button_t4055032469 * value)
	{
		___CancelButton_6 = value;
		Il2CppCodeGenWriteBarrier((&___CancelButton_6), value);
	}

	inline static int32_t get_offset_of_AugmentationObject_7() { return static_cast<int32_t>(offsetof(ContentManager_t211916338, ___AugmentationObject_7)); }
	inline GameObject_t1113636619 * get_AugmentationObject_7() const { return ___AugmentationObject_7; }
	inline GameObject_t1113636619 ** get_address_of_AugmentationObject_7() { return &___AugmentationObject_7; }
	inline void set_AugmentationObject_7(GameObject_t1113636619 * value)
	{
		___AugmentationObject_7 = value;
		Il2CppCodeGenWriteBarrier((&___AugmentationObject_7), value);
	}

	inline static int32_t get_offset_of_AnimationsManager_8() { return static_cast<int32_t>(offsetof(ContentManager_t211916338, ___AnimationsManager_8)); }
	inline AnimationsManager_t2281084567 * get_AnimationsManager_8() const { return ___AnimationsManager_8; }
	inline AnimationsManager_t2281084567 ** get_address_of_AnimationsManager_8() { return &___AnimationsManager_8; }
	inline void set_AnimationsManager_8(AnimationsManager_t2281084567 * value)
	{
		___AnimationsManager_8 = value;
		Il2CppCodeGenWriteBarrier((&___AnimationsManager_8), value);
	}

	inline static int32_t get_offset_of_JsonServerUrl_9() { return static_cast<int32_t>(offsetof(ContentManager_t211916338, ___JsonServerUrl_9)); }
	inline String_t* get_JsonServerUrl_9() const { return ___JsonServerUrl_9; }
	inline String_t** get_address_of_JsonServerUrl_9() { return &___JsonServerUrl_9; }
	inline void set_JsonServerUrl_9(String_t* value)
	{
		___JsonServerUrl_9 = value;
		Il2CppCodeGenWriteBarrier((&___JsonServerUrl_9), value);
	}

	inline static int32_t get_offset_of_mIsShowingBookData_10() { return static_cast<int32_t>(offsetof(ContentManager_t211916338, ___mIsShowingBookData_10)); }
	inline bool get_mIsShowingBookData_10() const { return ___mIsShowingBookData_10; }
	inline bool* get_address_of_mIsShowingBookData_10() { return &___mIsShowingBookData_10; }
	inline void set_mIsShowingBookData_10(bool value)
	{
		___mIsShowingBookData_10 = value;
	}

	inline static int32_t get_offset_of_mIsLoadingBookData_11() { return static_cast<int32_t>(offsetof(ContentManager_t211916338, ___mIsLoadingBookData_11)); }
	inline bool get_mIsLoadingBookData_11() const { return ___mIsLoadingBookData_11; }
	inline bool* get_address_of_mIsLoadingBookData_11() { return &___mIsLoadingBookData_11; }
	inline void set_mIsLoadingBookData_11(bool value)
	{
		___mIsLoadingBookData_11 = value;
	}

	inline static int32_t get_offset_of_mIsLoadingBookThumb_12() { return static_cast<int32_t>(offsetof(ContentManager_t211916338, ___mIsLoadingBookThumb_12)); }
	inline bool get_mIsLoadingBookThumb_12() const { return ___mIsLoadingBookThumb_12; }
	inline bool* get_address_of_mIsLoadingBookThumb_12() { return &___mIsLoadingBookThumb_12; }
	inline void set_mIsLoadingBookThumb_12(bool value)
	{
		___mIsLoadingBookThumb_12 = value;
	}

	inline static int32_t get_offset_of_mJsonBookInfo_13() { return static_cast<int32_t>(offsetof(ContentManager_t211916338, ___mJsonBookInfo_13)); }
	inline WWW_t3688466362 * get_mJsonBookInfo_13() const { return ___mJsonBookInfo_13; }
	inline WWW_t3688466362 ** get_address_of_mJsonBookInfo_13() { return &___mJsonBookInfo_13; }
	inline void set_mJsonBookInfo_13(WWW_t3688466362 * value)
	{
		___mJsonBookInfo_13 = value;
		Il2CppCodeGenWriteBarrier((&___mJsonBookInfo_13), value);
	}

	inline static int32_t get_offset_of_mBookThumb_14() { return static_cast<int32_t>(offsetof(ContentManager_t211916338, ___mBookThumb_14)); }
	inline WWW_t3688466362 * get_mBookThumb_14() const { return ___mBookThumb_14; }
	inline WWW_t3688466362 ** get_address_of_mBookThumb_14() { return &___mBookThumb_14; }
	inline void set_mBookThumb_14(WWW_t3688466362 * value)
	{
		___mBookThumb_14 = value;
		Il2CppCodeGenWriteBarrier((&___mBookThumb_14), value);
	}

	inline static int32_t get_offset_of_mBookData_15() { return static_cast<int32_t>(offsetof(ContentManager_t211916338, ___mBookData_15)); }
	inline BookData_t1076903227 * get_mBookData_15() const { return ___mBookData_15; }
	inline BookData_t1076903227 ** get_address_of_mBookData_15() { return &___mBookData_15; }
	inline void set_mBookData_15(BookData_t1076903227 * value)
	{
		___mBookData_15 = value;
		Il2CppCodeGenWriteBarrier((&___mBookData_15), value);
	}

	inline static int32_t get_offset_of_mIsBookThumbRequested_16() { return static_cast<int32_t>(offsetof(ContentManager_t211916338, ___mIsBookThumbRequested_16)); }
	inline bool get_mIsBookThumbRequested_16() const { return ___mIsBookThumbRequested_16; }
	inline bool* get_address_of_mIsBookThumbRequested_16() { return &___mIsBookThumbRequested_16; }
	inline void set_mIsBookThumbRequested_16(bool value)
	{
		___mIsBookThumbRequested_16 = value;
	}

	inline static int32_t get_offset_of_mBookInformationParser_17() { return static_cast<int32_t>(offsetof(ContentManager_t211916338, ___mBookInformationParser_17)); }
	inline BookInformationParser_t151156397 * get_mBookInformationParser_17() const { return ___mBookInformationParser_17; }
	inline BookInformationParser_t151156397 ** get_address_of_mBookInformationParser_17() { return &___mBookInformationParser_17; }
	inline void set_mBookInformationParser_17(BookInformationParser_t151156397 * value)
	{
		___mBookInformationParser_17 = value;
		Il2CppCodeGenWriteBarrier((&___mBookInformationParser_17), value);
	}

	inline static int32_t get_offset_of_mIsShowingMenu_18() { return static_cast<int32_t>(offsetof(ContentManager_t211916338, ___mIsShowingMenu_18)); }
	inline bool get_mIsShowingMenu_18() const { return ___mIsShowingMenu_18; }
	inline bool* get_address_of_mIsShowingMenu_18() { return &___mIsShowingMenu_18; }
	inline void set_mIsShowingMenu_18(bool value)
	{
		___mIsShowingMenu_18 = value;
	}

	inline static int32_t get_offset_of_mCloudRecoBehaviour_19() { return static_cast<int32_t>(offsetof(ContentManager_t211916338, ___mCloudRecoBehaviour_19)); }
	inline CloudRecoBehaviour_t431762792 * get_mCloudRecoBehaviour_19() const { return ___mCloudRecoBehaviour_19; }
	inline CloudRecoBehaviour_t431762792 ** get_address_of_mCloudRecoBehaviour_19() { return &___mCloudRecoBehaviour_19; }
	inline void set_mCloudRecoBehaviour_19(CloudRecoBehaviour_t431762792 * value)
	{
		___mCloudRecoBehaviour_19 = value;
		Il2CppCodeGenWriteBarrier((&___mCloudRecoBehaviour_19), value);
	}

	inline static int32_t get_offset_of_mTrackableBehaviour_20() { return static_cast<int32_t>(offsetof(ContentManager_t211916338, ___mTrackableBehaviour_20)); }
	inline TrackableBehaviour_t1113559212 * get_mTrackableBehaviour_20() const { return ___mTrackableBehaviour_20; }
	inline TrackableBehaviour_t1113559212 ** get_address_of_mTrackableBehaviour_20() { return &___mTrackableBehaviour_20; }
	inline void set_mTrackableBehaviour_20(TrackableBehaviour_t1113559212 * value)
	{
		___mTrackableBehaviour_20 = value;
		Il2CppCodeGenWriteBarrier((&___mTrackableBehaviour_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTENTMANAGER_T211916338_H
#ifndef FRAMERATESETTINGS_T3598747490_H
#define FRAMERATESETTINGS_T3598747490_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FrameRateSettings
struct  FrameRateSettings_t3598747490  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FRAMERATESETTINGS_T3598747490_H
#ifndef GRATTAGE_T1486418579_H
#define GRATTAGE_T1486418579_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Grattage
struct  Grattage_t1486418579  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Texture2D Grattage::tex
	Texture2D_t3840446185 * ___tex_4;
	// UnityEngine.Texture2D Grattage::imageGrattable
	Texture2D_t3840446185 * ___imageGrattable_5;
	// UnityEngine.Sprite Grattage::mySprite
	Sprite_t280657092 * ___mySprite_6;
	// UnityEngine.Sprite Grattage::mySpriteResult
	Sprite_t280657092 * ___mySpriteResult_7;
	// UnityEngine.SpriteRenderer Grattage::sr
	SpriteRenderer_t3235626157 * ___sr_8;
	// UnityEngine.SpriteRenderer Grattage::srResult
	SpriteRenderer_t3235626157 * ___srResult_9;
	// UnityEngine.GameObject Grattage::newSpriteObj
	GameObject_t1113636619 * ___newSpriteObj_10;
	// UnityEngine.GameObject Grattage::newSpriteResult
	GameObject_t1113636619 * ___newSpriteResult_11;
	// UnityEngine.GameObject Grattage::cur
	GameObject_t1113636619 * ___cur_12;
	// UnityEngine.Plane Grattage::plan
	Plane_t1000493321  ___plan_13;
	// UnityEngine.Texture2D Grattage::imageResult
	Texture2D_t3840446185 * ___imageResult_14;
	// UnityEngine.GameObject Grattage::ImageAGratter
	GameObject_t1113636619 * ___ImageAGratter_15;
	// UnityEngine.Color Grattage::C
	Color_t2555686324  ___C_16;
	// System.Single Grattage::pixelByUnity
	float ___pixelByUnity_17;
	// System.Int32 Grattage::pourcentageImageDecouverte
	int32_t ___pourcentageImageDecouverte_18;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> Grattage::pointsEffaces
	List_1_t3628304265 * ___pointsEffaces_19;
	// System.Collections.Generic.List`1<Pixel> Grattage::listePixels
	List_1_t4186031580 * ___listePixels_20;
	// UnityEngine.GameObject Grattage::PrefabFireworks
	GameObject_t1113636619 * ___PrefabFireworks_21;
	// UnityEngine.GameObject Grattage::bravo
	GameObject_t1113636619 * ___bravo_22;
	// UnityEngine.GameObject Grattage::perdu
	GameObject_t1113636619 * ___perdu_23;
	// UnityEngine.GameObject Grattage::cursor
	GameObject_t1113636619 * ___cursor_24;
	// UnityEngine.Shader Grattage::shader
	Shader_t4151988712 * ___shader_25;
	// System.Boolean Grattage::reussite
	bool ___reussite_26;
	// System.Boolean Grattage::cursoraffiche
	bool ___cursoraffiche_27;
	// System.Single Grattage::pourcentageSeuil
	float ___pourcentageSeuil_28;
	// System.Int32 Grattage::TailleDuPinceau
	int32_t ___TailleDuPinceau_29;

public:
	inline static int32_t get_offset_of_tex_4() { return static_cast<int32_t>(offsetof(Grattage_t1486418579, ___tex_4)); }
	inline Texture2D_t3840446185 * get_tex_4() const { return ___tex_4; }
	inline Texture2D_t3840446185 ** get_address_of_tex_4() { return &___tex_4; }
	inline void set_tex_4(Texture2D_t3840446185 * value)
	{
		___tex_4 = value;
		Il2CppCodeGenWriteBarrier((&___tex_4), value);
	}

	inline static int32_t get_offset_of_imageGrattable_5() { return static_cast<int32_t>(offsetof(Grattage_t1486418579, ___imageGrattable_5)); }
	inline Texture2D_t3840446185 * get_imageGrattable_5() const { return ___imageGrattable_5; }
	inline Texture2D_t3840446185 ** get_address_of_imageGrattable_5() { return &___imageGrattable_5; }
	inline void set_imageGrattable_5(Texture2D_t3840446185 * value)
	{
		___imageGrattable_5 = value;
		Il2CppCodeGenWriteBarrier((&___imageGrattable_5), value);
	}

	inline static int32_t get_offset_of_mySprite_6() { return static_cast<int32_t>(offsetof(Grattage_t1486418579, ___mySprite_6)); }
	inline Sprite_t280657092 * get_mySprite_6() const { return ___mySprite_6; }
	inline Sprite_t280657092 ** get_address_of_mySprite_6() { return &___mySprite_6; }
	inline void set_mySprite_6(Sprite_t280657092 * value)
	{
		___mySprite_6 = value;
		Il2CppCodeGenWriteBarrier((&___mySprite_6), value);
	}

	inline static int32_t get_offset_of_mySpriteResult_7() { return static_cast<int32_t>(offsetof(Grattage_t1486418579, ___mySpriteResult_7)); }
	inline Sprite_t280657092 * get_mySpriteResult_7() const { return ___mySpriteResult_7; }
	inline Sprite_t280657092 ** get_address_of_mySpriteResult_7() { return &___mySpriteResult_7; }
	inline void set_mySpriteResult_7(Sprite_t280657092 * value)
	{
		___mySpriteResult_7 = value;
		Il2CppCodeGenWriteBarrier((&___mySpriteResult_7), value);
	}

	inline static int32_t get_offset_of_sr_8() { return static_cast<int32_t>(offsetof(Grattage_t1486418579, ___sr_8)); }
	inline SpriteRenderer_t3235626157 * get_sr_8() const { return ___sr_8; }
	inline SpriteRenderer_t3235626157 ** get_address_of_sr_8() { return &___sr_8; }
	inline void set_sr_8(SpriteRenderer_t3235626157 * value)
	{
		___sr_8 = value;
		Il2CppCodeGenWriteBarrier((&___sr_8), value);
	}

	inline static int32_t get_offset_of_srResult_9() { return static_cast<int32_t>(offsetof(Grattage_t1486418579, ___srResult_9)); }
	inline SpriteRenderer_t3235626157 * get_srResult_9() const { return ___srResult_9; }
	inline SpriteRenderer_t3235626157 ** get_address_of_srResult_9() { return &___srResult_9; }
	inline void set_srResult_9(SpriteRenderer_t3235626157 * value)
	{
		___srResult_9 = value;
		Il2CppCodeGenWriteBarrier((&___srResult_9), value);
	}

	inline static int32_t get_offset_of_newSpriteObj_10() { return static_cast<int32_t>(offsetof(Grattage_t1486418579, ___newSpriteObj_10)); }
	inline GameObject_t1113636619 * get_newSpriteObj_10() const { return ___newSpriteObj_10; }
	inline GameObject_t1113636619 ** get_address_of_newSpriteObj_10() { return &___newSpriteObj_10; }
	inline void set_newSpriteObj_10(GameObject_t1113636619 * value)
	{
		___newSpriteObj_10 = value;
		Il2CppCodeGenWriteBarrier((&___newSpriteObj_10), value);
	}

	inline static int32_t get_offset_of_newSpriteResult_11() { return static_cast<int32_t>(offsetof(Grattage_t1486418579, ___newSpriteResult_11)); }
	inline GameObject_t1113636619 * get_newSpriteResult_11() const { return ___newSpriteResult_11; }
	inline GameObject_t1113636619 ** get_address_of_newSpriteResult_11() { return &___newSpriteResult_11; }
	inline void set_newSpriteResult_11(GameObject_t1113636619 * value)
	{
		___newSpriteResult_11 = value;
		Il2CppCodeGenWriteBarrier((&___newSpriteResult_11), value);
	}

	inline static int32_t get_offset_of_cur_12() { return static_cast<int32_t>(offsetof(Grattage_t1486418579, ___cur_12)); }
	inline GameObject_t1113636619 * get_cur_12() const { return ___cur_12; }
	inline GameObject_t1113636619 ** get_address_of_cur_12() { return &___cur_12; }
	inline void set_cur_12(GameObject_t1113636619 * value)
	{
		___cur_12 = value;
		Il2CppCodeGenWriteBarrier((&___cur_12), value);
	}

	inline static int32_t get_offset_of_plan_13() { return static_cast<int32_t>(offsetof(Grattage_t1486418579, ___plan_13)); }
	inline Plane_t1000493321  get_plan_13() const { return ___plan_13; }
	inline Plane_t1000493321 * get_address_of_plan_13() { return &___plan_13; }
	inline void set_plan_13(Plane_t1000493321  value)
	{
		___plan_13 = value;
	}

	inline static int32_t get_offset_of_imageResult_14() { return static_cast<int32_t>(offsetof(Grattage_t1486418579, ___imageResult_14)); }
	inline Texture2D_t3840446185 * get_imageResult_14() const { return ___imageResult_14; }
	inline Texture2D_t3840446185 ** get_address_of_imageResult_14() { return &___imageResult_14; }
	inline void set_imageResult_14(Texture2D_t3840446185 * value)
	{
		___imageResult_14 = value;
		Il2CppCodeGenWriteBarrier((&___imageResult_14), value);
	}

	inline static int32_t get_offset_of_ImageAGratter_15() { return static_cast<int32_t>(offsetof(Grattage_t1486418579, ___ImageAGratter_15)); }
	inline GameObject_t1113636619 * get_ImageAGratter_15() const { return ___ImageAGratter_15; }
	inline GameObject_t1113636619 ** get_address_of_ImageAGratter_15() { return &___ImageAGratter_15; }
	inline void set_ImageAGratter_15(GameObject_t1113636619 * value)
	{
		___ImageAGratter_15 = value;
		Il2CppCodeGenWriteBarrier((&___ImageAGratter_15), value);
	}

	inline static int32_t get_offset_of_C_16() { return static_cast<int32_t>(offsetof(Grattage_t1486418579, ___C_16)); }
	inline Color_t2555686324  get_C_16() const { return ___C_16; }
	inline Color_t2555686324 * get_address_of_C_16() { return &___C_16; }
	inline void set_C_16(Color_t2555686324  value)
	{
		___C_16 = value;
	}

	inline static int32_t get_offset_of_pixelByUnity_17() { return static_cast<int32_t>(offsetof(Grattage_t1486418579, ___pixelByUnity_17)); }
	inline float get_pixelByUnity_17() const { return ___pixelByUnity_17; }
	inline float* get_address_of_pixelByUnity_17() { return &___pixelByUnity_17; }
	inline void set_pixelByUnity_17(float value)
	{
		___pixelByUnity_17 = value;
	}

	inline static int32_t get_offset_of_pourcentageImageDecouverte_18() { return static_cast<int32_t>(offsetof(Grattage_t1486418579, ___pourcentageImageDecouverte_18)); }
	inline int32_t get_pourcentageImageDecouverte_18() const { return ___pourcentageImageDecouverte_18; }
	inline int32_t* get_address_of_pourcentageImageDecouverte_18() { return &___pourcentageImageDecouverte_18; }
	inline void set_pourcentageImageDecouverte_18(int32_t value)
	{
		___pourcentageImageDecouverte_18 = value;
	}

	inline static int32_t get_offset_of_pointsEffaces_19() { return static_cast<int32_t>(offsetof(Grattage_t1486418579, ___pointsEffaces_19)); }
	inline List_1_t3628304265 * get_pointsEffaces_19() const { return ___pointsEffaces_19; }
	inline List_1_t3628304265 ** get_address_of_pointsEffaces_19() { return &___pointsEffaces_19; }
	inline void set_pointsEffaces_19(List_1_t3628304265 * value)
	{
		___pointsEffaces_19 = value;
		Il2CppCodeGenWriteBarrier((&___pointsEffaces_19), value);
	}

	inline static int32_t get_offset_of_listePixels_20() { return static_cast<int32_t>(offsetof(Grattage_t1486418579, ___listePixels_20)); }
	inline List_1_t4186031580 * get_listePixels_20() const { return ___listePixels_20; }
	inline List_1_t4186031580 ** get_address_of_listePixels_20() { return &___listePixels_20; }
	inline void set_listePixels_20(List_1_t4186031580 * value)
	{
		___listePixels_20 = value;
		Il2CppCodeGenWriteBarrier((&___listePixels_20), value);
	}

	inline static int32_t get_offset_of_PrefabFireworks_21() { return static_cast<int32_t>(offsetof(Grattage_t1486418579, ___PrefabFireworks_21)); }
	inline GameObject_t1113636619 * get_PrefabFireworks_21() const { return ___PrefabFireworks_21; }
	inline GameObject_t1113636619 ** get_address_of_PrefabFireworks_21() { return &___PrefabFireworks_21; }
	inline void set_PrefabFireworks_21(GameObject_t1113636619 * value)
	{
		___PrefabFireworks_21 = value;
		Il2CppCodeGenWriteBarrier((&___PrefabFireworks_21), value);
	}

	inline static int32_t get_offset_of_bravo_22() { return static_cast<int32_t>(offsetof(Grattage_t1486418579, ___bravo_22)); }
	inline GameObject_t1113636619 * get_bravo_22() const { return ___bravo_22; }
	inline GameObject_t1113636619 ** get_address_of_bravo_22() { return &___bravo_22; }
	inline void set_bravo_22(GameObject_t1113636619 * value)
	{
		___bravo_22 = value;
		Il2CppCodeGenWriteBarrier((&___bravo_22), value);
	}

	inline static int32_t get_offset_of_perdu_23() { return static_cast<int32_t>(offsetof(Grattage_t1486418579, ___perdu_23)); }
	inline GameObject_t1113636619 * get_perdu_23() const { return ___perdu_23; }
	inline GameObject_t1113636619 ** get_address_of_perdu_23() { return &___perdu_23; }
	inline void set_perdu_23(GameObject_t1113636619 * value)
	{
		___perdu_23 = value;
		Il2CppCodeGenWriteBarrier((&___perdu_23), value);
	}

	inline static int32_t get_offset_of_cursor_24() { return static_cast<int32_t>(offsetof(Grattage_t1486418579, ___cursor_24)); }
	inline GameObject_t1113636619 * get_cursor_24() const { return ___cursor_24; }
	inline GameObject_t1113636619 ** get_address_of_cursor_24() { return &___cursor_24; }
	inline void set_cursor_24(GameObject_t1113636619 * value)
	{
		___cursor_24 = value;
		Il2CppCodeGenWriteBarrier((&___cursor_24), value);
	}

	inline static int32_t get_offset_of_shader_25() { return static_cast<int32_t>(offsetof(Grattage_t1486418579, ___shader_25)); }
	inline Shader_t4151988712 * get_shader_25() const { return ___shader_25; }
	inline Shader_t4151988712 ** get_address_of_shader_25() { return &___shader_25; }
	inline void set_shader_25(Shader_t4151988712 * value)
	{
		___shader_25 = value;
		Il2CppCodeGenWriteBarrier((&___shader_25), value);
	}

	inline static int32_t get_offset_of_reussite_26() { return static_cast<int32_t>(offsetof(Grattage_t1486418579, ___reussite_26)); }
	inline bool get_reussite_26() const { return ___reussite_26; }
	inline bool* get_address_of_reussite_26() { return &___reussite_26; }
	inline void set_reussite_26(bool value)
	{
		___reussite_26 = value;
	}

	inline static int32_t get_offset_of_cursoraffiche_27() { return static_cast<int32_t>(offsetof(Grattage_t1486418579, ___cursoraffiche_27)); }
	inline bool get_cursoraffiche_27() const { return ___cursoraffiche_27; }
	inline bool* get_address_of_cursoraffiche_27() { return &___cursoraffiche_27; }
	inline void set_cursoraffiche_27(bool value)
	{
		___cursoraffiche_27 = value;
	}

	inline static int32_t get_offset_of_pourcentageSeuil_28() { return static_cast<int32_t>(offsetof(Grattage_t1486418579, ___pourcentageSeuil_28)); }
	inline float get_pourcentageSeuil_28() const { return ___pourcentageSeuil_28; }
	inline float* get_address_of_pourcentageSeuil_28() { return &___pourcentageSeuil_28; }
	inline void set_pourcentageSeuil_28(float value)
	{
		___pourcentageSeuil_28 = value;
	}

	inline static int32_t get_offset_of_TailleDuPinceau_29() { return static_cast<int32_t>(offsetof(Grattage_t1486418579, ___TailleDuPinceau_29)); }
	inline int32_t get_TailleDuPinceau_29() const { return ___TailleDuPinceau_29; }
	inline int32_t* get_address_of_TailleDuPinceau_29() { return &___TailleDuPinceau_29; }
	inline void set_TailleDuPinceau_29(int32_t value)
	{
		___TailleDuPinceau_29 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRATTAGE_T1486418579_H
#ifndef GRATTAGEMANAGER_T1333860694_H
#define GRATTAGEMANAGER_T1333860694_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GrattageManager
struct  GrattageManager_t1333860694  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject GrattageManager::goFront
	GameObject_t1113636619 * ___goFront_4;
	// UnityEngine.GameObject GrattageManager::goBack
	GameObject_t1113636619 * ___goBack_5;
	// UnityEngine.Sprite GrattageManager::spriteFront
	Sprite_t280657092 * ___spriteFront_6;
	// UnityEngine.Sprite GrattageManager::spriteBack
	Sprite_t280657092 * ___spriteBack_7;
	// UnityEngine.SpriteRenderer GrattageManager::srFront
	SpriteRenderer_t3235626157 * ___srFront_8;
	// UnityEngine.SpriteRenderer GrattageManager::srBack
	SpriteRenderer_t3235626157 * ___srBack_9;
	// UnityEngine.Texture2D GrattageManager::tex
	Texture2D_t3840446185 * ___tex_10;
	// UnityEngine.Texture2D GrattageManager::imageGrattable
	Texture2D_t3840446185 * ___imageGrattable_11;
	// UnityEngine.Sprite GrattageManager::mySprite
	Sprite_t280657092 * ___mySprite_12;
	// UnityEngine.Sprite GrattageManager::mySpriteResult
	Sprite_t280657092 * ___mySpriteResult_13;
	// UnityEngine.SpriteRenderer GrattageManager::sr
	SpriteRenderer_t3235626157 * ___sr_14;
	// UnityEngine.SpriteRenderer GrattageManager::srResult
	SpriteRenderer_t3235626157 * ___srResult_15;
	// UnityEngine.GameObject GrattageManager::newSpriteObj
	GameObject_t1113636619 * ___newSpriteObj_16;
	// UnityEngine.GameObject GrattageManager::newSpriteResult
	GameObject_t1113636619 * ___newSpriteResult_17;
	// UnityEngine.GameObject GrattageManager::cur
	GameObject_t1113636619 * ___cur_18;
	// UnityEngine.Plane GrattageManager::plan
	Plane_t1000493321  ___plan_19;
	// UnityEngine.Texture2D GrattageManager::imageResult
	Texture2D_t3840446185 * ___imageResult_20;
	// UnityEngine.GameObject GrattageManager::ImageAGratter
	GameObject_t1113636619 * ___ImageAGratter_21;
	// UnityEngine.Color GrattageManager::C
	Color_t2555686324  ___C_22;
	// System.Single GrattageManager::pixelPerUnit
	float ___pixelPerUnit_23;
	// System.Int32 GrattageManager::pourcentageImageDecouverte
	int32_t ___pourcentageImageDecouverte_24;
	// UnityEngine.GameObject GrattageManager::PrefabFireworks
	GameObject_t1113636619 * ___PrefabFireworks_25;
	// UnityEngine.GameObject GrattageManager::bravo
	GameObject_t1113636619 * ___bravo_26;
	// UnityEngine.GameObject GrattageManager::perdu
	GameObject_t1113636619 * ___perdu_27;
	// UnityEngine.GameObject GrattageManager::cursor
	GameObject_t1113636619 * ___cursor_28;
	// System.Boolean GrattageManager::reussite
	bool ___reussite_29;
	// System.Boolean GrattageManager::cursoraffiche
	bool ___cursoraffiche_30;
	// System.String GrattageManager::urlImageGratter
	String_t* ___urlImageGratter_31;
	// System.String GrattageManager::urlImageBack
	String_t* ___urlImageBack_32;
	// System.Boolean GrattageManager::rejouer
	bool ___rejouer_33;
	// System.String GrattageManager::reglement
	String_t* ___reglement_34;
	// System.String GrattageManager::status
	String_t* ___status_35;
	// System.String GrattageManager::id
	String_t* ___id_36;
	// System.String GrattageManager::name
	String_t* ___name_37;
	// System.String GrattageManager::reward_msg
	String_t* ___reward_msg_38;
	// System.String GrattageManager::link_of_reward
	String_t* ___link_of_reward_39;
	// System.String GrattageManager::token
	String_t* ___token_40;
	// System.Single GrattageManager::pourcentageSeuil
	float ___pourcentageSeuil_41;
	// System.Int32 GrattageManager::TailleDuPinceau
	int32_t ___TailleDuPinceau_42;

public:
	inline static int32_t get_offset_of_goFront_4() { return static_cast<int32_t>(offsetof(GrattageManager_t1333860694, ___goFront_4)); }
	inline GameObject_t1113636619 * get_goFront_4() const { return ___goFront_4; }
	inline GameObject_t1113636619 ** get_address_of_goFront_4() { return &___goFront_4; }
	inline void set_goFront_4(GameObject_t1113636619 * value)
	{
		___goFront_4 = value;
		Il2CppCodeGenWriteBarrier((&___goFront_4), value);
	}

	inline static int32_t get_offset_of_goBack_5() { return static_cast<int32_t>(offsetof(GrattageManager_t1333860694, ___goBack_5)); }
	inline GameObject_t1113636619 * get_goBack_5() const { return ___goBack_5; }
	inline GameObject_t1113636619 ** get_address_of_goBack_5() { return &___goBack_5; }
	inline void set_goBack_5(GameObject_t1113636619 * value)
	{
		___goBack_5 = value;
		Il2CppCodeGenWriteBarrier((&___goBack_5), value);
	}

	inline static int32_t get_offset_of_spriteFront_6() { return static_cast<int32_t>(offsetof(GrattageManager_t1333860694, ___spriteFront_6)); }
	inline Sprite_t280657092 * get_spriteFront_6() const { return ___spriteFront_6; }
	inline Sprite_t280657092 ** get_address_of_spriteFront_6() { return &___spriteFront_6; }
	inline void set_spriteFront_6(Sprite_t280657092 * value)
	{
		___spriteFront_6 = value;
		Il2CppCodeGenWriteBarrier((&___spriteFront_6), value);
	}

	inline static int32_t get_offset_of_spriteBack_7() { return static_cast<int32_t>(offsetof(GrattageManager_t1333860694, ___spriteBack_7)); }
	inline Sprite_t280657092 * get_spriteBack_7() const { return ___spriteBack_7; }
	inline Sprite_t280657092 ** get_address_of_spriteBack_7() { return &___spriteBack_7; }
	inline void set_spriteBack_7(Sprite_t280657092 * value)
	{
		___spriteBack_7 = value;
		Il2CppCodeGenWriteBarrier((&___spriteBack_7), value);
	}

	inline static int32_t get_offset_of_srFront_8() { return static_cast<int32_t>(offsetof(GrattageManager_t1333860694, ___srFront_8)); }
	inline SpriteRenderer_t3235626157 * get_srFront_8() const { return ___srFront_8; }
	inline SpriteRenderer_t3235626157 ** get_address_of_srFront_8() { return &___srFront_8; }
	inline void set_srFront_8(SpriteRenderer_t3235626157 * value)
	{
		___srFront_8 = value;
		Il2CppCodeGenWriteBarrier((&___srFront_8), value);
	}

	inline static int32_t get_offset_of_srBack_9() { return static_cast<int32_t>(offsetof(GrattageManager_t1333860694, ___srBack_9)); }
	inline SpriteRenderer_t3235626157 * get_srBack_9() const { return ___srBack_9; }
	inline SpriteRenderer_t3235626157 ** get_address_of_srBack_9() { return &___srBack_9; }
	inline void set_srBack_9(SpriteRenderer_t3235626157 * value)
	{
		___srBack_9 = value;
		Il2CppCodeGenWriteBarrier((&___srBack_9), value);
	}

	inline static int32_t get_offset_of_tex_10() { return static_cast<int32_t>(offsetof(GrattageManager_t1333860694, ___tex_10)); }
	inline Texture2D_t3840446185 * get_tex_10() const { return ___tex_10; }
	inline Texture2D_t3840446185 ** get_address_of_tex_10() { return &___tex_10; }
	inline void set_tex_10(Texture2D_t3840446185 * value)
	{
		___tex_10 = value;
		Il2CppCodeGenWriteBarrier((&___tex_10), value);
	}

	inline static int32_t get_offset_of_imageGrattable_11() { return static_cast<int32_t>(offsetof(GrattageManager_t1333860694, ___imageGrattable_11)); }
	inline Texture2D_t3840446185 * get_imageGrattable_11() const { return ___imageGrattable_11; }
	inline Texture2D_t3840446185 ** get_address_of_imageGrattable_11() { return &___imageGrattable_11; }
	inline void set_imageGrattable_11(Texture2D_t3840446185 * value)
	{
		___imageGrattable_11 = value;
		Il2CppCodeGenWriteBarrier((&___imageGrattable_11), value);
	}

	inline static int32_t get_offset_of_mySprite_12() { return static_cast<int32_t>(offsetof(GrattageManager_t1333860694, ___mySprite_12)); }
	inline Sprite_t280657092 * get_mySprite_12() const { return ___mySprite_12; }
	inline Sprite_t280657092 ** get_address_of_mySprite_12() { return &___mySprite_12; }
	inline void set_mySprite_12(Sprite_t280657092 * value)
	{
		___mySprite_12 = value;
		Il2CppCodeGenWriteBarrier((&___mySprite_12), value);
	}

	inline static int32_t get_offset_of_mySpriteResult_13() { return static_cast<int32_t>(offsetof(GrattageManager_t1333860694, ___mySpriteResult_13)); }
	inline Sprite_t280657092 * get_mySpriteResult_13() const { return ___mySpriteResult_13; }
	inline Sprite_t280657092 ** get_address_of_mySpriteResult_13() { return &___mySpriteResult_13; }
	inline void set_mySpriteResult_13(Sprite_t280657092 * value)
	{
		___mySpriteResult_13 = value;
		Il2CppCodeGenWriteBarrier((&___mySpriteResult_13), value);
	}

	inline static int32_t get_offset_of_sr_14() { return static_cast<int32_t>(offsetof(GrattageManager_t1333860694, ___sr_14)); }
	inline SpriteRenderer_t3235626157 * get_sr_14() const { return ___sr_14; }
	inline SpriteRenderer_t3235626157 ** get_address_of_sr_14() { return &___sr_14; }
	inline void set_sr_14(SpriteRenderer_t3235626157 * value)
	{
		___sr_14 = value;
		Il2CppCodeGenWriteBarrier((&___sr_14), value);
	}

	inline static int32_t get_offset_of_srResult_15() { return static_cast<int32_t>(offsetof(GrattageManager_t1333860694, ___srResult_15)); }
	inline SpriteRenderer_t3235626157 * get_srResult_15() const { return ___srResult_15; }
	inline SpriteRenderer_t3235626157 ** get_address_of_srResult_15() { return &___srResult_15; }
	inline void set_srResult_15(SpriteRenderer_t3235626157 * value)
	{
		___srResult_15 = value;
		Il2CppCodeGenWriteBarrier((&___srResult_15), value);
	}

	inline static int32_t get_offset_of_newSpriteObj_16() { return static_cast<int32_t>(offsetof(GrattageManager_t1333860694, ___newSpriteObj_16)); }
	inline GameObject_t1113636619 * get_newSpriteObj_16() const { return ___newSpriteObj_16; }
	inline GameObject_t1113636619 ** get_address_of_newSpriteObj_16() { return &___newSpriteObj_16; }
	inline void set_newSpriteObj_16(GameObject_t1113636619 * value)
	{
		___newSpriteObj_16 = value;
		Il2CppCodeGenWriteBarrier((&___newSpriteObj_16), value);
	}

	inline static int32_t get_offset_of_newSpriteResult_17() { return static_cast<int32_t>(offsetof(GrattageManager_t1333860694, ___newSpriteResult_17)); }
	inline GameObject_t1113636619 * get_newSpriteResult_17() const { return ___newSpriteResult_17; }
	inline GameObject_t1113636619 ** get_address_of_newSpriteResult_17() { return &___newSpriteResult_17; }
	inline void set_newSpriteResult_17(GameObject_t1113636619 * value)
	{
		___newSpriteResult_17 = value;
		Il2CppCodeGenWriteBarrier((&___newSpriteResult_17), value);
	}

	inline static int32_t get_offset_of_cur_18() { return static_cast<int32_t>(offsetof(GrattageManager_t1333860694, ___cur_18)); }
	inline GameObject_t1113636619 * get_cur_18() const { return ___cur_18; }
	inline GameObject_t1113636619 ** get_address_of_cur_18() { return &___cur_18; }
	inline void set_cur_18(GameObject_t1113636619 * value)
	{
		___cur_18 = value;
		Il2CppCodeGenWriteBarrier((&___cur_18), value);
	}

	inline static int32_t get_offset_of_plan_19() { return static_cast<int32_t>(offsetof(GrattageManager_t1333860694, ___plan_19)); }
	inline Plane_t1000493321  get_plan_19() const { return ___plan_19; }
	inline Plane_t1000493321 * get_address_of_plan_19() { return &___plan_19; }
	inline void set_plan_19(Plane_t1000493321  value)
	{
		___plan_19 = value;
	}

	inline static int32_t get_offset_of_imageResult_20() { return static_cast<int32_t>(offsetof(GrattageManager_t1333860694, ___imageResult_20)); }
	inline Texture2D_t3840446185 * get_imageResult_20() const { return ___imageResult_20; }
	inline Texture2D_t3840446185 ** get_address_of_imageResult_20() { return &___imageResult_20; }
	inline void set_imageResult_20(Texture2D_t3840446185 * value)
	{
		___imageResult_20 = value;
		Il2CppCodeGenWriteBarrier((&___imageResult_20), value);
	}

	inline static int32_t get_offset_of_ImageAGratter_21() { return static_cast<int32_t>(offsetof(GrattageManager_t1333860694, ___ImageAGratter_21)); }
	inline GameObject_t1113636619 * get_ImageAGratter_21() const { return ___ImageAGratter_21; }
	inline GameObject_t1113636619 ** get_address_of_ImageAGratter_21() { return &___ImageAGratter_21; }
	inline void set_ImageAGratter_21(GameObject_t1113636619 * value)
	{
		___ImageAGratter_21 = value;
		Il2CppCodeGenWriteBarrier((&___ImageAGratter_21), value);
	}

	inline static int32_t get_offset_of_C_22() { return static_cast<int32_t>(offsetof(GrattageManager_t1333860694, ___C_22)); }
	inline Color_t2555686324  get_C_22() const { return ___C_22; }
	inline Color_t2555686324 * get_address_of_C_22() { return &___C_22; }
	inline void set_C_22(Color_t2555686324  value)
	{
		___C_22 = value;
	}

	inline static int32_t get_offset_of_pixelPerUnit_23() { return static_cast<int32_t>(offsetof(GrattageManager_t1333860694, ___pixelPerUnit_23)); }
	inline float get_pixelPerUnit_23() const { return ___pixelPerUnit_23; }
	inline float* get_address_of_pixelPerUnit_23() { return &___pixelPerUnit_23; }
	inline void set_pixelPerUnit_23(float value)
	{
		___pixelPerUnit_23 = value;
	}

	inline static int32_t get_offset_of_pourcentageImageDecouverte_24() { return static_cast<int32_t>(offsetof(GrattageManager_t1333860694, ___pourcentageImageDecouverte_24)); }
	inline int32_t get_pourcentageImageDecouverte_24() const { return ___pourcentageImageDecouverte_24; }
	inline int32_t* get_address_of_pourcentageImageDecouverte_24() { return &___pourcentageImageDecouverte_24; }
	inline void set_pourcentageImageDecouverte_24(int32_t value)
	{
		___pourcentageImageDecouverte_24 = value;
	}

	inline static int32_t get_offset_of_PrefabFireworks_25() { return static_cast<int32_t>(offsetof(GrattageManager_t1333860694, ___PrefabFireworks_25)); }
	inline GameObject_t1113636619 * get_PrefabFireworks_25() const { return ___PrefabFireworks_25; }
	inline GameObject_t1113636619 ** get_address_of_PrefabFireworks_25() { return &___PrefabFireworks_25; }
	inline void set_PrefabFireworks_25(GameObject_t1113636619 * value)
	{
		___PrefabFireworks_25 = value;
		Il2CppCodeGenWriteBarrier((&___PrefabFireworks_25), value);
	}

	inline static int32_t get_offset_of_bravo_26() { return static_cast<int32_t>(offsetof(GrattageManager_t1333860694, ___bravo_26)); }
	inline GameObject_t1113636619 * get_bravo_26() const { return ___bravo_26; }
	inline GameObject_t1113636619 ** get_address_of_bravo_26() { return &___bravo_26; }
	inline void set_bravo_26(GameObject_t1113636619 * value)
	{
		___bravo_26 = value;
		Il2CppCodeGenWriteBarrier((&___bravo_26), value);
	}

	inline static int32_t get_offset_of_perdu_27() { return static_cast<int32_t>(offsetof(GrattageManager_t1333860694, ___perdu_27)); }
	inline GameObject_t1113636619 * get_perdu_27() const { return ___perdu_27; }
	inline GameObject_t1113636619 ** get_address_of_perdu_27() { return &___perdu_27; }
	inline void set_perdu_27(GameObject_t1113636619 * value)
	{
		___perdu_27 = value;
		Il2CppCodeGenWriteBarrier((&___perdu_27), value);
	}

	inline static int32_t get_offset_of_cursor_28() { return static_cast<int32_t>(offsetof(GrattageManager_t1333860694, ___cursor_28)); }
	inline GameObject_t1113636619 * get_cursor_28() const { return ___cursor_28; }
	inline GameObject_t1113636619 ** get_address_of_cursor_28() { return &___cursor_28; }
	inline void set_cursor_28(GameObject_t1113636619 * value)
	{
		___cursor_28 = value;
		Il2CppCodeGenWriteBarrier((&___cursor_28), value);
	}

	inline static int32_t get_offset_of_reussite_29() { return static_cast<int32_t>(offsetof(GrattageManager_t1333860694, ___reussite_29)); }
	inline bool get_reussite_29() const { return ___reussite_29; }
	inline bool* get_address_of_reussite_29() { return &___reussite_29; }
	inline void set_reussite_29(bool value)
	{
		___reussite_29 = value;
	}

	inline static int32_t get_offset_of_cursoraffiche_30() { return static_cast<int32_t>(offsetof(GrattageManager_t1333860694, ___cursoraffiche_30)); }
	inline bool get_cursoraffiche_30() const { return ___cursoraffiche_30; }
	inline bool* get_address_of_cursoraffiche_30() { return &___cursoraffiche_30; }
	inline void set_cursoraffiche_30(bool value)
	{
		___cursoraffiche_30 = value;
	}

	inline static int32_t get_offset_of_urlImageGratter_31() { return static_cast<int32_t>(offsetof(GrattageManager_t1333860694, ___urlImageGratter_31)); }
	inline String_t* get_urlImageGratter_31() const { return ___urlImageGratter_31; }
	inline String_t** get_address_of_urlImageGratter_31() { return &___urlImageGratter_31; }
	inline void set_urlImageGratter_31(String_t* value)
	{
		___urlImageGratter_31 = value;
		Il2CppCodeGenWriteBarrier((&___urlImageGratter_31), value);
	}

	inline static int32_t get_offset_of_urlImageBack_32() { return static_cast<int32_t>(offsetof(GrattageManager_t1333860694, ___urlImageBack_32)); }
	inline String_t* get_urlImageBack_32() const { return ___urlImageBack_32; }
	inline String_t** get_address_of_urlImageBack_32() { return &___urlImageBack_32; }
	inline void set_urlImageBack_32(String_t* value)
	{
		___urlImageBack_32 = value;
		Il2CppCodeGenWriteBarrier((&___urlImageBack_32), value);
	}

	inline static int32_t get_offset_of_rejouer_33() { return static_cast<int32_t>(offsetof(GrattageManager_t1333860694, ___rejouer_33)); }
	inline bool get_rejouer_33() const { return ___rejouer_33; }
	inline bool* get_address_of_rejouer_33() { return &___rejouer_33; }
	inline void set_rejouer_33(bool value)
	{
		___rejouer_33 = value;
	}

	inline static int32_t get_offset_of_reglement_34() { return static_cast<int32_t>(offsetof(GrattageManager_t1333860694, ___reglement_34)); }
	inline String_t* get_reglement_34() const { return ___reglement_34; }
	inline String_t** get_address_of_reglement_34() { return &___reglement_34; }
	inline void set_reglement_34(String_t* value)
	{
		___reglement_34 = value;
		Il2CppCodeGenWriteBarrier((&___reglement_34), value);
	}

	inline static int32_t get_offset_of_status_35() { return static_cast<int32_t>(offsetof(GrattageManager_t1333860694, ___status_35)); }
	inline String_t* get_status_35() const { return ___status_35; }
	inline String_t** get_address_of_status_35() { return &___status_35; }
	inline void set_status_35(String_t* value)
	{
		___status_35 = value;
		Il2CppCodeGenWriteBarrier((&___status_35), value);
	}

	inline static int32_t get_offset_of_id_36() { return static_cast<int32_t>(offsetof(GrattageManager_t1333860694, ___id_36)); }
	inline String_t* get_id_36() const { return ___id_36; }
	inline String_t** get_address_of_id_36() { return &___id_36; }
	inline void set_id_36(String_t* value)
	{
		___id_36 = value;
		Il2CppCodeGenWriteBarrier((&___id_36), value);
	}

	inline static int32_t get_offset_of_name_37() { return static_cast<int32_t>(offsetof(GrattageManager_t1333860694, ___name_37)); }
	inline String_t* get_name_37() const { return ___name_37; }
	inline String_t** get_address_of_name_37() { return &___name_37; }
	inline void set_name_37(String_t* value)
	{
		___name_37 = value;
		Il2CppCodeGenWriteBarrier((&___name_37), value);
	}

	inline static int32_t get_offset_of_reward_msg_38() { return static_cast<int32_t>(offsetof(GrattageManager_t1333860694, ___reward_msg_38)); }
	inline String_t* get_reward_msg_38() const { return ___reward_msg_38; }
	inline String_t** get_address_of_reward_msg_38() { return &___reward_msg_38; }
	inline void set_reward_msg_38(String_t* value)
	{
		___reward_msg_38 = value;
		Il2CppCodeGenWriteBarrier((&___reward_msg_38), value);
	}

	inline static int32_t get_offset_of_link_of_reward_39() { return static_cast<int32_t>(offsetof(GrattageManager_t1333860694, ___link_of_reward_39)); }
	inline String_t* get_link_of_reward_39() const { return ___link_of_reward_39; }
	inline String_t** get_address_of_link_of_reward_39() { return &___link_of_reward_39; }
	inline void set_link_of_reward_39(String_t* value)
	{
		___link_of_reward_39 = value;
		Il2CppCodeGenWriteBarrier((&___link_of_reward_39), value);
	}

	inline static int32_t get_offset_of_token_40() { return static_cast<int32_t>(offsetof(GrattageManager_t1333860694, ___token_40)); }
	inline String_t* get_token_40() const { return ___token_40; }
	inline String_t** get_address_of_token_40() { return &___token_40; }
	inline void set_token_40(String_t* value)
	{
		___token_40 = value;
		Il2CppCodeGenWriteBarrier((&___token_40), value);
	}

	inline static int32_t get_offset_of_pourcentageSeuil_41() { return static_cast<int32_t>(offsetof(GrattageManager_t1333860694, ___pourcentageSeuil_41)); }
	inline float get_pourcentageSeuil_41() const { return ___pourcentageSeuil_41; }
	inline float* get_address_of_pourcentageSeuil_41() { return &___pourcentageSeuil_41; }
	inline void set_pourcentageSeuil_41(float value)
	{
		___pourcentageSeuil_41 = value;
	}

	inline static int32_t get_offset_of_TailleDuPinceau_42() { return static_cast<int32_t>(offsetof(GrattageManager_t1333860694, ___TailleDuPinceau_42)); }
	inline int32_t get_TailleDuPinceau_42() const { return ___TailleDuPinceau_42; }
	inline int32_t* get_address_of_TailleDuPinceau_42() { return &___TailleDuPinceau_42; }
	inline void set_TailleDuPinceau_42(int32_t value)
	{
		___TailleDuPinceau_42 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRATTAGEMANAGER_T1333860694_H
#ifndef GRATTAGESCRIPT_T3667968469_H
#define GRATTAGESCRIPT_T3667968469_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GrattageScript
struct  GrattageScript_t3667968469  : public MonoBehaviour_t3962482529
{
public:
	// System.String GrattageScript::UserGUID
	String_t* ___UserGUID_4;
	// System.String GrattageScript::markerUID
	String_t* ___markerUID_5;
	// System.String GrattageScript::appName
	String_t* ___appName_6;
	// UnityEngine.GameObject GrattageScript::boutonGagne
	GameObject_t1113636619 * ___boutonGagne_7;
	// UnityEngine.GameObject GrattageScript::boutonPerdu
	GameObject_t1113636619 * ___boutonPerdu_8;
	// UnityEngine.GameObject GrattageScript::goFront
	GameObject_t1113636619 * ___goFront_9;
	// UnityEngine.GameObject GrattageScript::goBack
	GameObject_t1113636619 * ___goBack_10;
	// UnityEngine.Sprite GrattageScript::spriteBack
	Sprite_t280657092 * ___spriteBack_11;
	// UnityEngine.GameObject GrattageScript::emailPanel
	GameObject_t1113636619 * ___emailPanel_12;
	// TMPro.TextMeshProUGUI GrattageScript::emailPanel_mail
	TextMeshProUGUI_t529313277 * ___emailPanel_mail_13;
	// TMPro.TextMeshProUGUI GrattageScript::emailPanel_text
	TextMeshProUGUI_t529313277 * ___emailPanel_text_14;
	// UnityEngine.UI.Button GrattageScript::emailPanel_button
	Button_t4055032469 * ___emailPanel_button_15;
	// UnityEngine.Renderer GrattageScript::srFront
	Renderer_t2627027031 * ___srFront_16;
	// UnityEngine.Renderer GrattageScript::srBack
	Renderer_t2627027031 * ___srBack_17;
	// UnityEngine.Texture2D GrattageScript::tex
	Texture2D_t3840446185 * ___tex_18;
	// UnityEngine.Texture2D GrattageScript::imageGrattable
	Texture2D_t3840446185 * ___imageGrattable_19;
	// UnityEngine.Color GrattageScript::C
	Color_t2555686324  ___C_20;
	// System.Single GrattageScript::pixelPerUnit
	float ___pixelPerUnit_21;
	// System.Int32 GrattageScript::pourcentageImageDecouverte
	int32_t ___pourcentageImageDecouverte_22;
	// UnityEngine.GameObject GrattageScript::PrefabFireworks
	GameObject_t1113636619 * ___PrefabFireworks_23;
	// UnityEngine.GameObject GrattageScript::bravo
	GameObject_t1113636619 * ___bravo_24;
	// UnityEngine.GameObject GrattageScript::perdu
	GameObject_t1113636619 * ___perdu_25;
	// UnityEngine.GameObject GrattageScript::cursor
	GameObject_t1113636619 * ___cursor_26;
	// System.Boolean GrattageScript::reussite
	bool ___reussite_27;
	// System.Boolean GrattageScript::cursoraffiche
	bool ___cursoraffiche_28;
	// System.String GrattageScript::urlImageGratter
	String_t* ___urlImageGratter_29;
	// System.String GrattageScript::urlImageBack
	String_t* ___urlImageBack_30;
	// System.Boolean GrattageScript::rejouer
	bool ___rejouer_31;
	// System.String GrattageScript::reglement
	String_t* ___reglement_32;
	// System.String GrattageScript::status
	String_t* ___status_33;
	// System.String GrattageScript::id
	String_t* ___id_34;
	// System.String GrattageScript::nameJeu
	String_t* ___nameJeu_35;
	// System.String GrattageScript::reward_msg
	String_t* ___reward_msg_36;
	// System.String GrattageScript::link_of_reward
	String_t* ___link_of_reward_37;
	// System.String GrattageScript::token
	String_t* ___token_38;
	// System.String GrattageScript::recup_mail_tirage
	String_t* ___recup_mail_tirage_39;
	// System.Boolean GrattageScript::tirage_au_sort
	bool ___tirage_au_sort_40;
	// System.Boolean GrattageScript::isError
	bool ___isError_41;
	// System.String GrattageScript::errorMessage
	String_t* ___errorMessage_42;
	// UnityEngine.GameObject GrattageScript::pano_regles
	GameObject_t1113636619 * ___pano_regles_43;
	// UnityEngine.GameObject GrattageScript::pano_error
	GameObject_t1113636619 * ___pano_error_44;
	// UnityEngine.GameObject GrattageScript::ancetre
	GameObject_t1113636619 * ___ancetre_45;
	// UnityEngine.RaycastHit GrattageScript::hit
	RaycastHit_t1056001966  ___hit_46;
	// UnityEngine.Ray GrattageScript::ray
	Ray_t3785851493  ___ray_47;
	// UnityEngine.RaycastHit[] GrattageScript::hitAllTemp
	RaycastHitU5BU5D_t1690781147* ___hitAllTemp_48;
	// UnityEngine.Collider GrattageScript::colliderPlaneFront
	Collider_t1773347010 * ___colliderPlaneFront_49;
	// TMPro.TextMeshPro GrattageScript::textGagne3D
	TextMeshPro_t2393593166 * ___textGagne3D_50;
	// System.Single GrattageScript::pourcentageSeuil
	float ___pourcentageSeuil_51;
	// System.Int32 GrattageScript::TailleDuPinceau
	int32_t ___TailleDuPinceau_52;

public:
	inline static int32_t get_offset_of_UserGUID_4() { return static_cast<int32_t>(offsetof(GrattageScript_t3667968469, ___UserGUID_4)); }
	inline String_t* get_UserGUID_4() const { return ___UserGUID_4; }
	inline String_t** get_address_of_UserGUID_4() { return &___UserGUID_4; }
	inline void set_UserGUID_4(String_t* value)
	{
		___UserGUID_4 = value;
		Il2CppCodeGenWriteBarrier((&___UserGUID_4), value);
	}

	inline static int32_t get_offset_of_markerUID_5() { return static_cast<int32_t>(offsetof(GrattageScript_t3667968469, ___markerUID_5)); }
	inline String_t* get_markerUID_5() const { return ___markerUID_5; }
	inline String_t** get_address_of_markerUID_5() { return &___markerUID_5; }
	inline void set_markerUID_5(String_t* value)
	{
		___markerUID_5 = value;
		Il2CppCodeGenWriteBarrier((&___markerUID_5), value);
	}

	inline static int32_t get_offset_of_appName_6() { return static_cast<int32_t>(offsetof(GrattageScript_t3667968469, ___appName_6)); }
	inline String_t* get_appName_6() const { return ___appName_6; }
	inline String_t** get_address_of_appName_6() { return &___appName_6; }
	inline void set_appName_6(String_t* value)
	{
		___appName_6 = value;
		Il2CppCodeGenWriteBarrier((&___appName_6), value);
	}

	inline static int32_t get_offset_of_boutonGagne_7() { return static_cast<int32_t>(offsetof(GrattageScript_t3667968469, ___boutonGagne_7)); }
	inline GameObject_t1113636619 * get_boutonGagne_7() const { return ___boutonGagne_7; }
	inline GameObject_t1113636619 ** get_address_of_boutonGagne_7() { return &___boutonGagne_7; }
	inline void set_boutonGagne_7(GameObject_t1113636619 * value)
	{
		___boutonGagne_7 = value;
		Il2CppCodeGenWriteBarrier((&___boutonGagne_7), value);
	}

	inline static int32_t get_offset_of_boutonPerdu_8() { return static_cast<int32_t>(offsetof(GrattageScript_t3667968469, ___boutonPerdu_8)); }
	inline GameObject_t1113636619 * get_boutonPerdu_8() const { return ___boutonPerdu_8; }
	inline GameObject_t1113636619 ** get_address_of_boutonPerdu_8() { return &___boutonPerdu_8; }
	inline void set_boutonPerdu_8(GameObject_t1113636619 * value)
	{
		___boutonPerdu_8 = value;
		Il2CppCodeGenWriteBarrier((&___boutonPerdu_8), value);
	}

	inline static int32_t get_offset_of_goFront_9() { return static_cast<int32_t>(offsetof(GrattageScript_t3667968469, ___goFront_9)); }
	inline GameObject_t1113636619 * get_goFront_9() const { return ___goFront_9; }
	inline GameObject_t1113636619 ** get_address_of_goFront_9() { return &___goFront_9; }
	inline void set_goFront_9(GameObject_t1113636619 * value)
	{
		___goFront_9 = value;
		Il2CppCodeGenWriteBarrier((&___goFront_9), value);
	}

	inline static int32_t get_offset_of_goBack_10() { return static_cast<int32_t>(offsetof(GrattageScript_t3667968469, ___goBack_10)); }
	inline GameObject_t1113636619 * get_goBack_10() const { return ___goBack_10; }
	inline GameObject_t1113636619 ** get_address_of_goBack_10() { return &___goBack_10; }
	inline void set_goBack_10(GameObject_t1113636619 * value)
	{
		___goBack_10 = value;
		Il2CppCodeGenWriteBarrier((&___goBack_10), value);
	}

	inline static int32_t get_offset_of_spriteBack_11() { return static_cast<int32_t>(offsetof(GrattageScript_t3667968469, ___spriteBack_11)); }
	inline Sprite_t280657092 * get_spriteBack_11() const { return ___spriteBack_11; }
	inline Sprite_t280657092 ** get_address_of_spriteBack_11() { return &___spriteBack_11; }
	inline void set_spriteBack_11(Sprite_t280657092 * value)
	{
		___spriteBack_11 = value;
		Il2CppCodeGenWriteBarrier((&___spriteBack_11), value);
	}

	inline static int32_t get_offset_of_emailPanel_12() { return static_cast<int32_t>(offsetof(GrattageScript_t3667968469, ___emailPanel_12)); }
	inline GameObject_t1113636619 * get_emailPanel_12() const { return ___emailPanel_12; }
	inline GameObject_t1113636619 ** get_address_of_emailPanel_12() { return &___emailPanel_12; }
	inline void set_emailPanel_12(GameObject_t1113636619 * value)
	{
		___emailPanel_12 = value;
		Il2CppCodeGenWriteBarrier((&___emailPanel_12), value);
	}

	inline static int32_t get_offset_of_emailPanel_mail_13() { return static_cast<int32_t>(offsetof(GrattageScript_t3667968469, ___emailPanel_mail_13)); }
	inline TextMeshProUGUI_t529313277 * get_emailPanel_mail_13() const { return ___emailPanel_mail_13; }
	inline TextMeshProUGUI_t529313277 ** get_address_of_emailPanel_mail_13() { return &___emailPanel_mail_13; }
	inline void set_emailPanel_mail_13(TextMeshProUGUI_t529313277 * value)
	{
		___emailPanel_mail_13 = value;
		Il2CppCodeGenWriteBarrier((&___emailPanel_mail_13), value);
	}

	inline static int32_t get_offset_of_emailPanel_text_14() { return static_cast<int32_t>(offsetof(GrattageScript_t3667968469, ___emailPanel_text_14)); }
	inline TextMeshProUGUI_t529313277 * get_emailPanel_text_14() const { return ___emailPanel_text_14; }
	inline TextMeshProUGUI_t529313277 ** get_address_of_emailPanel_text_14() { return &___emailPanel_text_14; }
	inline void set_emailPanel_text_14(TextMeshProUGUI_t529313277 * value)
	{
		___emailPanel_text_14 = value;
		Il2CppCodeGenWriteBarrier((&___emailPanel_text_14), value);
	}

	inline static int32_t get_offset_of_emailPanel_button_15() { return static_cast<int32_t>(offsetof(GrattageScript_t3667968469, ___emailPanel_button_15)); }
	inline Button_t4055032469 * get_emailPanel_button_15() const { return ___emailPanel_button_15; }
	inline Button_t4055032469 ** get_address_of_emailPanel_button_15() { return &___emailPanel_button_15; }
	inline void set_emailPanel_button_15(Button_t4055032469 * value)
	{
		___emailPanel_button_15 = value;
		Il2CppCodeGenWriteBarrier((&___emailPanel_button_15), value);
	}

	inline static int32_t get_offset_of_srFront_16() { return static_cast<int32_t>(offsetof(GrattageScript_t3667968469, ___srFront_16)); }
	inline Renderer_t2627027031 * get_srFront_16() const { return ___srFront_16; }
	inline Renderer_t2627027031 ** get_address_of_srFront_16() { return &___srFront_16; }
	inline void set_srFront_16(Renderer_t2627027031 * value)
	{
		___srFront_16 = value;
		Il2CppCodeGenWriteBarrier((&___srFront_16), value);
	}

	inline static int32_t get_offset_of_srBack_17() { return static_cast<int32_t>(offsetof(GrattageScript_t3667968469, ___srBack_17)); }
	inline Renderer_t2627027031 * get_srBack_17() const { return ___srBack_17; }
	inline Renderer_t2627027031 ** get_address_of_srBack_17() { return &___srBack_17; }
	inline void set_srBack_17(Renderer_t2627027031 * value)
	{
		___srBack_17 = value;
		Il2CppCodeGenWriteBarrier((&___srBack_17), value);
	}

	inline static int32_t get_offset_of_tex_18() { return static_cast<int32_t>(offsetof(GrattageScript_t3667968469, ___tex_18)); }
	inline Texture2D_t3840446185 * get_tex_18() const { return ___tex_18; }
	inline Texture2D_t3840446185 ** get_address_of_tex_18() { return &___tex_18; }
	inline void set_tex_18(Texture2D_t3840446185 * value)
	{
		___tex_18 = value;
		Il2CppCodeGenWriteBarrier((&___tex_18), value);
	}

	inline static int32_t get_offset_of_imageGrattable_19() { return static_cast<int32_t>(offsetof(GrattageScript_t3667968469, ___imageGrattable_19)); }
	inline Texture2D_t3840446185 * get_imageGrattable_19() const { return ___imageGrattable_19; }
	inline Texture2D_t3840446185 ** get_address_of_imageGrattable_19() { return &___imageGrattable_19; }
	inline void set_imageGrattable_19(Texture2D_t3840446185 * value)
	{
		___imageGrattable_19 = value;
		Il2CppCodeGenWriteBarrier((&___imageGrattable_19), value);
	}

	inline static int32_t get_offset_of_C_20() { return static_cast<int32_t>(offsetof(GrattageScript_t3667968469, ___C_20)); }
	inline Color_t2555686324  get_C_20() const { return ___C_20; }
	inline Color_t2555686324 * get_address_of_C_20() { return &___C_20; }
	inline void set_C_20(Color_t2555686324  value)
	{
		___C_20 = value;
	}

	inline static int32_t get_offset_of_pixelPerUnit_21() { return static_cast<int32_t>(offsetof(GrattageScript_t3667968469, ___pixelPerUnit_21)); }
	inline float get_pixelPerUnit_21() const { return ___pixelPerUnit_21; }
	inline float* get_address_of_pixelPerUnit_21() { return &___pixelPerUnit_21; }
	inline void set_pixelPerUnit_21(float value)
	{
		___pixelPerUnit_21 = value;
	}

	inline static int32_t get_offset_of_pourcentageImageDecouverte_22() { return static_cast<int32_t>(offsetof(GrattageScript_t3667968469, ___pourcentageImageDecouverte_22)); }
	inline int32_t get_pourcentageImageDecouverte_22() const { return ___pourcentageImageDecouverte_22; }
	inline int32_t* get_address_of_pourcentageImageDecouverte_22() { return &___pourcentageImageDecouverte_22; }
	inline void set_pourcentageImageDecouverte_22(int32_t value)
	{
		___pourcentageImageDecouverte_22 = value;
	}

	inline static int32_t get_offset_of_PrefabFireworks_23() { return static_cast<int32_t>(offsetof(GrattageScript_t3667968469, ___PrefabFireworks_23)); }
	inline GameObject_t1113636619 * get_PrefabFireworks_23() const { return ___PrefabFireworks_23; }
	inline GameObject_t1113636619 ** get_address_of_PrefabFireworks_23() { return &___PrefabFireworks_23; }
	inline void set_PrefabFireworks_23(GameObject_t1113636619 * value)
	{
		___PrefabFireworks_23 = value;
		Il2CppCodeGenWriteBarrier((&___PrefabFireworks_23), value);
	}

	inline static int32_t get_offset_of_bravo_24() { return static_cast<int32_t>(offsetof(GrattageScript_t3667968469, ___bravo_24)); }
	inline GameObject_t1113636619 * get_bravo_24() const { return ___bravo_24; }
	inline GameObject_t1113636619 ** get_address_of_bravo_24() { return &___bravo_24; }
	inline void set_bravo_24(GameObject_t1113636619 * value)
	{
		___bravo_24 = value;
		Il2CppCodeGenWriteBarrier((&___bravo_24), value);
	}

	inline static int32_t get_offset_of_perdu_25() { return static_cast<int32_t>(offsetof(GrattageScript_t3667968469, ___perdu_25)); }
	inline GameObject_t1113636619 * get_perdu_25() const { return ___perdu_25; }
	inline GameObject_t1113636619 ** get_address_of_perdu_25() { return &___perdu_25; }
	inline void set_perdu_25(GameObject_t1113636619 * value)
	{
		___perdu_25 = value;
		Il2CppCodeGenWriteBarrier((&___perdu_25), value);
	}

	inline static int32_t get_offset_of_cursor_26() { return static_cast<int32_t>(offsetof(GrattageScript_t3667968469, ___cursor_26)); }
	inline GameObject_t1113636619 * get_cursor_26() const { return ___cursor_26; }
	inline GameObject_t1113636619 ** get_address_of_cursor_26() { return &___cursor_26; }
	inline void set_cursor_26(GameObject_t1113636619 * value)
	{
		___cursor_26 = value;
		Il2CppCodeGenWriteBarrier((&___cursor_26), value);
	}

	inline static int32_t get_offset_of_reussite_27() { return static_cast<int32_t>(offsetof(GrattageScript_t3667968469, ___reussite_27)); }
	inline bool get_reussite_27() const { return ___reussite_27; }
	inline bool* get_address_of_reussite_27() { return &___reussite_27; }
	inline void set_reussite_27(bool value)
	{
		___reussite_27 = value;
	}

	inline static int32_t get_offset_of_cursoraffiche_28() { return static_cast<int32_t>(offsetof(GrattageScript_t3667968469, ___cursoraffiche_28)); }
	inline bool get_cursoraffiche_28() const { return ___cursoraffiche_28; }
	inline bool* get_address_of_cursoraffiche_28() { return &___cursoraffiche_28; }
	inline void set_cursoraffiche_28(bool value)
	{
		___cursoraffiche_28 = value;
	}

	inline static int32_t get_offset_of_urlImageGratter_29() { return static_cast<int32_t>(offsetof(GrattageScript_t3667968469, ___urlImageGratter_29)); }
	inline String_t* get_urlImageGratter_29() const { return ___urlImageGratter_29; }
	inline String_t** get_address_of_urlImageGratter_29() { return &___urlImageGratter_29; }
	inline void set_urlImageGratter_29(String_t* value)
	{
		___urlImageGratter_29 = value;
		Il2CppCodeGenWriteBarrier((&___urlImageGratter_29), value);
	}

	inline static int32_t get_offset_of_urlImageBack_30() { return static_cast<int32_t>(offsetof(GrattageScript_t3667968469, ___urlImageBack_30)); }
	inline String_t* get_urlImageBack_30() const { return ___urlImageBack_30; }
	inline String_t** get_address_of_urlImageBack_30() { return &___urlImageBack_30; }
	inline void set_urlImageBack_30(String_t* value)
	{
		___urlImageBack_30 = value;
		Il2CppCodeGenWriteBarrier((&___urlImageBack_30), value);
	}

	inline static int32_t get_offset_of_rejouer_31() { return static_cast<int32_t>(offsetof(GrattageScript_t3667968469, ___rejouer_31)); }
	inline bool get_rejouer_31() const { return ___rejouer_31; }
	inline bool* get_address_of_rejouer_31() { return &___rejouer_31; }
	inline void set_rejouer_31(bool value)
	{
		___rejouer_31 = value;
	}

	inline static int32_t get_offset_of_reglement_32() { return static_cast<int32_t>(offsetof(GrattageScript_t3667968469, ___reglement_32)); }
	inline String_t* get_reglement_32() const { return ___reglement_32; }
	inline String_t** get_address_of_reglement_32() { return &___reglement_32; }
	inline void set_reglement_32(String_t* value)
	{
		___reglement_32 = value;
		Il2CppCodeGenWriteBarrier((&___reglement_32), value);
	}

	inline static int32_t get_offset_of_status_33() { return static_cast<int32_t>(offsetof(GrattageScript_t3667968469, ___status_33)); }
	inline String_t* get_status_33() const { return ___status_33; }
	inline String_t** get_address_of_status_33() { return &___status_33; }
	inline void set_status_33(String_t* value)
	{
		___status_33 = value;
		Il2CppCodeGenWriteBarrier((&___status_33), value);
	}

	inline static int32_t get_offset_of_id_34() { return static_cast<int32_t>(offsetof(GrattageScript_t3667968469, ___id_34)); }
	inline String_t* get_id_34() const { return ___id_34; }
	inline String_t** get_address_of_id_34() { return &___id_34; }
	inline void set_id_34(String_t* value)
	{
		___id_34 = value;
		Il2CppCodeGenWriteBarrier((&___id_34), value);
	}

	inline static int32_t get_offset_of_nameJeu_35() { return static_cast<int32_t>(offsetof(GrattageScript_t3667968469, ___nameJeu_35)); }
	inline String_t* get_nameJeu_35() const { return ___nameJeu_35; }
	inline String_t** get_address_of_nameJeu_35() { return &___nameJeu_35; }
	inline void set_nameJeu_35(String_t* value)
	{
		___nameJeu_35 = value;
		Il2CppCodeGenWriteBarrier((&___nameJeu_35), value);
	}

	inline static int32_t get_offset_of_reward_msg_36() { return static_cast<int32_t>(offsetof(GrattageScript_t3667968469, ___reward_msg_36)); }
	inline String_t* get_reward_msg_36() const { return ___reward_msg_36; }
	inline String_t** get_address_of_reward_msg_36() { return &___reward_msg_36; }
	inline void set_reward_msg_36(String_t* value)
	{
		___reward_msg_36 = value;
		Il2CppCodeGenWriteBarrier((&___reward_msg_36), value);
	}

	inline static int32_t get_offset_of_link_of_reward_37() { return static_cast<int32_t>(offsetof(GrattageScript_t3667968469, ___link_of_reward_37)); }
	inline String_t* get_link_of_reward_37() const { return ___link_of_reward_37; }
	inline String_t** get_address_of_link_of_reward_37() { return &___link_of_reward_37; }
	inline void set_link_of_reward_37(String_t* value)
	{
		___link_of_reward_37 = value;
		Il2CppCodeGenWriteBarrier((&___link_of_reward_37), value);
	}

	inline static int32_t get_offset_of_token_38() { return static_cast<int32_t>(offsetof(GrattageScript_t3667968469, ___token_38)); }
	inline String_t* get_token_38() const { return ___token_38; }
	inline String_t** get_address_of_token_38() { return &___token_38; }
	inline void set_token_38(String_t* value)
	{
		___token_38 = value;
		Il2CppCodeGenWriteBarrier((&___token_38), value);
	}

	inline static int32_t get_offset_of_recup_mail_tirage_39() { return static_cast<int32_t>(offsetof(GrattageScript_t3667968469, ___recup_mail_tirage_39)); }
	inline String_t* get_recup_mail_tirage_39() const { return ___recup_mail_tirage_39; }
	inline String_t** get_address_of_recup_mail_tirage_39() { return &___recup_mail_tirage_39; }
	inline void set_recup_mail_tirage_39(String_t* value)
	{
		___recup_mail_tirage_39 = value;
		Il2CppCodeGenWriteBarrier((&___recup_mail_tirage_39), value);
	}

	inline static int32_t get_offset_of_tirage_au_sort_40() { return static_cast<int32_t>(offsetof(GrattageScript_t3667968469, ___tirage_au_sort_40)); }
	inline bool get_tirage_au_sort_40() const { return ___tirage_au_sort_40; }
	inline bool* get_address_of_tirage_au_sort_40() { return &___tirage_au_sort_40; }
	inline void set_tirage_au_sort_40(bool value)
	{
		___tirage_au_sort_40 = value;
	}

	inline static int32_t get_offset_of_isError_41() { return static_cast<int32_t>(offsetof(GrattageScript_t3667968469, ___isError_41)); }
	inline bool get_isError_41() const { return ___isError_41; }
	inline bool* get_address_of_isError_41() { return &___isError_41; }
	inline void set_isError_41(bool value)
	{
		___isError_41 = value;
	}

	inline static int32_t get_offset_of_errorMessage_42() { return static_cast<int32_t>(offsetof(GrattageScript_t3667968469, ___errorMessage_42)); }
	inline String_t* get_errorMessage_42() const { return ___errorMessage_42; }
	inline String_t** get_address_of_errorMessage_42() { return &___errorMessage_42; }
	inline void set_errorMessage_42(String_t* value)
	{
		___errorMessage_42 = value;
		Il2CppCodeGenWriteBarrier((&___errorMessage_42), value);
	}

	inline static int32_t get_offset_of_pano_regles_43() { return static_cast<int32_t>(offsetof(GrattageScript_t3667968469, ___pano_regles_43)); }
	inline GameObject_t1113636619 * get_pano_regles_43() const { return ___pano_regles_43; }
	inline GameObject_t1113636619 ** get_address_of_pano_regles_43() { return &___pano_regles_43; }
	inline void set_pano_regles_43(GameObject_t1113636619 * value)
	{
		___pano_regles_43 = value;
		Il2CppCodeGenWriteBarrier((&___pano_regles_43), value);
	}

	inline static int32_t get_offset_of_pano_error_44() { return static_cast<int32_t>(offsetof(GrattageScript_t3667968469, ___pano_error_44)); }
	inline GameObject_t1113636619 * get_pano_error_44() const { return ___pano_error_44; }
	inline GameObject_t1113636619 ** get_address_of_pano_error_44() { return &___pano_error_44; }
	inline void set_pano_error_44(GameObject_t1113636619 * value)
	{
		___pano_error_44 = value;
		Il2CppCodeGenWriteBarrier((&___pano_error_44), value);
	}

	inline static int32_t get_offset_of_ancetre_45() { return static_cast<int32_t>(offsetof(GrattageScript_t3667968469, ___ancetre_45)); }
	inline GameObject_t1113636619 * get_ancetre_45() const { return ___ancetre_45; }
	inline GameObject_t1113636619 ** get_address_of_ancetre_45() { return &___ancetre_45; }
	inline void set_ancetre_45(GameObject_t1113636619 * value)
	{
		___ancetre_45 = value;
		Il2CppCodeGenWriteBarrier((&___ancetre_45), value);
	}

	inline static int32_t get_offset_of_hit_46() { return static_cast<int32_t>(offsetof(GrattageScript_t3667968469, ___hit_46)); }
	inline RaycastHit_t1056001966  get_hit_46() const { return ___hit_46; }
	inline RaycastHit_t1056001966 * get_address_of_hit_46() { return &___hit_46; }
	inline void set_hit_46(RaycastHit_t1056001966  value)
	{
		___hit_46 = value;
	}

	inline static int32_t get_offset_of_ray_47() { return static_cast<int32_t>(offsetof(GrattageScript_t3667968469, ___ray_47)); }
	inline Ray_t3785851493  get_ray_47() const { return ___ray_47; }
	inline Ray_t3785851493 * get_address_of_ray_47() { return &___ray_47; }
	inline void set_ray_47(Ray_t3785851493  value)
	{
		___ray_47 = value;
	}

	inline static int32_t get_offset_of_hitAllTemp_48() { return static_cast<int32_t>(offsetof(GrattageScript_t3667968469, ___hitAllTemp_48)); }
	inline RaycastHitU5BU5D_t1690781147* get_hitAllTemp_48() const { return ___hitAllTemp_48; }
	inline RaycastHitU5BU5D_t1690781147** get_address_of_hitAllTemp_48() { return &___hitAllTemp_48; }
	inline void set_hitAllTemp_48(RaycastHitU5BU5D_t1690781147* value)
	{
		___hitAllTemp_48 = value;
		Il2CppCodeGenWriteBarrier((&___hitAllTemp_48), value);
	}

	inline static int32_t get_offset_of_colliderPlaneFront_49() { return static_cast<int32_t>(offsetof(GrattageScript_t3667968469, ___colliderPlaneFront_49)); }
	inline Collider_t1773347010 * get_colliderPlaneFront_49() const { return ___colliderPlaneFront_49; }
	inline Collider_t1773347010 ** get_address_of_colliderPlaneFront_49() { return &___colliderPlaneFront_49; }
	inline void set_colliderPlaneFront_49(Collider_t1773347010 * value)
	{
		___colliderPlaneFront_49 = value;
		Il2CppCodeGenWriteBarrier((&___colliderPlaneFront_49), value);
	}

	inline static int32_t get_offset_of_textGagne3D_50() { return static_cast<int32_t>(offsetof(GrattageScript_t3667968469, ___textGagne3D_50)); }
	inline TextMeshPro_t2393593166 * get_textGagne3D_50() const { return ___textGagne3D_50; }
	inline TextMeshPro_t2393593166 ** get_address_of_textGagne3D_50() { return &___textGagne3D_50; }
	inline void set_textGagne3D_50(TextMeshPro_t2393593166 * value)
	{
		___textGagne3D_50 = value;
		Il2CppCodeGenWriteBarrier((&___textGagne3D_50), value);
	}

	inline static int32_t get_offset_of_pourcentageSeuil_51() { return static_cast<int32_t>(offsetof(GrattageScript_t3667968469, ___pourcentageSeuil_51)); }
	inline float get_pourcentageSeuil_51() const { return ___pourcentageSeuil_51; }
	inline float* get_address_of_pourcentageSeuil_51() { return &___pourcentageSeuil_51; }
	inline void set_pourcentageSeuil_51(float value)
	{
		___pourcentageSeuil_51 = value;
	}

	inline static int32_t get_offset_of_TailleDuPinceau_52() { return static_cast<int32_t>(offsetof(GrattageScript_t3667968469, ___TailleDuPinceau_52)); }
	inline int32_t get_TailleDuPinceau_52() const { return ___TailleDuPinceau_52; }
	inline int32_t* get_address_of_TailleDuPinceau_52() { return &___TailleDuPinceau_52; }
	inline void set_TailleDuPinceau_52(int32_t value)
	{
		___TailleDuPinceau_52 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRATTAGESCRIPT_T3667968469_H
#ifndef INITERRORHANDLER_T2159361531_H
#define INITERRORHANDLER_T2159361531_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// InitErrorHandler
struct  InitErrorHandler_t2159361531  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Text InitErrorHandler::errorText
	Text_t1901882714 * ___errorText_4;
	// UnityEngine.Canvas InitErrorHandler::errorCanvas
	Canvas_t3310196443 * ___errorCanvas_5;
	// System.String InitErrorHandler::key
	String_t* ___key_6;
	// System.String InitErrorHandler::errorMsg
	String_t* ___errorMsg_7;

public:
	inline static int32_t get_offset_of_errorText_4() { return static_cast<int32_t>(offsetof(InitErrorHandler_t2159361531, ___errorText_4)); }
	inline Text_t1901882714 * get_errorText_4() const { return ___errorText_4; }
	inline Text_t1901882714 ** get_address_of_errorText_4() { return &___errorText_4; }
	inline void set_errorText_4(Text_t1901882714 * value)
	{
		___errorText_4 = value;
		Il2CppCodeGenWriteBarrier((&___errorText_4), value);
	}

	inline static int32_t get_offset_of_errorCanvas_5() { return static_cast<int32_t>(offsetof(InitErrorHandler_t2159361531, ___errorCanvas_5)); }
	inline Canvas_t3310196443 * get_errorCanvas_5() const { return ___errorCanvas_5; }
	inline Canvas_t3310196443 ** get_address_of_errorCanvas_5() { return &___errorCanvas_5; }
	inline void set_errorCanvas_5(Canvas_t3310196443 * value)
	{
		___errorCanvas_5 = value;
		Il2CppCodeGenWriteBarrier((&___errorCanvas_5), value);
	}

	inline static int32_t get_offset_of_key_6() { return static_cast<int32_t>(offsetof(InitErrorHandler_t2159361531, ___key_6)); }
	inline String_t* get_key_6() const { return ___key_6; }
	inline String_t** get_address_of_key_6() { return &___key_6; }
	inline void set_key_6(String_t* value)
	{
		___key_6 = value;
		Il2CppCodeGenWriteBarrier((&___key_6), value);
	}

	inline static int32_t get_offset_of_errorMsg_7() { return static_cast<int32_t>(offsetof(InitErrorHandler_t2159361531, ___errorMsg_7)); }
	inline String_t* get_errorMsg_7() const { return ___errorMsg_7; }
	inline String_t** get_address_of_errorMsg_7() { return &___errorMsg_7; }
	inline void set_errorMsg_7(String_t* value)
	{
		___errorMsg_7 = value;
		Il2CppCodeGenWriteBarrier((&___errorMsg_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INITERRORHANDLER_T2159361531_H
#ifndef LOADINGSCREEN_T2154736699_H
#define LOADINGSCREEN_T2154736699_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoadingScreen
struct  LoadingScreen_t2154736699  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean LoadingScreen::mChangeLevel
	bool ___mChangeLevel_4;
	// UnityEngine.UI.RawImage LoadingScreen::mUISpinner
	RawImage_t3182918964 * ___mUISpinner_5;

public:
	inline static int32_t get_offset_of_mChangeLevel_4() { return static_cast<int32_t>(offsetof(LoadingScreen_t2154736699, ___mChangeLevel_4)); }
	inline bool get_mChangeLevel_4() const { return ___mChangeLevel_4; }
	inline bool* get_address_of_mChangeLevel_4() { return &___mChangeLevel_4; }
	inline void set_mChangeLevel_4(bool value)
	{
		___mChangeLevel_4 = value;
	}

	inline static int32_t get_offset_of_mUISpinner_5() { return static_cast<int32_t>(offsetof(LoadingScreen_t2154736699, ___mUISpinner_5)); }
	inline RawImage_t3182918964 * get_mUISpinner_5() const { return ___mUISpinner_5; }
	inline RawImage_t3182918964 ** get_address_of_mUISpinner_5() { return &___mUISpinner_5; }
	inline void set_mUISpinner_5(RawImage_t3182918964 * value)
	{
		___mUISpinner_5 = value;
		Il2CppCodeGenWriteBarrier((&___mUISpinner_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOADINGSCREEN_T2154736699_H
#ifndef MENUANIMATOR_T2112910832_H
#define MENUANIMATOR_T2112910832_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MenuAnimator
struct  MenuAnimator_t2112910832  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Vector3 MenuAnimator::mVisiblePos
	Vector3_t3722313464  ___mVisiblePos_4;
	// UnityEngine.Vector3 MenuAnimator::mInvisiblePos
	Vector3_t3722313464  ___mInvisiblePos_5;
	// System.Single MenuAnimator::mVisibility
	float ___mVisibility_6;
	// System.Boolean MenuAnimator::mVisible
	bool ___mVisible_7;
	// UnityEngine.Canvas MenuAnimator::mCanvas
	Canvas_t3310196443 * ___mCanvas_8;
	// MenuOptions MenuAnimator::mMenuOptions
	MenuOptions_t1951716431 * ___mMenuOptions_9;
	// System.Single MenuAnimator::SlidingTime
	float ___SlidingTime_10;

public:
	inline static int32_t get_offset_of_mVisiblePos_4() { return static_cast<int32_t>(offsetof(MenuAnimator_t2112910832, ___mVisiblePos_4)); }
	inline Vector3_t3722313464  get_mVisiblePos_4() const { return ___mVisiblePos_4; }
	inline Vector3_t3722313464 * get_address_of_mVisiblePos_4() { return &___mVisiblePos_4; }
	inline void set_mVisiblePos_4(Vector3_t3722313464  value)
	{
		___mVisiblePos_4 = value;
	}

	inline static int32_t get_offset_of_mInvisiblePos_5() { return static_cast<int32_t>(offsetof(MenuAnimator_t2112910832, ___mInvisiblePos_5)); }
	inline Vector3_t3722313464  get_mInvisiblePos_5() const { return ___mInvisiblePos_5; }
	inline Vector3_t3722313464 * get_address_of_mInvisiblePos_5() { return &___mInvisiblePos_5; }
	inline void set_mInvisiblePos_5(Vector3_t3722313464  value)
	{
		___mInvisiblePos_5 = value;
	}

	inline static int32_t get_offset_of_mVisibility_6() { return static_cast<int32_t>(offsetof(MenuAnimator_t2112910832, ___mVisibility_6)); }
	inline float get_mVisibility_6() const { return ___mVisibility_6; }
	inline float* get_address_of_mVisibility_6() { return &___mVisibility_6; }
	inline void set_mVisibility_6(float value)
	{
		___mVisibility_6 = value;
	}

	inline static int32_t get_offset_of_mVisible_7() { return static_cast<int32_t>(offsetof(MenuAnimator_t2112910832, ___mVisible_7)); }
	inline bool get_mVisible_7() const { return ___mVisible_7; }
	inline bool* get_address_of_mVisible_7() { return &___mVisible_7; }
	inline void set_mVisible_7(bool value)
	{
		___mVisible_7 = value;
	}

	inline static int32_t get_offset_of_mCanvas_8() { return static_cast<int32_t>(offsetof(MenuAnimator_t2112910832, ___mCanvas_8)); }
	inline Canvas_t3310196443 * get_mCanvas_8() const { return ___mCanvas_8; }
	inline Canvas_t3310196443 ** get_address_of_mCanvas_8() { return &___mCanvas_8; }
	inline void set_mCanvas_8(Canvas_t3310196443 * value)
	{
		___mCanvas_8 = value;
		Il2CppCodeGenWriteBarrier((&___mCanvas_8), value);
	}

	inline static int32_t get_offset_of_mMenuOptions_9() { return static_cast<int32_t>(offsetof(MenuAnimator_t2112910832, ___mMenuOptions_9)); }
	inline MenuOptions_t1951716431 * get_mMenuOptions_9() const { return ___mMenuOptions_9; }
	inline MenuOptions_t1951716431 ** get_address_of_mMenuOptions_9() { return &___mMenuOptions_9; }
	inline void set_mMenuOptions_9(MenuOptions_t1951716431 * value)
	{
		___mMenuOptions_9 = value;
		Il2CppCodeGenWriteBarrier((&___mMenuOptions_9), value);
	}

	inline static int32_t get_offset_of_SlidingTime_10() { return static_cast<int32_t>(offsetof(MenuAnimator_t2112910832, ___SlidingTime_10)); }
	inline float get_SlidingTime_10() const { return ___SlidingTime_10; }
	inline float* get_address_of_SlidingTime_10() { return &___SlidingTime_10; }
	inline void set_SlidingTime_10(float value)
	{
		___SlidingTime_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MENUANIMATOR_T2112910832_H
#ifndef MENUOPTIONS_T1951716431_H
#define MENUOPTIONS_T1951716431_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MenuOptions
struct  MenuOptions_t1951716431  : public MonoBehaviour_t3962482529
{
public:
	// CameraSettings MenuOptions::mCamSettings
	CameraSettings_t3152619780 * ___mCamSettings_4;
	// TrackableSettings MenuOptions::mTrackableSettings
	TrackableSettings_t2862243993 * ___mTrackableSettings_5;
	// MenuAnimator MenuOptions::mMenuAnim
	MenuAnimator_t2112910832 * ___mMenuAnim_6;

public:
	inline static int32_t get_offset_of_mCamSettings_4() { return static_cast<int32_t>(offsetof(MenuOptions_t1951716431, ___mCamSettings_4)); }
	inline CameraSettings_t3152619780 * get_mCamSettings_4() const { return ___mCamSettings_4; }
	inline CameraSettings_t3152619780 ** get_address_of_mCamSettings_4() { return &___mCamSettings_4; }
	inline void set_mCamSettings_4(CameraSettings_t3152619780 * value)
	{
		___mCamSettings_4 = value;
		Il2CppCodeGenWriteBarrier((&___mCamSettings_4), value);
	}

	inline static int32_t get_offset_of_mTrackableSettings_5() { return static_cast<int32_t>(offsetof(MenuOptions_t1951716431, ___mTrackableSettings_5)); }
	inline TrackableSettings_t2862243993 * get_mTrackableSettings_5() const { return ___mTrackableSettings_5; }
	inline TrackableSettings_t2862243993 ** get_address_of_mTrackableSettings_5() { return &___mTrackableSettings_5; }
	inline void set_mTrackableSettings_5(TrackableSettings_t2862243993 * value)
	{
		___mTrackableSettings_5 = value;
		Il2CppCodeGenWriteBarrier((&___mTrackableSettings_5), value);
	}

	inline static int32_t get_offset_of_mMenuAnim_6() { return static_cast<int32_t>(offsetof(MenuOptions_t1951716431, ___mMenuAnim_6)); }
	inline MenuAnimator_t2112910832 * get_mMenuAnim_6() const { return ___mMenuAnim_6; }
	inline MenuAnimator_t2112910832 ** get_address_of_mMenuAnim_6() { return &___mMenuAnim_6; }
	inline void set_mMenuAnim_6(MenuAnimator_t2112910832 * value)
	{
		___mMenuAnim_6 = value;
		Il2CppCodeGenWriteBarrier((&___mMenuAnim_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MENUOPTIONS_T1951716431_H
#ifndef SCANLINE_T269422218_H
#define SCANLINE_T269422218_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ScanLine
struct  ScanLine_t269422218  : public MonoBehaviour_t3962482529
{
public:
	// System.Single ScanLine::mTime
	float ___mTime_4;
	// System.Single ScanLine::mScanDuration
	float ___mScanDuration_5;
	// System.Boolean ScanLine::mMovingDown
	bool ___mMovingDown_6;

public:
	inline static int32_t get_offset_of_mTime_4() { return static_cast<int32_t>(offsetof(ScanLine_t269422218, ___mTime_4)); }
	inline float get_mTime_4() const { return ___mTime_4; }
	inline float* get_address_of_mTime_4() { return &___mTime_4; }
	inline void set_mTime_4(float value)
	{
		___mTime_4 = value;
	}

	inline static int32_t get_offset_of_mScanDuration_5() { return static_cast<int32_t>(offsetof(ScanLine_t269422218, ___mScanDuration_5)); }
	inline float get_mScanDuration_5() const { return ___mScanDuration_5; }
	inline float* get_address_of_mScanDuration_5() { return &___mScanDuration_5; }
	inline void set_mScanDuration_5(float value)
	{
		___mScanDuration_5 = value;
	}

	inline static int32_t get_offset_of_mMovingDown_6() { return static_cast<int32_t>(offsetof(ScanLine_t269422218, ___mMovingDown_6)); }
	inline bool get_mMovingDown_6() const { return ___mMovingDown_6; }
	inline bool* get_address_of_mMovingDown_6() { return &___mMovingDown_6; }
	inline void set_mMovingDown_6(bool value)
	{
		___mMovingDown_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCANLINE_T269422218_H
#ifndef SEQUENCES_T3991711780_H
#define SEQUENCES_T3991711780_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sequences
struct  Sequences_t3991711780  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Transform Sequences::target
	Transform_t3600365921 * ___target_4;

public:
	inline static int32_t get_offset_of_target_4() { return static_cast<int32_t>(offsetof(Sequences_t3991711780, ___target_4)); }
	inline Transform_t3600365921 * get_target_4() const { return ___target_4; }
	inline Transform_t3600365921 ** get_address_of_target_4() { return &___target_4; }
	inline void set_target_4(Transform_t3600365921 * value)
	{
		___target_4 = value;
		Il2CppCodeGenWriteBarrier((&___target_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SEQUENCES_T3991711780_H
#ifndef STARSRATINGCONTROL_T657076381_H
#define STARSRATINGCONTROL_T657076381_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// StarsRatingControl
struct  StarsRatingControl_t657076381  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject[] StarsRatingControl::Stars
	GameObjectU5BU5D_t3328599146* ___Stars_4;
	// UnityEngine.Material StarsRatingControl::FillTexture
	Material_t340375123 * ___FillTexture_5;
	// UnityEngine.Material StarsRatingControl::EmptyTexture
	Material_t340375123 * ___EmptyTexture_6;
	// System.Int32 StarsRatingControl::mRating
	int32_t ___mRating_7;

public:
	inline static int32_t get_offset_of_Stars_4() { return static_cast<int32_t>(offsetof(StarsRatingControl_t657076381, ___Stars_4)); }
	inline GameObjectU5BU5D_t3328599146* get_Stars_4() const { return ___Stars_4; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_Stars_4() { return &___Stars_4; }
	inline void set_Stars_4(GameObjectU5BU5D_t3328599146* value)
	{
		___Stars_4 = value;
		Il2CppCodeGenWriteBarrier((&___Stars_4), value);
	}

	inline static int32_t get_offset_of_FillTexture_5() { return static_cast<int32_t>(offsetof(StarsRatingControl_t657076381, ___FillTexture_5)); }
	inline Material_t340375123 * get_FillTexture_5() const { return ___FillTexture_5; }
	inline Material_t340375123 ** get_address_of_FillTexture_5() { return &___FillTexture_5; }
	inline void set_FillTexture_5(Material_t340375123 * value)
	{
		___FillTexture_5 = value;
		Il2CppCodeGenWriteBarrier((&___FillTexture_5), value);
	}

	inline static int32_t get_offset_of_EmptyTexture_6() { return static_cast<int32_t>(offsetof(StarsRatingControl_t657076381, ___EmptyTexture_6)); }
	inline Material_t340375123 * get_EmptyTexture_6() const { return ___EmptyTexture_6; }
	inline Material_t340375123 ** get_address_of_EmptyTexture_6() { return &___EmptyTexture_6; }
	inline void set_EmptyTexture_6(Material_t340375123 * value)
	{
		___EmptyTexture_6 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyTexture_6), value);
	}

	inline static int32_t get_offset_of_mRating_7() { return static_cast<int32_t>(offsetof(StarsRatingControl_t657076381, ___mRating_7)); }
	inline int32_t get_mRating_7() const { return ___mRating_7; }
	inline int32_t* get_address_of_mRating_7() { return &___mRating_7; }
	inline void set_mRating_7(int32_t value)
	{
		___mRating_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STARSRATINGCONTROL_T657076381_H
#ifndef TAPHANDLER_T334234343_H
#define TAPHANDLER_T334234343_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TapHandler
struct  TapHandler_t334234343  : public MonoBehaviour_t3962482529
{
public:
	// System.Single TapHandler::mTimeSinceLastTap
	float ___mTimeSinceLastTap_5;
	// MenuAnimator TapHandler::mMenuAnim
	MenuAnimator_t2112910832 * ___mMenuAnim_6;
	// System.Int32 TapHandler::mTapCount
	int32_t ___mTapCount_7;

public:
	inline static int32_t get_offset_of_mTimeSinceLastTap_5() { return static_cast<int32_t>(offsetof(TapHandler_t334234343, ___mTimeSinceLastTap_5)); }
	inline float get_mTimeSinceLastTap_5() const { return ___mTimeSinceLastTap_5; }
	inline float* get_address_of_mTimeSinceLastTap_5() { return &___mTimeSinceLastTap_5; }
	inline void set_mTimeSinceLastTap_5(float value)
	{
		___mTimeSinceLastTap_5 = value;
	}

	inline static int32_t get_offset_of_mMenuAnim_6() { return static_cast<int32_t>(offsetof(TapHandler_t334234343, ___mMenuAnim_6)); }
	inline MenuAnimator_t2112910832 * get_mMenuAnim_6() const { return ___mMenuAnim_6; }
	inline MenuAnimator_t2112910832 ** get_address_of_mMenuAnim_6() { return &___mMenuAnim_6; }
	inline void set_mMenuAnim_6(MenuAnimator_t2112910832 * value)
	{
		___mMenuAnim_6 = value;
		Il2CppCodeGenWriteBarrier((&___mMenuAnim_6), value);
	}

	inline static int32_t get_offset_of_mTapCount_7() { return static_cast<int32_t>(offsetof(TapHandler_t334234343, ___mTapCount_7)); }
	inline int32_t get_mTapCount_7() const { return ___mTapCount_7; }
	inline int32_t* get_address_of_mTapCount_7() { return &___mTapCount_7; }
	inline void set_mTapCount_7(int32_t value)
	{
		___mTapCount_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TAPHANDLER_T334234343_H
#ifndef TRACKABLESETTINGS_T2862243993_H
#define TRACKABLESETTINGS_T2862243993_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TrackableSettings
struct  TrackableSettings_t2862243993  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean TrackableSettings::mExtTrackingEnabled
	bool ___mExtTrackingEnabled_4;

public:
	inline static int32_t get_offset_of_mExtTrackingEnabled_4() { return static_cast<int32_t>(offsetof(TrackableSettings_t2862243993, ___mExtTrackingEnabled_4)); }
	inline bool get_mExtTrackingEnabled_4() const { return ___mExtTrackingEnabled_4; }
	inline bool* get_address_of_mExtTrackingEnabled_4() { return &___mExtTrackingEnabled_4; }
	inline void set_mExtTrackingEnabled_4(bool value)
	{
		___mExtTrackingEnabled_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKABLESETTINGS_T2862243993_H
#ifndef VUFORIAMONOBEHAVIOUR_T1150221792_H
#define VUFORIAMONOBEHAVIOUR_T1150221792_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VuforiaMonoBehaviour
struct  VuforiaMonoBehaviour_t1150221792  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VUFORIAMONOBEHAVIOUR_T1150221792_H
#ifndef ANIMATIONIMAGE_T1423068421_H
#define ANIMATIONIMAGE_T1423068421_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// animationImage
struct  animationImage_t1423068421  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Texture2D animationImage::tex
	Texture2D_t3840446185 * ___tex_4;
	// UnityEngine.Sprite animationImage::mySprite
	Sprite_t280657092 * ___mySprite_5;
	// UnityEngine.Sprite animationImage::mySpriteResult
	Sprite_t280657092 * ___mySpriteResult_6;
	// UnityEngine.SpriteRenderer animationImage::sr
	SpriteRenderer_t3235626157 * ___sr_7;
	// UnityEngine.SpriteRenderer animationImage::srResult
	SpriteRenderer_t3235626157 * ___srResult_8;
	// UnityEngine.GameObject animationImage::newSpriteObj
	GameObject_t1113636619 * ___newSpriteObj_9;
	// UnityEngine.GameObject animationImage::newSpriteResult
	GameObject_t1113636619 * ___newSpriteResult_10;
	// UnityEngine.Color animationImage::C
	Color_t2555686324  ___C_11;
	// System.Single animationImage::pixelByUnity
	float ___pixelByUnity_12;
	// System.Collections.Generic.List`1<Pixel> animationImage::listePixels
	List_1_t4186031580 * ___listePixels_13;
	// System.Collections.Generic.List`1<System.Collections.Generic.List`1<Pixel>> animationImage::listeColPixels
	List_1_t1363139026 * ___listeColPixels_14;
	// System.Int32 animationImage::ChoixAnimation
	int32_t ___ChoixAnimation_15;
	// System.Boolean animationImage::animationFinie
	bool ___animationFinie_16;
	// System.Boolean animationImage::affichageFini
	bool ___affichageFini_17;
	// System.Int32 animationImage::numPix
	int32_t ___numPix_18;
	// System.Int32 animationImage::nbPix
	int32_t ___nbPix_19;
	// System.Int32 animationImage::nbAfficher
	int32_t ___nbAfficher_20;

public:
	inline static int32_t get_offset_of_tex_4() { return static_cast<int32_t>(offsetof(animationImage_t1423068421, ___tex_4)); }
	inline Texture2D_t3840446185 * get_tex_4() const { return ___tex_4; }
	inline Texture2D_t3840446185 ** get_address_of_tex_4() { return &___tex_4; }
	inline void set_tex_4(Texture2D_t3840446185 * value)
	{
		___tex_4 = value;
		Il2CppCodeGenWriteBarrier((&___tex_4), value);
	}

	inline static int32_t get_offset_of_mySprite_5() { return static_cast<int32_t>(offsetof(animationImage_t1423068421, ___mySprite_5)); }
	inline Sprite_t280657092 * get_mySprite_5() const { return ___mySprite_5; }
	inline Sprite_t280657092 ** get_address_of_mySprite_5() { return &___mySprite_5; }
	inline void set_mySprite_5(Sprite_t280657092 * value)
	{
		___mySprite_5 = value;
		Il2CppCodeGenWriteBarrier((&___mySprite_5), value);
	}

	inline static int32_t get_offset_of_mySpriteResult_6() { return static_cast<int32_t>(offsetof(animationImage_t1423068421, ___mySpriteResult_6)); }
	inline Sprite_t280657092 * get_mySpriteResult_6() const { return ___mySpriteResult_6; }
	inline Sprite_t280657092 ** get_address_of_mySpriteResult_6() { return &___mySpriteResult_6; }
	inline void set_mySpriteResult_6(Sprite_t280657092 * value)
	{
		___mySpriteResult_6 = value;
		Il2CppCodeGenWriteBarrier((&___mySpriteResult_6), value);
	}

	inline static int32_t get_offset_of_sr_7() { return static_cast<int32_t>(offsetof(animationImage_t1423068421, ___sr_7)); }
	inline SpriteRenderer_t3235626157 * get_sr_7() const { return ___sr_7; }
	inline SpriteRenderer_t3235626157 ** get_address_of_sr_7() { return &___sr_7; }
	inline void set_sr_7(SpriteRenderer_t3235626157 * value)
	{
		___sr_7 = value;
		Il2CppCodeGenWriteBarrier((&___sr_7), value);
	}

	inline static int32_t get_offset_of_srResult_8() { return static_cast<int32_t>(offsetof(animationImage_t1423068421, ___srResult_8)); }
	inline SpriteRenderer_t3235626157 * get_srResult_8() const { return ___srResult_8; }
	inline SpriteRenderer_t3235626157 ** get_address_of_srResult_8() { return &___srResult_8; }
	inline void set_srResult_8(SpriteRenderer_t3235626157 * value)
	{
		___srResult_8 = value;
		Il2CppCodeGenWriteBarrier((&___srResult_8), value);
	}

	inline static int32_t get_offset_of_newSpriteObj_9() { return static_cast<int32_t>(offsetof(animationImage_t1423068421, ___newSpriteObj_9)); }
	inline GameObject_t1113636619 * get_newSpriteObj_9() const { return ___newSpriteObj_9; }
	inline GameObject_t1113636619 ** get_address_of_newSpriteObj_9() { return &___newSpriteObj_9; }
	inline void set_newSpriteObj_9(GameObject_t1113636619 * value)
	{
		___newSpriteObj_9 = value;
		Il2CppCodeGenWriteBarrier((&___newSpriteObj_9), value);
	}

	inline static int32_t get_offset_of_newSpriteResult_10() { return static_cast<int32_t>(offsetof(animationImage_t1423068421, ___newSpriteResult_10)); }
	inline GameObject_t1113636619 * get_newSpriteResult_10() const { return ___newSpriteResult_10; }
	inline GameObject_t1113636619 ** get_address_of_newSpriteResult_10() { return &___newSpriteResult_10; }
	inline void set_newSpriteResult_10(GameObject_t1113636619 * value)
	{
		___newSpriteResult_10 = value;
		Il2CppCodeGenWriteBarrier((&___newSpriteResult_10), value);
	}

	inline static int32_t get_offset_of_C_11() { return static_cast<int32_t>(offsetof(animationImage_t1423068421, ___C_11)); }
	inline Color_t2555686324  get_C_11() const { return ___C_11; }
	inline Color_t2555686324 * get_address_of_C_11() { return &___C_11; }
	inline void set_C_11(Color_t2555686324  value)
	{
		___C_11 = value;
	}

	inline static int32_t get_offset_of_pixelByUnity_12() { return static_cast<int32_t>(offsetof(animationImage_t1423068421, ___pixelByUnity_12)); }
	inline float get_pixelByUnity_12() const { return ___pixelByUnity_12; }
	inline float* get_address_of_pixelByUnity_12() { return &___pixelByUnity_12; }
	inline void set_pixelByUnity_12(float value)
	{
		___pixelByUnity_12 = value;
	}

	inline static int32_t get_offset_of_listePixels_13() { return static_cast<int32_t>(offsetof(animationImage_t1423068421, ___listePixels_13)); }
	inline List_1_t4186031580 * get_listePixels_13() const { return ___listePixels_13; }
	inline List_1_t4186031580 ** get_address_of_listePixels_13() { return &___listePixels_13; }
	inline void set_listePixels_13(List_1_t4186031580 * value)
	{
		___listePixels_13 = value;
		Il2CppCodeGenWriteBarrier((&___listePixels_13), value);
	}

	inline static int32_t get_offset_of_listeColPixels_14() { return static_cast<int32_t>(offsetof(animationImage_t1423068421, ___listeColPixels_14)); }
	inline List_1_t1363139026 * get_listeColPixels_14() const { return ___listeColPixels_14; }
	inline List_1_t1363139026 ** get_address_of_listeColPixels_14() { return &___listeColPixels_14; }
	inline void set_listeColPixels_14(List_1_t1363139026 * value)
	{
		___listeColPixels_14 = value;
		Il2CppCodeGenWriteBarrier((&___listeColPixels_14), value);
	}

	inline static int32_t get_offset_of_ChoixAnimation_15() { return static_cast<int32_t>(offsetof(animationImage_t1423068421, ___ChoixAnimation_15)); }
	inline int32_t get_ChoixAnimation_15() const { return ___ChoixAnimation_15; }
	inline int32_t* get_address_of_ChoixAnimation_15() { return &___ChoixAnimation_15; }
	inline void set_ChoixAnimation_15(int32_t value)
	{
		___ChoixAnimation_15 = value;
	}

	inline static int32_t get_offset_of_animationFinie_16() { return static_cast<int32_t>(offsetof(animationImage_t1423068421, ___animationFinie_16)); }
	inline bool get_animationFinie_16() const { return ___animationFinie_16; }
	inline bool* get_address_of_animationFinie_16() { return &___animationFinie_16; }
	inline void set_animationFinie_16(bool value)
	{
		___animationFinie_16 = value;
	}

	inline static int32_t get_offset_of_affichageFini_17() { return static_cast<int32_t>(offsetof(animationImage_t1423068421, ___affichageFini_17)); }
	inline bool get_affichageFini_17() const { return ___affichageFini_17; }
	inline bool* get_address_of_affichageFini_17() { return &___affichageFini_17; }
	inline void set_affichageFini_17(bool value)
	{
		___affichageFini_17 = value;
	}

	inline static int32_t get_offset_of_numPix_18() { return static_cast<int32_t>(offsetof(animationImage_t1423068421, ___numPix_18)); }
	inline int32_t get_numPix_18() const { return ___numPix_18; }
	inline int32_t* get_address_of_numPix_18() { return &___numPix_18; }
	inline void set_numPix_18(int32_t value)
	{
		___numPix_18 = value;
	}

	inline static int32_t get_offset_of_nbPix_19() { return static_cast<int32_t>(offsetof(animationImage_t1423068421, ___nbPix_19)); }
	inline int32_t get_nbPix_19() const { return ___nbPix_19; }
	inline int32_t* get_address_of_nbPix_19() { return &___nbPix_19; }
	inline void set_nbPix_19(int32_t value)
	{
		___nbPix_19 = value;
	}

	inline static int32_t get_offset_of_nbAfficher_20() { return static_cast<int32_t>(offsetof(animationImage_t1423068421, ___nbAfficher_20)); }
	inline int32_t get_nbAfficher_20() const { return ___nbAfficher_20; }
	inline int32_t* get_address_of_nbAfficher_20() { return &___nbAfficher_20; }
	inline void set_nbAfficher_20(int32_t value)
	{
		___nbAfficher_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATIONIMAGE_T1423068421_H
#ifndef TRACKABLEBEHAVIOUR_T1113559212_H
#define TRACKABLEBEHAVIOUR_T1113559212_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.TrackableBehaviour
struct  TrackableBehaviour_t1113559212  : public VuforiaMonoBehaviour_t1150221792
{
public:
	// System.Double Vuforia.TrackableBehaviour::<TimeStamp>k__BackingField
	double ___U3CTimeStampU3Ek__BackingField_4;
	// System.String Vuforia.TrackableBehaviour::mTrackableName
	String_t* ___mTrackableName_5;
	// System.Boolean Vuforia.TrackableBehaviour::mPreserveChildSize
	bool ___mPreserveChildSize_6;
	// System.Boolean Vuforia.TrackableBehaviour::mInitializedInEditor
	bool ___mInitializedInEditor_7;
	// UnityEngine.Vector3 Vuforia.TrackableBehaviour::mPreviousScale
	Vector3_t3722313464  ___mPreviousScale_8;
	// Vuforia.TrackableBehaviour/Status Vuforia.TrackableBehaviour::mStatus
	int32_t ___mStatus_9;
	// Vuforia.Trackable Vuforia.TrackableBehaviour::mTrackable
	RuntimeObject* ___mTrackable_10;
	// System.Collections.Generic.List`1<Vuforia.ITrackableEventHandler> Vuforia.TrackableBehaviour::mTrackableEventHandlers
	List_1_t2968050330 * ___mTrackableEventHandlers_11;

public:
	inline static int32_t get_offset_of_U3CTimeStampU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t1113559212, ___U3CTimeStampU3Ek__BackingField_4)); }
	inline double get_U3CTimeStampU3Ek__BackingField_4() const { return ___U3CTimeStampU3Ek__BackingField_4; }
	inline double* get_address_of_U3CTimeStampU3Ek__BackingField_4() { return &___U3CTimeStampU3Ek__BackingField_4; }
	inline void set_U3CTimeStampU3Ek__BackingField_4(double value)
	{
		___U3CTimeStampU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_mTrackableName_5() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t1113559212, ___mTrackableName_5)); }
	inline String_t* get_mTrackableName_5() const { return ___mTrackableName_5; }
	inline String_t** get_address_of_mTrackableName_5() { return &___mTrackableName_5; }
	inline void set_mTrackableName_5(String_t* value)
	{
		___mTrackableName_5 = value;
		Il2CppCodeGenWriteBarrier((&___mTrackableName_5), value);
	}

	inline static int32_t get_offset_of_mPreserveChildSize_6() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t1113559212, ___mPreserveChildSize_6)); }
	inline bool get_mPreserveChildSize_6() const { return ___mPreserveChildSize_6; }
	inline bool* get_address_of_mPreserveChildSize_6() { return &___mPreserveChildSize_6; }
	inline void set_mPreserveChildSize_6(bool value)
	{
		___mPreserveChildSize_6 = value;
	}

	inline static int32_t get_offset_of_mInitializedInEditor_7() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t1113559212, ___mInitializedInEditor_7)); }
	inline bool get_mInitializedInEditor_7() const { return ___mInitializedInEditor_7; }
	inline bool* get_address_of_mInitializedInEditor_7() { return &___mInitializedInEditor_7; }
	inline void set_mInitializedInEditor_7(bool value)
	{
		___mInitializedInEditor_7 = value;
	}

	inline static int32_t get_offset_of_mPreviousScale_8() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t1113559212, ___mPreviousScale_8)); }
	inline Vector3_t3722313464  get_mPreviousScale_8() const { return ___mPreviousScale_8; }
	inline Vector3_t3722313464 * get_address_of_mPreviousScale_8() { return &___mPreviousScale_8; }
	inline void set_mPreviousScale_8(Vector3_t3722313464  value)
	{
		___mPreviousScale_8 = value;
	}

	inline static int32_t get_offset_of_mStatus_9() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t1113559212, ___mStatus_9)); }
	inline int32_t get_mStatus_9() const { return ___mStatus_9; }
	inline int32_t* get_address_of_mStatus_9() { return &___mStatus_9; }
	inline void set_mStatus_9(int32_t value)
	{
		___mStatus_9 = value;
	}

	inline static int32_t get_offset_of_mTrackable_10() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t1113559212, ___mTrackable_10)); }
	inline RuntimeObject* get_mTrackable_10() const { return ___mTrackable_10; }
	inline RuntimeObject** get_address_of_mTrackable_10() { return &___mTrackable_10; }
	inline void set_mTrackable_10(RuntimeObject* value)
	{
		___mTrackable_10 = value;
		Il2CppCodeGenWriteBarrier((&___mTrackable_10), value);
	}

	inline static int32_t get_offset_of_mTrackableEventHandlers_11() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t1113559212, ___mTrackableEventHandlers_11)); }
	inline List_1_t2968050330 * get_mTrackableEventHandlers_11() const { return ___mTrackableEventHandlers_11; }
	inline List_1_t2968050330 ** get_address_of_mTrackableEventHandlers_11() { return &___mTrackableEventHandlers_11; }
	inline void set_mTrackableEventHandlers_11(List_1_t2968050330 * value)
	{
		___mTrackableEventHandlers_11 = value;
		Il2CppCodeGenWriteBarrier((&___mTrackableEventHandlers_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKABLEBEHAVIOUR_T1113559212_H
#ifndef USERDEFINEDTARGETBUILDINGBEHAVIOUR_T4262637471_H
#define USERDEFINEDTARGETBUILDINGBEHAVIOUR_T4262637471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.UserDefinedTargetBuildingBehaviour
struct  UserDefinedTargetBuildingBehaviour_t4262637471  : public VuforiaMonoBehaviour_t1150221792
{
public:
	// Vuforia.ObjectTracker Vuforia.UserDefinedTargetBuildingBehaviour::mObjectTracker
	ObjectTracker_t4177997237 * ___mObjectTracker_4;
	// Vuforia.ImageTargetBuilder/FrameQuality Vuforia.UserDefinedTargetBuildingBehaviour::mLastFrameQuality
	int32_t ___mLastFrameQuality_5;
	// System.Boolean Vuforia.UserDefinedTargetBuildingBehaviour::mCurrentlyScanning
	bool ___mCurrentlyScanning_6;
	// System.Boolean Vuforia.UserDefinedTargetBuildingBehaviour::mWasScanningBeforeDisable
	bool ___mWasScanningBeforeDisable_7;
	// System.Boolean Vuforia.UserDefinedTargetBuildingBehaviour::mCurrentlyBuilding
	bool ___mCurrentlyBuilding_8;
	// System.Boolean Vuforia.UserDefinedTargetBuildingBehaviour::mWasBuildingBeforeDisable
	bool ___mWasBuildingBeforeDisable_9;
	// System.Boolean Vuforia.UserDefinedTargetBuildingBehaviour::mOnInitializedCalled
	bool ___mOnInitializedCalled_10;
	// System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler> Vuforia.UserDefinedTargetBuildingBehaviour::mHandlers
	List_1_t2728888017 * ___mHandlers_11;
	// System.Boolean Vuforia.UserDefinedTargetBuildingBehaviour::StopTrackerWhileScanning
	bool ___StopTrackerWhileScanning_12;
	// System.Boolean Vuforia.UserDefinedTargetBuildingBehaviour::StartScanningAutomatically
	bool ___StartScanningAutomatically_13;
	// System.Boolean Vuforia.UserDefinedTargetBuildingBehaviour::StopScanningWhenFinshedBuilding
	bool ___StopScanningWhenFinshedBuilding_14;

public:
	inline static int32_t get_offset_of_mObjectTracker_4() { return static_cast<int32_t>(offsetof(UserDefinedTargetBuildingBehaviour_t4262637471, ___mObjectTracker_4)); }
	inline ObjectTracker_t4177997237 * get_mObjectTracker_4() const { return ___mObjectTracker_4; }
	inline ObjectTracker_t4177997237 ** get_address_of_mObjectTracker_4() { return &___mObjectTracker_4; }
	inline void set_mObjectTracker_4(ObjectTracker_t4177997237 * value)
	{
		___mObjectTracker_4 = value;
		Il2CppCodeGenWriteBarrier((&___mObjectTracker_4), value);
	}

	inline static int32_t get_offset_of_mLastFrameQuality_5() { return static_cast<int32_t>(offsetof(UserDefinedTargetBuildingBehaviour_t4262637471, ___mLastFrameQuality_5)); }
	inline int32_t get_mLastFrameQuality_5() const { return ___mLastFrameQuality_5; }
	inline int32_t* get_address_of_mLastFrameQuality_5() { return &___mLastFrameQuality_5; }
	inline void set_mLastFrameQuality_5(int32_t value)
	{
		___mLastFrameQuality_5 = value;
	}

	inline static int32_t get_offset_of_mCurrentlyScanning_6() { return static_cast<int32_t>(offsetof(UserDefinedTargetBuildingBehaviour_t4262637471, ___mCurrentlyScanning_6)); }
	inline bool get_mCurrentlyScanning_6() const { return ___mCurrentlyScanning_6; }
	inline bool* get_address_of_mCurrentlyScanning_6() { return &___mCurrentlyScanning_6; }
	inline void set_mCurrentlyScanning_6(bool value)
	{
		___mCurrentlyScanning_6 = value;
	}

	inline static int32_t get_offset_of_mWasScanningBeforeDisable_7() { return static_cast<int32_t>(offsetof(UserDefinedTargetBuildingBehaviour_t4262637471, ___mWasScanningBeforeDisable_7)); }
	inline bool get_mWasScanningBeforeDisable_7() const { return ___mWasScanningBeforeDisable_7; }
	inline bool* get_address_of_mWasScanningBeforeDisable_7() { return &___mWasScanningBeforeDisable_7; }
	inline void set_mWasScanningBeforeDisable_7(bool value)
	{
		___mWasScanningBeforeDisable_7 = value;
	}

	inline static int32_t get_offset_of_mCurrentlyBuilding_8() { return static_cast<int32_t>(offsetof(UserDefinedTargetBuildingBehaviour_t4262637471, ___mCurrentlyBuilding_8)); }
	inline bool get_mCurrentlyBuilding_8() const { return ___mCurrentlyBuilding_8; }
	inline bool* get_address_of_mCurrentlyBuilding_8() { return &___mCurrentlyBuilding_8; }
	inline void set_mCurrentlyBuilding_8(bool value)
	{
		___mCurrentlyBuilding_8 = value;
	}

	inline static int32_t get_offset_of_mWasBuildingBeforeDisable_9() { return static_cast<int32_t>(offsetof(UserDefinedTargetBuildingBehaviour_t4262637471, ___mWasBuildingBeforeDisable_9)); }
	inline bool get_mWasBuildingBeforeDisable_9() const { return ___mWasBuildingBeforeDisable_9; }
	inline bool* get_address_of_mWasBuildingBeforeDisable_9() { return &___mWasBuildingBeforeDisable_9; }
	inline void set_mWasBuildingBeforeDisable_9(bool value)
	{
		___mWasBuildingBeforeDisable_9 = value;
	}

	inline static int32_t get_offset_of_mOnInitializedCalled_10() { return static_cast<int32_t>(offsetof(UserDefinedTargetBuildingBehaviour_t4262637471, ___mOnInitializedCalled_10)); }
	inline bool get_mOnInitializedCalled_10() const { return ___mOnInitializedCalled_10; }
	inline bool* get_address_of_mOnInitializedCalled_10() { return &___mOnInitializedCalled_10; }
	inline void set_mOnInitializedCalled_10(bool value)
	{
		___mOnInitializedCalled_10 = value;
	}

	inline static int32_t get_offset_of_mHandlers_11() { return static_cast<int32_t>(offsetof(UserDefinedTargetBuildingBehaviour_t4262637471, ___mHandlers_11)); }
	inline List_1_t2728888017 * get_mHandlers_11() const { return ___mHandlers_11; }
	inline List_1_t2728888017 ** get_address_of_mHandlers_11() { return &___mHandlers_11; }
	inline void set_mHandlers_11(List_1_t2728888017 * value)
	{
		___mHandlers_11 = value;
		Il2CppCodeGenWriteBarrier((&___mHandlers_11), value);
	}

	inline static int32_t get_offset_of_StopTrackerWhileScanning_12() { return static_cast<int32_t>(offsetof(UserDefinedTargetBuildingBehaviour_t4262637471, ___StopTrackerWhileScanning_12)); }
	inline bool get_StopTrackerWhileScanning_12() const { return ___StopTrackerWhileScanning_12; }
	inline bool* get_address_of_StopTrackerWhileScanning_12() { return &___StopTrackerWhileScanning_12; }
	inline void set_StopTrackerWhileScanning_12(bool value)
	{
		___StopTrackerWhileScanning_12 = value;
	}

	inline static int32_t get_offset_of_StartScanningAutomatically_13() { return static_cast<int32_t>(offsetof(UserDefinedTargetBuildingBehaviour_t4262637471, ___StartScanningAutomatically_13)); }
	inline bool get_StartScanningAutomatically_13() const { return ___StartScanningAutomatically_13; }
	inline bool* get_address_of_StartScanningAutomatically_13() { return &___StartScanningAutomatically_13; }
	inline void set_StartScanningAutomatically_13(bool value)
	{
		___StartScanningAutomatically_13 = value;
	}

	inline static int32_t get_offset_of_StopScanningWhenFinshedBuilding_14() { return static_cast<int32_t>(offsetof(UserDefinedTargetBuildingBehaviour_t4262637471, ___StopScanningWhenFinshedBuilding_14)); }
	inline bool get_StopScanningWhenFinshedBuilding_14() const { return ___StopScanningWhenFinshedBuilding_14; }
	inline bool* get_address_of_StopScanningWhenFinshedBuilding_14() { return &___StopScanningWhenFinshedBuilding_14; }
	inline void set_StopScanningWhenFinshedBuilding_14(bool value)
	{
		___StopScanningWhenFinshedBuilding_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // USERDEFINEDTARGETBUILDINGBEHAVIOUR_T4262637471_H
#ifndef VIDEOBACKGROUNDBEHAVIOUR_T1552899074_H
#define VIDEOBACKGROUNDBEHAVIOUR_T1552899074_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VideoBackgroundBehaviour
struct  VideoBackgroundBehaviour_t1552899074  : public VuforiaMonoBehaviour_t1150221792
{
public:
	// System.Int32 Vuforia.VideoBackgroundBehaviour::mClearBuffers
	int32_t ___mClearBuffers_4;
	// System.Int32 Vuforia.VideoBackgroundBehaviour::mSkipStateUpdates
	int32_t ___mSkipStateUpdates_5;
	// Vuforia.VuforiaARController Vuforia.VideoBackgroundBehaviour::mVuforiaARController
	VuforiaARController_t1876945237 * ___mVuforiaARController_6;
	// UnityEngine.Camera Vuforia.VideoBackgroundBehaviour::mCamera
	Camera_t4157153871 * ___mCamera_7;
	// Vuforia.BackgroundPlaneBehaviour Vuforia.VideoBackgroundBehaviour::mBackgroundBehaviour
	BackgroundPlaneBehaviour_t3333547397 * ___mBackgroundBehaviour_8;
	// System.Collections.Generic.HashSet`1<UnityEngine.MeshRenderer> Vuforia.VideoBackgroundBehaviour::mDisabledMeshRenderers
	HashSet_1_t3446926030 * ___mDisabledMeshRenderers_11;

public:
	inline static int32_t get_offset_of_mClearBuffers_4() { return static_cast<int32_t>(offsetof(VideoBackgroundBehaviour_t1552899074, ___mClearBuffers_4)); }
	inline int32_t get_mClearBuffers_4() const { return ___mClearBuffers_4; }
	inline int32_t* get_address_of_mClearBuffers_4() { return &___mClearBuffers_4; }
	inline void set_mClearBuffers_4(int32_t value)
	{
		___mClearBuffers_4 = value;
	}

	inline static int32_t get_offset_of_mSkipStateUpdates_5() { return static_cast<int32_t>(offsetof(VideoBackgroundBehaviour_t1552899074, ___mSkipStateUpdates_5)); }
	inline int32_t get_mSkipStateUpdates_5() const { return ___mSkipStateUpdates_5; }
	inline int32_t* get_address_of_mSkipStateUpdates_5() { return &___mSkipStateUpdates_5; }
	inline void set_mSkipStateUpdates_5(int32_t value)
	{
		___mSkipStateUpdates_5 = value;
	}

	inline static int32_t get_offset_of_mVuforiaARController_6() { return static_cast<int32_t>(offsetof(VideoBackgroundBehaviour_t1552899074, ___mVuforiaARController_6)); }
	inline VuforiaARController_t1876945237 * get_mVuforiaARController_6() const { return ___mVuforiaARController_6; }
	inline VuforiaARController_t1876945237 ** get_address_of_mVuforiaARController_6() { return &___mVuforiaARController_6; }
	inline void set_mVuforiaARController_6(VuforiaARController_t1876945237 * value)
	{
		___mVuforiaARController_6 = value;
		Il2CppCodeGenWriteBarrier((&___mVuforiaARController_6), value);
	}

	inline static int32_t get_offset_of_mCamera_7() { return static_cast<int32_t>(offsetof(VideoBackgroundBehaviour_t1552899074, ___mCamera_7)); }
	inline Camera_t4157153871 * get_mCamera_7() const { return ___mCamera_7; }
	inline Camera_t4157153871 ** get_address_of_mCamera_7() { return &___mCamera_7; }
	inline void set_mCamera_7(Camera_t4157153871 * value)
	{
		___mCamera_7 = value;
		Il2CppCodeGenWriteBarrier((&___mCamera_7), value);
	}

	inline static int32_t get_offset_of_mBackgroundBehaviour_8() { return static_cast<int32_t>(offsetof(VideoBackgroundBehaviour_t1552899074, ___mBackgroundBehaviour_8)); }
	inline BackgroundPlaneBehaviour_t3333547397 * get_mBackgroundBehaviour_8() const { return ___mBackgroundBehaviour_8; }
	inline BackgroundPlaneBehaviour_t3333547397 ** get_address_of_mBackgroundBehaviour_8() { return &___mBackgroundBehaviour_8; }
	inline void set_mBackgroundBehaviour_8(BackgroundPlaneBehaviour_t3333547397 * value)
	{
		___mBackgroundBehaviour_8 = value;
		Il2CppCodeGenWriteBarrier((&___mBackgroundBehaviour_8), value);
	}

	inline static int32_t get_offset_of_mDisabledMeshRenderers_11() { return static_cast<int32_t>(offsetof(VideoBackgroundBehaviour_t1552899074, ___mDisabledMeshRenderers_11)); }
	inline HashSet_1_t3446926030 * get_mDisabledMeshRenderers_11() const { return ___mDisabledMeshRenderers_11; }
	inline HashSet_1_t3446926030 ** get_address_of_mDisabledMeshRenderers_11() { return &___mDisabledMeshRenderers_11; }
	inline void set_mDisabledMeshRenderers_11(HashSet_1_t3446926030 * value)
	{
		___mDisabledMeshRenderers_11 = value;
		Il2CppCodeGenWriteBarrier((&___mDisabledMeshRenderers_11), value);
	}
};

struct VideoBackgroundBehaviour_t1552899074_StaticFields
{
public:
	// System.Int32 Vuforia.VideoBackgroundBehaviour::mFrameCounter
	int32_t ___mFrameCounter_9;
	// System.Int32 Vuforia.VideoBackgroundBehaviour::mRenderCounter
	int32_t ___mRenderCounter_10;

public:
	inline static int32_t get_offset_of_mFrameCounter_9() { return static_cast<int32_t>(offsetof(VideoBackgroundBehaviour_t1552899074_StaticFields, ___mFrameCounter_9)); }
	inline int32_t get_mFrameCounter_9() const { return ___mFrameCounter_9; }
	inline int32_t* get_address_of_mFrameCounter_9() { return &___mFrameCounter_9; }
	inline void set_mFrameCounter_9(int32_t value)
	{
		___mFrameCounter_9 = value;
	}

	inline static int32_t get_offset_of_mRenderCounter_10() { return static_cast<int32_t>(offsetof(VideoBackgroundBehaviour_t1552899074_StaticFields, ___mRenderCounter_10)); }
	inline int32_t get_mRenderCounter_10() const { return ___mRenderCounter_10; }
	inline int32_t* get_address_of_mRenderCounter_10() { return &___mRenderCounter_10; }
	inline void set_mRenderCounter_10(int32_t value)
	{
		___mRenderCounter_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIDEOBACKGROUNDBEHAVIOUR_T1552899074_H
#ifndef VIRTUALBUTTONBEHAVIOUR_T1436326451_H
#define VIRTUALBUTTONBEHAVIOUR_T1436326451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VirtualButtonBehaviour
struct  VirtualButtonBehaviour_t1436326451  : public VuforiaMonoBehaviour_t1150221792
{
public:
	// System.String Vuforia.VirtualButtonBehaviour::mName
	String_t* ___mName_5;
	// Vuforia.VirtualButton/Sensitivity Vuforia.VirtualButtonBehaviour::mSensitivity
	int32_t ___mSensitivity_6;
	// System.Boolean Vuforia.VirtualButtonBehaviour::mHasUpdatedPose
	bool ___mHasUpdatedPose_7;
	// UnityEngine.Matrix4x4 Vuforia.VirtualButtonBehaviour::mPrevTransform
	Matrix4x4_t1817901843  ___mPrevTransform_8;
	// UnityEngine.GameObject Vuforia.VirtualButtonBehaviour::mPrevParent
	GameObject_t1113636619 * ___mPrevParent_9;
	// System.Boolean Vuforia.VirtualButtonBehaviour::mSensitivityDirty
	bool ___mSensitivityDirty_10;
	// Vuforia.VirtualButton/Sensitivity Vuforia.VirtualButtonBehaviour::mPreviousSensitivity
	int32_t ___mPreviousSensitivity_11;
	// System.Boolean Vuforia.VirtualButtonBehaviour::mPreviouslyEnabled
	bool ___mPreviouslyEnabled_12;
	// System.Boolean Vuforia.VirtualButtonBehaviour::mPressed
	bool ___mPressed_13;
	// System.Collections.Generic.List`1<Vuforia.IVirtualButtonEventHandler> Vuforia.VirtualButtonBehaviour::mHandlers
	List_1_t365750880 * ___mHandlers_14;
	// UnityEngine.Vector2 Vuforia.VirtualButtonBehaviour::mLeftTop
	Vector2_t2156229523  ___mLeftTop_15;
	// UnityEngine.Vector2 Vuforia.VirtualButtonBehaviour::mRightBottom
	Vector2_t2156229523  ___mRightBottom_16;
	// System.Boolean Vuforia.VirtualButtonBehaviour::mUnregisterOnDestroy
	bool ___mUnregisterOnDestroy_17;
	// Vuforia.VirtualButton Vuforia.VirtualButtonBehaviour::mVirtualButton
	VirtualButton_t386166510 * ___mVirtualButton_18;

public:
	inline static int32_t get_offset_of_mName_5() { return static_cast<int32_t>(offsetof(VirtualButtonBehaviour_t1436326451, ___mName_5)); }
	inline String_t* get_mName_5() const { return ___mName_5; }
	inline String_t** get_address_of_mName_5() { return &___mName_5; }
	inline void set_mName_5(String_t* value)
	{
		___mName_5 = value;
		Il2CppCodeGenWriteBarrier((&___mName_5), value);
	}

	inline static int32_t get_offset_of_mSensitivity_6() { return static_cast<int32_t>(offsetof(VirtualButtonBehaviour_t1436326451, ___mSensitivity_6)); }
	inline int32_t get_mSensitivity_6() const { return ___mSensitivity_6; }
	inline int32_t* get_address_of_mSensitivity_6() { return &___mSensitivity_6; }
	inline void set_mSensitivity_6(int32_t value)
	{
		___mSensitivity_6 = value;
	}

	inline static int32_t get_offset_of_mHasUpdatedPose_7() { return static_cast<int32_t>(offsetof(VirtualButtonBehaviour_t1436326451, ___mHasUpdatedPose_7)); }
	inline bool get_mHasUpdatedPose_7() const { return ___mHasUpdatedPose_7; }
	inline bool* get_address_of_mHasUpdatedPose_7() { return &___mHasUpdatedPose_7; }
	inline void set_mHasUpdatedPose_7(bool value)
	{
		___mHasUpdatedPose_7 = value;
	}

	inline static int32_t get_offset_of_mPrevTransform_8() { return static_cast<int32_t>(offsetof(VirtualButtonBehaviour_t1436326451, ___mPrevTransform_8)); }
	inline Matrix4x4_t1817901843  get_mPrevTransform_8() const { return ___mPrevTransform_8; }
	inline Matrix4x4_t1817901843 * get_address_of_mPrevTransform_8() { return &___mPrevTransform_8; }
	inline void set_mPrevTransform_8(Matrix4x4_t1817901843  value)
	{
		___mPrevTransform_8 = value;
	}

	inline static int32_t get_offset_of_mPrevParent_9() { return static_cast<int32_t>(offsetof(VirtualButtonBehaviour_t1436326451, ___mPrevParent_9)); }
	inline GameObject_t1113636619 * get_mPrevParent_9() const { return ___mPrevParent_9; }
	inline GameObject_t1113636619 ** get_address_of_mPrevParent_9() { return &___mPrevParent_9; }
	inline void set_mPrevParent_9(GameObject_t1113636619 * value)
	{
		___mPrevParent_9 = value;
		Il2CppCodeGenWriteBarrier((&___mPrevParent_9), value);
	}

	inline static int32_t get_offset_of_mSensitivityDirty_10() { return static_cast<int32_t>(offsetof(VirtualButtonBehaviour_t1436326451, ___mSensitivityDirty_10)); }
	inline bool get_mSensitivityDirty_10() const { return ___mSensitivityDirty_10; }
	inline bool* get_address_of_mSensitivityDirty_10() { return &___mSensitivityDirty_10; }
	inline void set_mSensitivityDirty_10(bool value)
	{
		___mSensitivityDirty_10 = value;
	}

	inline static int32_t get_offset_of_mPreviousSensitivity_11() { return static_cast<int32_t>(offsetof(VirtualButtonBehaviour_t1436326451, ___mPreviousSensitivity_11)); }
	inline int32_t get_mPreviousSensitivity_11() const { return ___mPreviousSensitivity_11; }
	inline int32_t* get_address_of_mPreviousSensitivity_11() { return &___mPreviousSensitivity_11; }
	inline void set_mPreviousSensitivity_11(int32_t value)
	{
		___mPreviousSensitivity_11 = value;
	}

	inline static int32_t get_offset_of_mPreviouslyEnabled_12() { return static_cast<int32_t>(offsetof(VirtualButtonBehaviour_t1436326451, ___mPreviouslyEnabled_12)); }
	inline bool get_mPreviouslyEnabled_12() const { return ___mPreviouslyEnabled_12; }
	inline bool* get_address_of_mPreviouslyEnabled_12() { return &___mPreviouslyEnabled_12; }
	inline void set_mPreviouslyEnabled_12(bool value)
	{
		___mPreviouslyEnabled_12 = value;
	}

	inline static int32_t get_offset_of_mPressed_13() { return static_cast<int32_t>(offsetof(VirtualButtonBehaviour_t1436326451, ___mPressed_13)); }
	inline bool get_mPressed_13() const { return ___mPressed_13; }
	inline bool* get_address_of_mPressed_13() { return &___mPressed_13; }
	inline void set_mPressed_13(bool value)
	{
		___mPressed_13 = value;
	}

	inline static int32_t get_offset_of_mHandlers_14() { return static_cast<int32_t>(offsetof(VirtualButtonBehaviour_t1436326451, ___mHandlers_14)); }
	inline List_1_t365750880 * get_mHandlers_14() const { return ___mHandlers_14; }
	inline List_1_t365750880 ** get_address_of_mHandlers_14() { return &___mHandlers_14; }
	inline void set_mHandlers_14(List_1_t365750880 * value)
	{
		___mHandlers_14 = value;
		Il2CppCodeGenWriteBarrier((&___mHandlers_14), value);
	}

	inline static int32_t get_offset_of_mLeftTop_15() { return static_cast<int32_t>(offsetof(VirtualButtonBehaviour_t1436326451, ___mLeftTop_15)); }
	inline Vector2_t2156229523  get_mLeftTop_15() const { return ___mLeftTop_15; }
	inline Vector2_t2156229523 * get_address_of_mLeftTop_15() { return &___mLeftTop_15; }
	inline void set_mLeftTop_15(Vector2_t2156229523  value)
	{
		___mLeftTop_15 = value;
	}

	inline static int32_t get_offset_of_mRightBottom_16() { return static_cast<int32_t>(offsetof(VirtualButtonBehaviour_t1436326451, ___mRightBottom_16)); }
	inline Vector2_t2156229523  get_mRightBottom_16() const { return ___mRightBottom_16; }
	inline Vector2_t2156229523 * get_address_of_mRightBottom_16() { return &___mRightBottom_16; }
	inline void set_mRightBottom_16(Vector2_t2156229523  value)
	{
		___mRightBottom_16 = value;
	}

	inline static int32_t get_offset_of_mUnregisterOnDestroy_17() { return static_cast<int32_t>(offsetof(VirtualButtonBehaviour_t1436326451, ___mUnregisterOnDestroy_17)); }
	inline bool get_mUnregisterOnDestroy_17() const { return ___mUnregisterOnDestroy_17; }
	inline bool* get_address_of_mUnregisterOnDestroy_17() { return &___mUnregisterOnDestroy_17; }
	inline void set_mUnregisterOnDestroy_17(bool value)
	{
		___mUnregisterOnDestroy_17 = value;
	}

	inline static int32_t get_offset_of_mVirtualButton_18() { return static_cast<int32_t>(offsetof(VirtualButtonBehaviour_t1436326451, ___mVirtualButton_18)); }
	inline VirtualButton_t386166510 * get_mVirtualButton_18() const { return ___mVirtualButton_18; }
	inline VirtualButton_t386166510 ** get_address_of_mVirtualButton_18() { return &___mVirtualButton_18; }
	inline void set_mVirtualButton_18(VirtualButton_t386166510 * value)
	{
		___mVirtualButton_18 = value;
		Il2CppCodeGenWriteBarrier((&___mVirtualButton_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIRTUALBUTTONBEHAVIOUR_T1436326451_H
#ifndef WIREFRAMEBEHAVIOUR_T1831066704_H
#define WIREFRAMEBEHAVIOUR_T1831066704_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.WireframeBehaviour
struct  WireframeBehaviour_t1831066704  : public VuforiaMonoBehaviour_t1150221792
{
public:
	// UnityEngine.Material Vuforia.WireframeBehaviour::lineMaterial
	Material_t340375123 * ___lineMaterial_4;
	// System.Boolean Vuforia.WireframeBehaviour::ShowLines
	bool ___ShowLines_5;
	// UnityEngine.Color Vuforia.WireframeBehaviour::LineColor
	Color_t2555686324  ___LineColor_6;
	// UnityEngine.Material Vuforia.WireframeBehaviour::mLineMaterial
	Material_t340375123 * ___mLineMaterial_7;

public:
	inline static int32_t get_offset_of_lineMaterial_4() { return static_cast<int32_t>(offsetof(WireframeBehaviour_t1831066704, ___lineMaterial_4)); }
	inline Material_t340375123 * get_lineMaterial_4() const { return ___lineMaterial_4; }
	inline Material_t340375123 ** get_address_of_lineMaterial_4() { return &___lineMaterial_4; }
	inline void set_lineMaterial_4(Material_t340375123 * value)
	{
		___lineMaterial_4 = value;
		Il2CppCodeGenWriteBarrier((&___lineMaterial_4), value);
	}

	inline static int32_t get_offset_of_ShowLines_5() { return static_cast<int32_t>(offsetof(WireframeBehaviour_t1831066704, ___ShowLines_5)); }
	inline bool get_ShowLines_5() const { return ___ShowLines_5; }
	inline bool* get_address_of_ShowLines_5() { return &___ShowLines_5; }
	inline void set_ShowLines_5(bool value)
	{
		___ShowLines_5 = value;
	}

	inline static int32_t get_offset_of_LineColor_6() { return static_cast<int32_t>(offsetof(WireframeBehaviour_t1831066704, ___LineColor_6)); }
	inline Color_t2555686324  get_LineColor_6() const { return ___LineColor_6; }
	inline Color_t2555686324 * get_address_of_LineColor_6() { return &___LineColor_6; }
	inline void set_LineColor_6(Color_t2555686324  value)
	{
		___LineColor_6 = value;
	}

	inline static int32_t get_offset_of_mLineMaterial_7() { return static_cast<int32_t>(offsetof(WireframeBehaviour_t1831066704, ___mLineMaterial_7)); }
	inline Material_t340375123 * get_mLineMaterial_7() const { return ___mLineMaterial_7; }
	inline Material_t340375123 ** get_address_of_mLineMaterial_7() { return &___mLineMaterial_7; }
	inline void set_mLineMaterial_7(Material_t340375123 * value)
	{
		___mLineMaterial_7 = value;
		Il2CppCodeGenWriteBarrier((&___mLineMaterial_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WIREFRAMEBEHAVIOUR_T1831066704_H
#ifndef WIREFRAMETRACKABLEEVENTHANDLER_T2143753312_H
#define WIREFRAMETRACKABLEEVENTHANDLER_T2143753312_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.WireframeTrackableEventHandler
struct  WireframeTrackableEventHandler_t2143753312  : public VuforiaMonoBehaviour_t1150221792
{
public:
	// Vuforia.TrackableBehaviour Vuforia.WireframeTrackableEventHandler::mTrackableBehaviour
	TrackableBehaviour_t1113559212 * ___mTrackableBehaviour_4;

public:
	inline static int32_t get_offset_of_mTrackableBehaviour_4() { return static_cast<int32_t>(offsetof(WireframeTrackableEventHandler_t2143753312, ___mTrackableBehaviour_4)); }
	inline TrackableBehaviour_t1113559212 * get_mTrackableBehaviour_4() const { return ___mTrackableBehaviour_4; }
	inline TrackableBehaviour_t1113559212 ** get_address_of_mTrackableBehaviour_4() { return &___mTrackableBehaviour_4; }
	inline void set_mTrackableBehaviour_4(TrackableBehaviour_t1113559212 * value)
	{
		___mTrackableBehaviour_4 = value;
		Il2CppCodeGenWriteBarrier((&___mTrackableBehaviour_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WIREFRAMETRACKABLEEVENTHANDLER_T2143753312_H
#ifndef DATASETTRACKABLEBEHAVIOUR_T3430730379_H
#define DATASETTRACKABLEBEHAVIOUR_T3430730379_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.DataSetTrackableBehaviour
struct  DataSetTrackableBehaviour_t3430730379  : public TrackableBehaviour_t1113559212
{
public:
	// System.String Vuforia.DataSetTrackableBehaviour::mDataSetPath
	String_t* ___mDataSetPath_12;
	// System.Boolean Vuforia.DataSetTrackableBehaviour::mExtendedTracking
	bool ___mExtendedTracking_13;

public:
	inline static int32_t get_offset_of_mDataSetPath_12() { return static_cast<int32_t>(offsetof(DataSetTrackableBehaviour_t3430730379, ___mDataSetPath_12)); }
	inline String_t* get_mDataSetPath_12() const { return ___mDataSetPath_12; }
	inline String_t** get_address_of_mDataSetPath_12() { return &___mDataSetPath_12; }
	inline void set_mDataSetPath_12(String_t* value)
	{
		___mDataSetPath_12 = value;
		Il2CppCodeGenWriteBarrier((&___mDataSetPath_12), value);
	}

	inline static int32_t get_offset_of_mExtendedTracking_13() { return static_cast<int32_t>(offsetof(DataSetTrackableBehaviour_t3430730379, ___mExtendedTracking_13)); }
	inline bool get_mExtendedTracking_13() const { return ___mExtendedTracking_13; }
	inline bool* get_address_of_mExtendedTracking_13() { return &___mExtendedTracking_13; }
	inline void set_mExtendedTracking_13(bool value)
	{
		___mExtendedTracking_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATASETTRACKABLEBEHAVIOUR_T3430730379_H
#ifndef IMAGETARGETBEHAVIOUR_T2200418350_H
#define IMAGETARGETBEHAVIOUR_T2200418350_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.ImageTargetBehaviour
struct  ImageTargetBehaviour_t2200418350  : public DataSetTrackableBehaviour_t3430730379
{
public:
	// System.Single Vuforia.ImageTargetBehaviour::mAspectRatio
	float ___mAspectRatio_14;
	// Vuforia.ImageTargetType Vuforia.ImageTargetBehaviour::mImageTargetType
	int32_t ___mImageTargetType_15;
	// System.Single Vuforia.ImageTargetBehaviour::mWidth
	float ___mWidth_16;
	// System.Single Vuforia.ImageTargetBehaviour::mHeight
	float ___mHeight_17;
	// Vuforia.ImageTarget Vuforia.ImageTargetBehaviour::mImageTarget
	RuntimeObject* ___mImageTarget_18;
	// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VirtualButtonBehaviour> Vuforia.ImageTargetBehaviour::mVirtualButtonBehaviours
	Dictionary_2_t325039782 * ___mVirtualButtonBehaviours_19;
	// System.Single Vuforia.ImageTargetBehaviour::mLastTransformScale
	float ___mLastTransformScale_20;
	// UnityEngine.Vector2 Vuforia.ImageTargetBehaviour::mLastSize
	Vector2_t2156229523  ___mLastSize_21;

public:
	inline static int32_t get_offset_of_mAspectRatio_14() { return static_cast<int32_t>(offsetof(ImageTargetBehaviour_t2200418350, ___mAspectRatio_14)); }
	inline float get_mAspectRatio_14() const { return ___mAspectRatio_14; }
	inline float* get_address_of_mAspectRatio_14() { return &___mAspectRatio_14; }
	inline void set_mAspectRatio_14(float value)
	{
		___mAspectRatio_14 = value;
	}

	inline static int32_t get_offset_of_mImageTargetType_15() { return static_cast<int32_t>(offsetof(ImageTargetBehaviour_t2200418350, ___mImageTargetType_15)); }
	inline int32_t get_mImageTargetType_15() const { return ___mImageTargetType_15; }
	inline int32_t* get_address_of_mImageTargetType_15() { return &___mImageTargetType_15; }
	inline void set_mImageTargetType_15(int32_t value)
	{
		___mImageTargetType_15 = value;
	}

	inline static int32_t get_offset_of_mWidth_16() { return static_cast<int32_t>(offsetof(ImageTargetBehaviour_t2200418350, ___mWidth_16)); }
	inline float get_mWidth_16() const { return ___mWidth_16; }
	inline float* get_address_of_mWidth_16() { return &___mWidth_16; }
	inline void set_mWidth_16(float value)
	{
		___mWidth_16 = value;
	}

	inline static int32_t get_offset_of_mHeight_17() { return static_cast<int32_t>(offsetof(ImageTargetBehaviour_t2200418350, ___mHeight_17)); }
	inline float get_mHeight_17() const { return ___mHeight_17; }
	inline float* get_address_of_mHeight_17() { return &___mHeight_17; }
	inline void set_mHeight_17(float value)
	{
		___mHeight_17 = value;
	}

	inline static int32_t get_offset_of_mImageTarget_18() { return static_cast<int32_t>(offsetof(ImageTargetBehaviour_t2200418350, ___mImageTarget_18)); }
	inline RuntimeObject* get_mImageTarget_18() const { return ___mImageTarget_18; }
	inline RuntimeObject** get_address_of_mImageTarget_18() { return &___mImageTarget_18; }
	inline void set_mImageTarget_18(RuntimeObject* value)
	{
		___mImageTarget_18 = value;
		Il2CppCodeGenWriteBarrier((&___mImageTarget_18), value);
	}

	inline static int32_t get_offset_of_mVirtualButtonBehaviours_19() { return static_cast<int32_t>(offsetof(ImageTargetBehaviour_t2200418350, ___mVirtualButtonBehaviours_19)); }
	inline Dictionary_2_t325039782 * get_mVirtualButtonBehaviours_19() const { return ___mVirtualButtonBehaviours_19; }
	inline Dictionary_2_t325039782 ** get_address_of_mVirtualButtonBehaviours_19() { return &___mVirtualButtonBehaviours_19; }
	inline void set_mVirtualButtonBehaviours_19(Dictionary_2_t325039782 * value)
	{
		___mVirtualButtonBehaviours_19 = value;
		Il2CppCodeGenWriteBarrier((&___mVirtualButtonBehaviours_19), value);
	}

	inline static int32_t get_offset_of_mLastTransformScale_20() { return static_cast<int32_t>(offsetof(ImageTargetBehaviour_t2200418350, ___mLastTransformScale_20)); }
	inline float get_mLastTransformScale_20() const { return ___mLastTransformScale_20; }
	inline float* get_address_of_mLastTransformScale_20() { return &___mLastTransformScale_20; }
	inline void set_mLastTransformScale_20(float value)
	{
		___mLastTransformScale_20 = value;
	}

	inline static int32_t get_offset_of_mLastSize_21() { return static_cast<int32_t>(offsetof(ImageTargetBehaviour_t2200418350, ___mLastSize_21)); }
	inline Vector2_t2156229523  get_mLastSize_21() const { return ___mLastSize_21; }
	inline Vector2_t2156229523 * get_address_of_mLastSize_21() { return &___mLastSize_21; }
	inline void set_mLastSize_21(Vector2_t2156229523  value)
	{
		___mLastSize_21 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMAGETARGETBEHAVIOUR_T2200418350_H
#ifndef MULTITARGETBEHAVIOUR_T2061511750_H
#define MULTITARGETBEHAVIOUR_T2061511750_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.MultiTargetBehaviour
struct  MultiTargetBehaviour_t2061511750  : public DataSetTrackableBehaviour_t3430730379
{
public:
	// Vuforia.MultiTarget Vuforia.MultiTargetBehaviour::mMultiTarget
	RuntimeObject* ___mMultiTarget_14;

public:
	inline static int32_t get_offset_of_mMultiTarget_14() { return static_cast<int32_t>(offsetof(MultiTargetBehaviour_t2061511750, ___mMultiTarget_14)); }
	inline RuntimeObject* get_mMultiTarget_14() const { return ___mMultiTarget_14; }
	inline RuntimeObject** get_address_of_mMultiTarget_14() { return &___mMultiTarget_14; }
	inline void set_mMultiTarget_14(RuntimeObject* value)
	{
		___mMultiTarget_14 = value;
		Il2CppCodeGenWriteBarrier((&___mMultiTarget_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTITARGETBEHAVIOUR_T2061511750_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2800 = { sizeof (ProfileData_t3519391925)+ sizeof (RuntimeObject), sizeof(ProfileData_t3519391925 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2800[3] = 
{
	ProfileData_t3519391925::get_offset_of_RequestedTextureSize_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ProfileData_t3519391925::get_offset_of_ResampledTextureSize_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ProfileData_t3519391925::get_offset_of_RequestedFPS_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2801 = { sizeof (ProfileCollection_t901995765)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2801[2] = 
{
	ProfileCollection_t901995765::get_offset_of_DefaultProfile_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ProfileCollection_t901995765::get_offset_of_Profiles_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2802 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2803 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2804 = { sizeof (Image_t745056343), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2804[10] = 
{
	Image_t745056343::get_offset_of_mWidth_0(),
	Image_t745056343::get_offset_of_mHeight_1(),
	Image_t745056343::get_offset_of_mStride_2(),
	Image_t745056343::get_offset_of_mBufferWidth_3(),
	Image_t745056343::get_offset_of_mBufferHeight_4(),
	Image_t745056343::get_offset_of_mPixelFormat_5(),
	Image_t745056343::get_offset_of_mData_6(),
	Image_t745056343::get_offset_of_mUnmanagedData_7(),
	Image_t745056343::get_offset_of_mDataSet_8(),
	Image_t745056343::get_offset_of_mPixel32_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2805 = { sizeof (PIXEL_FORMAT_t3209881435)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2805[7] = 
{
	PIXEL_FORMAT_t3209881435::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2806 = { sizeof (ImageTargetBehaviour_t2200418350), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2806[8] = 
{
	ImageTargetBehaviour_t2200418350::get_offset_of_mAspectRatio_14(),
	ImageTargetBehaviour_t2200418350::get_offset_of_mImageTargetType_15(),
	ImageTargetBehaviour_t2200418350::get_offset_of_mWidth_16(),
	ImageTargetBehaviour_t2200418350::get_offset_of_mHeight_17(),
	ImageTargetBehaviour_t2200418350::get_offset_of_mImageTarget_18(),
	ImageTargetBehaviour_t2200418350::get_offset_of_mVirtualButtonBehaviours_19(),
	ImageTargetBehaviour_t2200418350::get_offset_of_mLastTransformScale_20(),
	ImageTargetBehaviour_t2200418350::get_offset_of_mLastSize_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2807 = { sizeof (ObjectTracker_t4177997237), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2807[4] = 
{
	ObjectTracker_t4177997237::get_offset_of_mActiveDataSets_1(),
	ObjectTracker_t4177997237::get_offset_of_mDataSets_2(),
	ObjectTracker_t4177997237::get_offset_of_mImageTargetBuilder_3(),
	ObjectTracker_t4177997237::get_offset_of_mTargetFinder_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2808 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2809 = { sizeof (MultiTargetBehaviour_t2061511750), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2809[1] = 
{
	MultiTargetBehaviour_t2061511750::get_offset_of_mMultiTarget_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2810 = { sizeof (VuforiaUnity_t1788908542), -1, sizeof(VuforiaUnity_t1788908542_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2810[4] = 
{
	VuforiaUnity_t1788908542_StaticFields::get_offset_of_mHoloLensApiAbstraction_0(),
	0,
	VuforiaUnity_t1788908542_StaticFields::get_offset_of_mRendererDirty_2(),
	VuforiaUnity_t1788908542_StaticFields::get_offset_of_mWrapperType_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2811 = { sizeof (InitError_t3420749710)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2811[12] = 
{
	InitError_t3420749710::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2812 = { sizeof (VuforiaHint_t545805519)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2812[4] = 
{
	VuforiaHint_t545805519::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2813 = { sizeof (StorageType_t857810839)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2813[4] = 
{
	StorageType_t857810839::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2814 = { sizeof (VuforiaARController_t1876945237), -1, sizeof(VuforiaARController_t1876945237_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2814[33] = 
{
	VuforiaARController_t1876945237::get_offset_of_CameraDeviceModeSetting_1(),
	VuforiaARController_t1876945237::get_offset_of_MaxSimultaneousImageTargets_2(),
	VuforiaARController_t1876945237::get_offset_of_MaxSimultaneousObjectTargets_3(),
	VuforiaARController_t1876945237::get_offset_of_UseDelayedLoadingObjectTargets_4(),
	VuforiaARController_t1876945237::get_offset_of_CameraDirection_5(),
	VuforiaARController_t1876945237::get_offset_of_MirrorVideoBackground_6(),
	VuforiaARController_t1876945237::get_offset_of_mWorldCenterMode_7(),
	VuforiaARController_t1876945237::get_offset_of_mWorldCenter_8(),
	VuforiaARController_t1876945237::get_offset_of_mVideoBgEventHandlers_9(),
	VuforiaARController_t1876945237::get_offset_of_mOnVuforiaInitialized_10(),
	VuforiaARController_t1876945237::get_offset_of_mOnVuforiaStarted_11(),
	VuforiaARController_t1876945237::get_offset_of_mOnVuforiaDeinitialized_12(),
	VuforiaARController_t1876945237::get_offset_of_mOnTrackablesUpdated_13(),
	VuforiaARController_t1876945237::get_offset_of_mRenderOnUpdate_14(),
	VuforiaARController_t1876945237::get_offset_of_mOnPause_15(),
	VuforiaARController_t1876945237::get_offset_of_mPaused_16(),
	VuforiaARController_t1876945237::get_offset_of_mOnBackgroundTextureChanged_17(),
	VuforiaARController_t1876945237::get_offset_of_mStartHasBeenInvoked_18(),
	VuforiaARController_t1876945237::get_offset_of_mHasStarted_19(),
	VuforiaARController_t1876945237::get_offset_of_mCameraConfiguration_20(),
	VuforiaARController_t1876945237::get_offset_of_mEyewearBehaviour_21(),
	VuforiaARController_t1876945237::get_offset_of_mVideoBackgroundMgr_22(),
	VuforiaARController_t1876945237::get_offset_of_mCheckStopCamera_23(),
	VuforiaARController_t1876945237::get_offset_of_mClearMaterial_24(),
	VuforiaARController_t1876945237::get_offset_of_mMetalRendering_25(),
	VuforiaARController_t1876945237::get_offset_of_mHasStartedOnce_26(),
	VuforiaARController_t1876945237::get_offset_of_mWasEnabledBeforePause_27(),
	VuforiaARController_t1876945237::get_offset_of_mObjectTrackerWasActiveBeforePause_28(),
	VuforiaARController_t1876945237::get_offset_of_mObjectTrackerWasActiveBeforeDisabling_29(),
	VuforiaARController_t1876945237::get_offset_of_mLastUpdatedFrame_30(),
	VuforiaARController_t1876945237::get_offset_of_mTrackersRequestedToDeinit_31(),
	VuforiaARController_t1876945237_StaticFields::get_offset_of_mInstance_32(),
	VuforiaARController_t1876945237_StaticFields::get_offset_of_mPadlock_33(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2815 = { sizeof (WorldCenterMode_t3672819471)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2815[5] = 
{
	WorldCenterMode_t3672819471::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2816 = { sizeof (VuforiaMacros_t2044285728)+ sizeof (RuntimeObject), sizeof(VuforiaMacros_t2044285728 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2816[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2817 = { sizeof (VuforiaManager_t1653423889), -1, sizeof(VuforiaManager_t1653423889_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2817[25] = 
{
	VuforiaManager_t1653423889::get_offset_of_U3CVideoBackgroundTextureSetU3Ek__BackingField_0(),
	VuforiaManager_t1653423889_StaticFields::get_offset_of_sInstance_1(),
	VuforiaManager_t1653423889::get_offset_of_mWorldCenterMode_2(),
	VuforiaManager_t1653423889::get_offset_of_mWorldCenter_3(),
	VuforiaManager_t1653423889::get_offset_of_mVuMarkWorldCenter_4(),
	VuforiaManager_t1653423889::get_offset_of_mARCameraTransform_5(),
	VuforiaManager_t1653423889::get_offset_of_mCentralAnchorPoint_6(),
	VuforiaManager_t1653423889::get_offset_of_mTrackableResultDataArray_7(),
	VuforiaManager_t1653423889::get_offset_of_mVuMarkDataArray_8(),
	VuforiaManager_t1653423889::get_offset_of_mVuMarkResultDataArray_9(),
	VuforiaManager_t1653423889::get_offset_of_mWCTrackableFoundQueue_10(),
	VuforiaManager_t1653423889::get_offset_of_mImageHeaderData_11(),
	VuforiaManager_t1653423889::get_offset_of_mNumImageHeaders_12(),
	VuforiaManager_t1653423889::get_offset_of_mInjectedFrameIdx_13(),
	VuforiaManager_t1653423889::get_offset_of_mLastProcessedFrameStatePtr_14(),
	VuforiaManager_t1653423889::get_offset_of_mInitialized_15(),
	VuforiaManager_t1653423889::get_offset_of_mPaused_16(),
	VuforiaManager_t1653423889::get_offset_of_mFrameState_17(),
	VuforiaManager_t1653423889::get_offset_of_mAutoRotationState_18(),
	VuforiaManager_t1653423889::get_offset_of_mVideoBackgroundNeedsRedrawing_19(),
	VuforiaManager_t1653423889::get_offset_of_mDiscardStatesForRendering_20(),
	VuforiaManager_t1653423889::get_offset_of_mLastFrameIdx_21(),
	VuforiaManager_t1653423889::get_offset_of_mIsSeeThroughDevice_22(),
	VuforiaManager_t1653423889::get_offset_of_mLateLatchingManager_23(),
	VuforiaManager_t1653423889::get_offset_of_mCameraCalibrationComparer_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2818 = { sizeof (TrackableIdPair_t4227350457)+ sizeof (RuntimeObject), sizeof(TrackableIdPair_t4227350457 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2818[2] = 
{
	TrackableIdPair_t4227350457::get_offset_of_TrackableId_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TrackableIdPair_t4227350457::get_offset_of_ResultId_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2819 = { sizeof (AutoRotationState_t2150317116)+ sizeof (RuntimeObject), sizeof(AutoRotationState_t2150317116_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2819[5] = 
{
	AutoRotationState_t2150317116::get_offset_of_setOnPause_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AutoRotationState_t2150317116::get_offset_of_autorotateToPortrait_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AutoRotationState_t2150317116::get_offset_of_autorotateToPortraitUpsideDown_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AutoRotationState_t2150317116::get_offset_of_autorotateToLandscapeLeft_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AutoRotationState_t2150317116::get_offset_of_autorotateToLandscapeRight_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2820 = { sizeof (U3CU3Ec__DisplayClass73_0_t779439468), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2820[1] = 
{
	U3CU3Ec__DisplayClass73_0_t779439468::get_offset_of_id_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2821 = { sizeof (VuforiaRenderer_t3433045970), -1, sizeof(VuforiaRenderer_t3433045970_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2821[7] = 
{
	VuforiaRenderer_t3433045970_StaticFields::get_offset_of_sInstance_0(),
	VuforiaRenderer_t3433045970::get_offset_of_mVideoBGConfig_1(),
	VuforiaRenderer_t3433045970::get_offset_of_mVideoBGConfigSet_2(),
	VuforiaRenderer_t3433045970::get_offset_of_mVideoBackgroundTexture_3(),
	VuforiaRenderer_t3433045970::get_offset_of_mBackgroundTextureHasChanged_4(),
	VuforiaRenderer_t3433045970::get_offset_of_mLastSetReflection_5(),
	VuforiaRenderer_t3433045970::get_offset_of_mNativeRenderingCallback_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2822 = { sizeof (FpsHint_t2906034572)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2822[5] = 
{
	FpsHint_t2906034572::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2823 = { sizeof (VideoBackgroundReflection_t736962841)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2823[4] = 
{
	VideoBackgroundReflection_t736962841::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2824 = { sizeof (VideoBGCfgData_t994527297)+ sizeof (RuntimeObject), sizeof(VideoBGCfgData_t994527297 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2824[4] = 
{
	VideoBGCfgData_t994527297::get_offset_of_position_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	VideoBGCfgData_t994527297::get_offset_of_size_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	VideoBGCfgData_t994527297::get_offset_of_enabled_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	VideoBGCfgData_t994527297::get_offset_of_reflectionInteger_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2825 = { sizeof (Vec2I_t3527036565)+ sizeof (RuntimeObject), sizeof(Vec2I_t3527036565 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2825[2] = 
{
	Vec2I_t3527036565::get_offset_of_x_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Vec2I_t3527036565::get_offset_of_y_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2826 = { sizeof (VideoTextureInfo_t1805965052)+ sizeof (RuntimeObject), sizeof(VideoTextureInfo_t1805965052 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2826[2] = 
{
	VideoTextureInfo_t1805965052::get_offset_of_textureSize_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	VideoTextureInfo_t1805965052::get_offset_of_imageSize_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2827 = { sizeof (RendererAPI_t402009282)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2827[5] = 
{
	RendererAPI_t402009282::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2828 = { sizeof (RenderEvent_t1863578599)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2828[7] = 
{
	RenderEvent_t1863578599::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2829 = { sizeof (VuforiaRuntimeUtilities_t399660591), -1, sizeof(VuforiaRuntimeUtilities_t399660591_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2829[2] = 
{
	VuforiaRuntimeUtilities_t399660591_StaticFields::get_offset_of_sWebCamUsed_0(),
	VuforiaRuntimeUtilities_t399660591_StaticFields::get_offset_of_sNativePluginSupport_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2830 = { sizeof (InitializableBool_t3274999204)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2830[4] = 
{
	InitializableBool_t3274999204::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2831 = { sizeof (GlobalVars_t2485087241), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2831[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2832 = { sizeof (SurfaceUtilities_t1841955943), -1, sizeof(SurfaceUtilities_t1841955943_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2832[1] = 
{
	SurfaceUtilities_t1841955943_StaticFields::get_offset_of_mScreenOrientation_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2833 = { sizeof (TargetFinder_t2439332195), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2833[4] = 
{
	TargetFinder_t2439332195::get_offset_of_mTargetFinderStatePtr_0(),
	TargetFinder_t2439332195::get_offset_of_mTargetFinderState_1(),
	TargetFinder_t2439332195::get_offset_of_mNewResults_2(),
	TargetFinder_t2439332195::get_offset_of_mImageTargets_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2834 = { sizeof (InitState_t538152685)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2834[6] = 
{
	InitState_t538152685::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2835 = { sizeof (UpdateState_t1279515537)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2835[12] = 
{
	UpdateState_t1279515537::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2836 = { sizeof (FilterMode_t1400485161)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2836[3] = 
{
	FilterMode_t1400485161::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2837 = { sizeof (TargetSearchResult_t3441982613)+ sizeof (RuntimeObject), sizeof(TargetSearchResult_t3441982613_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2837[6] = 
{
	TargetSearchResult_t3441982613::get_offset_of_TargetName_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TargetSearchResult_t3441982613::get_offset_of_UniqueTargetId_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TargetSearchResult_t3441982613::get_offset_of_TargetSize_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TargetSearchResult_t3441982613::get_offset_of_MetaData_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TargetSearchResult_t3441982613::get_offset_of_TrackingRating_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TargetSearchResult_t3441982613::get_offset_of_TargetSearchResultPtr_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2838 = { sizeof (TargetFinderState_t3286805956)+ sizeof (RuntimeObject), sizeof(TargetFinderState_t3286805956 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2838[4] = 
{
	TargetFinderState_t3286805956::get_offset_of_IsRequesting_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TargetFinderState_t3286805956::get_offset_of_UpdateState_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TargetFinderState_t3286805956::get_offset_of_ResultCount_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TargetFinderState_t3286805956::get_offset_of_unused_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2839 = { sizeof (InternalTargetSearchResult_t3697474723)+ sizeof (RuntimeObject), sizeof(InternalTargetSearchResult_t3697474723 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2839[6] = 
{
	InternalTargetSearchResult_t3697474723::get_offset_of_TargetNamePtr_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	InternalTargetSearchResult_t3697474723::get_offset_of_UniqueTargetIdPtr_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	InternalTargetSearchResult_t3697474723::get_offset_of_MetaDataPtr_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	InternalTargetSearchResult_t3697474723::get_offset_of_TargetSearchResultPtr_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	InternalTargetSearchResult_t3697474723::get_offset_of_TargetSize_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	InternalTargetSearchResult_t3697474723::get_offset_of_TrackingRating_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2840 = { sizeof (SimpleTargetData_t4194873257)+ sizeof (RuntimeObject), sizeof(SimpleTargetData_t4194873257 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2840[2] = 
{
	SimpleTargetData_t4194873257::get_offset_of_id_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SimpleTargetData_t4194873257::get_offset_of_unused_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2841 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2842 = { sizeof (TrackableBehaviour_t1113559212), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2842[8] = 
{
	TrackableBehaviour_t1113559212::get_offset_of_U3CTimeStampU3Ek__BackingField_4(),
	TrackableBehaviour_t1113559212::get_offset_of_mTrackableName_5(),
	TrackableBehaviour_t1113559212::get_offset_of_mPreserveChildSize_6(),
	TrackableBehaviour_t1113559212::get_offset_of_mInitializedInEditor_7(),
	TrackableBehaviour_t1113559212::get_offset_of_mPreviousScale_8(),
	TrackableBehaviour_t1113559212::get_offset_of_mStatus_9(),
	TrackableBehaviour_t1113559212::get_offset_of_mTrackable_10(),
	TrackableBehaviour_t1113559212::get_offset_of_mTrackableEventHandlers_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2843 = { sizeof (Status_t1100905814)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2843[10] = 
{
	Status_t1100905814::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2844 = { sizeof (StatusInfo_t1633251416)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2844[6] = 
{
	StatusInfo_t1633251416::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2845 = { sizeof (CoordinateSystem_t4035406609)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2845[4] = 
{
	CoordinateSystem_t4035406609::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2846 = { sizeof (TrackableSource_t2567074243), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2846[1] = 
{
	TrackableSource_t2567074243::get_offset_of_U3CTrackableSourcePtrU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2847 = { sizeof (Tracker_t2709586299), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2847[1] = 
{
	Tracker_t2709586299::get_offset_of_U3CIsActiveU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2848 = { sizeof (TrackerManager_t1703337244), -1, sizeof(TrackerManager_t1703337244_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2848[5] = 
{
	TrackerManager_t1703337244_StaticFields::get_offset_of_mInstance_0(),
	TrackerManager_t1703337244::get_offset_of_mStateManager_1(),
	TrackerManager_t1703337244::get_offset_of_mTrackers_2(),
	TrackerManager_t1703337244::get_offset_of_mTrackerCreators_3(),
	TrackerManager_t1703337244::get_offset_of_mTrackerNativeDeinitializers_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2849 = { sizeof (U3CU3Ec_t1451390621), -1, sizeof(U3CU3Ec_t1451390621_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2849[5] = 
{
	U3CU3Ec_t1451390621_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t1451390621_StaticFields::get_offset_of_U3CU3E9__8_0_1(),
	U3CU3Ec_t1451390621_StaticFields::get_offset_of_U3CU3E9__8_1_2(),
	U3CU3Ec_t1451390621_StaticFields::get_offset_of_U3CU3E9__8_2_3(),
	U3CU3Ec_t1451390621_StaticFields::get_offset_of_U3CU3E9__8_3_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2850 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2851 = { sizeof (UserDefinedTargetBuildingBehaviour_t4262637471), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2851[11] = 
{
	UserDefinedTargetBuildingBehaviour_t4262637471::get_offset_of_mObjectTracker_4(),
	UserDefinedTargetBuildingBehaviour_t4262637471::get_offset_of_mLastFrameQuality_5(),
	UserDefinedTargetBuildingBehaviour_t4262637471::get_offset_of_mCurrentlyScanning_6(),
	UserDefinedTargetBuildingBehaviour_t4262637471::get_offset_of_mWasScanningBeforeDisable_7(),
	UserDefinedTargetBuildingBehaviour_t4262637471::get_offset_of_mCurrentlyBuilding_8(),
	UserDefinedTargetBuildingBehaviour_t4262637471::get_offset_of_mWasBuildingBeforeDisable_9(),
	UserDefinedTargetBuildingBehaviour_t4262637471::get_offset_of_mOnInitializedCalled_10(),
	UserDefinedTargetBuildingBehaviour_t4262637471::get_offset_of_mHandlers_11(),
	UserDefinedTargetBuildingBehaviour_t4262637471::get_offset_of_StopTrackerWhileScanning_12(),
	UserDefinedTargetBuildingBehaviour_t4262637471::get_offset_of_StartScanningAutomatically_13(),
	UserDefinedTargetBuildingBehaviour_t4262637471::get_offset_of_StopScanningWhenFinshedBuilding_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2852 = { sizeof (VideoBackgroundBehaviour_t1552899074), -1, sizeof(VideoBackgroundBehaviour_t1552899074_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2852[8] = 
{
	VideoBackgroundBehaviour_t1552899074::get_offset_of_mClearBuffers_4(),
	VideoBackgroundBehaviour_t1552899074::get_offset_of_mSkipStateUpdates_5(),
	VideoBackgroundBehaviour_t1552899074::get_offset_of_mVuforiaARController_6(),
	VideoBackgroundBehaviour_t1552899074::get_offset_of_mCamera_7(),
	VideoBackgroundBehaviour_t1552899074::get_offset_of_mBackgroundBehaviour_8(),
	VideoBackgroundBehaviour_t1552899074_StaticFields::get_offset_of_mFrameCounter_9(),
	VideoBackgroundBehaviour_t1552899074_StaticFields::get_offset_of_mRenderCounter_10(),
	VideoBackgroundBehaviour_t1552899074::get_offset_of_mDisabledMeshRenderers_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2853 = { sizeof (VideoBackgroundManager_t2198727358), -1, sizeof(VideoBackgroundManager_t2198727358_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2853[8] = 
{
	VideoBackgroundManager_t2198727358::get_offset_of_mClippingMode_1(),
	VideoBackgroundManager_t2198727358::get_offset_of_mMatteShader_2(),
	VideoBackgroundManager_t2198727358::get_offset_of_mVideoBackgroundEnabled_3(),
	VideoBackgroundManager_t2198727358::get_offset_of_mTexture_4(),
	VideoBackgroundManager_t2198727358::get_offset_of_mVideoBgConfigChanged_5(),
	VideoBackgroundManager_t2198727358::get_offset_of_mNativeTexturePtr_6(),
	VideoBackgroundManager_t2198727358_StaticFields::get_offset_of_mInstance_7(),
	VideoBackgroundManager_t2198727358_StaticFields::get_offset_of_mPadlock_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2854 = { sizeof (VirtualButton_t386166510), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2854[7] = 
{
	VirtualButton_t386166510::get_offset_of_mName_0(),
	VirtualButton_t386166510::get_offset_of_mID_1(),
	VirtualButton_t386166510::get_offset_of_mArea_2(),
	VirtualButton_t386166510::get_offset_of_mIsEnabled_3(),
	VirtualButton_t386166510::get_offset_of_mParentImageTarget_4(),
	VirtualButton_t386166510::get_offset_of_mParentDataSet_5(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2855 = { sizeof (Sensitivity_t3045829715)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2855[4] = 
{
	Sensitivity_t3045829715::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2856 = { sizeof (VirtualButtonBehaviour_t1436326451), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2856[15] = 
{
	0,
	VirtualButtonBehaviour_t1436326451::get_offset_of_mName_5(),
	VirtualButtonBehaviour_t1436326451::get_offset_of_mSensitivity_6(),
	VirtualButtonBehaviour_t1436326451::get_offset_of_mHasUpdatedPose_7(),
	VirtualButtonBehaviour_t1436326451::get_offset_of_mPrevTransform_8(),
	VirtualButtonBehaviour_t1436326451::get_offset_of_mPrevParent_9(),
	VirtualButtonBehaviour_t1436326451::get_offset_of_mSensitivityDirty_10(),
	VirtualButtonBehaviour_t1436326451::get_offset_of_mPreviousSensitivity_11(),
	VirtualButtonBehaviour_t1436326451::get_offset_of_mPreviouslyEnabled_12(),
	VirtualButtonBehaviour_t1436326451::get_offset_of_mPressed_13(),
	VirtualButtonBehaviour_t1436326451::get_offset_of_mHandlers_14(),
	VirtualButtonBehaviour_t1436326451::get_offset_of_mLeftTop_15(),
	VirtualButtonBehaviour_t1436326451::get_offset_of_mRightBottom_16(),
	VirtualButtonBehaviour_t1436326451::get_offset_of_mUnregisterOnDestroy_17(),
	VirtualButtonBehaviour_t1436326451::get_offset_of_mVirtualButton_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2857 = { sizeof (WebCamARController_t3718642882), -1, sizeof(WebCamARController_t3718642882_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2857[7] = 
{
	WebCamARController_t3718642882::get_offset_of_RenderTextureLayer_1(),
	WebCamARController_t3718642882::get_offset_of_mDeviceNameSetInEditor_2(),
	WebCamARController_t3718642882::get_offset_of_mFlipHorizontally_3(),
	WebCamARController_t3718642882::get_offset_of_mWebCamImpl_4(),
	WebCamARController_t3718642882::get_offset_of_mWebCamTexAdaptorProvider_5(),
	WebCamARController_t3718642882_StaticFields::get_offset_of_mInstance_6(),
	WebCamARController_t3718642882_StaticFields::get_offset_of_mPadlock_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2858 = { sizeof (U3CU3Ec_t3582055403), -1, sizeof(U3CU3Ec_t3582055403_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2858[2] = 
{
	U3CU3Ec_t3582055403_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t3582055403_StaticFields::get_offset_of_U3CU3E9__7_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2859 = { sizeof (EyewearCalibrationProfileManager_t947793426), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2860 = { sizeof (EyewearUserCalibrator_t2926839199), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2861 = { sizeof (WireframeBehaviour_t1831066704), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2861[4] = 
{
	WireframeBehaviour_t1831066704::get_offset_of_lineMaterial_4(),
	WireframeBehaviour_t1831066704::get_offset_of_ShowLines_5(),
	WireframeBehaviour_t1831066704::get_offset_of_LineColor_6(),
	WireframeBehaviour_t1831066704::get_offset_of_mLineMaterial_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2862 = { sizeof (WireframeTrackableEventHandler_t2143753312), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2862[1] = 
{
	WireframeTrackableEventHandler_t2143753312::get_offset_of_mTrackableBehaviour_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2863 = { sizeof (U3CPrivateImplementationDetailsU3E_t3057255368), -1, sizeof(U3CPrivateImplementationDetailsU3E_t3057255368_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2863[1] = 
{
	U3CPrivateImplementationDetailsU3E_t3057255368_StaticFields::get_offset_of_U3898C2022A0C02FCE602BF05E1C09BD48301606E5_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2864 = { sizeof (__StaticArrayInitTypeSizeU3D24_t3517759979)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D24_t3517759979 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2865 = { sizeof (U3CModuleU3E_t692745555), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2866 = { sizeof (CameraSettings_t3152619780), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2866[4] = 
{
	CameraSettings_t3152619780::get_offset_of_mVuforiaStarted_4(),
	CameraSettings_t3152619780::get_offset_of_mAutofocusEnabled_5(),
	CameraSettings_t3152619780::get_offset_of_mFlashTorchEnabled_6(),
	CameraSettings_t3152619780::get_offset_of_mActiveDirection_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2867 = { sizeof (U3CRestoreOriginalFocusModeU3Ec__Iterator0_t2912012229), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2867[4] = 
{
	U3CRestoreOriginalFocusModeU3Ec__Iterator0_t2912012229::get_offset_of_U24this_0(),
	U3CRestoreOriginalFocusModeU3Ec__Iterator0_t2912012229::get_offset_of_U24current_1(),
	U3CRestoreOriginalFocusModeU3Ec__Iterator0_t2912012229::get_offset_of_U24disposing_2(),
	U3CRestoreOriginalFocusModeU3Ec__Iterator0_t2912012229::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2868 = { sizeof (FrameRateSettings_t3598747490), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2869 = { sizeof (InitErrorHandler_t2159361531), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2869[4] = 
{
	InitErrorHandler_t2159361531::get_offset_of_errorText_4(),
	InitErrorHandler_t2159361531::get_offset_of_errorCanvas_5(),
	InitErrorHandler_t2159361531::get_offset_of_key_6(),
	InitErrorHandler_t2159361531::get_offset_of_errorMsg_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2870 = { sizeof (MenuAnimator_t2112910832), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2870[7] = 
{
	MenuAnimator_t2112910832::get_offset_of_mVisiblePos_4(),
	MenuAnimator_t2112910832::get_offset_of_mInvisiblePos_5(),
	MenuAnimator_t2112910832::get_offset_of_mVisibility_6(),
	MenuAnimator_t2112910832::get_offset_of_mVisible_7(),
	MenuAnimator_t2112910832::get_offset_of_mCanvas_8(),
	MenuAnimator_t2112910832::get_offset_of_mMenuOptions_9(),
	MenuAnimator_t2112910832::get_offset_of_SlidingTime_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2871 = { sizeof (MenuOptions_t1951716431), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2871[3] = 
{
	MenuOptions_t1951716431::get_offset_of_mCamSettings_4(),
	MenuOptions_t1951716431::get_offset_of_mTrackableSettings_5(),
	MenuOptions_t1951716431::get_offset_of_mMenuAnim_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2872 = { sizeof (AboutScreen_t2183797299), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2873 = { sizeof (AsyncSceneLoader_t621267272), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2873[1] = 
{
	AsyncSceneLoader_t621267272::get_offset_of_loadingDelay_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2874 = { sizeof (U3CLoadNextSceneAfterU3Ec__Iterator0_t994227170), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2874[4] = 
{
	U3CLoadNextSceneAfterU3Ec__Iterator0_t994227170::get_offset_of_seconds_0(),
	U3CLoadNextSceneAfterU3Ec__Iterator0_t994227170::get_offset_of_U24current_1(),
	U3CLoadNextSceneAfterU3Ec__Iterator0_t994227170::get_offset_of_U24disposing_2(),
	U3CLoadNextSceneAfterU3Ec__Iterator0_t994227170::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2875 = { sizeof (LoadingScreen_t2154736699), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2875[2] = 
{
	LoadingScreen_t2154736699::get_offset_of_mChangeLevel_4(),
	LoadingScreen_t2154736699::get_offset_of_mUISpinner_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2876 = { sizeof (TapHandler_t334234343), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2876[4] = 
{
	0,
	TapHandler_t334234343::get_offset_of_mTimeSinceLastTap_5(),
	TapHandler_t334234343::get_offset_of_mMenuAnim_6(),
	TapHandler_t334234343::get_offset_of_mTapCount_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2877 = { sizeof (TrackableSettings_t2862243993), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2877[1] = 
{
	TrackableSettings_t2862243993::get_offset_of_mExtTrackingEnabled_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2878 = { sizeof (Basics_t653963782), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2878[2] = 
{
	Basics_t653963782::get_offset_of_cubeA_4(),
	Basics_t653963782::get_offset_of_cubeB_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2879 = { sizeof (Sequences_t3991711780), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2879[1] = 
{
	Sequences_t3991711780::get_offset_of_target_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2880 = { sizeof (Pixel_t2713956838), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2880[3] = 
{
	Pixel_t2713956838::get_offset_of_i_0(),
	Pixel_t2713956838::get_offset_of_j_1(),
	Pixel_t2713956838::get_offset_of_couleur_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2881 = { sizeof (animationImage_t1423068421), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2881[17] = 
{
	animationImage_t1423068421::get_offset_of_tex_4(),
	animationImage_t1423068421::get_offset_of_mySprite_5(),
	animationImage_t1423068421::get_offset_of_mySpriteResult_6(),
	animationImage_t1423068421::get_offset_of_sr_7(),
	animationImage_t1423068421::get_offset_of_srResult_8(),
	animationImage_t1423068421::get_offset_of_newSpriteObj_9(),
	animationImage_t1423068421::get_offset_of_newSpriteResult_10(),
	animationImage_t1423068421::get_offset_of_C_11(),
	animationImage_t1423068421::get_offset_of_pixelByUnity_12(),
	animationImage_t1423068421::get_offset_of_listePixels_13(),
	animationImage_t1423068421::get_offset_of_listeColPixels_14(),
	animationImage_t1423068421::get_offset_of_ChoixAnimation_15(),
	animationImage_t1423068421::get_offset_of_animationFinie_16(),
	animationImage_t1423068421::get_offset_of_affichageFini_17(),
	animationImage_t1423068421::get_offset_of_numPix_18(),
	animationImage_t1423068421::get_offset_of_nbPix_19(),
	animationImage_t1423068421::get_offset_of_nbAfficher_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2882 = { sizeof (Grattage_t1486418579), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2882[26] = 
{
	Grattage_t1486418579::get_offset_of_tex_4(),
	Grattage_t1486418579::get_offset_of_imageGrattable_5(),
	Grattage_t1486418579::get_offset_of_mySprite_6(),
	Grattage_t1486418579::get_offset_of_mySpriteResult_7(),
	Grattage_t1486418579::get_offset_of_sr_8(),
	Grattage_t1486418579::get_offset_of_srResult_9(),
	Grattage_t1486418579::get_offset_of_newSpriteObj_10(),
	Grattage_t1486418579::get_offset_of_newSpriteResult_11(),
	Grattage_t1486418579::get_offset_of_cur_12(),
	Grattage_t1486418579::get_offset_of_plan_13(),
	Grattage_t1486418579::get_offset_of_imageResult_14(),
	Grattage_t1486418579::get_offset_of_ImageAGratter_15(),
	Grattage_t1486418579::get_offset_of_C_16(),
	Grattage_t1486418579::get_offset_of_pixelByUnity_17(),
	Grattage_t1486418579::get_offset_of_pourcentageImageDecouverte_18(),
	Grattage_t1486418579::get_offset_of_pointsEffaces_19(),
	Grattage_t1486418579::get_offset_of_listePixels_20(),
	Grattage_t1486418579::get_offset_of_PrefabFireworks_21(),
	Grattage_t1486418579::get_offset_of_bravo_22(),
	Grattage_t1486418579::get_offset_of_perdu_23(),
	Grattage_t1486418579::get_offset_of_cursor_24(),
	Grattage_t1486418579::get_offset_of_shader_25(),
	Grattage_t1486418579::get_offset_of_reussite_26(),
	Grattage_t1486418579::get_offset_of_cursoraffiche_27(),
	Grattage_t1486418579::get_offset_of_pourcentageSeuil_28(),
	Grattage_t1486418579::get_offset_of_TailleDuPinceau_29(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2883 = { sizeof (GrattageManager_t1333860694), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2883[39] = 
{
	GrattageManager_t1333860694::get_offset_of_goFront_4(),
	GrattageManager_t1333860694::get_offset_of_goBack_5(),
	GrattageManager_t1333860694::get_offset_of_spriteFront_6(),
	GrattageManager_t1333860694::get_offset_of_spriteBack_7(),
	GrattageManager_t1333860694::get_offset_of_srFront_8(),
	GrattageManager_t1333860694::get_offset_of_srBack_9(),
	GrattageManager_t1333860694::get_offset_of_tex_10(),
	GrattageManager_t1333860694::get_offset_of_imageGrattable_11(),
	GrattageManager_t1333860694::get_offset_of_mySprite_12(),
	GrattageManager_t1333860694::get_offset_of_mySpriteResult_13(),
	GrattageManager_t1333860694::get_offset_of_sr_14(),
	GrattageManager_t1333860694::get_offset_of_srResult_15(),
	GrattageManager_t1333860694::get_offset_of_newSpriteObj_16(),
	GrattageManager_t1333860694::get_offset_of_newSpriteResult_17(),
	GrattageManager_t1333860694::get_offset_of_cur_18(),
	GrattageManager_t1333860694::get_offset_of_plan_19(),
	GrattageManager_t1333860694::get_offset_of_imageResult_20(),
	GrattageManager_t1333860694::get_offset_of_ImageAGratter_21(),
	GrattageManager_t1333860694::get_offset_of_C_22(),
	GrattageManager_t1333860694::get_offset_of_pixelPerUnit_23(),
	GrattageManager_t1333860694::get_offset_of_pourcentageImageDecouverte_24(),
	GrattageManager_t1333860694::get_offset_of_PrefabFireworks_25(),
	GrattageManager_t1333860694::get_offset_of_bravo_26(),
	GrattageManager_t1333860694::get_offset_of_perdu_27(),
	GrattageManager_t1333860694::get_offset_of_cursor_28(),
	GrattageManager_t1333860694::get_offset_of_reussite_29(),
	GrattageManager_t1333860694::get_offset_of_cursoraffiche_30(),
	GrattageManager_t1333860694::get_offset_of_urlImageGratter_31(),
	GrattageManager_t1333860694::get_offset_of_urlImageBack_32(),
	GrattageManager_t1333860694::get_offset_of_rejouer_33(),
	GrattageManager_t1333860694::get_offset_of_reglement_34(),
	GrattageManager_t1333860694::get_offset_of_status_35(),
	GrattageManager_t1333860694::get_offset_of_id_36(),
	GrattageManager_t1333860694::get_offset_of_name_37(),
	GrattageManager_t1333860694::get_offset_of_reward_msg_38(),
	GrattageManager_t1333860694::get_offset_of_link_of_reward_39(),
	GrattageManager_t1333860694::get_offset_of_token_40(),
	GrattageManager_t1333860694::get_offset_of_pourcentageSeuil_41(),
	GrattageManager_t1333860694::get_offset_of_TailleDuPinceau_42(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2884 = { sizeof (U3CsetupURL_IMPU3Ec__Iterator0_t1720507757), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2884[6] = 
{
	U3CsetupURL_IMPU3Ec__Iterator0_t1720507757::get_offset_of_U3CwwwFrontU3E__0_0(),
	U3CsetupURL_IMPU3Ec__Iterator0_t1720507757::get_offset_of_U3CwwwBackU3E__0_1(),
	U3CsetupURL_IMPU3Ec__Iterator0_t1720507757::get_offset_of_U24this_2(),
	U3CsetupURL_IMPU3Ec__Iterator0_t1720507757::get_offset_of_U24current_3(),
	U3CsetupURL_IMPU3Ec__Iterator0_t1720507757::get_offset_of_U24disposing_4(),
	U3CsetupURL_IMPU3Ec__Iterator0_t1720507757::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2885 = { sizeof (GrattageScript_t3667968469), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2885[49] = 
{
	GrattageScript_t3667968469::get_offset_of_UserGUID_4(),
	GrattageScript_t3667968469::get_offset_of_markerUID_5(),
	GrattageScript_t3667968469::get_offset_of_appName_6(),
	GrattageScript_t3667968469::get_offset_of_boutonGagne_7(),
	GrattageScript_t3667968469::get_offset_of_boutonPerdu_8(),
	GrattageScript_t3667968469::get_offset_of_goFront_9(),
	GrattageScript_t3667968469::get_offset_of_goBack_10(),
	GrattageScript_t3667968469::get_offset_of_spriteBack_11(),
	GrattageScript_t3667968469::get_offset_of_emailPanel_12(),
	GrattageScript_t3667968469::get_offset_of_emailPanel_mail_13(),
	GrattageScript_t3667968469::get_offset_of_emailPanel_text_14(),
	GrattageScript_t3667968469::get_offset_of_emailPanel_button_15(),
	GrattageScript_t3667968469::get_offset_of_srFront_16(),
	GrattageScript_t3667968469::get_offset_of_srBack_17(),
	GrattageScript_t3667968469::get_offset_of_tex_18(),
	GrattageScript_t3667968469::get_offset_of_imageGrattable_19(),
	GrattageScript_t3667968469::get_offset_of_C_20(),
	GrattageScript_t3667968469::get_offset_of_pixelPerUnit_21(),
	GrattageScript_t3667968469::get_offset_of_pourcentageImageDecouverte_22(),
	GrattageScript_t3667968469::get_offset_of_PrefabFireworks_23(),
	GrattageScript_t3667968469::get_offset_of_bravo_24(),
	GrattageScript_t3667968469::get_offset_of_perdu_25(),
	GrattageScript_t3667968469::get_offset_of_cursor_26(),
	GrattageScript_t3667968469::get_offset_of_reussite_27(),
	GrattageScript_t3667968469::get_offset_of_cursoraffiche_28(),
	GrattageScript_t3667968469::get_offset_of_urlImageGratter_29(),
	GrattageScript_t3667968469::get_offset_of_urlImageBack_30(),
	GrattageScript_t3667968469::get_offset_of_rejouer_31(),
	GrattageScript_t3667968469::get_offset_of_reglement_32(),
	GrattageScript_t3667968469::get_offset_of_status_33(),
	GrattageScript_t3667968469::get_offset_of_id_34(),
	GrattageScript_t3667968469::get_offset_of_nameJeu_35(),
	GrattageScript_t3667968469::get_offset_of_reward_msg_36(),
	GrattageScript_t3667968469::get_offset_of_link_of_reward_37(),
	GrattageScript_t3667968469::get_offset_of_token_38(),
	GrattageScript_t3667968469::get_offset_of_recup_mail_tirage_39(),
	GrattageScript_t3667968469::get_offset_of_tirage_au_sort_40(),
	GrattageScript_t3667968469::get_offset_of_isError_41(),
	GrattageScript_t3667968469::get_offset_of_errorMessage_42(),
	GrattageScript_t3667968469::get_offset_of_pano_regles_43(),
	GrattageScript_t3667968469::get_offset_of_pano_error_44(),
	GrattageScript_t3667968469::get_offset_of_ancetre_45(),
	GrattageScript_t3667968469::get_offset_of_hit_46(),
	GrattageScript_t3667968469::get_offset_of_ray_47(),
	GrattageScript_t3667968469::get_offset_of_hitAllTemp_48(),
	GrattageScript_t3667968469::get_offset_of_colliderPlaneFront_49(),
	GrattageScript_t3667968469::get_offset_of_textGagne3D_50(),
	GrattageScript_t3667968469::get_offset_of_pourcentageSeuil_51(),
	GrattageScript_t3667968469::get_offset_of_TailleDuPinceau_52(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2886 = { sizeof (U3CsetupURL_IMPU3Ec__Iterator0_t540476453), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2886[11] = 
{
	U3CsetupURL_IMPU3Ec__Iterator0_t540476453::get_offset_of_posX_0(),
	U3CsetupURL_IMPU3Ec__Iterator0_t540476453::get_offset_of_posY_1(),
	U3CsetupURL_IMPU3Ec__Iterator0_t540476453::get_offset_of_layer_2(),
	U3CsetupURL_IMPU3Ec__Iterator0_t540476453::get_offset_of_sizeX_3(),
	U3CsetupURL_IMPU3Ec__Iterator0_t540476453::get_offset_of_sizeY_4(),
	U3CsetupURL_IMPU3Ec__Iterator0_t540476453::get_offset_of_U3CwwwFrontU3E__0_5(),
	U3CsetupURL_IMPU3Ec__Iterator0_t540476453::get_offset_of_U3CwwwBackU3E__0_6(),
	U3CsetupURL_IMPU3Ec__Iterator0_t540476453::get_offset_of_U24this_7(),
	U3CsetupURL_IMPU3Ec__Iterator0_t540476453::get_offset_of_U24current_8(),
	U3CsetupURL_IMPU3Ec__Iterator0_t540476453::get_offset_of_U24disposing_9(),
	U3CsetupURL_IMPU3Ec__Iterator0_t540476453::get_offset_of_U24PC_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2887 = { sizeof (AboutManager_t2961629990), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2887[1] = 
{
	AboutManager_t2961629990::get_offset_of_aboutText_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2888 = { sizeof (AnimationsManager_t2281084567), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2888[11] = 
{
	AnimationsManager_t2281084567::get_offset_of_OverlayPosition_4(),
	AnimationsManager_t2281084567::get_offset_of_sliderZoom_5(),
	AnimationsManager_t2281084567::get_offset_of_contentManager_6(),
	AnimationsManager_t2281084567::get_offset_of_savedPositionCamera_7(),
	AnimationsManager_t2281084567::get_offset_of_savedPositionOverlay_8(),
	AnimationsManager_t2281084567::get_offset_of_savedUpVector_9(),
	AnimationsManager_t2281084567::get_offset_of_mAugmentationObject_10(),
	AnimationsManager_t2281084567::get_offset_of_mIsTracking_11(),
	AnimationsManager_t2281084567::get_offset_of_mDoAnimationTo2D_12(),
	AnimationsManager_t2281084567::get_offset_of_mDoAnimationTo3D_13(),
	AnimationsManager_t2281084567::get_offset_of_mIsShowingOverlay_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2889 = { sizeof (BookData_t1076903227), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2889[8] = 
{
	BookData_t1076903227::get_offset_of_U3CBookTitleU3Ek__BackingField_0(),
	BookData_t1076903227::get_offset_of_U3CBookAuthorU3Ek__BackingField_1(),
	BookData_t1076903227::get_offset_of_U3CBookRatingU3Ek__BackingField_2(),
	BookData_t1076903227::get_offset_of_U3CBookOverallRatingU3Ek__BackingField_3(),
	BookData_t1076903227::get_offset_of_U3CBookRegularPriceU3Ek__BackingField_4(),
	BookData_t1076903227::get_offset_of_U3CBookYourPriceU3Ek__BackingField_5(),
	BookData_t1076903227::get_offset_of_U3CBookThumbUrlU3Ek__BackingField_6(),
	BookData_t1076903227::get_offset_of_U3CBookDetailUrlU3Ek__BackingField_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2890 = { sizeof (BookInformationParser_t151156397), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2890[8] = 
{
	BookInformationParser_t151156397::get_offset_of_mBookObject_0(),
	BookInformationParser_t151156397::get_offset_of_mBookTitle_1(),
	BookInformationParser_t151156397::get_offset_of_mBookAuthor_2(),
	BookInformationParser_t151156397::get_offset_of_mBookRegularPrice_3(),
	BookInformationParser_t151156397::get_offset_of_mBookOverallRatings_4(),
	BookInformationParser_t151156397::get_offset_of_mBookStarsRating_5(),
	BookInformationParser_t151156397::get_offset_of_mBookYourPrice_6(),
	BookInformationParser_t151156397::get_offset_of_mBookThumb_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2891 = { sizeof (CloudRecoEventHandler_t613522768), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2891[10] = 
{
	CloudRecoEventHandler_t613522768::get_offset_of_mCloudRecoBehaviour_4(),
	CloudRecoEventHandler_t613522768::get_offset_of_mObjectTracker_5(),
	CloudRecoEventHandler_t613522768::get_offset_of_mContentManager_6(),
	CloudRecoEventHandler_t613522768::get_offset_of_mParentOfImageTargetTemplate_7(),
	CloudRecoEventHandler_t613522768::get_offset_of_mMustRestartApp_8(),
	CloudRecoEventHandler_t613522768::get_offset_of_errorTitle_9(),
	CloudRecoEventHandler_t613522768::get_offset_of_errorMsg_10(),
	CloudRecoEventHandler_t613522768::get_offset_of_ImageTargetTemplate_11(),
	CloudRecoEventHandler_t613522768::get_offset_of_scanLine_12(),
	CloudRecoEventHandler_t613522768::get_offset_of_isTracking_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2892 = { sizeof (ContentManager_t211916338), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2892[17] = 
{
	ContentManager_t211916338::get_offset_of_LoadingSpinnerBackground_4(),
	ContentManager_t211916338::get_offset_of_LoadingSpinnerImage_5(),
	ContentManager_t211916338::get_offset_of_CancelButton_6(),
	ContentManager_t211916338::get_offset_of_AugmentationObject_7(),
	ContentManager_t211916338::get_offset_of_AnimationsManager_8(),
	ContentManager_t211916338::get_offset_of_JsonServerUrl_9(),
	ContentManager_t211916338::get_offset_of_mIsShowingBookData_10(),
	ContentManager_t211916338::get_offset_of_mIsLoadingBookData_11(),
	ContentManager_t211916338::get_offset_of_mIsLoadingBookThumb_12(),
	ContentManager_t211916338::get_offset_of_mJsonBookInfo_13(),
	ContentManager_t211916338::get_offset_of_mBookThumb_14(),
	ContentManager_t211916338::get_offset_of_mBookData_15(),
	ContentManager_t211916338::get_offset_of_mIsBookThumbRequested_16(),
	ContentManager_t211916338::get_offset_of_mBookInformationParser_17(),
	ContentManager_t211916338::get_offset_of_mIsShowingMenu_18(),
	ContentManager_t211916338::get_offset_of_mCloudRecoBehaviour_19(),
	ContentManager_t211916338::get_offset_of_mTrackableBehaviour_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2893 = { sizeof (U3CLoadJSONBookDataU3Ec__Iterator0_t2579545968), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2893[6] = 
{
	U3CLoadJSONBookDataU3Ec__Iterator0_t2579545968::get_offset_of_jsonBookUrl_0(),
	U3CLoadJSONBookDataU3Ec__Iterator0_t2579545968::get_offset_of_U3CfullBookURLU3E__0_1(),
	U3CLoadJSONBookDataU3Ec__Iterator0_t2579545968::get_offset_of_U24this_2(),
	U3CLoadJSONBookDataU3Ec__Iterator0_t2579545968::get_offset_of_U24current_3(),
	U3CLoadJSONBookDataU3Ec__Iterator0_t2579545968::get_offset_of_U24disposing_4(),
	U3CLoadJSONBookDataU3Ec__Iterator0_t2579545968::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2894 = { sizeof (JSONParser_t75429706), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2894[8] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2895 = { sizeof (ScanLine_t269422218), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2895[3] = 
{
	ScanLine_t269422218::get_offset_of_mTime_4(),
	ScanLine_t269422218::get_offset_of_mScanDuration_5(),
	ScanLine_t269422218::get_offset_of_mMovingDown_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2896 = { sizeof (StarsRatingControl_t657076381), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2896[4] = 
{
	StarsRatingControl_t657076381::get_offset_of_Stars_4(),
	StarsRatingControl_t657076381::get_offset_of_FillTexture_5(),
	StarsRatingControl_t657076381::get_offset_of_EmptyTexture_6(),
	StarsRatingControl_t657076381::get_offset_of_mRating_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2897 = { sizeof (ActionContent_t2142700484), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2897[7] = 
{
	ActionContent_t2142700484::get_offset_of_urlImage_4(),
	ActionContent_t2142700484::get_offset_of_plane_5(),
	ActionContent_t2142700484::get_offset_of_hit_6(),
	ActionContent_t2142700484::get_offset_of_ray_7(),
	ActionContent_t2142700484::get_offset_of_hitAllTemp_8(),
	ActionContent_t2142700484::get_offset_of_collider_9(),
	ActionContent_t2142700484::get_offset_of_action_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2898 = { sizeof (U3CloadImageU3Ec__Iterator0_t987217000), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2898[13] = 
{
	U3CloadImageU3Ec__Iterator0_t987217000::get_offset_of_url_0(),
	U3CloadImageU3Ec__Iterator0_t987217000::get_offset_of_U3CwwwU3E__0_1(),
	U3CloadImageU3Ec__Iterator0_t987217000::get_offset_of_U3CrdU3E__0_2(),
	U3CloadImageU3Ec__Iterator0_t987217000::get_offset_of_posX_3(),
	U3CloadImageU3Ec__Iterator0_t987217000::get_offset_of_posY_4(),
	U3CloadImageU3Ec__Iterator0_t987217000::get_offset_of_layer_5(),
	U3CloadImageU3Ec__Iterator0_t987217000::get_offset_of_sizeX_6(),
	U3CloadImageU3Ec__Iterator0_t987217000::get_offset_of_sizeY_7(),
	U3CloadImageU3Ec__Iterator0_t987217000::get_offset_of_rotation_8(),
	U3CloadImageU3Ec__Iterator0_t987217000::get_offset_of_U24this_9(),
	U3CloadImageU3Ec__Iterator0_t987217000::get_offset_of_U24current_10(),
	U3CloadImageU3Ec__Iterator0_t987217000::get_offset_of_U24disposing_11(),
	U3CloadImageU3Ec__Iterator0_t987217000::get_offset_of_U24PC_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2899 = { sizeof (BaseMarkerApp_t2308075069), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2899[2] = 
{
	BaseMarkerApp_t2308075069::get_offset_of_isMarkedForDistroy_4(),
	BaseMarkerApp_t2308075069::get_offset_of_AppID_5(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
