﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// ActionLauncher/LaunchActionEvent
struct LaunchActionEvent_t1220164713;
// AnimationsManager
struct AnimationsManager_t2281084567;
// Click3D
struct Click3D_t85340219;
// ClickAudio
struct ClickAudio_t2208965782;
// ClickMe
struct ClickMe_t220080192;
// ClickMeChroma
struct ClickMeChroma_t2203948016;
// ClickVideo
struct ClickVideo_t284620550;
// ContentManager2
struct ContentManager2_t2980555998;
// CreditLoader
struct CreditLoader_t1693052478;
// DynamicObject/LaunchActionEvent
struct LaunchActionEvent_t193601726;
// DynamicObject/OnTransitionDoneDeleg
struct OnTransitionDoneDeleg_t1142753823;
// FullscreenVideoPlayer
struct FullscreenVideoPlayer_t1619346304;
// ImageContent
struct ImageContent_t959306442;
// LangManager
struct LangManager_t3681335332;
// MarkerContent
struct MarkerContent_t762212139;
// MarkerData
struct MarkerData_t1252592219;
// MarkerLayer[]
struct MarkerLayerU5BU5D_t4163602498;
// MarkerManager
struct MarkerManager_t1031803141;
// Page
struct Page_t2586579457;
// PageTransition
struct PageTransition_t3158442731;
// Page[]
struct PageU5BU5D_t2014447836;
// PanoForm
struct PanoForm_t499938582;
// ScanLine
struct ScanLine_t269422218;
// SimpleJSON.JSONArray
struct JSONArray_t2340361630;
// SimpleJSON.JSONNode
struct JSONNode_t2946056997;
// SimpleJSON.JSONObject
struct JSONObject_t4158403488;
// SplashTimer
struct SplashTimer_t2585461764;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// System.Byte[]
struct ByteU5BU5D_t4116647657;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Collections.Generic.Dictionary`2<System.String,SimpleJSON.JSONNode>
struct Dictionary_2_t2731313296;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t2736202052;
// System.Collections.Generic.Dictionary`2<System.String,System.String>[]
struct Dictionary_2U5BU5D_t1236182565;
// System.Collections.Generic.IEnumerator`1<SimpleJSON.JSONNode>
struct IEnumerator_1_t3378627465;
// System.Collections.Generic.List`1<Click3D>
struct List_1_t1557414961;
// System.Collections.Generic.List`1<SimpleJSON.JSONNode>
struct List_1_t123164443;
// System.DelegateData
struct DelegateData_t1677132599;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.IO.DirectoryInfo
struct DirectoryInfo_t35957480;
// System.IO.DirectoryInfo[]
struct DirectoryInfoU5BU5D_t1343554681;
// System.IO.FileInfo
struct FileInfo_t1169991790;
// System.IO.FileInfo[]
struct FileInfoU5BU5D_t2389029403;
// System.Int32[]
struct Int32U5BU5D_t385246372;
// System.Object[]
struct ObjectU5BU5D_t2843939325;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.String
struct String_t;
// System.Text.StringBuilder
struct StringBuilder_t;
// System.Void
struct Void_t1185182177;
// TMP_OpenUrl/LinkInfos[]
struct LinkInfosU5BU5D_t2629923838;
// TMPro.TMP_InputField
struct TMP_InputField_t1099764886;
// TMPro.TextMeshProUGUI
struct TextMeshProUGUI_t529313277;
// TranslationManager
struct TranslationManager_t363825059;
// UnityEngine.Animator
struct Animator_t434523843;
// UnityEngine.AudioSource
struct AudioSource_t3935305588;
// UnityEngine.Camera
struct Camera_t4157153871;
// UnityEngine.Collider
struct Collider_t1773347010;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_t2498835369;
// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t3050769227;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3328599146;
// UnityEngine.Material
struct Material_t340375123;
// UnityEngine.MeshRenderer
struct MeshRenderer_t587009260;
// UnityEngine.Networking.UnityWebRequest
struct UnityWebRequest_t463507806;
// UnityEngine.RaycastHit[]
struct RaycastHitU5BU5D_t1690781147;
// UnityEngine.RectTransform
struct RectTransform_t3704657025;
// UnityEngine.Renderer
struct Renderer_t2627027031;
// UnityEngine.Shader
struct Shader_t4151988712;
// UnityEngine.Sprite
struct Sprite_t280657092;
// UnityEngine.TextMesh
struct TextMesh_t1536577757;
// UnityEngine.Texture2D
struct Texture2D_t3840446185;
// UnityEngine.Texture[]
struct TextureU5BU5D_t908295702;
// UnityEngine.Transform
struct Transform_t3600365921;
// UnityEngine.UI.Button
struct Button_t4055032469;
// UnityEngine.UI.Image
struct Image_t2670269651;
// UnityEngine.UI.Image[]
struct ImageU5BU5D_t2439009922;
// UnityEngine.UI.InputField
struct InputField_t3762917431;
// UnityEngine.UI.MaskableGraphic[]
struct MaskableGraphicU5BU5D_t434946574;
// UnityEngine.UI.RawImage
struct RawImage_t3182918964;
// UnityEngine.UI.Scrollbar
struct Scrollbar_t1494447233;
// UnityEngine.UI.Slider
struct Slider_t3903728902;
// UnityEngine.UI.Text
struct Text_t1901882714;
// UnityEngine.UI.Toggle
struct Toggle_t2735377061;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1718750761;
// UnityEngine.Vector4[]
struct Vector4U5BU5D_t934056436;
// UnityEngine.Video.VideoClip
struct VideoClip_t1281919028;
// UnityEngine.Video.VideoPlayer
struct VideoPlayer_t1683042537;
// UnityEngine.WWW
struct WWW_t3688466362;
// UnityEngine.WWWForm
struct WWWForm_t4064702195;
// UnityEngine.WaitForSeconds
struct WaitForSeconds_t1699091251;
// Vuforia.CloudRecoBehaviour
struct CloudRecoBehaviour_t431762792;
// Vuforia.TrackableBehaviour
struct TrackableBehaviour_t1113559212;
// captureAndSendScreen
struct captureAndSendScreen_t3196082656;
// startUpLANG
struct startUpLANG_t1517666504;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CDOWNLOAD3DOBJECTU3EC__ITERATOR0_T465293986_H
#define U3CDOWNLOAD3DOBJECTU3EC__ITERATOR0_T465293986_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Click3D/<download3DObject>c__Iterator0
struct  U3Cdownload3DObjectU3Ec__Iterator0_t465293986  : public RuntimeObject
{
public:
	// Click3D Click3D/<download3DObject>c__Iterator0::$this
	Click3D_t85340219 * ___U24this_0;
	// System.Object Click3D/<download3DObject>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean Click3D/<download3DObject>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 Click3D/<download3DObject>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3Cdownload3DObjectU3Ec__Iterator0_t465293986, ___U24this_0)); }
	inline Click3D_t85340219 * get_U24this_0() const { return ___U24this_0; }
	inline Click3D_t85340219 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(Click3D_t85340219 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3Cdownload3DObjectU3Ec__Iterator0_t465293986, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3Cdownload3DObjectU3Ec__Iterator0_t465293986, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3Cdownload3DObjectU3Ec__Iterator0_t465293986, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDOWNLOAD3DOBJECTU3EC__ITERATOR0_T465293986_H
#ifndef U3CLOADIMAGEU3EC__ITERATOR0_T710270773_H
#define U3CLOADIMAGEU3EC__ITERATOR0_T710270773_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ClickAudio/<loadImage>c__Iterator0
struct  U3CloadImageU3Ec__Iterator0_t710270773  : public RuntimeObject
{
public:
	// UnityEngine.WWW ClickAudio/<loadImage>c__Iterator0::<www>__0
	WWW_t3688466362 * ___U3CwwwU3E__0_0;
	// UnityEngine.Renderer ClickAudio/<loadImage>c__Iterator0::<rd>__0
	Renderer_t2627027031 * ___U3CrdU3E__0_1;
	// System.Single ClickAudio/<loadImage>c__Iterator0::posX
	float ___posX_2;
	// System.Single ClickAudio/<loadImage>c__Iterator0::posY
	float ___posY_3;
	// System.Single ClickAudio/<loadImage>c__Iterator0::sizeX
	float ___sizeX_4;
	// System.Single ClickAudio/<loadImage>c__Iterator0::sizeY
	float ___sizeY_5;
	// System.Single ClickAudio/<loadImage>c__Iterator0::<ratio>__0
	float ___U3CratioU3E__0_6;
	// UnityEngine.WWW ClickAudio/<loadImage>c__Iterator0::<wwwAudio>__0
	WWW_t3688466362 * ___U3CwwwAudioU3E__0_7;
	// ClickAudio ClickAudio/<loadImage>c__Iterator0::$this
	ClickAudio_t2208965782 * ___U24this_8;
	// System.Object ClickAudio/<loadImage>c__Iterator0::$current
	RuntimeObject * ___U24current_9;
	// System.Boolean ClickAudio/<loadImage>c__Iterator0::$disposing
	bool ___U24disposing_10;
	// System.Int32 ClickAudio/<loadImage>c__Iterator0::$PC
	int32_t ___U24PC_11;

public:
	inline static int32_t get_offset_of_U3CwwwU3E__0_0() { return static_cast<int32_t>(offsetof(U3CloadImageU3Ec__Iterator0_t710270773, ___U3CwwwU3E__0_0)); }
	inline WWW_t3688466362 * get_U3CwwwU3E__0_0() const { return ___U3CwwwU3E__0_0; }
	inline WWW_t3688466362 ** get_address_of_U3CwwwU3E__0_0() { return &___U3CwwwU3E__0_0; }
	inline void set_U3CwwwU3E__0_0(WWW_t3688466362 * value)
	{
		___U3CwwwU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwwwU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3CrdU3E__0_1() { return static_cast<int32_t>(offsetof(U3CloadImageU3Ec__Iterator0_t710270773, ___U3CrdU3E__0_1)); }
	inline Renderer_t2627027031 * get_U3CrdU3E__0_1() const { return ___U3CrdU3E__0_1; }
	inline Renderer_t2627027031 ** get_address_of_U3CrdU3E__0_1() { return &___U3CrdU3E__0_1; }
	inline void set_U3CrdU3E__0_1(Renderer_t2627027031 * value)
	{
		___U3CrdU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrdU3E__0_1), value);
	}

	inline static int32_t get_offset_of_posX_2() { return static_cast<int32_t>(offsetof(U3CloadImageU3Ec__Iterator0_t710270773, ___posX_2)); }
	inline float get_posX_2() const { return ___posX_2; }
	inline float* get_address_of_posX_2() { return &___posX_2; }
	inline void set_posX_2(float value)
	{
		___posX_2 = value;
	}

	inline static int32_t get_offset_of_posY_3() { return static_cast<int32_t>(offsetof(U3CloadImageU3Ec__Iterator0_t710270773, ___posY_3)); }
	inline float get_posY_3() const { return ___posY_3; }
	inline float* get_address_of_posY_3() { return &___posY_3; }
	inline void set_posY_3(float value)
	{
		___posY_3 = value;
	}

	inline static int32_t get_offset_of_sizeX_4() { return static_cast<int32_t>(offsetof(U3CloadImageU3Ec__Iterator0_t710270773, ___sizeX_4)); }
	inline float get_sizeX_4() const { return ___sizeX_4; }
	inline float* get_address_of_sizeX_4() { return &___sizeX_4; }
	inline void set_sizeX_4(float value)
	{
		___sizeX_4 = value;
	}

	inline static int32_t get_offset_of_sizeY_5() { return static_cast<int32_t>(offsetof(U3CloadImageU3Ec__Iterator0_t710270773, ___sizeY_5)); }
	inline float get_sizeY_5() const { return ___sizeY_5; }
	inline float* get_address_of_sizeY_5() { return &___sizeY_5; }
	inline void set_sizeY_5(float value)
	{
		___sizeY_5 = value;
	}

	inline static int32_t get_offset_of_U3CratioU3E__0_6() { return static_cast<int32_t>(offsetof(U3CloadImageU3Ec__Iterator0_t710270773, ___U3CratioU3E__0_6)); }
	inline float get_U3CratioU3E__0_6() const { return ___U3CratioU3E__0_6; }
	inline float* get_address_of_U3CratioU3E__0_6() { return &___U3CratioU3E__0_6; }
	inline void set_U3CratioU3E__0_6(float value)
	{
		___U3CratioU3E__0_6 = value;
	}

	inline static int32_t get_offset_of_U3CwwwAudioU3E__0_7() { return static_cast<int32_t>(offsetof(U3CloadImageU3Ec__Iterator0_t710270773, ___U3CwwwAudioU3E__0_7)); }
	inline WWW_t3688466362 * get_U3CwwwAudioU3E__0_7() const { return ___U3CwwwAudioU3E__0_7; }
	inline WWW_t3688466362 ** get_address_of_U3CwwwAudioU3E__0_7() { return &___U3CwwwAudioU3E__0_7; }
	inline void set_U3CwwwAudioU3E__0_7(WWW_t3688466362 * value)
	{
		___U3CwwwAudioU3E__0_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwwwAudioU3E__0_7), value);
	}

	inline static int32_t get_offset_of_U24this_8() { return static_cast<int32_t>(offsetof(U3CloadImageU3Ec__Iterator0_t710270773, ___U24this_8)); }
	inline ClickAudio_t2208965782 * get_U24this_8() const { return ___U24this_8; }
	inline ClickAudio_t2208965782 ** get_address_of_U24this_8() { return &___U24this_8; }
	inline void set_U24this_8(ClickAudio_t2208965782 * value)
	{
		___U24this_8 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_8), value);
	}

	inline static int32_t get_offset_of_U24current_9() { return static_cast<int32_t>(offsetof(U3CloadImageU3Ec__Iterator0_t710270773, ___U24current_9)); }
	inline RuntimeObject * get_U24current_9() const { return ___U24current_9; }
	inline RuntimeObject ** get_address_of_U24current_9() { return &___U24current_9; }
	inline void set_U24current_9(RuntimeObject * value)
	{
		___U24current_9 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_9), value);
	}

	inline static int32_t get_offset_of_U24disposing_10() { return static_cast<int32_t>(offsetof(U3CloadImageU3Ec__Iterator0_t710270773, ___U24disposing_10)); }
	inline bool get_U24disposing_10() const { return ___U24disposing_10; }
	inline bool* get_address_of_U24disposing_10() { return &___U24disposing_10; }
	inline void set_U24disposing_10(bool value)
	{
		___U24disposing_10 = value;
	}

	inline static int32_t get_offset_of_U24PC_11() { return static_cast<int32_t>(offsetof(U3CloadImageU3Ec__Iterator0_t710270773, ___U24PC_11)); }
	inline int32_t get_U24PC_11() const { return ___U24PC_11; }
	inline int32_t* get_address_of_U24PC_11() { return &___U24PC_11; }
	inline void set_U24PC_11(int32_t value)
	{
		___U24PC_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADIMAGEU3EC__ITERATOR0_T710270773_H
#ifndef U3CLOADIMAGEU3EC__ITERATOR0_T3805092064_H
#define U3CLOADIMAGEU3EC__ITERATOR0_T3805092064_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ClickMe/<loadImage>c__Iterator0
struct  U3CloadImageU3Ec__Iterator0_t3805092064  : public RuntimeObject
{
public:
	// UnityEngine.WWW ClickMe/<loadImage>c__Iterator0::<www>__0
	WWW_t3688466362 * ___U3CwwwU3E__0_0;
	// UnityEngine.Renderer ClickMe/<loadImage>c__Iterator0::<rd>__0
	Renderer_t2627027031 * ___U3CrdU3E__0_1;
	// System.Single ClickMe/<loadImage>c__Iterator0::posX
	float ___posX_2;
	// System.Single ClickMe/<loadImage>c__Iterator0::posY
	float ___posY_3;
	// System.Single ClickMe/<loadImage>c__Iterator0::sizeX
	float ___sizeX_4;
	// System.Single ClickMe/<loadImage>c__Iterator0::sizeY
	float ___sizeY_5;
	// System.Single ClickMe/<loadImage>c__Iterator0::<ratio>__0
	float ___U3CratioU3E__0_6;
	// ClickMe ClickMe/<loadImage>c__Iterator0::$this
	ClickMe_t220080192 * ___U24this_7;
	// System.Object ClickMe/<loadImage>c__Iterator0::$current
	RuntimeObject * ___U24current_8;
	// System.Boolean ClickMe/<loadImage>c__Iterator0::$disposing
	bool ___U24disposing_9;
	// System.Int32 ClickMe/<loadImage>c__Iterator0::$PC
	int32_t ___U24PC_10;

public:
	inline static int32_t get_offset_of_U3CwwwU3E__0_0() { return static_cast<int32_t>(offsetof(U3CloadImageU3Ec__Iterator0_t3805092064, ___U3CwwwU3E__0_0)); }
	inline WWW_t3688466362 * get_U3CwwwU3E__0_0() const { return ___U3CwwwU3E__0_0; }
	inline WWW_t3688466362 ** get_address_of_U3CwwwU3E__0_0() { return &___U3CwwwU3E__0_0; }
	inline void set_U3CwwwU3E__0_0(WWW_t3688466362 * value)
	{
		___U3CwwwU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwwwU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3CrdU3E__0_1() { return static_cast<int32_t>(offsetof(U3CloadImageU3Ec__Iterator0_t3805092064, ___U3CrdU3E__0_1)); }
	inline Renderer_t2627027031 * get_U3CrdU3E__0_1() const { return ___U3CrdU3E__0_1; }
	inline Renderer_t2627027031 ** get_address_of_U3CrdU3E__0_1() { return &___U3CrdU3E__0_1; }
	inline void set_U3CrdU3E__0_1(Renderer_t2627027031 * value)
	{
		___U3CrdU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrdU3E__0_1), value);
	}

	inline static int32_t get_offset_of_posX_2() { return static_cast<int32_t>(offsetof(U3CloadImageU3Ec__Iterator0_t3805092064, ___posX_2)); }
	inline float get_posX_2() const { return ___posX_2; }
	inline float* get_address_of_posX_2() { return &___posX_2; }
	inline void set_posX_2(float value)
	{
		___posX_2 = value;
	}

	inline static int32_t get_offset_of_posY_3() { return static_cast<int32_t>(offsetof(U3CloadImageU3Ec__Iterator0_t3805092064, ___posY_3)); }
	inline float get_posY_3() const { return ___posY_3; }
	inline float* get_address_of_posY_3() { return &___posY_3; }
	inline void set_posY_3(float value)
	{
		___posY_3 = value;
	}

	inline static int32_t get_offset_of_sizeX_4() { return static_cast<int32_t>(offsetof(U3CloadImageU3Ec__Iterator0_t3805092064, ___sizeX_4)); }
	inline float get_sizeX_4() const { return ___sizeX_4; }
	inline float* get_address_of_sizeX_4() { return &___sizeX_4; }
	inline void set_sizeX_4(float value)
	{
		___sizeX_4 = value;
	}

	inline static int32_t get_offset_of_sizeY_5() { return static_cast<int32_t>(offsetof(U3CloadImageU3Ec__Iterator0_t3805092064, ___sizeY_5)); }
	inline float get_sizeY_5() const { return ___sizeY_5; }
	inline float* get_address_of_sizeY_5() { return &___sizeY_5; }
	inline void set_sizeY_5(float value)
	{
		___sizeY_5 = value;
	}

	inline static int32_t get_offset_of_U3CratioU3E__0_6() { return static_cast<int32_t>(offsetof(U3CloadImageU3Ec__Iterator0_t3805092064, ___U3CratioU3E__0_6)); }
	inline float get_U3CratioU3E__0_6() const { return ___U3CratioU3E__0_6; }
	inline float* get_address_of_U3CratioU3E__0_6() { return &___U3CratioU3E__0_6; }
	inline void set_U3CratioU3E__0_6(float value)
	{
		___U3CratioU3E__0_6 = value;
	}

	inline static int32_t get_offset_of_U24this_7() { return static_cast<int32_t>(offsetof(U3CloadImageU3Ec__Iterator0_t3805092064, ___U24this_7)); }
	inline ClickMe_t220080192 * get_U24this_7() const { return ___U24this_7; }
	inline ClickMe_t220080192 ** get_address_of_U24this_7() { return &___U24this_7; }
	inline void set_U24this_7(ClickMe_t220080192 * value)
	{
		___U24this_7 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_7), value);
	}

	inline static int32_t get_offset_of_U24current_8() { return static_cast<int32_t>(offsetof(U3CloadImageU3Ec__Iterator0_t3805092064, ___U24current_8)); }
	inline RuntimeObject * get_U24current_8() const { return ___U24current_8; }
	inline RuntimeObject ** get_address_of_U24current_8() { return &___U24current_8; }
	inline void set_U24current_8(RuntimeObject * value)
	{
		___U24current_8 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_8), value);
	}

	inline static int32_t get_offset_of_U24disposing_9() { return static_cast<int32_t>(offsetof(U3CloadImageU3Ec__Iterator0_t3805092064, ___U24disposing_9)); }
	inline bool get_U24disposing_9() const { return ___U24disposing_9; }
	inline bool* get_address_of_U24disposing_9() { return &___U24disposing_9; }
	inline void set_U24disposing_9(bool value)
	{
		___U24disposing_9 = value;
	}

	inline static int32_t get_offset_of_U24PC_10() { return static_cast<int32_t>(offsetof(U3CloadImageU3Ec__Iterator0_t3805092064, ___U24PC_10)); }
	inline int32_t get_U24PC_10() const { return ___U24PC_10; }
	inline int32_t* get_address_of_U24PC_10() { return &___U24PC_10; }
	inline void set_U24PC_10(int32_t value)
	{
		___U24PC_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADIMAGEU3EC__ITERATOR0_T3805092064_H
#ifndef U3CPLAYVIDEO_IMPU3EC__ITERATOR1_T2231702528_H
#define U3CPLAYVIDEO_IMPU3EC__ITERATOR1_T2231702528_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ClickMe/<playVideo_IMP>c__Iterator1
struct  U3CplayVideo_IMPU3Ec__Iterator1_t2231702528  : public RuntimeObject
{
public:
	// ClickMe ClickMe/<playVideo_IMP>c__Iterator1::$this
	ClickMe_t220080192 * ___U24this_0;
	// System.Object ClickMe/<playVideo_IMP>c__Iterator1::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean ClickMe/<playVideo_IMP>c__Iterator1::$disposing
	bool ___U24disposing_2;
	// System.Int32 ClickMe/<playVideo_IMP>c__Iterator1::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CplayVideo_IMPU3Ec__Iterator1_t2231702528, ___U24this_0)); }
	inline ClickMe_t220080192 * get_U24this_0() const { return ___U24this_0; }
	inline ClickMe_t220080192 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(ClickMe_t220080192 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CplayVideo_IMPU3Ec__Iterator1_t2231702528, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CplayVideo_IMPU3Ec__Iterator1_t2231702528, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CplayVideo_IMPU3Ec__Iterator1_t2231702528, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPLAYVIDEO_IMPU3EC__ITERATOR1_T2231702528_H
#ifndef U3CLOADIMAGEU3EC__ITERATOR0_T689634614_H
#define U3CLOADIMAGEU3EC__ITERATOR0_T689634614_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ClickMeChroma/<loadImage>c__Iterator0
struct  U3CloadImageU3Ec__Iterator0_t689634614  : public RuntimeObject
{
public:
	// UnityEngine.WWW ClickMeChroma/<loadImage>c__Iterator0::<www>__0
	WWW_t3688466362 * ___U3CwwwU3E__0_0;
	// UnityEngine.Renderer ClickMeChroma/<loadImage>c__Iterator0::<rd>__0
	Renderer_t2627027031 * ___U3CrdU3E__0_1;
	// System.Single ClickMeChroma/<loadImage>c__Iterator0::posX
	float ___posX_2;
	// System.Single ClickMeChroma/<loadImage>c__Iterator0::posY
	float ___posY_3;
	// System.Single ClickMeChroma/<loadImage>c__Iterator0::sizeX
	float ___sizeX_4;
	// System.Single ClickMeChroma/<loadImage>c__Iterator0::sizeY
	float ___sizeY_5;
	// System.Single ClickMeChroma/<loadImage>c__Iterator0::<ratio>__0
	float ___U3CratioU3E__0_6;
	// System.Single ClickMeChroma/<loadImage>c__Iterator0::inclinaison
	float ___inclinaison_7;
	// ClickMeChroma ClickMeChroma/<loadImage>c__Iterator0::$this
	ClickMeChroma_t2203948016 * ___U24this_8;
	// System.Object ClickMeChroma/<loadImage>c__Iterator0::$current
	RuntimeObject * ___U24current_9;
	// System.Boolean ClickMeChroma/<loadImage>c__Iterator0::$disposing
	bool ___U24disposing_10;
	// System.Int32 ClickMeChroma/<loadImage>c__Iterator0::$PC
	int32_t ___U24PC_11;

public:
	inline static int32_t get_offset_of_U3CwwwU3E__0_0() { return static_cast<int32_t>(offsetof(U3CloadImageU3Ec__Iterator0_t689634614, ___U3CwwwU3E__0_0)); }
	inline WWW_t3688466362 * get_U3CwwwU3E__0_0() const { return ___U3CwwwU3E__0_0; }
	inline WWW_t3688466362 ** get_address_of_U3CwwwU3E__0_0() { return &___U3CwwwU3E__0_0; }
	inline void set_U3CwwwU3E__0_0(WWW_t3688466362 * value)
	{
		___U3CwwwU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwwwU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3CrdU3E__0_1() { return static_cast<int32_t>(offsetof(U3CloadImageU3Ec__Iterator0_t689634614, ___U3CrdU3E__0_1)); }
	inline Renderer_t2627027031 * get_U3CrdU3E__0_1() const { return ___U3CrdU3E__0_1; }
	inline Renderer_t2627027031 ** get_address_of_U3CrdU3E__0_1() { return &___U3CrdU3E__0_1; }
	inline void set_U3CrdU3E__0_1(Renderer_t2627027031 * value)
	{
		___U3CrdU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrdU3E__0_1), value);
	}

	inline static int32_t get_offset_of_posX_2() { return static_cast<int32_t>(offsetof(U3CloadImageU3Ec__Iterator0_t689634614, ___posX_2)); }
	inline float get_posX_2() const { return ___posX_2; }
	inline float* get_address_of_posX_2() { return &___posX_2; }
	inline void set_posX_2(float value)
	{
		___posX_2 = value;
	}

	inline static int32_t get_offset_of_posY_3() { return static_cast<int32_t>(offsetof(U3CloadImageU3Ec__Iterator0_t689634614, ___posY_3)); }
	inline float get_posY_3() const { return ___posY_3; }
	inline float* get_address_of_posY_3() { return &___posY_3; }
	inline void set_posY_3(float value)
	{
		___posY_3 = value;
	}

	inline static int32_t get_offset_of_sizeX_4() { return static_cast<int32_t>(offsetof(U3CloadImageU3Ec__Iterator0_t689634614, ___sizeX_4)); }
	inline float get_sizeX_4() const { return ___sizeX_4; }
	inline float* get_address_of_sizeX_4() { return &___sizeX_4; }
	inline void set_sizeX_4(float value)
	{
		___sizeX_4 = value;
	}

	inline static int32_t get_offset_of_sizeY_5() { return static_cast<int32_t>(offsetof(U3CloadImageU3Ec__Iterator0_t689634614, ___sizeY_5)); }
	inline float get_sizeY_5() const { return ___sizeY_5; }
	inline float* get_address_of_sizeY_5() { return &___sizeY_5; }
	inline void set_sizeY_5(float value)
	{
		___sizeY_5 = value;
	}

	inline static int32_t get_offset_of_U3CratioU3E__0_6() { return static_cast<int32_t>(offsetof(U3CloadImageU3Ec__Iterator0_t689634614, ___U3CratioU3E__0_6)); }
	inline float get_U3CratioU3E__0_6() const { return ___U3CratioU3E__0_6; }
	inline float* get_address_of_U3CratioU3E__0_6() { return &___U3CratioU3E__0_6; }
	inline void set_U3CratioU3E__0_6(float value)
	{
		___U3CratioU3E__0_6 = value;
	}

	inline static int32_t get_offset_of_inclinaison_7() { return static_cast<int32_t>(offsetof(U3CloadImageU3Ec__Iterator0_t689634614, ___inclinaison_7)); }
	inline float get_inclinaison_7() const { return ___inclinaison_7; }
	inline float* get_address_of_inclinaison_7() { return &___inclinaison_7; }
	inline void set_inclinaison_7(float value)
	{
		___inclinaison_7 = value;
	}

	inline static int32_t get_offset_of_U24this_8() { return static_cast<int32_t>(offsetof(U3CloadImageU3Ec__Iterator0_t689634614, ___U24this_8)); }
	inline ClickMeChroma_t2203948016 * get_U24this_8() const { return ___U24this_8; }
	inline ClickMeChroma_t2203948016 ** get_address_of_U24this_8() { return &___U24this_8; }
	inline void set_U24this_8(ClickMeChroma_t2203948016 * value)
	{
		___U24this_8 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_8), value);
	}

	inline static int32_t get_offset_of_U24current_9() { return static_cast<int32_t>(offsetof(U3CloadImageU3Ec__Iterator0_t689634614, ___U24current_9)); }
	inline RuntimeObject * get_U24current_9() const { return ___U24current_9; }
	inline RuntimeObject ** get_address_of_U24current_9() { return &___U24current_9; }
	inline void set_U24current_9(RuntimeObject * value)
	{
		___U24current_9 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_9), value);
	}

	inline static int32_t get_offset_of_U24disposing_10() { return static_cast<int32_t>(offsetof(U3CloadImageU3Ec__Iterator0_t689634614, ___U24disposing_10)); }
	inline bool get_U24disposing_10() const { return ___U24disposing_10; }
	inline bool* get_address_of_U24disposing_10() { return &___U24disposing_10; }
	inline void set_U24disposing_10(bool value)
	{
		___U24disposing_10 = value;
	}

	inline static int32_t get_offset_of_U24PC_11() { return static_cast<int32_t>(offsetof(U3CloadImageU3Ec__Iterator0_t689634614, ___U24PC_11)); }
	inline int32_t get_U24PC_11() const { return ___U24PC_11; }
	inline int32_t* get_address_of_U24PC_11() { return &___U24PC_11; }
	inline void set_U24PC_11(int32_t value)
	{
		___U24PC_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADIMAGEU3EC__ITERATOR0_T689634614_H
#ifndef U3CPLAYVIDEO_IMPU3EC__ITERATOR2_T2424110195_H
#define U3CPLAYVIDEO_IMPU3EC__ITERATOR2_T2424110195_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ClickMeChroma/<playVideo_IMP>c__Iterator2
struct  U3CplayVideo_IMPU3Ec__Iterator2_t2424110195  : public RuntimeObject
{
public:
	// ClickMeChroma ClickMeChroma/<playVideo_IMP>c__Iterator2::$this
	ClickMeChroma_t2203948016 * ___U24this_0;
	// System.Object ClickMeChroma/<playVideo_IMP>c__Iterator2::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean ClickMeChroma/<playVideo_IMP>c__Iterator2::$disposing
	bool ___U24disposing_2;
	// System.Int32 ClickMeChroma/<playVideo_IMP>c__Iterator2::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CplayVideo_IMPU3Ec__Iterator2_t2424110195, ___U24this_0)); }
	inline ClickMeChroma_t2203948016 * get_U24this_0() const { return ___U24this_0; }
	inline ClickMeChroma_t2203948016 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(ClickMeChroma_t2203948016 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CplayVideo_IMPU3Ec__Iterator2_t2424110195, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CplayVideo_IMPU3Ec__Iterator2_t2424110195, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CplayVideo_IMPU3Ec__Iterator2_t2424110195, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPLAYVIDEO_IMPU3EC__ITERATOR2_T2424110195_H
#ifndef U3CPREPAREVIDEO_IMPU3EC__ITERATOR1_T2281535404_H
#define U3CPREPAREVIDEO_IMPU3EC__ITERATOR1_T2281535404_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ClickMeChroma/<prepareVideo_IMP>c__Iterator1
struct  U3CprepareVideo_IMPU3Ec__Iterator1_t2281535404  : public RuntimeObject
{
public:
	// UnityEngine.WaitForSeconds ClickMeChroma/<prepareVideo_IMP>c__Iterator1::<waitTime>__0
	WaitForSeconds_t1699091251 * ___U3CwaitTimeU3E__0_0;
	// ClickMeChroma ClickMeChroma/<prepareVideo_IMP>c__Iterator1::$this
	ClickMeChroma_t2203948016 * ___U24this_1;
	// System.Object ClickMeChroma/<prepareVideo_IMP>c__Iterator1::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean ClickMeChroma/<prepareVideo_IMP>c__Iterator1::$disposing
	bool ___U24disposing_3;
	// System.Int32 ClickMeChroma/<prepareVideo_IMP>c__Iterator1::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CwaitTimeU3E__0_0() { return static_cast<int32_t>(offsetof(U3CprepareVideo_IMPU3Ec__Iterator1_t2281535404, ___U3CwaitTimeU3E__0_0)); }
	inline WaitForSeconds_t1699091251 * get_U3CwaitTimeU3E__0_0() const { return ___U3CwaitTimeU3E__0_0; }
	inline WaitForSeconds_t1699091251 ** get_address_of_U3CwaitTimeU3E__0_0() { return &___U3CwaitTimeU3E__0_0; }
	inline void set_U3CwaitTimeU3E__0_0(WaitForSeconds_t1699091251 * value)
	{
		___U3CwaitTimeU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwaitTimeU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CprepareVideo_IMPU3Ec__Iterator1_t2281535404, ___U24this_1)); }
	inline ClickMeChroma_t2203948016 * get_U24this_1() const { return ___U24this_1; }
	inline ClickMeChroma_t2203948016 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(ClickMeChroma_t2203948016 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CprepareVideo_IMPU3Ec__Iterator1_t2281535404, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CprepareVideo_IMPU3Ec__Iterator1_t2281535404, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CprepareVideo_IMPU3Ec__Iterator1_t2281535404, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPREPAREVIDEO_IMPU3EC__ITERATOR1_T2281535404_H
#ifndef U3CLOADIMAGEU3EC__ITERATOR0_T2755249628_H
#define U3CLOADIMAGEU3EC__ITERATOR0_T2755249628_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ClickVideo/<loadImage>c__Iterator0
struct  U3CloadImageU3Ec__Iterator0_t2755249628  : public RuntimeObject
{
public:
	// UnityEngine.WWW ClickVideo/<loadImage>c__Iterator0::<www>__0
	WWW_t3688466362 * ___U3CwwwU3E__0_0;
	// UnityEngine.Renderer ClickVideo/<loadImage>c__Iterator0::<rd>__0
	Renderer_t2627027031 * ___U3CrdU3E__0_1;
	// System.Single ClickVideo/<loadImage>c__Iterator0::posX
	float ___posX_2;
	// System.Single ClickVideo/<loadImage>c__Iterator0::posY
	float ___posY_3;
	// System.Single ClickVideo/<loadImage>c__Iterator0::sizeX
	float ___sizeX_4;
	// System.Single ClickVideo/<loadImage>c__Iterator0::sizeY
	float ___sizeY_5;
	// System.Single ClickVideo/<loadImage>c__Iterator0::<ratio>__0
	float ___U3CratioU3E__0_6;
	// ClickVideo ClickVideo/<loadImage>c__Iterator0::$this
	ClickVideo_t284620550 * ___U24this_7;
	// System.Object ClickVideo/<loadImage>c__Iterator0::$current
	RuntimeObject * ___U24current_8;
	// System.Boolean ClickVideo/<loadImage>c__Iterator0::$disposing
	bool ___U24disposing_9;
	// System.Int32 ClickVideo/<loadImage>c__Iterator0::$PC
	int32_t ___U24PC_10;

public:
	inline static int32_t get_offset_of_U3CwwwU3E__0_0() { return static_cast<int32_t>(offsetof(U3CloadImageU3Ec__Iterator0_t2755249628, ___U3CwwwU3E__0_0)); }
	inline WWW_t3688466362 * get_U3CwwwU3E__0_0() const { return ___U3CwwwU3E__0_0; }
	inline WWW_t3688466362 ** get_address_of_U3CwwwU3E__0_0() { return &___U3CwwwU3E__0_0; }
	inline void set_U3CwwwU3E__0_0(WWW_t3688466362 * value)
	{
		___U3CwwwU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwwwU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3CrdU3E__0_1() { return static_cast<int32_t>(offsetof(U3CloadImageU3Ec__Iterator0_t2755249628, ___U3CrdU3E__0_1)); }
	inline Renderer_t2627027031 * get_U3CrdU3E__0_1() const { return ___U3CrdU3E__0_1; }
	inline Renderer_t2627027031 ** get_address_of_U3CrdU3E__0_1() { return &___U3CrdU3E__0_1; }
	inline void set_U3CrdU3E__0_1(Renderer_t2627027031 * value)
	{
		___U3CrdU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrdU3E__0_1), value);
	}

	inline static int32_t get_offset_of_posX_2() { return static_cast<int32_t>(offsetof(U3CloadImageU3Ec__Iterator0_t2755249628, ___posX_2)); }
	inline float get_posX_2() const { return ___posX_2; }
	inline float* get_address_of_posX_2() { return &___posX_2; }
	inline void set_posX_2(float value)
	{
		___posX_2 = value;
	}

	inline static int32_t get_offset_of_posY_3() { return static_cast<int32_t>(offsetof(U3CloadImageU3Ec__Iterator0_t2755249628, ___posY_3)); }
	inline float get_posY_3() const { return ___posY_3; }
	inline float* get_address_of_posY_3() { return &___posY_3; }
	inline void set_posY_3(float value)
	{
		___posY_3 = value;
	}

	inline static int32_t get_offset_of_sizeX_4() { return static_cast<int32_t>(offsetof(U3CloadImageU3Ec__Iterator0_t2755249628, ___sizeX_4)); }
	inline float get_sizeX_4() const { return ___sizeX_4; }
	inline float* get_address_of_sizeX_4() { return &___sizeX_4; }
	inline void set_sizeX_4(float value)
	{
		___sizeX_4 = value;
	}

	inline static int32_t get_offset_of_sizeY_5() { return static_cast<int32_t>(offsetof(U3CloadImageU3Ec__Iterator0_t2755249628, ___sizeY_5)); }
	inline float get_sizeY_5() const { return ___sizeY_5; }
	inline float* get_address_of_sizeY_5() { return &___sizeY_5; }
	inline void set_sizeY_5(float value)
	{
		___sizeY_5 = value;
	}

	inline static int32_t get_offset_of_U3CratioU3E__0_6() { return static_cast<int32_t>(offsetof(U3CloadImageU3Ec__Iterator0_t2755249628, ___U3CratioU3E__0_6)); }
	inline float get_U3CratioU3E__0_6() const { return ___U3CratioU3E__0_6; }
	inline float* get_address_of_U3CratioU3E__0_6() { return &___U3CratioU3E__0_6; }
	inline void set_U3CratioU3E__0_6(float value)
	{
		___U3CratioU3E__0_6 = value;
	}

	inline static int32_t get_offset_of_U24this_7() { return static_cast<int32_t>(offsetof(U3CloadImageU3Ec__Iterator0_t2755249628, ___U24this_7)); }
	inline ClickVideo_t284620550 * get_U24this_7() const { return ___U24this_7; }
	inline ClickVideo_t284620550 ** get_address_of_U24this_7() { return &___U24this_7; }
	inline void set_U24this_7(ClickVideo_t284620550 * value)
	{
		___U24this_7 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_7), value);
	}

	inline static int32_t get_offset_of_U24current_8() { return static_cast<int32_t>(offsetof(U3CloadImageU3Ec__Iterator0_t2755249628, ___U24current_8)); }
	inline RuntimeObject * get_U24current_8() const { return ___U24current_8; }
	inline RuntimeObject ** get_address_of_U24current_8() { return &___U24current_8; }
	inline void set_U24current_8(RuntimeObject * value)
	{
		___U24current_8 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_8), value);
	}

	inline static int32_t get_offset_of_U24disposing_9() { return static_cast<int32_t>(offsetof(U3CloadImageU3Ec__Iterator0_t2755249628, ___U24disposing_9)); }
	inline bool get_U24disposing_9() const { return ___U24disposing_9; }
	inline bool* get_address_of_U24disposing_9() { return &___U24disposing_9; }
	inline void set_U24disposing_9(bool value)
	{
		___U24disposing_9 = value;
	}

	inline static int32_t get_offset_of_U24PC_10() { return static_cast<int32_t>(offsetof(U3CloadImageU3Ec__Iterator0_t2755249628, ___U24PC_10)); }
	inline int32_t get_U24PC_10() const { return ___U24PC_10; }
	inline int32_t* get_address_of_U24PC_10() { return &___U24PC_10; }
	inline void set_U24PC_10(int32_t value)
	{
		___U24PC_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADIMAGEU3EC__ITERATOR0_T2755249628_H
#ifndef U3CPREPAREVIDEO_IMPU3EC__ITERATOR1_T2339890399_H
#define U3CPREPAREVIDEO_IMPU3EC__ITERATOR1_T2339890399_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ClickVideo/<prepareVideo_IMP>c__Iterator1
struct  U3CprepareVideo_IMPU3Ec__Iterator1_t2339890399  : public RuntimeObject
{
public:
	// UnityEngine.WaitForSeconds ClickVideo/<prepareVideo_IMP>c__Iterator1::<waitTime>__0
	WaitForSeconds_t1699091251 * ___U3CwaitTimeU3E__0_0;
	// ClickVideo ClickVideo/<prepareVideo_IMP>c__Iterator1::$this
	ClickVideo_t284620550 * ___U24this_1;
	// System.Object ClickVideo/<prepareVideo_IMP>c__Iterator1::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean ClickVideo/<prepareVideo_IMP>c__Iterator1::$disposing
	bool ___U24disposing_3;
	// System.Int32 ClickVideo/<prepareVideo_IMP>c__Iterator1::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CwaitTimeU3E__0_0() { return static_cast<int32_t>(offsetof(U3CprepareVideo_IMPU3Ec__Iterator1_t2339890399, ___U3CwaitTimeU3E__0_0)); }
	inline WaitForSeconds_t1699091251 * get_U3CwaitTimeU3E__0_0() const { return ___U3CwaitTimeU3E__0_0; }
	inline WaitForSeconds_t1699091251 ** get_address_of_U3CwaitTimeU3E__0_0() { return &___U3CwaitTimeU3E__0_0; }
	inline void set_U3CwaitTimeU3E__0_0(WaitForSeconds_t1699091251 * value)
	{
		___U3CwaitTimeU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwaitTimeU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CprepareVideo_IMPU3Ec__Iterator1_t2339890399, ___U24this_1)); }
	inline ClickVideo_t284620550 * get_U24this_1() const { return ___U24this_1; }
	inline ClickVideo_t284620550 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(ClickVideo_t284620550 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CprepareVideo_IMPU3Ec__Iterator1_t2339890399, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CprepareVideo_IMPU3Ec__Iterator1_t2339890399, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CprepareVideo_IMPU3Ec__Iterator1_t2339890399, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPREPAREVIDEO_IMPU3EC__ITERATOR1_T2339890399_H
#ifndef U3CIMPL_SENDCAPTUREU3EC__ITERATOR1_T421012197_H
#define U3CIMPL_SENDCAPTUREU3EC__ITERATOR1_T421012197_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ContentManager2/<IMPL_sendCapture>c__Iterator1
struct  U3CIMPL_sendCaptureU3Ec__Iterator1_t421012197  : public RuntimeObject
{
public:
	// UnityEngine.Renderer ContentManager2/<IMPL_sendCapture>c__Iterator1::<rendererBack>__1
	Renderer_t2627027031 * ___U3CrendererBackU3E__1_0;
	// UnityEngine.Texture2D ContentManager2/<IMPL_sendCapture>c__Iterator1::<tex>__1
	Texture2D_t3840446185 * ___U3CtexU3E__1_1;
	// UnityEngine.Texture2D ContentManager2/<IMPL_sendCapture>c__Iterator1::<tex2>__1
	Texture2D_t3840446185 * ___U3Ctex2U3E__1_2;
	// System.Byte[] ContentManager2/<IMPL_sendCapture>c__Iterator1::<bytes>__1
	ByteU5BU5D_t4116647657* ___U3CbytesU3E__1_3;
	// UnityEngine.WWWForm ContentManager2/<IMPL_sendCapture>c__Iterator1::<form>__1
	WWWForm_t4064702195 * ___U3CformU3E__1_4;
	// UnityEngine.Networking.UnityWebRequest ContentManager2/<IMPL_sendCapture>c__Iterator1::<w>__1
	UnityWebRequest_t463507806 * ___U3CwU3E__1_5;
	// ContentManager2 ContentManager2/<IMPL_sendCapture>c__Iterator1::$this
	ContentManager2_t2980555998 * ___U24this_6;
	// System.Object ContentManager2/<IMPL_sendCapture>c__Iterator1::$current
	RuntimeObject * ___U24current_7;
	// System.Boolean ContentManager2/<IMPL_sendCapture>c__Iterator1::$disposing
	bool ___U24disposing_8;
	// System.Int32 ContentManager2/<IMPL_sendCapture>c__Iterator1::$PC
	int32_t ___U24PC_9;

public:
	inline static int32_t get_offset_of_U3CrendererBackU3E__1_0() { return static_cast<int32_t>(offsetof(U3CIMPL_sendCaptureU3Ec__Iterator1_t421012197, ___U3CrendererBackU3E__1_0)); }
	inline Renderer_t2627027031 * get_U3CrendererBackU3E__1_0() const { return ___U3CrendererBackU3E__1_0; }
	inline Renderer_t2627027031 ** get_address_of_U3CrendererBackU3E__1_0() { return &___U3CrendererBackU3E__1_0; }
	inline void set_U3CrendererBackU3E__1_0(Renderer_t2627027031 * value)
	{
		___U3CrendererBackU3E__1_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrendererBackU3E__1_0), value);
	}

	inline static int32_t get_offset_of_U3CtexU3E__1_1() { return static_cast<int32_t>(offsetof(U3CIMPL_sendCaptureU3Ec__Iterator1_t421012197, ___U3CtexU3E__1_1)); }
	inline Texture2D_t3840446185 * get_U3CtexU3E__1_1() const { return ___U3CtexU3E__1_1; }
	inline Texture2D_t3840446185 ** get_address_of_U3CtexU3E__1_1() { return &___U3CtexU3E__1_1; }
	inline void set_U3CtexU3E__1_1(Texture2D_t3840446185 * value)
	{
		___U3CtexU3E__1_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtexU3E__1_1), value);
	}

	inline static int32_t get_offset_of_U3Ctex2U3E__1_2() { return static_cast<int32_t>(offsetof(U3CIMPL_sendCaptureU3Ec__Iterator1_t421012197, ___U3Ctex2U3E__1_2)); }
	inline Texture2D_t3840446185 * get_U3Ctex2U3E__1_2() const { return ___U3Ctex2U3E__1_2; }
	inline Texture2D_t3840446185 ** get_address_of_U3Ctex2U3E__1_2() { return &___U3Ctex2U3E__1_2; }
	inline void set_U3Ctex2U3E__1_2(Texture2D_t3840446185 * value)
	{
		___U3Ctex2U3E__1_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3Ctex2U3E__1_2), value);
	}

	inline static int32_t get_offset_of_U3CbytesU3E__1_3() { return static_cast<int32_t>(offsetof(U3CIMPL_sendCaptureU3Ec__Iterator1_t421012197, ___U3CbytesU3E__1_3)); }
	inline ByteU5BU5D_t4116647657* get_U3CbytesU3E__1_3() const { return ___U3CbytesU3E__1_3; }
	inline ByteU5BU5D_t4116647657** get_address_of_U3CbytesU3E__1_3() { return &___U3CbytesU3E__1_3; }
	inline void set_U3CbytesU3E__1_3(ByteU5BU5D_t4116647657* value)
	{
		___U3CbytesU3E__1_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CbytesU3E__1_3), value);
	}

	inline static int32_t get_offset_of_U3CformU3E__1_4() { return static_cast<int32_t>(offsetof(U3CIMPL_sendCaptureU3Ec__Iterator1_t421012197, ___U3CformU3E__1_4)); }
	inline WWWForm_t4064702195 * get_U3CformU3E__1_4() const { return ___U3CformU3E__1_4; }
	inline WWWForm_t4064702195 ** get_address_of_U3CformU3E__1_4() { return &___U3CformU3E__1_4; }
	inline void set_U3CformU3E__1_4(WWWForm_t4064702195 * value)
	{
		___U3CformU3E__1_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CformU3E__1_4), value);
	}

	inline static int32_t get_offset_of_U3CwU3E__1_5() { return static_cast<int32_t>(offsetof(U3CIMPL_sendCaptureU3Ec__Iterator1_t421012197, ___U3CwU3E__1_5)); }
	inline UnityWebRequest_t463507806 * get_U3CwU3E__1_5() const { return ___U3CwU3E__1_5; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CwU3E__1_5() { return &___U3CwU3E__1_5; }
	inline void set_U3CwU3E__1_5(UnityWebRequest_t463507806 * value)
	{
		___U3CwU3E__1_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwU3E__1_5), value);
	}

	inline static int32_t get_offset_of_U24this_6() { return static_cast<int32_t>(offsetof(U3CIMPL_sendCaptureU3Ec__Iterator1_t421012197, ___U24this_6)); }
	inline ContentManager2_t2980555998 * get_U24this_6() const { return ___U24this_6; }
	inline ContentManager2_t2980555998 ** get_address_of_U24this_6() { return &___U24this_6; }
	inline void set_U24this_6(ContentManager2_t2980555998 * value)
	{
		___U24this_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_6), value);
	}

	inline static int32_t get_offset_of_U24current_7() { return static_cast<int32_t>(offsetof(U3CIMPL_sendCaptureU3Ec__Iterator1_t421012197, ___U24current_7)); }
	inline RuntimeObject * get_U24current_7() const { return ___U24current_7; }
	inline RuntimeObject ** get_address_of_U24current_7() { return &___U24current_7; }
	inline void set_U24current_7(RuntimeObject * value)
	{
		___U24current_7 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_7), value);
	}

	inline static int32_t get_offset_of_U24disposing_8() { return static_cast<int32_t>(offsetof(U3CIMPL_sendCaptureU3Ec__Iterator1_t421012197, ___U24disposing_8)); }
	inline bool get_U24disposing_8() const { return ___U24disposing_8; }
	inline bool* get_address_of_U24disposing_8() { return &___U24disposing_8; }
	inline void set_U24disposing_8(bool value)
	{
		___U24disposing_8 = value;
	}

	inline static int32_t get_offset_of_U24PC_9() { return static_cast<int32_t>(offsetof(U3CIMPL_sendCaptureU3Ec__Iterator1_t421012197, ___U24PC_9)); }
	inline int32_t get_U24PC_9() const { return ___U24PC_9; }
	inline int32_t* get_address_of_U24PC_9() { return &___U24PC_9; }
	inline void set_U24PC_9(int32_t value)
	{
		___U24PC_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CIMPL_SENDCAPTUREU3EC__ITERATOR1_T421012197_H
#ifndef U3CGET_CONTENTU3EC__ITERATOR0_T243170899_H
#define U3CGET_CONTENTU3EC__ITERATOR0_T243170899_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ContentManager2/<get_content>c__Iterator0
struct  U3Cget_contentU3Ec__Iterator0_t243170899  : public RuntimeObject
{
public:
	// System.String ContentManager2/<get_content>c__Iterator0::markerUID
	String_t* ___markerUID_0;
	// UnityEngine.GameObject ContentManager2/<get_content>c__Iterator0::<calendar>__1
	GameObject_t1113636619 * ___U3CcalendarU3E__1_1;
	// System.String ContentManager2/<get_content>c__Iterator0::<url>__2
	String_t* ___U3CurlU3E__2_2;
	// UnityEngine.WWW ContentManager2/<get_content>c__Iterator0::<www>__2
	WWW_t3688466362 * ___U3CwwwU3E__2_3;
	// MarkerManager ContentManager2/<get_content>c__Iterator0::<mng>__2
	MarkerManager_t1031803141 * ___U3CmngU3E__2_4;
	// System.Single ContentManager2/<get_content>c__Iterator0::<layer>__2
	float ___U3ClayerU3E__2_5;
	// System.Single ContentManager2/<get_content>c__Iterator0::<Rt>__2
	float ___U3CRtU3E__2_6;
	// System.Int32 ContentManager2/<get_content>c__Iterator0::<index3DObj>__2
	int32_t ___U3Cindex3DObjU3E__2_7;
	// ContentManager2 ContentManager2/<get_content>c__Iterator0::$this
	ContentManager2_t2980555998 * ___U24this_8;
	// System.Object ContentManager2/<get_content>c__Iterator0::$current
	RuntimeObject * ___U24current_9;
	// System.Boolean ContentManager2/<get_content>c__Iterator0::$disposing
	bool ___U24disposing_10;
	// System.Int32 ContentManager2/<get_content>c__Iterator0::$PC
	int32_t ___U24PC_11;

public:
	inline static int32_t get_offset_of_markerUID_0() { return static_cast<int32_t>(offsetof(U3Cget_contentU3Ec__Iterator0_t243170899, ___markerUID_0)); }
	inline String_t* get_markerUID_0() const { return ___markerUID_0; }
	inline String_t** get_address_of_markerUID_0() { return &___markerUID_0; }
	inline void set_markerUID_0(String_t* value)
	{
		___markerUID_0 = value;
		Il2CppCodeGenWriteBarrier((&___markerUID_0), value);
	}

	inline static int32_t get_offset_of_U3CcalendarU3E__1_1() { return static_cast<int32_t>(offsetof(U3Cget_contentU3Ec__Iterator0_t243170899, ___U3CcalendarU3E__1_1)); }
	inline GameObject_t1113636619 * get_U3CcalendarU3E__1_1() const { return ___U3CcalendarU3E__1_1; }
	inline GameObject_t1113636619 ** get_address_of_U3CcalendarU3E__1_1() { return &___U3CcalendarU3E__1_1; }
	inline void set_U3CcalendarU3E__1_1(GameObject_t1113636619 * value)
	{
		___U3CcalendarU3E__1_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcalendarU3E__1_1), value);
	}

	inline static int32_t get_offset_of_U3CurlU3E__2_2() { return static_cast<int32_t>(offsetof(U3Cget_contentU3Ec__Iterator0_t243170899, ___U3CurlU3E__2_2)); }
	inline String_t* get_U3CurlU3E__2_2() const { return ___U3CurlU3E__2_2; }
	inline String_t** get_address_of_U3CurlU3E__2_2() { return &___U3CurlU3E__2_2; }
	inline void set_U3CurlU3E__2_2(String_t* value)
	{
		___U3CurlU3E__2_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CurlU3E__2_2), value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E__2_3() { return static_cast<int32_t>(offsetof(U3Cget_contentU3Ec__Iterator0_t243170899, ___U3CwwwU3E__2_3)); }
	inline WWW_t3688466362 * get_U3CwwwU3E__2_3() const { return ___U3CwwwU3E__2_3; }
	inline WWW_t3688466362 ** get_address_of_U3CwwwU3E__2_3() { return &___U3CwwwU3E__2_3; }
	inline void set_U3CwwwU3E__2_3(WWW_t3688466362 * value)
	{
		___U3CwwwU3E__2_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwwwU3E__2_3), value);
	}

	inline static int32_t get_offset_of_U3CmngU3E__2_4() { return static_cast<int32_t>(offsetof(U3Cget_contentU3Ec__Iterator0_t243170899, ___U3CmngU3E__2_4)); }
	inline MarkerManager_t1031803141 * get_U3CmngU3E__2_4() const { return ___U3CmngU3E__2_4; }
	inline MarkerManager_t1031803141 ** get_address_of_U3CmngU3E__2_4() { return &___U3CmngU3E__2_4; }
	inline void set_U3CmngU3E__2_4(MarkerManager_t1031803141 * value)
	{
		___U3CmngU3E__2_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmngU3E__2_4), value);
	}

	inline static int32_t get_offset_of_U3ClayerU3E__2_5() { return static_cast<int32_t>(offsetof(U3Cget_contentU3Ec__Iterator0_t243170899, ___U3ClayerU3E__2_5)); }
	inline float get_U3ClayerU3E__2_5() const { return ___U3ClayerU3E__2_5; }
	inline float* get_address_of_U3ClayerU3E__2_5() { return &___U3ClayerU3E__2_5; }
	inline void set_U3ClayerU3E__2_5(float value)
	{
		___U3ClayerU3E__2_5 = value;
	}

	inline static int32_t get_offset_of_U3CRtU3E__2_6() { return static_cast<int32_t>(offsetof(U3Cget_contentU3Ec__Iterator0_t243170899, ___U3CRtU3E__2_6)); }
	inline float get_U3CRtU3E__2_6() const { return ___U3CRtU3E__2_6; }
	inline float* get_address_of_U3CRtU3E__2_6() { return &___U3CRtU3E__2_6; }
	inline void set_U3CRtU3E__2_6(float value)
	{
		___U3CRtU3E__2_6 = value;
	}

	inline static int32_t get_offset_of_U3Cindex3DObjU3E__2_7() { return static_cast<int32_t>(offsetof(U3Cget_contentU3Ec__Iterator0_t243170899, ___U3Cindex3DObjU3E__2_7)); }
	inline int32_t get_U3Cindex3DObjU3E__2_7() const { return ___U3Cindex3DObjU3E__2_7; }
	inline int32_t* get_address_of_U3Cindex3DObjU3E__2_7() { return &___U3Cindex3DObjU3E__2_7; }
	inline void set_U3Cindex3DObjU3E__2_7(int32_t value)
	{
		___U3Cindex3DObjU3E__2_7 = value;
	}

	inline static int32_t get_offset_of_U24this_8() { return static_cast<int32_t>(offsetof(U3Cget_contentU3Ec__Iterator0_t243170899, ___U24this_8)); }
	inline ContentManager2_t2980555998 * get_U24this_8() const { return ___U24this_8; }
	inline ContentManager2_t2980555998 ** get_address_of_U24this_8() { return &___U24this_8; }
	inline void set_U24this_8(ContentManager2_t2980555998 * value)
	{
		___U24this_8 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_8), value);
	}

	inline static int32_t get_offset_of_U24current_9() { return static_cast<int32_t>(offsetof(U3Cget_contentU3Ec__Iterator0_t243170899, ___U24current_9)); }
	inline RuntimeObject * get_U24current_9() const { return ___U24current_9; }
	inline RuntimeObject ** get_address_of_U24current_9() { return &___U24current_9; }
	inline void set_U24current_9(RuntimeObject * value)
	{
		___U24current_9 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_9), value);
	}

	inline static int32_t get_offset_of_U24disposing_10() { return static_cast<int32_t>(offsetof(U3Cget_contentU3Ec__Iterator0_t243170899, ___U24disposing_10)); }
	inline bool get_U24disposing_10() const { return ___U24disposing_10; }
	inline bool* get_address_of_U24disposing_10() { return &___U24disposing_10; }
	inline void set_U24disposing_10(bool value)
	{
		___U24disposing_10 = value;
	}

	inline static int32_t get_offset_of_U24PC_11() { return static_cast<int32_t>(offsetof(U3Cget_contentU3Ec__Iterator0_t243170899, ___U24PC_11)); }
	inline int32_t get_U24PC_11() const { return ___U24PC_11; }
	inline int32_t* get_address_of_U24PC_11() { return &___U24PC_11; }
	inline void set_U24PC_11(int32_t value)
	{
		___U24PC_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGET_CONTENTU3EC__ITERATOR0_T243170899_H
#ifndef EXTENSIONMETHODS_T729511191_H
#define EXTENSIONMETHODS_T729511191_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExtensionMethods
struct  ExtensionMethods_t729511191  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXTENSIONMETHODS_T729511191_H
#ifndef FACE3DMANAGER_T2951744723_H
#define FACE3DMANAGER_T2951744723_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Face3DManager
struct  Face3DManager_t2951744723  : public RuntimeObject
{
public:
	// System.Boolean Face3DManager::isShowingContent
	bool ___isShowingContent_1;
	// System.Collections.Generic.List`1<Click3D> Face3DManager::listFaces
	List_1_t1557414961 * ___listFaces_2;
	// System.Boolean Face3DManager::isFullscreen
	bool ___isFullscreen_3;
	// System.Int32 Face3DManager::selected
	int32_t ___selected_4;

public:
	inline static int32_t get_offset_of_isShowingContent_1() { return static_cast<int32_t>(offsetof(Face3DManager_t2951744723, ___isShowingContent_1)); }
	inline bool get_isShowingContent_1() const { return ___isShowingContent_1; }
	inline bool* get_address_of_isShowingContent_1() { return &___isShowingContent_1; }
	inline void set_isShowingContent_1(bool value)
	{
		___isShowingContent_1 = value;
	}

	inline static int32_t get_offset_of_listFaces_2() { return static_cast<int32_t>(offsetof(Face3DManager_t2951744723, ___listFaces_2)); }
	inline List_1_t1557414961 * get_listFaces_2() const { return ___listFaces_2; }
	inline List_1_t1557414961 ** get_address_of_listFaces_2() { return &___listFaces_2; }
	inline void set_listFaces_2(List_1_t1557414961 * value)
	{
		___listFaces_2 = value;
		Il2CppCodeGenWriteBarrier((&___listFaces_2), value);
	}

	inline static int32_t get_offset_of_isFullscreen_3() { return static_cast<int32_t>(offsetof(Face3DManager_t2951744723, ___isFullscreen_3)); }
	inline bool get_isFullscreen_3() const { return ___isFullscreen_3; }
	inline bool* get_address_of_isFullscreen_3() { return &___isFullscreen_3; }
	inline void set_isFullscreen_3(bool value)
	{
		___isFullscreen_3 = value;
	}

	inline static int32_t get_offset_of_selected_4() { return static_cast<int32_t>(offsetof(Face3DManager_t2951744723, ___selected_4)); }
	inline int32_t get_selected_4() const { return ___selected_4; }
	inline int32_t* get_address_of_selected_4() { return &___selected_4; }
	inline void set_selected_4(int32_t value)
	{
		___selected_4 = value;
	}
};

struct Face3DManager_t2951744723_StaticFields
{
public:
	// Face3DManager Face3DManager::_Instance
	Face3DManager_t2951744723 * ____Instance_0;

public:
	inline static int32_t get_offset_of__Instance_0() { return static_cast<int32_t>(offsetof(Face3DManager_t2951744723_StaticFields, ____Instance_0)); }
	inline Face3DManager_t2951744723 * get__Instance_0() const { return ____Instance_0; }
	inline Face3DManager_t2951744723 ** get_address_of__Instance_0() { return &____Instance_0; }
	inline void set__Instance_0(Face3DManager_t2951744723 * value)
	{
		____Instance_0 = value;
		Il2CppCodeGenWriteBarrier((&____Instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FACE3DMANAGER_T2951744723_H
#ifndef U3CPLAYVIDEOU3EC__ITERATOR0_T3060609596_H
#define U3CPLAYVIDEOU3EC__ITERATOR0_T3060609596_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullscreenVideoPlayer/<playVideo>c__Iterator0
struct  U3CplayVideoU3Ec__Iterator0_t3060609596  : public RuntimeObject
{
public:
	// FullscreenVideoPlayer FullscreenVideoPlayer/<playVideo>c__Iterator0::$this
	FullscreenVideoPlayer_t1619346304 * ___U24this_0;
	// System.Object FullscreenVideoPlayer/<playVideo>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean FullscreenVideoPlayer/<playVideo>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 FullscreenVideoPlayer/<playVideo>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CplayVideoU3Ec__Iterator0_t3060609596, ___U24this_0)); }
	inline FullscreenVideoPlayer_t1619346304 * get_U24this_0() const { return ___U24this_0; }
	inline FullscreenVideoPlayer_t1619346304 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(FullscreenVideoPlayer_t1619346304 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CplayVideoU3Ec__Iterator0_t3060609596, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CplayVideoU3Ec__Iterator0_t3060609596, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CplayVideoU3Ec__Iterator0_t3060609596, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPLAYVIDEOU3EC__ITERATOR0_T3060609596_H
#ifndef U3CLOADIMAGEU3EC__ITERATOR0_T2022358625_H
#define U3CLOADIMAGEU3EC__ITERATOR0_T2022358625_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ImageContent/<loadImage>c__Iterator0
struct  U3CloadImageU3Ec__Iterator0_t2022358625  : public RuntimeObject
{
public:
	// System.String ImageContent/<loadImage>c__Iterator0::url
	String_t* ___url_0;
	// UnityEngine.WWW ImageContent/<loadImage>c__Iterator0::<www>__0
	WWW_t3688466362 * ___U3CwwwU3E__0_1;
	// UnityEngine.Renderer ImageContent/<loadImage>c__Iterator0::<rd>__0
	Renderer_t2627027031 * ___U3CrdU3E__0_2;
	// System.Single ImageContent/<loadImage>c__Iterator0::posX
	float ___posX_3;
	// System.Single ImageContent/<loadImage>c__Iterator0::posY
	float ___posY_4;
	// System.Single ImageContent/<loadImage>c__Iterator0::layer
	float ___layer_5;
	// System.Single ImageContent/<loadImage>c__Iterator0::sizeX
	float ___sizeX_6;
	// System.Single ImageContent/<loadImage>c__Iterator0::sizeY
	float ___sizeY_7;
	// System.Single ImageContent/<loadImage>c__Iterator0::rotation
	float ___rotation_8;
	// ImageContent ImageContent/<loadImage>c__Iterator0::$this
	ImageContent_t959306442 * ___U24this_9;
	// System.Object ImageContent/<loadImage>c__Iterator0::$current
	RuntimeObject * ___U24current_10;
	// System.Boolean ImageContent/<loadImage>c__Iterator0::$disposing
	bool ___U24disposing_11;
	// System.Int32 ImageContent/<loadImage>c__Iterator0::$PC
	int32_t ___U24PC_12;

public:
	inline static int32_t get_offset_of_url_0() { return static_cast<int32_t>(offsetof(U3CloadImageU3Ec__Iterator0_t2022358625, ___url_0)); }
	inline String_t* get_url_0() const { return ___url_0; }
	inline String_t** get_address_of_url_0() { return &___url_0; }
	inline void set_url_0(String_t* value)
	{
		___url_0 = value;
		Il2CppCodeGenWriteBarrier((&___url_0), value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E__0_1() { return static_cast<int32_t>(offsetof(U3CloadImageU3Ec__Iterator0_t2022358625, ___U3CwwwU3E__0_1)); }
	inline WWW_t3688466362 * get_U3CwwwU3E__0_1() const { return ___U3CwwwU3E__0_1; }
	inline WWW_t3688466362 ** get_address_of_U3CwwwU3E__0_1() { return &___U3CwwwU3E__0_1; }
	inline void set_U3CwwwU3E__0_1(WWW_t3688466362 * value)
	{
		___U3CwwwU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwwwU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U3CrdU3E__0_2() { return static_cast<int32_t>(offsetof(U3CloadImageU3Ec__Iterator0_t2022358625, ___U3CrdU3E__0_2)); }
	inline Renderer_t2627027031 * get_U3CrdU3E__0_2() const { return ___U3CrdU3E__0_2; }
	inline Renderer_t2627027031 ** get_address_of_U3CrdU3E__0_2() { return &___U3CrdU3E__0_2; }
	inline void set_U3CrdU3E__0_2(Renderer_t2627027031 * value)
	{
		___U3CrdU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrdU3E__0_2), value);
	}

	inline static int32_t get_offset_of_posX_3() { return static_cast<int32_t>(offsetof(U3CloadImageU3Ec__Iterator0_t2022358625, ___posX_3)); }
	inline float get_posX_3() const { return ___posX_3; }
	inline float* get_address_of_posX_3() { return &___posX_3; }
	inline void set_posX_3(float value)
	{
		___posX_3 = value;
	}

	inline static int32_t get_offset_of_posY_4() { return static_cast<int32_t>(offsetof(U3CloadImageU3Ec__Iterator0_t2022358625, ___posY_4)); }
	inline float get_posY_4() const { return ___posY_4; }
	inline float* get_address_of_posY_4() { return &___posY_4; }
	inline void set_posY_4(float value)
	{
		___posY_4 = value;
	}

	inline static int32_t get_offset_of_layer_5() { return static_cast<int32_t>(offsetof(U3CloadImageU3Ec__Iterator0_t2022358625, ___layer_5)); }
	inline float get_layer_5() const { return ___layer_5; }
	inline float* get_address_of_layer_5() { return &___layer_5; }
	inline void set_layer_5(float value)
	{
		___layer_5 = value;
	}

	inline static int32_t get_offset_of_sizeX_6() { return static_cast<int32_t>(offsetof(U3CloadImageU3Ec__Iterator0_t2022358625, ___sizeX_6)); }
	inline float get_sizeX_6() const { return ___sizeX_6; }
	inline float* get_address_of_sizeX_6() { return &___sizeX_6; }
	inline void set_sizeX_6(float value)
	{
		___sizeX_6 = value;
	}

	inline static int32_t get_offset_of_sizeY_7() { return static_cast<int32_t>(offsetof(U3CloadImageU3Ec__Iterator0_t2022358625, ___sizeY_7)); }
	inline float get_sizeY_7() const { return ___sizeY_7; }
	inline float* get_address_of_sizeY_7() { return &___sizeY_7; }
	inline void set_sizeY_7(float value)
	{
		___sizeY_7 = value;
	}

	inline static int32_t get_offset_of_rotation_8() { return static_cast<int32_t>(offsetof(U3CloadImageU3Ec__Iterator0_t2022358625, ___rotation_8)); }
	inline float get_rotation_8() const { return ___rotation_8; }
	inline float* get_address_of_rotation_8() { return &___rotation_8; }
	inline void set_rotation_8(float value)
	{
		___rotation_8 = value;
	}

	inline static int32_t get_offset_of_U24this_9() { return static_cast<int32_t>(offsetof(U3CloadImageU3Ec__Iterator0_t2022358625, ___U24this_9)); }
	inline ImageContent_t959306442 * get_U24this_9() const { return ___U24this_9; }
	inline ImageContent_t959306442 ** get_address_of_U24this_9() { return &___U24this_9; }
	inline void set_U24this_9(ImageContent_t959306442 * value)
	{
		___U24this_9 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_9), value);
	}

	inline static int32_t get_offset_of_U24current_10() { return static_cast<int32_t>(offsetof(U3CloadImageU3Ec__Iterator0_t2022358625, ___U24current_10)); }
	inline RuntimeObject * get_U24current_10() const { return ___U24current_10; }
	inline RuntimeObject ** get_address_of_U24current_10() { return &___U24current_10; }
	inline void set_U24current_10(RuntimeObject * value)
	{
		___U24current_10 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_10), value);
	}

	inline static int32_t get_offset_of_U24disposing_11() { return static_cast<int32_t>(offsetof(U3CloadImageU3Ec__Iterator0_t2022358625, ___U24disposing_11)); }
	inline bool get_U24disposing_11() const { return ___U24disposing_11; }
	inline bool* get_address_of_U24disposing_11() { return &___U24disposing_11; }
	inline void set_U24disposing_11(bool value)
	{
		___U24disposing_11 = value;
	}

	inline static int32_t get_offset_of_U24PC_12() { return static_cast<int32_t>(offsetof(U3CloadImageU3Ec__Iterator0_t2022358625, ___U24PC_12)); }
	inline int32_t get_U24PC_12() const { return ___U24PC_12; }
	inline int32_t* get_address_of_U24PC_12() { return &___U24PC_12; }
	inline void set_U24PC_12(int32_t value)
	{
		___U24PC_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADIMAGEU3EC__ITERATOR0_T2022358625_H
#ifndef U3CGETLANGUAGEJSONU3EC__ITERATOR1_T1445961214_H
#define U3CGETLANGUAGEJSONU3EC__ITERATOR1_T1445961214_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LangManager/<GetLanguageJSON>c__Iterator1
struct  U3CGetLanguageJSONU3Ec__Iterator1_t1445961214  : public RuntimeObject
{
public:
	// System.String LangManager/<GetLanguageJSON>c__Iterator1::<url>__0
	String_t* ___U3CurlU3E__0_0;
	// UnityEngine.WWW LangManager/<GetLanguageJSON>c__Iterator1::<www>__0
	WWW_t3688466362 * ___U3CwwwU3E__0_1;
	// MarkerManager LangManager/<GetLanguageJSON>c__Iterator1::<manager>__0
	MarkerManager_t1031803141 * ___U3CmanagerU3E__0_2;
	// LangManager LangManager/<GetLanguageJSON>c__Iterator1::$this
	LangManager_t3681335332 * ___U24this_3;
	// System.Object LangManager/<GetLanguageJSON>c__Iterator1::$current
	RuntimeObject * ___U24current_4;
	// System.Boolean LangManager/<GetLanguageJSON>c__Iterator1::$disposing
	bool ___U24disposing_5;
	// System.Int32 LangManager/<GetLanguageJSON>c__Iterator1::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_U3CurlU3E__0_0() { return static_cast<int32_t>(offsetof(U3CGetLanguageJSONU3Ec__Iterator1_t1445961214, ___U3CurlU3E__0_0)); }
	inline String_t* get_U3CurlU3E__0_0() const { return ___U3CurlU3E__0_0; }
	inline String_t** get_address_of_U3CurlU3E__0_0() { return &___U3CurlU3E__0_0; }
	inline void set_U3CurlU3E__0_0(String_t* value)
	{
		___U3CurlU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CurlU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E__0_1() { return static_cast<int32_t>(offsetof(U3CGetLanguageJSONU3Ec__Iterator1_t1445961214, ___U3CwwwU3E__0_1)); }
	inline WWW_t3688466362 * get_U3CwwwU3E__0_1() const { return ___U3CwwwU3E__0_1; }
	inline WWW_t3688466362 ** get_address_of_U3CwwwU3E__0_1() { return &___U3CwwwU3E__0_1; }
	inline void set_U3CwwwU3E__0_1(WWW_t3688466362 * value)
	{
		___U3CwwwU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwwwU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U3CmanagerU3E__0_2() { return static_cast<int32_t>(offsetof(U3CGetLanguageJSONU3Ec__Iterator1_t1445961214, ___U3CmanagerU3E__0_2)); }
	inline MarkerManager_t1031803141 * get_U3CmanagerU3E__0_2() const { return ___U3CmanagerU3E__0_2; }
	inline MarkerManager_t1031803141 ** get_address_of_U3CmanagerU3E__0_2() { return &___U3CmanagerU3E__0_2; }
	inline void set_U3CmanagerU3E__0_2(MarkerManager_t1031803141 * value)
	{
		___U3CmanagerU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmanagerU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CGetLanguageJSONU3Ec__Iterator1_t1445961214, ___U24this_3)); }
	inline LangManager_t3681335332 * get_U24this_3() const { return ___U24this_3; }
	inline LangManager_t3681335332 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(LangManager_t3681335332 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CGetLanguageJSONU3Ec__Iterator1_t1445961214, ___U24current_4)); }
	inline RuntimeObject * get_U24current_4() const { return ___U24current_4; }
	inline RuntimeObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(RuntimeObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CGetLanguageJSONU3Ec__Iterator1_t1445961214, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CGetLanguageJSONU3Ec__Iterator1_t1445961214, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETLANGUAGEJSONU3EC__ITERATOR1_T1445961214_H
#ifndef U3CCHECKWWWLANGUAGEU3EC__ITERATOR0_T934303629_H
#define U3CCHECKWWWLANGUAGEU3EC__ITERATOR0_T934303629_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LangManager/<checkWWWlanguage>c__Iterator0
struct  U3CcheckWWWlanguageU3Ec__Iterator0_t934303629  : public RuntimeObject
{
public:
	// System.String LangManager/<checkWWWlanguage>c__Iterator0::cnty
	String_t* ___cnty_0;
	// System.String LangManager/<checkWWWlanguage>c__Iterator0::<lg1>__0
	String_t* ___U3Clg1U3E__0_1;
	// System.String LangManager/<checkWWWlanguage>c__Iterator0::<lg2>__0
	String_t* ___U3Clg2U3E__0_2;
	// System.String LangManager/<checkWWWlanguage>c__Iterator0::<lg3>__0
	String_t* ___U3Clg3U3E__0_3;
	// System.Int32[] LangManager/<checkWWWlanguage>c__Iterator0::<valKey>__0
	Int32U5BU5D_t385246372* ___U3CvalKeyU3E__0_4;
	// System.String LangManager/<checkWWWlanguage>c__Iterator0::<url>__0
	String_t* ___U3CurlU3E__0_5;
	// UnityEngine.WWW LangManager/<checkWWWlanguage>c__Iterator0::<www>__0
	WWW_t3688466362 * ___U3CwwwU3E__0_6;
	// TranslationManager LangManager/<checkWWWlanguage>c__Iterator0::<tm>__0
	TranslationManager_t363825059 * ___U3CtmU3E__0_7;
	// SimpleJSON.JSONNode LangManager/<checkWWWlanguage>c__Iterator0::<Nbase>__0
	JSONNode_t2946056997 * ___U3CNbaseU3E__0_8;
	// LangManager LangManager/<checkWWWlanguage>c__Iterator0::$this
	LangManager_t3681335332 * ___U24this_9;
	// System.Object LangManager/<checkWWWlanguage>c__Iterator0::$current
	RuntimeObject * ___U24current_10;
	// System.Boolean LangManager/<checkWWWlanguage>c__Iterator0::$disposing
	bool ___U24disposing_11;
	// System.Int32 LangManager/<checkWWWlanguage>c__Iterator0::$PC
	int32_t ___U24PC_12;

public:
	inline static int32_t get_offset_of_cnty_0() { return static_cast<int32_t>(offsetof(U3CcheckWWWlanguageU3Ec__Iterator0_t934303629, ___cnty_0)); }
	inline String_t* get_cnty_0() const { return ___cnty_0; }
	inline String_t** get_address_of_cnty_0() { return &___cnty_0; }
	inline void set_cnty_0(String_t* value)
	{
		___cnty_0 = value;
		Il2CppCodeGenWriteBarrier((&___cnty_0), value);
	}

	inline static int32_t get_offset_of_U3Clg1U3E__0_1() { return static_cast<int32_t>(offsetof(U3CcheckWWWlanguageU3Ec__Iterator0_t934303629, ___U3Clg1U3E__0_1)); }
	inline String_t* get_U3Clg1U3E__0_1() const { return ___U3Clg1U3E__0_1; }
	inline String_t** get_address_of_U3Clg1U3E__0_1() { return &___U3Clg1U3E__0_1; }
	inline void set_U3Clg1U3E__0_1(String_t* value)
	{
		___U3Clg1U3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3Clg1U3E__0_1), value);
	}

	inline static int32_t get_offset_of_U3Clg2U3E__0_2() { return static_cast<int32_t>(offsetof(U3CcheckWWWlanguageU3Ec__Iterator0_t934303629, ___U3Clg2U3E__0_2)); }
	inline String_t* get_U3Clg2U3E__0_2() const { return ___U3Clg2U3E__0_2; }
	inline String_t** get_address_of_U3Clg2U3E__0_2() { return &___U3Clg2U3E__0_2; }
	inline void set_U3Clg2U3E__0_2(String_t* value)
	{
		___U3Clg2U3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3Clg2U3E__0_2), value);
	}

	inline static int32_t get_offset_of_U3Clg3U3E__0_3() { return static_cast<int32_t>(offsetof(U3CcheckWWWlanguageU3Ec__Iterator0_t934303629, ___U3Clg3U3E__0_3)); }
	inline String_t* get_U3Clg3U3E__0_3() const { return ___U3Clg3U3E__0_3; }
	inline String_t** get_address_of_U3Clg3U3E__0_3() { return &___U3Clg3U3E__0_3; }
	inline void set_U3Clg3U3E__0_3(String_t* value)
	{
		___U3Clg3U3E__0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3Clg3U3E__0_3), value);
	}

	inline static int32_t get_offset_of_U3CvalKeyU3E__0_4() { return static_cast<int32_t>(offsetof(U3CcheckWWWlanguageU3Ec__Iterator0_t934303629, ___U3CvalKeyU3E__0_4)); }
	inline Int32U5BU5D_t385246372* get_U3CvalKeyU3E__0_4() const { return ___U3CvalKeyU3E__0_4; }
	inline Int32U5BU5D_t385246372** get_address_of_U3CvalKeyU3E__0_4() { return &___U3CvalKeyU3E__0_4; }
	inline void set_U3CvalKeyU3E__0_4(Int32U5BU5D_t385246372* value)
	{
		___U3CvalKeyU3E__0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CvalKeyU3E__0_4), value);
	}

	inline static int32_t get_offset_of_U3CurlU3E__0_5() { return static_cast<int32_t>(offsetof(U3CcheckWWWlanguageU3Ec__Iterator0_t934303629, ___U3CurlU3E__0_5)); }
	inline String_t* get_U3CurlU3E__0_5() const { return ___U3CurlU3E__0_5; }
	inline String_t** get_address_of_U3CurlU3E__0_5() { return &___U3CurlU3E__0_5; }
	inline void set_U3CurlU3E__0_5(String_t* value)
	{
		___U3CurlU3E__0_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CurlU3E__0_5), value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E__0_6() { return static_cast<int32_t>(offsetof(U3CcheckWWWlanguageU3Ec__Iterator0_t934303629, ___U3CwwwU3E__0_6)); }
	inline WWW_t3688466362 * get_U3CwwwU3E__0_6() const { return ___U3CwwwU3E__0_6; }
	inline WWW_t3688466362 ** get_address_of_U3CwwwU3E__0_6() { return &___U3CwwwU3E__0_6; }
	inline void set_U3CwwwU3E__0_6(WWW_t3688466362 * value)
	{
		___U3CwwwU3E__0_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwwwU3E__0_6), value);
	}

	inline static int32_t get_offset_of_U3CtmU3E__0_7() { return static_cast<int32_t>(offsetof(U3CcheckWWWlanguageU3Ec__Iterator0_t934303629, ___U3CtmU3E__0_7)); }
	inline TranslationManager_t363825059 * get_U3CtmU3E__0_7() const { return ___U3CtmU3E__0_7; }
	inline TranslationManager_t363825059 ** get_address_of_U3CtmU3E__0_7() { return &___U3CtmU3E__0_7; }
	inline void set_U3CtmU3E__0_7(TranslationManager_t363825059 * value)
	{
		___U3CtmU3E__0_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtmU3E__0_7), value);
	}

	inline static int32_t get_offset_of_U3CNbaseU3E__0_8() { return static_cast<int32_t>(offsetof(U3CcheckWWWlanguageU3Ec__Iterator0_t934303629, ___U3CNbaseU3E__0_8)); }
	inline JSONNode_t2946056997 * get_U3CNbaseU3E__0_8() const { return ___U3CNbaseU3E__0_8; }
	inline JSONNode_t2946056997 ** get_address_of_U3CNbaseU3E__0_8() { return &___U3CNbaseU3E__0_8; }
	inline void set_U3CNbaseU3E__0_8(JSONNode_t2946056997 * value)
	{
		___U3CNbaseU3E__0_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNbaseU3E__0_8), value);
	}

	inline static int32_t get_offset_of_U24this_9() { return static_cast<int32_t>(offsetof(U3CcheckWWWlanguageU3Ec__Iterator0_t934303629, ___U24this_9)); }
	inline LangManager_t3681335332 * get_U24this_9() const { return ___U24this_9; }
	inline LangManager_t3681335332 ** get_address_of_U24this_9() { return &___U24this_9; }
	inline void set_U24this_9(LangManager_t3681335332 * value)
	{
		___U24this_9 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_9), value);
	}

	inline static int32_t get_offset_of_U24current_10() { return static_cast<int32_t>(offsetof(U3CcheckWWWlanguageU3Ec__Iterator0_t934303629, ___U24current_10)); }
	inline RuntimeObject * get_U24current_10() const { return ___U24current_10; }
	inline RuntimeObject ** get_address_of_U24current_10() { return &___U24current_10; }
	inline void set_U24current_10(RuntimeObject * value)
	{
		___U24current_10 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_10), value);
	}

	inline static int32_t get_offset_of_U24disposing_11() { return static_cast<int32_t>(offsetof(U3CcheckWWWlanguageU3Ec__Iterator0_t934303629, ___U24disposing_11)); }
	inline bool get_U24disposing_11() const { return ___U24disposing_11; }
	inline bool* get_address_of_U24disposing_11() { return &___U24disposing_11; }
	inline void set_U24disposing_11(bool value)
	{
		___U24disposing_11 = value;
	}

	inline static int32_t get_offset_of_U24PC_12() { return static_cast<int32_t>(offsetof(U3CcheckWWWlanguageU3Ec__Iterator0_t934303629, ___U24PC_12)); }
	inline int32_t get_U24PC_12() const { return ___U24PC_12; }
	inline int32_t* get_address_of_U24PC_12() { return &___U24PC_12; }
	inline void set_U24PC_12(int32_t value)
	{
		___U24PC_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCHECKWWWLANGUAGEU3EC__ITERATOR0_T934303629_H
#ifndef U3CLOADIMAGEU3EC__ITERATOR0_T936851250_H
#define U3CLOADIMAGEU3EC__ITERATOR0_T936851250_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MarkerContent/<loadImage>c__Iterator0
struct  U3CloadImageU3Ec__Iterator0_t936851250  : public RuntimeObject
{
public:
	// System.String MarkerContent/<loadImage>c__Iterator0::url_low
	String_t* ___url_low_0;
	// UnityEngine.WWW MarkerContent/<loadImage>c__Iterator0::<wwwLow>__0
	WWW_t3688466362 * ___U3CwwwLowU3E__0_1;
	// UnityEngine.Renderer MarkerContent/<loadImage>c__Iterator0::<rd>__0
	Renderer_t2627027031 * ___U3CrdU3E__0_2;
	// System.Single MarkerContent/<loadImage>c__Iterator0::posX
	float ___posX_3;
	// System.Single MarkerContent/<loadImage>c__Iterator0::posY
	float ___posY_4;
	// System.Single MarkerContent/<loadImage>c__Iterator0::layer
	float ___layer_5;
	// System.Single MarkerContent/<loadImage>c__Iterator0::sizeX
	float ___sizeX_6;
	// System.Single MarkerContent/<loadImage>c__Iterator0::sizeY
	float ___sizeY_7;
	// System.Single MarkerContent/<loadImage>c__Iterator0::rotation
	float ___rotation_8;
	// System.String MarkerContent/<loadImage>c__Iterator0::url_thumb
	String_t* ___url_thumb_9;
	// UnityEngine.WWW MarkerContent/<loadImage>c__Iterator0::<wwwThumb>__0
	WWW_t3688466362 * ___U3CwwwThumbU3E__0_10;
	// System.String MarkerContent/<loadImage>c__Iterator0::url
	String_t* ___url_11;
	// UnityEngine.WWW MarkerContent/<loadImage>c__Iterator0::<www>__0
	WWW_t3688466362 * ___U3CwwwU3E__0_12;
	// MarkerContent MarkerContent/<loadImage>c__Iterator0::$this
	MarkerContent_t762212139 * ___U24this_13;
	// System.Object MarkerContent/<loadImage>c__Iterator0::$current
	RuntimeObject * ___U24current_14;
	// System.Boolean MarkerContent/<loadImage>c__Iterator0::$disposing
	bool ___U24disposing_15;
	// System.Int32 MarkerContent/<loadImage>c__Iterator0::$PC
	int32_t ___U24PC_16;

public:
	inline static int32_t get_offset_of_url_low_0() { return static_cast<int32_t>(offsetof(U3CloadImageU3Ec__Iterator0_t936851250, ___url_low_0)); }
	inline String_t* get_url_low_0() const { return ___url_low_0; }
	inline String_t** get_address_of_url_low_0() { return &___url_low_0; }
	inline void set_url_low_0(String_t* value)
	{
		___url_low_0 = value;
		Il2CppCodeGenWriteBarrier((&___url_low_0), value);
	}

	inline static int32_t get_offset_of_U3CwwwLowU3E__0_1() { return static_cast<int32_t>(offsetof(U3CloadImageU3Ec__Iterator0_t936851250, ___U3CwwwLowU3E__0_1)); }
	inline WWW_t3688466362 * get_U3CwwwLowU3E__0_1() const { return ___U3CwwwLowU3E__0_1; }
	inline WWW_t3688466362 ** get_address_of_U3CwwwLowU3E__0_1() { return &___U3CwwwLowU3E__0_1; }
	inline void set_U3CwwwLowU3E__0_1(WWW_t3688466362 * value)
	{
		___U3CwwwLowU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwwwLowU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U3CrdU3E__0_2() { return static_cast<int32_t>(offsetof(U3CloadImageU3Ec__Iterator0_t936851250, ___U3CrdU3E__0_2)); }
	inline Renderer_t2627027031 * get_U3CrdU3E__0_2() const { return ___U3CrdU3E__0_2; }
	inline Renderer_t2627027031 ** get_address_of_U3CrdU3E__0_2() { return &___U3CrdU3E__0_2; }
	inline void set_U3CrdU3E__0_2(Renderer_t2627027031 * value)
	{
		___U3CrdU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrdU3E__0_2), value);
	}

	inline static int32_t get_offset_of_posX_3() { return static_cast<int32_t>(offsetof(U3CloadImageU3Ec__Iterator0_t936851250, ___posX_3)); }
	inline float get_posX_3() const { return ___posX_3; }
	inline float* get_address_of_posX_3() { return &___posX_3; }
	inline void set_posX_3(float value)
	{
		___posX_3 = value;
	}

	inline static int32_t get_offset_of_posY_4() { return static_cast<int32_t>(offsetof(U3CloadImageU3Ec__Iterator0_t936851250, ___posY_4)); }
	inline float get_posY_4() const { return ___posY_4; }
	inline float* get_address_of_posY_4() { return &___posY_4; }
	inline void set_posY_4(float value)
	{
		___posY_4 = value;
	}

	inline static int32_t get_offset_of_layer_5() { return static_cast<int32_t>(offsetof(U3CloadImageU3Ec__Iterator0_t936851250, ___layer_5)); }
	inline float get_layer_5() const { return ___layer_5; }
	inline float* get_address_of_layer_5() { return &___layer_5; }
	inline void set_layer_5(float value)
	{
		___layer_5 = value;
	}

	inline static int32_t get_offset_of_sizeX_6() { return static_cast<int32_t>(offsetof(U3CloadImageU3Ec__Iterator0_t936851250, ___sizeX_6)); }
	inline float get_sizeX_6() const { return ___sizeX_6; }
	inline float* get_address_of_sizeX_6() { return &___sizeX_6; }
	inline void set_sizeX_6(float value)
	{
		___sizeX_6 = value;
	}

	inline static int32_t get_offset_of_sizeY_7() { return static_cast<int32_t>(offsetof(U3CloadImageU3Ec__Iterator0_t936851250, ___sizeY_7)); }
	inline float get_sizeY_7() const { return ___sizeY_7; }
	inline float* get_address_of_sizeY_7() { return &___sizeY_7; }
	inline void set_sizeY_7(float value)
	{
		___sizeY_7 = value;
	}

	inline static int32_t get_offset_of_rotation_8() { return static_cast<int32_t>(offsetof(U3CloadImageU3Ec__Iterator0_t936851250, ___rotation_8)); }
	inline float get_rotation_8() const { return ___rotation_8; }
	inline float* get_address_of_rotation_8() { return &___rotation_8; }
	inline void set_rotation_8(float value)
	{
		___rotation_8 = value;
	}

	inline static int32_t get_offset_of_url_thumb_9() { return static_cast<int32_t>(offsetof(U3CloadImageU3Ec__Iterator0_t936851250, ___url_thumb_9)); }
	inline String_t* get_url_thumb_9() const { return ___url_thumb_9; }
	inline String_t** get_address_of_url_thumb_9() { return &___url_thumb_9; }
	inline void set_url_thumb_9(String_t* value)
	{
		___url_thumb_9 = value;
		Il2CppCodeGenWriteBarrier((&___url_thumb_9), value);
	}

	inline static int32_t get_offset_of_U3CwwwThumbU3E__0_10() { return static_cast<int32_t>(offsetof(U3CloadImageU3Ec__Iterator0_t936851250, ___U3CwwwThumbU3E__0_10)); }
	inline WWW_t3688466362 * get_U3CwwwThumbU3E__0_10() const { return ___U3CwwwThumbU3E__0_10; }
	inline WWW_t3688466362 ** get_address_of_U3CwwwThumbU3E__0_10() { return &___U3CwwwThumbU3E__0_10; }
	inline void set_U3CwwwThumbU3E__0_10(WWW_t3688466362 * value)
	{
		___U3CwwwThumbU3E__0_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwwwThumbU3E__0_10), value);
	}

	inline static int32_t get_offset_of_url_11() { return static_cast<int32_t>(offsetof(U3CloadImageU3Ec__Iterator0_t936851250, ___url_11)); }
	inline String_t* get_url_11() const { return ___url_11; }
	inline String_t** get_address_of_url_11() { return &___url_11; }
	inline void set_url_11(String_t* value)
	{
		___url_11 = value;
		Il2CppCodeGenWriteBarrier((&___url_11), value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E__0_12() { return static_cast<int32_t>(offsetof(U3CloadImageU3Ec__Iterator0_t936851250, ___U3CwwwU3E__0_12)); }
	inline WWW_t3688466362 * get_U3CwwwU3E__0_12() const { return ___U3CwwwU3E__0_12; }
	inline WWW_t3688466362 ** get_address_of_U3CwwwU3E__0_12() { return &___U3CwwwU3E__0_12; }
	inline void set_U3CwwwU3E__0_12(WWW_t3688466362 * value)
	{
		___U3CwwwU3E__0_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwwwU3E__0_12), value);
	}

	inline static int32_t get_offset_of_U24this_13() { return static_cast<int32_t>(offsetof(U3CloadImageU3Ec__Iterator0_t936851250, ___U24this_13)); }
	inline MarkerContent_t762212139 * get_U24this_13() const { return ___U24this_13; }
	inline MarkerContent_t762212139 ** get_address_of_U24this_13() { return &___U24this_13; }
	inline void set_U24this_13(MarkerContent_t762212139 * value)
	{
		___U24this_13 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_13), value);
	}

	inline static int32_t get_offset_of_U24current_14() { return static_cast<int32_t>(offsetof(U3CloadImageU3Ec__Iterator0_t936851250, ___U24current_14)); }
	inline RuntimeObject * get_U24current_14() const { return ___U24current_14; }
	inline RuntimeObject ** get_address_of_U24current_14() { return &___U24current_14; }
	inline void set_U24current_14(RuntimeObject * value)
	{
		___U24current_14 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_14), value);
	}

	inline static int32_t get_offset_of_U24disposing_15() { return static_cast<int32_t>(offsetof(U3CloadImageU3Ec__Iterator0_t936851250, ___U24disposing_15)); }
	inline bool get_U24disposing_15() const { return ___U24disposing_15; }
	inline bool* get_address_of_U24disposing_15() { return &___U24disposing_15; }
	inline void set_U24disposing_15(bool value)
	{
		___U24disposing_15 = value;
	}

	inline static int32_t get_offset_of_U24PC_16() { return static_cast<int32_t>(offsetof(U3CloadImageU3Ec__Iterator0_t936851250, ___U24PC_16)); }
	inline int32_t get_U24PC_16() const { return ___U24PC_16; }
	inline int32_t* get_address_of_U24PC_16() { return &___U24PC_16; }
	inline void set_U24PC_16(int32_t value)
	{
		___U24PC_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADIMAGEU3EC__ITERATOR0_T936851250_H
#ifndef MARKERDATA_T1252592219_H
#define MARKERDATA_T1252592219_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MarkerData
struct  MarkerData_t1252592219  : public RuntimeObject
{
public:
	// System.Boolean MarkerData::visible
	bool ___visible_0;
	// System.String MarkerData::id
	String_t* ___id_1;
	// System.String MarkerData::target_url
	String_t* ___target_url_2;
	// System.String MarkerData::target_thumb_url
	String_t* ___target_thumb_url_3;
	// System.String MarkerData::target_low_lod_url
	String_t* ___target_low_lod_url_4;
	// System.Single MarkerData::width
	float ___width_5;
	// System.Single MarkerData::height
	float ___height_6;
	// System.Int32 MarkerData::layerCount
	int32_t ___layerCount_7;
	// MarkerLayer[] MarkerData::layers
	MarkerLayerU5BU5D_t4163602498* ___layers_8;

public:
	inline static int32_t get_offset_of_visible_0() { return static_cast<int32_t>(offsetof(MarkerData_t1252592219, ___visible_0)); }
	inline bool get_visible_0() const { return ___visible_0; }
	inline bool* get_address_of_visible_0() { return &___visible_0; }
	inline void set_visible_0(bool value)
	{
		___visible_0 = value;
	}

	inline static int32_t get_offset_of_id_1() { return static_cast<int32_t>(offsetof(MarkerData_t1252592219, ___id_1)); }
	inline String_t* get_id_1() const { return ___id_1; }
	inline String_t** get_address_of_id_1() { return &___id_1; }
	inline void set_id_1(String_t* value)
	{
		___id_1 = value;
		Il2CppCodeGenWriteBarrier((&___id_1), value);
	}

	inline static int32_t get_offset_of_target_url_2() { return static_cast<int32_t>(offsetof(MarkerData_t1252592219, ___target_url_2)); }
	inline String_t* get_target_url_2() const { return ___target_url_2; }
	inline String_t** get_address_of_target_url_2() { return &___target_url_2; }
	inline void set_target_url_2(String_t* value)
	{
		___target_url_2 = value;
		Il2CppCodeGenWriteBarrier((&___target_url_2), value);
	}

	inline static int32_t get_offset_of_target_thumb_url_3() { return static_cast<int32_t>(offsetof(MarkerData_t1252592219, ___target_thumb_url_3)); }
	inline String_t* get_target_thumb_url_3() const { return ___target_thumb_url_3; }
	inline String_t** get_address_of_target_thumb_url_3() { return &___target_thumb_url_3; }
	inline void set_target_thumb_url_3(String_t* value)
	{
		___target_thumb_url_3 = value;
		Il2CppCodeGenWriteBarrier((&___target_thumb_url_3), value);
	}

	inline static int32_t get_offset_of_target_low_lod_url_4() { return static_cast<int32_t>(offsetof(MarkerData_t1252592219, ___target_low_lod_url_4)); }
	inline String_t* get_target_low_lod_url_4() const { return ___target_low_lod_url_4; }
	inline String_t** get_address_of_target_low_lod_url_4() { return &___target_low_lod_url_4; }
	inline void set_target_low_lod_url_4(String_t* value)
	{
		___target_low_lod_url_4 = value;
		Il2CppCodeGenWriteBarrier((&___target_low_lod_url_4), value);
	}

	inline static int32_t get_offset_of_width_5() { return static_cast<int32_t>(offsetof(MarkerData_t1252592219, ___width_5)); }
	inline float get_width_5() const { return ___width_5; }
	inline float* get_address_of_width_5() { return &___width_5; }
	inline void set_width_5(float value)
	{
		___width_5 = value;
	}

	inline static int32_t get_offset_of_height_6() { return static_cast<int32_t>(offsetof(MarkerData_t1252592219, ___height_6)); }
	inline float get_height_6() const { return ___height_6; }
	inline float* get_address_of_height_6() { return &___height_6; }
	inline void set_height_6(float value)
	{
		___height_6 = value;
	}

	inline static int32_t get_offset_of_layerCount_7() { return static_cast<int32_t>(offsetof(MarkerData_t1252592219, ___layerCount_7)); }
	inline int32_t get_layerCount_7() const { return ___layerCount_7; }
	inline int32_t* get_address_of_layerCount_7() { return &___layerCount_7; }
	inline void set_layerCount_7(int32_t value)
	{
		___layerCount_7 = value;
	}

	inline static int32_t get_offset_of_layers_8() { return static_cast<int32_t>(offsetof(MarkerData_t1252592219, ___layers_8)); }
	inline MarkerLayerU5BU5D_t4163602498* get_layers_8() const { return ___layers_8; }
	inline MarkerLayerU5BU5D_t4163602498** get_address_of_layers_8() { return &___layers_8; }
	inline void set_layers_8(MarkerLayerU5BU5D_t4163602498* value)
	{
		___layers_8 = value;
		Il2CppCodeGenWriteBarrier((&___layers_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MARKERDATA_T1252592219_H
#ifndef MARKERLAYER_T3962172947_H
#define MARKERLAYER_T3962172947_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MarkerLayer
struct  MarkerLayer_t3962172947  : public RuntimeObject
{
public:
	// System.Boolean MarkerLayer::enable
	bool ___enable_0;
	// System.Single MarkerLayer::posX
	float ___posX_1;
	// System.Single MarkerLayer::posY
	float ___posY_2;
	// System.Single MarkerLayer::rotation
	float ___rotation_3;
	// System.Single MarkerLayer::inclinaison
	float ___inclinaison_4;
	// System.Single MarkerLayer::width
	float ___width_5;
	// System.Single MarkerLayer::height
	float ___height_6;
	// System.String MarkerLayer::type
	String_t* ___type_7;
	// System.String MarkerLayer::image
	String_t* ___image_8;
	// System.Boolean MarkerLayer::isChromakey
	bool ___isChromakey_9;
	// System.String MarkerLayer::zip1
	String_t* ___zip1_10;
	// System.String MarkerLayer::zip2
	String_t* ___zip2_11;
	// System.String MarkerLayer::GUID
	String_t* ___GUID_12;
	// System.Int32 MarkerLayer::videoWidth
	int32_t ___videoWidth_13;
	// System.Int32 MarkerLayer::videoHeight
	int32_t ___videoHeight_14;
	// System.String MarkerLayer::url
	String_t* ___url_15;
	// System.String MarkerLayer::action
	String_t* ___action_16;
	// System.Boolean MarkerLayer::autoplay
	bool ___autoplay_17;
	// System.Int32 MarkerLayer::jeu_CodeErreur
	int32_t ___jeu_CodeErreur_18;
	// System.String MarkerLayer::jeu_strErreur
	String_t* ___jeu_strErreur_19;
	// System.String MarkerLayer::jeu_imageAGratter
	String_t* ___jeu_imageAGratter_20;
	// System.Boolean MarkerLayer::jeu_rejouer
	bool ___jeu_rejouer_21;
	// System.String MarkerLayer::jeu_reglement
	String_t* ___jeu_reglement_22;
	// System.String MarkerLayer::jeu_status
	String_t* ___jeu_status_23;
	// System.Boolean MarkerLayer::jeu_tirage
	bool ___jeu_tirage_24;
	// System.String MarkerLayer::jeu_id
	String_t* ___jeu_id_25;
	// System.String MarkerLayer::jeu_name
	String_t* ___jeu_name_26;
	// System.String MarkerLayer::jeu_reward_msg
	String_t* ___jeu_reward_msg_27;
	// System.String MarkerLayer::jeu_link_of_reward
	String_t* ___jeu_link_of_reward_28;
	// System.String MarkerLayer::jeu_source_gagne
	String_t* ___jeu_source_gagne_29;
	// System.String MarkerLayer::jeu_thumb
	String_t* ___jeu_thumb_30;
	// System.String MarkerLayer::jeu_token
	String_t* ___jeu_token_31;
	// System.String MarkerLayer::jeu_recupMailTirage
	String_t* ___jeu_recupMailTirage_32;
	// System.String MarkerLayer::jeu_source_perdu
	String_t* ___jeu_source_perdu_33;
	// System.String MarkerLayer::jeu_thumb_perdu
	String_t* ___jeu_thumb_perdu_34;
	// System.Boolean MarkerLayer::jeu_data_email
	bool ___jeu_data_email_35;
	// System.Boolean MarkerLayer::jeu_data_email_oblig
	bool ___jeu_data_email_oblig_36;
	// System.Boolean MarkerLayer::jeu_data_nom
	bool ___jeu_data_nom_37;
	// System.Boolean MarkerLayer::jeu_data_nom_oblig
	bool ___jeu_data_nom_oblig_38;
	// System.Boolean MarkerLayer::jeu_data_prenom
	bool ___jeu_data_prenom_39;
	// System.Boolean MarkerLayer::jeu_data_prenom_oblig
	bool ___jeu_data_prenom_oblig_40;
	// System.Boolean MarkerLayer::jeu_data_societe
	bool ___jeu_data_societe_41;
	// System.Boolean MarkerLayer::jeu_data_societe_oblig
	bool ___jeu_data_societe_oblig_42;
	// System.Boolean MarkerLayer::jeu_data_telephone
	bool ___jeu_data_telephone_43;
	// System.Boolean MarkerLayer::jeu_data_telephone_oblig
	bool ___jeu_data_telephone_oblig_44;
	// System.Boolean MarkerLayer::jeu_data_datenaissance
	bool ___jeu_data_datenaissance_45;
	// System.Boolean MarkerLayer::jeu_data_datenaissance_oblig
	bool ___jeu_data_datenaissance_oblig_46;
	// System.Boolean MarkerLayer::jeu_data_extrafield
	bool ___jeu_data_extrafield_47;
	// System.Boolean MarkerLayer::jeu_data_extrafield_oblig
	bool ___jeu_data_extrafield_oblig_48;
	// System.String MarkerLayer::jeu_data_str_extrafield
	String_t* ___jeu_data_str_extrafield_49;
	// System.String MarkerLayer::son_source
	String_t* ___son_source_50;

public:
	inline static int32_t get_offset_of_enable_0() { return static_cast<int32_t>(offsetof(MarkerLayer_t3962172947, ___enable_0)); }
	inline bool get_enable_0() const { return ___enable_0; }
	inline bool* get_address_of_enable_0() { return &___enable_0; }
	inline void set_enable_0(bool value)
	{
		___enable_0 = value;
	}

	inline static int32_t get_offset_of_posX_1() { return static_cast<int32_t>(offsetof(MarkerLayer_t3962172947, ___posX_1)); }
	inline float get_posX_1() const { return ___posX_1; }
	inline float* get_address_of_posX_1() { return &___posX_1; }
	inline void set_posX_1(float value)
	{
		___posX_1 = value;
	}

	inline static int32_t get_offset_of_posY_2() { return static_cast<int32_t>(offsetof(MarkerLayer_t3962172947, ___posY_2)); }
	inline float get_posY_2() const { return ___posY_2; }
	inline float* get_address_of_posY_2() { return &___posY_2; }
	inline void set_posY_2(float value)
	{
		___posY_2 = value;
	}

	inline static int32_t get_offset_of_rotation_3() { return static_cast<int32_t>(offsetof(MarkerLayer_t3962172947, ___rotation_3)); }
	inline float get_rotation_3() const { return ___rotation_3; }
	inline float* get_address_of_rotation_3() { return &___rotation_3; }
	inline void set_rotation_3(float value)
	{
		___rotation_3 = value;
	}

	inline static int32_t get_offset_of_inclinaison_4() { return static_cast<int32_t>(offsetof(MarkerLayer_t3962172947, ___inclinaison_4)); }
	inline float get_inclinaison_4() const { return ___inclinaison_4; }
	inline float* get_address_of_inclinaison_4() { return &___inclinaison_4; }
	inline void set_inclinaison_4(float value)
	{
		___inclinaison_4 = value;
	}

	inline static int32_t get_offset_of_width_5() { return static_cast<int32_t>(offsetof(MarkerLayer_t3962172947, ___width_5)); }
	inline float get_width_5() const { return ___width_5; }
	inline float* get_address_of_width_5() { return &___width_5; }
	inline void set_width_5(float value)
	{
		___width_5 = value;
	}

	inline static int32_t get_offset_of_height_6() { return static_cast<int32_t>(offsetof(MarkerLayer_t3962172947, ___height_6)); }
	inline float get_height_6() const { return ___height_6; }
	inline float* get_address_of_height_6() { return &___height_6; }
	inline void set_height_6(float value)
	{
		___height_6 = value;
	}

	inline static int32_t get_offset_of_type_7() { return static_cast<int32_t>(offsetof(MarkerLayer_t3962172947, ___type_7)); }
	inline String_t* get_type_7() const { return ___type_7; }
	inline String_t** get_address_of_type_7() { return &___type_7; }
	inline void set_type_7(String_t* value)
	{
		___type_7 = value;
		Il2CppCodeGenWriteBarrier((&___type_7), value);
	}

	inline static int32_t get_offset_of_image_8() { return static_cast<int32_t>(offsetof(MarkerLayer_t3962172947, ___image_8)); }
	inline String_t* get_image_8() const { return ___image_8; }
	inline String_t** get_address_of_image_8() { return &___image_8; }
	inline void set_image_8(String_t* value)
	{
		___image_8 = value;
		Il2CppCodeGenWriteBarrier((&___image_8), value);
	}

	inline static int32_t get_offset_of_isChromakey_9() { return static_cast<int32_t>(offsetof(MarkerLayer_t3962172947, ___isChromakey_9)); }
	inline bool get_isChromakey_9() const { return ___isChromakey_9; }
	inline bool* get_address_of_isChromakey_9() { return &___isChromakey_9; }
	inline void set_isChromakey_9(bool value)
	{
		___isChromakey_9 = value;
	}

	inline static int32_t get_offset_of_zip1_10() { return static_cast<int32_t>(offsetof(MarkerLayer_t3962172947, ___zip1_10)); }
	inline String_t* get_zip1_10() const { return ___zip1_10; }
	inline String_t** get_address_of_zip1_10() { return &___zip1_10; }
	inline void set_zip1_10(String_t* value)
	{
		___zip1_10 = value;
		Il2CppCodeGenWriteBarrier((&___zip1_10), value);
	}

	inline static int32_t get_offset_of_zip2_11() { return static_cast<int32_t>(offsetof(MarkerLayer_t3962172947, ___zip2_11)); }
	inline String_t* get_zip2_11() const { return ___zip2_11; }
	inline String_t** get_address_of_zip2_11() { return &___zip2_11; }
	inline void set_zip2_11(String_t* value)
	{
		___zip2_11 = value;
		Il2CppCodeGenWriteBarrier((&___zip2_11), value);
	}

	inline static int32_t get_offset_of_GUID_12() { return static_cast<int32_t>(offsetof(MarkerLayer_t3962172947, ___GUID_12)); }
	inline String_t* get_GUID_12() const { return ___GUID_12; }
	inline String_t** get_address_of_GUID_12() { return &___GUID_12; }
	inline void set_GUID_12(String_t* value)
	{
		___GUID_12 = value;
		Il2CppCodeGenWriteBarrier((&___GUID_12), value);
	}

	inline static int32_t get_offset_of_videoWidth_13() { return static_cast<int32_t>(offsetof(MarkerLayer_t3962172947, ___videoWidth_13)); }
	inline int32_t get_videoWidth_13() const { return ___videoWidth_13; }
	inline int32_t* get_address_of_videoWidth_13() { return &___videoWidth_13; }
	inline void set_videoWidth_13(int32_t value)
	{
		___videoWidth_13 = value;
	}

	inline static int32_t get_offset_of_videoHeight_14() { return static_cast<int32_t>(offsetof(MarkerLayer_t3962172947, ___videoHeight_14)); }
	inline int32_t get_videoHeight_14() const { return ___videoHeight_14; }
	inline int32_t* get_address_of_videoHeight_14() { return &___videoHeight_14; }
	inline void set_videoHeight_14(int32_t value)
	{
		___videoHeight_14 = value;
	}

	inline static int32_t get_offset_of_url_15() { return static_cast<int32_t>(offsetof(MarkerLayer_t3962172947, ___url_15)); }
	inline String_t* get_url_15() const { return ___url_15; }
	inline String_t** get_address_of_url_15() { return &___url_15; }
	inline void set_url_15(String_t* value)
	{
		___url_15 = value;
		Il2CppCodeGenWriteBarrier((&___url_15), value);
	}

	inline static int32_t get_offset_of_action_16() { return static_cast<int32_t>(offsetof(MarkerLayer_t3962172947, ___action_16)); }
	inline String_t* get_action_16() const { return ___action_16; }
	inline String_t** get_address_of_action_16() { return &___action_16; }
	inline void set_action_16(String_t* value)
	{
		___action_16 = value;
		Il2CppCodeGenWriteBarrier((&___action_16), value);
	}

	inline static int32_t get_offset_of_autoplay_17() { return static_cast<int32_t>(offsetof(MarkerLayer_t3962172947, ___autoplay_17)); }
	inline bool get_autoplay_17() const { return ___autoplay_17; }
	inline bool* get_address_of_autoplay_17() { return &___autoplay_17; }
	inline void set_autoplay_17(bool value)
	{
		___autoplay_17 = value;
	}

	inline static int32_t get_offset_of_jeu_CodeErreur_18() { return static_cast<int32_t>(offsetof(MarkerLayer_t3962172947, ___jeu_CodeErreur_18)); }
	inline int32_t get_jeu_CodeErreur_18() const { return ___jeu_CodeErreur_18; }
	inline int32_t* get_address_of_jeu_CodeErreur_18() { return &___jeu_CodeErreur_18; }
	inline void set_jeu_CodeErreur_18(int32_t value)
	{
		___jeu_CodeErreur_18 = value;
	}

	inline static int32_t get_offset_of_jeu_strErreur_19() { return static_cast<int32_t>(offsetof(MarkerLayer_t3962172947, ___jeu_strErreur_19)); }
	inline String_t* get_jeu_strErreur_19() const { return ___jeu_strErreur_19; }
	inline String_t** get_address_of_jeu_strErreur_19() { return &___jeu_strErreur_19; }
	inline void set_jeu_strErreur_19(String_t* value)
	{
		___jeu_strErreur_19 = value;
		Il2CppCodeGenWriteBarrier((&___jeu_strErreur_19), value);
	}

	inline static int32_t get_offset_of_jeu_imageAGratter_20() { return static_cast<int32_t>(offsetof(MarkerLayer_t3962172947, ___jeu_imageAGratter_20)); }
	inline String_t* get_jeu_imageAGratter_20() const { return ___jeu_imageAGratter_20; }
	inline String_t** get_address_of_jeu_imageAGratter_20() { return &___jeu_imageAGratter_20; }
	inline void set_jeu_imageAGratter_20(String_t* value)
	{
		___jeu_imageAGratter_20 = value;
		Il2CppCodeGenWriteBarrier((&___jeu_imageAGratter_20), value);
	}

	inline static int32_t get_offset_of_jeu_rejouer_21() { return static_cast<int32_t>(offsetof(MarkerLayer_t3962172947, ___jeu_rejouer_21)); }
	inline bool get_jeu_rejouer_21() const { return ___jeu_rejouer_21; }
	inline bool* get_address_of_jeu_rejouer_21() { return &___jeu_rejouer_21; }
	inline void set_jeu_rejouer_21(bool value)
	{
		___jeu_rejouer_21 = value;
	}

	inline static int32_t get_offset_of_jeu_reglement_22() { return static_cast<int32_t>(offsetof(MarkerLayer_t3962172947, ___jeu_reglement_22)); }
	inline String_t* get_jeu_reglement_22() const { return ___jeu_reglement_22; }
	inline String_t** get_address_of_jeu_reglement_22() { return &___jeu_reglement_22; }
	inline void set_jeu_reglement_22(String_t* value)
	{
		___jeu_reglement_22 = value;
		Il2CppCodeGenWriteBarrier((&___jeu_reglement_22), value);
	}

	inline static int32_t get_offset_of_jeu_status_23() { return static_cast<int32_t>(offsetof(MarkerLayer_t3962172947, ___jeu_status_23)); }
	inline String_t* get_jeu_status_23() const { return ___jeu_status_23; }
	inline String_t** get_address_of_jeu_status_23() { return &___jeu_status_23; }
	inline void set_jeu_status_23(String_t* value)
	{
		___jeu_status_23 = value;
		Il2CppCodeGenWriteBarrier((&___jeu_status_23), value);
	}

	inline static int32_t get_offset_of_jeu_tirage_24() { return static_cast<int32_t>(offsetof(MarkerLayer_t3962172947, ___jeu_tirage_24)); }
	inline bool get_jeu_tirage_24() const { return ___jeu_tirage_24; }
	inline bool* get_address_of_jeu_tirage_24() { return &___jeu_tirage_24; }
	inline void set_jeu_tirage_24(bool value)
	{
		___jeu_tirage_24 = value;
	}

	inline static int32_t get_offset_of_jeu_id_25() { return static_cast<int32_t>(offsetof(MarkerLayer_t3962172947, ___jeu_id_25)); }
	inline String_t* get_jeu_id_25() const { return ___jeu_id_25; }
	inline String_t** get_address_of_jeu_id_25() { return &___jeu_id_25; }
	inline void set_jeu_id_25(String_t* value)
	{
		___jeu_id_25 = value;
		Il2CppCodeGenWriteBarrier((&___jeu_id_25), value);
	}

	inline static int32_t get_offset_of_jeu_name_26() { return static_cast<int32_t>(offsetof(MarkerLayer_t3962172947, ___jeu_name_26)); }
	inline String_t* get_jeu_name_26() const { return ___jeu_name_26; }
	inline String_t** get_address_of_jeu_name_26() { return &___jeu_name_26; }
	inline void set_jeu_name_26(String_t* value)
	{
		___jeu_name_26 = value;
		Il2CppCodeGenWriteBarrier((&___jeu_name_26), value);
	}

	inline static int32_t get_offset_of_jeu_reward_msg_27() { return static_cast<int32_t>(offsetof(MarkerLayer_t3962172947, ___jeu_reward_msg_27)); }
	inline String_t* get_jeu_reward_msg_27() const { return ___jeu_reward_msg_27; }
	inline String_t** get_address_of_jeu_reward_msg_27() { return &___jeu_reward_msg_27; }
	inline void set_jeu_reward_msg_27(String_t* value)
	{
		___jeu_reward_msg_27 = value;
		Il2CppCodeGenWriteBarrier((&___jeu_reward_msg_27), value);
	}

	inline static int32_t get_offset_of_jeu_link_of_reward_28() { return static_cast<int32_t>(offsetof(MarkerLayer_t3962172947, ___jeu_link_of_reward_28)); }
	inline String_t* get_jeu_link_of_reward_28() const { return ___jeu_link_of_reward_28; }
	inline String_t** get_address_of_jeu_link_of_reward_28() { return &___jeu_link_of_reward_28; }
	inline void set_jeu_link_of_reward_28(String_t* value)
	{
		___jeu_link_of_reward_28 = value;
		Il2CppCodeGenWriteBarrier((&___jeu_link_of_reward_28), value);
	}

	inline static int32_t get_offset_of_jeu_source_gagne_29() { return static_cast<int32_t>(offsetof(MarkerLayer_t3962172947, ___jeu_source_gagne_29)); }
	inline String_t* get_jeu_source_gagne_29() const { return ___jeu_source_gagne_29; }
	inline String_t** get_address_of_jeu_source_gagne_29() { return &___jeu_source_gagne_29; }
	inline void set_jeu_source_gagne_29(String_t* value)
	{
		___jeu_source_gagne_29 = value;
		Il2CppCodeGenWriteBarrier((&___jeu_source_gagne_29), value);
	}

	inline static int32_t get_offset_of_jeu_thumb_30() { return static_cast<int32_t>(offsetof(MarkerLayer_t3962172947, ___jeu_thumb_30)); }
	inline String_t* get_jeu_thumb_30() const { return ___jeu_thumb_30; }
	inline String_t** get_address_of_jeu_thumb_30() { return &___jeu_thumb_30; }
	inline void set_jeu_thumb_30(String_t* value)
	{
		___jeu_thumb_30 = value;
		Il2CppCodeGenWriteBarrier((&___jeu_thumb_30), value);
	}

	inline static int32_t get_offset_of_jeu_token_31() { return static_cast<int32_t>(offsetof(MarkerLayer_t3962172947, ___jeu_token_31)); }
	inline String_t* get_jeu_token_31() const { return ___jeu_token_31; }
	inline String_t** get_address_of_jeu_token_31() { return &___jeu_token_31; }
	inline void set_jeu_token_31(String_t* value)
	{
		___jeu_token_31 = value;
		Il2CppCodeGenWriteBarrier((&___jeu_token_31), value);
	}

	inline static int32_t get_offset_of_jeu_recupMailTirage_32() { return static_cast<int32_t>(offsetof(MarkerLayer_t3962172947, ___jeu_recupMailTirage_32)); }
	inline String_t* get_jeu_recupMailTirage_32() const { return ___jeu_recupMailTirage_32; }
	inline String_t** get_address_of_jeu_recupMailTirage_32() { return &___jeu_recupMailTirage_32; }
	inline void set_jeu_recupMailTirage_32(String_t* value)
	{
		___jeu_recupMailTirage_32 = value;
		Il2CppCodeGenWriteBarrier((&___jeu_recupMailTirage_32), value);
	}

	inline static int32_t get_offset_of_jeu_source_perdu_33() { return static_cast<int32_t>(offsetof(MarkerLayer_t3962172947, ___jeu_source_perdu_33)); }
	inline String_t* get_jeu_source_perdu_33() const { return ___jeu_source_perdu_33; }
	inline String_t** get_address_of_jeu_source_perdu_33() { return &___jeu_source_perdu_33; }
	inline void set_jeu_source_perdu_33(String_t* value)
	{
		___jeu_source_perdu_33 = value;
		Il2CppCodeGenWriteBarrier((&___jeu_source_perdu_33), value);
	}

	inline static int32_t get_offset_of_jeu_thumb_perdu_34() { return static_cast<int32_t>(offsetof(MarkerLayer_t3962172947, ___jeu_thumb_perdu_34)); }
	inline String_t* get_jeu_thumb_perdu_34() const { return ___jeu_thumb_perdu_34; }
	inline String_t** get_address_of_jeu_thumb_perdu_34() { return &___jeu_thumb_perdu_34; }
	inline void set_jeu_thumb_perdu_34(String_t* value)
	{
		___jeu_thumb_perdu_34 = value;
		Il2CppCodeGenWriteBarrier((&___jeu_thumb_perdu_34), value);
	}

	inline static int32_t get_offset_of_jeu_data_email_35() { return static_cast<int32_t>(offsetof(MarkerLayer_t3962172947, ___jeu_data_email_35)); }
	inline bool get_jeu_data_email_35() const { return ___jeu_data_email_35; }
	inline bool* get_address_of_jeu_data_email_35() { return &___jeu_data_email_35; }
	inline void set_jeu_data_email_35(bool value)
	{
		___jeu_data_email_35 = value;
	}

	inline static int32_t get_offset_of_jeu_data_email_oblig_36() { return static_cast<int32_t>(offsetof(MarkerLayer_t3962172947, ___jeu_data_email_oblig_36)); }
	inline bool get_jeu_data_email_oblig_36() const { return ___jeu_data_email_oblig_36; }
	inline bool* get_address_of_jeu_data_email_oblig_36() { return &___jeu_data_email_oblig_36; }
	inline void set_jeu_data_email_oblig_36(bool value)
	{
		___jeu_data_email_oblig_36 = value;
	}

	inline static int32_t get_offset_of_jeu_data_nom_37() { return static_cast<int32_t>(offsetof(MarkerLayer_t3962172947, ___jeu_data_nom_37)); }
	inline bool get_jeu_data_nom_37() const { return ___jeu_data_nom_37; }
	inline bool* get_address_of_jeu_data_nom_37() { return &___jeu_data_nom_37; }
	inline void set_jeu_data_nom_37(bool value)
	{
		___jeu_data_nom_37 = value;
	}

	inline static int32_t get_offset_of_jeu_data_nom_oblig_38() { return static_cast<int32_t>(offsetof(MarkerLayer_t3962172947, ___jeu_data_nom_oblig_38)); }
	inline bool get_jeu_data_nom_oblig_38() const { return ___jeu_data_nom_oblig_38; }
	inline bool* get_address_of_jeu_data_nom_oblig_38() { return &___jeu_data_nom_oblig_38; }
	inline void set_jeu_data_nom_oblig_38(bool value)
	{
		___jeu_data_nom_oblig_38 = value;
	}

	inline static int32_t get_offset_of_jeu_data_prenom_39() { return static_cast<int32_t>(offsetof(MarkerLayer_t3962172947, ___jeu_data_prenom_39)); }
	inline bool get_jeu_data_prenom_39() const { return ___jeu_data_prenom_39; }
	inline bool* get_address_of_jeu_data_prenom_39() { return &___jeu_data_prenom_39; }
	inline void set_jeu_data_prenom_39(bool value)
	{
		___jeu_data_prenom_39 = value;
	}

	inline static int32_t get_offset_of_jeu_data_prenom_oblig_40() { return static_cast<int32_t>(offsetof(MarkerLayer_t3962172947, ___jeu_data_prenom_oblig_40)); }
	inline bool get_jeu_data_prenom_oblig_40() const { return ___jeu_data_prenom_oblig_40; }
	inline bool* get_address_of_jeu_data_prenom_oblig_40() { return &___jeu_data_prenom_oblig_40; }
	inline void set_jeu_data_prenom_oblig_40(bool value)
	{
		___jeu_data_prenom_oblig_40 = value;
	}

	inline static int32_t get_offset_of_jeu_data_societe_41() { return static_cast<int32_t>(offsetof(MarkerLayer_t3962172947, ___jeu_data_societe_41)); }
	inline bool get_jeu_data_societe_41() const { return ___jeu_data_societe_41; }
	inline bool* get_address_of_jeu_data_societe_41() { return &___jeu_data_societe_41; }
	inline void set_jeu_data_societe_41(bool value)
	{
		___jeu_data_societe_41 = value;
	}

	inline static int32_t get_offset_of_jeu_data_societe_oblig_42() { return static_cast<int32_t>(offsetof(MarkerLayer_t3962172947, ___jeu_data_societe_oblig_42)); }
	inline bool get_jeu_data_societe_oblig_42() const { return ___jeu_data_societe_oblig_42; }
	inline bool* get_address_of_jeu_data_societe_oblig_42() { return &___jeu_data_societe_oblig_42; }
	inline void set_jeu_data_societe_oblig_42(bool value)
	{
		___jeu_data_societe_oblig_42 = value;
	}

	inline static int32_t get_offset_of_jeu_data_telephone_43() { return static_cast<int32_t>(offsetof(MarkerLayer_t3962172947, ___jeu_data_telephone_43)); }
	inline bool get_jeu_data_telephone_43() const { return ___jeu_data_telephone_43; }
	inline bool* get_address_of_jeu_data_telephone_43() { return &___jeu_data_telephone_43; }
	inline void set_jeu_data_telephone_43(bool value)
	{
		___jeu_data_telephone_43 = value;
	}

	inline static int32_t get_offset_of_jeu_data_telephone_oblig_44() { return static_cast<int32_t>(offsetof(MarkerLayer_t3962172947, ___jeu_data_telephone_oblig_44)); }
	inline bool get_jeu_data_telephone_oblig_44() const { return ___jeu_data_telephone_oblig_44; }
	inline bool* get_address_of_jeu_data_telephone_oblig_44() { return &___jeu_data_telephone_oblig_44; }
	inline void set_jeu_data_telephone_oblig_44(bool value)
	{
		___jeu_data_telephone_oblig_44 = value;
	}

	inline static int32_t get_offset_of_jeu_data_datenaissance_45() { return static_cast<int32_t>(offsetof(MarkerLayer_t3962172947, ___jeu_data_datenaissance_45)); }
	inline bool get_jeu_data_datenaissance_45() const { return ___jeu_data_datenaissance_45; }
	inline bool* get_address_of_jeu_data_datenaissance_45() { return &___jeu_data_datenaissance_45; }
	inline void set_jeu_data_datenaissance_45(bool value)
	{
		___jeu_data_datenaissance_45 = value;
	}

	inline static int32_t get_offset_of_jeu_data_datenaissance_oblig_46() { return static_cast<int32_t>(offsetof(MarkerLayer_t3962172947, ___jeu_data_datenaissance_oblig_46)); }
	inline bool get_jeu_data_datenaissance_oblig_46() const { return ___jeu_data_datenaissance_oblig_46; }
	inline bool* get_address_of_jeu_data_datenaissance_oblig_46() { return &___jeu_data_datenaissance_oblig_46; }
	inline void set_jeu_data_datenaissance_oblig_46(bool value)
	{
		___jeu_data_datenaissance_oblig_46 = value;
	}

	inline static int32_t get_offset_of_jeu_data_extrafield_47() { return static_cast<int32_t>(offsetof(MarkerLayer_t3962172947, ___jeu_data_extrafield_47)); }
	inline bool get_jeu_data_extrafield_47() const { return ___jeu_data_extrafield_47; }
	inline bool* get_address_of_jeu_data_extrafield_47() { return &___jeu_data_extrafield_47; }
	inline void set_jeu_data_extrafield_47(bool value)
	{
		___jeu_data_extrafield_47 = value;
	}

	inline static int32_t get_offset_of_jeu_data_extrafield_oblig_48() { return static_cast<int32_t>(offsetof(MarkerLayer_t3962172947, ___jeu_data_extrafield_oblig_48)); }
	inline bool get_jeu_data_extrafield_oblig_48() const { return ___jeu_data_extrafield_oblig_48; }
	inline bool* get_address_of_jeu_data_extrafield_oblig_48() { return &___jeu_data_extrafield_oblig_48; }
	inline void set_jeu_data_extrafield_oblig_48(bool value)
	{
		___jeu_data_extrafield_oblig_48 = value;
	}

	inline static int32_t get_offset_of_jeu_data_str_extrafield_49() { return static_cast<int32_t>(offsetof(MarkerLayer_t3962172947, ___jeu_data_str_extrafield_49)); }
	inline String_t* get_jeu_data_str_extrafield_49() const { return ___jeu_data_str_extrafield_49; }
	inline String_t** get_address_of_jeu_data_str_extrafield_49() { return &___jeu_data_str_extrafield_49; }
	inline void set_jeu_data_str_extrafield_49(String_t* value)
	{
		___jeu_data_str_extrafield_49 = value;
		Il2CppCodeGenWriteBarrier((&___jeu_data_str_extrafield_49), value);
	}

	inline static int32_t get_offset_of_son_source_50() { return static_cast<int32_t>(offsetof(MarkerLayer_t3962172947, ___son_source_50)); }
	inline String_t* get_son_source_50() const { return ___son_source_50; }
	inline String_t** get_address_of_son_source_50() { return &___son_source_50; }
	inline void set_son_source_50(String_t* value)
	{
		___son_source_50 = value;
		Il2CppCodeGenWriteBarrier((&___son_source_50), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MARKERLAYER_T3962172947_H
#ifndef MARKERMANAGER_T1031803141_H
#define MARKERMANAGER_T1031803141_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MarkerManager
struct  MarkerManager_t1031803141  : public RuntimeObject
{
public:
	// System.Int32 MarkerManager::layerCount
	int32_t ___layerCount_0;
	// MarkerData MarkerManager::marker
	MarkerData_t1252592219 * ___marker_1;

public:
	inline static int32_t get_offset_of_layerCount_0() { return static_cast<int32_t>(offsetof(MarkerManager_t1031803141, ___layerCount_0)); }
	inline int32_t get_layerCount_0() const { return ___layerCount_0; }
	inline int32_t* get_address_of_layerCount_0() { return &___layerCount_0; }
	inline void set_layerCount_0(int32_t value)
	{
		___layerCount_0 = value;
	}

	inline static int32_t get_offset_of_marker_1() { return static_cast<int32_t>(offsetof(MarkerManager_t1031803141, ___marker_1)); }
	inline MarkerData_t1252592219 * get_marker_1() const { return ___marker_1; }
	inline MarkerData_t1252592219 ** get_address_of_marker_1() { return &___marker_1; }
	inline void set_marker_1(MarkerData_t1252592219 * value)
	{
		___marker_1 = value;
		Il2CppCodeGenWriteBarrier((&___marker_1), value);
	}
};

struct MarkerManager_t1031803141_StaticFields
{
public:
	// MarkerManager MarkerManager::_Instance
	MarkerManager_t1031803141 * ____Instance_2;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> MarkerManager::<>f__switch$map0
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map0_3;

public:
	inline static int32_t get_offset_of__Instance_2() { return static_cast<int32_t>(offsetof(MarkerManager_t1031803141_StaticFields, ____Instance_2)); }
	inline MarkerManager_t1031803141 * get__Instance_2() const { return ____Instance_2; }
	inline MarkerManager_t1031803141 ** get_address_of__Instance_2() { return &____Instance_2; }
	inline void set__Instance_2(MarkerManager_t1031803141 * value)
	{
		____Instance_2 = value;
		Il2CppCodeGenWriteBarrier((&____Instance_2), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map0_3() { return static_cast<int32_t>(offsetof(MarkerManager_t1031803141_StaticFields, ___U3CU3Ef__switchU24map0_3)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map0_3() const { return ___U3CU3Ef__switchU24map0_3; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map0_3() { return &___U3CU3Ef__switchU24map0_3; }
	inline void set_U3CU3Ef__switchU24map0_3(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map0_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MARKERMANAGER_T1031803141_H
#ifndef U3CLOADU3EC__ITERATOR0_T1679360452_H
#define U3CLOADU3EC__ITERATOR0_T1679360452_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OBJLoaderManager/<load>c__Iterator0
struct  U3CloadU3Ec__Iterator0_t1679360452  : public RuntimeObject
{
public:
	// UnityEngine.WWW OBJLoaderManager/<load>c__Iterator0::<www>__0
	WWW_t3688466362 * ___U3CwwwU3E__0_0;
	// System.Object OBJLoaderManager/<load>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean OBJLoaderManager/<load>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 OBJLoaderManager/<load>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U3CwwwU3E__0_0() { return static_cast<int32_t>(offsetof(U3CloadU3Ec__Iterator0_t1679360452, ___U3CwwwU3E__0_0)); }
	inline WWW_t3688466362 * get_U3CwwwU3E__0_0() const { return ___U3CwwwU3E__0_0; }
	inline WWW_t3688466362 ** get_address_of_U3CwwwU3E__0_0() { return &___U3CwwwU3E__0_0; }
	inline void set_U3CwwwU3E__0_0(WWW_t3688466362 * value)
	{
		___U3CwwwU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwwwU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CloadU3Ec__Iterator0_t1679360452, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CloadU3Ec__Iterator0_t1679360452, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CloadU3Ec__Iterator0_t1679360452, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADU3EC__ITERATOR0_T1679360452_H
#ifndef U3CGOTOSPECIFICPAGEU3EC__ANONSTOREY0_T632505814_H
#define U3CGOTOSPECIFICPAGEU3EC__ANONSTOREY0_T632505814_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PageTransition/<GotoSpecificPage>c__AnonStorey0
struct  U3CGotoSpecificPageU3Ec__AnonStorey0_t632505814  : public RuntimeObject
{
public:
	// Page PageTransition/<GotoSpecificPage>c__AnonStorey0::currentPage
	Page_t2586579457 * ___currentPage_0;

public:
	inline static int32_t get_offset_of_currentPage_0() { return static_cast<int32_t>(offsetof(U3CGotoSpecificPageU3Ec__AnonStorey0_t632505814, ___currentPage_0)); }
	inline Page_t2586579457 * get_currentPage_0() const { return ___currentPage_0; }
	inline Page_t2586579457 ** get_address_of_currentPage_0() { return &___currentPage_0; }
	inline void set_currentPage_0(Page_t2586579457 * value)
	{
		___currentPage_0 = value;
		Il2CppCodeGenWriteBarrier((&___currentPage_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGOTOSPECIFICPAGEU3EC__ANONSTOREY0_T632505814_H
#ifndef U3CSENDEMAIL_IMPU3EC__ITERATOR0_T3798648854_H
#define U3CSENDEMAIL_IMPU3EC__ITERATOR0_T3798648854_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PanoForm/<sendEmail_IMP>c__Iterator0
struct  U3CsendEmail_IMPU3Ec__Iterator0_t3798648854  : public RuntimeObject
{
public:
	// System.String PanoForm/<sendEmail_IMP>c__Iterator0::<url>__0
	String_t* ___U3CurlU3E__0_0;
	// UnityEngine.WWW PanoForm/<sendEmail_IMP>c__Iterator0::<www>__0
	WWW_t3688466362 * ___U3CwwwU3E__0_1;
	// SimpleJSON.JSONNode PanoForm/<sendEmail_IMP>c__Iterator0::<Nbase>__0
	JSONNode_t2946056997 * ___U3CNbaseU3E__0_2;
	// System.String PanoForm/<sendEmail_IMP>c__Iterator0::<status>__0
	String_t* ___U3CstatusU3E__0_3;
	// System.String PanoForm/<sendEmail_IMP>c__Iterator0::<textDisplay>__0
	String_t* ___U3CtextDisplayU3E__0_4;
	// PanoForm PanoForm/<sendEmail_IMP>c__Iterator0::$this
	PanoForm_t499938582 * ___U24this_5;
	// System.Object PanoForm/<sendEmail_IMP>c__Iterator0::$current
	RuntimeObject * ___U24current_6;
	// System.Boolean PanoForm/<sendEmail_IMP>c__Iterator0::$disposing
	bool ___U24disposing_7;
	// System.Int32 PanoForm/<sendEmail_IMP>c__Iterator0::$PC
	int32_t ___U24PC_8;

public:
	inline static int32_t get_offset_of_U3CurlU3E__0_0() { return static_cast<int32_t>(offsetof(U3CsendEmail_IMPU3Ec__Iterator0_t3798648854, ___U3CurlU3E__0_0)); }
	inline String_t* get_U3CurlU3E__0_0() const { return ___U3CurlU3E__0_0; }
	inline String_t** get_address_of_U3CurlU3E__0_0() { return &___U3CurlU3E__0_0; }
	inline void set_U3CurlU3E__0_0(String_t* value)
	{
		___U3CurlU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CurlU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E__0_1() { return static_cast<int32_t>(offsetof(U3CsendEmail_IMPU3Ec__Iterator0_t3798648854, ___U3CwwwU3E__0_1)); }
	inline WWW_t3688466362 * get_U3CwwwU3E__0_1() const { return ___U3CwwwU3E__0_1; }
	inline WWW_t3688466362 ** get_address_of_U3CwwwU3E__0_1() { return &___U3CwwwU3E__0_1; }
	inline void set_U3CwwwU3E__0_1(WWW_t3688466362 * value)
	{
		___U3CwwwU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwwwU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U3CNbaseU3E__0_2() { return static_cast<int32_t>(offsetof(U3CsendEmail_IMPU3Ec__Iterator0_t3798648854, ___U3CNbaseU3E__0_2)); }
	inline JSONNode_t2946056997 * get_U3CNbaseU3E__0_2() const { return ___U3CNbaseU3E__0_2; }
	inline JSONNode_t2946056997 ** get_address_of_U3CNbaseU3E__0_2() { return &___U3CNbaseU3E__0_2; }
	inline void set_U3CNbaseU3E__0_2(JSONNode_t2946056997 * value)
	{
		___U3CNbaseU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNbaseU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U3CstatusU3E__0_3() { return static_cast<int32_t>(offsetof(U3CsendEmail_IMPU3Ec__Iterator0_t3798648854, ___U3CstatusU3E__0_3)); }
	inline String_t* get_U3CstatusU3E__0_3() const { return ___U3CstatusU3E__0_3; }
	inline String_t** get_address_of_U3CstatusU3E__0_3() { return &___U3CstatusU3E__0_3; }
	inline void set_U3CstatusU3E__0_3(String_t* value)
	{
		___U3CstatusU3E__0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CstatusU3E__0_3), value);
	}

	inline static int32_t get_offset_of_U3CtextDisplayU3E__0_4() { return static_cast<int32_t>(offsetof(U3CsendEmail_IMPU3Ec__Iterator0_t3798648854, ___U3CtextDisplayU3E__0_4)); }
	inline String_t* get_U3CtextDisplayU3E__0_4() const { return ___U3CtextDisplayU3E__0_4; }
	inline String_t** get_address_of_U3CtextDisplayU3E__0_4() { return &___U3CtextDisplayU3E__0_4; }
	inline void set_U3CtextDisplayU3E__0_4(String_t* value)
	{
		___U3CtextDisplayU3E__0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtextDisplayU3E__0_4), value);
	}

	inline static int32_t get_offset_of_U24this_5() { return static_cast<int32_t>(offsetof(U3CsendEmail_IMPU3Ec__Iterator0_t3798648854, ___U24this_5)); }
	inline PanoForm_t499938582 * get_U24this_5() const { return ___U24this_5; }
	inline PanoForm_t499938582 ** get_address_of_U24this_5() { return &___U24this_5; }
	inline void set_U24this_5(PanoForm_t499938582 * value)
	{
		___U24this_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_5), value);
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CsendEmail_IMPU3Ec__Iterator0_t3798648854, ___U24current_6)); }
	inline RuntimeObject * get_U24current_6() const { return ___U24current_6; }
	inline RuntimeObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(RuntimeObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_6), value);
	}

	inline static int32_t get_offset_of_U24disposing_7() { return static_cast<int32_t>(offsetof(U3CsendEmail_IMPU3Ec__Iterator0_t3798648854, ___U24disposing_7)); }
	inline bool get_U24disposing_7() const { return ___U24disposing_7; }
	inline bool* get_address_of_U24disposing_7() { return &___U24disposing_7; }
	inline void set_U24disposing_7(bool value)
	{
		___U24disposing_7 = value;
	}

	inline static int32_t get_offset_of_U24PC_8() { return static_cast<int32_t>(offsetof(U3CsendEmail_IMPU3Ec__Iterator0_t3798648854, ___U24PC_8)); }
	inline int32_t get_U24PC_8() const { return ___U24PC_8; }
	inline int32_t* get_address_of_U24PC_8() { return &___U24PC_8; }
	inline void set_U24PC_8(int32_t value)
	{
		___U24PC_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSENDEMAIL_IMPU3EC__ITERATOR0_T3798648854_H
#ifndef JSON_T1924642173_H
#define JSON_T1924642173_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleJSON.JSON
struct  JSON_t1924642173  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSON_T1924642173_H
#ifndef JSONNODE_T2946056997_H
#define JSONNODE_T2946056997_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleJSON.JSONNode
struct  JSONNode_t2946056997  : public RuntimeObject
{
public:

public:
};

struct JSONNode_t2946056997_StaticFields
{
public:
	// System.Text.StringBuilder SimpleJSON.JSONNode::m_EscapeBuilder
	StringBuilder_t * ___m_EscapeBuilder_0;

public:
	inline static int32_t get_offset_of_m_EscapeBuilder_0() { return static_cast<int32_t>(offsetof(JSONNode_t2946056997_StaticFields, ___m_EscapeBuilder_0)); }
	inline StringBuilder_t * get_m_EscapeBuilder_0() const { return ___m_EscapeBuilder_0; }
	inline StringBuilder_t ** get_address_of_m_EscapeBuilder_0() { return &___m_EscapeBuilder_0; }
	inline void set_m_EscapeBuilder_0(StringBuilder_t * value)
	{
		___m_EscapeBuilder_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_EscapeBuilder_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONNODE_T2946056997_H
#ifndef U3CU3EC__ITERATOR0_T2360448098_H
#define U3CU3EC__ITERATOR0_T2360448098_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleJSON.JSONNode/<>c__Iterator0
struct  U3CU3Ec__Iterator0_t2360448098  : public RuntimeObject
{
public:
	// System.String SimpleJSON.JSONNode/<>c__Iterator0::$current
	String_t* ___U24current_0;
	// System.Boolean SimpleJSON.JSONNode/<>c__Iterator0::$disposing
	bool ___U24disposing_1;
	// System.Int32 SimpleJSON.JSONNode/<>c__Iterator0::$PC
	int32_t ___U24PC_2;

public:
	inline static int32_t get_offset_of_U24current_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t2360448098, ___U24current_0)); }
	inline String_t* get_U24current_0() const { return ___U24current_0; }
	inline String_t** get_address_of_U24current_0() { return &___U24current_0; }
	inline void set_U24current_0(String_t* value)
	{
		___U24current_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_0), value);
	}

	inline static int32_t get_offset_of_U24disposing_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t2360448098, ___U24disposing_1)); }
	inline bool get_U24disposing_1() const { return ___U24disposing_1; }
	inline bool* get_address_of_U24disposing_1() { return &___U24disposing_1; }
	inline void set_U24disposing_1(bool value)
	{
		___U24disposing_1 = value;
	}

	inline static int32_t get_offset_of_U24PC_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t2360448098, ___U24PC_2)); }
	inline int32_t get_U24PC_2() const { return ___U24PC_2; }
	inline int32_t* get_address_of_U24PC_2() { return &___U24PC_2; }
	inline void set_U24PC_2(int32_t value)
	{
		___U24PC_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__ITERATOR0_T2360448098_H
#ifndef U3CU3EC__ITERATOR1_T2360382562_H
#define U3CU3EC__ITERATOR1_T2360382562_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleJSON.JSONNode/<>c__Iterator1
struct  U3CU3Ec__Iterator1_t2360382562  : public RuntimeObject
{
public:
	// SimpleJSON.JSONNode SimpleJSON.JSONNode/<>c__Iterator1::$current
	JSONNode_t2946056997 * ___U24current_0;
	// System.Boolean SimpleJSON.JSONNode/<>c__Iterator1::$disposing
	bool ___U24disposing_1;
	// System.Int32 SimpleJSON.JSONNode/<>c__Iterator1::$PC
	int32_t ___U24PC_2;

public:
	inline static int32_t get_offset_of_U24current_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator1_t2360382562, ___U24current_0)); }
	inline JSONNode_t2946056997 * get_U24current_0() const { return ___U24current_0; }
	inline JSONNode_t2946056997 ** get_address_of_U24current_0() { return &___U24current_0; }
	inline void set_U24current_0(JSONNode_t2946056997 * value)
	{
		___U24current_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_0), value);
	}

	inline static int32_t get_offset_of_U24disposing_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator1_t2360382562, ___U24disposing_1)); }
	inline bool get_U24disposing_1() const { return ___U24disposing_1; }
	inline bool* get_address_of_U24disposing_1() { return &___U24disposing_1; }
	inline void set_U24disposing_1(bool value)
	{
		___U24disposing_1 = value;
	}

	inline static int32_t get_offset_of_U24PC_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator1_t2360382562, ___U24PC_2)); }
	inline int32_t get_U24PC_2() const { return ___U24PC_2; }
	inline int32_t* get_address_of_U24PC_2() { return &___U24PC_2; }
	inline void set_U24PC_2(int32_t value)
	{
		___U24PC_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__ITERATOR1_T2360382562_H
#ifndef U3CU3EC__ITERATOR2_T2360579170_H
#define U3CU3EC__ITERATOR2_T2360579170_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleJSON.JSONNode/<>c__Iterator2
struct  U3CU3Ec__Iterator2_t2360579170  : public RuntimeObject
{
public:
	// System.Collections.Generic.IEnumerator`1<SimpleJSON.JSONNode> SimpleJSON.JSONNode/<>c__Iterator2::$locvar0
	RuntimeObject* ___U24locvar0_0;
	// SimpleJSON.JSONNode SimpleJSON.JSONNode/<>c__Iterator2::<C>__1
	JSONNode_t2946056997 * ___U3CCU3E__1_1;
	// System.Collections.Generic.IEnumerator`1<SimpleJSON.JSONNode> SimpleJSON.JSONNode/<>c__Iterator2::$locvar1
	RuntimeObject* ___U24locvar1_2;
	// SimpleJSON.JSONNode SimpleJSON.JSONNode/<>c__Iterator2::<D>__2
	JSONNode_t2946056997 * ___U3CDU3E__2_3;
	// SimpleJSON.JSONNode SimpleJSON.JSONNode/<>c__Iterator2::$this
	JSONNode_t2946056997 * ___U24this_4;
	// SimpleJSON.JSONNode SimpleJSON.JSONNode/<>c__Iterator2::$current
	JSONNode_t2946056997 * ___U24current_5;
	// System.Boolean SimpleJSON.JSONNode/<>c__Iterator2::$disposing
	bool ___U24disposing_6;
	// System.Int32 SimpleJSON.JSONNode/<>c__Iterator2::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_U24locvar0_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator2_t2360579170, ___U24locvar0_0)); }
	inline RuntimeObject* get_U24locvar0_0() const { return ___U24locvar0_0; }
	inline RuntimeObject** get_address_of_U24locvar0_0() { return &___U24locvar0_0; }
	inline void set_U24locvar0_0(RuntimeObject* value)
	{
		___U24locvar0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24locvar0_0), value);
	}

	inline static int32_t get_offset_of_U3CCU3E__1_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator2_t2360579170, ___U3CCU3E__1_1)); }
	inline JSONNode_t2946056997 * get_U3CCU3E__1_1() const { return ___U3CCU3E__1_1; }
	inline JSONNode_t2946056997 ** get_address_of_U3CCU3E__1_1() { return &___U3CCU3E__1_1; }
	inline void set_U3CCU3E__1_1(JSONNode_t2946056997 * value)
	{
		___U3CCU3E__1_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCU3E__1_1), value);
	}

	inline static int32_t get_offset_of_U24locvar1_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator2_t2360579170, ___U24locvar1_2)); }
	inline RuntimeObject* get_U24locvar1_2() const { return ___U24locvar1_2; }
	inline RuntimeObject** get_address_of_U24locvar1_2() { return &___U24locvar1_2; }
	inline void set_U24locvar1_2(RuntimeObject* value)
	{
		___U24locvar1_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24locvar1_2), value);
	}

	inline static int32_t get_offset_of_U3CDU3E__2_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator2_t2360579170, ___U3CDU3E__2_3)); }
	inline JSONNode_t2946056997 * get_U3CDU3E__2_3() const { return ___U3CDU3E__2_3; }
	inline JSONNode_t2946056997 ** get_address_of_U3CDU3E__2_3() { return &___U3CDU3E__2_3; }
	inline void set_U3CDU3E__2_3(JSONNode_t2946056997 * value)
	{
		___U3CDU3E__2_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDU3E__2_3), value);
	}

	inline static int32_t get_offset_of_U24this_4() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator2_t2360579170, ___U24this_4)); }
	inline JSONNode_t2946056997 * get_U24this_4() const { return ___U24this_4; }
	inline JSONNode_t2946056997 ** get_address_of_U24this_4() { return &___U24this_4; }
	inline void set_U24this_4(JSONNode_t2946056997 * value)
	{
		___U24this_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_4), value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator2_t2360579170, ___U24current_5)); }
	inline JSONNode_t2946056997 * get_U24current_5() const { return ___U24current_5; }
	inline JSONNode_t2946056997 ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(JSONNode_t2946056997 * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_5), value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator2_t2360579170, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator2_t2360579170, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__ITERATOR2_T2360579170_H
#ifndef U3CREMOVEU3EC__ANONSTOREY3_T2674960018_H
#define U3CREMOVEU3EC__ANONSTOREY3_T2674960018_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleJSON.JSONObject/<Remove>c__AnonStorey3
struct  U3CRemoveU3Ec__AnonStorey3_t2674960018  : public RuntimeObject
{
public:
	// SimpleJSON.JSONNode SimpleJSON.JSONObject/<Remove>c__AnonStorey3::aNode
	JSONNode_t2946056997 * ___aNode_0;

public:
	inline static int32_t get_offset_of_aNode_0() { return static_cast<int32_t>(offsetof(U3CRemoveU3Ec__AnonStorey3_t2674960018, ___aNode_0)); }
	inline JSONNode_t2946056997 * get_aNode_0() const { return ___aNode_0; }
	inline JSONNode_t2946056997 ** get_address_of_aNode_0() { return &___aNode_0; }
	inline void set_aNode_0(JSONNode_t2946056997 * value)
	{
		___aNode_0 = value;
		Il2CppCodeGenWriteBarrier((&___aNode_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CREMOVEU3EC__ANONSTOREY3_T2674960018_H
#ifndef U3CDELETECACHEU3EC__ITERATOR1_T475136523_H
#define U3CDELETECACHEU3EC__ITERATOR1_T475136523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SplashTimer/<DeleteCache>c__Iterator1
struct  U3CDeleteCacheU3Ec__Iterator1_t475136523  : public RuntimeObject
{
public:
	// System.String SplashTimer/<DeleteCache>c__Iterator1::<path>__0
	String_t* ___U3CpathU3E__0_0;
	// System.IO.DirectoryInfo SplashTimer/<DeleteCache>c__Iterator1::<di>__0
	DirectoryInfo_t35957480 * ___U3CdiU3E__0_1;
	// System.IO.FileInfo[] SplashTimer/<DeleteCache>c__Iterator1::$locvar0
	FileInfoU5BU5D_t2389029403* ___U24locvar0_2;
	// System.Int32 SplashTimer/<DeleteCache>c__Iterator1::$locvar1
	int32_t ___U24locvar1_3;
	// System.IO.FileInfo SplashTimer/<DeleteCache>c__Iterator1::<file>__1
	FileInfo_t1169991790 * ___U3CfileU3E__1_4;
	// System.IO.DirectoryInfo[] SplashTimer/<DeleteCache>c__Iterator1::$locvar2
	DirectoryInfoU5BU5D_t1343554681* ___U24locvar2_5;
	// System.Int32 SplashTimer/<DeleteCache>c__Iterator1::$locvar3
	int32_t ___U24locvar3_6;
	// System.IO.DirectoryInfo SplashTimer/<DeleteCache>c__Iterator1::<dir>__2
	DirectoryInfo_t35957480 * ___U3CdirU3E__2_7;
	// System.Object SplashTimer/<DeleteCache>c__Iterator1::$current
	RuntimeObject * ___U24current_8;
	// System.Boolean SplashTimer/<DeleteCache>c__Iterator1::$disposing
	bool ___U24disposing_9;
	// System.Int32 SplashTimer/<DeleteCache>c__Iterator1::$PC
	int32_t ___U24PC_10;

public:
	inline static int32_t get_offset_of_U3CpathU3E__0_0() { return static_cast<int32_t>(offsetof(U3CDeleteCacheU3Ec__Iterator1_t475136523, ___U3CpathU3E__0_0)); }
	inline String_t* get_U3CpathU3E__0_0() const { return ___U3CpathU3E__0_0; }
	inline String_t** get_address_of_U3CpathU3E__0_0() { return &___U3CpathU3E__0_0; }
	inline void set_U3CpathU3E__0_0(String_t* value)
	{
		___U3CpathU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CpathU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3CdiU3E__0_1() { return static_cast<int32_t>(offsetof(U3CDeleteCacheU3Ec__Iterator1_t475136523, ___U3CdiU3E__0_1)); }
	inline DirectoryInfo_t35957480 * get_U3CdiU3E__0_1() const { return ___U3CdiU3E__0_1; }
	inline DirectoryInfo_t35957480 ** get_address_of_U3CdiU3E__0_1() { return &___U3CdiU3E__0_1; }
	inline void set_U3CdiU3E__0_1(DirectoryInfo_t35957480 * value)
	{
		___U3CdiU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CdiU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U24locvar0_2() { return static_cast<int32_t>(offsetof(U3CDeleteCacheU3Ec__Iterator1_t475136523, ___U24locvar0_2)); }
	inline FileInfoU5BU5D_t2389029403* get_U24locvar0_2() const { return ___U24locvar0_2; }
	inline FileInfoU5BU5D_t2389029403** get_address_of_U24locvar0_2() { return &___U24locvar0_2; }
	inline void set_U24locvar0_2(FileInfoU5BU5D_t2389029403* value)
	{
		___U24locvar0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24locvar0_2), value);
	}

	inline static int32_t get_offset_of_U24locvar1_3() { return static_cast<int32_t>(offsetof(U3CDeleteCacheU3Ec__Iterator1_t475136523, ___U24locvar1_3)); }
	inline int32_t get_U24locvar1_3() const { return ___U24locvar1_3; }
	inline int32_t* get_address_of_U24locvar1_3() { return &___U24locvar1_3; }
	inline void set_U24locvar1_3(int32_t value)
	{
		___U24locvar1_3 = value;
	}

	inline static int32_t get_offset_of_U3CfileU3E__1_4() { return static_cast<int32_t>(offsetof(U3CDeleteCacheU3Ec__Iterator1_t475136523, ___U3CfileU3E__1_4)); }
	inline FileInfo_t1169991790 * get_U3CfileU3E__1_4() const { return ___U3CfileU3E__1_4; }
	inline FileInfo_t1169991790 ** get_address_of_U3CfileU3E__1_4() { return &___U3CfileU3E__1_4; }
	inline void set_U3CfileU3E__1_4(FileInfo_t1169991790 * value)
	{
		___U3CfileU3E__1_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CfileU3E__1_4), value);
	}

	inline static int32_t get_offset_of_U24locvar2_5() { return static_cast<int32_t>(offsetof(U3CDeleteCacheU3Ec__Iterator1_t475136523, ___U24locvar2_5)); }
	inline DirectoryInfoU5BU5D_t1343554681* get_U24locvar2_5() const { return ___U24locvar2_5; }
	inline DirectoryInfoU5BU5D_t1343554681** get_address_of_U24locvar2_5() { return &___U24locvar2_5; }
	inline void set_U24locvar2_5(DirectoryInfoU5BU5D_t1343554681* value)
	{
		___U24locvar2_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24locvar2_5), value);
	}

	inline static int32_t get_offset_of_U24locvar3_6() { return static_cast<int32_t>(offsetof(U3CDeleteCacheU3Ec__Iterator1_t475136523, ___U24locvar3_6)); }
	inline int32_t get_U24locvar3_6() const { return ___U24locvar3_6; }
	inline int32_t* get_address_of_U24locvar3_6() { return &___U24locvar3_6; }
	inline void set_U24locvar3_6(int32_t value)
	{
		___U24locvar3_6 = value;
	}

	inline static int32_t get_offset_of_U3CdirU3E__2_7() { return static_cast<int32_t>(offsetof(U3CDeleteCacheU3Ec__Iterator1_t475136523, ___U3CdirU3E__2_7)); }
	inline DirectoryInfo_t35957480 * get_U3CdirU3E__2_7() const { return ___U3CdirU3E__2_7; }
	inline DirectoryInfo_t35957480 ** get_address_of_U3CdirU3E__2_7() { return &___U3CdirU3E__2_7; }
	inline void set_U3CdirU3E__2_7(DirectoryInfo_t35957480 * value)
	{
		___U3CdirU3E__2_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CdirU3E__2_7), value);
	}

	inline static int32_t get_offset_of_U24current_8() { return static_cast<int32_t>(offsetof(U3CDeleteCacheU3Ec__Iterator1_t475136523, ___U24current_8)); }
	inline RuntimeObject * get_U24current_8() const { return ___U24current_8; }
	inline RuntimeObject ** get_address_of_U24current_8() { return &___U24current_8; }
	inline void set_U24current_8(RuntimeObject * value)
	{
		___U24current_8 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_8), value);
	}

	inline static int32_t get_offset_of_U24disposing_9() { return static_cast<int32_t>(offsetof(U3CDeleteCacheU3Ec__Iterator1_t475136523, ___U24disposing_9)); }
	inline bool get_U24disposing_9() const { return ___U24disposing_9; }
	inline bool* get_address_of_U24disposing_9() { return &___U24disposing_9; }
	inline void set_U24disposing_9(bool value)
	{
		___U24disposing_9 = value;
	}

	inline static int32_t get_offset_of_U24PC_10() { return static_cast<int32_t>(offsetof(U3CDeleteCacheU3Ec__Iterator1_t475136523, ___U24PC_10)); }
	inline int32_t get_U24PC_10() const { return ___U24PC_10; }
	inline int32_t* get_address_of_U24PC_10() { return &___U24PC_10; }
	inline void set_U24PC_10(int32_t value)
	{
		___U24PC_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDELETECACHEU3EC__ITERATOR1_T475136523_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef TRANSLATIONMANAGER_T363825059_H
#define TRANSLATIONMANAGER_T363825059_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TranslationManager
struct  TranslationManager_t363825059  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.String>[] TranslationManager::lang
	Dictionary_2U5BU5D_t1236182565* ___lang_0;

public:
	inline static int32_t get_offset_of_lang_0() { return static_cast<int32_t>(offsetof(TranslationManager_t363825059, ___lang_0)); }
	inline Dictionary_2U5BU5D_t1236182565* get_lang_0() const { return ___lang_0; }
	inline Dictionary_2U5BU5D_t1236182565** get_address_of_lang_0() { return &___lang_0; }
	inline void set_lang_0(Dictionary_2U5BU5D_t1236182565* value)
	{
		___lang_0 = value;
		Il2CppCodeGenWriteBarrier((&___lang_0), value);
	}
};

struct TranslationManager_t363825059_StaticFields
{
public:
	// System.Int32 TranslationManager::choosenLang
	int32_t ___choosenLang_1;
	// TranslationManager TranslationManager::_Instance
	TranslationManager_t363825059 * ____Instance_2;

public:
	inline static int32_t get_offset_of_choosenLang_1() { return static_cast<int32_t>(offsetof(TranslationManager_t363825059_StaticFields, ___choosenLang_1)); }
	inline int32_t get_choosenLang_1() const { return ___choosenLang_1; }
	inline int32_t* get_address_of_choosenLang_1() { return &___choosenLang_1; }
	inline void set_choosenLang_1(int32_t value)
	{
		___choosenLang_1 = value;
	}

	inline static int32_t get_offset_of__Instance_2() { return static_cast<int32_t>(offsetof(TranslationManager_t363825059_StaticFields, ____Instance_2)); }
	inline TranslationManager_t363825059 * get__Instance_2() const { return ____Instance_2; }
	inline TranslationManager_t363825059 ** get_address_of__Instance_2() { return &____Instance_2; }
	inline void set__Instance_2(TranslationManager_t363825059 * value)
	{
		____Instance_2 = value;
		Il2CppCodeGenWriteBarrier((&____Instance_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSLATIONMANAGER_T363825059_H
#ifndef UNITYEVENTBASE_T3960448221_H
#define UNITYEVENTBASE_T3960448221_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEventBase
struct  UnityEventBase_t3960448221  : public RuntimeObject
{
public:
	// UnityEngine.Events.InvokableCallList UnityEngine.Events.UnityEventBase::m_Calls
	InvokableCallList_t2498835369 * ___m_Calls_0;
	// UnityEngine.Events.PersistentCallGroup UnityEngine.Events.UnityEventBase::m_PersistentCalls
	PersistentCallGroup_t3050769227 * ___m_PersistentCalls_1;
	// System.String UnityEngine.Events.UnityEventBase::m_TypeName
	String_t* ___m_TypeName_2;
	// System.Boolean UnityEngine.Events.UnityEventBase::m_CallsDirty
	bool ___m_CallsDirty_3;

public:
	inline static int32_t get_offset_of_m_Calls_0() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_Calls_0)); }
	inline InvokableCallList_t2498835369 * get_m_Calls_0() const { return ___m_Calls_0; }
	inline InvokableCallList_t2498835369 ** get_address_of_m_Calls_0() { return &___m_Calls_0; }
	inline void set_m_Calls_0(InvokableCallList_t2498835369 * value)
	{
		___m_Calls_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Calls_0), value);
	}

	inline static int32_t get_offset_of_m_PersistentCalls_1() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_PersistentCalls_1)); }
	inline PersistentCallGroup_t3050769227 * get_m_PersistentCalls_1() const { return ___m_PersistentCalls_1; }
	inline PersistentCallGroup_t3050769227 ** get_address_of_m_PersistentCalls_1() { return &___m_PersistentCalls_1; }
	inline void set_m_PersistentCalls_1(PersistentCallGroup_t3050769227 * value)
	{
		___m_PersistentCalls_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PersistentCalls_1), value);
	}

	inline static int32_t get_offset_of_m_TypeName_2() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_TypeName_2)); }
	inline String_t* get_m_TypeName_2() const { return ___m_TypeName_2; }
	inline String_t** get_address_of_m_TypeName_2() { return &___m_TypeName_2; }
	inline void set_m_TypeName_2(String_t* value)
	{
		___m_TypeName_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_TypeName_2), value);
	}

	inline static int32_t get_offset_of_m_CallsDirty_3() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_CallsDirty_3)); }
	inline bool get_m_CallsDirty_3() const { return ___m_CallsDirty_3; }
	inline bool* get_address_of_m_CallsDirty_3() { return &___m_CallsDirty_3; }
	inline void set_m_CallsDirty_3(bool value)
	{
		___m_CallsDirty_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENTBASE_T3960448221_H
#ifndef U3CIMPL_SENDCAPTUREU3EC__ITERATOR0_T1292966789_H
#define U3CIMPL_SENDCAPTUREU3EC__ITERATOR0_T1292966789_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// captureAndSendScreen/<IMPL_sendCapture>c__Iterator0
struct  U3CIMPL_sendCaptureU3Ec__Iterator0_t1292966789  : public RuntimeObject
{
public:
	// System.Int32 captureAndSendScreen/<IMPL_sendCapture>c__Iterator0::<width>__0
	int32_t ___U3CwidthU3E__0_0;
	// System.Int32 captureAndSendScreen/<IMPL_sendCapture>c__Iterator0::<height>__0
	int32_t ___U3CheightU3E__0_1;
	// UnityEngine.Renderer captureAndSendScreen/<IMPL_sendCapture>c__Iterator0::<rendererBack>__0
	Renderer_t2627027031 * ___U3CrendererBackU3E__0_2;
	// UnityEngine.Texture2D captureAndSendScreen/<IMPL_sendCapture>c__Iterator0::<tex>__0
	Texture2D_t3840446185 * ___U3CtexU3E__0_3;
	// System.Byte[] captureAndSendScreen/<IMPL_sendCapture>c__Iterator0::<bytes>__0
	ByteU5BU5D_t4116647657* ___U3CbytesU3E__0_4;
	// UnityEngine.WWWForm captureAndSendScreen/<IMPL_sendCapture>c__Iterator0::<form>__0
	WWWForm_t4064702195 * ___U3CformU3E__0_5;
	// UnityEngine.Networking.UnityWebRequest captureAndSendScreen/<IMPL_sendCapture>c__Iterator0::<w>__0
	UnityWebRequest_t463507806 * ___U3CwU3E__0_6;
	// captureAndSendScreen captureAndSendScreen/<IMPL_sendCapture>c__Iterator0::$this
	captureAndSendScreen_t3196082656 * ___U24this_7;
	// System.Object captureAndSendScreen/<IMPL_sendCapture>c__Iterator0::$current
	RuntimeObject * ___U24current_8;
	// System.Boolean captureAndSendScreen/<IMPL_sendCapture>c__Iterator0::$disposing
	bool ___U24disposing_9;
	// System.Int32 captureAndSendScreen/<IMPL_sendCapture>c__Iterator0::$PC
	int32_t ___U24PC_10;

public:
	inline static int32_t get_offset_of_U3CwidthU3E__0_0() { return static_cast<int32_t>(offsetof(U3CIMPL_sendCaptureU3Ec__Iterator0_t1292966789, ___U3CwidthU3E__0_0)); }
	inline int32_t get_U3CwidthU3E__0_0() const { return ___U3CwidthU3E__0_0; }
	inline int32_t* get_address_of_U3CwidthU3E__0_0() { return &___U3CwidthU3E__0_0; }
	inline void set_U3CwidthU3E__0_0(int32_t value)
	{
		___U3CwidthU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CheightU3E__0_1() { return static_cast<int32_t>(offsetof(U3CIMPL_sendCaptureU3Ec__Iterator0_t1292966789, ___U3CheightU3E__0_1)); }
	inline int32_t get_U3CheightU3E__0_1() const { return ___U3CheightU3E__0_1; }
	inline int32_t* get_address_of_U3CheightU3E__0_1() { return &___U3CheightU3E__0_1; }
	inline void set_U3CheightU3E__0_1(int32_t value)
	{
		___U3CheightU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U3CrendererBackU3E__0_2() { return static_cast<int32_t>(offsetof(U3CIMPL_sendCaptureU3Ec__Iterator0_t1292966789, ___U3CrendererBackU3E__0_2)); }
	inline Renderer_t2627027031 * get_U3CrendererBackU3E__0_2() const { return ___U3CrendererBackU3E__0_2; }
	inline Renderer_t2627027031 ** get_address_of_U3CrendererBackU3E__0_2() { return &___U3CrendererBackU3E__0_2; }
	inline void set_U3CrendererBackU3E__0_2(Renderer_t2627027031 * value)
	{
		___U3CrendererBackU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrendererBackU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U3CtexU3E__0_3() { return static_cast<int32_t>(offsetof(U3CIMPL_sendCaptureU3Ec__Iterator0_t1292966789, ___U3CtexU3E__0_3)); }
	inline Texture2D_t3840446185 * get_U3CtexU3E__0_3() const { return ___U3CtexU3E__0_3; }
	inline Texture2D_t3840446185 ** get_address_of_U3CtexU3E__0_3() { return &___U3CtexU3E__0_3; }
	inline void set_U3CtexU3E__0_3(Texture2D_t3840446185 * value)
	{
		___U3CtexU3E__0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtexU3E__0_3), value);
	}

	inline static int32_t get_offset_of_U3CbytesU3E__0_4() { return static_cast<int32_t>(offsetof(U3CIMPL_sendCaptureU3Ec__Iterator0_t1292966789, ___U3CbytesU3E__0_4)); }
	inline ByteU5BU5D_t4116647657* get_U3CbytesU3E__0_4() const { return ___U3CbytesU3E__0_4; }
	inline ByteU5BU5D_t4116647657** get_address_of_U3CbytesU3E__0_4() { return &___U3CbytesU3E__0_4; }
	inline void set_U3CbytesU3E__0_4(ByteU5BU5D_t4116647657* value)
	{
		___U3CbytesU3E__0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CbytesU3E__0_4), value);
	}

	inline static int32_t get_offset_of_U3CformU3E__0_5() { return static_cast<int32_t>(offsetof(U3CIMPL_sendCaptureU3Ec__Iterator0_t1292966789, ___U3CformU3E__0_5)); }
	inline WWWForm_t4064702195 * get_U3CformU3E__0_5() const { return ___U3CformU3E__0_5; }
	inline WWWForm_t4064702195 ** get_address_of_U3CformU3E__0_5() { return &___U3CformU3E__0_5; }
	inline void set_U3CformU3E__0_5(WWWForm_t4064702195 * value)
	{
		___U3CformU3E__0_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CformU3E__0_5), value);
	}

	inline static int32_t get_offset_of_U3CwU3E__0_6() { return static_cast<int32_t>(offsetof(U3CIMPL_sendCaptureU3Ec__Iterator0_t1292966789, ___U3CwU3E__0_6)); }
	inline UnityWebRequest_t463507806 * get_U3CwU3E__0_6() const { return ___U3CwU3E__0_6; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CwU3E__0_6() { return &___U3CwU3E__0_6; }
	inline void set_U3CwU3E__0_6(UnityWebRequest_t463507806 * value)
	{
		___U3CwU3E__0_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwU3E__0_6), value);
	}

	inline static int32_t get_offset_of_U24this_7() { return static_cast<int32_t>(offsetof(U3CIMPL_sendCaptureU3Ec__Iterator0_t1292966789, ___U24this_7)); }
	inline captureAndSendScreen_t3196082656 * get_U24this_7() const { return ___U24this_7; }
	inline captureAndSendScreen_t3196082656 ** get_address_of_U24this_7() { return &___U24this_7; }
	inline void set_U24this_7(captureAndSendScreen_t3196082656 * value)
	{
		___U24this_7 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_7), value);
	}

	inline static int32_t get_offset_of_U24current_8() { return static_cast<int32_t>(offsetof(U3CIMPL_sendCaptureU3Ec__Iterator0_t1292966789, ___U24current_8)); }
	inline RuntimeObject * get_U24current_8() const { return ___U24current_8; }
	inline RuntimeObject ** get_address_of_U24current_8() { return &___U24current_8; }
	inline void set_U24current_8(RuntimeObject * value)
	{
		___U24current_8 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_8), value);
	}

	inline static int32_t get_offset_of_U24disposing_9() { return static_cast<int32_t>(offsetof(U3CIMPL_sendCaptureU3Ec__Iterator0_t1292966789, ___U24disposing_9)); }
	inline bool get_U24disposing_9() const { return ___U24disposing_9; }
	inline bool* get_address_of_U24disposing_9() { return &___U24disposing_9; }
	inline void set_U24disposing_9(bool value)
	{
		___U24disposing_9 = value;
	}

	inline static int32_t get_offset_of_U24PC_10() { return static_cast<int32_t>(offsetof(U3CIMPL_sendCaptureU3Ec__Iterator0_t1292966789, ___U24PC_10)); }
	inline int32_t get_U24PC_10() const { return ___U24PC_10; }
	inline int32_t* get_address_of_U24PC_10() { return &___U24PC_10; }
	inline void set_U24PC_10(int32_t value)
	{
		___U24PC_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CIMPL_SENDCAPTUREU3EC__ITERATOR0_T1292966789_H
#ifndef U3CGETLANGUAGEJSONU3EC__ITERATOR0_T3550365120_H
#define U3CGETLANGUAGEJSONU3EC__ITERATOR0_T3550365120_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// startUpLANG/<GetLanguageJSON>c__Iterator0
struct  U3CGetLanguageJSONU3Ec__Iterator0_t3550365120  : public RuntimeObject
{
public:
	// System.String startUpLANG/<GetLanguageJSON>c__Iterator0::<url>__0
	String_t* ___U3CurlU3E__0_0;
	// UnityEngine.WWW startUpLANG/<GetLanguageJSON>c__Iterator0::<www>__0
	WWW_t3688466362 * ___U3CwwwU3E__0_1;
	// MarkerManager startUpLANG/<GetLanguageJSON>c__Iterator0::<manager>__0
	MarkerManager_t1031803141 * ___U3CmanagerU3E__0_2;
	// startUpLANG startUpLANG/<GetLanguageJSON>c__Iterator0::$this
	startUpLANG_t1517666504 * ___U24this_3;
	// System.Object startUpLANG/<GetLanguageJSON>c__Iterator0::$current
	RuntimeObject * ___U24current_4;
	// System.Boolean startUpLANG/<GetLanguageJSON>c__Iterator0::$disposing
	bool ___U24disposing_5;
	// System.Int32 startUpLANG/<GetLanguageJSON>c__Iterator0::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_U3CurlU3E__0_0() { return static_cast<int32_t>(offsetof(U3CGetLanguageJSONU3Ec__Iterator0_t3550365120, ___U3CurlU3E__0_0)); }
	inline String_t* get_U3CurlU3E__0_0() const { return ___U3CurlU3E__0_0; }
	inline String_t** get_address_of_U3CurlU3E__0_0() { return &___U3CurlU3E__0_0; }
	inline void set_U3CurlU3E__0_0(String_t* value)
	{
		___U3CurlU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CurlU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E__0_1() { return static_cast<int32_t>(offsetof(U3CGetLanguageJSONU3Ec__Iterator0_t3550365120, ___U3CwwwU3E__0_1)); }
	inline WWW_t3688466362 * get_U3CwwwU3E__0_1() const { return ___U3CwwwU3E__0_1; }
	inline WWW_t3688466362 ** get_address_of_U3CwwwU3E__0_1() { return &___U3CwwwU3E__0_1; }
	inline void set_U3CwwwU3E__0_1(WWW_t3688466362 * value)
	{
		___U3CwwwU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwwwU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U3CmanagerU3E__0_2() { return static_cast<int32_t>(offsetof(U3CGetLanguageJSONU3Ec__Iterator0_t3550365120, ___U3CmanagerU3E__0_2)); }
	inline MarkerManager_t1031803141 * get_U3CmanagerU3E__0_2() const { return ___U3CmanagerU3E__0_2; }
	inline MarkerManager_t1031803141 ** get_address_of_U3CmanagerU3E__0_2() { return &___U3CmanagerU3E__0_2; }
	inline void set_U3CmanagerU3E__0_2(MarkerManager_t1031803141 * value)
	{
		___U3CmanagerU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmanagerU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CGetLanguageJSONU3Ec__Iterator0_t3550365120, ___U24this_3)); }
	inline startUpLANG_t1517666504 * get_U24this_3() const { return ___U24this_3; }
	inline startUpLANG_t1517666504 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(startUpLANG_t1517666504 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CGetLanguageJSONU3Ec__Iterator0_t3550365120, ___U24current_4)); }
	inline RuntimeObject * get_U24current_4() const { return ___U24current_4; }
	inline RuntimeObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(RuntimeObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CGetLanguageJSONU3Ec__Iterator0_t3550365120, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CGetLanguageJSONU3Ec__Iterator0_t3550365120, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETLANGUAGEJSONU3EC__ITERATOR0_T3550365120_H
#ifndef JSONARRAY_T2340361630_H
#define JSONARRAY_T2340361630_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleJSON.JSONArray
struct  JSONArray_t2340361630  : public JSONNode_t2946056997
{
public:
	// System.Collections.Generic.List`1<SimpleJSON.JSONNode> SimpleJSON.JSONArray::m_List
	List_1_t123164443 * ___m_List_1;

public:
	inline static int32_t get_offset_of_m_List_1() { return static_cast<int32_t>(offsetof(JSONArray_t2340361630, ___m_List_1)); }
	inline List_1_t123164443 * get_m_List_1() const { return ___m_List_1; }
	inline List_1_t123164443 ** get_address_of_m_List_1() { return &___m_List_1; }
	inline void set_m_List_1(List_1_t123164443 * value)
	{
		___m_List_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_List_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONARRAY_T2340361630_H
#ifndef JSONBOOL_T130112664_H
#define JSONBOOL_T130112664_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleJSON.JSONBool
struct  JSONBool_t130112664  : public JSONNode_t2946056997
{
public:
	// System.Boolean SimpleJSON.JSONBool::m_Data
	bool ___m_Data_1;

public:
	inline static int32_t get_offset_of_m_Data_1() { return static_cast<int32_t>(offsetof(JSONBool_t130112664, ___m_Data_1)); }
	inline bool get_m_Data_1() const { return ___m_Data_1; }
	inline bool* get_address_of_m_Data_1() { return &___m_Data_1; }
	inline void set_m_Data_1(bool value)
	{
		___m_Data_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONBOOL_T130112664_H
#ifndef JSONLAZYCREATOR_T3621052039_H
#define JSONLAZYCREATOR_T3621052039_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleJSON.JSONLazyCreator
struct  JSONLazyCreator_t3621052039  : public JSONNode_t2946056997
{
public:
	// SimpleJSON.JSONNode SimpleJSON.JSONLazyCreator::m_Node
	JSONNode_t2946056997 * ___m_Node_1;
	// System.String SimpleJSON.JSONLazyCreator::m_Key
	String_t* ___m_Key_2;

public:
	inline static int32_t get_offset_of_m_Node_1() { return static_cast<int32_t>(offsetof(JSONLazyCreator_t3621052039, ___m_Node_1)); }
	inline JSONNode_t2946056997 * get_m_Node_1() const { return ___m_Node_1; }
	inline JSONNode_t2946056997 ** get_address_of_m_Node_1() { return &___m_Node_1; }
	inline void set_m_Node_1(JSONNode_t2946056997 * value)
	{
		___m_Node_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Node_1), value);
	}

	inline static int32_t get_offset_of_m_Key_2() { return static_cast<int32_t>(offsetof(JSONLazyCreator_t3621052039, ___m_Key_2)); }
	inline String_t* get_m_Key_2() const { return ___m_Key_2; }
	inline String_t** get_address_of_m_Key_2() { return &___m_Key_2; }
	inline void set_m_Key_2(String_t* value)
	{
		___m_Key_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Key_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONLAZYCREATOR_T3621052039_H
#ifndef JSONNULL_T1736727710_H
#define JSONNULL_T1736727710_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleJSON.JSONNull
struct  JSONNull_t1736727710  : public JSONNode_t2946056997
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONNULL_T1736727710_H
#ifndef JSONNUMBER_T4005729108_H
#define JSONNUMBER_T4005729108_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleJSON.JSONNumber
struct  JSONNumber_t4005729108  : public JSONNode_t2946056997
{
public:
	// System.Double SimpleJSON.JSONNumber::m_Data
	double ___m_Data_1;

public:
	inline static int32_t get_offset_of_m_Data_1() { return static_cast<int32_t>(offsetof(JSONNumber_t4005729108, ___m_Data_1)); }
	inline double get_m_Data_1() const { return ___m_Data_1; }
	inline double* get_address_of_m_Data_1() { return &___m_Data_1; }
	inline void set_m_Data_1(double value)
	{
		___m_Data_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONNUMBER_T4005729108_H
#ifndef JSONOBJECT_T4158403488_H
#define JSONOBJECT_T4158403488_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleJSON.JSONObject
struct  JSONObject_t4158403488  : public JSONNode_t2946056997
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,SimpleJSON.JSONNode> SimpleJSON.JSONObject::m_Dict
	Dictionary_2_t2731313296 * ___m_Dict_1;
	// System.String SimpleJSON.JSONObject::jsonKey
	String_t* ___jsonKey_2;

public:
	inline static int32_t get_offset_of_m_Dict_1() { return static_cast<int32_t>(offsetof(JSONObject_t4158403488, ___m_Dict_1)); }
	inline Dictionary_2_t2731313296 * get_m_Dict_1() const { return ___m_Dict_1; }
	inline Dictionary_2_t2731313296 ** get_address_of_m_Dict_1() { return &___m_Dict_1; }
	inline void set_m_Dict_1(Dictionary_2_t2731313296 * value)
	{
		___m_Dict_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Dict_1), value);
	}

	inline static int32_t get_offset_of_jsonKey_2() { return static_cast<int32_t>(offsetof(JSONObject_t4158403488, ___jsonKey_2)); }
	inline String_t* get_jsonKey_2() const { return ___jsonKey_2; }
	inline String_t** get_address_of_jsonKey_2() { return &___jsonKey_2; }
	inline void set_jsonKey_2(String_t* value)
	{
		___jsonKey_2 = value;
		Il2CppCodeGenWriteBarrier((&___jsonKey_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONOBJECT_T4158403488_H
#ifndef JSONSTRING_T3803360443_H
#define JSONSTRING_T3803360443_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleJSON.JSONString
struct  JSONString_t3803360443  : public JSONNode_t2946056997
{
public:
	// System.String SimpleJSON.JSONString::m_Data
	String_t* ___m_Data_1;

public:
	inline static int32_t get_offset_of_m_Data_1() { return static_cast<int32_t>(offsetof(JSONString_t3803360443, ___m_Data_1)); }
	inline String_t* get_m_Data_1() const { return ___m_Data_1; }
	inline String_t** get_address_of_m_Data_1() { return &___m_Data_1; }
	inline void set_m_Data_1(String_t* value)
	{
		___m_Data_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Data_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONSTRING_T3803360443_H
#ifndef KEYVALUEPAIR_2_T834018167_H
#define KEYVALUEPAIR_2_T834018167_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.String,SimpleJSON.JSONNode>
struct  KeyValuePair_2_t834018167 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	String_t* ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	JSONNode_t2946056997 * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t834018167, ___key_0)); }
	inline String_t* get_key_0() const { return ___key_0; }
	inline String_t** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(String_t* value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t834018167, ___value_1)); }
	inline JSONNode_t2946056997 * get_value_1() const { return ___value_1; }
	inline JSONNode_t2946056997 ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(JSONNode_t2946056997 * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T834018167_H
#ifndef ENUMERATOR_T2012408320_H
#define ENUMERATOR_T2012408320_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<SimpleJSON.JSONNode>
struct  Enumerator_t2012408320 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::l
	List_1_t123164443 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	JSONNode_t2946056997 * ___current_3;

public:
	inline static int32_t get_offset_of_l_0() { return static_cast<int32_t>(offsetof(Enumerator_t2012408320, ___l_0)); }
	inline List_1_t123164443 * get_l_0() const { return ___l_0; }
	inline List_1_t123164443 ** get_address_of_l_0() { return &___l_0; }
	inline void set_l_0(List_1_t123164443 * value)
	{
		___l_0 = value;
		Il2CppCodeGenWriteBarrier((&___l_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t2012408320, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_ver_2() { return static_cast<int32_t>(offsetof(Enumerator_t2012408320, ___ver_2)); }
	inline int32_t get_ver_2() const { return ___ver_2; }
	inline int32_t* get_address_of_ver_2() { return &___ver_2; }
	inline void set_ver_2(int32_t value)
	{
		___ver_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t2012408320, ___current_3)); }
	inline JSONNode_t2946056997 * get_current_3() const { return ___current_3; }
	inline JSONNode_t2946056997 ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(JSONNode_t2946056997 * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((&___current_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T2012408320_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef UNITYEVENT_T2581268647_H
#define UNITYEVENT_T2581268647_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent
struct  UnityEvent_t2581268647  : public UnityEventBase_t3960448221
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent::m_InvokeArray
	ObjectU5BU5D_t2843939325* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_t2581268647, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t2843939325* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t2843939325** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t2843939325* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_T2581268647_H
#ifndef RECT_T2360479859_H
#define RECT_T2360479859_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rect
struct  Rect_t2360479859 
{
public:
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_XMin_0)); }
	inline float get_m_XMin_0() const { return ___m_XMin_0; }
	inline float* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(float value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_YMin_1)); }
	inline float get_m_YMin_1() const { return ___m_YMin_1; }
	inline float* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(float value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_Width_2)); }
	inline float get_m_Width_2() const { return ___m_Width_2; }
	inline float* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(float value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_Height_3)); }
	inline float get_m_Height_3() const { return ___m_Height_3; }
	inline float* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(float value)
	{
		___m_Height_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECT_T2360479859_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_5)); }
	inline Vector3_t3722313464  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_t3722313464  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_6)); }
	inline Vector3_t3722313464  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_t3722313464 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_t3722313464  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_7)); }
	inline Vector3_t3722313464  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_t3722313464 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_t3722313464  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_8)); }
	inline Vector3_t3722313464  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_t3722313464 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_t3722313464  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_9)); }
	inline Vector3_t3722313464  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_t3722313464 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_t3722313464  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_10)); }
	inline Vector3_t3722313464  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_t3722313464 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_t3722313464  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_11)); }
	inline Vector3_t3722313464  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_t3722313464  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_12)); }
	inline Vector3_t3722313464  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_t3722313464 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_t3722313464  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef LAUNCHACTIONEVENT_T1220164713_H
#define LAUNCHACTIONEVENT_T1220164713_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ActionLauncher/LaunchActionEvent
struct  LaunchActionEvent_t1220164713  : public UnityEvent_t2581268647
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAUNCHACTIONEVENT_T1220164713_H
#ifndef AXIS_T1613265627_H
#define AXIS_T1613265627_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraFacingBillboard/Axis
struct  Axis_t1613265627 
{
public:
	// System.Int32 CameraFacingBillboard/Axis::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Axis_t1613265627, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXIS_T1613265627_H
#ifndef U3CGETCREDITU3EC__ITERATOR0_T2852871274_H
#define U3CGETCREDITU3EC__ITERATOR0_T2852871274_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CreditLoader/<getCredit>c__Iterator0
struct  U3CgetCreditU3Ec__Iterator0_t2852871274  : public RuntimeObject
{
public:
	// System.String CreditLoader/<getCredit>c__Iterator0::<url>__0
	String_t* ___U3CurlU3E__0_0;
	// UnityEngine.WWW CreditLoader/<getCredit>c__Iterator0::<www>__0
	WWW_t3688466362 * ___U3CwwwU3E__0_1;
	// SimpleJSON.JSONNode CreditLoader/<getCredit>c__Iterator0::<Nbase>__0
	JSONNode_t2946056997 * ___U3CNbaseU3E__0_2;
	// System.String CreditLoader/<getCredit>c__Iterator0::<corpsText>__0
	String_t* ___U3CcorpsTextU3E__0_3;
	// UnityEngine.WWW CreditLoader/<getCredit>c__Iterator0::<wwwimage>__0
	WWW_t3688466362 * ___U3CwwwimageU3E__0_4;
	// UnityEngine.Texture2D CreditLoader/<getCredit>c__Iterator0::<texture>__0
	Texture2D_t3840446185 * ___U3CtextureU3E__0_5;
	// UnityEngine.Rect CreditLoader/<getCredit>c__Iterator0::<rec>__0
	Rect_t2360479859  ___U3CrecU3E__0_6;
	// UnityEngine.Sprite CreditLoader/<getCredit>c__Iterator0::<spriteToUse>__0
	Sprite_t280657092 * ___U3CspriteToUseU3E__0_7;
	// UnityEngine.Color CreditLoader/<getCredit>c__Iterator0::<newCol>__0
	Color_t2555686324  ___U3CnewColU3E__0_8;
	// CreditLoader CreditLoader/<getCredit>c__Iterator0::$this
	CreditLoader_t1693052478 * ___U24this_9;
	// System.Object CreditLoader/<getCredit>c__Iterator0::$current
	RuntimeObject * ___U24current_10;
	// System.Boolean CreditLoader/<getCredit>c__Iterator0::$disposing
	bool ___U24disposing_11;
	// System.Int32 CreditLoader/<getCredit>c__Iterator0::$PC
	int32_t ___U24PC_12;

public:
	inline static int32_t get_offset_of_U3CurlU3E__0_0() { return static_cast<int32_t>(offsetof(U3CgetCreditU3Ec__Iterator0_t2852871274, ___U3CurlU3E__0_0)); }
	inline String_t* get_U3CurlU3E__0_0() const { return ___U3CurlU3E__0_0; }
	inline String_t** get_address_of_U3CurlU3E__0_0() { return &___U3CurlU3E__0_0; }
	inline void set_U3CurlU3E__0_0(String_t* value)
	{
		___U3CurlU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CurlU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E__0_1() { return static_cast<int32_t>(offsetof(U3CgetCreditU3Ec__Iterator0_t2852871274, ___U3CwwwU3E__0_1)); }
	inline WWW_t3688466362 * get_U3CwwwU3E__0_1() const { return ___U3CwwwU3E__0_1; }
	inline WWW_t3688466362 ** get_address_of_U3CwwwU3E__0_1() { return &___U3CwwwU3E__0_1; }
	inline void set_U3CwwwU3E__0_1(WWW_t3688466362 * value)
	{
		___U3CwwwU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwwwU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U3CNbaseU3E__0_2() { return static_cast<int32_t>(offsetof(U3CgetCreditU3Ec__Iterator0_t2852871274, ___U3CNbaseU3E__0_2)); }
	inline JSONNode_t2946056997 * get_U3CNbaseU3E__0_2() const { return ___U3CNbaseU3E__0_2; }
	inline JSONNode_t2946056997 ** get_address_of_U3CNbaseU3E__0_2() { return &___U3CNbaseU3E__0_2; }
	inline void set_U3CNbaseU3E__0_2(JSONNode_t2946056997 * value)
	{
		___U3CNbaseU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNbaseU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U3CcorpsTextU3E__0_3() { return static_cast<int32_t>(offsetof(U3CgetCreditU3Ec__Iterator0_t2852871274, ___U3CcorpsTextU3E__0_3)); }
	inline String_t* get_U3CcorpsTextU3E__0_3() const { return ___U3CcorpsTextU3E__0_3; }
	inline String_t** get_address_of_U3CcorpsTextU3E__0_3() { return &___U3CcorpsTextU3E__0_3; }
	inline void set_U3CcorpsTextU3E__0_3(String_t* value)
	{
		___U3CcorpsTextU3E__0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcorpsTextU3E__0_3), value);
	}

	inline static int32_t get_offset_of_U3CwwwimageU3E__0_4() { return static_cast<int32_t>(offsetof(U3CgetCreditU3Ec__Iterator0_t2852871274, ___U3CwwwimageU3E__0_4)); }
	inline WWW_t3688466362 * get_U3CwwwimageU3E__0_4() const { return ___U3CwwwimageU3E__0_4; }
	inline WWW_t3688466362 ** get_address_of_U3CwwwimageU3E__0_4() { return &___U3CwwwimageU3E__0_4; }
	inline void set_U3CwwwimageU3E__0_4(WWW_t3688466362 * value)
	{
		___U3CwwwimageU3E__0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwwwimageU3E__0_4), value);
	}

	inline static int32_t get_offset_of_U3CtextureU3E__0_5() { return static_cast<int32_t>(offsetof(U3CgetCreditU3Ec__Iterator0_t2852871274, ___U3CtextureU3E__0_5)); }
	inline Texture2D_t3840446185 * get_U3CtextureU3E__0_5() const { return ___U3CtextureU3E__0_5; }
	inline Texture2D_t3840446185 ** get_address_of_U3CtextureU3E__0_5() { return &___U3CtextureU3E__0_5; }
	inline void set_U3CtextureU3E__0_5(Texture2D_t3840446185 * value)
	{
		___U3CtextureU3E__0_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtextureU3E__0_5), value);
	}

	inline static int32_t get_offset_of_U3CrecU3E__0_6() { return static_cast<int32_t>(offsetof(U3CgetCreditU3Ec__Iterator0_t2852871274, ___U3CrecU3E__0_6)); }
	inline Rect_t2360479859  get_U3CrecU3E__0_6() const { return ___U3CrecU3E__0_6; }
	inline Rect_t2360479859 * get_address_of_U3CrecU3E__0_6() { return &___U3CrecU3E__0_6; }
	inline void set_U3CrecU3E__0_6(Rect_t2360479859  value)
	{
		___U3CrecU3E__0_6 = value;
	}

	inline static int32_t get_offset_of_U3CspriteToUseU3E__0_7() { return static_cast<int32_t>(offsetof(U3CgetCreditU3Ec__Iterator0_t2852871274, ___U3CspriteToUseU3E__0_7)); }
	inline Sprite_t280657092 * get_U3CspriteToUseU3E__0_7() const { return ___U3CspriteToUseU3E__0_7; }
	inline Sprite_t280657092 ** get_address_of_U3CspriteToUseU3E__0_7() { return &___U3CspriteToUseU3E__0_7; }
	inline void set_U3CspriteToUseU3E__0_7(Sprite_t280657092 * value)
	{
		___U3CspriteToUseU3E__0_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CspriteToUseU3E__0_7), value);
	}

	inline static int32_t get_offset_of_U3CnewColU3E__0_8() { return static_cast<int32_t>(offsetof(U3CgetCreditU3Ec__Iterator0_t2852871274, ___U3CnewColU3E__0_8)); }
	inline Color_t2555686324  get_U3CnewColU3E__0_8() const { return ___U3CnewColU3E__0_8; }
	inline Color_t2555686324 * get_address_of_U3CnewColU3E__0_8() { return &___U3CnewColU3E__0_8; }
	inline void set_U3CnewColU3E__0_8(Color_t2555686324  value)
	{
		___U3CnewColU3E__0_8 = value;
	}

	inline static int32_t get_offset_of_U24this_9() { return static_cast<int32_t>(offsetof(U3CgetCreditU3Ec__Iterator0_t2852871274, ___U24this_9)); }
	inline CreditLoader_t1693052478 * get_U24this_9() const { return ___U24this_9; }
	inline CreditLoader_t1693052478 ** get_address_of_U24this_9() { return &___U24this_9; }
	inline void set_U24this_9(CreditLoader_t1693052478 * value)
	{
		___U24this_9 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_9), value);
	}

	inline static int32_t get_offset_of_U24current_10() { return static_cast<int32_t>(offsetof(U3CgetCreditU3Ec__Iterator0_t2852871274, ___U24current_10)); }
	inline RuntimeObject * get_U24current_10() const { return ___U24current_10; }
	inline RuntimeObject ** get_address_of_U24current_10() { return &___U24current_10; }
	inline void set_U24current_10(RuntimeObject * value)
	{
		___U24current_10 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_10), value);
	}

	inline static int32_t get_offset_of_U24disposing_11() { return static_cast<int32_t>(offsetof(U3CgetCreditU3Ec__Iterator0_t2852871274, ___U24disposing_11)); }
	inline bool get_U24disposing_11() const { return ___U24disposing_11; }
	inline bool* get_address_of_U24disposing_11() { return &___U24disposing_11; }
	inline void set_U24disposing_11(bool value)
	{
		___U24disposing_11 = value;
	}

	inline static int32_t get_offset_of_U24PC_12() { return static_cast<int32_t>(offsetof(U3CgetCreditU3Ec__Iterator0_t2852871274, ___U24PC_12)); }
	inline int32_t get_U24PC_12() const { return ___U24PC_12; }
	inline int32_t* get_address_of_U24PC_12() { return &___U24PC_12; }
	inline void set_U24PC_12(int32_t value)
	{
		___U24PC_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETCREDITU3EC__ITERATOR0_T2852871274_H
#ifndef LAUNCHACTIONEVENT_T193601726_H
#define LAUNCHACTIONEVENT_T193601726_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DynamicObject/LaunchActionEvent
struct  LaunchActionEvent_t193601726  : public UnityEvent_t2581268647
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAUNCHACTIONEVENT_T193601726_H
#ifndef TRANSITION_T850483325_H
#define TRANSITION_T850483325_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DynamicObject/Transition
struct  Transition_t850483325 
{
public:
	// System.Int32 DynamicObject/Transition::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Transition_t850483325, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSITION_T850483325_H
#ifndef MENUBURGERSTATUS_T3073057943_H
#define MENUBURGERSTATUS_T3073057943_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MenuBurger/MenuBurgerStatus
struct  MenuBurgerStatus_t3073057943 
{
public:
	// System.Int32 MenuBurger/MenuBurgerStatus::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(MenuBurgerStatus_t3073057943, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MENUBURGERSTATUS_T3073057943_H
#ifndef STATUS_T3896441745_H
#define STATUS_T3896441745_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Page/Status
struct  Status_t3896441745 
{
public:
	// System.Int32 Page/Status::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Status_t3896441745, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATUS_T3896441745_H
#ifndef U3CU3EC__ITERATOR0_T2066799033_H
#define U3CU3EC__ITERATOR0_T2066799033_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleJSON.JSONArray/<>c__Iterator0
struct  U3CU3Ec__Iterator0_t2066799033  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1/Enumerator<SimpleJSON.JSONNode> SimpleJSON.JSONArray/<>c__Iterator0::$locvar0
	Enumerator_t2012408320  ___U24locvar0_0;
	// SimpleJSON.JSONNode SimpleJSON.JSONArray/<>c__Iterator0::<N>__1
	JSONNode_t2946056997 * ___U3CNU3E__1_1;
	// SimpleJSON.JSONArray SimpleJSON.JSONArray/<>c__Iterator0::$this
	JSONArray_t2340361630 * ___U24this_2;
	// SimpleJSON.JSONNode SimpleJSON.JSONArray/<>c__Iterator0::$current
	JSONNode_t2946056997 * ___U24current_3;
	// System.Boolean SimpleJSON.JSONArray/<>c__Iterator0::$disposing
	bool ___U24disposing_4;
	// System.Int32 SimpleJSON.JSONArray/<>c__Iterator0::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U24locvar0_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t2066799033, ___U24locvar0_0)); }
	inline Enumerator_t2012408320  get_U24locvar0_0() const { return ___U24locvar0_0; }
	inline Enumerator_t2012408320 * get_address_of_U24locvar0_0() { return &___U24locvar0_0; }
	inline void set_U24locvar0_0(Enumerator_t2012408320  value)
	{
		___U24locvar0_0 = value;
	}

	inline static int32_t get_offset_of_U3CNU3E__1_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t2066799033, ___U3CNU3E__1_1)); }
	inline JSONNode_t2946056997 * get_U3CNU3E__1_1() const { return ___U3CNU3E__1_1; }
	inline JSONNode_t2946056997 ** get_address_of_U3CNU3E__1_1() { return &___U3CNU3E__1_1; }
	inline void set_U3CNU3E__1_1(JSONNode_t2946056997 * value)
	{
		___U3CNU3E__1_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNU3E__1_1), value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t2066799033, ___U24this_2)); }
	inline JSONArray_t2340361630 * get_U24this_2() const { return ___U24this_2; }
	inline JSONArray_t2340361630 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(JSONArray_t2340361630 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t2066799033, ___U24current_3)); }
	inline JSONNode_t2946056997 * get_U24current_3() const { return ___U24current_3; }
	inline JSONNode_t2946056997 ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(JSONNode_t2946056997 * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t2066799033, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t2066799033, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__ITERATOR0_T2066799033_H
#ifndef U3CGETENUMERATORU3EC__ITERATOR1_T1509986145_H
#define U3CGETENUMERATORU3EC__ITERATOR1_T1509986145_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleJSON.JSONArray/<GetEnumerator>c__Iterator1
struct  U3CGetEnumeratorU3Ec__Iterator1_t1509986145  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1/Enumerator<SimpleJSON.JSONNode> SimpleJSON.JSONArray/<GetEnumerator>c__Iterator1::$locvar0
	Enumerator_t2012408320  ___U24locvar0_0;
	// SimpleJSON.JSONNode SimpleJSON.JSONArray/<GetEnumerator>c__Iterator1::<N>__1
	JSONNode_t2946056997 * ___U3CNU3E__1_1;
	// SimpleJSON.JSONArray SimpleJSON.JSONArray/<GetEnumerator>c__Iterator1::$this
	JSONArray_t2340361630 * ___U24this_2;
	// System.Object SimpleJSON.JSONArray/<GetEnumerator>c__Iterator1::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean SimpleJSON.JSONArray/<GetEnumerator>c__Iterator1::$disposing
	bool ___U24disposing_4;
	// System.Int32 SimpleJSON.JSONArray/<GetEnumerator>c__Iterator1::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U24locvar0_0() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator1_t1509986145, ___U24locvar0_0)); }
	inline Enumerator_t2012408320  get_U24locvar0_0() const { return ___U24locvar0_0; }
	inline Enumerator_t2012408320 * get_address_of_U24locvar0_0() { return &___U24locvar0_0; }
	inline void set_U24locvar0_0(Enumerator_t2012408320  value)
	{
		___U24locvar0_0 = value;
	}

	inline static int32_t get_offset_of_U3CNU3E__1_1() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator1_t1509986145, ___U3CNU3E__1_1)); }
	inline JSONNode_t2946056997 * get_U3CNU3E__1_1() const { return ___U3CNU3E__1_1; }
	inline JSONNode_t2946056997 ** get_address_of_U3CNU3E__1_1() { return &___U3CNU3E__1_1; }
	inline void set_U3CNU3E__1_1(JSONNode_t2946056997 * value)
	{
		___U3CNU3E__1_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNU3E__1_1), value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator1_t1509986145, ___U24this_2)); }
	inline JSONArray_t2340361630 * get_U24this_2() const { return ___U24this_2; }
	inline JSONArray_t2340361630 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(JSONArray_t2340361630 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator1_t1509986145, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator1_t1509986145, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator1_t1509986145, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETENUMERATORU3EC__ITERATOR1_T1509986145_H
#ifndef JSONNODETYPE_T2191432521_H
#define JSONNODETYPE_T2191432521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleJSON.JSONNodeType
struct  JSONNodeType_t2191432521 
{
public:
	// System.Int32 SimpleJSON.JSONNodeType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(JSONNodeType_t2191432521, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONNODETYPE_T2191432521_H
#ifndef U3CLOADCREDITIMAGEU3EC__ITERATOR0_T278559497_H
#define U3CLOADCREDITIMAGEU3EC__ITERATOR0_T278559497_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SplashTimer/<loadCreditImage>c__Iterator0
struct  U3CloadCreditImageU3Ec__Iterator0_t278559497  : public RuntimeObject
{
public:
	// System.String SplashTimer/<loadCreditImage>c__Iterator0::<url>__0
	String_t* ___U3CurlU3E__0_0;
	// UnityEngine.WWW SplashTimer/<loadCreditImage>c__Iterator0::<www>__0
	WWW_t3688466362 * ___U3CwwwU3E__0_1;
	// SimpleJSON.JSONNode SplashTimer/<loadCreditImage>c__Iterator0::<Nbase>__0
	JSONNode_t2946056997 * ___U3CNbaseU3E__0_2;
	// UnityEngine.WWW SplashTimer/<loadCreditImage>c__Iterator0::<wwwimage>__0
	WWW_t3688466362 * ___U3CwwwimageU3E__0_3;
	// UnityEngine.Texture2D SplashTimer/<loadCreditImage>c__Iterator0::<texture>__0
	Texture2D_t3840446185 * ___U3CtextureU3E__0_4;
	// UnityEngine.Rect SplashTimer/<loadCreditImage>c__Iterator0::<rec>__0
	Rect_t2360479859  ___U3CrecU3E__0_5;
	// UnityEngine.Sprite SplashTimer/<loadCreditImage>c__Iterator0::<spriteToUse>__0
	Sprite_t280657092 * ___U3CspriteToUseU3E__0_6;
	// SplashTimer SplashTimer/<loadCreditImage>c__Iterator0::$this
	SplashTimer_t2585461764 * ___U24this_7;
	// System.Object SplashTimer/<loadCreditImage>c__Iterator0::$current
	RuntimeObject * ___U24current_8;
	// System.Boolean SplashTimer/<loadCreditImage>c__Iterator0::$disposing
	bool ___U24disposing_9;
	// System.Int32 SplashTimer/<loadCreditImage>c__Iterator0::$PC
	int32_t ___U24PC_10;

public:
	inline static int32_t get_offset_of_U3CurlU3E__0_0() { return static_cast<int32_t>(offsetof(U3CloadCreditImageU3Ec__Iterator0_t278559497, ___U3CurlU3E__0_0)); }
	inline String_t* get_U3CurlU3E__0_0() const { return ___U3CurlU3E__0_0; }
	inline String_t** get_address_of_U3CurlU3E__0_0() { return &___U3CurlU3E__0_0; }
	inline void set_U3CurlU3E__0_0(String_t* value)
	{
		___U3CurlU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CurlU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E__0_1() { return static_cast<int32_t>(offsetof(U3CloadCreditImageU3Ec__Iterator0_t278559497, ___U3CwwwU3E__0_1)); }
	inline WWW_t3688466362 * get_U3CwwwU3E__0_1() const { return ___U3CwwwU3E__0_1; }
	inline WWW_t3688466362 ** get_address_of_U3CwwwU3E__0_1() { return &___U3CwwwU3E__0_1; }
	inline void set_U3CwwwU3E__0_1(WWW_t3688466362 * value)
	{
		___U3CwwwU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwwwU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U3CNbaseU3E__0_2() { return static_cast<int32_t>(offsetof(U3CloadCreditImageU3Ec__Iterator0_t278559497, ___U3CNbaseU3E__0_2)); }
	inline JSONNode_t2946056997 * get_U3CNbaseU3E__0_2() const { return ___U3CNbaseU3E__0_2; }
	inline JSONNode_t2946056997 ** get_address_of_U3CNbaseU3E__0_2() { return &___U3CNbaseU3E__0_2; }
	inline void set_U3CNbaseU3E__0_2(JSONNode_t2946056997 * value)
	{
		___U3CNbaseU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNbaseU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U3CwwwimageU3E__0_3() { return static_cast<int32_t>(offsetof(U3CloadCreditImageU3Ec__Iterator0_t278559497, ___U3CwwwimageU3E__0_3)); }
	inline WWW_t3688466362 * get_U3CwwwimageU3E__0_3() const { return ___U3CwwwimageU3E__0_3; }
	inline WWW_t3688466362 ** get_address_of_U3CwwwimageU3E__0_3() { return &___U3CwwwimageU3E__0_3; }
	inline void set_U3CwwwimageU3E__0_3(WWW_t3688466362 * value)
	{
		___U3CwwwimageU3E__0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwwwimageU3E__0_3), value);
	}

	inline static int32_t get_offset_of_U3CtextureU3E__0_4() { return static_cast<int32_t>(offsetof(U3CloadCreditImageU3Ec__Iterator0_t278559497, ___U3CtextureU3E__0_4)); }
	inline Texture2D_t3840446185 * get_U3CtextureU3E__0_4() const { return ___U3CtextureU3E__0_4; }
	inline Texture2D_t3840446185 ** get_address_of_U3CtextureU3E__0_4() { return &___U3CtextureU3E__0_4; }
	inline void set_U3CtextureU3E__0_4(Texture2D_t3840446185 * value)
	{
		___U3CtextureU3E__0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtextureU3E__0_4), value);
	}

	inline static int32_t get_offset_of_U3CrecU3E__0_5() { return static_cast<int32_t>(offsetof(U3CloadCreditImageU3Ec__Iterator0_t278559497, ___U3CrecU3E__0_5)); }
	inline Rect_t2360479859  get_U3CrecU3E__0_5() const { return ___U3CrecU3E__0_5; }
	inline Rect_t2360479859 * get_address_of_U3CrecU3E__0_5() { return &___U3CrecU3E__0_5; }
	inline void set_U3CrecU3E__0_5(Rect_t2360479859  value)
	{
		___U3CrecU3E__0_5 = value;
	}

	inline static int32_t get_offset_of_U3CspriteToUseU3E__0_6() { return static_cast<int32_t>(offsetof(U3CloadCreditImageU3Ec__Iterator0_t278559497, ___U3CspriteToUseU3E__0_6)); }
	inline Sprite_t280657092 * get_U3CspriteToUseU3E__0_6() const { return ___U3CspriteToUseU3E__0_6; }
	inline Sprite_t280657092 ** get_address_of_U3CspriteToUseU3E__0_6() { return &___U3CspriteToUseU3E__0_6; }
	inline void set_U3CspriteToUseU3E__0_6(Sprite_t280657092 * value)
	{
		___U3CspriteToUseU3E__0_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CspriteToUseU3E__0_6), value);
	}

	inline static int32_t get_offset_of_U24this_7() { return static_cast<int32_t>(offsetof(U3CloadCreditImageU3Ec__Iterator0_t278559497, ___U24this_7)); }
	inline SplashTimer_t2585461764 * get_U24this_7() const { return ___U24this_7; }
	inline SplashTimer_t2585461764 ** get_address_of_U24this_7() { return &___U24this_7; }
	inline void set_U24this_7(SplashTimer_t2585461764 * value)
	{
		___U24this_7 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_7), value);
	}

	inline static int32_t get_offset_of_U24current_8() { return static_cast<int32_t>(offsetof(U3CloadCreditImageU3Ec__Iterator0_t278559497, ___U24current_8)); }
	inline RuntimeObject * get_U24current_8() const { return ___U24current_8; }
	inline RuntimeObject ** get_address_of_U24current_8() { return &___U24current_8; }
	inline void set_U24current_8(RuntimeObject * value)
	{
		___U24current_8 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_8), value);
	}

	inline static int32_t get_offset_of_U24disposing_9() { return static_cast<int32_t>(offsetof(U3CloadCreditImageU3Ec__Iterator0_t278559497, ___U24disposing_9)); }
	inline bool get_U24disposing_9() const { return ___U24disposing_9; }
	inline bool* get_address_of_U24disposing_9() { return &___U24disposing_9; }
	inline void set_U24disposing_9(bool value)
	{
		___U24disposing_9 = value;
	}

	inline static int32_t get_offset_of_U24PC_10() { return static_cast<int32_t>(offsetof(U3CloadCreditImageU3Ec__Iterator0_t278559497, ___U24PC_10)); }
	inline int32_t get_U24PC_10() const { return ___U24PC_10; }
	inline int32_t* get_address_of_U24PC_10() { return &___U24PC_10; }
	inline void set_U24PC_10(int32_t value)
	{
		___U24PC_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADCREDITIMAGEU3EC__ITERATOR0_T278559497_H
#ifndef ENUMERATOR_T390528775_H
#define ENUMERATOR_T390528775_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2/Enumerator<System.String,SimpleJSON.JSONNode>
struct  Enumerator_t390528775 
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator::dictionary
	Dictionary_2_t2731313296 * ___dictionary_0;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator::stamp
	int32_t ___stamp_2;
	// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator::current
	KeyValuePair_2_t834018167  ___current_3;

public:
	inline static int32_t get_offset_of_dictionary_0() { return static_cast<int32_t>(offsetof(Enumerator_t390528775, ___dictionary_0)); }
	inline Dictionary_2_t2731313296 * get_dictionary_0() const { return ___dictionary_0; }
	inline Dictionary_2_t2731313296 ** get_address_of_dictionary_0() { return &___dictionary_0; }
	inline void set_dictionary_0(Dictionary_2_t2731313296 * value)
	{
		___dictionary_0 = value;
		Il2CppCodeGenWriteBarrier((&___dictionary_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t390528775, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_stamp_2() { return static_cast<int32_t>(offsetof(Enumerator_t390528775, ___stamp_2)); }
	inline int32_t get_stamp_2() const { return ___stamp_2; }
	inline int32_t* get_address_of_stamp_2() { return &___stamp_2; }
	inline void set_stamp_2(int32_t value)
	{
		___stamp_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t390528775, ___current_3)); }
	inline KeyValuePair_2_t834018167  get_current_3() const { return ___current_3; }
	inline KeyValuePair_2_t834018167 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(KeyValuePair_2_t834018167  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T390528775_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_8)); }
	inline DelegateData_t1677132599 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1677132599 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1677132599 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T1188392813_H
#ifndef BOUNDS_T2266837910_H
#define BOUNDS_T2266837910_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Bounds
struct  Bounds_t2266837910 
{
public:
	// UnityEngine.Vector3 UnityEngine.Bounds::m_Center
	Vector3_t3722313464  ___m_Center_0;
	// UnityEngine.Vector3 UnityEngine.Bounds::m_Extents
	Vector3_t3722313464  ___m_Extents_1;

public:
	inline static int32_t get_offset_of_m_Center_0() { return static_cast<int32_t>(offsetof(Bounds_t2266837910, ___m_Center_0)); }
	inline Vector3_t3722313464  get_m_Center_0() const { return ___m_Center_0; }
	inline Vector3_t3722313464 * get_address_of_m_Center_0() { return &___m_Center_0; }
	inline void set_m_Center_0(Vector3_t3722313464  value)
	{
		___m_Center_0 = value;
	}

	inline static int32_t get_offset_of_m_Extents_1() { return static_cast<int32_t>(offsetof(Bounds_t2266837910, ___m_Extents_1)); }
	inline Vector3_t3722313464  get_m_Extents_1() const { return ___m_Extents_1; }
	inline Vector3_t3722313464 * get_address_of_m_Extents_1() { return &___m_Extents_1; }
	inline void set_m_Extents_1(Vector3_t3722313464  value)
	{
		___m_Extents_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOUNDS_T2266837910_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef RAY_T3785851493_H
#define RAY_T3785851493_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Ray
struct  Ray_t3785851493 
{
public:
	// UnityEngine.Vector3 UnityEngine.Ray::m_Origin
	Vector3_t3722313464  ___m_Origin_0;
	// UnityEngine.Vector3 UnityEngine.Ray::m_Direction
	Vector3_t3722313464  ___m_Direction_1;

public:
	inline static int32_t get_offset_of_m_Origin_0() { return static_cast<int32_t>(offsetof(Ray_t3785851493, ___m_Origin_0)); }
	inline Vector3_t3722313464  get_m_Origin_0() const { return ___m_Origin_0; }
	inline Vector3_t3722313464 * get_address_of_m_Origin_0() { return &___m_Origin_0; }
	inline void set_m_Origin_0(Vector3_t3722313464  value)
	{
		___m_Origin_0 = value;
	}

	inline static int32_t get_offset_of_m_Direction_1() { return static_cast<int32_t>(offsetof(Ray_t3785851493, ___m_Direction_1)); }
	inline Vector3_t3722313464  get_m_Direction_1() const { return ___m_Direction_1; }
	inline Vector3_t3722313464 * get_address_of_m_Direction_1() { return &___m_Direction_1; }
	inline void set_m_Direction_1(Vector3_t3722313464  value)
	{
		___m_Direction_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAY_T3785851493_H
#ifndef RAYCASTHIT_T1056001966_H
#define RAYCASTHIT_T1056001966_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RaycastHit
struct  RaycastHit_t1056001966 
{
public:
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Point
	Vector3_t3722313464  ___m_Point_0;
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Normal
	Vector3_t3722313464  ___m_Normal_1;
	// System.UInt32 UnityEngine.RaycastHit::m_FaceID
	uint32_t ___m_FaceID_2;
	// System.Single UnityEngine.RaycastHit::m_Distance
	float ___m_Distance_3;
	// UnityEngine.Vector2 UnityEngine.RaycastHit::m_UV
	Vector2_t2156229523  ___m_UV_4;
	// System.Int32 UnityEngine.RaycastHit::m_Collider
	int32_t ___m_Collider_5;

public:
	inline static int32_t get_offset_of_m_Point_0() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_Point_0)); }
	inline Vector3_t3722313464  get_m_Point_0() const { return ___m_Point_0; }
	inline Vector3_t3722313464 * get_address_of_m_Point_0() { return &___m_Point_0; }
	inline void set_m_Point_0(Vector3_t3722313464  value)
	{
		___m_Point_0 = value;
	}

	inline static int32_t get_offset_of_m_Normal_1() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_Normal_1)); }
	inline Vector3_t3722313464  get_m_Normal_1() const { return ___m_Normal_1; }
	inline Vector3_t3722313464 * get_address_of_m_Normal_1() { return &___m_Normal_1; }
	inline void set_m_Normal_1(Vector3_t3722313464  value)
	{
		___m_Normal_1 = value;
	}

	inline static int32_t get_offset_of_m_FaceID_2() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_FaceID_2)); }
	inline uint32_t get_m_FaceID_2() const { return ___m_FaceID_2; }
	inline uint32_t* get_address_of_m_FaceID_2() { return &___m_FaceID_2; }
	inline void set_m_FaceID_2(uint32_t value)
	{
		___m_FaceID_2 = value;
	}

	inline static int32_t get_offset_of_m_Distance_3() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_Distance_3)); }
	inline float get_m_Distance_3() const { return ___m_Distance_3; }
	inline float* get_address_of_m_Distance_3() { return &___m_Distance_3; }
	inline void set_m_Distance_3(float value)
	{
		___m_Distance_3 = value;
	}

	inline static int32_t get_offset_of_m_UV_4() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_UV_4)); }
	inline Vector2_t2156229523  get_m_UV_4() const { return ___m_UV_4; }
	inline Vector2_t2156229523 * get_address_of_m_UV_4() { return &___m_UV_4; }
	inline void set_m_UV_4(Vector2_t2156229523  value)
	{
		___m_UV_4 = value;
	}

	inline static int32_t get_offset_of_m_Collider_5() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_Collider_5)); }
	inline int32_t get_m_Collider_5() const { return ___m_Collider_5; }
	inline int32_t* get_address_of_m_Collider_5() { return &___m_Collider_5; }
	inline void set_m_Collider_5(int32_t value)
	{
		___m_Collider_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAYCASTHIT_T1056001966_H
#ifndef PIXEL_FORMAT_T3209881435_H
#define PIXEL_FORMAT_T3209881435_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.Image/PIXEL_FORMAT
struct  PIXEL_FORMAT_t3209881435 
{
public:
	// System.Int32 Vuforia.Image/PIXEL_FORMAT::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PIXEL_FORMAT_t3209881435, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PIXEL_FORMAT_T3209881435_H
#ifndef U3CLOADIMPU3EC__ITERATOR1_T3157777824_H
#define U3CLOADIMPU3EC__ITERATOR1_T3157777824_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Click3D/<loadIMP>c__Iterator1
struct  U3CloadIMPU3Ec__Iterator1_t3157777824  : public RuntimeObject
{
public:
	// System.String Click3D/<loadIMP>c__Iterator1::<outputFile>__0
	String_t* ___U3CoutputFileU3E__0_0;
	// System.String Click3D/<loadIMP>c__Iterator1::<outputPath>__0
	String_t* ___U3CoutputPathU3E__0_1;
	// System.String Click3D/<loadIMP>c__Iterator1::url
	String_t* ___url_2;
	// UnityEngine.WWW Click3D/<loadIMP>c__Iterator1::<www>__1
	WWW_t3688466362 * ___U3CwwwU3E__1_3;
	// System.Int32 Click3D/<loadIMP>c__Iterator1::<val>__2
	int32_t ___U3CvalU3E__2_4;
	// System.Single Click3D/<loadIMP>c__Iterator1::<valf>__2
	float ___U3CvalfU3E__2_5;
	// System.Byte[] Click3D/<loadIMP>c__Iterator1::<data>__1
	ByteU5BU5D_t4116647657* ___U3CdataU3E__1_6;
	// UnityEngine.Bounds Click3D/<loadIMP>c__Iterator1::<containerBounds>__0
	Bounds_t2266837910  ___U3CcontainerBoundsU3E__0_7;
	// UnityEngine.GameObject Click3D/<loadIMP>c__Iterator1::<obj>__0
	GameObject_t1113636619 * ___U3CobjU3E__0_8;
	// System.Single Click3D/<loadIMP>c__Iterator1::<radius>__0
	float ___U3CradiusU3E__0_9;
	// Click3D Click3D/<loadIMP>c__Iterator1::$this
	Click3D_t85340219 * ___U24this_10;
	// System.Object Click3D/<loadIMP>c__Iterator1::$current
	RuntimeObject * ___U24current_11;
	// System.Boolean Click3D/<loadIMP>c__Iterator1::$disposing
	bool ___U24disposing_12;
	// System.Int32 Click3D/<loadIMP>c__Iterator1::$PC
	int32_t ___U24PC_13;

public:
	inline static int32_t get_offset_of_U3CoutputFileU3E__0_0() { return static_cast<int32_t>(offsetof(U3CloadIMPU3Ec__Iterator1_t3157777824, ___U3CoutputFileU3E__0_0)); }
	inline String_t* get_U3CoutputFileU3E__0_0() const { return ___U3CoutputFileU3E__0_0; }
	inline String_t** get_address_of_U3CoutputFileU3E__0_0() { return &___U3CoutputFileU3E__0_0; }
	inline void set_U3CoutputFileU3E__0_0(String_t* value)
	{
		___U3CoutputFileU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CoutputFileU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3CoutputPathU3E__0_1() { return static_cast<int32_t>(offsetof(U3CloadIMPU3Ec__Iterator1_t3157777824, ___U3CoutputPathU3E__0_1)); }
	inline String_t* get_U3CoutputPathU3E__0_1() const { return ___U3CoutputPathU3E__0_1; }
	inline String_t** get_address_of_U3CoutputPathU3E__0_1() { return &___U3CoutputPathU3E__0_1; }
	inline void set_U3CoutputPathU3E__0_1(String_t* value)
	{
		___U3CoutputPathU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CoutputPathU3E__0_1), value);
	}

	inline static int32_t get_offset_of_url_2() { return static_cast<int32_t>(offsetof(U3CloadIMPU3Ec__Iterator1_t3157777824, ___url_2)); }
	inline String_t* get_url_2() const { return ___url_2; }
	inline String_t** get_address_of_url_2() { return &___url_2; }
	inline void set_url_2(String_t* value)
	{
		___url_2 = value;
		Il2CppCodeGenWriteBarrier((&___url_2), value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E__1_3() { return static_cast<int32_t>(offsetof(U3CloadIMPU3Ec__Iterator1_t3157777824, ___U3CwwwU3E__1_3)); }
	inline WWW_t3688466362 * get_U3CwwwU3E__1_3() const { return ___U3CwwwU3E__1_3; }
	inline WWW_t3688466362 ** get_address_of_U3CwwwU3E__1_3() { return &___U3CwwwU3E__1_3; }
	inline void set_U3CwwwU3E__1_3(WWW_t3688466362 * value)
	{
		___U3CwwwU3E__1_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwwwU3E__1_3), value);
	}

	inline static int32_t get_offset_of_U3CvalU3E__2_4() { return static_cast<int32_t>(offsetof(U3CloadIMPU3Ec__Iterator1_t3157777824, ___U3CvalU3E__2_4)); }
	inline int32_t get_U3CvalU3E__2_4() const { return ___U3CvalU3E__2_4; }
	inline int32_t* get_address_of_U3CvalU3E__2_4() { return &___U3CvalU3E__2_4; }
	inline void set_U3CvalU3E__2_4(int32_t value)
	{
		___U3CvalU3E__2_4 = value;
	}

	inline static int32_t get_offset_of_U3CvalfU3E__2_5() { return static_cast<int32_t>(offsetof(U3CloadIMPU3Ec__Iterator1_t3157777824, ___U3CvalfU3E__2_5)); }
	inline float get_U3CvalfU3E__2_5() const { return ___U3CvalfU3E__2_5; }
	inline float* get_address_of_U3CvalfU3E__2_5() { return &___U3CvalfU3E__2_5; }
	inline void set_U3CvalfU3E__2_5(float value)
	{
		___U3CvalfU3E__2_5 = value;
	}

	inline static int32_t get_offset_of_U3CdataU3E__1_6() { return static_cast<int32_t>(offsetof(U3CloadIMPU3Ec__Iterator1_t3157777824, ___U3CdataU3E__1_6)); }
	inline ByteU5BU5D_t4116647657* get_U3CdataU3E__1_6() const { return ___U3CdataU3E__1_6; }
	inline ByteU5BU5D_t4116647657** get_address_of_U3CdataU3E__1_6() { return &___U3CdataU3E__1_6; }
	inline void set_U3CdataU3E__1_6(ByteU5BU5D_t4116647657* value)
	{
		___U3CdataU3E__1_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CdataU3E__1_6), value);
	}

	inline static int32_t get_offset_of_U3CcontainerBoundsU3E__0_7() { return static_cast<int32_t>(offsetof(U3CloadIMPU3Ec__Iterator1_t3157777824, ___U3CcontainerBoundsU3E__0_7)); }
	inline Bounds_t2266837910  get_U3CcontainerBoundsU3E__0_7() const { return ___U3CcontainerBoundsU3E__0_7; }
	inline Bounds_t2266837910 * get_address_of_U3CcontainerBoundsU3E__0_7() { return &___U3CcontainerBoundsU3E__0_7; }
	inline void set_U3CcontainerBoundsU3E__0_7(Bounds_t2266837910  value)
	{
		___U3CcontainerBoundsU3E__0_7 = value;
	}

	inline static int32_t get_offset_of_U3CobjU3E__0_8() { return static_cast<int32_t>(offsetof(U3CloadIMPU3Ec__Iterator1_t3157777824, ___U3CobjU3E__0_8)); }
	inline GameObject_t1113636619 * get_U3CobjU3E__0_8() const { return ___U3CobjU3E__0_8; }
	inline GameObject_t1113636619 ** get_address_of_U3CobjU3E__0_8() { return &___U3CobjU3E__0_8; }
	inline void set_U3CobjU3E__0_8(GameObject_t1113636619 * value)
	{
		___U3CobjU3E__0_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CobjU3E__0_8), value);
	}

	inline static int32_t get_offset_of_U3CradiusU3E__0_9() { return static_cast<int32_t>(offsetof(U3CloadIMPU3Ec__Iterator1_t3157777824, ___U3CradiusU3E__0_9)); }
	inline float get_U3CradiusU3E__0_9() const { return ___U3CradiusU3E__0_9; }
	inline float* get_address_of_U3CradiusU3E__0_9() { return &___U3CradiusU3E__0_9; }
	inline void set_U3CradiusU3E__0_9(float value)
	{
		___U3CradiusU3E__0_9 = value;
	}

	inline static int32_t get_offset_of_U24this_10() { return static_cast<int32_t>(offsetof(U3CloadIMPU3Ec__Iterator1_t3157777824, ___U24this_10)); }
	inline Click3D_t85340219 * get_U24this_10() const { return ___U24this_10; }
	inline Click3D_t85340219 ** get_address_of_U24this_10() { return &___U24this_10; }
	inline void set_U24this_10(Click3D_t85340219 * value)
	{
		___U24this_10 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_10), value);
	}

	inline static int32_t get_offset_of_U24current_11() { return static_cast<int32_t>(offsetof(U3CloadIMPU3Ec__Iterator1_t3157777824, ___U24current_11)); }
	inline RuntimeObject * get_U24current_11() const { return ___U24current_11; }
	inline RuntimeObject ** get_address_of_U24current_11() { return &___U24current_11; }
	inline void set_U24current_11(RuntimeObject * value)
	{
		___U24current_11 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_11), value);
	}

	inline static int32_t get_offset_of_U24disposing_12() { return static_cast<int32_t>(offsetof(U3CloadIMPU3Ec__Iterator1_t3157777824, ___U24disposing_12)); }
	inline bool get_U24disposing_12() const { return ___U24disposing_12; }
	inline bool* get_address_of_U24disposing_12() { return &___U24disposing_12; }
	inline void set_U24disposing_12(bool value)
	{
		___U24disposing_12 = value;
	}

	inline static int32_t get_offset_of_U24PC_13() { return static_cast<int32_t>(offsetof(U3CloadIMPU3Ec__Iterator1_t3157777824, ___U24PC_13)); }
	inline int32_t get_U24PC_13() const { return ___U24PC_13; }
	inline int32_t* get_address_of_U24PC_13() { return &___U24PC_13; }
	inline void set_U24PC_13(int32_t value)
	{
		___U24PC_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADIMPU3EC__ITERATOR1_T3157777824_H
#ifndef U3CU3EC__ITERATOR0_T2553288236_H
#define U3CU3EC__ITERATOR0_T2553288236_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleJSON.JSONObject/<>c__Iterator0
struct  U3CU3Ec__Iterator0_t2553288236  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2/Enumerator<System.String,SimpleJSON.JSONNode> SimpleJSON.JSONObject/<>c__Iterator0::$locvar0
	Enumerator_t390528775  ___U24locvar0_0;
	// System.Collections.Generic.KeyValuePair`2<System.String,SimpleJSON.JSONNode> SimpleJSON.JSONObject/<>c__Iterator0::<N>__1
	KeyValuePair_2_t834018167  ___U3CNU3E__1_1;
	// SimpleJSON.JSONObject SimpleJSON.JSONObject/<>c__Iterator0::$this
	JSONObject_t4158403488 * ___U24this_2;
	// SimpleJSON.JSONNode SimpleJSON.JSONObject/<>c__Iterator0::$current
	JSONNode_t2946056997 * ___U24current_3;
	// System.Boolean SimpleJSON.JSONObject/<>c__Iterator0::$disposing
	bool ___U24disposing_4;
	// System.Int32 SimpleJSON.JSONObject/<>c__Iterator0::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U24locvar0_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t2553288236, ___U24locvar0_0)); }
	inline Enumerator_t390528775  get_U24locvar0_0() const { return ___U24locvar0_0; }
	inline Enumerator_t390528775 * get_address_of_U24locvar0_0() { return &___U24locvar0_0; }
	inline void set_U24locvar0_0(Enumerator_t390528775  value)
	{
		___U24locvar0_0 = value;
	}

	inline static int32_t get_offset_of_U3CNU3E__1_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t2553288236, ___U3CNU3E__1_1)); }
	inline KeyValuePair_2_t834018167  get_U3CNU3E__1_1() const { return ___U3CNU3E__1_1; }
	inline KeyValuePair_2_t834018167 * get_address_of_U3CNU3E__1_1() { return &___U3CNU3E__1_1; }
	inline void set_U3CNU3E__1_1(KeyValuePair_2_t834018167  value)
	{
		___U3CNU3E__1_1 = value;
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t2553288236, ___U24this_2)); }
	inline JSONObject_t4158403488 * get_U24this_2() const { return ___U24this_2; }
	inline JSONObject_t4158403488 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(JSONObject_t4158403488 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t2553288236, ___U24current_3)); }
	inline JSONNode_t2946056997 * get_U24current_3() const { return ___U24current_3; }
	inline JSONNode_t2946056997 ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(JSONNode_t2946056997 * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t2553288236, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t2553288236, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__ITERATOR0_T2553288236_H
#ifndef U3CGETENUMERATORU3EC__ITERATOR1_T1727131513_H
#define U3CGETENUMERATORU3EC__ITERATOR1_T1727131513_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleJSON.JSONObject/<GetEnumerator>c__Iterator1
struct  U3CGetEnumeratorU3Ec__Iterator1_t1727131513  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2/Enumerator<System.String,SimpleJSON.JSONNode> SimpleJSON.JSONObject/<GetEnumerator>c__Iterator1::$locvar0
	Enumerator_t390528775  ___U24locvar0_0;
	// System.Collections.Generic.KeyValuePair`2<System.String,SimpleJSON.JSONNode> SimpleJSON.JSONObject/<GetEnumerator>c__Iterator1::<N>__1
	KeyValuePair_2_t834018167  ___U3CNU3E__1_1;
	// SimpleJSON.JSONObject SimpleJSON.JSONObject/<GetEnumerator>c__Iterator1::$this
	JSONObject_t4158403488 * ___U24this_2;
	// System.Object SimpleJSON.JSONObject/<GetEnumerator>c__Iterator1::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean SimpleJSON.JSONObject/<GetEnumerator>c__Iterator1::$disposing
	bool ___U24disposing_4;
	// System.Int32 SimpleJSON.JSONObject/<GetEnumerator>c__Iterator1::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U24locvar0_0() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator1_t1727131513, ___U24locvar0_0)); }
	inline Enumerator_t390528775  get_U24locvar0_0() const { return ___U24locvar0_0; }
	inline Enumerator_t390528775 * get_address_of_U24locvar0_0() { return &___U24locvar0_0; }
	inline void set_U24locvar0_0(Enumerator_t390528775  value)
	{
		___U24locvar0_0 = value;
	}

	inline static int32_t get_offset_of_U3CNU3E__1_1() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator1_t1727131513, ___U3CNU3E__1_1)); }
	inline KeyValuePair_2_t834018167  get_U3CNU3E__1_1() const { return ___U3CNU3E__1_1; }
	inline KeyValuePair_2_t834018167 * get_address_of_U3CNU3E__1_1() { return &___U3CNU3E__1_1; }
	inline void set_U3CNU3E__1_1(KeyValuePair_2_t834018167  value)
	{
		___U3CNU3E__1_1 = value;
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator1_t1727131513, ___U24this_2)); }
	inline JSONObject_t4158403488 * get_U24this_2() const { return ___U24this_2; }
	inline JSONObject_t4158403488 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(JSONObject_t4158403488 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator1_t1727131513, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator1_t1727131513, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator1_t1727131513, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETENUMERATORU3EC__ITERATOR1_T1727131513_H
#ifndef ENUMERATOR_T2913490280_H
#define ENUMERATOR_T2913490280_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.String,SimpleJSON.JSONNode>
struct  Enumerator_t2913490280 
{
public:
	// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator::host_enumerator
	Enumerator_t390528775  ___host_enumerator_0;

public:
	inline static int32_t get_offset_of_host_enumerator_0() { return static_cast<int32_t>(offsetof(Enumerator_t2913490280, ___host_enumerator_0)); }
	inline Enumerator_t390528775  get_host_enumerator_0() const { return ___host_enumerator_0; }
	inline Enumerator_t390528775 * get_address_of_host_enumerator_0() { return &___host_enumerator_0; }
	inline void set_host_enumerator_0(Enumerator_t390528775  value)
	{
		___host_enumerator_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T2913490280_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___prev_9)); }
	inline MulticastDelegate_t * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___kpm_next_10)); }
	inline MulticastDelegate_t * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef ONTRANSITIONDONEDELEG_T1142753823_H
#define ONTRANSITIONDONEDELEG_T1142753823_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DynamicObject/OnTransitionDoneDeleg
struct  OnTransitionDoneDeleg_t1142753823  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONTRANSITIONDONEDELEG_T1142753823_H
#ifndef U3CU3EC__ITERATOR2_T2935625260_H
#define U3CU3EC__ITERATOR2_T2935625260_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleJSON.JSONObject/<>c__Iterator2
struct  U3CU3Ec__Iterator2_t2935625260  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.String,SimpleJSON.JSONNode> SimpleJSON.JSONObject/<>c__Iterator2::$locvar0
	Enumerator_t2913490280  ___U24locvar0_0;
	// System.String SimpleJSON.JSONObject/<>c__Iterator2::<key>__1
	String_t* ___U3CkeyU3E__1_1;
	// SimpleJSON.JSONObject SimpleJSON.JSONObject/<>c__Iterator2::$this
	JSONObject_t4158403488 * ___U24this_2;
	// System.String SimpleJSON.JSONObject/<>c__Iterator2::$current
	String_t* ___U24current_3;
	// System.Boolean SimpleJSON.JSONObject/<>c__Iterator2::$disposing
	bool ___U24disposing_4;
	// System.Int32 SimpleJSON.JSONObject/<>c__Iterator2::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U24locvar0_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator2_t2935625260, ___U24locvar0_0)); }
	inline Enumerator_t2913490280  get_U24locvar0_0() const { return ___U24locvar0_0; }
	inline Enumerator_t2913490280 * get_address_of_U24locvar0_0() { return &___U24locvar0_0; }
	inline void set_U24locvar0_0(Enumerator_t2913490280  value)
	{
		___U24locvar0_0 = value;
	}

	inline static int32_t get_offset_of_U3CkeyU3E__1_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator2_t2935625260, ___U3CkeyU3E__1_1)); }
	inline String_t* get_U3CkeyU3E__1_1() const { return ___U3CkeyU3E__1_1; }
	inline String_t** get_address_of_U3CkeyU3E__1_1() { return &___U3CkeyU3E__1_1; }
	inline void set_U3CkeyU3E__1_1(String_t* value)
	{
		___U3CkeyU3E__1_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CkeyU3E__1_1), value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator2_t2935625260, ___U24this_2)); }
	inline JSONObject_t4158403488 * get_U24this_2() const { return ___U24this_2; }
	inline JSONObject_t4158403488 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(JSONObject_t4158403488 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator2_t2935625260, ___U24current_3)); }
	inline String_t* get_U24current_3() const { return ___U24current_3; }
	inline String_t** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(String_t* value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator2_t2935625260, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator2_t2935625260, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__ITERATOR2_T2935625260_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef ACTIONLAUNCHER_T253899709_H
#define ACTIONLAUNCHER_T253899709_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ActionLauncher
struct  ActionLauncher_t253899709  : public MonoBehaviour_t3962482529
{
public:
	// ActionLauncher/LaunchActionEvent ActionLauncher::m_OnLaunchAction
	LaunchActionEvent_t1220164713 * ___m_OnLaunchAction_4;

public:
	inline static int32_t get_offset_of_m_OnLaunchAction_4() { return static_cast<int32_t>(offsetof(ActionLauncher_t253899709, ___m_OnLaunchAction_4)); }
	inline LaunchActionEvent_t1220164713 * get_m_OnLaunchAction_4() const { return ___m_OnLaunchAction_4; }
	inline LaunchActionEvent_t1220164713 ** get_address_of_m_OnLaunchAction_4() { return &___m_OnLaunchAction_4; }
	inline void set_m_OnLaunchAction_4(LaunchActionEvent_t1220164713 * value)
	{
		___m_OnLaunchAction_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnLaunchAction_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTIONLAUNCHER_T253899709_H
#ifndef CAMERAFACINGBILLBOARD_T1991403390_H
#define CAMERAFACINGBILLBOARD_T1991403390_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraFacingBillboard
struct  CameraFacingBillboard_t1991403390  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Camera CameraFacingBillboard::referenceCamera
	Camera_t4157153871 * ___referenceCamera_4;
	// System.Boolean CameraFacingBillboard::reverseFace
	bool ___reverseFace_5;
	// CameraFacingBillboard/Axis CameraFacingBillboard::axis
	int32_t ___axis_6;

public:
	inline static int32_t get_offset_of_referenceCamera_4() { return static_cast<int32_t>(offsetof(CameraFacingBillboard_t1991403390, ___referenceCamera_4)); }
	inline Camera_t4157153871 * get_referenceCamera_4() const { return ___referenceCamera_4; }
	inline Camera_t4157153871 ** get_address_of_referenceCamera_4() { return &___referenceCamera_4; }
	inline void set_referenceCamera_4(Camera_t4157153871 * value)
	{
		___referenceCamera_4 = value;
		Il2CppCodeGenWriteBarrier((&___referenceCamera_4), value);
	}

	inline static int32_t get_offset_of_reverseFace_5() { return static_cast<int32_t>(offsetof(CameraFacingBillboard_t1991403390, ___reverseFace_5)); }
	inline bool get_reverseFace_5() const { return ___reverseFace_5; }
	inline bool* get_address_of_reverseFace_5() { return &___reverseFace_5; }
	inline void set_reverseFace_5(bool value)
	{
		___reverseFace_5 = value;
	}

	inline static int32_t get_offset_of_axis_6() { return static_cast<int32_t>(offsetof(CameraFacingBillboard_t1991403390, ___axis_6)); }
	inline int32_t get_axis_6() const { return ___axis_6; }
	inline int32_t* get_address_of_axis_6() { return &___axis_6; }
	inline void set_axis_6(int32_t value)
	{
		___axis_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERAFACINGBILLBOARD_T1991403390_H
#ifndef CLICK3D_T85340219_H
#define CLICK3D_T85340219_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Click3D
struct  Click3D_t85340219  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Collider Click3D::fullscreenBtn
	Collider_t1773347010 * ___fullscreenBtn_4;
	// UnityEngine.Collider Click3D::playBtn
	Collider_t1773347010 * ___playBtn_5;
	// UnityEngine.Collider Click3D::pauseBtn
	Collider_t1773347010 * ___pauseBtn_6;
	// UnityEngine.Collider Click3D::closeBtn
	Collider_t1773347010 * ___closeBtn_7;
	// UnityEngine.Ray Click3D::ray
	Ray_t3785851493  ___ray_8;
	// UnityEngine.RaycastHit[] Click3D::hitAllTemp
	RaycastHitU5BU5D_t1690781147* ___hitAllTemp_9;
	// UnityEngine.Collider Click3D::collider3D
	Collider_t1773347010 * ___collider3D_10;
	// System.String Click3D::url3D
	String_t* ___url3D_11;
	// System.String Click3D::GUID
	String_t* ___GUID_12;
	// UnityEngine.GameObject Click3D::container
	GameObject_t1113636619 * ___container_13;
	// UnityEngine.GameObject Click3D::closeButton
	GameObject_t1113636619 * ___closeButton_14;
	// ContentManager2 Click3D::contentManager
	ContentManager2_t2980555998 * ___contentManager_15;
	// UnityEngine.GameObject Click3D::face
	GameObject_t1113636619 * ___face_16;
	// UnityEngine.GameObject Click3D::FS_OBJ_container
	GameObject_t1113636619 * ___FS_OBJ_container_17;
	// UnityEngine.GameObject Click3D::FS_OBJ
	GameObject_t1113636619 * ___FS_OBJ_18;
	// UnityEngine.GameObject Click3D::gears
	GameObject_t1113636619 * ___gears_19;
	// UnityEngine.GameObject Click3D::CameraOBJContainer
	GameObject_t1113636619 * ___CameraOBJContainer_20;
	// UnityEngine.GameObject Click3D::sphereCloseUp
	GameObject_t1113636619 * ___sphereCloseUp_21;
	// UnityEngine.TextMesh Click3D::text3d
	TextMesh_t1536577757 * ___text3d_22;
	// UnityEngine.GameObject Click3D::cubeEtalon
	GameObject_t1113636619 * ___cubeEtalon_23;
	// System.Boolean Click3D::hasBeenDownloaded
	bool ___hasBeenDownloaded_24;
	// System.Boolean Click3D::requestAutoPlay
	bool ___requestAutoPlay_25;
	// System.Boolean Click3D::isOpen
	bool ___isOpen_27;
	// System.Int32 Click3D::ID
	int32_t ___ID_28;
	// System.Boolean Click3D::isDownloading
	bool ___isDownloading_29;
	// UnityEngine.Bounds Click3D::combinedBounds
	Bounds_t2266837910  ___combinedBounds_30;
	// System.String Click3D::pathDownloading
	String_t* ___pathDownloading_31;

public:
	inline static int32_t get_offset_of_fullscreenBtn_4() { return static_cast<int32_t>(offsetof(Click3D_t85340219, ___fullscreenBtn_4)); }
	inline Collider_t1773347010 * get_fullscreenBtn_4() const { return ___fullscreenBtn_4; }
	inline Collider_t1773347010 ** get_address_of_fullscreenBtn_4() { return &___fullscreenBtn_4; }
	inline void set_fullscreenBtn_4(Collider_t1773347010 * value)
	{
		___fullscreenBtn_4 = value;
		Il2CppCodeGenWriteBarrier((&___fullscreenBtn_4), value);
	}

	inline static int32_t get_offset_of_playBtn_5() { return static_cast<int32_t>(offsetof(Click3D_t85340219, ___playBtn_5)); }
	inline Collider_t1773347010 * get_playBtn_5() const { return ___playBtn_5; }
	inline Collider_t1773347010 ** get_address_of_playBtn_5() { return &___playBtn_5; }
	inline void set_playBtn_5(Collider_t1773347010 * value)
	{
		___playBtn_5 = value;
		Il2CppCodeGenWriteBarrier((&___playBtn_5), value);
	}

	inline static int32_t get_offset_of_pauseBtn_6() { return static_cast<int32_t>(offsetof(Click3D_t85340219, ___pauseBtn_6)); }
	inline Collider_t1773347010 * get_pauseBtn_6() const { return ___pauseBtn_6; }
	inline Collider_t1773347010 ** get_address_of_pauseBtn_6() { return &___pauseBtn_6; }
	inline void set_pauseBtn_6(Collider_t1773347010 * value)
	{
		___pauseBtn_6 = value;
		Il2CppCodeGenWriteBarrier((&___pauseBtn_6), value);
	}

	inline static int32_t get_offset_of_closeBtn_7() { return static_cast<int32_t>(offsetof(Click3D_t85340219, ___closeBtn_7)); }
	inline Collider_t1773347010 * get_closeBtn_7() const { return ___closeBtn_7; }
	inline Collider_t1773347010 ** get_address_of_closeBtn_7() { return &___closeBtn_7; }
	inline void set_closeBtn_7(Collider_t1773347010 * value)
	{
		___closeBtn_7 = value;
		Il2CppCodeGenWriteBarrier((&___closeBtn_7), value);
	}

	inline static int32_t get_offset_of_ray_8() { return static_cast<int32_t>(offsetof(Click3D_t85340219, ___ray_8)); }
	inline Ray_t3785851493  get_ray_8() const { return ___ray_8; }
	inline Ray_t3785851493 * get_address_of_ray_8() { return &___ray_8; }
	inline void set_ray_8(Ray_t3785851493  value)
	{
		___ray_8 = value;
	}

	inline static int32_t get_offset_of_hitAllTemp_9() { return static_cast<int32_t>(offsetof(Click3D_t85340219, ___hitAllTemp_9)); }
	inline RaycastHitU5BU5D_t1690781147* get_hitAllTemp_9() const { return ___hitAllTemp_9; }
	inline RaycastHitU5BU5D_t1690781147** get_address_of_hitAllTemp_9() { return &___hitAllTemp_9; }
	inline void set_hitAllTemp_9(RaycastHitU5BU5D_t1690781147* value)
	{
		___hitAllTemp_9 = value;
		Il2CppCodeGenWriteBarrier((&___hitAllTemp_9), value);
	}

	inline static int32_t get_offset_of_collider3D_10() { return static_cast<int32_t>(offsetof(Click3D_t85340219, ___collider3D_10)); }
	inline Collider_t1773347010 * get_collider3D_10() const { return ___collider3D_10; }
	inline Collider_t1773347010 ** get_address_of_collider3D_10() { return &___collider3D_10; }
	inline void set_collider3D_10(Collider_t1773347010 * value)
	{
		___collider3D_10 = value;
		Il2CppCodeGenWriteBarrier((&___collider3D_10), value);
	}

	inline static int32_t get_offset_of_url3D_11() { return static_cast<int32_t>(offsetof(Click3D_t85340219, ___url3D_11)); }
	inline String_t* get_url3D_11() const { return ___url3D_11; }
	inline String_t** get_address_of_url3D_11() { return &___url3D_11; }
	inline void set_url3D_11(String_t* value)
	{
		___url3D_11 = value;
		Il2CppCodeGenWriteBarrier((&___url3D_11), value);
	}

	inline static int32_t get_offset_of_GUID_12() { return static_cast<int32_t>(offsetof(Click3D_t85340219, ___GUID_12)); }
	inline String_t* get_GUID_12() const { return ___GUID_12; }
	inline String_t** get_address_of_GUID_12() { return &___GUID_12; }
	inline void set_GUID_12(String_t* value)
	{
		___GUID_12 = value;
		Il2CppCodeGenWriteBarrier((&___GUID_12), value);
	}

	inline static int32_t get_offset_of_container_13() { return static_cast<int32_t>(offsetof(Click3D_t85340219, ___container_13)); }
	inline GameObject_t1113636619 * get_container_13() const { return ___container_13; }
	inline GameObject_t1113636619 ** get_address_of_container_13() { return &___container_13; }
	inline void set_container_13(GameObject_t1113636619 * value)
	{
		___container_13 = value;
		Il2CppCodeGenWriteBarrier((&___container_13), value);
	}

	inline static int32_t get_offset_of_closeButton_14() { return static_cast<int32_t>(offsetof(Click3D_t85340219, ___closeButton_14)); }
	inline GameObject_t1113636619 * get_closeButton_14() const { return ___closeButton_14; }
	inline GameObject_t1113636619 ** get_address_of_closeButton_14() { return &___closeButton_14; }
	inline void set_closeButton_14(GameObject_t1113636619 * value)
	{
		___closeButton_14 = value;
		Il2CppCodeGenWriteBarrier((&___closeButton_14), value);
	}

	inline static int32_t get_offset_of_contentManager_15() { return static_cast<int32_t>(offsetof(Click3D_t85340219, ___contentManager_15)); }
	inline ContentManager2_t2980555998 * get_contentManager_15() const { return ___contentManager_15; }
	inline ContentManager2_t2980555998 ** get_address_of_contentManager_15() { return &___contentManager_15; }
	inline void set_contentManager_15(ContentManager2_t2980555998 * value)
	{
		___contentManager_15 = value;
		Il2CppCodeGenWriteBarrier((&___contentManager_15), value);
	}

	inline static int32_t get_offset_of_face_16() { return static_cast<int32_t>(offsetof(Click3D_t85340219, ___face_16)); }
	inline GameObject_t1113636619 * get_face_16() const { return ___face_16; }
	inline GameObject_t1113636619 ** get_address_of_face_16() { return &___face_16; }
	inline void set_face_16(GameObject_t1113636619 * value)
	{
		___face_16 = value;
		Il2CppCodeGenWriteBarrier((&___face_16), value);
	}

	inline static int32_t get_offset_of_FS_OBJ_container_17() { return static_cast<int32_t>(offsetof(Click3D_t85340219, ___FS_OBJ_container_17)); }
	inline GameObject_t1113636619 * get_FS_OBJ_container_17() const { return ___FS_OBJ_container_17; }
	inline GameObject_t1113636619 ** get_address_of_FS_OBJ_container_17() { return &___FS_OBJ_container_17; }
	inline void set_FS_OBJ_container_17(GameObject_t1113636619 * value)
	{
		___FS_OBJ_container_17 = value;
		Il2CppCodeGenWriteBarrier((&___FS_OBJ_container_17), value);
	}

	inline static int32_t get_offset_of_FS_OBJ_18() { return static_cast<int32_t>(offsetof(Click3D_t85340219, ___FS_OBJ_18)); }
	inline GameObject_t1113636619 * get_FS_OBJ_18() const { return ___FS_OBJ_18; }
	inline GameObject_t1113636619 ** get_address_of_FS_OBJ_18() { return &___FS_OBJ_18; }
	inline void set_FS_OBJ_18(GameObject_t1113636619 * value)
	{
		___FS_OBJ_18 = value;
		Il2CppCodeGenWriteBarrier((&___FS_OBJ_18), value);
	}

	inline static int32_t get_offset_of_gears_19() { return static_cast<int32_t>(offsetof(Click3D_t85340219, ___gears_19)); }
	inline GameObject_t1113636619 * get_gears_19() const { return ___gears_19; }
	inline GameObject_t1113636619 ** get_address_of_gears_19() { return &___gears_19; }
	inline void set_gears_19(GameObject_t1113636619 * value)
	{
		___gears_19 = value;
		Il2CppCodeGenWriteBarrier((&___gears_19), value);
	}

	inline static int32_t get_offset_of_CameraOBJContainer_20() { return static_cast<int32_t>(offsetof(Click3D_t85340219, ___CameraOBJContainer_20)); }
	inline GameObject_t1113636619 * get_CameraOBJContainer_20() const { return ___CameraOBJContainer_20; }
	inline GameObject_t1113636619 ** get_address_of_CameraOBJContainer_20() { return &___CameraOBJContainer_20; }
	inline void set_CameraOBJContainer_20(GameObject_t1113636619 * value)
	{
		___CameraOBJContainer_20 = value;
		Il2CppCodeGenWriteBarrier((&___CameraOBJContainer_20), value);
	}

	inline static int32_t get_offset_of_sphereCloseUp_21() { return static_cast<int32_t>(offsetof(Click3D_t85340219, ___sphereCloseUp_21)); }
	inline GameObject_t1113636619 * get_sphereCloseUp_21() const { return ___sphereCloseUp_21; }
	inline GameObject_t1113636619 ** get_address_of_sphereCloseUp_21() { return &___sphereCloseUp_21; }
	inline void set_sphereCloseUp_21(GameObject_t1113636619 * value)
	{
		___sphereCloseUp_21 = value;
		Il2CppCodeGenWriteBarrier((&___sphereCloseUp_21), value);
	}

	inline static int32_t get_offset_of_text3d_22() { return static_cast<int32_t>(offsetof(Click3D_t85340219, ___text3d_22)); }
	inline TextMesh_t1536577757 * get_text3d_22() const { return ___text3d_22; }
	inline TextMesh_t1536577757 ** get_address_of_text3d_22() { return &___text3d_22; }
	inline void set_text3d_22(TextMesh_t1536577757 * value)
	{
		___text3d_22 = value;
		Il2CppCodeGenWriteBarrier((&___text3d_22), value);
	}

	inline static int32_t get_offset_of_cubeEtalon_23() { return static_cast<int32_t>(offsetof(Click3D_t85340219, ___cubeEtalon_23)); }
	inline GameObject_t1113636619 * get_cubeEtalon_23() const { return ___cubeEtalon_23; }
	inline GameObject_t1113636619 ** get_address_of_cubeEtalon_23() { return &___cubeEtalon_23; }
	inline void set_cubeEtalon_23(GameObject_t1113636619 * value)
	{
		___cubeEtalon_23 = value;
		Il2CppCodeGenWriteBarrier((&___cubeEtalon_23), value);
	}

	inline static int32_t get_offset_of_hasBeenDownloaded_24() { return static_cast<int32_t>(offsetof(Click3D_t85340219, ___hasBeenDownloaded_24)); }
	inline bool get_hasBeenDownloaded_24() const { return ___hasBeenDownloaded_24; }
	inline bool* get_address_of_hasBeenDownloaded_24() { return &___hasBeenDownloaded_24; }
	inline void set_hasBeenDownloaded_24(bool value)
	{
		___hasBeenDownloaded_24 = value;
	}

	inline static int32_t get_offset_of_requestAutoPlay_25() { return static_cast<int32_t>(offsetof(Click3D_t85340219, ___requestAutoPlay_25)); }
	inline bool get_requestAutoPlay_25() const { return ___requestAutoPlay_25; }
	inline bool* get_address_of_requestAutoPlay_25() { return &___requestAutoPlay_25; }
	inline void set_requestAutoPlay_25(bool value)
	{
		___requestAutoPlay_25 = value;
	}

	inline static int32_t get_offset_of_isOpen_27() { return static_cast<int32_t>(offsetof(Click3D_t85340219, ___isOpen_27)); }
	inline bool get_isOpen_27() const { return ___isOpen_27; }
	inline bool* get_address_of_isOpen_27() { return &___isOpen_27; }
	inline void set_isOpen_27(bool value)
	{
		___isOpen_27 = value;
	}

	inline static int32_t get_offset_of_ID_28() { return static_cast<int32_t>(offsetof(Click3D_t85340219, ___ID_28)); }
	inline int32_t get_ID_28() const { return ___ID_28; }
	inline int32_t* get_address_of_ID_28() { return &___ID_28; }
	inline void set_ID_28(int32_t value)
	{
		___ID_28 = value;
	}

	inline static int32_t get_offset_of_isDownloading_29() { return static_cast<int32_t>(offsetof(Click3D_t85340219, ___isDownloading_29)); }
	inline bool get_isDownloading_29() const { return ___isDownloading_29; }
	inline bool* get_address_of_isDownloading_29() { return &___isDownloading_29; }
	inline void set_isDownloading_29(bool value)
	{
		___isDownloading_29 = value;
	}

	inline static int32_t get_offset_of_combinedBounds_30() { return static_cast<int32_t>(offsetof(Click3D_t85340219, ___combinedBounds_30)); }
	inline Bounds_t2266837910  get_combinedBounds_30() const { return ___combinedBounds_30; }
	inline Bounds_t2266837910 * get_address_of_combinedBounds_30() { return &___combinedBounds_30; }
	inline void set_combinedBounds_30(Bounds_t2266837910  value)
	{
		___combinedBounds_30 = value;
	}

	inline static int32_t get_offset_of_pathDownloading_31() { return static_cast<int32_t>(offsetof(Click3D_t85340219, ___pathDownloading_31)); }
	inline String_t* get_pathDownloading_31() const { return ___pathDownloading_31; }
	inline String_t** get_address_of_pathDownloading_31() { return &___pathDownloading_31; }
	inline void set_pathDownloading_31(String_t* value)
	{
		___pathDownloading_31 = value;
		Il2CppCodeGenWriteBarrier((&___pathDownloading_31), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLICK3D_T85340219_H
#ifndef CLICKAUDIO_T2208965782_H
#define CLICKAUDIO_T2208965782_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ClickAudio
struct  ClickAudio_t2208965782  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.RaycastHit ClickAudio::hit
	RaycastHit_t1056001966  ___hit_4;
	// UnityEngine.Ray ClickAudio::ray
	Ray_t3785851493  ___ray_5;
	// UnityEngine.RaycastHit[] ClickAudio::hitAllTemp
	RaycastHitU5BU5D_t1690781147* ___hitAllTemp_6;
	// UnityEngine.Collider ClickAudio::colliderPlay
	Collider_t1773347010 * ___colliderPlay_7;
	// UnityEngine.Collider ClickAudio::colliderPause
	Collider_t1773347010 * ___colliderPause_8;
	// UnityEngine.Collider ClickAudio::colliderSoundOn
	Collider_t1773347010 * ___colliderSoundOn_9;
	// UnityEngine.Collider ClickAudio::colliderSoundOff
	Collider_t1773347010 * ___colliderSoundOff_10;
	// UnityEngine.Collider ClickAudio::colliderPlayer
	Collider_t1773347010 * ___colliderPlayer_11;
	// System.String ClickAudio::audioURL
	String_t* ___audioURL_12;
	// System.String ClickAudio::image
	String_t* ___image_13;
	// UnityEngine.GameObject ClickAudio::plane
	GameObject_t1113636619 * ___plane_14;
	// UnityEngine.GameObject ClickAudio::button
	GameObject_t1113636619 * ___button_15;
	// System.Boolean ClickAudio::requestAutoPlay
	bool ___requestAutoPlay_16;
	// UnityEngine.AudioSource ClickAudio::audioPlayer
	AudioSource_t3935305588 * ___audioPlayer_17;

public:
	inline static int32_t get_offset_of_hit_4() { return static_cast<int32_t>(offsetof(ClickAudio_t2208965782, ___hit_4)); }
	inline RaycastHit_t1056001966  get_hit_4() const { return ___hit_4; }
	inline RaycastHit_t1056001966 * get_address_of_hit_4() { return &___hit_4; }
	inline void set_hit_4(RaycastHit_t1056001966  value)
	{
		___hit_4 = value;
	}

	inline static int32_t get_offset_of_ray_5() { return static_cast<int32_t>(offsetof(ClickAudio_t2208965782, ___ray_5)); }
	inline Ray_t3785851493  get_ray_5() const { return ___ray_5; }
	inline Ray_t3785851493 * get_address_of_ray_5() { return &___ray_5; }
	inline void set_ray_5(Ray_t3785851493  value)
	{
		___ray_5 = value;
	}

	inline static int32_t get_offset_of_hitAllTemp_6() { return static_cast<int32_t>(offsetof(ClickAudio_t2208965782, ___hitAllTemp_6)); }
	inline RaycastHitU5BU5D_t1690781147* get_hitAllTemp_6() const { return ___hitAllTemp_6; }
	inline RaycastHitU5BU5D_t1690781147** get_address_of_hitAllTemp_6() { return &___hitAllTemp_6; }
	inline void set_hitAllTemp_6(RaycastHitU5BU5D_t1690781147* value)
	{
		___hitAllTemp_6 = value;
		Il2CppCodeGenWriteBarrier((&___hitAllTemp_6), value);
	}

	inline static int32_t get_offset_of_colliderPlay_7() { return static_cast<int32_t>(offsetof(ClickAudio_t2208965782, ___colliderPlay_7)); }
	inline Collider_t1773347010 * get_colliderPlay_7() const { return ___colliderPlay_7; }
	inline Collider_t1773347010 ** get_address_of_colliderPlay_7() { return &___colliderPlay_7; }
	inline void set_colliderPlay_7(Collider_t1773347010 * value)
	{
		___colliderPlay_7 = value;
		Il2CppCodeGenWriteBarrier((&___colliderPlay_7), value);
	}

	inline static int32_t get_offset_of_colliderPause_8() { return static_cast<int32_t>(offsetof(ClickAudio_t2208965782, ___colliderPause_8)); }
	inline Collider_t1773347010 * get_colliderPause_8() const { return ___colliderPause_8; }
	inline Collider_t1773347010 ** get_address_of_colliderPause_8() { return &___colliderPause_8; }
	inline void set_colliderPause_8(Collider_t1773347010 * value)
	{
		___colliderPause_8 = value;
		Il2CppCodeGenWriteBarrier((&___colliderPause_8), value);
	}

	inline static int32_t get_offset_of_colliderSoundOn_9() { return static_cast<int32_t>(offsetof(ClickAudio_t2208965782, ___colliderSoundOn_9)); }
	inline Collider_t1773347010 * get_colliderSoundOn_9() const { return ___colliderSoundOn_9; }
	inline Collider_t1773347010 ** get_address_of_colliderSoundOn_9() { return &___colliderSoundOn_9; }
	inline void set_colliderSoundOn_9(Collider_t1773347010 * value)
	{
		___colliderSoundOn_9 = value;
		Il2CppCodeGenWriteBarrier((&___colliderSoundOn_9), value);
	}

	inline static int32_t get_offset_of_colliderSoundOff_10() { return static_cast<int32_t>(offsetof(ClickAudio_t2208965782, ___colliderSoundOff_10)); }
	inline Collider_t1773347010 * get_colliderSoundOff_10() const { return ___colliderSoundOff_10; }
	inline Collider_t1773347010 ** get_address_of_colliderSoundOff_10() { return &___colliderSoundOff_10; }
	inline void set_colliderSoundOff_10(Collider_t1773347010 * value)
	{
		___colliderSoundOff_10 = value;
		Il2CppCodeGenWriteBarrier((&___colliderSoundOff_10), value);
	}

	inline static int32_t get_offset_of_colliderPlayer_11() { return static_cast<int32_t>(offsetof(ClickAudio_t2208965782, ___colliderPlayer_11)); }
	inline Collider_t1773347010 * get_colliderPlayer_11() const { return ___colliderPlayer_11; }
	inline Collider_t1773347010 ** get_address_of_colliderPlayer_11() { return &___colliderPlayer_11; }
	inline void set_colliderPlayer_11(Collider_t1773347010 * value)
	{
		___colliderPlayer_11 = value;
		Il2CppCodeGenWriteBarrier((&___colliderPlayer_11), value);
	}

	inline static int32_t get_offset_of_audioURL_12() { return static_cast<int32_t>(offsetof(ClickAudio_t2208965782, ___audioURL_12)); }
	inline String_t* get_audioURL_12() const { return ___audioURL_12; }
	inline String_t** get_address_of_audioURL_12() { return &___audioURL_12; }
	inline void set_audioURL_12(String_t* value)
	{
		___audioURL_12 = value;
		Il2CppCodeGenWriteBarrier((&___audioURL_12), value);
	}

	inline static int32_t get_offset_of_image_13() { return static_cast<int32_t>(offsetof(ClickAudio_t2208965782, ___image_13)); }
	inline String_t* get_image_13() const { return ___image_13; }
	inline String_t** get_address_of_image_13() { return &___image_13; }
	inline void set_image_13(String_t* value)
	{
		___image_13 = value;
		Il2CppCodeGenWriteBarrier((&___image_13), value);
	}

	inline static int32_t get_offset_of_plane_14() { return static_cast<int32_t>(offsetof(ClickAudio_t2208965782, ___plane_14)); }
	inline GameObject_t1113636619 * get_plane_14() const { return ___plane_14; }
	inline GameObject_t1113636619 ** get_address_of_plane_14() { return &___plane_14; }
	inline void set_plane_14(GameObject_t1113636619 * value)
	{
		___plane_14 = value;
		Il2CppCodeGenWriteBarrier((&___plane_14), value);
	}

	inline static int32_t get_offset_of_button_15() { return static_cast<int32_t>(offsetof(ClickAudio_t2208965782, ___button_15)); }
	inline GameObject_t1113636619 * get_button_15() const { return ___button_15; }
	inline GameObject_t1113636619 ** get_address_of_button_15() { return &___button_15; }
	inline void set_button_15(GameObject_t1113636619 * value)
	{
		___button_15 = value;
		Il2CppCodeGenWriteBarrier((&___button_15), value);
	}

	inline static int32_t get_offset_of_requestAutoPlay_16() { return static_cast<int32_t>(offsetof(ClickAudio_t2208965782, ___requestAutoPlay_16)); }
	inline bool get_requestAutoPlay_16() const { return ___requestAutoPlay_16; }
	inline bool* get_address_of_requestAutoPlay_16() { return &___requestAutoPlay_16; }
	inline void set_requestAutoPlay_16(bool value)
	{
		___requestAutoPlay_16 = value;
	}

	inline static int32_t get_offset_of_audioPlayer_17() { return static_cast<int32_t>(offsetof(ClickAudio_t2208965782, ___audioPlayer_17)); }
	inline AudioSource_t3935305588 * get_audioPlayer_17() const { return ___audioPlayer_17; }
	inline AudioSource_t3935305588 ** get_address_of_audioPlayer_17() { return &___audioPlayer_17; }
	inline void set_audioPlayer_17(AudioSource_t3935305588 * value)
	{
		___audioPlayer_17 = value;
		Il2CppCodeGenWriteBarrier((&___audioPlayer_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLICKAUDIO_T2208965782_H
#ifndef CLICKME_T220080192_H
#define CLICKME_T220080192_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ClickMe
struct  ClickMe_t220080192  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.RaycastHit ClickMe::hit
	RaycastHit_t1056001966  ___hit_4;
	// UnityEngine.Ray ClickMe::ray
	Ray_t3785851493  ___ray_5;
	// UnityEngine.RaycastHit[] ClickMe::hitAllTemp
	RaycastHitU5BU5D_t1690781147* ___hitAllTemp_6;
	// UnityEngine.Collider ClickMe::colliderFullScreen
	Collider_t1773347010 * ___colliderFullScreen_7;
	// UnityEngine.Collider ClickMe::colliderPlay
	Collider_t1773347010 * ___colliderPlay_8;
	// UnityEngine.Collider ClickMe::colliderPause
	Collider_t1773347010 * ___colliderPause_9;
	// UnityEngine.Collider ClickMe::colliderSoundOn
	Collider_t1773347010 * ___colliderSoundOn_10;
	// UnityEngine.Collider ClickMe::colliderSoundOff
	Collider_t1773347010 * ___colliderSoundOff_11;
	// UnityEngine.Collider ClickMe::colliderPlayer
	Collider_t1773347010 * ___colliderPlayer_12;
	// UnityEngine.GameObject ClickMe::fullscreenVideoPlayer
	GameObject_t1113636619 * ___fullscreenVideoPlayer_13;
	// UnityEngine.GameObject ClickMe::playerLandscape
	GameObject_t1113636619 * ___playerLandscape_14;
	// UnityEngine.GameObject ClickMe::playerPortrait
	GameObject_t1113636619 * ___playerPortrait_15;
	// UnityEngine.UI.RawImage ClickMe::landscape_Raw
	RawImage_t3182918964 * ___landscape_Raw_16;
	// UnityEngine.UI.RawImage ClickMe::portrait_Raw
	RawImage_t3182918964 * ___portrait_Raw_17;
	// System.String ClickMe::urlVideo
	String_t* ___urlVideo_18;
	// System.Int32 ClickMe::vidWidth
	int32_t ___vidWidth_19;
	// System.Int32 ClickMe::vidHeight
	int32_t ___vidHeight_20;
	// System.String ClickMe::image
	String_t* ___image_21;
	// UnityEngine.Video.VideoPlayer ClickMe::playerCtrl3D
	VideoPlayer_t1683042537 * ___playerCtrl3D_22;
	// UnityEngine.GameObject ClickMe::iconeFS
	GameObject_t1113636619 * ___iconeFS_23;
	// UnityEngine.GameObject ClickMe::plane
	GameObject_t1113636619 * ___plane_24;
	// UnityEngine.GameObject ClickMe::button
	GameObject_t1113636619 * ___button_25;
	// UnityEngine.GameObject ClickMe::playerVideo3D
	GameObject_t1113636619 * ___playerVideo3D_26;

public:
	inline static int32_t get_offset_of_hit_4() { return static_cast<int32_t>(offsetof(ClickMe_t220080192, ___hit_4)); }
	inline RaycastHit_t1056001966  get_hit_4() const { return ___hit_4; }
	inline RaycastHit_t1056001966 * get_address_of_hit_4() { return &___hit_4; }
	inline void set_hit_4(RaycastHit_t1056001966  value)
	{
		___hit_4 = value;
	}

	inline static int32_t get_offset_of_ray_5() { return static_cast<int32_t>(offsetof(ClickMe_t220080192, ___ray_5)); }
	inline Ray_t3785851493  get_ray_5() const { return ___ray_5; }
	inline Ray_t3785851493 * get_address_of_ray_5() { return &___ray_5; }
	inline void set_ray_5(Ray_t3785851493  value)
	{
		___ray_5 = value;
	}

	inline static int32_t get_offset_of_hitAllTemp_6() { return static_cast<int32_t>(offsetof(ClickMe_t220080192, ___hitAllTemp_6)); }
	inline RaycastHitU5BU5D_t1690781147* get_hitAllTemp_6() const { return ___hitAllTemp_6; }
	inline RaycastHitU5BU5D_t1690781147** get_address_of_hitAllTemp_6() { return &___hitAllTemp_6; }
	inline void set_hitAllTemp_6(RaycastHitU5BU5D_t1690781147* value)
	{
		___hitAllTemp_6 = value;
		Il2CppCodeGenWriteBarrier((&___hitAllTemp_6), value);
	}

	inline static int32_t get_offset_of_colliderFullScreen_7() { return static_cast<int32_t>(offsetof(ClickMe_t220080192, ___colliderFullScreen_7)); }
	inline Collider_t1773347010 * get_colliderFullScreen_7() const { return ___colliderFullScreen_7; }
	inline Collider_t1773347010 ** get_address_of_colliderFullScreen_7() { return &___colliderFullScreen_7; }
	inline void set_colliderFullScreen_7(Collider_t1773347010 * value)
	{
		___colliderFullScreen_7 = value;
		Il2CppCodeGenWriteBarrier((&___colliderFullScreen_7), value);
	}

	inline static int32_t get_offset_of_colliderPlay_8() { return static_cast<int32_t>(offsetof(ClickMe_t220080192, ___colliderPlay_8)); }
	inline Collider_t1773347010 * get_colliderPlay_8() const { return ___colliderPlay_8; }
	inline Collider_t1773347010 ** get_address_of_colliderPlay_8() { return &___colliderPlay_8; }
	inline void set_colliderPlay_8(Collider_t1773347010 * value)
	{
		___colliderPlay_8 = value;
		Il2CppCodeGenWriteBarrier((&___colliderPlay_8), value);
	}

	inline static int32_t get_offset_of_colliderPause_9() { return static_cast<int32_t>(offsetof(ClickMe_t220080192, ___colliderPause_9)); }
	inline Collider_t1773347010 * get_colliderPause_9() const { return ___colliderPause_9; }
	inline Collider_t1773347010 ** get_address_of_colliderPause_9() { return &___colliderPause_9; }
	inline void set_colliderPause_9(Collider_t1773347010 * value)
	{
		___colliderPause_9 = value;
		Il2CppCodeGenWriteBarrier((&___colliderPause_9), value);
	}

	inline static int32_t get_offset_of_colliderSoundOn_10() { return static_cast<int32_t>(offsetof(ClickMe_t220080192, ___colliderSoundOn_10)); }
	inline Collider_t1773347010 * get_colliderSoundOn_10() const { return ___colliderSoundOn_10; }
	inline Collider_t1773347010 ** get_address_of_colliderSoundOn_10() { return &___colliderSoundOn_10; }
	inline void set_colliderSoundOn_10(Collider_t1773347010 * value)
	{
		___colliderSoundOn_10 = value;
		Il2CppCodeGenWriteBarrier((&___colliderSoundOn_10), value);
	}

	inline static int32_t get_offset_of_colliderSoundOff_11() { return static_cast<int32_t>(offsetof(ClickMe_t220080192, ___colliderSoundOff_11)); }
	inline Collider_t1773347010 * get_colliderSoundOff_11() const { return ___colliderSoundOff_11; }
	inline Collider_t1773347010 ** get_address_of_colliderSoundOff_11() { return &___colliderSoundOff_11; }
	inline void set_colliderSoundOff_11(Collider_t1773347010 * value)
	{
		___colliderSoundOff_11 = value;
		Il2CppCodeGenWriteBarrier((&___colliderSoundOff_11), value);
	}

	inline static int32_t get_offset_of_colliderPlayer_12() { return static_cast<int32_t>(offsetof(ClickMe_t220080192, ___colliderPlayer_12)); }
	inline Collider_t1773347010 * get_colliderPlayer_12() const { return ___colliderPlayer_12; }
	inline Collider_t1773347010 ** get_address_of_colliderPlayer_12() { return &___colliderPlayer_12; }
	inline void set_colliderPlayer_12(Collider_t1773347010 * value)
	{
		___colliderPlayer_12 = value;
		Il2CppCodeGenWriteBarrier((&___colliderPlayer_12), value);
	}

	inline static int32_t get_offset_of_fullscreenVideoPlayer_13() { return static_cast<int32_t>(offsetof(ClickMe_t220080192, ___fullscreenVideoPlayer_13)); }
	inline GameObject_t1113636619 * get_fullscreenVideoPlayer_13() const { return ___fullscreenVideoPlayer_13; }
	inline GameObject_t1113636619 ** get_address_of_fullscreenVideoPlayer_13() { return &___fullscreenVideoPlayer_13; }
	inline void set_fullscreenVideoPlayer_13(GameObject_t1113636619 * value)
	{
		___fullscreenVideoPlayer_13 = value;
		Il2CppCodeGenWriteBarrier((&___fullscreenVideoPlayer_13), value);
	}

	inline static int32_t get_offset_of_playerLandscape_14() { return static_cast<int32_t>(offsetof(ClickMe_t220080192, ___playerLandscape_14)); }
	inline GameObject_t1113636619 * get_playerLandscape_14() const { return ___playerLandscape_14; }
	inline GameObject_t1113636619 ** get_address_of_playerLandscape_14() { return &___playerLandscape_14; }
	inline void set_playerLandscape_14(GameObject_t1113636619 * value)
	{
		___playerLandscape_14 = value;
		Il2CppCodeGenWriteBarrier((&___playerLandscape_14), value);
	}

	inline static int32_t get_offset_of_playerPortrait_15() { return static_cast<int32_t>(offsetof(ClickMe_t220080192, ___playerPortrait_15)); }
	inline GameObject_t1113636619 * get_playerPortrait_15() const { return ___playerPortrait_15; }
	inline GameObject_t1113636619 ** get_address_of_playerPortrait_15() { return &___playerPortrait_15; }
	inline void set_playerPortrait_15(GameObject_t1113636619 * value)
	{
		___playerPortrait_15 = value;
		Il2CppCodeGenWriteBarrier((&___playerPortrait_15), value);
	}

	inline static int32_t get_offset_of_landscape_Raw_16() { return static_cast<int32_t>(offsetof(ClickMe_t220080192, ___landscape_Raw_16)); }
	inline RawImage_t3182918964 * get_landscape_Raw_16() const { return ___landscape_Raw_16; }
	inline RawImage_t3182918964 ** get_address_of_landscape_Raw_16() { return &___landscape_Raw_16; }
	inline void set_landscape_Raw_16(RawImage_t3182918964 * value)
	{
		___landscape_Raw_16 = value;
		Il2CppCodeGenWriteBarrier((&___landscape_Raw_16), value);
	}

	inline static int32_t get_offset_of_portrait_Raw_17() { return static_cast<int32_t>(offsetof(ClickMe_t220080192, ___portrait_Raw_17)); }
	inline RawImage_t3182918964 * get_portrait_Raw_17() const { return ___portrait_Raw_17; }
	inline RawImage_t3182918964 ** get_address_of_portrait_Raw_17() { return &___portrait_Raw_17; }
	inline void set_portrait_Raw_17(RawImage_t3182918964 * value)
	{
		___portrait_Raw_17 = value;
		Il2CppCodeGenWriteBarrier((&___portrait_Raw_17), value);
	}

	inline static int32_t get_offset_of_urlVideo_18() { return static_cast<int32_t>(offsetof(ClickMe_t220080192, ___urlVideo_18)); }
	inline String_t* get_urlVideo_18() const { return ___urlVideo_18; }
	inline String_t** get_address_of_urlVideo_18() { return &___urlVideo_18; }
	inline void set_urlVideo_18(String_t* value)
	{
		___urlVideo_18 = value;
		Il2CppCodeGenWriteBarrier((&___urlVideo_18), value);
	}

	inline static int32_t get_offset_of_vidWidth_19() { return static_cast<int32_t>(offsetof(ClickMe_t220080192, ___vidWidth_19)); }
	inline int32_t get_vidWidth_19() const { return ___vidWidth_19; }
	inline int32_t* get_address_of_vidWidth_19() { return &___vidWidth_19; }
	inline void set_vidWidth_19(int32_t value)
	{
		___vidWidth_19 = value;
	}

	inline static int32_t get_offset_of_vidHeight_20() { return static_cast<int32_t>(offsetof(ClickMe_t220080192, ___vidHeight_20)); }
	inline int32_t get_vidHeight_20() const { return ___vidHeight_20; }
	inline int32_t* get_address_of_vidHeight_20() { return &___vidHeight_20; }
	inline void set_vidHeight_20(int32_t value)
	{
		___vidHeight_20 = value;
	}

	inline static int32_t get_offset_of_image_21() { return static_cast<int32_t>(offsetof(ClickMe_t220080192, ___image_21)); }
	inline String_t* get_image_21() const { return ___image_21; }
	inline String_t** get_address_of_image_21() { return &___image_21; }
	inline void set_image_21(String_t* value)
	{
		___image_21 = value;
		Il2CppCodeGenWriteBarrier((&___image_21), value);
	}

	inline static int32_t get_offset_of_playerCtrl3D_22() { return static_cast<int32_t>(offsetof(ClickMe_t220080192, ___playerCtrl3D_22)); }
	inline VideoPlayer_t1683042537 * get_playerCtrl3D_22() const { return ___playerCtrl3D_22; }
	inline VideoPlayer_t1683042537 ** get_address_of_playerCtrl3D_22() { return &___playerCtrl3D_22; }
	inline void set_playerCtrl3D_22(VideoPlayer_t1683042537 * value)
	{
		___playerCtrl3D_22 = value;
		Il2CppCodeGenWriteBarrier((&___playerCtrl3D_22), value);
	}

	inline static int32_t get_offset_of_iconeFS_23() { return static_cast<int32_t>(offsetof(ClickMe_t220080192, ___iconeFS_23)); }
	inline GameObject_t1113636619 * get_iconeFS_23() const { return ___iconeFS_23; }
	inline GameObject_t1113636619 ** get_address_of_iconeFS_23() { return &___iconeFS_23; }
	inline void set_iconeFS_23(GameObject_t1113636619 * value)
	{
		___iconeFS_23 = value;
		Il2CppCodeGenWriteBarrier((&___iconeFS_23), value);
	}

	inline static int32_t get_offset_of_plane_24() { return static_cast<int32_t>(offsetof(ClickMe_t220080192, ___plane_24)); }
	inline GameObject_t1113636619 * get_plane_24() const { return ___plane_24; }
	inline GameObject_t1113636619 ** get_address_of_plane_24() { return &___plane_24; }
	inline void set_plane_24(GameObject_t1113636619 * value)
	{
		___plane_24 = value;
		Il2CppCodeGenWriteBarrier((&___plane_24), value);
	}

	inline static int32_t get_offset_of_button_25() { return static_cast<int32_t>(offsetof(ClickMe_t220080192, ___button_25)); }
	inline GameObject_t1113636619 * get_button_25() const { return ___button_25; }
	inline GameObject_t1113636619 ** get_address_of_button_25() { return &___button_25; }
	inline void set_button_25(GameObject_t1113636619 * value)
	{
		___button_25 = value;
		Il2CppCodeGenWriteBarrier((&___button_25), value);
	}

	inline static int32_t get_offset_of_playerVideo3D_26() { return static_cast<int32_t>(offsetof(ClickMe_t220080192, ___playerVideo3D_26)); }
	inline GameObject_t1113636619 * get_playerVideo3D_26() const { return ___playerVideo3D_26; }
	inline GameObject_t1113636619 ** get_address_of_playerVideo3D_26() { return &___playerVideo3D_26; }
	inline void set_playerVideo3D_26(GameObject_t1113636619 * value)
	{
		___playerVideo3D_26 = value;
		Il2CppCodeGenWriteBarrier((&___playerVideo3D_26), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLICKME_T220080192_H
#ifndef CLICKMECHROMA_T2203948016_H
#define CLICKMECHROMA_T2203948016_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ClickMeChroma
struct  ClickMeChroma_t2203948016  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.RaycastHit ClickMeChroma::hit
	RaycastHit_t1056001966  ___hit_4;
	// UnityEngine.Ray ClickMeChroma::ray
	Ray_t3785851493  ___ray_5;
	// UnityEngine.RaycastHit[] ClickMeChroma::hitAllTemp
	RaycastHitU5BU5D_t1690781147* ___hitAllTemp_6;
	// UnityEngine.Collider ClickMeChroma::collider
	Collider_t1773347010 * ___collider_7;
	// System.String ClickMeChroma::urlVideo
	String_t* ___urlVideo_8;
	// System.Int32 ClickMeChroma::vidWidth
	int32_t ___vidWidth_9;
	// System.Int32 ClickMeChroma::vidHeight
	int32_t ___vidHeight_10;
	// System.String ClickMeChroma::image
	String_t* ___image_11;
	// UnityEngine.GameObject ClickMeChroma::plane
	GameObject_t1113636619 * ___plane_12;
	// UnityEngine.GameObject ClickMeChroma::button
	GameObject_t1113636619 * ___button_13;
	// UnityEngine.GameObject ClickMeChroma::playerContainer
	GameObject_t1113636619 * ___playerContainer_14;
	// UnityEngine.Video.VideoPlayer ClickMeChroma::videoPlayer
	VideoPlayer_t1683042537 * ___videoPlayer_15;
	// UnityEngine.GameObject ClickMeChroma::playerChromakey
	GameObject_t1113636619 * ___playerChromakey_16;
	// System.Boolean ClickMeChroma::requestAutoPlay
	bool ___requestAutoPlay_17;
	// System.Boolean ClickMeChroma::initialized
	bool ___initialized_18;

public:
	inline static int32_t get_offset_of_hit_4() { return static_cast<int32_t>(offsetof(ClickMeChroma_t2203948016, ___hit_4)); }
	inline RaycastHit_t1056001966  get_hit_4() const { return ___hit_4; }
	inline RaycastHit_t1056001966 * get_address_of_hit_4() { return &___hit_4; }
	inline void set_hit_4(RaycastHit_t1056001966  value)
	{
		___hit_4 = value;
	}

	inline static int32_t get_offset_of_ray_5() { return static_cast<int32_t>(offsetof(ClickMeChroma_t2203948016, ___ray_5)); }
	inline Ray_t3785851493  get_ray_5() const { return ___ray_5; }
	inline Ray_t3785851493 * get_address_of_ray_5() { return &___ray_5; }
	inline void set_ray_5(Ray_t3785851493  value)
	{
		___ray_5 = value;
	}

	inline static int32_t get_offset_of_hitAllTemp_6() { return static_cast<int32_t>(offsetof(ClickMeChroma_t2203948016, ___hitAllTemp_6)); }
	inline RaycastHitU5BU5D_t1690781147* get_hitAllTemp_6() const { return ___hitAllTemp_6; }
	inline RaycastHitU5BU5D_t1690781147** get_address_of_hitAllTemp_6() { return &___hitAllTemp_6; }
	inline void set_hitAllTemp_6(RaycastHitU5BU5D_t1690781147* value)
	{
		___hitAllTemp_6 = value;
		Il2CppCodeGenWriteBarrier((&___hitAllTemp_6), value);
	}

	inline static int32_t get_offset_of_collider_7() { return static_cast<int32_t>(offsetof(ClickMeChroma_t2203948016, ___collider_7)); }
	inline Collider_t1773347010 * get_collider_7() const { return ___collider_7; }
	inline Collider_t1773347010 ** get_address_of_collider_7() { return &___collider_7; }
	inline void set_collider_7(Collider_t1773347010 * value)
	{
		___collider_7 = value;
		Il2CppCodeGenWriteBarrier((&___collider_7), value);
	}

	inline static int32_t get_offset_of_urlVideo_8() { return static_cast<int32_t>(offsetof(ClickMeChroma_t2203948016, ___urlVideo_8)); }
	inline String_t* get_urlVideo_8() const { return ___urlVideo_8; }
	inline String_t** get_address_of_urlVideo_8() { return &___urlVideo_8; }
	inline void set_urlVideo_8(String_t* value)
	{
		___urlVideo_8 = value;
		Il2CppCodeGenWriteBarrier((&___urlVideo_8), value);
	}

	inline static int32_t get_offset_of_vidWidth_9() { return static_cast<int32_t>(offsetof(ClickMeChroma_t2203948016, ___vidWidth_9)); }
	inline int32_t get_vidWidth_9() const { return ___vidWidth_9; }
	inline int32_t* get_address_of_vidWidth_9() { return &___vidWidth_9; }
	inline void set_vidWidth_9(int32_t value)
	{
		___vidWidth_9 = value;
	}

	inline static int32_t get_offset_of_vidHeight_10() { return static_cast<int32_t>(offsetof(ClickMeChroma_t2203948016, ___vidHeight_10)); }
	inline int32_t get_vidHeight_10() const { return ___vidHeight_10; }
	inline int32_t* get_address_of_vidHeight_10() { return &___vidHeight_10; }
	inline void set_vidHeight_10(int32_t value)
	{
		___vidHeight_10 = value;
	}

	inline static int32_t get_offset_of_image_11() { return static_cast<int32_t>(offsetof(ClickMeChroma_t2203948016, ___image_11)); }
	inline String_t* get_image_11() const { return ___image_11; }
	inline String_t** get_address_of_image_11() { return &___image_11; }
	inline void set_image_11(String_t* value)
	{
		___image_11 = value;
		Il2CppCodeGenWriteBarrier((&___image_11), value);
	}

	inline static int32_t get_offset_of_plane_12() { return static_cast<int32_t>(offsetof(ClickMeChroma_t2203948016, ___plane_12)); }
	inline GameObject_t1113636619 * get_plane_12() const { return ___plane_12; }
	inline GameObject_t1113636619 ** get_address_of_plane_12() { return &___plane_12; }
	inline void set_plane_12(GameObject_t1113636619 * value)
	{
		___plane_12 = value;
		Il2CppCodeGenWriteBarrier((&___plane_12), value);
	}

	inline static int32_t get_offset_of_button_13() { return static_cast<int32_t>(offsetof(ClickMeChroma_t2203948016, ___button_13)); }
	inline GameObject_t1113636619 * get_button_13() const { return ___button_13; }
	inline GameObject_t1113636619 ** get_address_of_button_13() { return &___button_13; }
	inline void set_button_13(GameObject_t1113636619 * value)
	{
		___button_13 = value;
		Il2CppCodeGenWriteBarrier((&___button_13), value);
	}

	inline static int32_t get_offset_of_playerContainer_14() { return static_cast<int32_t>(offsetof(ClickMeChroma_t2203948016, ___playerContainer_14)); }
	inline GameObject_t1113636619 * get_playerContainer_14() const { return ___playerContainer_14; }
	inline GameObject_t1113636619 ** get_address_of_playerContainer_14() { return &___playerContainer_14; }
	inline void set_playerContainer_14(GameObject_t1113636619 * value)
	{
		___playerContainer_14 = value;
		Il2CppCodeGenWriteBarrier((&___playerContainer_14), value);
	}

	inline static int32_t get_offset_of_videoPlayer_15() { return static_cast<int32_t>(offsetof(ClickMeChroma_t2203948016, ___videoPlayer_15)); }
	inline VideoPlayer_t1683042537 * get_videoPlayer_15() const { return ___videoPlayer_15; }
	inline VideoPlayer_t1683042537 ** get_address_of_videoPlayer_15() { return &___videoPlayer_15; }
	inline void set_videoPlayer_15(VideoPlayer_t1683042537 * value)
	{
		___videoPlayer_15 = value;
		Il2CppCodeGenWriteBarrier((&___videoPlayer_15), value);
	}

	inline static int32_t get_offset_of_playerChromakey_16() { return static_cast<int32_t>(offsetof(ClickMeChroma_t2203948016, ___playerChromakey_16)); }
	inline GameObject_t1113636619 * get_playerChromakey_16() const { return ___playerChromakey_16; }
	inline GameObject_t1113636619 ** get_address_of_playerChromakey_16() { return &___playerChromakey_16; }
	inline void set_playerChromakey_16(GameObject_t1113636619 * value)
	{
		___playerChromakey_16 = value;
		Il2CppCodeGenWriteBarrier((&___playerChromakey_16), value);
	}

	inline static int32_t get_offset_of_requestAutoPlay_17() { return static_cast<int32_t>(offsetof(ClickMeChroma_t2203948016, ___requestAutoPlay_17)); }
	inline bool get_requestAutoPlay_17() const { return ___requestAutoPlay_17; }
	inline bool* get_address_of_requestAutoPlay_17() { return &___requestAutoPlay_17; }
	inline void set_requestAutoPlay_17(bool value)
	{
		___requestAutoPlay_17 = value;
	}

	inline static int32_t get_offset_of_initialized_18() { return static_cast<int32_t>(offsetof(ClickMeChroma_t2203948016, ___initialized_18)); }
	inline bool get_initialized_18() const { return ___initialized_18; }
	inline bool* get_address_of_initialized_18() { return &___initialized_18; }
	inline void set_initialized_18(bool value)
	{
		___initialized_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLICKMECHROMA_T2203948016_H
#ifndef CLICKVIDEO_T284620550_H
#define CLICKVIDEO_T284620550_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ClickVideo
struct  ClickVideo_t284620550  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean ClickVideo::isMarkedForDistroy
	bool ___isMarkedForDistroy_4;
	// System.Boolean ClickVideo::initialized
	bool ___initialized_5;
	// UnityEngine.RaycastHit ClickVideo::hit
	RaycastHit_t1056001966  ___hit_6;
	// UnityEngine.Ray ClickVideo::ray
	Ray_t3785851493  ___ray_7;
	// UnityEngine.RaycastHit[] ClickVideo::hitAllTemp
	RaycastHitU5BU5D_t1690781147* ___hitAllTemp_8;
	// UnityEngine.UI.RawImage ClickVideo::imgTest
	RawImage_t3182918964 * ___imgTest_9;
	// UnityEngine.Collider ClickVideo::colliderFullScreen
	Collider_t1773347010 * ___colliderFullScreen_10;
	// UnityEngine.Collider ClickVideo::colliderPlay
	Collider_t1773347010 * ___colliderPlay_11;
	// UnityEngine.Collider ClickVideo::colliderPause
	Collider_t1773347010 * ___colliderPause_12;
	// UnityEngine.Collider ClickVideo::colliderSoundOn
	Collider_t1773347010 * ___colliderSoundOn_13;
	// UnityEngine.Collider ClickVideo::colliderSoundOff
	Collider_t1773347010 * ___colliderSoundOff_14;
	// UnityEngine.Collider ClickVideo::colliderPlayer
	Collider_t1773347010 * ___colliderPlayer_15;
	// UnityEngine.GameObject ClickVideo::fullscreenVideoPlayer
	GameObject_t1113636619 * ___fullscreenVideoPlayer_16;
	// UnityEngine.GameObject ClickVideo::playerLandscape
	GameObject_t1113636619 * ___playerLandscape_17;
	// UnityEngine.GameObject ClickVideo::playerPortrait
	GameObject_t1113636619 * ___playerPortrait_18;
	// UnityEngine.UI.RawImage ClickVideo::landscape_Raw
	RawImage_t3182918964 * ___landscape_Raw_19;
	// UnityEngine.UI.RawImage ClickVideo::portrait_Raw
	RawImage_t3182918964 * ___portrait_Raw_20;
	// System.String ClickVideo::urlVideo
	String_t* ___urlVideo_21;
	// System.Int32 ClickVideo::vidWidth
	int32_t ___vidWidth_22;
	// System.Int32 ClickVideo::vidHeight
	int32_t ___vidHeight_23;
	// System.String ClickVideo::image
	String_t* ___image_24;
	// UnityEngine.Video.VideoPlayer ClickVideo::playerCtrl3D
	VideoPlayer_t1683042537 * ___playerCtrl3D_25;
	// UnityEngine.GameObject ClickVideo::iconeFS
	GameObject_t1113636619 * ___iconeFS_26;
	// UnityEngine.GameObject ClickVideo::plane
	GameObject_t1113636619 * ___plane_27;
	// UnityEngine.GameObject ClickVideo::button
	GameObject_t1113636619 * ___button_28;
	// UnityEngine.GameObject ClickVideo::playerVideo3D
	GameObject_t1113636619 * ___playerVideo3D_29;
	// FullscreenVideoPlayer ClickVideo::fsvp
	FullscreenVideoPlayer_t1619346304 * ___fsvp_30;
	// UnityEngine.GameObject ClickVideo::GO_playerContainer
	GameObject_t1113636619 * ___GO_playerContainer_31;
	// System.Boolean ClickVideo::requestAutoPlay
	bool ___requestAutoPlay_32;

public:
	inline static int32_t get_offset_of_isMarkedForDistroy_4() { return static_cast<int32_t>(offsetof(ClickVideo_t284620550, ___isMarkedForDistroy_4)); }
	inline bool get_isMarkedForDistroy_4() const { return ___isMarkedForDistroy_4; }
	inline bool* get_address_of_isMarkedForDistroy_4() { return &___isMarkedForDistroy_4; }
	inline void set_isMarkedForDistroy_4(bool value)
	{
		___isMarkedForDistroy_4 = value;
	}

	inline static int32_t get_offset_of_initialized_5() { return static_cast<int32_t>(offsetof(ClickVideo_t284620550, ___initialized_5)); }
	inline bool get_initialized_5() const { return ___initialized_5; }
	inline bool* get_address_of_initialized_5() { return &___initialized_5; }
	inline void set_initialized_5(bool value)
	{
		___initialized_5 = value;
	}

	inline static int32_t get_offset_of_hit_6() { return static_cast<int32_t>(offsetof(ClickVideo_t284620550, ___hit_6)); }
	inline RaycastHit_t1056001966  get_hit_6() const { return ___hit_6; }
	inline RaycastHit_t1056001966 * get_address_of_hit_6() { return &___hit_6; }
	inline void set_hit_6(RaycastHit_t1056001966  value)
	{
		___hit_6 = value;
	}

	inline static int32_t get_offset_of_ray_7() { return static_cast<int32_t>(offsetof(ClickVideo_t284620550, ___ray_7)); }
	inline Ray_t3785851493  get_ray_7() const { return ___ray_7; }
	inline Ray_t3785851493 * get_address_of_ray_7() { return &___ray_7; }
	inline void set_ray_7(Ray_t3785851493  value)
	{
		___ray_7 = value;
	}

	inline static int32_t get_offset_of_hitAllTemp_8() { return static_cast<int32_t>(offsetof(ClickVideo_t284620550, ___hitAllTemp_8)); }
	inline RaycastHitU5BU5D_t1690781147* get_hitAllTemp_8() const { return ___hitAllTemp_8; }
	inline RaycastHitU5BU5D_t1690781147** get_address_of_hitAllTemp_8() { return &___hitAllTemp_8; }
	inline void set_hitAllTemp_8(RaycastHitU5BU5D_t1690781147* value)
	{
		___hitAllTemp_8 = value;
		Il2CppCodeGenWriteBarrier((&___hitAllTemp_8), value);
	}

	inline static int32_t get_offset_of_imgTest_9() { return static_cast<int32_t>(offsetof(ClickVideo_t284620550, ___imgTest_9)); }
	inline RawImage_t3182918964 * get_imgTest_9() const { return ___imgTest_9; }
	inline RawImage_t3182918964 ** get_address_of_imgTest_9() { return &___imgTest_9; }
	inline void set_imgTest_9(RawImage_t3182918964 * value)
	{
		___imgTest_9 = value;
		Il2CppCodeGenWriteBarrier((&___imgTest_9), value);
	}

	inline static int32_t get_offset_of_colliderFullScreen_10() { return static_cast<int32_t>(offsetof(ClickVideo_t284620550, ___colliderFullScreen_10)); }
	inline Collider_t1773347010 * get_colliderFullScreen_10() const { return ___colliderFullScreen_10; }
	inline Collider_t1773347010 ** get_address_of_colliderFullScreen_10() { return &___colliderFullScreen_10; }
	inline void set_colliderFullScreen_10(Collider_t1773347010 * value)
	{
		___colliderFullScreen_10 = value;
		Il2CppCodeGenWriteBarrier((&___colliderFullScreen_10), value);
	}

	inline static int32_t get_offset_of_colliderPlay_11() { return static_cast<int32_t>(offsetof(ClickVideo_t284620550, ___colliderPlay_11)); }
	inline Collider_t1773347010 * get_colliderPlay_11() const { return ___colliderPlay_11; }
	inline Collider_t1773347010 ** get_address_of_colliderPlay_11() { return &___colliderPlay_11; }
	inline void set_colliderPlay_11(Collider_t1773347010 * value)
	{
		___colliderPlay_11 = value;
		Il2CppCodeGenWriteBarrier((&___colliderPlay_11), value);
	}

	inline static int32_t get_offset_of_colliderPause_12() { return static_cast<int32_t>(offsetof(ClickVideo_t284620550, ___colliderPause_12)); }
	inline Collider_t1773347010 * get_colliderPause_12() const { return ___colliderPause_12; }
	inline Collider_t1773347010 ** get_address_of_colliderPause_12() { return &___colliderPause_12; }
	inline void set_colliderPause_12(Collider_t1773347010 * value)
	{
		___colliderPause_12 = value;
		Il2CppCodeGenWriteBarrier((&___colliderPause_12), value);
	}

	inline static int32_t get_offset_of_colliderSoundOn_13() { return static_cast<int32_t>(offsetof(ClickVideo_t284620550, ___colliderSoundOn_13)); }
	inline Collider_t1773347010 * get_colliderSoundOn_13() const { return ___colliderSoundOn_13; }
	inline Collider_t1773347010 ** get_address_of_colliderSoundOn_13() { return &___colliderSoundOn_13; }
	inline void set_colliderSoundOn_13(Collider_t1773347010 * value)
	{
		___colliderSoundOn_13 = value;
		Il2CppCodeGenWriteBarrier((&___colliderSoundOn_13), value);
	}

	inline static int32_t get_offset_of_colliderSoundOff_14() { return static_cast<int32_t>(offsetof(ClickVideo_t284620550, ___colliderSoundOff_14)); }
	inline Collider_t1773347010 * get_colliderSoundOff_14() const { return ___colliderSoundOff_14; }
	inline Collider_t1773347010 ** get_address_of_colliderSoundOff_14() { return &___colliderSoundOff_14; }
	inline void set_colliderSoundOff_14(Collider_t1773347010 * value)
	{
		___colliderSoundOff_14 = value;
		Il2CppCodeGenWriteBarrier((&___colliderSoundOff_14), value);
	}

	inline static int32_t get_offset_of_colliderPlayer_15() { return static_cast<int32_t>(offsetof(ClickVideo_t284620550, ___colliderPlayer_15)); }
	inline Collider_t1773347010 * get_colliderPlayer_15() const { return ___colliderPlayer_15; }
	inline Collider_t1773347010 ** get_address_of_colliderPlayer_15() { return &___colliderPlayer_15; }
	inline void set_colliderPlayer_15(Collider_t1773347010 * value)
	{
		___colliderPlayer_15 = value;
		Il2CppCodeGenWriteBarrier((&___colliderPlayer_15), value);
	}

	inline static int32_t get_offset_of_fullscreenVideoPlayer_16() { return static_cast<int32_t>(offsetof(ClickVideo_t284620550, ___fullscreenVideoPlayer_16)); }
	inline GameObject_t1113636619 * get_fullscreenVideoPlayer_16() const { return ___fullscreenVideoPlayer_16; }
	inline GameObject_t1113636619 ** get_address_of_fullscreenVideoPlayer_16() { return &___fullscreenVideoPlayer_16; }
	inline void set_fullscreenVideoPlayer_16(GameObject_t1113636619 * value)
	{
		___fullscreenVideoPlayer_16 = value;
		Il2CppCodeGenWriteBarrier((&___fullscreenVideoPlayer_16), value);
	}

	inline static int32_t get_offset_of_playerLandscape_17() { return static_cast<int32_t>(offsetof(ClickVideo_t284620550, ___playerLandscape_17)); }
	inline GameObject_t1113636619 * get_playerLandscape_17() const { return ___playerLandscape_17; }
	inline GameObject_t1113636619 ** get_address_of_playerLandscape_17() { return &___playerLandscape_17; }
	inline void set_playerLandscape_17(GameObject_t1113636619 * value)
	{
		___playerLandscape_17 = value;
		Il2CppCodeGenWriteBarrier((&___playerLandscape_17), value);
	}

	inline static int32_t get_offset_of_playerPortrait_18() { return static_cast<int32_t>(offsetof(ClickVideo_t284620550, ___playerPortrait_18)); }
	inline GameObject_t1113636619 * get_playerPortrait_18() const { return ___playerPortrait_18; }
	inline GameObject_t1113636619 ** get_address_of_playerPortrait_18() { return &___playerPortrait_18; }
	inline void set_playerPortrait_18(GameObject_t1113636619 * value)
	{
		___playerPortrait_18 = value;
		Il2CppCodeGenWriteBarrier((&___playerPortrait_18), value);
	}

	inline static int32_t get_offset_of_landscape_Raw_19() { return static_cast<int32_t>(offsetof(ClickVideo_t284620550, ___landscape_Raw_19)); }
	inline RawImage_t3182918964 * get_landscape_Raw_19() const { return ___landscape_Raw_19; }
	inline RawImage_t3182918964 ** get_address_of_landscape_Raw_19() { return &___landscape_Raw_19; }
	inline void set_landscape_Raw_19(RawImage_t3182918964 * value)
	{
		___landscape_Raw_19 = value;
		Il2CppCodeGenWriteBarrier((&___landscape_Raw_19), value);
	}

	inline static int32_t get_offset_of_portrait_Raw_20() { return static_cast<int32_t>(offsetof(ClickVideo_t284620550, ___portrait_Raw_20)); }
	inline RawImage_t3182918964 * get_portrait_Raw_20() const { return ___portrait_Raw_20; }
	inline RawImage_t3182918964 ** get_address_of_portrait_Raw_20() { return &___portrait_Raw_20; }
	inline void set_portrait_Raw_20(RawImage_t3182918964 * value)
	{
		___portrait_Raw_20 = value;
		Il2CppCodeGenWriteBarrier((&___portrait_Raw_20), value);
	}

	inline static int32_t get_offset_of_urlVideo_21() { return static_cast<int32_t>(offsetof(ClickVideo_t284620550, ___urlVideo_21)); }
	inline String_t* get_urlVideo_21() const { return ___urlVideo_21; }
	inline String_t** get_address_of_urlVideo_21() { return &___urlVideo_21; }
	inline void set_urlVideo_21(String_t* value)
	{
		___urlVideo_21 = value;
		Il2CppCodeGenWriteBarrier((&___urlVideo_21), value);
	}

	inline static int32_t get_offset_of_vidWidth_22() { return static_cast<int32_t>(offsetof(ClickVideo_t284620550, ___vidWidth_22)); }
	inline int32_t get_vidWidth_22() const { return ___vidWidth_22; }
	inline int32_t* get_address_of_vidWidth_22() { return &___vidWidth_22; }
	inline void set_vidWidth_22(int32_t value)
	{
		___vidWidth_22 = value;
	}

	inline static int32_t get_offset_of_vidHeight_23() { return static_cast<int32_t>(offsetof(ClickVideo_t284620550, ___vidHeight_23)); }
	inline int32_t get_vidHeight_23() const { return ___vidHeight_23; }
	inline int32_t* get_address_of_vidHeight_23() { return &___vidHeight_23; }
	inline void set_vidHeight_23(int32_t value)
	{
		___vidHeight_23 = value;
	}

	inline static int32_t get_offset_of_image_24() { return static_cast<int32_t>(offsetof(ClickVideo_t284620550, ___image_24)); }
	inline String_t* get_image_24() const { return ___image_24; }
	inline String_t** get_address_of_image_24() { return &___image_24; }
	inline void set_image_24(String_t* value)
	{
		___image_24 = value;
		Il2CppCodeGenWriteBarrier((&___image_24), value);
	}

	inline static int32_t get_offset_of_playerCtrl3D_25() { return static_cast<int32_t>(offsetof(ClickVideo_t284620550, ___playerCtrl3D_25)); }
	inline VideoPlayer_t1683042537 * get_playerCtrl3D_25() const { return ___playerCtrl3D_25; }
	inline VideoPlayer_t1683042537 ** get_address_of_playerCtrl3D_25() { return &___playerCtrl3D_25; }
	inline void set_playerCtrl3D_25(VideoPlayer_t1683042537 * value)
	{
		___playerCtrl3D_25 = value;
		Il2CppCodeGenWriteBarrier((&___playerCtrl3D_25), value);
	}

	inline static int32_t get_offset_of_iconeFS_26() { return static_cast<int32_t>(offsetof(ClickVideo_t284620550, ___iconeFS_26)); }
	inline GameObject_t1113636619 * get_iconeFS_26() const { return ___iconeFS_26; }
	inline GameObject_t1113636619 ** get_address_of_iconeFS_26() { return &___iconeFS_26; }
	inline void set_iconeFS_26(GameObject_t1113636619 * value)
	{
		___iconeFS_26 = value;
		Il2CppCodeGenWriteBarrier((&___iconeFS_26), value);
	}

	inline static int32_t get_offset_of_plane_27() { return static_cast<int32_t>(offsetof(ClickVideo_t284620550, ___plane_27)); }
	inline GameObject_t1113636619 * get_plane_27() const { return ___plane_27; }
	inline GameObject_t1113636619 ** get_address_of_plane_27() { return &___plane_27; }
	inline void set_plane_27(GameObject_t1113636619 * value)
	{
		___plane_27 = value;
		Il2CppCodeGenWriteBarrier((&___plane_27), value);
	}

	inline static int32_t get_offset_of_button_28() { return static_cast<int32_t>(offsetof(ClickVideo_t284620550, ___button_28)); }
	inline GameObject_t1113636619 * get_button_28() const { return ___button_28; }
	inline GameObject_t1113636619 ** get_address_of_button_28() { return &___button_28; }
	inline void set_button_28(GameObject_t1113636619 * value)
	{
		___button_28 = value;
		Il2CppCodeGenWriteBarrier((&___button_28), value);
	}

	inline static int32_t get_offset_of_playerVideo3D_29() { return static_cast<int32_t>(offsetof(ClickVideo_t284620550, ___playerVideo3D_29)); }
	inline GameObject_t1113636619 * get_playerVideo3D_29() const { return ___playerVideo3D_29; }
	inline GameObject_t1113636619 ** get_address_of_playerVideo3D_29() { return &___playerVideo3D_29; }
	inline void set_playerVideo3D_29(GameObject_t1113636619 * value)
	{
		___playerVideo3D_29 = value;
		Il2CppCodeGenWriteBarrier((&___playerVideo3D_29), value);
	}

	inline static int32_t get_offset_of_fsvp_30() { return static_cast<int32_t>(offsetof(ClickVideo_t284620550, ___fsvp_30)); }
	inline FullscreenVideoPlayer_t1619346304 * get_fsvp_30() const { return ___fsvp_30; }
	inline FullscreenVideoPlayer_t1619346304 ** get_address_of_fsvp_30() { return &___fsvp_30; }
	inline void set_fsvp_30(FullscreenVideoPlayer_t1619346304 * value)
	{
		___fsvp_30 = value;
		Il2CppCodeGenWriteBarrier((&___fsvp_30), value);
	}

	inline static int32_t get_offset_of_GO_playerContainer_31() { return static_cast<int32_t>(offsetof(ClickVideo_t284620550, ___GO_playerContainer_31)); }
	inline GameObject_t1113636619 * get_GO_playerContainer_31() const { return ___GO_playerContainer_31; }
	inline GameObject_t1113636619 ** get_address_of_GO_playerContainer_31() { return &___GO_playerContainer_31; }
	inline void set_GO_playerContainer_31(GameObject_t1113636619 * value)
	{
		___GO_playerContainer_31 = value;
		Il2CppCodeGenWriteBarrier((&___GO_playerContainer_31), value);
	}

	inline static int32_t get_offset_of_requestAutoPlay_32() { return static_cast<int32_t>(offsetof(ClickVideo_t284620550, ___requestAutoPlay_32)); }
	inline bool get_requestAutoPlay_32() const { return ___requestAutoPlay_32; }
	inline bool* get_address_of_requestAutoPlay_32() { return &___requestAutoPlay_32; }
	inline void set_requestAutoPlay_32(bool value)
	{
		___requestAutoPlay_32 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLICKVIDEO_T284620550_H
#ifndef CLOUDRECOTRACKABLEEVENTHANDLER_T3792508927_H
#define CLOUDRECOTRACKABLEEVENTHANDLER_T3792508927_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CloudRecoTrackableEventHandler
struct  CloudRecoTrackableEventHandler_t3792508927  : public MonoBehaviour_t3962482529
{
public:
	// ScanLine CloudRecoTrackableEventHandler::scanLine
	ScanLine_t269422218 * ___scanLine_4;
	// ContentManager2 CloudRecoTrackableEventHandler::contentManager
	ContentManager2_t2980555998 * ___contentManager_5;
	// Vuforia.TrackableBehaviour CloudRecoTrackableEventHandler::mTrackableBehaviour
	TrackableBehaviour_t1113559212 * ___mTrackableBehaviour_6;
	// System.Boolean CloudRecoTrackableEventHandler::isTracking
	bool ___isTracking_7;

public:
	inline static int32_t get_offset_of_scanLine_4() { return static_cast<int32_t>(offsetof(CloudRecoTrackableEventHandler_t3792508927, ___scanLine_4)); }
	inline ScanLine_t269422218 * get_scanLine_4() const { return ___scanLine_4; }
	inline ScanLine_t269422218 ** get_address_of_scanLine_4() { return &___scanLine_4; }
	inline void set_scanLine_4(ScanLine_t269422218 * value)
	{
		___scanLine_4 = value;
		Il2CppCodeGenWriteBarrier((&___scanLine_4), value);
	}

	inline static int32_t get_offset_of_contentManager_5() { return static_cast<int32_t>(offsetof(CloudRecoTrackableEventHandler_t3792508927, ___contentManager_5)); }
	inline ContentManager2_t2980555998 * get_contentManager_5() const { return ___contentManager_5; }
	inline ContentManager2_t2980555998 ** get_address_of_contentManager_5() { return &___contentManager_5; }
	inline void set_contentManager_5(ContentManager2_t2980555998 * value)
	{
		___contentManager_5 = value;
		Il2CppCodeGenWriteBarrier((&___contentManager_5), value);
	}

	inline static int32_t get_offset_of_mTrackableBehaviour_6() { return static_cast<int32_t>(offsetof(CloudRecoTrackableEventHandler_t3792508927, ___mTrackableBehaviour_6)); }
	inline TrackableBehaviour_t1113559212 * get_mTrackableBehaviour_6() const { return ___mTrackableBehaviour_6; }
	inline TrackableBehaviour_t1113559212 ** get_address_of_mTrackableBehaviour_6() { return &___mTrackableBehaviour_6; }
	inline void set_mTrackableBehaviour_6(TrackableBehaviour_t1113559212 * value)
	{
		___mTrackableBehaviour_6 = value;
		Il2CppCodeGenWriteBarrier((&___mTrackableBehaviour_6), value);
	}

	inline static int32_t get_offset_of_isTracking_7() { return static_cast<int32_t>(offsetof(CloudRecoTrackableEventHandler_t3792508927, ___isTracking_7)); }
	inline bool get_isTracking_7() const { return ___isTracking_7; }
	inline bool* get_address_of_isTracking_7() { return &___isTracking_7; }
	inline void set_isTracking_7(bool value)
	{
		___isTracking_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLOUDRECOTRACKABLEEVENTHANDLER_T3792508927_H
#ifndef CONTENTMANAGER2_T2980555998_H
#define CONTENTMANAGER2_T2980555998_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ContentManager2
struct  ContentManager2_t2980555998  : public MonoBehaviour_t3962482529
{
public:
	// captureAndSendScreen ContentManager2::captureSend
	captureAndSendScreen_t3196082656 * ___captureSend_5;
	// UnityEngine.GameObject ContentManager2::AugmentationObject
	GameObject_t1113636619 * ___AugmentationObject_6;
	// AnimationsManager ContentManager2::AnimationsManager
	AnimationsManager_t2281084567 * ___AnimationsManager_7;
	// UnityEngine.GameObject ContentManager2::calendarWidget
	GameObject_t1113636619 * ___calendarWidget_8;
	// System.Boolean ContentManager2::isMarkerShown
	bool ___isMarkerShown_9;
	// System.String ContentManager2::UserGUID
	String_t* ___UserGUID_10;
	// System.String ContentManager2::appName
	String_t* ___appName_11;
	// UnityEngine.GameObject ContentManager2::markerWidget
	GameObject_t1113636619 * ___markerWidget_12;
	// UnityEngine.GameObject ContentManager2::actionWidget
	GameObject_t1113636619 * ___actionWidget_13;
	// UnityEngine.GameObject ContentManager2::jeuWidget
	GameObject_t1113636619 * ___jeuWidget_14;
	// UnityEngine.GameObject ContentManager2::imageWidget
	GameObject_t1113636619 * ___imageWidget_15;
	// UnityEngine.GameObject ContentManager2::videoWidget
	GameObject_t1113636619 * ___videoWidget_16;
	// UnityEngine.GameObject ContentManager2::audioWidget
	GameObject_t1113636619 * ___audioWidget_17;
	// UnityEngine.GameObject ContentManager2::chromaKeyWidget
	GameObject_t1113636619 * ___chromaKeyWidget_18;
	// UnityEngine.GameObject ContentManager2::videoPlayerPortrait
	GameObject_t1113636619 * ___videoPlayerPortrait_19;
	// UnityEngine.UI.RawImage ContentManager2::videoPlayerPortrait_RAW
	RawImage_t3182918964 * ___videoPlayerPortrait_RAW_20;
	// UnityEngine.GameObject ContentManager2::videoPlayerLandscape
	GameObject_t1113636619 * ___videoPlayerLandscape_21;
	// UnityEngine.UI.RawImage ContentManager2::videoPlayerLandscape_RAW
	RawImage_t3182918964 * ___videoPlayerLandscape_RAW_22;
	// UnityEngine.GameObject ContentManager2::fullscreenVideoPlayer
	GameObject_t1113636619 * ___fullscreenVideoPlayer_23;
	// UnityEngine.GameObject ContentManager2::objectWidget
	GameObject_t1113636619 * ___objectWidget_24;
	// UnityEngine.UI.RawImage ContentManager2::rawImgPortrait
	RawImage_t3182918964 * ___rawImgPortrait_25;
	// UnityEngine.UI.RawImage ContentManager2::rawImgLandscape
	RawImage_t3182918964 * ___rawImgLandscape_26;
	// UnityEngine.GameObject ContentManager2::panelBack
	GameObject_t1113636619 * ___panelBack_27;
	// UnityEngine.GameObject ContentManager2::CameraOBJContainer
	GameObject_t1113636619 * ___CameraOBJContainer_28;
	// UnityEngine.GameObject ContentManager2::FS_OBJ_container
	GameObject_t1113636619 * ___FS_OBJ_container_29;
	// UnityEngine.GameObject ContentManager2::closeButton
	GameObject_t1113636619 * ___closeButton_30;
	// UnityEngine.GameObject ContentManager2::backButtonBackMarker
	GameObject_t1113636619 * ___backButtonBackMarker_31;
	// UnityEngine.GameObject ContentManager2::backMarkerON
	GameObject_t1113636619 * ___backMarkerON_32;
	// UnityEngine.GameObject ContentManager2::backMarkerOFF
	GameObject_t1113636619 * ___backMarkerOFF_33;
	// System.Boolean ContentManager2::isShowingBackMarker
	bool ___isShowingBackMarker_34;
	// UnityEngine.GameObject ContentManager2::sphereCloseUp
	GameObject_t1113636619 * ___sphereCloseUp_35;
	// UnityEngine.GameObject ContentManager2::cubeEtalon
	GameObject_t1113636619 * ___cubeEtalon_36;
	// UnityEngine.GameObject ContentManager2::closeMarkerButton
	GameObject_t1113636619 * ___closeMarkerButton_37;
	// UnityEngine.UI.Slider ContentManager2::sliderZoom
	Slider_t3903728902 * ___sliderZoom_38;
	// UnityEngine.GameObject ContentManager2::cancelButton
	GameObject_t1113636619 * ___cancelButton_39;
	// UnityEngine.GameObject ContentManager2::fullscreen3DObject
	GameObject_t1113636619 * ___fullscreen3DObject_40;
	// UnityEngine.GameObject ContentManager2::emailPanel
	GameObject_t1113636619 * ___emailPanel_41;
	// TMPro.TextMeshProUGUI ContentManager2::emailPanel_mail
	TextMeshProUGUI_t529313277 * ___emailPanel_mail_42;
	// TMPro.TextMeshProUGUI ContentManager2::emailPanel_text
	TextMeshProUGUI_t529313277 * ___emailPanel_text_43;
	// UnityEngine.UI.Button ContentManager2::emailPanel_button
	Button_t4055032469 * ___emailPanel_button_44;
	// System.String ContentManager2::screenShotURL
	String_t* ___screenShotURL_45;
	// UnityEngine.GameObject ContentManager2::nodeTex
	GameObject_t1113636619 * ___nodeTex_46;
	// Vuforia.Image/PIXEL_FORMAT ContentManager2::mPixelFormat
	int32_t ___mPixelFormat_47;
	// System.Boolean ContentManager2::mFormatRegistered
	bool ___mFormatRegistered_48;
	// Vuforia.CloudRecoBehaviour ContentManager2::mCloudRecoBehaviour
	CloudRecoBehaviour_t431762792 * ___mCloudRecoBehaviour_49;
	// Vuforia.TrackableBehaviour ContentManager2::mTrackableBehaviour
	TrackableBehaviour_t1113559212 * ___mTrackableBehaviour_50;
	// System.Boolean ContentManager2::mIsShowingMarkerData
	bool ___mIsShowingMarkerData_51;
	// System.String ContentManager2::currentMarker
	String_t* ___currentMarker_52;
	// System.Boolean ContentManager2::isTracking3D
	bool ___isTracking3D_53;
	// UnityEngine.GameObject ContentManager2::pano_regles
	GameObject_t1113636619 * ___pano_regles_54;
	// UnityEngine.GameObject ContentManager2::pano_data
	GameObject_t1113636619 * ___pano_data_55;
	// UnityEngine.GameObject ContentManager2::pano_error
	GameObject_t1113636619 * ___pano_error_56;
	// UnityEngine.GameObject ContentManager2::pano_gagne
	GameObject_t1113636619 * ___pano_gagne_57;

public:
	inline static int32_t get_offset_of_captureSend_5() { return static_cast<int32_t>(offsetof(ContentManager2_t2980555998, ___captureSend_5)); }
	inline captureAndSendScreen_t3196082656 * get_captureSend_5() const { return ___captureSend_5; }
	inline captureAndSendScreen_t3196082656 ** get_address_of_captureSend_5() { return &___captureSend_5; }
	inline void set_captureSend_5(captureAndSendScreen_t3196082656 * value)
	{
		___captureSend_5 = value;
		Il2CppCodeGenWriteBarrier((&___captureSend_5), value);
	}

	inline static int32_t get_offset_of_AugmentationObject_6() { return static_cast<int32_t>(offsetof(ContentManager2_t2980555998, ___AugmentationObject_6)); }
	inline GameObject_t1113636619 * get_AugmentationObject_6() const { return ___AugmentationObject_6; }
	inline GameObject_t1113636619 ** get_address_of_AugmentationObject_6() { return &___AugmentationObject_6; }
	inline void set_AugmentationObject_6(GameObject_t1113636619 * value)
	{
		___AugmentationObject_6 = value;
		Il2CppCodeGenWriteBarrier((&___AugmentationObject_6), value);
	}

	inline static int32_t get_offset_of_AnimationsManager_7() { return static_cast<int32_t>(offsetof(ContentManager2_t2980555998, ___AnimationsManager_7)); }
	inline AnimationsManager_t2281084567 * get_AnimationsManager_7() const { return ___AnimationsManager_7; }
	inline AnimationsManager_t2281084567 ** get_address_of_AnimationsManager_7() { return &___AnimationsManager_7; }
	inline void set_AnimationsManager_7(AnimationsManager_t2281084567 * value)
	{
		___AnimationsManager_7 = value;
		Il2CppCodeGenWriteBarrier((&___AnimationsManager_7), value);
	}

	inline static int32_t get_offset_of_calendarWidget_8() { return static_cast<int32_t>(offsetof(ContentManager2_t2980555998, ___calendarWidget_8)); }
	inline GameObject_t1113636619 * get_calendarWidget_8() const { return ___calendarWidget_8; }
	inline GameObject_t1113636619 ** get_address_of_calendarWidget_8() { return &___calendarWidget_8; }
	inline void set_calendarWidget_8(GameObject_t1113636619 * value)
	{
		___calendarWidget_8 = value;
		Il2CppCodeGenWriteBarrier((&___calendarWidget_8), value);
	}

	inline static int32_t get_offset_of_isMarkerShown_9() { return static_cast<int32_t>(offsetof(ContentManager2_t2980555998, ___isMarkerShown_9)); }
	inline bool get_isMarkerShown_9() const { return ___isMarkerShown_9; }
	inline bool* get_address_of_isMarkerShown_9() { return &___isMarkerShown_9; }
	inline void set_isMarkerShown_9(bool value)
	{
		___isMarkerShown_9 = value;
	}

	inline static int32_t get_offset_of_UserGUID_10() { return static_cast<int32_t>(offsetof(ContentManager2_t2980555998, ___UserGUID_10)); }
	inline String_t* get_UserGUID_10() const { return ___UserGUID_10; }
	inline String_t** get_address_of_UserGUID_10() { return &___UserGUID_10; }
	inline void set_UserGUID_10(String_t* value)
	{
		___UserGUID_10 = value;
		Il2CppCodeGenWriteBarrier((&___UserGUID_10), value);
	}

	inline static int32_t get_offset_of_appName_11() { return static_cast<int32_t>(offsetof(ContentManager2_t2980555998, ___appName_11)); }
	inline String_t* get_appName_11() const { return ___appName_11; }
	inline String_t** get_address_of_appName_11() { return &___appName_11; }
	inline void set_appName_11(String_t* value)
	{
		___appName_11 = value;
		Il2CppCodeGenWriteBarrier((&___appName_11), value);
	}

	inline static int32_t get_offset_of_markerWidget_12() { return static_cast<int32_t>(offsetof(ContentManager2_t2980555998, ___markerWidget_12)); }
	inline GameObject_t1113636619 * get_markerWidget_12() const { return ___markerWidget_12; }
	inline GameObject_t1113636619 ** get_address_of_markerWidget_12() { return &___markerWidget_12; }
	inline void set_markerWidget_12(GameObject_t1113636619 * value)
	{
		___markerWidget_12 = value;
		Il2CppCodeGenWriteBarrier((&___markerWidget_12), value);
	}

	inline static int32_t get_offset_of_actionWidget_13() { return static_cast<int32_t>(offsetof(ContentManager2_t2980555998, ___actionWidget_13)); }
	inline GameObject_t1113636619 * get_actionWidget_13() const { return ___actionWidget_13; }
	inline GameObject_t1113636619 ** get_address_of_actionWidget_13() { return &___actionWidget_13; }
	inline void set_actionWidget_13(GameObject_t1113636619 * value)
	{
		___actionWidget_13 = value;
		Il2CppCodeGenWriteBarrier((&___actionWidget_13), value);
	}

	inline static int32_t get_offset_of_jeuWidget_14() { return static_cast<int32_t>(offsetof(ContentManager2_t2980555998, ___jeuWidget_14)); }
	inline GameObject_t1113636619 * get_jeuWidget_14() const { return ___jeuWidget_14; }
	inline GameObject_t1113636619 ** get_address_of_jeuWidget_14() { return &___jeuWidget_14; }
	inline void set_jeuWidget_14(GameObject_t1113636619 * value)
	{
		___jeuWidget_14 = value;
		Il2CppCodeGenWriteBarrier((&___jeuWidget_14), value);
	}

	inline static int32_t get_offset_of_imageWidget_15() { return static_cast<int32_t>(offsetof(ContentManager2_t2980555998, ___imageWidget_15)); }
	inline GameObject_t1113636619 * get_imageWidget_15() const { return ___imageWidget_15; }
	inline GameObject_t1113636619 ** get_address_of_imageWidget_15() { return &___imageWidget_15; }
	inline void set_imageWidget_15(GameObject_t1113636619 * value)
	{
		___imageWidget_15 = value;
		Il2CppCodeGenWriteBarrier((&___imageWidget_15), value);
	}

	inline static int32_t get_offset_of_videoWidget_16() { return static_cast<int32_t>(offsetof(ContentManager2_t2980555998, ___videoWidget_16)); }
	inline GameObject_t1113636619 * get_videoWidget_16() const { return ___videoWidget_16; }
	inline GameObject_t1113636619 ** get_address_of_videoWidget_16() { return &___videoWidget_16; }
	inline void set_videoWidget_16(GameObject_t1113636619 * value)
	{
		___videoWidget_16 = value;
		Il2CppCodeGenWriteBarrier((&___videoWidget_16), value);
	}

	inline static int32_t get_offset_of_audioWidget_17() { return static_cast<int32_t>(offsetof(ContentManager2_t2980555998, ___audioWidget_17)); }
	inline GameObject_t1113636619 * get_audioWidget_17() const { return ___audioWidget_17; }
	inline GameObject_t1113636619 ** get_address_of_audioWidget_17() { return &___audioWidget_17; }
	inline void set_audioWidget_17(GameObject_t1113636619 * value)
	{
		___audioWidget_17 = value;
		Il2CppCodeGenWriteBarrier((&___audioWidget_17), value);
	}

	inline static int32_t get_offset_of_chromaKeyWidget_18() { return static_cast<int32_t>(offsetof(ContentManager2_t2980555998, ___chromaKeyWidget_18)); }
	inline GameObject_t1113636619 * get_chromaKeyWidget_18() const { return ___chromaKeyWidget_18; }
	inline GameObject_t1113636619 ** get_address_of_chromaKeyWidget_18() { return &___chromaKeyWidget_18; }
	inline void set_chromaKeyWidget_18(GameObject_t1113636619 * value)
	{
		___chromaKeyWidget_18 = value;
		Il2CppCodeGenWriteBarrier((&___chromaKeyWidget_18), value);
	}

	inline static int32_t get_offset_of_videoPlayerPortrait_19() { return static_cast<int32_t>(offsetof(ContentManager2_t2980555998, ___videoPlayerPortrait_19)); }
	inline GameObject_t1113636619 * get_videoPlayerPortrait_19() const { return ___videoPlayerPortrait_19; }
	inline GameObject_t1113636619 ** get_address_of_videoPlayerPortrait_19() { return &___videoPlayerPortrait_19; }
	inline void set_videoPlayerPortrait_19(GameObject_t1113636619 * value)
	{
		___videoPlayerPortrait_19 = value;
		Il2CppCodeGenWriteBarrier((&___videoPlayerPortrait_19), value);
	}

	inline static int32_t get_offset_of_videoPlayerPortrait_RAW_20() { return static_cast<int32_t>(offsetof(ContentManager2_t2980555998, ___videoPlayerPortrait_RAW_20)); }
	inline RawImage_t3182918964 * get_videoPlayerPortrait_RAW_20() const { return ___videoPlayerPortrait_RAW_20; }
	inline RawImage_t3182918964 ** get_address_of_videoPlayerPortrait_RAW_20() { return &___videoPlayerPortrait_RAW_20; }
	inline void set_videoPlayerPortrait_RAW_20(RawImage_t3182918964 * value)
	{
		___videoPlayerPortrait_RAW_20 = value;
		Il2CppCodeGenWriteBarrier((&___videoPlayerPortrait_RAW_20), value);
	}

	inline static int32_t get_offset_of_videoPlayerLandscape_21() { return static_cast<int32_t>(offsetof(ContentManager2_t2980555998, ___videoPlayerLandscape_21)); }
	inline GameObject_t1113636619 * get_videoPlayerLandscape_21() const { return ___videoPlayerLandscape_21; }
	inline GameObject_t1113636619 ** get_address_of_videoPlayerLandscape_21() { return &___videoPlayerLandscape_21; }
	inline void set_videoPlayerLandscape_21(GameObject_t1113636619 * value)
	{
		___videoPlayerLandscape_21 = value;
		Il2CppCodeGenWriteBarrier((&___videoPlayerLandscape_21), value);
	}

	inline static int32_t get_offset_of_videoPlayerLandscape_RAW_22() { return static_cast<int32_t>(offsetof(ContentManager2_t2980555998, ___videoPlayerLandscape_RAW_22)); }
	inline RawImage_t3182918964 * get_videoPlayerLandscape_RAW_22() const { return ___videoPlayerLandscape_RAW_22; }
	inline RawImage_t3182918964 ** get_address_of_videoPlayerLandscape_RAW_22() { return &___videoPlayerLandscape_RAW_22; }
	inline void set_videoPlayerLandscape_RAW_22(RawImage_t3182918964 * value)
	{
		___videoPlayerLandscape_RAW_22 = value;
		Il2CppCodeGenWriteBarrier((&___videoPlayerLandscape_RAW_22), value);
	}

	inline static int32_t get_offset_of_fullscreenVideoPlayer_23() { return static_cast<int32_t>(offsetof(ContentManager2_t2980555998, ___fullscreenVideoPlayer_23)); }
	inline GameObject_t1113636619 * get_fullscreenVideoPlayer_23() const { return ___fullscreenVideoPlayer_23; }
	inline GameObject_t1113636619 ** get_address_of_fullscreenVideoPlayer_23() { return &___fullscreenVideoPlayer_23; }
	inline void set_fullscreenVideoPlayer_23(GameObject_t1113636619 * value)
	{
		___fullscreenVideoPlayer_23 = value;
		Il2CppCodeGenWriteBarrier((&___fullscreenVideoPlayer_23), value);
	}

	inline static int32_t get_offset_of_objectWidget_24() { return static_cast<int32_t>(offsetof(ContentManager2_t2980555998, ___objectWidget_24)); }
	inline GameObject_t1113636619 * get_objectWidget_24() const { return ___objectWidget_24; }
	inline GameObject_t1113636619 ** get_address_of_objectWidget_24() { return &___objectWidget_24; }
	inline void set_objectWidget_24(GameObject_t1113636619 * value)
	{
		___objectWidget_24 = value;
		Il2CppCodeGenWriteBarrier((&___objectWidget_24), value);
	}

	inline static int32_t get_offset_of_rawImgPortrait_25() { return static_cast<int32_t>(offsetof(ContentManager2_t2980555998, ___rawImgPortrait_25)); }
	inline RawImage_t3182918964 * get_rawImgPortrait_25() const { return ___rawImgPortrait_25; }
	inline RawImage_t3182918964 ** get_address_of_rawImgPortrait_25() { return &___rawImgPortrait_25; }
	inline void set_rawImgPortrait_25(RawImage_t3182918964 * value)
	{
		___rawImgPortrait_25 = value;
		Il2CppCodeGenWriteBarrier((&___rawImgPortrait_25), value);
	}

	inline static int32_t get_offset_of_rawImgLandscape_26() { return static_cast<int32_t>(offsetof(ContentManager2_t2980555998, ___rawImgLandscape_26)); }
	inline RawImage_t3182918964 * get_rawImgLandscape_26() const { return ___rawImgLandscape_26; }
	inline RawImage_t3182918964 ** get_address_of_rawImgLandscape_26() { return &___rawImgLandscape_26; }
	inline void set_rawImgLandscape_26(RawImage_t3182918964 * value)
	{
		___rawImgLandscape_26 = value;
		Il2CppCodeGenWriteBarrier((&___rawImgLandscape_26), value);
	}

	inline static int32_t get_offset_of_panelBack_27() { return static_cast<int32_t>(offsetof(ContentManager2_t2980555998, ___panelBack_27)); }
	inline GameObject_t1113636619 * get_panelBack_27() const { return ___panelBack_27; }
	inline GameObject_t1113636619 ** get_address_of_panelBack_27() { return &___panelBack_27; }
	inline void set_panelBack_27(GameObject_t1113636619 * value)
	{
		___panelBack_27 = value;
		Il2CppCodeGenWriteBarrier((&___panelBack_27), value);
	}

	inline static int32_t get_offset_of_CameraOBJContainer_28() { return static_cast<int32_t>(offsetof(ContentManager2_t2980555998, ___CameraOBJContainer_28)); }
	inline GameObject_t1113636619 * get_CameraOBJContainer_28() const { return ___CameraOBJContainer_28; }
	inline GameObject_t1113636619 ** get_address_of_CameraOBJContainer_28() { return &___CameraOBJContainer_28; }
	inline void set_CameraOBJContainer_28(GameObject_t1113636619 * value)
	{
		___CameraOBJContainer_28 = value;
		Il2CppCodeGenWriteBarrier((&___CameraOBJContainer_28), value);
	}

	inline static int32_t get_offset_of_FS_OBJ_container_29() { return static_cast<int32_t>(offsetof(ContentManager2_t2980555998, ___FS_OBJ_container_29)); }
	inline GameObject_t1113636619 * get_FS_OBJ_container_29() const { return ___FS_OBJ_container_29; }
	inline GameObject_t1113636619 ** get_address_of_FS_OBJ_container_29() { return &___FS_OBJ_container_29; }
	inline void set_FS_OBJ_container_29(GameObject_t1113636619 * value)
	{
		___FS_OBJ_container_29 = value;
		Il2CppCodeGenWriteBarrier((&___FS_OBJ_container_29), value);
	}

	inline static int32_t get_offset_of_closeButton_30() { return static_cast<int32_t>(offsetof(ContentManager2_t2980555998, ___closeButton_30)); }
	inline GameObject_t1113636619 * get_closeButton_30() const { return ___closeButton_30; }
	inline GameObject_t1113636619 ** get_address_of_closeButton_30() { return &___closeButton_30; }
	inline void set_closeButton_30(GameObject_t1113636619 * value)
	{
		___closeButton_30 = value;
		Il2CppCodeGenWriteBarrier((&___closeButton_30), value);
	}

	inline static int32_t get_offset_of_backButtonBackMarker_31() { return static_cast<int32_t>(offsetof(ContentManager2_t2980555998, ___backButtonBackMarker_31)); }
	inline GameObject_t1113636619 * get_backButtonBackMarker_31() const { return ___backButtonBackMarker_31; }
	inline GameObject_t1113636619 ** get_address_of_backButtonBackMarker_31() { return &___backButtonBackMarker_31; }
	inline void set_backButtonBackMarker_31(GameObject_t1113636619 * value)
	{
		___backButtonBackMarker_31 = value;
		Il2CppCodeGenWriteBarrier((&___backButtonBackMarker_31), value);
	}

	inline static int32_t get_offset_of_backMarkerON_32() { return static_cast<int32_t>(offsetof(ContentManager2_t2980555998, ___backMarkerON_32)); }
	inline GameObject_t1113636619 * get_backMarkerON_32() const { return ___backMarkerON_32; }
	inline GameObject_t1113636619 ** get_address_of_backMarkerON_32() { return &___backMarkerON_32; }
	inline void set_backMarkerON_32(GameObject_t1113636619 * value)
	{
		___backMarkerON_32 = value;
		Il2CppCodeGenWriteBarrier((&___backMarkerON_32), value);
	}

	inline static int32_t get_offset_of_backMarkerOFF_33() { return static_cast<int32_t>(offsetof(ContentManager2_t2980555998, ___backMarkerOFF_33)); }
	inline GameObject_t1113636619 * get_backMarkerOFF_33() const { return ___backMarkerOFF_33; }
	inline GameObject_t1113636619 ** get_address_of_backMarkerOFF_33() { return &___backMarkerOFF_33; }
	inline void set_backMarkerOFF_33(GameObject_t1113636619 * value)
	{
		___backMarkerOFF_33 = value;
		Il2CppCodeGenWriteBarrier((&___backMarkerOFF_33), value);
	}

	inline static int32_t get_offset_of_isShowingBackMarker_34() { return static_cast<int32_t>(offsetof(ContentManager2_t2980555998, ___isShowingBackMarker_34)); }
	inline bool get_isShowingBackMarker_34() const { return ___isShowingBackMarker_34; }
	inline bool* get_address_of_isShowingBackMarker_34() { return &___isShowingBackMarker_34; }
	inline void set_isShowingBackMarker_34(bool value)
	{
		___isShowingBackMarker_34 = value;
	}

	inline static int32_t get_offset_of_sphereCloseUp_35() { return static_cast<int32_t>(offsetof(ContentManager2_t2980555998, ___sphereCloseUp_35)); }
	inline GameObject_t1113636619 * get_sphereCloseUp_35() const { return ___sphereCloseUp_35; }
	inline GameObject_t1113636619 ** get_address_of_sphereCloseUp_35() { return &___sphereCloseUp_35; }
	inline void set_sphereCloseUp_35(GameObject_t1113636619 * value)
	{
		___sphereCloseUp_35 = value;
		Il2CppCodeGenWriteBarrier((&___sphereCloseUp_35), value);
	}

	inline static int32_t get_offset_of_cubeEtalon_36() { return static_cast<int32_t>(offsetof(ContentManager2_t2980555998, ___cubeEtalon_36)); }
	inline GameObject_t1113636619 * get_cubeEtalon_36() const { return ___cubeEtalon_36; }
	inline GameObject_t1113636619 ** get_address_of_cubeEtalon_36() { return &___cubeEtalon_36; }
	inline void set_cubeEtalon_36(GameObject_t1113636619 * value)
	{
		___cubeEtalon_36 = value;
		Il2CppCodeGenWriteBarrier((&___cubeEtalon_36), value);
	}

	inline static int32_t get_offset_of_closeMarkerButton_37() { return static_cast<int32_t>(offsetof(ContentManager2_t2980555998, ___closeMarkerButton_37)); }
	inline GameObject_t1113636619 * get_closeMarkerButton_37() const { return ___closeMarkerButton_37; }
	inline GameObject_t1113636619 ** get_address_of_closeMarkerButton_37() { return &___closeMarkerButton_37; }
	inline void set_closeMarkerButton_37(GameObject_t1113636619 * value)
	{
		___closeMarkerButton_37 = value;
		Il2CppCodeGenWriteBarrier((&___closeMarkerButton_37), value);
	}

	inline static int32_t get_offset_of_sliderZoom_38() { return static_cast<int32_t>(offsetof(ContentManager2_t2980555998, ___sliderZoom_38)); }
	inline Slider_t3903728902 * get_sliderZoom_38() const { return ___sliderZoom_38; }
	inline Slider_t3903728902 ** get_address_of_sliderZoom_38() { return &___sliderZoom_38; }
	inline void set_sliderZoom_38(Slider_t3903728902 * value)
	{
		___sliderZoom_38 = value;
		Il2CppCodeGenWriteBarrier((&___sliderZoom_38), value);
	}

	inline static int32_t get_offset_of_cancelButton_39() { return static_cast<int32_t>(offsetof(ContentManager2_t2980555998, ___cancelButton_39)); }
	inline GameObject_t1113636619 * get_cancelButton_39() const { return ___cancelButton_39; }
	inline GameObject_t1113636619 ** get_address_of_cancelButton_39() { return &___cancelButton_39; }
	inline void set_cancelButton_39(GameObject_t1113636619 * value)
	{
		___cancelButton_39 = value;
		Il2CppCodeGenWriteBarrier((&___cancelButton_39), value);
	}

	inline static int32_t get_offset_of_fullscreen3DObject_40() { return static_cast<int32_t>(offsetof(ContentManager2_t2980555998, ___fullscreen3DObject_40)); }
	inline GameObject_t1113636619 * get_fullscreen3DObject_40() const { return ___fullscreen3DObject_40; }
	inline GameObject_t1113636619 ** get_address_of_fullscreen3DObject_40() { return &___fullscreen3DObject_40; }
	inline void set_fullscreen3DObject_40(GameObject_t1113636619 * value)
	{
		___fullscreen3DObject_40 = value;
		Il2CppCodeGenWriteBarrier((&___fullscreen3DObject_40), value);
	}

	inline static int32_t get_offset_of_emailPanel_41() { return static_cast<int32_t>(offsetof(ContentManager2_t2980555998, ___emailPanel_41)); }
	inline GameObject_t1113636619 * get_emailPanel_41() const { return ___emailPanel_41; }
	inline GameObject_t1113636619 ** get_address_of_emailPanel_41() { return &___emailPanel_41; }
	inline void set_emailPanel_41(GameObject_t1113636619 * value)
	{
		___emailPanel_41 = value;
		Il2CppCodeGenWriteBarrier((&___emailPanel_41), value);
	}

	inline static int32_t get_offset_of_emailPanel_mail_42() { return static_cast<int32_t>(offsetof(ContentManager2_t2980555998, ___emailPanel_mail_42)); }
	inline TextMeshProUGUI_t529313277 * get_emailPanel_mail_42() const { return ___emailPanel_mail_42; }
	inline TextMeshProUGUI_t529313277 ** get_address_of_emailPanel_mail_42() { return &___emailPanel_mail_42; }
	inline void set_emailPanel_mail_42(TextMeshProUGUI_t529313277 * value)
	{
		___emailPanel_mail_42 = value;
		Il2CppCodeGenWriteBarrier((&___emailPanel_mail_42), value);
	}

	inline static int32_t get_offset_of_emailPanel_text_43() { return static_cast<int32_t>(offsetof(ContentManager2_t2980555998, ___emailPanel_text_43)); }
	inline TextMeshProUGUI_t529313277 * get_emailPanel_text_43() const { return ___emailPanel_text_43; }
	inline TextMeshProUGUI_t529313277 ** get_address_of_emailPanel_text_43() { return &___emailPanel_text_43; }
	inline void set_emailPanel_text_43(TextMeshProUGUI_t529313277 * value)
	{
		___emailPanel_text_43 = value;
		Il2CppCodeGenWriteBarrier((&___emailPanel_text_43), value);
	}

	inline static int32_t get_offset_of_emailPanel_button_44() { return static_cast<int32_t>(offsetof(ContentManager2_t2980555998, ___emailPanel_button_44)); }
	inline Button_t4055032469 * get_emailPanel_button_44() const { return ___emailPanel_button_44; }
	inline Button_t4055032469 ** get_address_of_emailPanel_button_44() { return &___emailPanel_button_44; }
	inline void set_emailPanel_button_44(Button_t4055032469 * value)
	{
		___emailPanel_button_44 = value;
		Il2CppCodeGenWriteBarrier((&___emailPanel_button_44), value);
	}

	inline static int32_t get_offset_of_screenShotURL_45() { return static_cast<int32_t>(offsetof(ContentManager2_t2980555998, ___screenShotURL_45)); }
	inline String_t* get_screenShotURL_45() const { return ___screenShotURL_45; }
	inline String_t** get_address_of_screenShotURL_45() { return &___screenShotURL_45; }
	inline void set_screenShotURL_45(String_t* value)
	{
		___screenShotURL_45 = value;
		Il2CppCodeGenWriteBarrier((&___screenShotURL_45), value);
	}

	inline static int32_t get_offset_of_nodeTex_46() { return static_cast<int32_t>(offsetof(ContentManager2_t2980555998, ___nodeTex_46)); }
	inline GameObject_t1113636619 * get_nodeTex_46() const { return ___nodeTex_46; }
	inline GameObject_t1113636619 ** get_address_of_nodeTex_46() { return &___nodeTex_46; }
	inline void set_nodeTex_46(GameObject_t1113636619 * value)
	{
		___nodeTex_46 = value;
		Il2CppCodeGenWriteBarrier((&___nodeTex_46), value);
	}

	inline static int32_t get_offset_of_mPixelFormat_47() { return static_cast<int32_t>(offsetof(ContentManager2_t2980555998, ___mPixelFormat_47)); }
	inline int32_t get_mPixelFormat_47() const { return ___mPixelFormat_47; }
	inline int32_t* get_address_of_mPixelFormat_47() { return &___mPixelFormat_47; }
	inline void set_mPixelFormat_47(int32_t value)
	{
		___mPixelFormat_47 = value;
	}

	inline static int32_t get_offset_of_mFormatRegistered_48() { return static_cast<int32_t>(offsetof(ContentManager2_t2980555998, ___mFormatRegistered_48)); }
	inline bool get_mFormatRegistered_48() const { return ___mFormatRegistered_48; }
	inline bool* get_address_of_mFormatRegistered_48() { return &___mFormatRegistered_48; }
	inline void set_mFormatRegistered_48(bool value)
	{
		___mFormatRegistered_48 = value;
	}

	inline static int32_t get_offset_of_mCloudRecoBehaviour_49() { return static_cast<int32_t>(offsetof(ContentManager2_t2980555998, ___mCloudRecoBehaviour_49)); }
	inline CloudRecoBehaviour_t431762792 * get_mCloudRecoBehaviour_49() const { return ___mCloudRecoBehaviour_49; }
	inline CloudRecoBehaviour_t431762792 ** get_address_of_mCloudRecoBehaviour_49() { return &___mCloudRecoBehaviour_49; }
	inline void set_mCloudRecoBehaviour_49(CloudRecoBehaviour_t431762792 * value)
	{
		___mCloudRecoBehaviour_49 = value;
		Il2CppCodeGenWriteBarrier((&___mCloudRecoBehaviour_49), value);
	}

	inline static int32_t get_offset_of_mTrackableBehaviour_50() { return static_cast<int32_t>(offsetof(ContentManager2_t2980555998, ___mTrackableBehaviour_50)); }
	inline TrackableBehaviour_t1113559212 * get_mTrackableBehaviour_50() const { return ___mTrackableBehaviour_50; }
	inline TrackableBehaviour_t1113559212 ** get_address_of_mTrackableBehaviour_50() { return &___mTrackableBehaviour_50; }
	inline void set_mTrackableBehaviour_50(TrackableBehaviour_t1113559212 * value)
	{
		___mTrackableBehaviour_50 = value;
		Il2CppCodeGenWriteBarrier((&___mTrackableBehaviour_50), value);
	}

	inline static int32_t get_offset_of_mIsShowingMarkerData_51() { return static_cast<int32_t>(offsetof(ContentManager2_t2980555998, ___mIsShowingMarkerData_51)); }
	inline bool get_mIsShowingMarkerData_51() const { return ___mIsShowingMarkerData_51; }
	inline bool* get_address_of_mIsShowingMarkerData_51() { return &___mIsShowingMarkerData_51; }
	inline void set_mIsShowingMarkerData_51(bool value)
	{
		___mIsShowingMarkerData_51 = value;
	}

	inline static int32_t get_offset_of_currentMarker_52() { return static_cast<int32_t>(offsetof(ContentManager2_t2980555998, ___currentMarker_52)); }
	inline String_t* get_currentMarker_52() const { return ___currentMarker_52; }
	inline String_t** get_address_of_currentMarker_52() { return &___currentMarker_52; }
	inline void set_currentMarker_52(String_t* value)
	{
		___currentMarker_52 = value;
		Il2CppCodeGenWriteBarrier((&___currentMarker_52), value);
	}

	inline static int32_t get_offset_of_isTracking3D_53() { return static_cast<int32_t>(offsetof(ContentManager2_t2980555998, ___isTracking3D_53)); }
	inline bool get_isTracking3D_53() const { return ___isTracking3D_53; }
	inline bool* get_address_of_isTracking3D_53() { return &___isTracking3D_53; }
	inline void set_isTracking3D_53(bool value)
	{
		___isTracking3D_53 = value;
	}

	inline static int32_t get_offset_of_pano_regles_54() { return static_cast<int32_t>(offsetof(ContentManager2_t2980555998, ___pano_regles_54)); }
	inline GameObject_t1113636619 * get_pano_regles_54() const { return ___pano_regles_54; }
	inline GameObject_t1113636619 ** get_address_of_pano_regles_54() { return &___pano_regles_54; }
	inline void set_pano_regles_54(GameObject_t1113636619 * value)
	{
		___pano_regles_54 = value;
		Il2CppCodeGenWriteBarrier((&___pano_regles_54), value);
	}

	inline static int32_t get_offset_of_pano_data_55() { return static_cast<int32_t>(offsetof(ContentManager2_t2980555998, ___pano_data_55)); }
	inline GameObject_t1113636619 * get_pano_data_55() const { return ___pano_data_55; }
	inline GameObject_t1113636619 ** get_address_of_pano_data_55() { return &___pano_data_55; }
	inline void set_pano_data_55(GameObject_t1113636619 * value)
	{
		___pano_data_55 = value;
		Il2CppCodeGenWriteBarrier((&___pano_data_55), value);
	}

	inline static int32_t get_offset_of_pano_error_56() { return static_cast<int32_t>(offsetof(ContentManager2_t2980555998, ___pano_error_56)); }
	inline GameObject_t1113636619 * get_pano_error_56() const { return ___pano_error_56; }
	inline GameObject_t1113636619 ** get_address_of_pano_error_56() { return &___pano_error_56; }
	inline void set_pano_error_56(GameObject_t1113636619 * value)
	{
		___pano_error_56 = value;
		Il2CppCodeGenWriteBarrier((&___pano_error_56), value);
	}

	inline static int32_t get_offset_of_pano_gagne_57() { return static_cast<int32_t>(offsetof(ContentManager2_t2980555998, ___pano_gagne_57)); }
	inline GameObject_t1113636619 * get_pano_gagne_57() const { return ___pano_gagne_57; }
	inline GameObject_t1113636619 ** get_address_of_pano_gagne_57() { return &___pano_gagne_57; }
	inline void set_pano_gagne_57(GameObject_t1113636619 * value)
	{
		___pano_gagne_57 = value;
		Il2CppCodeGenWriteBarrier((&___pano_gagne_57), value);
	}
};

struct ContentManager2_t2980555998_StaticFields
{
public:
	// System.String ContentManager2::baseName
	String_t* ___baseName_4;

public:
	inline static int32_t get_offset_of_baseName_4() { return static_cast<int32_t>(offsetof(ContentManager2_t2980555998_StaticFields, ___baseName_4)); }
	inline String_t* get_baseName_4() const { return ___baseName_4; }
	inline String_t** get_address_of_baseName_4() { return &___baseName_4; }
	inline void set_baseName_4(String_t* value)
	{
		___baseName_4 = value;
		Il2CppCodeGenWriteBarrier((&___baseName_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTENTMANAGER2_T2980555998_H
#ifndef CREDITLOADER_T1693052478_H
#define CREDITLOADER_T1693052478_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CreditLoader
struct  CreditLoader_t1693052478  : public MonoBehaviour_t3962482529
{
public:
	// ContentManager2 CreditLoader::contentManager
	ContentManager2_t2980555998 * ___contentManager_4;
	// UnityEngine.UI.Text CreditLoader::titre
	Text_t1901882714 * ___titre_5;
	// TMPro.TextMeshProUGUI CreditLoader::textPro
	TextMeshProUGUI_t529313277 * ___textPro_6;
	// UnityEngine.UI.Text CreditLoader::mail
	Text_t1901882714 * ___mail_7;
	// UnityEngine.UI.Text CreditLoader::tel
	Text_t1901882714 * ___tel_8;
	// UnityEngine.UI.Image CreditLoader::icone_mail
	Image_t2670269651 * ___icone_mail_9;
	// UnityEngine.UI.Image CreditLoader::icone_phone
	Image_t2670269651 * ___icone_phone_10;
	// UnityEngine.UI.Scrollbar CreditLoader::scrollbarCredit
	Scrollbar_t1494447233 * ___scrollbarCredit_11;
	// UnityEngine.UI.Image CreditLoader::logoAlpha
	Image_t2670269651 * ___logoAlpha_12;
	// UnityEngine.UI.Image CreditLoader::channelLogo
	Image_t2670269651 * ___channelLogo_13;
	// UnityEngine.UI.Image CreditLoader::closeButton
	Image_t2670269651 * ___closeButton_14;
	// UnityEngine.UI.Image CreditLoader::button1
	Image_t2670269651 * ___button1_15;
	// UnityEngine.UI.Image CreditLoader::button2
	Image_t2670269651 * ___button2_16;
	// UnityEngine.UI.Image CreditLoader::button3
	Image_t2670269651 * ___button3_17;
	// UnityEngine.UI.Image CreditLoader::button4
	Image_t2670269651 * ___button4_18;
	// UnityEngine.UI.Text CreditLoader::etape1
	Text_t1901882714 * ___etape1_19;

public:
	inline static int32_t get_offset_of_contentManager_4() { return static_cast<int32_t>(offsetof(CreditLoader_t1693052478, ___contentManager_4)); }
	inline ContentManager2_t2980555998 * get_contentManager_4() const { return ___contentManager_4; }
	inline ContentManager2_t2980555998 ** get_address_of_contentManager_4() { return &___contentManager_4; }
	inline void set_contentManager_4(ContentManager2_t2980555998 * value)
	{
		___contentManager_4 = value;
		Il2CppCodeGenWriteBarrier((&___contentManager_4), value);
	}

	inline static int32_t get_offset_of_titre_5() { return static_cast<int32_t>(offsetof(CreditLoader_t1693052478, ___titre_5)); }
	inline Text_t1901882714 * get_titre_5() const { return ___titre_5; }
	inline Text_t1901882714 ** get_address_of_titre_5() { return &___titre_5; }
	inline void set_titre_5(Text_t1901882714 * value)
	{
		___titre_5 = value;
		Il2CppCodeGenWriteBarrier((&___titre_5), value);
	}

	inline static int32_t get_offset_of_textPro_6() { return static_cast<int32_t>(offsetof(CreditLoader_t1693052478, ___textPro_6)); }
	inline TextMeshProUGUI_t529313277 * get_textPro_6() const { return ___textPro_6; }
	inline TextMeshProUGUI_t529313277 ** get_address_of_textPro_6() { return &___textPro_6; }
	inline void set_textPro_6(TextMeshProUGUI_t529313277 * value)
	{
		___textPro_6 = value;
		Il2CppCodeGenWriteBarrier((&___textPro_6), value);
	}

	inline static int32_t get_offset_of_mail_7() { return static_cast<int32_t>(offsetof(CreditLoader_t1693052478, ___mail_7)); }
	inline Text_t1901882714 * get_mail_7() const { return ___mail_7; }
	inline Text_t1901882714 ** get_address_of_mail_7() { return &___mail_7; }
	inline void set_mail_7(Text_t1901882714 * value)
	{
		___mail_7 = value;
		Il2CppCodeGenWriteBarrier((&___mail_7), value);
	}

	inline static int32_t get_offset_of_tel_8() { return static_cast<int32_t>(offsetof(CreditLoader_t1693052478, ___tel_8)); }
	inline Text_t1901882714 * get_tel_8() const { return ___tel_8; }
	inline Text_t1901882714 ** get_address_of_tel_8() { return &___tel_8; }
	inline void set_tel_8(Text_t1901882714 * value)
	{
		___tel_8 = value;
		Il2CppCodeGenWriteBarrier((&___tel_8), value);
	}

	inline static int32_t get_offset_of_icone_mail_9() { return static_cast<int32_t>(offsetof(CreditLoader_t1693052478, ___icone_mail_9)); }
	inline Image_t2670269651 * get_icone_mail_9() const { return ___icone_mail_9; }
	inline Image_t2670269651 ** get_address_of_icone_mail_9() { return &___icone_mail_9; }
	inline void set_icone_mail_9(Image_t2670269651 * value)
	{
		___icone_mail_9 = value;
		Il2CppCodeGenWriteBarrier((&___icone_mail_9), value);
	}

	inline static int32_t get_offset_of_icone_phone_10() { return static_cast<int32_t>(offsetof(CreditLoader_t1693052478, ___icone_phone_10)); }
	inline Image_t2670269651 * get_icone_phone_10() const { return ___icone_phone_10; }
	inline Image_t2670269651 ** get_address_of_icone_phone_10() { return &___icone_phone_10; }
	inline void set_icone_phone_10(Image_t2670269651 * value)
	{
		___icone_phone_10 = value;
		Il2CppCodeGenWriteBarrier((&___icone_phone_10), value);
	}

	inline static int32_t get_offset_of_scrollbarCredit_11() { return static_cast<int32_t>(offsetof(CreditLoader_t1693052478, ___scrollbarCredit_11)); }
	inline Scrollbar_t1494447233 * get_scrollbarCredit_11() const { return ___scrollbarCredit_11; }
	inline Scrollbar_t1494447233 ** get_address_of_scrollbarCredit_11() { return &___scrollbarCredit_11; }
	inline void set_scrollbarCredit_11(Scrollbar_t1494447233 * value)
	{
		___scrollbarCredit_11 = value;
		Il2CppCodeGenWriteBarrier((&___scrollbarCredit_11), value);
	}

	inline static int32_t get_offset_of_logoAlpha_12() { return static_cast<int32_t>(offsetof(CreditLoader_t1693052478, ___logoAlpha_12)); }
	inline Image_t2670269651 * get_logoAlpha_12() const { return ___logoAlpha_12; }
	inline Image_t2670269651 ** get_address_of_logoAlpha_12() { return &___logoAlpha_12; }
	inline void set_logoAlpha_12(Image_t2670269651 * value)
	{
		___logoAlpha_12 = value;
		Il2CppCodeGenWriteBarrier((&___logoAlpha_12), value);
	}

	inline static int32_t get_offset_of_channelLogo_13() { return static_cast<int32_t>(offsetof(CreditLoader_t1693052478, ___channelLogo_13)); }
	inline Image_t2670269651 * get_channelLogo_13() const { return ___channelLogo_13; }
	inline Image_t2670269651 ** get_address_of_channelLogo_13() { return &___channelLogo_13; }
	inline void set_channelLogo_13(Image_t2670269651 * value)
	{
		___channelLogo_13 = value;
		Il2CppCodeGenWriteBarrier((&___channelLogo_13), value);
	}

	inline static int32_t get_offset_of_closeButton_14() { return static_cast<int32_t>(offsetof(CreditLoader_t1693052478, ___closeButton_14)); }
	inline Image_t2670269651 * get_closeButton_14() const { return ___closeButton_14; }
	inline Image_t2670269651 ** get_address_of_closeButton_14() { return &___closeButton_14; }
	inline void set_closeButton_14(Image_t2670269651 * value)
	{
		___closeButton_14 = value;
		Il2CppCodeGenWriteBarrier((&___closeButton_14), value);
	}

	inline static int32_t get_offset_of_button1_15() { return static_cast<int32_t>(offsetof(CreditLoader_t1693052478, ___button1_15)); }
	inline Image_t2670269651 * get_button1_15() const { return ___button1_15; }
	inline Image_t2670269651 ** get_address_of_button1_15() { return &___button1_15; }
	inline void set_button1_15(Image_t2670269651 * value)
	{
		___button1_15 = value;
		Il2CppCodeGenWriteBarrier((&___button1_15), value);
	}

	inline static int32_t get_offset_of_button2_16() { return static_cast<int32_t>(offsetof(CreditLoader_t1693052478, ___button2_16)); }
	inline Image_t2670269651 * get_button2_16() const { return ___button2_16; }
	inline Image_t2670269651 ** get_address_of_button2_16() { return &___button2_16; }
	inline void set_button2_16(Image_t2670269651 * value)
	{
		___button2_16 = value;
		Il2CppCodeGenWriteBarrier((&___button2_16), value);
	}

	inline static int32_t get_offset_of_button3_17() { return static_cast<int32_t>(offsetof(CreditLoader_t1693052478, ___button3_17)); }
	inline Image_t2670269651 * get_button3_17() const { return ___button3_17; }
	inline Image_t2670269651 ** get_address_of_button3_17() { return &___button3_17; }
	inline void set_button3_17(Image_t2670269651 * value)
	{
		___button3_17 = value;
		Il2CppCodeGenWriteBarrier((&___button3_17), value);
	}

	inline static int32_t get_offset_of_button4_18() { return static_cast<int32_t>(offsetof(CreditLoader_t1693052478, ___button4_18)); }
	inline Image_t2670269651 * get_button4_18() const { return ___button4_18; }
	inline Image_t2670269651 ** get_address_of_button4_18() { return &___button4_18; }
	inline void set_button4_18(Image_t2670269651 * value)
	{
		___button4_18 = value;
		Il2CppCodeGenWriteBarrier((&___button4_18), value);
	}

	inline static int32_t get_offset_of_etape1_19() { return static_cast<int32_t>(offsetof(CreditLoader_t1693052478, ___etape1_19)); }
	inline Text_t1901882714 * get_etape1_19() const { return ___etape1_19; }
	inline Text_t1901882714 ** get_address_of_etape1_19() { return &___etape1_19; }
	inline void set_etape1_19(Text_t1901882714 * value)
	{
		___etape1_19 = value;
		Il2CppCodeGenWriteBarrier((&___etape1_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CREDITLOADER_T1693052478_H
#ifndef DEFERREDFOGEFFECT_T753821049_H
#define DEFERREDFOGEFFECT_T753821049_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DeferredFogEffect
struct  DeferredFogEffect_t753821049  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Shader DeferredFogEffect::deferredFog
	Shader_t4151988712 * ___deferredFog_4;
	// UnityEngine.Material DeferredFogEffect::fogMaterial
	Material_t340375123 * ___fogMaterial_5;
	// UnityEngine.Camera DeferredFogEffect::deferredCamera
	Camera_t4157153871 * ___deferredCamera_6;
	// UnityEngine.Vector3[] DeferredFogEffect::frustumCorners
	Vector3U5BU5D_t1718750761* ___frustumCorners_7;
	// UnityEngine.Vector4[] DeferredFogEffect::vectorArray
	Vector4U5BU5D_t934056436* ___vectorArray_8;

public:
	inline static int32_t get_offset_of_deferredFog_4() { return static_cast<int32_t>(offsetof(DeferredFogEffect_t753821049, ___deferredFog_4)); }
	inline Shader_t4151988712 * get_deferredFog_4() const { return ___deferredFog_4; }
	inline Shader_t4151988712 ** get_address_of_deferredFog_4() { return &___deferredFog_4; }
	inline void set_deferredFog_4(Shader_t4151988712 * value)
	{
		___deferredFog_4 = value;
		Il2CppCodeGenWriteBarrier((&___deferredFog_4), value);
	}

	inline static int32_t get_offset_of_fogMaterial_5() { return static_cast<int32_t>(offsetof(DeferredFogEffect_t753821049, ___fogMaterial_5)); }
	inline Material_t340375123 * get_fogMaterial_5() const { return ___fogMaterial_5; }
	inline Material_t340375123 ** get_address_of_fogMaterial_5() { return &___fogMaterial_5; }
	inline void set_fogMaterial_5(Material_t340375123 * value)
	{
		___fogMaterial_5 = value;
		Il2CppCodeGenWriteBarrier((&___fogMaterial_5), value);
	}

	inline static int32_t get_offset_of_deferredCamera_6() { return static_cast<int32_t>(offsetof(DeferredFogEffect_t753821049, ___deferredCamera_6)); }
	inline Camera_t4157153871 * get_deferredCamera_6() const { return ___deferredCamera_6; }
	inline Camera_t4157153871 ** get_address_of_deferredCamera_6() { return &___deferredCamera_6; }
	inline void set_deferredCamera_6(Camera_t4157153871 * value)
	{
		___deferredCamera_6 = value;
		Il2CppCodeGenWriteBarrier((&___deferredCamera_6), value);
	}

	inline static int32_t get_offset_of_frustumCorners_7() { return static_cast<int32_t>(offsetof(DeferredFogEffect_t753821049, ___frustumCorners_7)); }
	inline Vector3U5BU5D_t1718750761* get_frustumCorners_7() const { return ___frustumCorners_7; }
	inline Vector3U5BU5D_t1718750761** get_address_of_frustumCorners_7() { return &___frustumCorners_7; }
	inline void set_frustumCorners_7(Vector3U5BU5D_t1718750761* value)
	{
		___frustumCorners_7 = value;
		Il2CppCodeGenWriteBarrier((&___frustumCorners_7), value);
	}

	inline static int32_t get_offset_of_vectorArray_8() { return static_cast<int32_t>(offsetof(DeferredFogEffect_t753821049, ___vectorArray_8)); }
	inline Vector4U5BU5D_t934056436* get_vectorArray_8() const { return ___vectorArray_8; }
	inline Vector4U5BU5D_t934056436** get_address_of_vectorArray_8() { return &___vectorArray_8; }
	inline void set_vectorArray_8(Vector4U5BU5D_t934056436* value)
	{
		___vectorArray_8 = value;
		Il2CppCodeGenWriteBarrier((&___vectorArray_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFERREDFOGEFFECT_T753821049_H
#ifndef DYNAMICOBJECT_T314656423_H
#define DYNAMICOBJECT_T314656423_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DynamicObject
struct  DynamicObject_t314656423  : public MonoBehaviour_t3962482529
{
public:
	// DynamicObject/Transition DynamicObject::showTransition
	int32_t ___showTransition_4;
	// System.Single DynamicObject::transitionSpeed
	float ___transitionSpeed_5;
	// DynamicObject/LaunchActionEvent DynamicObject::onShowTransitionStartAction
	LaunchActionEvent_t193601726 * ___onShowTransitionStartAction_6;
	// DynamicObject/LaunchActionEvent DynamicObject::onShowTransitionDoneAction
	LaunchActionEvent_t193601726 * ___onShowTransitionDoneAction_7;
	// DynamicObject/LaunchActionEvent DynamicObject::onHideTransitionStartAction
	LaunchActionEvent_t193601726 * ___onHideTransitionStartAction_8;
	// DynamicObject/LaunchActionEvent DynamicObject::onHideTransitionDoneAction
	LaunchActionEvent_t193601726 * ___onHideTransitionDoneAction_9;
	// DynamicObject/OnTransitionDoneDeleg DynamicObject::onTransitionDoneEvt
	OnTransitionDoneDeleg_t1142753823 * ___onTransitionDoneEvt_10;
	// UnityEngine.Vector3 DynamicObject::showTransitionVector
	Vector3_t3722313464  ___showTransitionVector_11;
	// UnityEngine.RectTransform DynamicObject::rectTransform
	RectTransform_t3704657025 * ___rectTransform_12;
	// UnityEngine.RectTransform DynamicObject::anchorTop
	RectTransform_t3704657025 * ___anchorTop_13;
	// UnityEngine.RectTransform DynamicObject::anchorBottom
	RectTransform_t3704657025 * ___anchorBottom_14;
	// UnityEngine.RectTransform DynamicObject::anchorLeft
	RectTransform_t3704657025 * ___anchorLeft_15;
	// UnityEngine.RectTransform DynamicObject::anchorRight
	RectTransform_t3704657025 * ___anchorRight_16;
	// UnityEngine.UI.MaskableGraphic[] DynamicObject::elements
	MaskableGraphicU5BU5D_t434946574* ___elements_17;

public:
	inline static int32_t get_offset_of_showTransition_4() { return static_cast<int32_t>(offsetof(DynamicObject_t314656423, ___showTransition_4)); }
	inline int32_t get_showTransition_4() const { return ___showTransition_4; }
	inline int32_t* get_address_of_showTransition_4() { return &___showTransition_4; }
	inline void set_showTransition_4(int32_t value)
	{
		___showTransition_4 = value;
	}

	inline static int32_t get_offset_of_transitionSpeed_5() { return static_cast<int32_t>(offsetof(DynamicObject_t314656423, ___transitionSpeed_5)); }
	inline float get_transitionSpeed_5() const { return ___transitionSpeed_5; }
	inline float* get_address_of_transitionSpeed_5() { return &___transitionSpeed_5; }
	inline void set_transitionSpeed_5(float value)
	{
		___transitionSpeed_5 = value;
	}

	inline static int32_t get_offset_of_onShowTransitionStartAction_6() { return static_cast<int32_t>(offsetof(DynamicObject_t314656423, ___onShowTransitionStartAction_6)); }
	inline LaunchActionEvent_t193601726 * get_onShowTransitionStartAction_6() const { return ___onShowTransitionStartAction_6; }
	inline LaunchActionEvent_t193601726 ** get_address_of_onShowTransitionStartAction_6() { return &___onShowTransitionStartAction_6; }
	inline void set_onShowTransitionStartAction_6(LaunchActionEvent_t193601726 * value)
	{
		___onShowTransitionStartAction_6 = value;
		Il2CppCodeGenWriteBarrier((&___onShowTransitionStartAction_6), value);
	}

	inline static int32_t get_offset_of_onShowTransitionDoneAction_7() { return static_cast<int32_t>(offsetof(DynamicObject_t314656423, ___onShowTransitionDoneAction_7)); }
	inline LaunchActionEvent_t193601726 * get_onShowTransitionDoneAction_7() const { return ___onShowTransitionDoneAction_7; }
	inline LaunchActionEvent_t193601726 ** get_address_of_onShowTransitionDoneAction_7() { return &___onShowTransitionDoneAction_7; }
	inline void set_onShowTransitionDoneAction_7(LaunchActionEvent_t193601726 * value)
	{
		___onShowTransitionDoneAction_7 = value;
		Il2CppCodeGenWriteBarrier((&___onShowTransitionDoneAction_7), value);
	}

	inline static int32_t get_offset_of_onHideTransitionStartAction_8() { return static_cast<int32_t>(offsetof(DynamicObject_t314656423, ___onHideTransitionStartAction_8)); }
	inline LaunchActionEvent_t193601726 * get_onHideTransitionStartAction_8() const { return ___onHideTransitionStartAction_8; }
	inline LaunchActionEvent_t193601726 ** get_address_of_onHideTransitionStartAction_8() { return &___onHideTransitionStartAction_8; }
	inline void set_onHideTransitionStartAction_8(LaunchActionEvent_t193601726 * value)
	{
		___onHideTransitionStartAction_8 = value;
		Il2CppCodeGenWriteBarrier((&___onHideTransitionStartAction_8), value);
	}

	inline static int32_t get_offset_of_onHideTransitionDoneAction_9() { return static_cast<int32_t>(offsetof(DynamicObject_t314656423, ___onHideTransitionDoneAction_9)); }
	inline LaunchActionEvent_t193601726 * get_onHideTransitionDoneAction_9() const { return ___onHideTransitionDoneAction_9; }
	inline LaunchActionEvent_t193601726 ** get_address_of_onHideTransitionDoneAction_9() { return &___onHideTransitionDoneAction_9; }
	inline void set_onHideTransitionDoneAction_9(LaunchActionEvent_t193601726 * value)
	{
		___onHideTransitionDoneAction_9 = value;
		Il2CppCodeGenWriteBarrier((&___onHideTransitionDoneAction_9), value);
	}

	inline static int32_t get_offset_of_onTransitionDoneEvt_10() { return static_cast<int32_t>(offsetof(DynamicObject_t314656423, ___onTransitionDoneEvt_10)); }
	inline OnTransitionDoneDeleg_t1142753823 * get_onTransitionDoneEvt_10() const { return ___onTransitionDoneEvt_10; }
	inline OnTransitionDoneDeleg_t1142753823 ** get_address_of_onTransitionDoneEvt_10() { return &___onTransitionDoneEvt_10; }
	inline void set_onTransitionDoneEvt_10(OnTransitionDoneDeleg_t1142753823 * value)
	{
		___onTransitionDoneEvt_10 = value;
		Il2CppCodeGenWriteBarrier((&___onTransitionDoneEvt_10), value);
	}

	inline static int32_t get_offset_of_showTransitionVector_11() { return static_cast<int32_t>(offsetof(DynamicObject_t314656423, ___showTransitionVector_11)); }
	inline Vector3_t3722313464  get_showTransitionVector_11() const { return ___showTransitionVector_11; }
	inline Vector3_t3722313464 * get_address_of_showTransitionVector_11() { return &___showTransitionVector_11; }
	inline void set_showTransitionVector_11(Vector3_t3722313464  value)
	{
		___showTransitionVector_11 = value;
	}

	inline static int32_t get_offset_of_rectTransform_12() { return static_cast<int32_t>(offsetof(DynamicObject_t314656423, ___rectTransform_12)); }
	inline RectTransform_t3704657025 * get_rectTransform_12() const { return ___rectTransform_12; }
	inline RectTransform_t3704657025 ** get_address_of_rectTransform_12() { return &___rectTransform_12; }
	inline void set_rectTransform_12(RectTransform_t3704657025 * value)
	{
		___rectTransform_12 = value;
		Il2CppCodeGenWriteBarrier((&___rectTransform_12), value);
	}

	inline static int32_t get_offset_of_anchorTop_13() { return static_cast<int32_t>(offsetof(DynamicObject_t314656423, ___anchorTop_13)); }
	inline RectTransform_t3704657025 * get_anchorTop_13() const { return ___anchorTop_13; }
	inline RectTransform_t3704657025 ** get_address_of_anchorTop_13() { return &___anchorTop_13; }
	inline void set_anchorTop_13(RectTransform_t3704657025 * value)
	{
		___anchorTop_13 = value;
		Il2CppCodeGenWriteBarrier((&___anchorTop_13), value);
	}

	inline static int32_t get_offset_of_anchorBottom_14() { return static_cast<int32_t>(offsetof(DynamicObject_t314656423, ___anchorBottom_14)); }
	inline RectTransform_t3704657025 * get_anchorBottom_14() const { return ___anchorBottom_14; }
	inline RectTransform_t3704657025 ** get_address_of_anchorBottom_14() { return &___anchorBottom_14; }
	inline void set_anchorBottom_14(RectTransform_t3704657025 * value)
	{
		___anchorBottom_14 = value;
		Il2CppCodeGenWriteBarrier((&___anchorBottom_14), value);
	}

	inline static int32_t get_offset_of_anchorLeft_15() { return static_cast<int32_t>(offsetof(DynamicObject_t314656423, ___anchorLeft_15)); }
	inline RectTransform_t3704657025 * get_anchorLeft_15() const { return ___anchorLeft_15; }
	inline RectTransform_t3704657025 ** get_address_of_anchorLeft_15() { return &___anchorLeft_15; }
	inline void set_anchorLeft_15(RectTransform_t3704657025 * value)
	{
		___anchorLeft_15 = value;
		Il2CppCodeGenWriteBarrier((&___anchorLeft_15), value);
	}

	inline static int32_t get_offset_of_anchorRight_16() { return static_cast<int32_t>(offsetof(DynamicObject_t314656423, ___anchorRight_16)); }
	inline RectTransform_t3704657025 * get_anchorRight_16() const { return ___anchorRight_16; }
	inline RectTransform_t3704657025 ** get_address_of_anchorRight_16() { return &___anchorRight_16; }
	inline void set_anchorRight_16(RectTransform_t3704657025 * value)
	{
		___anchorRight_16 = value;
		Il2CppCodeGenWriteBarrier((&___anchorRight_16), value);
	}

	inline static int32_t get_offset_of_elements_17() { return static_cast<int32_t>(offsetof(DynamicObject_t314656423, ___elements_17)); }
	inline MaskableGraphicU5BU5D_t434946574* get_elements_17() const { return ___elements_17; }
	inline MaskableGraphicU5BU5D_t434946574** get_address_of_elements_17() { return &___elements_17; }
	inline void set_elements_17(MaskableGraphicU5BU5D_t434946574* value)
	{
		___elements_17 = value;
		Il2CppCodeGenWriteBarrier((&___elements_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DYNAMICOBJECT_T314656423_H
#ifndef EMISSIVEOSCILLATOR_T3402490064_H
#define EMISSIVEOSCILLATOR_T3402490064_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EmissiveOscillator
struct  EmissiveOscillator_t3402490064  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.MeshRenderer EmissiveOscillator::emissiveRenderer
	MeshRenderer_t587009260 * ___emissiveRenderer_4;
	// UnityEngine.Material EmissiveOscillator::emissiveMaterial
	Material_t340375123 * ___emissiveMaterial_5;

public:
	inline static int32_t get_offset_of_emissiveRenderer_4() { return static_cast<int32_t>(offsetof(EmissiveOscillator_t3402490064, ___emissiveRenderer_4)); }
	inline MeshRenderer_t587009260 * get_emissiveRenderer_4() const { return ___emissiveRenderer_4; }
	inline MeshRenderer_t587009260 ** get_address_of_emissiveRenderer_4() { return &___emissiveRenderer_4; }
	inline void set_emissiveRenderer_4(MeshRenderer_t587009260 * value)
	{
		___emissiveRenderer_4 = value;
		Il2CppCodeGenWriteBarrier((&___emissiveRenderer_4), value);
	}

	inline static int32_t get_offset_of_emissiveMaterial_5() { return static_cast<int32_t>(offsetof(EmissiveOscillator_t3402490064, ___emissiveMaterial_5)); }
	inline Material_t340375123 * get_emissiveMaterial_5() const { return ___emissiveMaterial_5; }
	inline Material_t340375123 ** get_address_of_emissiveMaterial_5() { return &___emissiveMaterial_5; }
	inline void set_emissiveMaterial_5(Material_t340375123 * value)
	{
		___emissiveMaterial_5 = value;
		Il2CppCodeGenWriteBarrier((&___emissiveMaterial_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EMISSIVEOSCILLATOR_T3402490064_H
#ifndef FOCUSMANAGER_T1358311062_H
#define FOCUSMANAGER_T1358311062_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FocusManager
struct  FocusManager_t1358311062  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FOCUSMANAGER_T1358311062_H
#ifndef FULLSCREENVIDEOPLAYER_T1619346304_H
#define FULLSCREENVIDEOPLAYER_T1619346304_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullscreenVideoPlayer
struct  FullscreenVideoPlayer_t1619346304  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject FullscreenVideoPlayer::landScapeObject
	GameObject_t1113636619 * ___landScapeObject_4;
	// UnityEngine.GameObject FullscreenVideoPlayer::portraitObject
	GameObject_t1113636619 * ___portraitObject_5;
	// UnityEngine.Video.VideoPlayer FullscreenVideoPlayer::videoPlayer
	VideoPlayer_t1683042537 * ___videoPlayer_6;
	// UnityEngine.AudioSource FullscreenVideoPlayer::audioSrc
	AudioSource_t3935305588 * ___audioSrc_7;
	// System.Int64 FullscreenVideoPlayer::currentFrame
	int64_t ___currentFrame_8;
	// UnityEngine.UI.RawImage FullscreenVideoPlayer::landscape_Raw
	RawImage_t3182918964 * ___landscape_Raw_9;
	// UnityEngine.UI.RawImage FullscreenVideoPlayer::portrait_Raw
	RawImage_t3182918964 * ___portrait_Raw_10;
	// UnityEngine.UI.Slider FullscreenVideoPlayer::sliderLandscape
	Slider_t3903728902 * ___sliderLandscape_11;
	// UnityEngine.UI.Slider FullscreenVideoPlayer::sliderPortrait
	Slider_t3903728902 * ___sliderPortrait_12;
	// UnityEngine.UI.Slider FullscreenVideoPlayer::seekBarLandscape
	Slider_t3903728902 * ___seekBarLandscape_13;
	// UnityEngine.UI.Slider FullscreenVideoPlayer::seekBarPortrait
	Slider_t3903728902 * ___seekBarPortrait_14;
	// UnityEngine.GameObject FullscreenVideoPlayer::playButtonLandscape
	GameObject_t1113636619 * ___playButtonLandscape_15;
	// UnityEngine.GameObject FullscreenVideoPlayer::playButtonPortrait
	GameObject_t1113636619 * ___playButtonPortrait_16;
	// UnityEngine.GameObject FullscreenVideoPlayer::pauseButtonLandscape
	GameObject_t1113636619 * ___pauseButtonLandscape_17;
	// UnityEngine.GameObject FullscreenVideoPlayer::pauseButtonPortrait
	GameObject_t1113636619 * ___pauseButtonPortrait_18;
	// UnityEngine.GameObject FullscreenVideoPlayer::panelLandscape
	GameObject_t1113636619 * ___panelLandscape_19;
	// UnityEngine.GameObject FullscreenVideoPlayer::panelPortrait
	GameObject_t1113636619 * ___panelPortrait_20;
	// ContentManager2 FullscreenVideoPlayer::contentManager
	ContentManager2_t2980555998 * ___contentManager_21;
	// System.String FullscreenVideoPlayer::url
	String_t* ___url_22;
	// System.Boolean FullscreenVideoPlayer::isLandscape
	bool ___isLandscape_23;

public:
	inline static int32_t get_offset_of_landScapeObject_4() { return static_cast<int32_t>(offsetof(FullscreenVideoPlayer_t1619346304, ___landScapeObject_4)); }
	inline GameObject_t1113636619 * get_landScapeObject_4() const { return ___landScapeObject_4; }
	inline GameObject_t1113636619 ** get_address_of_landScapeObject_4() { return &___landScapeObject_4; }
	inline void set_landScapeObject_4(GameObject_t1113636619 * value)
	{
		___landScapeObject_4 = value;
		Il2CppCodeGenWriteBarrier((&___landScapeObject_4), value);
	}

	inline static int32_t get_offset_of_portraitObject_5() { return static_cast<int32_t>(offsetof(FullscreenVideoPlayer_t1619346304, ___portraitObject_5)); }
	inline GameObject_t1113636619 * get_portraitObject_5() const { return ___portraitObject_5; }
	inline GameObject_t1113636619 ** get_address_of_portraitObject_5() { return &___portraitObject_5; }
	inline void set_portraitObject_5(GameObject_t1113636619 * value)
	{
		___portraitObject_5 = value;
		Il2CppCodeGenWriteBarrier((&___portraitObject_5), value);
	}

	inline static int32_t get_offset_of_videoPlayer_6() { return static_cast<int32_t>(offsetof(FullscreenVideoPlayer_t1619346304, ___videoPlayer_6)); }
	inline VideoPlayer_t1683042537 * get_videoPlayer_6() const { return ___videoPlayer_6; }
	inline VideoPlayer_t1683042537 ** get_address_of_videoPlayer_6() { return &___videoPlayer_6; }
	inline void set_videoPlayer_6(VideoPlayer_t1683042537 * value)
	{
		___videoPlayer_6 = value;
		Il2CppCodeGenWriteBarrier((&___videoPlayer_6), value);
	}

	inline static int32_t get_offset_of_audioSrc_7() { return static_cast<int32_t>(offsetof(FullscreenVideoPlayer_t1619346304, ___audioSrc_7)); }
	inline AudioSource_t3935305588 * get_audioSrc_7() const { return ___audioSrc_7; }
	inline AudioSource_t3935305588 ** get_address_of_audioSrc_7() { return &___audioSrc_7; }
	inline void set_audioSrc_7(AudioSource_t3935305588 * value)
	{
		___audioSrc_7 = value;
		Il2CppCodeGenWriteBarrier((&___audioSrc_7), value);
	}

	inline static int32_t get_offset_of_currentFrame_8() { return static_cast<int32_t>(offsetof(FullscreenVideoPlayer_t1619346304, ___currentFrame_8)); }
	inline int64_t get_currentFrame_8() const { return ___currentFrame_8; }
	inline int64_t* get_address_of_currentFrame_8() { return &___currentFrame_8; }
	inline void set_currentFrame_8(int64_t value)
	{
		___currentFrame_8 = value;
	}

	inline static int32_t get_offset_of_landscape_Raw_9() { return static_cast<int32_t>(offsetof(FullscreenVideoPlayer_t1619346304, ___landscape_Raw_9)); }
	inline RawImage_t3182918964 * get_landscape_Raw_9() const { return ___landscape_Raw_9; }
	inline RawImage_t3182918964 ** get_address_of_landscape_Raw_9() { return &___landscape_Raw_9; }
	inline void set_landscape_Raw_9(RawImage_t3182918964 * value)
	{
		___landscape_Raw_9 = value;
		Il2CppCodeGenWriteBarrier((&___landscape_Raw_9), value);
	}

	inline static int32_t get_offset_of_portrait_Raw_10() { return static_cast<int32_t>(offsetof(FullscreenVideoPlayer_t1619346304, ___portrait_Raw_10)); }
	inline RawImage_t3182918964 * get_portrait_Raw_10() const { return ___portrait_Raw_10; }
	inline RawImage_t3182918964 ** get_address_of_portrait_Raw_10() { return &___portrait_Raw_10; }
	inline void set_portrait_Raw_10(RawImage_t3182918964 * value)
	{
		___portrait_Raw_10 = value;
		Il2CppCodeGenWriteBarrier((&___portrait_Raw_10), value);
	}

	inline static int32_t get_offset_of_sliderLandscape_11() { return static_cast<int32_t>(offsetof(FullscreenVideoPlayer_t1619346304, ___sliderLandscape_11)); }
	inline Slider_t3903728902 * get_sliderLandscape_11() const { return ___sliderLandscape_11; }
	inline Slider_t3903728902 ** get_address_of_sliderLandscape_11() { return &___sliderLandscape_11; }
	inline void set_sliderLandscape_11(Slider_t3903728902 * value)
	{
		___sliderLandscape_11 = value;
		Il2CppCodeGenWriteBarrier((&___sliderLandscape_11), value);
	}

	inline static int32_t get_offset_of_sliderPortrait_12() { return static_cast<int32_t>(offsetof(FullscreenVideoPlayer_t1619346304, ___sliderPortrait_12)); }
	inline Slider_t3903728902 * get_sliderPortrait_12() const { return ___sliderPortrait_12; }
	inline Slider_t3903728902 ** get_address_of_sliderPortrait_12() { return &___sliderPortrait_12; }
	inline void set_sliderPortrait_12(Slider_t3903728902 * value)
	{
		___sliderPortrait_12 = value;
		Il2CppCodeGenWriteBarrier((&___sliderPortrait_12), value);
	}

	inline static int32_t get_offset_of_seekBarLandscape_13() { return static_cast<int32_t>(offsetof(FullscreenVideoPlayer_t1619346304, ___seekBarLandscape_13)); }
	inline Slider_t3903728902 * get_seekBarLandscape_13() const { return ___seekBarLandscape_13; }
	inline Slider_t3903728902 ** get_address_of_seekBarLandscape_13() { return &___seekBarLandscape_13; }
	inline void set_seekBarLandscape_13(Slider_t3903728902 * value)
	{
		___seekBarLandscape_13 = value;
		Il2CppCodeGenWriteBarrier((&___seekBarLandscape_13), value);
	}

	inline static int32_t get_offset_of_seekBarPortrait_14() { return static_cast<int32_t>(offsetof(FullscreenVideoPlayer_t1619346304, ___seekBarPortrait_14)); }
	inline Slider_t3903728902 * get_seekBarPortrait_14() const { return ___seekBarPortrait_14; }
	inline Slider_t3903728902 ** get_address_of_seekBarPortrait_14() { return &___seekBarPortrait_14; }
	inline void set_seekBarPortrait_14(Slider_t3903728902 * value)
	{
		___seekBarPortrait_14 = value;
		Il2CppCodeGenWriteBarrier((&___seekBarPortrait_14), value);
	}

	inline static int32_t get_offset_of_playButtonLandscape_15() { return static_cast<int32_t>(offsetof(FullscreenVideoPlayer_t1619346304, ___playButtonLandscape_15)); }
	inline GameObject_t1113636619 * get_playButtonLandscape_15() const { return ___playButtonLandscape_15; }
	inline GameObject_t1113636619 ** get_address_of_playButtonLandscape_15() { return &___playButtonLandscape_15; }
	inline void set_playButtonLandscape_15(GameObject_t1113636619 * value)
	{
		___playButtonLandscape_15 = value;
		Il2CppCodeGenWriteBarrier((&___playButtonLandscape_15), value);
	}

	inline static int32_t get_offset_of_playButtonPortrait_16() { return static_cast<int32_t>(offsetof(FullscreenVideoPlayer_t1619346304, ___playButtonPortrait_16)); }
	inline GameObject_t1113636619 * get_playButtonPortrait_16() const { return ___playButtonPortrait_16; }
	inline GameObject_t1113636619 ** get_address_of_playButtonPortrait_16() { return &___playButtonPortrait_16; }
	inline void set_playButtonPortrait_16(GameObject_t1113636619 * value)
	{
		___playButtonPortrait_16 = value;
		Il2CppCodeGenWriteBarrier((&___playButtonPortrait_16), value);
	}

	inline static int32_t get_offset_of_pauseButtonLandscape_17() { return static_cast<int32_t>(offsetof(FullscreenVideoPlayer_t1619346304, ___pauseButtonLandscape_17)); }
	inline GameObject_t1113636619 * get_pauseButtonLandscape_17() const { return ___pauseButtonLandscape_17; }
	inline GameObject_t1113636619 ** get_address_of_pauseButtonLandscape_17() { return &___pauseButtonLandscape_17; }
	inline void set_pauseButtonLandscape_17(GameObject_t1113636619 * value)
	{
		___pauseButtonLandscape_17 = value;
		Il2CppCodeGenWriteBarrier((&___pauseButtonLandscape_17), value);
	}

	inline static int32_t get_offset_of_pauseButtonPortrait_18() { return static_cast<int32_t>(offsetof(FullscreenVideoPlayer_t1619346304, ___pauseButtonPortrait_18)); }
	inline GameObject_t1113636619 * get_pauseButtonPortrait_18() const { return ___pauseButtonPortrait_18; }
	inline GameObject_t1113636619 ** get_address_of_pauseButtonPortrait_18() { return &___pauseButtonPortrait_18; }
	inline void set_pauseButtonPortrait_18(GameObject_t1113636619 * value)
	{
		___pauseButtonPortrait_18 = value;
		Il2CppCodeGenWriteBarrier((&___pauseButtonPortrait_18), value);
	}

	inline static int32_t get_offset_of_panelLandscape_19() { return static_cast<int32_t>(offsetof(FullscreenVideoPlayer_t1619346304, ___panelLandscape_19)); }
	inline GameObject_t1113636619 * get_panelLandscape_19() const { return ___panelLandscape_19; }
	inline GameObject_t1113636619 ** get_address_of_panelLandscape_19() { return &___panelLandscape_19; }
	inline void set_panelLandscape_19(GameObject_t1113636619 * value)
	{
		___panelLandscape_19 = value;
		Il2CppCodeGenWriteBarrier((&___panelLandscape_19), value);
	}

	inline static int32_t get_offset_of_panelPortrait_20() { return static_cast<int32_t>(offsetof(FullscreenVideoPlayer_t1619346304, ___panelPortrait_20)); }
	inline GameObject_t1113636619 * get_panelPortrait_20() const { return ___panelPortrait_20; }
	inline GameObject_t1113636619 ** get_address_of_panelPortrait_20() { return &___panelPortrait_20; }
	inline void set_panelPortrait_20(GameObject_t1113636619 * value)
	{
		___panelPortrait_20 = value;
		Il2CppCodeGenWriteBarrier((&___panelPortrait_20), value);
	}

	inline static int32_t get_offset_of_contentManager_21() { return static_cast<int32_t>(offsetof(FullscreenVideoPlayer_t1619346304, ___contentManager_21)); }
	inline ContentManager2_t2980555998 * get_contentManager_21() const { return ___contentManager_21; }
	inline ContentManager2_t2980555998 ** get_address_of_contentManager_21() { return &___contentManager_21; }
	inline void set_contentManager_21(ContentManager2_t2980555998 * value)
	{
		___contentManager_21 = value;
		Il2CppCodeGenWriteBarrier((&___contentManager_21), value);
	}

	inline static int32_t get_offset_of_url_22() { return static_cast<int32_t>(offsetof(FullscreenVideoPlayer_t1619346304, ___url_22)); }
	inline String_t* get_url_22() const { return ___url_22; }
	inline String_t** get_address_of_url_22() { return &___url_22; }
	inline void set_url_22(String_t* value)
	{
		___url_22 = value;
		Il2CppCodeGenWriteBarrier((&___url_22), value);
	}

	inline static int32_t get_offset_of_isLandscape_23() { return static_cast<int32_t>(offsetof(FullscreenVideoPlayer_t1619346304, ___isLandscape_23)); }
	inline bool get_isLandscape_23() const { return ___isLandscape_23; }
	inline bool* get_address_of_isLandscape_23() { return &___isLandscape_23; }
	inline void set_isLandscape_23(bool value)
	{
		___isLandscape_23 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FULLSCREENVIDEOPLAYER_T1619346304_H
#ifndef GPUINSTANCINGTEST_T3692336574_H
#define GPUINSTANCINGTEST_T3692336574_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GPUInstancingTest
struct  GPUInstancingTest_t3692336574  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Transform GPUInstancingTest::prefab
	Transform_t3600365921 * ___prefab_4;
	// System.Int32 GPUInstancingTest::instances
	int32_t ___instances_5;
	// System.Single GPUInstancingTest::radius
	float ___radius_6;

public:
	inline static int32_t get_offset_of_prefab_4() { return static_cast<int32_t>(offsetof(GPUInstancingTest_t3692336574, ___prefab_4)); }
	inline Transform_t3600365921 * get_prefab_4() const { return ___prefab_4; }
	inline Transform_t3600365921 ** get_address_of_prefab_4() { return &___prefab_4; }
	inline void set_prefab_4(Transform_t3600365921 * value)
	{
		___prefab_4 = value;
		Il2CppCodeGenWriteBarrier((&___prefab_4), value);
	}

	inline static int32_t get_offset_of_instances_5() { return static_cast<int32_t>(offsetof(GPUInstancingTest_t3692336574, ___instances_5)); }
	inline int32_t get_instances_5() const { return ___instances_5; }
	inline int32_t* get_address_of_instances_5() { return &___instances_5; }
	inline void set_instances_5(int32_t value)
	{
		___instances_5 = value;
	}

	inline static int32_t get_offset_of_radius_6() { return static_cast<int32_t>(offsetof(GPUInstancingTest_t3692336574, ___radius_6)); }
	inline float get_radius_6() const { return ___radius_6; }
	inline float* get_address_of_radius_6() { return &___radius_6; }
	inline void set_radius_6(float value)
	{
		___radius_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GPUINSTANCINGTEST_T3692336574_H
#ifndef GYROMANAGER_T2156710008_H
#define GYROMANAGER_T2156710008_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GyroManager
struct  GyroManager_t2156710008  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject[] GyroManager::quads
	GameObjectU5BU5D_t3328599146* ___quads_4;
	// UnityEngine.Texture[] GyroManager::labels
	TextureU5BU5D_t908295702* ___labels_5;
	// UnityEngine.Camera GyroManager::cam
	Camera_t4157153871 * ___cam_6;

public:
	inline static int32_t get_offset_of_quads_4() { return static_cast<int32_t>(offsetof(GyroManager_t2156710008, ___quads_4)); }
	inline GameObjectU5BU5D_t3328599146* get_quads_4() const { return ___quads_4; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_quads_4() { return &___quads_4; }
	inline void set_quads_4(GameObjectU5BU5D_t3328599146* value)
	{
		___quads_4 = value;
		Il2CppCodeGenWriteBarrier((&___quads_4), value);
	}

	inline static int32_t get_offset_of_labels_5() { return static_cast<int32_t>(offsetof(GyroManager_t2156710008, ___labels_5)); }
	inline TextureU5BU5D_t908295702* get_labels_5() const { return ___labels_5; }
	inline TextureU5BU5D_t908295702** get_address_of_labels_5() { return &___labels_5; }
	inline void set_labels_5(TextureU5BU5D_t908295702* value)
	{
		___labels_5 = value;
		Il2CppCodeGenWriteBarrier((&___labels_5), value);
	}

	inline static int32_t get_offset_of_cam_6() { return static_cast<int32_t>(offsetof(GyroManager_t2156710008, ___cam_6)); }
	inline Camera_t4157153871 * get_cam_6() const { return ___cam_6; }
	inline Camera_t4157153871 ** get_address_of_cam_6() { return &___cam_6; }
	inline void set_cam_6(Camera_t4157153871 * value)
	{
		___cam_6 = value;
		Il2CppCodeGenWriteBarrier((&___cam_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GYROMANAGER_T2156710008_H
#ifndef HELPMANAGER_T1803709799_H
#define HELPMANAGER_T1803709799_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HelpManager
struct  HelpManager_t1803709799  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject HelpManager::panel1
	GameObject_t1113636619 * ___panel1_4;
	// UnityEngine.GameObject HelpManager::panel2
	GameObject_t1113636619 * ___panel2_5;
	// UnityEngine.GameObject HelpManager::panel3
	GameObject_t1113636619 * ___panel3_6;
	// UnityEngine.GameObject HelpManager::panel4
	GameObject_t1113636619 * ___panel4_7;
	// UnityEngine.UI.Text HelpManager::help1_text
	Text_t1901882714 * ___help1_text_8;
	// UnityEngine.UI.Text HelpManager::help2_text
	Text_t1901882714 * ___help2_text_9;
	// UnityEngine.UI.Text HelpManager::help3_text
	Text_t1901882714 * ___help3_text_10;
	// UnityEngine.UI.Text HelpManager::help4_text
	Text_t1901882714 * ___help4_text_11;
	// UnityEngine.UI.Text HelpManager::help5_text
	Text_t1901882714 * ___help5_text_12;
	// UnityEngine.UI.Text HelpManager::help7_text
	Text_t1901882714 * ___help7_text_13;
	// UnityEngine.UI.Text HelpManager::help9_text
	Text_t1901882714 * ___help9_text_14;
	// System.String HelpManager::help1
	String_t* ___help1_15;
	// System.String HelpManager::help2
	String_t* ___help2_16;
	// System.String HelpManager::help3
	String_t* ___help3_17;
	// System.String HelpManager::help4
	String_t* ___help4_18;
	// System.String HelpManager::help5
	String_t* ___help5_19;
	// System.String HelpManager::help6
	String_t* ___help6_20;
	// System.String HelpManager::help7
	String_t* ___help7_21;
	// System.String HelpManager::help8
	String_t* ___help8_22;
	// System.String HelpManager::help9
	String_t* ___help9_23;
	// System.String HelpManager::help10
	String_t* ___help10_24;
	// UnityEngine.Vector2 HelpManager::firstPressPos
	Vector2_t2156229523  ___firstPressPos_25;
	// UnityEngine.Vector2 HelpManager::secondPressPos
	Vector2_t2156229523  ___secondPressPos_26;
	// UnityEngine.Vector2 HelpManager::currentSwipe
	Vector2_t2156229523  ___currentSwipe_27;
	// System.Int32 HelpManager::currentPage
	int32_t ___currentPage_28;

public:
	inline static int32_t get_offset_of_panel1_4() { return static_cast<int32_t>(offsetof(HelpManager_t1803709799, ___panel1_4)); }
	inline GameObject_t1113636619 * get_panel1_4() const { return ___panel1_4; }
	inline GameObject_t1113636619 ** get_address_of_panel1_4() { return &___panel1_4; }
	inline void set_panel1_4(GameObject_t1113636619 * value)
	{
		___panel1_4 = value;
		Il2CppCodeGenWriteBarrier((&___panel1_4), value);
	}

	inline static int32_t get_offset_of_panel2_5() { return static_cast<int32_t>(offsetof(HelpManager_t1803709799, ___panel2_5)); }
	inline GameObject_t1113636619 * get_panel2_5() const { return ___panel2_5; }
	inline GameObject_t1113636619 ** get_address_of_panel2_5() { return &___panel2_5; }
	inline void set_panel2_5(GameObject_t1113636619 * value)
	{
		___panel2_5 = value;
		Il2CppCodeGenWriteBarrier((&___panel2_5), value);
	}

	inline static int32_t get_offset_of_panel3_6() { return static_cast<int32_t>(offsetof(HelpManager_t1803709799, ___panel3_6)); }
	inline GameObject_t1113636619 * get_panel3_6() const { return ___panel3_6; }
	inline GameObject_t1113636619 ** get_address_of_panel3_6() { return &___panel3_6; }
	inline void set_panel3_6(GameObject_t1113636619 * value)
	{
		___panel3_6 = value;
		Il2CppCodeGenWriteBarrier((&___panel3_6), value);
	}

	inline static int32_t get_offset_of_panel4_7() { return static_cast<int32_t>(offsetof(HelpManager_t1803709799, ___panel4_7)); }
	inline GameObject_t1113636619 * get_panel4_7() const { return ___panel4_7; }
	inline GameObject_t1113636619 ** get_address_of_panel4_7() { return &___panel4_7; }
	inline void set_panel4_7(GameObject_t1113636619 * value)
	{
		___panel4_7 = value;
		Il2CppCodeGenWriteBarrier((&___panel4_7), value);
	}

	inline static int32_t get_offset_of_help1_text_8() { return static_cast<int32_t>(offsetof(HelpManager_t1803709799, ___help1_text_8)); }
	inline Text_t1901882714 * get_help1_text_8() const { return ___help1_text_8; }
	inline Text_t1901882714 ** get_address_of_help1_text_8() { return &___help1_text_8; }
	inline void set_help1_text_8(Text_t1901882714 * value)
	{
		___help1_text_8 = value;
		Il2CppCodeGenWriteBarrier((&___help1_text_8), value);
	}

	inline static int32_t get_offset_of_help2_text_9() { return static_cast<int32_t>(offsetof(HelpManager_t1803709799, ___help2_text_9)); }
	inline Text_t1901882714 * get_help2_text_9() const { return ___help2_text_9; }
	inline Text_t1901882714 ** get_address_of_help2_text_9() { return &___help2_text_9; }
	inline void set_help2_text_9(Text_t1901882714 * value)
	{
		___help2_text_9 = value;
		Il2CppCodeGenWriteBarrier((&___help2_text_9), value);
	}

	inline static int32_t get_offset_of_help3_text_10() { return static_cast<int32_t>(offsetof(HelpManager_t1803709799, ___help3_text_10)); }
	inline Text_t1901882714 * get_help3_text_10() const { return ___help3_text_10; }
	inline Text_t1901882714 ** get_address_of_help3_text_10() { return &___help3_text_10; }
	inline void set_help3_text_10(Text_t1901882714 * value)
	{
		___help3_text_10 = value;
		Il2CppCodeGenWriteBarrier((&___help3_text_10), value);
	}

	inline static int32_t get_offset_of_help4_text_11() { return static_cast<int32_t>(offsetof(HelpManager_t1803709799, ___help4_text_11)); }
	inline Text_t1901882714 * get_help4_text_11() const { return ___help4_text_11; }
	inline Text_t1901882714 ** get_address_of_help4_text_11() { return &___help4_text_11; }
	inline void set_help4_text_11(Text_t1901882714 * value)
	{
		___help4_text_11 = value;
		Il2CppCodeGenWriteBarrier((&___help4_text_11), value);
	}

	inline static int32_t get_offset_of_help5_text_12() { return static_cast<int32_t>(offsetof(HelpManager_t1803709799, ___help5_text_12)); }
	inline Text_t1901882714 * get_help5_text_12() const { return ___help5_text_12; }
	inline Text_t1901882714 ** get_address_of_help5_text_12() { return &___help5_text_12; }
	inline void set_help5_text_12(Text_t1901882714 * value)
	{
		___help5_text_12 = value;
		Il2CppCodeGenWriteBarrier((&___help5_text_12), value);
	}

	inline static int32_t get_offset_of_help7_text_13() { return static_cast<int32_t>(offsetof(HelpManager_t1803709799, ___help7_text_13)); }
	inline Text_t1901882714 * get_help7_text_13() const { return ___help7_text_13; }
	inline Text_t1901882714 ** get_address_of_help7_text_13() { return &___help7_text_13; }
	inline void set_help7_text_13(Text_t1901882714 * value)
	{
		___help7_text_13 = value;
		Il2CppCodeGenWriteBarrier((&___help7_text_13), value);
	}

	inline static int32_t get_offset_of_help9_text_14() { return static_cast<int32_t>(offsetof(HelpManager_t1803709799, ___help9_text_14)); }
	inline Text_t1901882714 * get_help9_text_14() const { return ___help9_text_14; }
	inline Text_t1901882714 ** get_address_of_help9_text_14() { return &___help9_text_14; }
	inline void set_help9_text_14(Text_t1901882714 * value)
	{
		___help9_text_14 = value;
		Il2CppCodeGenWriteBarrier((&___help9_text_14), value);
	}

	inline static int32_t get_offset_of_help1_15() { return static_cast<int32_t>(offsetof(HelpManager_t1803709799, ___help1_15)); }
	inline String_t* get_help1_15() const { return ___help1_15; }
	inline String_t** get_address_of_help1_15() { return &___help1_15; }
	inline void set_help1_15(String_t* value)
	{
		___help1_15 = value;
		Il2CppCodeGenWriteBarrier((&___help1_15), value);
	}

	inline static int32_t get_offset_of_help2_16() { return static_cast<int32_t>(offsetof(HelpManager_t1803709799, ___help2_16)); }
	inline String_t* get_help2_16() const { return ___help2_16; }
	inline String_t** get_address_of_help2_16() { return &___help2_16; }
	inline void set_help2_16(String_t* value)
	{
		___help2_16 = value;
		Il2CppCodeGenWriteBarrier((&___help2_16), value);
	}

	inline static int32_t get_offset_of_help3_17() { return static_cast<int32_t>(offsetof(HelpManager_t1803709799, ___help3_17)); }
	inline String_t* get_help3_17() const { return ___help3_17; }
	inline String_t** get_address_of_help3_17() { return &___help3_17; }
	inline void set_help3_17(String_t* value)
	{
		___help3_17 = value;
		Il2CppCodeGenWriteBarrier((&___help3_17), value);
	}

	inline static int32_t get_offset_of_help4_18() { return static_cast<int32_t>(offsetof(HelpManager_t1803709799, ___help4_18)); }
	inline String_t* get_help4_18() const { return ___help4_18; }
	inline String_t** get_address_of_help4_18() { return &___help4_18; }
	inline void set_help4_18(String_t* value)
	{
		___help4_18 = value;
		Il2CppCodeGenWriteBarrier((&___help4_18), value);
	}

	inline static int32_t get_offset_of_help5_19() { return static_cast<int32_t>(offsetof(HelpManager_t1803709799, ___help5_19)); }
	inline String_t* get_help5_19() const { return ___help5_19; }
	inline String_t** get_address_of_help5_19() { return &___help5_19; }
	inline void set_help5_19(String_t* value)
	{
		___help5_19 = value;
		Il2CppCodeGenWriteBarrier((&___help5_19), value);
	}

	inline static int32_t get_offset_of_help6_20() { return static_cast<int32_t>(offsetof(HelpManager_t1803709799, ___help6_20)); }
	inline String_t* get_help6_20() const { return ___help6_20; }
	inline String_t** get_address_of_help6_20() { return &___help6_20; }
	inline void set_help6_20(String_t* value)
	{
		___help6_20 = value;
		Il2CppCodeGenWriteBarrier((&___help6_20), value);
	}

	inline static int32_t get_offset_of_help7_21() { return static_cast<int32_t>(offsetof(HelpManager_t1803709799, ___help7_21)); }
	inline String_t* get_help7_21() const { return ___help7_21; }
	inline String_t** get_address_of_help7_21() { return &___help7_21; }
	inline void set_help7_21(String_t* value)
	{
		___help7_21 = value;
		Il2CppCodeGenWriteBarrier((&___help7_21), value);
	}

	inline static int32_t get_offset_of_help8_22() { return static_cast<int32_t>(offsetof(HelpManager_t1803709799, ___help8_22)); }
	inline String_t* get_help8_22() const { return ___help8_22; }
	inline String_t** get_address_of_help8_22() { return &___help8_22; }
	inline void set_help8_22(String_t* value)
	{
		___help8_22 = value;
		Il2CppCodeGenWriteBarrier((&___help8_22), value);
	}

	inline static int32_t get_offset_of_help9_23() { return static_cast<int32_t>(offsetof(HelpManager_t1803709799, ___help9_23)); }
	inline String_t* get_help9_23() const { return ___help9_23; }
	inline String_t** get_address_of_help9_23() { return &___help9_23; }
	inline void set_help9_23(String_t* value)
	{
		___help9_23 = value;
		Il2CppCodeGenWriteBarrier((&___help9_23), value);
	}

	inline static int32_t get_offset_of_help10_24() { return static_cast<int32_t>(offsetof(HelpManager_t1803709799, ___help10_24)); }
	inline String_t* get_help10_24() const { return ___help10_24; }
	inline String_t** get_address_of_help10_24() { return &___help10_24; }
	inline void set_help10_24(String_t* value)
	{
		___help10_24 = value;
		Il2CppCodeGenWriteBarrier((&___help10_24), value);
	}

	inline static int32_t get_offset_of_firstPressPos_25() { return static_cast<int32_t>(offsetof(HelpManager_t1803709799, ___firstPressPos_25)); }
	inline Vector2_t2156229523  get_firstPressPos_25() const { return ___firstPressPos_25; }
	inline Vector2_t2156229523 * get_address_of_firstPressPos_25() { return &___firstPressPos_25; }
	inline void set_firstPressPos_25(Vector2_t2156229523  value)
	{
		___firstPressPos_25 = value;
	}

	inline static int32_t get_offset_of_secondPressPos_26() { return static_cast<int32_t>(offsetof(HelpManager_t1803709799, ___secondPressPos_26)); }
	inline Vector2_t2156229523  get_secondPressPos_26() const { return ___secondPressPos_26; }
	inline Vector2_t2156229523 * get_address_of_secondPressPos_26() { return &___secondPressPos_26; }
	inline void set_secondPressPos_26(Vector2_t2156229523  value)
	{
		___secondPressPos_26 = value;
	}

	inline static int32_t get_offset_of_currentSwipe_27() { return static_cast<int32_t>(offsetof(HelpManager_t1803709799, ___currentSwipe_27)); }
	inline Vector2_t2156229523  get_currentSwipe_27() const { return ___currentSwipe_27; }
	inline Vector2_t2156229523 * get_address_of_currentSwipe_27() { return &___currentSwipe_27; }
	inline void set_currentSwipe_27(Vector2_t2156229523  value)
	{
		___currentSwipe_27 = value;
	}

	inline static int32_t get_offset_of_currentPage_28() { return static_cast<int32_t>(offsetof(HelpManager_t1803709799, ___currentPage_28)); }
	inline int32_t get_currentPage_28() const { return ___currentPage_28; }
	inline int32_t* get_address_of_currentPage_28() { return &___currentPage_28; }
	inline void set_currentPage_28(int32_t value)
	{
		___currentPage_28 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HELPMANAGER_T1803709799_H
#ifndef IMAGECONTENT_T959306442_H
#define IMAGECONTENT_T959306442_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ImageContent
struct  ImageContent_t959306442  : public MonoBehaviour_t3962482529
{
public:
	// System.String ImageContent::urlImage
	String_t* ___urlImage_4;
	// UnityEngine.GameObject ImageContent::plane
	GameObject_t1113636619 * ___plane_5;
	// System.Boolean ImageContent::canClick
	bool ___canClick_6;
	// ContentManager2 ImageContent::contentManager
	ContentManager2_t2980555998 * ___contentManager_7;
	// UnityEngine.RaycastHit ImageContent::hit
	RaycastHit_t1056001966  ___hit_8;
	// UnityEngine.Ray ImageContent::ray
	Ray_t3785851493  ___ray_9;
	// UnityEngine.RaycastHit[] ImageContent::hitAllTemp
	RaycastHitU5BU5D_t1690781147* ___hitAllTemp_10;
	// UnityEngine.Collider ImageContent::collider
	Collider_t1773347010 * ___collider_11;
	// UnityEngine.UI.RawImage ImageContent::rawImage
	RawImage_t3182918964 * ___rawImage_12;
	// UnityEngine.UI.RawImage ImageContent::rawImageLandscape
	RawImage_t3182918964 * ___rawImageLandscape_13;
	// UnityEngine.GameObject ImageContent::panelBack
	GameObject_t1113636619 * ___panelBack_14;

public:
	inline static int32_t get_offset_of_urlImage_4() { return static_cast<int32_t>(offsetof(ImageContent_t959306442, ___urlImage_4)); }
	inline String_t* get_urlImage_4() const { return ___urlImage_4; }
	inline String_t** get_address_of_urlImage_4() { return &___urlImage_4; }
	inline void set_urlImage_4(String_t* value)
	{
		___urlImage_4 = value;
		Il2CppCodeGenWriteBarrier((&___urlImage_4), value);
	}

	inline static int32_t get_offset_of_plane_5() { return static_cast<int32_t>(offsetof(ImageContent_t959306442, ___plane_5)); }
	inline GameObject_t1113636619 * get_plane_5() const { return ___plane_5; }
	inline GameObject_t1113636619 ** get_address_of_plane_5() { return &___plane_5; }
	inline void set_plane_5(GameObject_t1113636619 * value)
	{
		___plane_5 = value;
		Il2CppCodeGenWriteBarrier((&___plane_5), value);
	}

	inline static int32_t get_offset_of_canClick_6() { return static_cast<int32_t>(offsetof(ImageContent_t959306442, ___canClick_6)); }
	inline bool get_canClick_6() const { return ___canClick_6; }
	inline bool* get_address_of_canClick_6() { return &___canClick_6; }
	inline void set_canClick_6(bool value)
	{
		___canClick_6 = value;
	}

	inline static int32_t get_offset_of_contentManager_7() { return static_cast<int32_t>(offsetof(ImageContent_t959306442, ___contentManager_7)); }
	inline ContentManager2_t2980555998 * get_contentManager_7() const { return ___contentManager_7; }
	inline ContentManager2_t2980555998 ** get_address_of_contentManager_7() { return &___contentManager_7; }
	inline void set_contentManager_7(ContentManager2_t2980555998 * value)
	{
		___contentManager_7 = value;
		Il2CppCodeGenWriteBarrier((&___contentManager_7), value);
	}

	inline static int32_t get_offset_of_hit_8() { return static_cast<int32_t>(offsetof(ImageContent_t959306442, ___hit_8)); }
	inline RaycastHit_t1056001966  get_hit_8() const { return ___hit_8; }
	inline RaycastHit_t1056001966 * get_address_of_hit_8() { return &___hit_8; }
	inline void set_hit_8(RaycastHit_t1056001966  value)
	{
		___hit_8 = value;
	}

	inline static int32_t get_offset_of_ray_9() { return static_cast<int32_t>(offsetof(ImageContent_t959306442, ___ray_9)); }
	inline Ray_t3785851493  get_ray_9() const { return ___ray_9; }
	inline Ray_t3785851493 * get_address_of_ray_9() { return &___ray_9; }
	inline void set_ray_9(Ray_t3785851493  value)
	{
		___ray_9 = value;
	}

	inline static int32_t get_offset_of_hitAllTemp_10() { return static_cast<int32_t>(offsetof(ImageContent_t959306442, ___hitAllTemp_10)); }
	inline RaycastHitU5BU5D_t1690781147* get_hitAllTemp_10() const { return ___hitAllTemp_10; }
	inline RaycastHitU5BU5D_t1690781147** get_address_of_hitAllTemp_10() { return &___hitAllTemp_10; }
	inline void set_hitAllTemp_10(RaycastHitU5BU5D_t1690781147* value)
	{
		___hitAllTemp_10 = value;
		Il2CppCodeGenWriteBarrier((&___hitAllTemp_10), value);
	}

	inline static int32_t get_offset_of_collider_11() { return static_cast<int32_t>(offsetof(ImageContent_t959306442, ___collider_11)); }
	inline Collider_t1773347010 * get_collider_11() const { return ___collider_11; }
	inline Collider_t1773347010 ** get_address_of_collider_11() { return &___collider_11; }
	inline void set_collider_11(Collider_t1773347010 * value)
	{
		___collider_11 = value;
		Il2CppCodeGenWriteBarrier((&___collider_11), value);
	}

	inline static int32_t get_offset_of_rawImage_12() { return static_cast<int32_t>(offsetof(ImageContent_t959306442, ___rawImage_12)); }
	inline RawImage_t3182918964 * get_rawImage_12() const { return ___rawImage_12; }
	inline RawImage_t3182918964 ** get_address_of_rawImage_12() { return &___rawImage_12; }
	inline void set_rawImage_12(RawImage_t3182918964 * value)
	{
		___rawImage_12 = value;
		Il2CppCodeGenWriteBarrier((&___rawImage_12), value);
	}

	inline static int32_t get_offset_of_rawImageLandscape_13() { return static_cast<int32_t>(offsetof(ImageContent_t959306442, ___rawImageLandscape_13)); }
	inline RawImage_t3182918964 * get_rawImageLandscape_13() const { return ___rawImageLandscape_13; }
	inline RawImage_t3182918964 ** get_address_of_rawImageLandscape_13() { return &___rawImageLandscape_13; }
	inline void set_rawImageLandscape_13(RawImage_t3182918964 * value)
	{
		___rawImageLandscape_13 = value;
		Il2CppCodeGenWriteBarrier((&___rawImageLandscape_13), value);
	}

	inline static int32_t get_offset_of_panelBack_14() { return static_cast<int32_t>(offsetof(ImageContent_t959306442, ___panelBack_14)); }
	inline GameObject_t1113636619 * get_panelBack_14() const { return ___panelBack_14; }
	inline GameObject_t1113636619 ** get_address_of_panelBack_14() { return &___panelBack_14; }
	inline void set_panelBack_14(GameObject_t1113636619 * value)
	{
		___panelBack_14 = value;
		Il2CppCodeGenWriteBarrier((&___panelBack_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMAGECONTENT_T959306442_H
#ifndef IMAGEWIDGET_T3173080225_H
#define IMAGEWIDGET_T3173080225_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ImageWidget
struct  ImageWidget_t3173080225  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject ImageWidget::imageLandscape
	GameObject_t1113636619 * ___imageLandscape_4;
	// UnityEngine.GameObject ImageWidget::imagePortrait
	GameObject_t1113636619 * ___imagePortrait_5;
	// UnityEngine.GameObject ImageWidget::panelBack
	GameObject_t1113636619 * ___panelBack_6;
	// ContentManager2 ImageWidget::contentManager
	ContentManager2_t2980555998 * ___contentManager_7;

public:
	inline static int32_t get_offset_of_imageLandscape_4() { return static_cast<int32_t>(offsetof(ImageWidget_t3173080225, ___imageLandscape_4)); }
	inline GameObject_t1113636619 * get_imageLandscape_4() const { return ___imageLandscape_4; }
	inline GameObject_t1113636619 ** get_address_of_imageLandscape_4() { return &___imageLandscape_4; }
	inline void set_imageLandscape_4(GameObject_t1113636619 * value)
	{
		___imageLandscape_4 = value;
		Il2CppCodeGenWriteBarrier((&___imageLandscape_4), value);
	}

	inline static int32_t get_offset_of_imagePortrait_5() { return static_cast<int32_t>(offsetof(ImageWidget_t3173080225, ___imagePortrait_5)); }
	inline GameObject_t1113636619 * get_imagePortrait_5() const { return ___imagePortrait_5; }
	inline GameObject_t1113636619 ** get_address_of_imagePortrait_5() { return &___imagePortrait_5; }
	inline void set_imagePortrait_5(GameObject_t1113636619 * value)
	{
		___imagePortrait_5 = value;
		Il2CppCodeGenWriteBarrier((&___imagePortrait_5), value);
	}

	inline static int32_t get_offset_of_panelBack_6() { return static_cast<int32_t>(offsetof(ImageWidget_t3173080225, ___panelBack_6)); }
	inline GameObject_t1113636619 * get_panelBack_6() const { return ___panelBack_6; }
	inline GameObject_t1113636619 ** get_address_of_panelBack_6() { return &___panelBack_6; }
	inline void set_panelBack_6(GameObject_t1113636619 * value)
	{
		___panelBack_6 = value;
		Il2CppCodeGenWriteBarrier((&___panelBack_6), value);
	}

	inline static int32_t get_offset_of_contentManager_7() { return static_cast<int32_t>(offsetof(ImageWidget_t3173080225, ___contentManager_7)); }
	inline ContentManager2_t2980555998 * get_contentManager_7() const { return ___contentManager_7; }
	inline ContentManager2_t2980555998 ** get_address_of_contentManager_7() { return &___contentManager_7; }
	inline void set_contentManager_7(ContentManager2_t2980555998 * value)
	{
		___contentManager_7 = value;
		Il2CppCodeGenWriteBarrier((&___contentManager_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMAGEWIDGET_T3173080225_H
#ifndef LANGMANAGER_T3681335332_H
#define LANGMANAGER_T3681335332_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LangManager
struct  LangManager_t3681335332  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject LangManager::CTN1
	GameObject_t1113636619 * ___CTN1_4;
	// UnityEngine.GameObject LangManager::CTN2
	GameObject_t1113636619 * ___CTN2_5;
	// UnityEngine.GameObject LangManager::CTN3
	GameObject_t1113636619 * ___CTN3_6;
	// UnityEngine.GameObject LangManager::CTN4
	GameObject_t1113636619 * ___CTN4_7;
	// UnityEngine.GameObject LangManager::CTN5
	GameObject_t1113636619 * ___CTN5_8;
	// UnityEngine.GameObject LangManager::SV_CTN1
	GameObject_t1113636619 * ___SV_CTN1_9;
	// UnityEngine.GameObject LangManager::SV_CTN2
	GameObject_t1113636619 * ___SV_CTN2_10;
	// UnityEngine.GameObject LangManager::SV_CTN3
	GameObject_t1113636619 * ___SV_CTN3_11;
	// UnityEngine.GameObject LangManager::SV_CTN4
	GameObject_t1113636619 * ___SV_CTN4_12;
	// UnityEngine.GameObject LangManager::SV_CTN5
	GameObject_t1113636619 * ___SV_CTN5_13;
	// UnityEngine.GameObject LangManager::imageNA
	GameObject_t1113636619 * ___imageNA_14;
	// UnityEngine.UI.Image LangManager::CTN1_ca
	Image_t2670269651 * ___CTN1_ca_15;
	// UnityEngine.UI.Image LangManager::CTN1_do
	Image_t2670269651 * ___CTN1_do_16;
	// UnityEngine.UI.Image LangManager::CTN1_tt
	Image_t2670269651 * ___CTN1_tt_17;
	// UnityEngine.UI.Image LangManager::CTN1_us
	Image_t2670269651 * ___CTN1_us_18;
	// UnityEngine.UI.Image LangManager::CTN1_sv
	Image_t2670269651 * ___CTN1_sv_19;
	// UnityEngine.UI.Image LangManager::CTN1_gt
	Image_t2670269651 * ___CTN1_gt_20;
	// UnityEngine.UI.Image LangManager::CTN1_mx
	Image_t2670269651 * ___CTN1_mx_21;
	// UnityEngine.UI.Image LangManager::CTN1_ni
	Image_t2670269651 * ___CTN1_ni_22;
	// UnityEngine.UI.Image LangManager::CTN1_pa
	Image_t2670269651 * ___CTN1_pa_23;
	// UnityEngine.UI.Image LangManager::CTN2_ar
	Image_t2670269651 * ___CTN2_ar_24;
	// UnityEngine.UI.Image LangManager::CTN2_bo
	Image_t2670269651 * ___CTN2_bo_25;
	// UnityEngine.UI.Image LangManager::CTN2_br
	Image_t2670269651 * ___CTN2_br_26;
	// UnityEngine.UI.Image LangManager::CTN2_cl
	Image_t2670269651 * ___CTN2_cl_27;
	// UnityEngine.UI.Image LangManager::CTN2_co
	Image_t2670269651 * ___CTN2_co_28;
	// UnityEngine.UI.Image LangManager::CTN2_ec
	Image_t2670269651 * ___CTN2_ec_29;
	// UnityEngine.UI.Image LangManager::CTN2_py
	Image_t2670269651 * ___CTN2_py_30;
	// UnityEngine.UI.Image LangManager::CTN2_uy
	Image_t2670269651 * ___CTN2_uy_31;
	// UnityEngine.UI.Image LangManager::CTN3_gr
	Image_t2670269651 * ___CTN3_gr_32;
	// UnityEngine.UI.Image LangManager::CTN3_tr
	Image_t2670269651 * ___CTN3_tr_33;
	// UnityEngine.UI.Image LangManager::CTN3_lt
	Image_t2670269651 * ___CTN3_lt_34;
	// UnityEngine.UI.Image LangManager::CTN3_pl
	Image_t2670269651 * ___CTN3_pl_35;
	// UnityEngine.UI.Image LangManager::CTN3_ru
	Image_t2670269651 * ___CTN3_ru_36;
	// UnityEngine.UI.Image LangManager::CTN3_dk
	Image_t2670269651 * ___CTN3_dk_37;
	// UnityEngine.UI.Image LangManager::CTN3_it
	Image_t2670269651 * ___CTN3_it_38;
	// UnityEngine.UI.Image LangManager::CTN3_ch
	Image_t2670269651 * ___CTN3_ch_39;
	// UnityEngine.UI.Image LangManager::CTN3_no
	Image_t2670269651 * ___CTN3_no_40;
	// UnityEngine.UI.Image LangManager::CTN3_se
	Image_t2670269651 * ___CTN3_se_41;
	// UnityEngine.UI.Image LangManager::CTN3_at
	Image_t2670269651 * ___CTN3_at_42;
	// UnityEngine.UI.Image LangManager::CTN3_fr
	Image_t2670269651 * ___CTN3_fr_43;
	// UnityEngine.UI.Image LangManager::CTN3_de
	Image_t2670269651 * ___CTN3_de_44;
	// UnityEngine.UI.Image LangManager::CTN3_is
	Image_t2670269651 * ___CTN3_is_45;
	// UnityEngine.UI.Image LangManager::CTN3_ie
	Image_t2670269651 * ___CTN3_ie_46;
	// UnityEngine.UI.Image LangManager::CTN3_gb
	Image_t2670269651 * ___CTN3_gb_47;
	// UnityEngine.UI.Image LangManager::CTN3_es
	Image_t2670269651 * ___CTN3_es_48;
	// UnityEngine.UI.Image LangManager::CTN3_be
	Image_t2670269651 * ___CTN3_be_49;
	// UnityEngine.UI.Image LangManager::CTN3_nl
	Image_t2670269651 * ___CTN3_nl_50;
	// UnityEngine.UI.Image LangManager::CTN4_cn
	Image_t2670269651 * ___CTN4_cn_51;
	// UnityEngine.UI.Image LangManager::CTN4_in
	Image_t2670269651 * ___CTN4_in_52;
	// UnityEngine.UI.Image LangManager::CTN4_jp
	Image_t2670269651 * ___CTN4_jp_53;
	// UnityEngine.UI.Image LangManager::CTN4_kr
	Image_t2670269651 * ___CTN4_kr_54;
	// UnityEngine.UI.Image LangManager::CTN4_pk
	Image_t2670269651 * ___CTN4_pk_55;
	// UnityEngine.UI.Image LangManager::CTN4_sg
	Image_t2670269651 * ___CTN4_sg_56;
	// UnityEngine.UI.Image LangManager::CTN4_sr
	Image_t2670269651 * ___CTN4_sr_57;
	// UnityEngine.UI.Image LangManager::CTN5_au
	Image_t2670269651 * ___CTN5_au_58;
	// UnityEngine.UI.Image LangManager::CTN5_nz
	Image_t2670269651 * ___CTN5_nz_59;
	// UnityEngine.GameObject LangManager::button1
	GameObject_t1113636619 * ___button1_60;
	// UnityEngine.GameObject LangManager::button2
	GameObject_t1113636619 * ___button2_61;
	// UnityEngine.GameObject LangManager::button3
	GameObject_t1113636619 * ___button3_62;
	// UnityEngine.GameObject LangManager::panelConfirmation
	GameObject_t1113636619 * ___panelConfirmation_63;
	// UnityEngine.UI.Text LangManager::confirmationText
	Text_t1901882714 * ___confirmationText_64;
	// UnityEngine.UI.Image LangManager::confirmationFlag
	Image_t2670269651 * ___confirmationFlag_65;
	// UnityEngine.GameObject LangManager::buttonClose
	GameObject_t1113636619 * ___buttonClose_66;
	// System.Int32 LangManager::selectedLanguageID
	int32_t ___selectedLanguageID_67;
	// UnityEngine.UI.Image[] LangManager::allCTN1_flags
	ImageU5BU5D_t2439009922* ___allCTN1_flags_68;
	// UnityEngine.UI.Image[] LangManager::allCTN2_flags
	ImageU5BU5D_t2439009922* ___allCTN2_flags_69;
	// UnityEngine.UI.Image[] LangManager::allCTN3_flags
	ImageU5BU5D_t2439009922* ___allCTN3_flags_70;
	// UnityEngine.UI.Image[] LangManager::allCTN4_flags
	ImageU5BU5D_t2439009922* ___allCTN4_flags_71;
	// UnityEngine.UI.Image[] LangManager::allCTN5_flags
	ImageU5BU5D_t2439009922* ___allCTN5_flags_72;

public:
	inline static int32_t get_offset_of_CTN1_4() { return static_cast<int32_t>(offsetof(LangManager_t3681335332, ___CTN1_4)); }
	inline GameObject_t1113636619 * get_CTN1_4() const { return ___CTN1_4; }
	inline GameObject_t1113636619 ** get_address_of_CTN1_4() { return &___CTN1_4; }
	inline void set_CTN1_4(GameObject_t1113636619 * value)
	{
		___CTN1_4 = value;
		Il2CppCodeGenWriteBarrier((&___CTN1_4), value);
	}

	inline static int32_t get_offset_of_CTN2_5() { return static_cast<int32_t>(offsetof(LangManager_t3681335332, ___CTN2_5)); }
	inline GameObject_t1113636619 * get_CTN2_5() const { return ___CTN2_5; }
	inline GameObject_t1113636619 ** get_address_of_CTN2_5() { return &___CTN2_5; }
	inline void set_CTN2_5(GameObject_t1113636619 * value)
	{
		___CTN2_5 = value;
		Il2CppCodeGenWriteBarrier((&___CTN2_5), value);
	}

	inline static int32_t get_offset_of_CTN3_6() { return static_cast<int32_t>(offsetof(LangManager_t3681335332, ___CTN3_6)); }
	inline GameObject_t1113636619 * get_CTN3_6() const { return ___CTN3_6; }
	inline GameObject_t1113636619 ** get_address_of_CTN3_6() { return &___CTN3_6; }
	inline void set_CTN3_6(GameObject_t1113636619 * value)
	{
		___CTN3_6 = value;
		Il2CppCodeGenWriteBarrier((&___CTN3_6), value);
	}

	inline static int32_t get_offset_of_CTN4_7() { return static_cast<int32_t>(offsetof(LangManager_t3681335332, ___CTN4_7)); }
	inline GameObject_t1113636619 * get_CTN4_7() const { return ___CTN4_7; }
	inline GameObject_t1113636619 ** get_address_of_CTN4_7() { return &___CTN4_7; }
	inline void set_CTN4_7(GameObject_t1113636619 * value)
	{
		___CTN4_7 = value;
		Il2CppCodeGenWriteBarrier((&___CTN4_7), value);
	}

	inline static int32_t get_offset_of_CTN5_8() { return static_cast<int32_t>(offsetof(LangManager_t3681335332, ___CTN5_8)); }
	inline GameObject_t1113636619 * get_CTN5_8() const { return ___CTN5_8; }
	inline GameObject_t1113636619 ** get_address_of_CTN5_8() { return &___CTN5_8; }
	inline void set_CTN5_8(GameObject_t1113636619 * value)
	{
		___CTN5_8 = value;
		Il2CppCodeGenWriteBarrier((&___CTN5_8), value);
	}

	inline static int32_t get_offset_of_SV_CTN1_9() { return static_cast<int32_t>(offsetof(LangManager_t3681335332, ___SV_CTN1_9)); }
	inline GameObject_t1113636619 * get_SV_CTN1_9() const { return ___SV_CTN1_9; }
	inline GameObject_t1113636619 ** get_address_of_SV_CTN1_9() { return &___SV_CTN1_9; }
	inline void set_SV_CTN1_9(GameObject_t1113636619 * value)
	{
		___SV_CTN1_9 = value;
		Il2CppCodeGenWriteBarrier((&___SV_CTN1_9), value);
	}

	inline static int32_t get_offset_of_SV_CTN2_10() { return static_cast<int32_t>(offsetof(LangManager_t3681335332, ___SV_CTN2_10)); }
	inline GameObject_t1113636619 * get_SV_CTN2_10() const { return ___SV_CTN2_10; }
	inline GameObject_t1113636619 ** get_address_of_SV_CTN2_10() { return &___SV_CTN2_10; }
	inline void set_SV_CTN2_10(GameObject_t1113636619 * value)
	{
		___SV_CTN2_10 = value;
		Il2CppCodeGenWriteBarrier((&___SV_CTN2_10), value);
	}

	inline static int32_t get_offset_of_SV_CTN3_11() { return static_cast<int32_t>(offsetof(LangManager_t3681335332, ___SV_CTN3_11)); }
	inline GameObject_t1113636619 * get_SV_CTN3_11() const { return ___SV_CTN3_11; }
	inline GameObject_t1113636619 ** get_address_of_SV_CTN3_11() { return &___SV_CTN3_11; }
	inline void set_SV_CTN3_11(GameObject_t1113636619 * value)
	{
		___SV_CTN3_11 = value;
		Il2CppCodeGenWriteBarrier((&___SV_CTN3_11), value);
	}

	inline static int32_t get_offset_of_SV_CTN4_12() { return static_cast<int32_t>(offsetof(LangManager_t3681335332, ___SV_CTN4_12)); }
	inline GameObject_t1113636619 * get_SV_CTN4_12() const { return ___SV_CTN4_12; }
	inline GameObject_t1113636619 ** get_address_of_SV_CTN4_12() { return &___SV_CTN4_12; }
	inline void set_SV_CTN4_12(GameObject_t1113636619 * value)
	{
		___SV_CTN4_12 = value;
		Il2CppCodeGenWriteBarrier((&___SV_CTN4_12), value);
	}

	inline static int32_t get_offset_of_SV_CTN5_13() { return static_cast<int32_t>(offsetof(LangManager_t3681335332, ___SV_CTN5_13)); }
	inline GameObject_t1113636619 * get_SV_CTN5_13() const { return ___SV_CTN5_13; }
	inline GameObject_t1113636619 ** get_address_of_SV_CTN5_13() { return &___SV_CTN5_13; }
	inline void set_SV_CTN5_13(GameObject_t1113636619 * value)
	{
		___SV_CTN5_13 = value;
		Il2CppCodeGenWriteBarrier((&___SV_CTN5_13), value);
	}

	inline static int32_t get_offset_of_imageNA_14() { return static_cast<int32_t>(offsetof(LangManager_t3681335332, ___imageNA_14)); }
	inline GameObject_t1113636619 * get_imageNA_14() const { return ___imageNA_14; }
	inline GameObject_t1113636619 ** get_address_of_imageNA_14() { return &___imageNA_14; }
	inline void set_imageNA_14(GameObject_t1113636619 * value)
	{
		___imageNA_14 = value;
		Il2CppCodeGenWriteBarrier((&___imageNA_14), value);
	}

	inline static int32_t get_offset_of_CTN1_ca_15() { return static_cast<int32_t>(offsetof(LangManager_t3681335332, ___CTN1_ca_15)); }
	inline Image_t2670269651 * get_CTN1_ca_15() const { return ___CTN1_ca_15; }
	inline Image_t2670269651 ** get_address_of_CTN1_ca_15() { return &___CTN1_ca_15; }
	inline void set_CTN1_ca_15(Image_t2670269651 * value)
	{
		___CTN1_ca_15 = value;
		Il2CppCodeGenWriteBarrier((&___CTN1_ca_15), value);
	}

	inline static int32_t get_offset_of_CTN1_do_16() { return static_cast<int32_t>(offsetof(LangManager_t3681335332, ___CTN1_do_16)); }
	inline Image_t2670269651 * get_CTN1_do_16() const { return ___CTN1_do_16; }
	inline Image_t2670269651 ** get_address_of_CTN1_do_16() { return &___CTN1_do_16; }
	inline void set_CTN1_do_16(Image_t2670269651 * value)
	{
		___CTN1_do_16 = value;
		Il2CppCodeGenWriteBarrier((&___CTN1_do_16), value);
	}

	inline static int32_t get_offset_of_CTN1_tt_17() { return static_cast<int32_t>(offsetof(LangManager_t3681335332, ___CTN1_tt_17)); }
	inline Image_t2670269651 * get_CTN1_tt_17() const { return ___CTN1_tt_17; }
	inline Image_t2670269651 ** get_address_of_CTN1_tt_17() { return &___CTN1_tt_17; }
	inline void set_CTN1_tt_17(Image_t2670269651 * value)
	{
		___CTN1_tt_17 = value;
		Il2CppCodeGenWriteBarrier((&___CTN1_tt_17), value);
	}

	inline static int32_t get_offset_of_CTN1_us_18() { return static_cast<int32_t>(offsetof(LangManager_t3681335332, ___CTN1_us_18)); }
	inline Image_t2670269651 * get_CTN1_us_18() const { return ___CTN1_us_18; }
	inline Image_t2670269651 ** get_address_of_CTN1_us_18() { return &___CTN1_us_18; }
	inline void set_CTN1_us_18(Image_t2670269651 * value)
	{
		___CTN1_us_18 = value;
		Il2CppCodeGenWriteBarrier((&___CTN1_us_18), value);
	}

	inline static int32_t get_offset_of_CTN1_sv_19() { return static_cast<int32_t>(offsetof(LangManager_t3681335332, ___CTN1_sv_19)); }
	inline Image_t2670269651 * get_CTN1_sv_19() const { return ___CTN1_sv_19; }
	inline Image_t2670269651 ** get_address_of_CTN1_sv_19() { return &___CTN1_sv_19; }
	inline void set_CTN1_sv_19(Image_t2670269651 * value)
	{
		___CTN1_sv_19 = value;
		Il2CppCodeGenWriteBarrier((&___CTN1_sv_19), value);
	}

	inline static int32_t get_offset_of_CTN1_gt_20() { return static_cast<int32_t>(offsetof(LangManager_t3681335332, ___CTN1_gt_20)); }
	inline Image_t2670269651 * get_CTN1_gt_20() const { return ___CTN1_gt_20; }
	inline Image_t2670269651 ** get_address_of_CTN1_gt_20() { return &___CTN1_gt_20; }
	inline void set_CTN1_gt_20(Image_t2670269651 * value)
	{
		___CTN1_gt_20 = value;
		Il2CppCodeGenWriteBarrier((&___CTN1_gt_20), value);
	}

	inline static int32_t get_offset_of_CTN1_mx_21() { return static_cast<int32_t>(offsetof(LangManager_t3681335332, ___CTN1_mx_21)); }
	inline Image_t2670269651 * get_CTN1_mx_21() const { return ___CTN1_mx_21; }
	inline Image_t2670269651 ** get_address_of_CTN1_mx_21() { return &___CTN1_mx_21; }
	inline void set_CTN1_mx_21(Image_t2670269651 * value)
	{
		___CTN1_mx_21 = value;
		Il2CppCodeGenWriteBarrier((&___CTN1_mx_21), value);
	}

	inline static int32_t get_offset_of_CTN1_ni_22() { return static_cast<int32_t>(offsetof(LangManager_t3681335332, ___CTN1_ni_22)); }
	inline Image_t2670269651 * get_CTN1_ni_22() const { return ___CTN1_ni_22; }
	inline Image_t2670269651 ** get_address_of_CTN1_ni_22() { return &___CTN1_ni_22; }
	inline void set_CTN1_ni_22(Image_t2670269651 * value)
	{
		___CTN1_ni_22 = value;
		Il2CppCodeGenWriteBarrier((&___CTN1_ni_22), value);
	}

	inline static int32_t get_offset_of_CTN1_pa_23() { return static_cast<int32_t>(offsetof(LangManager_t3681335332, ___CTN1_pa_23)); }
	inline Image_t2670269651 * get_CTN1_pa_23() const { return ___CTN1_pa_23; }
	inline Image_t2670269651 ** get_address_of_CTN1_pa_23() { return &___CTN1_pa_23; }
	inline void set_CTN1_pa_23(Image_t2670269651 * value)
	{
		___CTN1_pa_23 = value;
		Il2CppCodeGenWriteBarrier((&___CTN1_pa_23), value);
	}

	inline static int32_t get_offset_of_CTN2_ar_24() { return static_cast<int32_t>(offsetof(LangManager_t3681335332, ___CTN2_ar_24)); }
	inline Image_t2670269651 * get_CTN2_ar_24() const { return ___CTN2_ar_24; }
	inline Image_t2670269651 ** get_address_of_CTN2_ar_24() { return &___CTN2_ar_24; }
	inline void set_CTN2_ar_24(Image_t2670269651 * value)
	{
		___CTN2_ar_24 = value;
		Il2CppCodeGenWriteBarrier((&___CTN2_ar_24), value);
	}

	inline static int32_t get_offset_of_CTN2_bo_25() { return static_cast<int32_t>(offsetof(LangManager_t3681335332, ___CTN2_bo_25)); }
	inline Image_t2670269651 * get_CTN2_bo_25() const { return ___CTN2_bo_25; }
	inline Image_t2670269651 ** get_address_of_CTN2_bo_25() { return &___CTN2_bo_25; }
	inline void set_CTN2_bo_25(Image_t2670269651 * value)
	{
		___CTN2_bo_25 = value;
		Il2CppCodeGenWriteBarrier((&___CTN2_bo_25), value);
	}

	inline static int32_t get_offset_of_CTN2_br_26() { return static_cast<int32_t>(offsetof(LangManager_t3681335332, ___CTN2_br_26)); }
	inline Image_t2670269651 * get_CTN2_br_26() const { return ___CTN2_br_26; }
	inline Image_t2670269651 ** get_address_of_CTN2_br_26() { return &___CTN2_br_26; }
	inline void set_CTN2_br_26(Image_t2670269651 * value)
	{
		___CTN2_br_26 = value;
		Il2CppCodeGenWriteBarrier((&___CTN2_br_26), value);
	}

	inline static int32_t get_offset_of_CTN2_cl_27() { return static_cast<int32_t>(offsetof(LangManager_t3681335332, ___CTN2_cl_27)); }
	inline Image_t2670269651 * get_CTN2_cl_27() const { return ___CTN2_cl_27; }
	inline Image_t2670269651 ** get_address_of_CTN2_cl_27() { return &___CTN2_cl_27; }
	inline void set_CTN2_cl_27(Image_t2670269651 * value)
	{
		___CTN2_cl_27 = value;
		Il2CppCodeGenWriteBarrier((&___CTN2_cl_27), value);
	}

	inline static int32_t get_offset_of_CTN2_co_28() { return static_cast<int32_t>(offsetof(LangManager_t3681335332, ___CTN2_co_28)); }
	inline Image_t2670269651 * get_CTN2_co_28() const { return ___CTN2_co_28; }
	inline Image_t2670269651 ** get_address_of_CTN2_co_28() { return &___CTN2_co_28; }
	inline void set_CTN2_co_28(Image_t2670269651 * value)
	{
		___CTN2_co_28 = value;
		Il2CppCodeGenWriteBarrier((&___CTN2_co_28), value);
	}

	inline static int32_t get_offset_of_CTN2_ec_29() { return static_cast<int32_t>(offsetof(LangManager_t3681335332, ___CTN2_ec_29)); }
	inline Image_t2670269651 * get_CTN2_ec_29() const { return ___CTN2_ec_29; }
	inline Image_t2670269651 ** get_address_of_CTN2_ec_29() { return &___CTN2_ec_29; }
	inline void set_CTN2_ec_29(Image_t2670269651 * value)
	{
		___CTN2_ec_29 = value;
		Il2CppCodeGenWriteBarrier((&___CTN2_ec_29), value);
	}

	inline static int32_t get_offset_of_CTN2_py_30() { return static_cast<int32_t>(offsetof(LangManager_t3681335332, ___CTN2_py_30)); }
	inline Image_t2670269651 * get_CTN2_py_30() const { return ___CTN2_py_30; }
	inline Image_t2670269651 ** get_address_of_CTN2_py_30() { return &___CTN2_py_30; }
	inline void set_CTN2_py_30(Image_t2670269651 * value)
	{
		___CTN2_py_30 = value;
		Il2CppCodeGenWriteBarrier((&___CTN2_py_30), value);
	}

	inline static int32_t get_offset_of_CTN2_uy_31() { return static_cast<int32_t>(offsetof(LangManager_t3681335332, ___CTN2_uy_31)); }
	inline Image_t2670269651 * get_CTN2_uy_31() const { return ___CTN2_uy_31; }
	inline Image_t2670269651 ** get_address_of_CTN2_uy_31() { return &___CTN2_uy_31; }
	inline void set_CTN2_uy_31(Image_t2670269651 * value)
	{
		___CTN2_uy_31 = value;
		Il2CppCodeGenWriteBarrier((&___CTN2_uy_31), value);
	}

	inline static int32_t get_offset_of_CTN3_gr_32() { return static_cast<int32_t>(offsetof(LangManager_t3681335332, ___CTN3_gr_32)); }
	inline Image_t2670269651 * get_CTN3_gr_32() const { return ___CTN3_gr_32; }
	inline Image_t2670269651 ** get_address_of_CTN3_gr_32() { return &___CTN3_gr_32; }
	inline void set_CTN3_gr_32(Image_t2670269651 * value)
	{
		___CTN3_gr_32 = value;
		Il2CppCodeGenWriteBarrier((&___CTN3_gr_32), value);
	}

	inline static int32_t get_offset_of_CTN3_tr_33() { return static_cast<int32_t>(offsetof(LangManager_t3681335332, ___CTN3_tr_33)); }
	inline Image_t2670269651 * get_CTN3_tr_33() const { return ___CTN3_tr_33; }
	inline Image_t2670269651 ** get_address_of_CTN3_tr_33() { return &___CTN3_tr_33; }
	inline void set_CTN3_tr_33(Image_t2670269651 * value)
	{
		___CTN3_tr_33 = value;
		Il2CppCodeGenWriteBarrier((&___CTN3_tr_33), value);
	}

	inline static int32_t get_offset_of_CTN3_lt_34() { return static_cast<int32_t>(offsetof(LangManager_t3681335332, ___CTN3_lt_34)); }
	inline Image_t2670269651 * get_CTN3_lt_34() const { return ___CTN3_lt_34; }
	inline Image_t2670269651 ** get_address_of_CTN3_lt_34() { return &___CTN3_lt_34; }
	inline void set_CTN3_lt_34(Image_t2670269651 * value)
	{
		___CTN3_lt_34 = value;
		Il2CppCodeGenWriteBarrier((&___CTN3_lt_34), value);
	}

	inline static int32_t get_offset_of_CTN3_pl_35() { return static_cast<int32_t>(offsetof(LangManager_t3681335332, ___CTN3_pl_35)); }
	inline Image_t2670269651 * get_CTN3_pl_35() const { return ___CTN3_pl_35; }
	inline Image_t2670269651 ** get_address_of_CTN3_pl_35() { return &___CTN3_pl_35; }
	inline void set_CTN3_pl_35(Image_t2670269651 * value)
	{
		___CTN3_pl_35 = value;
		Il2CppCodeGenWriteBarrier((&___CTN3_pl_35), value);
	}

	inline static int32_t get_offset_of_CTN3_ru_36() { return static_cast<int32_t>(offsetof(LangManager_t3681335332, ___CTN3_ru_36)); }
	inline Image_t2670269651 * get_CTN3_ru_36() const { return ___CTN3_ru_36; }
	inline Image_t2670269651 ** get_address_of_CTN3_ru_36() { return &___CTN3_ru_36; }
	inline void set_CTN3_ru_36(Image_t2670269651 * value)
	{
		___CTN3_ru_36 = value;
		Il2CppCodeGenWriteBarrier((&___CTN3_ru_36), value);
	}

	inline static int32_t get_offset_of_CTN3_dk_37() { return static_cast<int32_t>(offsetof(LangManager_t3681335332, ___CTN3_dk_37)); }
	inline Image_t2670269651 * get_CTN3_dk_37() const { return ___CTN3_dk_37; }
	inline Image_t2670269651 ** get_address_of_CTN3_dk_37() { return &___CTN3_dk_37; }
	inline void set_CTN3_dk_37(Image_t2670269651 * value)
	{
		___CTN3_dk_37 = value;
		Il2CppCodeGenWriteBarrier((&___CTN3_dk_37), value);
	}

	inline static int32_t get_offset_of_CTN3_it_38() { return static_cast<int32_t>(offsetof(LangManager_t3681335332, ___CTN3_it_38)); }
	inline Image_t2670269651 * get_CTN3_it_38() const { return ___CTN3_it_38; }
	inline Image_t2670269651 ** get_address_of_CTN3_it_38() { return &___CTN3_it_38; }
	inline void set_CTN3_it_38(Image_t2670269651 * value)
	{
		___CTN3_it_38 = value;
		Il2CppCodeGenWriteBarrier((&___CTN3_it_38), value);
	}

	inline static int32_t get_offset_of_CTN3_ch_39() { return static_cast<int32_t>(offsetof(LangManager_t3681335332, ___CTN3_ch_39)); }
	inline Image_t2670269651 * get_CTN3_ch_39() const { return ___CTN3_ch_39; }
	inline Image_t2670269651 ** get_address_of_CTN3_ch_39() { return &___CTN3_ch_39; }
	inline void set_CTN3_ch_39(Image_t2670269651 * value)
	{
		___CTN3_ch_39 = value;
		Il2CppCodeGenWriteBarrier((&___CTN3_ch_39), value);
	}

	inline static int32_t get_offset_of_CTN3_no_40() { return static_cast<int32_t>(offsetof(LangManager_t3681335332, ___CTN3_no_40)); }
	inline Image_t2670269651 * get_CTN3_no_40() const { return ___CTN3_no_40; }
	inline Image_t2670269651 ** get_address_of_CTN3_no_40() { return &___CTN3_no_40; }
	inline void set_CTN3_no_40(Image_t2670269651 * value)
	{
		___CTN3_no_40 = value;
		Il2CppCodeGenWriteBarrier((&___CTN3_no_40), value);
	}

	inline static int32_t get_offset_of_CTN3_se_41() { return static_cast<int32_t>(offsetof(LangManager_t3681335332, ___CTN3_se_41)); }
	inline Image_t2670269651 * get_CTN3_se_41() const { return ___CTN3_se_41; }
	inline Image_t2670269651 ** get_address_of_CTN3_se_41() { return &___CTN3_se_41; }
	inline void set_CTN3_se_41(Image_t2670269651 * value)
	{
		___CTN3_se_41 = value;
		Il2CppCodeGenWriteBarrier((&___CTN3_se_41), value);
	}

	inline static int32_t get_offset_of_CTN3_at_42() { return static_cast<int32_t>(offsetof(LangManager_t3681335332, ___CTN3_at_42)); }
	inline Image_t2670269651 * get_CTN3_at_42() const { return ___CTN3_at_42; }
	inline Image_t2670269651 ** get_address_of_CTN3_at_42() { return &___CTN3_at_42; }
	inline void set_CTN3_at_42(Image_t2670269651 * value)
	{
		___CTN3_at_42 = value;
		Il2CppCodeGenWriteBarrier((&___CTN3_at_42), value);
	}

	inline static int32_t get_offset_of_CTN3_fr_43() { return static_cast<int32_t>(offsetof(LangManager_t3681335332, ___CTN3_fr_43)); }
	inline Image_t2670269651 * get_CTN3_fr_43() const { return ___CTN3_fr_43; }
	inline Image_t2670269651 ** get_address_of_CTN3_fr_43() { return &___CTN3_fr_43; }
	inline void set_CTN3_fr_43(Image_t2670269651 * value)
	{
		___CTN3_fr_43 = value;
		Il2CppCodeGenWriteBarrier((&___CTN3_fr_43), value);
	}

	inline static int32_t get_offset_of_CTN3_de_44() { return static_cast<int32_t>(offsetof(LangManager_t3681335332, ___CTN3_de_44)); }
	inline Image_t2670269651 * get_CTN3_de_44() const { return ___CTN3_de_44; }
	inline Image_t2670269651 ** get_address_of_CTN3_de_44() { return &___CTN3_de_44; }
	inline void set_CTN3_de_44(Image_t2670269651 * value)
	{
		___CTN3_de_44 = value;
		Il2CppCodeGenWriteBarrier((&___CTN3_de_44), value);
	}

	inline static int32_t get_offset_of_CTN3_is_45() { return static_cast<int32_t>(offsetof(LangManager_t3681335332, ___CTN3_is_45)); }
	inline Image_t2670269651 * get_CTN3_is_45() const { return ___CTN3_is_45; }
	inline Image_t2670269651 ** get_address_of_CTN3_is_45() { return &___CTN3_is_45; }
	inline void set_CTN3_is_45(Image_t2670269651 * value)
	{
		___CTN3_is_45 = value;
		Il2CppCodeGenWriteBarrier((&___CTN3_is_45), value);
	}

	inline static int32_t get_offset_of_CTN3_ie_46() { return static_cast<int32_t>(offsetof(LangManager_t3681335332, ___CTN3_ie_46)); }
	inline Image_t2670269651 * get_CTN3_ie_46() const { return ___CTN3_ie_46; }
	inline Image_t2670269651 ** get_address_of_CTN3_ie_46() { return &___CTN3_ie_46; }
	inline void set_CTN3_ie_46(Image_t2670269651 * value)
	{
		___CTN3_ie_46 = value;
		Il2CppCodeGenWriteBarrier((&___CTN3_ie_46), value);
	}

	inline static int32_t get_offset_of_CTN3_gb_47() { return static_cast<int32_t>(offsetof(LangManager_t3681335332, ___CTN3_gb_47)); }
	inline Image_t2670269651 * get_CTN3_gb_47() const { return ___CTN3_gb_47; }
	inline Image_t2670269651 ** get_address_of_CTN3_gb_47() { return &___CTN3_gb_47; }
	inline void set_CTN3_gb_47(Image_t2670269651 * value)
	{
		___CTN3_gb_47 = value;
		Il2CppCodeGenWriteBarrier((&___CTN3_gb_47), value);
	}

	inline static int32_t get_offset_of_CTN3_es_48() { return static_cast<int32_t>(offsetof(LangManager_t3681335332, ___CTN3_es_48)); }
	inline Image_t2670269651 * get_CTN3_es_48() const { return ___CTN3_es_48; }
	inline Image_t2670269651 ** get_address_of_CTN3_es_48() { return &___CTN3_es_48; }
	inline void set_CTN3_es_48(Image_t2670269651 * value)
	{
		___CTN3_es_48 = value;
		Il2CppCodeGenWriteBarrier((&___CTN3_es_48), value);
	}

	inline static int32_t get_offset_of_CTN3_be_49() { return static_cast<int32_t>(offsetof(LangManager_t3681335332, ___CTN3_be_49)); }
	inline Image_t2670269651 * get_CTN3_be_49() const { return ___CTN3_be_49; }
	inline Image_t2670269651 ** get_address_of_CTN3_be_49() { return &___CTN3_be_49; }
	inline void set_CTN3_be_49(Image_t2670269651 * value)
	{
		___CTN3_be_49 = value;
		Il2CppCodeGenWriteBarrier((&___CTN3_be_49), value);
	}

	inline static int32_t get_offset_of_CTN3_nl_50() { return static_cast<int32_t>(offsetof(LangManager_t3681335332, ___CTN3_nl_50)); }
	inline Image_t2670269651 * get_CTN3_nl_50() const { return ___CTN3_nl_50; }
	inline Image_t2670269651 ** get_address_of_CTN3_nl_50() { return &___CTN3_nl_50; }
	inline void set_CTN3_nl_50(Image_t2670269651 * value)
	{
		___CTN3_nl_50 = value;
		Il2CppCodeGenWriteBarrier((&___CTN3_nl_50), value);
	}

	inline static int32_t get_offset_of_CTN4_cn_51() { return static_cast<int32_t>(offsetof(LangManager_t3681335332, ___CTN4_cn_51)); }
	inline Image_t2670269651 * get_CTN4_cn_51() const { return ___CTN4_cn_51; }
	inline Image_t2670269651 ** get_address_of_CTN4_cn_51() { return &___CTN4_cn_51; }
	inline void set_CTN4_cn_51(Image_t2670269651 * value)
	{
		___CTN4_cn_51 = value;
		Il2CppCodeGenWriteBarrier((&___CTN4_cn_51), value);
	}

	inline static int32_t get_offset_of_CTN4_in_52() { return static_cast<int32_t>(offsetof(LangManager_t3681335332, ___CTN4_in_52)); }
	inline Image_t2670269651 * get_CTN4_in_52() const { return ___CTN4_in_52; }
	inline Image_t2670269651 ** get_address_of_CTN4_in_52() { return &___CTN4_in_52; }
	inline void set_CTN4_in_52(Image_t2670269651 * value)
	{
		___CTN4_in_52 = value;
		Il2CppCodeGenWriteBarrier((&___CTN4_in_52), value);
	}

	inline static int32_t get_offset_of_CTN4_jp_53() { return static_cast<int32_t>(offsetof(LangManager_t3681335332, ___CTN4_jp_53)); }
	inline Image_t2670269651 * get_CTN4_jp_53() const { return ___CTN4_jp_53; }
	inline Image_t2670269651 ** get_address_of_CTN4_jp_53() { return &___CTN4_jp_53; }
	inline void set_CTN4_jp_53(Image_t2670269651 * value)
	{
		___CTN4_jp_53 = value;
		Il2CppCodeGenWriteBarrier((&___CTN4_jp_53), value);
	}

	inline static int32_t get_offset_of_CTN4_kr_54() { return static_cast<int32_t>(offsetof(LangManager_t3681335332, ___CTN4_kr_54)); }
	inline Image_t2670269651 * get_CTN4_kr_54() const { return ___CTN4_kr_54; }
	inline Image_t2670269651 ** get_address_of_CTN4_kr_54() { return &___CTN4_kr_54; }
	inline void set_CTN4_kr_54(Image_t2670269651 * value)
	{
		___CTN4_kr_54 = value;
		Il2CppCodeGenWriteBarrier((&___CTN4_kr_54), value);
	}

	inline static int32_t get_offset_of_CTN4_pk_55() { return static_cast<int32_t>(offsetof(LangManager_t3681335332, ___CTN4_pk_55)); }
	inline Image_t2670269651 * get_CTN4_pk_55() const { return ___CTN4_pk_55; }
	inline Image_t2670269651 ** get_address_of_CTN4_pk_55() { return &___CTN4_pk_55; }
	inline void set_CTN4_pk_55(Image_t2670269651 * value)
	{
		___CTN4_pk_55 = value;
		Il2CppCodeGenWriteBarrier((&___CTN4_pk_55), value);
	}

	inline static int32_t get_offset_of_CTN4_sg_56() { return static_cast<int32_t>(offsetof(LangManager_t3681335332, ___CTN4_sg_56)); }
	inline Image_t2670269651 * get_CTN4_sg_56() const { return ___CTN4_sg_56; }
	inline Image_t2670269651 ** get_address_of_CTN4_sg_56() { return &___CTN4_sg_56; }
	inline void set_CTN4_sg_56(Image_t2670269651 * value)
	{
		___CTN4_sg_56 = value;
		Il2CppCodeGenWriteBarrier((&___CTN4_sg_56), value);
	}

	inline static int32_t get_offset_of_CTN4_sr_57() { return static_cast<int32_t>(offsetof(LangManager_t3681335332, ___CTN4_sr_57)); }
	inline Image_t2670269651 * get_CTN4_sr_57() const { return ___CTN4_sr_57; }
	inline Image_t2670269651 ** get_address_of_CTN4_sr_57() { return &___CTN4_sr_57; }
	inline void set_CTN4_sr_57(Image_t2670269651 * value)
	{
		___CTN4_sr_57 = value;
		Il2CppCodeGenWriteBarrier((&___CTN4_sr_57), value);
	}

	inline static int32_t get_offset_of_CTN5_au_58() { return static_cast<int32_t>(offsetof(LangManager_t3681335332, ___CTN5_au_58)); }
	inline Image_t2670269651 * get_CTN5_au_58() const { return ___CTN5_au_58; }
	inline Image_t2670269651 ** get_address_of_CTN5_au_58() { return &___CTN5_au_58; }
	inline void set_CTN5_au_58(Image_t2670269651 * value)
	{
		___CTN5_au_58 = value;
		Il2CppCodeGenWriteBarrier((&___CTN5_au_58), value);
	}

	inline static int32_t get_offset_of_CTN5_nz_59() { return static_cast<int32_t>(offsetof(LangManager_t3681335332, ___CTN5_nz_59)); }
	inline Image_t2670269651 * get_CTN5_nz_59() const { return ___CTN5_nz_59; }
	inline Image_t2670269651 ** get_address_of_CTN5_nz_59() { return &___CTN5_nz_59; }
	inline void set_CTN5_nz_59(Image_t2670269651 * value)
	{
		___CTN5_nz_59 = value;
		Il2CppCodeGenWriteBarrier((&___CTN5_nz_59), value);
	}

	inline static int32_t get_offset_of_button1_60() { return static_cast<int32_t>(offsetof(LangManager_t3681335332, ___button1_60)); }
	inline GameObject_t1113636619 * get_button1_60() const { return ___button1_60; }
	inline GameObject_t1113636619 ** get_address_of_button1_60() { return &___button1_60; }
	inline void set_button1_60(GameObject_t1113636619 * value)
	{
		___button1_60 = value;
		Il2CppCodeGenWriteBarrier((&___button1_60), value);
	}

	inline static int32_t get_offset_of_button2_61() { return static_cast<int32_t>(offsetof(LangManager_t3681335332, ___button2_61)); }
	inline GameObject_t1113636619 * get_button2_61() const { return ___button2_61; }
	inline GameObject_t1113636619 ** get_address_of_button2_61() { return &___button2_61; }
	inline void set_button2_61(GameObject_t1113636619 * value)
	{
		___button2_61 = value;
		Il2CppCodeGenWriteBarrier((&___button2_61), value);
	}

	inline static int32_t get_offset_of_button3_62() { return static_cast<int32_t>(offsetof(LangManager_t3681335332, ___button3_62)); }
	inline GameObject_t1113636619 * get_button3_62() const { return ___button3_62; }
	inline GameObject_t1113636619 ** get_address_of_button3_62() { return &___button3_62; }
	inline void set_button3_62(GameObject_t1113636619 * value)
	{
		___button3_62 = value;
		Il2CppCodeGenWriteBarrier((&___button3_62), value);
	}

	inline static int32_t get_offset_of_panelConfirmation_63() { return static_cast<int32_t>(offsetof(LangManager_t3681335332, ___panelConfirmation_63)); }
	inline GameObject_t1113636619 * get_panelConfirmation_63() const { return ___panelConfirmation_63; }
	inline GameObject_t1113636619 ** get_address_of_panelConfirmation_63() { return &___panelConfirmation_63; }
	inline void set_panelConfirmation_63(GameObject_t1113636619 * value)
	{
		___panelConfirmation_63 = value;
		Il2CppCodeGenWriteBarrier((&___panelConfirmation_63), value);
	}

	inline static int32_t get_offset_of_confirmationText_64() { return static_cast<int32_t>(offsetof(LangManager_t3681335332, ___confirmationText_64)); }
	inline Text_t1901882714 * get_confirmationText_64() const { return ___confirmationText_64; }
	inline Text_t1901882714 ** get_address_of_confirmationText_64() { return &___confirmationText_64; }
	inline void set_confirmationText_64(Text_t1901882714 * value)
	{
		___confirmationText_64 = value;
		Il2CppCodeGenWriteBarrier((&___confirmationText_64), value);
	}

	inline static int32_t get_offset_of_confirmationFlag_65() { return static_cast<int32_t>(offsetof(LangManager_t3681335332, ___confirmationFlag_65)); }
	inline Image_t2670269651 * get_confirmationFlag_65() const { return ___confirmationFlag_65; }
	inline Image_t2670269651 ** get_address_of_confirmationFlag_65() { return &___confirmationFlag_65; }
	inline void set_confirmationFlag_65(Image_t2670269651 * value)
	{
		___confirmationFlag_65 = value;
		Il2CppCodeGenWriteBarrier((&___confirmationFlag_65), value);
	}

	inline static int32_t get_offset_of_buttonClose_66() { return static_cast<int32_t>(offsetof(LangManager_t3681335332, ___buttonClose_66)); }
	inline GameObject_t1113636619 * get_buttonClose_66() const { return ___buttonClose_66; }
	inline GameObject_t1113636619 ** get_address_of_buttonClose_66() { return &___buttonClose_66; }
	inline void set_buttonClose_66(GameObject_t1113636619 * value)
	{
		___buttonClose_66 = value;
		Il2CppCodeGenWriteBarrier((&___buttonClose_66), value);
	}

	inline static int32_t get_offset_of_selectedLanguageID_67() { return static_cast<int32_t>(offsetof(LangManager_t3681335332, ___selectedLanguageID_67)); }
	inline int32_t get_selectedLanguageID_67() const { return ___selectedLanguageID_67; }
	inline int32_t* get_address_of_selectedLanguageID_67() { return &___selectedLanguageID_67; }
	inline void set_selectedLanguageID_67(int32_t value)
	{
		___selectedLanguageID_67 = value;
	}

	inline static int32_t get_offset_of_allCTN1_flags_68() { return static_cast<int32_t>(offsetof(LangManager_t3681335332, ___allCTN1_flags_68)); }
	inline ImageU5BU5D_t2439009922* get_allCTN1_flags_68() const { return ___allCTN1_flags_68; }
	inline ImageU5BU5D_t2439009922** get_address_of_allCTN1_flags_68() { return &___allCTN1_flags_68; }
	inline void set_allCTN1_flags_68(ImageU5BU5D_t2439009922* value)
	{
		___allCTN1_flags_68 = value;
		Il2CppCodeGenWriteBarrier((&___allCTN1_flags_68), value);
	}

	inline static int32_t get_offset_of_allCTN2_flags_69() { return static_cast<int32_t>(offsetof(LangManager_t3681335332, ___allCTN2_flags_69)); }
	inline ImageU5BU5D_t2439009922* get_allCTN2_flags_69() const { return ___allCTN2_flags_69; }
	inline ImageU5BU5D_t2439009922** get_address_of_allCTN2_flags_69() { return &___allCTN2_flags_69; }
	inline void set_allCTN2_flags_69(ImageU5BU5D_t2439009922* value)
	{
		___allCTN2_flags_69 = value;
		Il2CppCodeGenWriteBarrier((&___allCTN2_flags_69), value);
	}

	inline static int32_t get_offset_of_allCTN3_flags_70() { return static_cast<int32_t>(offsetof(LangManager_t3681335332, ___allCTN3_flags_70)); }
	inline ImageU5BU5D_t2439009922* get_allCTN3_flags_70() const { return ___allCTN3_flags_70; }
	inline ImageU5BU5D_t2439009922** get_address_of_allCTN3_flags_70() { return &___allCTN3_flags_70; }
	inline void set_allCTN3_flags_70(ImageU5BU5D_t2439009922* value)
	{
		___allCTN3_flags_70 = value;
		Il2CppCodeGenWriteBarrier((&___allCTN3_flags_70), value);
	}

	inline static int32_t get_offset_of_allCTN4_flags_71() { return static_cast<int32_t>(offsetof(LangManager_t3681335332, ___allCTN4_flags_71)); }
	inline ImageU5BU5D_t2439009922* get_allCTN4_flags_71() const { return ___allCTN4_flags_71; }
	inline ImageU5BU5D_t2439009922** get_address_of_allCTN4_flags_71() { return &___allCTN4_flags_71; }
	inline void set_allCTN4_flags_71(ImageU5BU5D_t2439009922* value)
	{
		___allCTN4_flags_71 = value;
		Il2CppCodeGenWriteBarrier((&___allCTN4_flags_71), value);
	}

	inline static int32_t get_offset_of_allCTN5_flags_72() { return static_cast<int32_t>(offsetof(LangManager_t3681335332, ___allCTN5_flags_72)); }
	inline ImageU5BU5D_t2439009922* get_allCTN5_flags_72() const { return ___allCTN5_flags_72; }
	inline ImageU5BU5D_t2439009922** get_address_of_allCTN5_flags_72() { return &___allCTN5_flags_72; }
	inline void set_allCTN5_flags_72(ImageU5BU5D_t2439009922* value)
	{
		___allCTN5_flags_72 = value;
		Il2CppCodeGenWriteBarrier((&___allCTN5_flags_72), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LANGMANAGER_T3681335332_H
#ifndef LANGUAGEIDHOLDER_T3554124395_H
#define LANGUAGEIDHOLDER_T3554124395_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LanguageIDHolder
struct  LanguageIDHolder_t3554124395  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 LanguageIDHolder::codeLang
	int32_t ___codeLang_4;
	// System.String LanguageIDHolder::language
	String_t* ___language_5;

public:
	inline static int32_t get_offset_of_codeLang_4() { return static_cast<int32_t>(offsetof(LanguageIDHolder_t3554124395, ___codeLang_4)); }
	inline int32_t get_codeLang_4() const { return ___codeLang_4; }
	inline int32_t* get_address_of_codeLang_4() { return &___codeLang_4; }
	inline void set_codeLang_4(int32_t value)
	{
		___codeLang_4 = value;
	}

	inline static int32_t get_offset_of_language_5() { return static_cast<int32_t>(offsetof(LanguageIDHolder_t3554124395, ___language_5)); }
	inline String_t* get_language_5() const { return ___language_5; }
	inline String_t** get_address_of_language_5() { return &___language_5; }
	inline void set_language_5(String_t* value)
	{
		___language_5 = value;
		Il2CppCodeGenWriteBarrier((&___language_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LANGUAGEIDHOLDER_T3554124395_H
#ifndef MAINMENUMANAGER_T3185368703_H
#define MAINMENUMANAGER_T3185368703_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MainMenuManager
struct  MainMenuManager_t3185368703  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAINMENUMANAGER_T3185368703_H
#ifndef MARKERCONTENT_T762212139_H
#define MARKERCONTENT_T762212139_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MarkerContent
struct  MarkerContent_t762212139  : public MonoBehaviour_t3962482529
{
public:
	// System.String MarkerContent::urlImage
	String_t* ___urlImage_4;
	// UnityEngine.GameObject MarkerContent::plane
	GameObject_t1113636619 * ___plane_5;
	// UnityEngine.GameObject MarkerContent::afficheMarker
	GameObject_t1113636619 * ___afficheMarker_6;
	// UnityEngine.GameObject MarkerContent::backButtonBackMarker
	GameObject_t1113636619 * ___backButtonBackMarker_7;
	// UnityEngine.GameObject MarkerContent::backMarkerON
	GameObject_t1113636619 * ___backMarkerON_8;
	// UnityEngine.GameObject MarkerContent::backMarkerOFF
	GameObject_t1113636619 * ___backMarkerOFF_9;

public:
	inline static int32_t get_offset_of_urlImage_4() { return static_cast<int32_t>(offsetof(MarkerContent_t762212139, ___urlImage_4)); }
	inline String_t* get_urlImage_4() const { return ___urlImage_4; }
	inline String_t** get_address_of_urlImage_4() { return &___urlImage_4; }
	inline void set_urlImage_4(String_t* value)
	{
		___urlImage_4 = value;
		Il2CppCodeGenWriteBarrier((&___urlImage_4), value);
	}

	inline static int32_t get_offset_of_plane_5() { return static_cast<int32_t>(offsetof(MarkerContent_t762212139, ___plane_5)); }
	inline GameObject_t1113636619 * get_plane_5() const { return ___plane_5; }
	inline GameObject_t1113636619 ** get_address_of_plane_5() { return &___plane_5; }
	inline void set_plane_5(GameObject_t1113636619 * value)
	{
		___plane_5 = value;
		Il2CppCodeGenWriteBarrier((&___plane_5), value);
	}

	inline static int32_t get_offset_of_afficheMarker_6() { return static_cast<int32_t>(offsetof(MarkerContent_t762212139, ___afficheMarker_6)); }
	inline GameObject_t1113636619 * get_afficheMarker_6() const { return ___afficheMarker_6; }
	inline GameObject_t1113636619 ** get_address_of_afficheMarker_6() { return &___afficheMarker_6; }
	inline void set_afficheMarker_6(GameObject_t1113636619 * value)
	{
		___afficheMarker_6 = value;
		Il2CppCodeGenWriteBarrier((&___afficheMarker_6), value);
	}

	inline static int32_t get_offset_of_backButtonBackMarker_7() { return static_cast<int32_t>(offsetof(MarkerContent_t762212139, ___backButtonBackMarker_7)); }
	inline GameObject_t1113636619 * get_backButtonBackMarker_7() const { return ___backButtonBackMarker_7; }
	inline GameObject_t1113636619 ** get_address_of_backButtonBackMarker_7() { return &___backButtonBackMarker_7; }
	inline void set_backButtonBackMarker_7(GameObject_t1113636619 * value)
	{
		___backButtonBackMarker_7 = value;
		Il2CppCodeGenWriteBarrier((&___backButtonBackMarker_7), value);
	}

	inline static int32_t get_offset_of_backMarkerON_8() { return static_cast<int32_t>(offsetof(MarkerContent_t762212139, ___backMarkerON_8)); }
	inline GameObject_t1113636619 * get_backMarkerON_8() const { return ___backMarkerON_8; }
	inline GameObject_t1113636619 ** get_address_of_backMarkerON_8() { return &___backMarkerON_8; }
	inline void set_backMarkerON_8(GameObject_t1113636619 * value)
	{
		___backMarkerON_8 = value;
		Il2CppCodeGenWriteBarrier((&___backMarkerON_8), value);
	}

	inline static int32_t get_offset_of_backMarkerOFF_9() { return static_cast<int32_t>(offsetof(MarkerContent_t762212139, ___backMarkerOFF_9)); }
	inline GameObject_t1113636619 * get_backMarkerOFF_9() const { return ___backMarkerOFF_9; }
	inline GameObject_t1113636619 ** get_address_of_backMarkerOFF_9() { return &___backMarkerOFF_9; }
	inline void set_backMarkerOFF_9(GameObject_t1113636619 * value)
	{
		___backMarkerOFF_9 = value;
		Il2CppCodeGenWriteBarrier((&___backMarkerOFF_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MARKERCONTENT_T762212139_H
#ifndef MENUBURGER_T1089396821_H
#define MENUBURGER_T1089396821_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MenuBurger
struct  MenuBurger_t1089396821  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean MenuBurger::isOpenAtStart
	bool ___isOpenAtStart_4;
	// MenuBurger/MenuBurgerStatus MenuBurger::status
	int32_t ___status_5;
	// UnityEngine.Video.VideoPlayer MenuBurger::videoTransition
	VideoPlayer_t1683042537 * ___videoTransition_6;
	// UnityEngine.Video.VideoClip MenuBurger::videoOpenTransition
	VideoClip_t1281919028 * ___videoOpenTransition_7;
	// UnityEngine.Video.VideoClip MenuBurger::videoCloseTransition
	VideoClip_t1281919028 * ___videoCloseTransition_8;
	// System.Single MenuBurger::menuSpeed
	float ___menuSpeed_9;
	// UnityEngine.RectTransform MenuBurger::anchorOpen
	RectTransform_t3704657025 * ___anchorOpen_10;
	// UnityEngine.RectTransform MenuBurger::anchorClose
	RectTransform_t3704657025 * ___anchorClose_11;
	// UnityEngine.RectTransform MenuBurger::menuBody
	RectTransform_t3704657025 * ___menuBody_12;
	// UnityEngine.RectTransform MenuBurger::backgoundBody
	RectTransform_t3704657025 * ___backgoundBody_13;
	// UnityEngine.UI.Button MenuBurger::toggleMenuButtonStyle
	Button_t4055032469 * ___toggleMenuButtonStyle_14;
	// UnityEngine.Animator MenuBurger::iconMenu
	Animator_t434523843 * ___iconMenu_15;
	// UnityEngine.Vector3 MenuBurger::closeMvt
	Vector3_t3722313464  ___closeMvt_16;
	// UnityEngine.Vector3 MenuBurger::openMvt
	Vector3_t3722313464  ___openMvt_17;

public:
	inline static int32_t get_offset_of_isOpenAtStart_4() { return static_cast<int32_t>(offsetof(MenuBurger_t1089396821, ___isOpenAtStart_4)); }
	inline bool get_isOpenAtStart_4() const { return ___isOpenAtStart_4; }
	inline bool* get_address_of_isOpenAtStart_4() { return &___isOpenAtStart_4; }
	inline void set_isOpenAtStart_4(bool value)
	{
		___isOpenAtStart_4 = value;
	}

	inline static int32_t get_offset_of_status_5() { return static_cast<int32_t>(offsetof(MenuBurger_t1089396821, ___status_5)); }
	inline int32_t get_status_5() const { return ___status_5; }
	inline int32_t* get_address_of_status_5() { return &___status_5; }
	inline void set_status_5(int32_t value)
	{
		___status_5 = value;
	}

	inline static int32_t get_offset_of_videoTransition_6() { return static_cast<int32_t>(offsetof(MenuBurger_t1089396821, ___videoTransition_6)); }
	inline VideoPlayer_t1683042537 * get_videoTransition_6() const { return ___videoTransition_6; }
	inline VideoPlayer_t1683042537 ** get_address_of_videoTransition_6() { return &___videoTransition_6; }
	inline void set_videoTransition_6(VideoPlayer_t1683042537 * value)
	{
		___videoTransition_6 = value;
		Il2CppCodeGenWriteBarrier((&___videoTransition_6), value);
	}

	inline static int32_t get_offset_of_videoOpenTransition_7() { return static_cast<int32_t>(offsetof(MenuBurger_t1089396821, ___videoOpenTransition_7)); }
	inline VideoClip_t1281919028 * get_videoOpenTransition_7() const { return ___videoOpenTransition_7; }
	inline VideoClip_t1281919028 ** get_address_of_videoOpenTransition_7() { return &___videoOpenTransition_7; }
	inline void set_videoOpenTransition_7(VideoClip_t1281919028 * value)
	{
		___videoOpenTransition_7 = value;
		Il2CppCodeGenWriteBarrier((&___videoOpenTransition_7), value);
	}

	inline static int32_t get_offset_of_videoCloseTransition_8() { return static_cast<int32_t>(offsetof(MenuBurger_t1089396821, ___videoCloseTransition_8)); }
	inline VideoClip_t1281919028 * get_videoCloseTransition_8() const { return ___videoCloseTransition_8; }
	inline VideoClip_t1281919028 ** get_address_of_videoCloseTransition_8() { return &___videoCloseTransition_8; }
	inline void set_videoCloseTransition_8(VideoClip_t1281919028 * value)
	{
		___videoCloseTransition_8 = value;
		Il2CppCodeGenWriteBarrier((&___videoCloseTransition_8), value);
	}

	inline static int32_t get_offset_of_menuSpeed_9() { return static_cast<int32_t>(offsetof(MenuBurger_t1089396821, ___menuSpeed_9)); }
	inline float get_menuSpeed_9() const { return ___menuSpeed_9; }
	inline float* get_address_of_menuSpeed_9() { return &___menuSpeed_9; }
	inline void set_menuSpeed_9(float value)
	{
		___menuSpeed_9 = value;
	}

	inline static int32_t get_offset_of_anchorOpen_10() { return static_cast<int32_t>(offsetof(MenuBurger_t1089396821, ___anchorOpen_10)); }
	inline RectTransform_t3704657025 * get_anchorOpen_10() const { return ___anchorOpen_10; }
	inline RectTransform_t3704657025 ** get_address_of_anchorOpen_10() { return &___anchorOpen_10; }
	inline void set_anchorOpen_10(RectTransform_t3704657025 * value)
	{
		___anchorOpen_10 = value;
		Il2CppCodeGenWriteBarrier((&___anchorOpen_10), value);
	}

	inline static int32_t get_offset_of_anchorClose_11() { return static_cast<int32_t>(offsetof(MenuBurger_t1089396821, ___anchorClose_11)); }
	inline RectTransform_t3704657025 * get_anchorClose_11() const { return ___anchorClose_11; }
	inline RectTransform_t3704657025 ** get_address_of_anchorClose_11() { return &___anchorClose_11; }
	inline void set_anchorClose_11(RectTransform_t3704657025 * value)
	{
		___anchorClose_11 = value;
		Il2CppCodeGenWriteBarrier((&___anchorClose_11), value);
	}

	inline static int32_t get_offset_of_menuBody_12() { return static_cast<int32_t>(offsetof(MenuBurger_t1089396821, ___menuBody_12)); }
	inline RectTransform_t3704657025 * get_menuBody_12() const { return ___menuBody_12; }
	inline RectTransform_t3704657025 ** get_address_of_menuBody_12() { return &___menuBody_12; }
	inline void set_menuBody_12(RectTransform_t3704657025 * value)
	{
		___menuBody_12 = value;
		Il2CppCodeGenWriteBarrier((&___menuBody_12), value);
	}

	inline static int32_t get_offset_of_backgoundBody_13() { return static_cast<int32_t>(offsetof(MenuBurger_t1089396821, ___backgoundBody_13)); }
	inline RectTransform_t3704657025 * get_backgoundBody_13() const { return ___backgoundBody_13; }
	inline RectTransform_t3704657025 ** get_address_of_backgoundBody_13() { return &___backgoundBody_13; }
	inline void set_backgoundBody_13(RectTransform_t3704657025 * value)
	{
		___backgoundBody_13 = value;
		Il2CppCodeGenWriteBarrier((&___backgoundBody_13), value);
	}

	inline static int32_t get_offset_of_toggleMenuButtonStyle_14() { return static_cast<int32_t>(offsetof(MenuBurger_t1089396821, ___toggleMenuButtonStyle_14)); }
	inline Button_t4055032469 * get_toggleMenuButtonStyle_14() const { return ___toggleMenuButtonStyle_14; }
	inline Button_t4055032469 ** get_address_of_toggleMenuButtonStyle_14() { return &___toggleMenuButtonStyle_14; }
	inline void set_toggleMenuButtonStyle_14(Button_t4055032469 * value)
	{
		___toggleMenuButtonStyle_14 = value;
		Il2CppCodeGenWriteBarrier((&___toggleMenuButtonStyle_14), value);
	}

	inline static int32_t get_offset_of_iconMenu_15() { return static_cast<int32_t>(offsetof(MenuBurger_t1089396821, ___iconMenu_15)); }
	inline Animator_t434523843 * get_iconMenu_15() const { return ___iconMenu_15; }
	inline Animator_t434523843 ** get_address_of_iconMenu_15() { return &___iconMenu_15; }
	inline void set_iconMenu_15(Animator_t434523843 * value)
	{
		___iconMenu_15 = value;
		Il2CppCodeGenWriteBarrier((&___iconMenu_15), value);
	}

	inline static int32_t get_offset_of_closeMvt_16() { return static_cast<int32_t>(offsetof(MenuBurger_t1089396821, ___closeMvt_16)); }
	inline Vector3_t3722313464  get_closeMvt_16() const { return ___closeMvt_16; }
	inline Vector3_t3722313464 * get_address_of_closeMvt_16() { return &___closeMvt_16; }
	inline void set_closeMvt_16(Vector3_t3722313464  value)
	{
		___closeMvt_16 = value;
	}

	inline static int32_t get_offset_of_openMvt_17() { return static_cast<int32_t>(offsetof(MenuBurger_t1089396821, ___openMvt_17)); }
	inline Vector3_t3722313464  get_openMvt_17() const { return ___openMvt_17; }
	inline Vector3_t3722313464 * get_address_of_openMvt_17() { return &___openMvt_17; }
	inline void set_openMvt_17(Vector3_t3722313464  value)
	{
		___openMvt_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MENUBURGER_T1089396821_H
#ifndef OBJLOADERMANAGER_T3565543175_H
#define OBJLOADERMANAGER_T3565543175_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OBJLoaderManager
struct  OBJLoaderManager_t3565543175  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJLOADERMANAGER_T3565543175_H
#ifndef OPENURL_T3441221081_H
#define OPENURL_T3441221081_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OpenURL
struct  OpenURL_t3441221081  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Button OpenURL::openUrlButton
	Button_t4055032469 * ___openUrlButton_4;
	// System.String OpenURL::url
	String_t* ___url_5;

public:
	inline static int32_t get_offset_of_openUrlButton_4() { return static_cast<int32_t>(offsetof(OpenURL_t3441221081, ___openUrlButton_4)); }
	inline Button_t4055032469 * get_openUrlButton_4() const { return ___openUrlButton_4; }
	inline Button_t4055032469 ** get_address_of_openUrlButton_4() { return &___openUrlButton_4; }
	inline void set_openUrlButton_4(Button_t4055032469 * value)
	{
		___openUrlButton_4 = value;
		Il2CppCodeGenWriteBarrier((&___openUrlButton_4), value);
	}

	inline static int32_t get_offset_of_url_5() { return static_cast<int32_t>(offsetof(OpenURL_t3441221081, ___url_5)); }
	inline String_t* get_url_5() const { return ___url_5; }
	inline String_t** get_address_of_url_5() { return &___url_5; }
	inline void set_url_5(String_t* value)
	{
		___url_5 = value;
		Il2CppCodeGenWriteBarrier((&___url_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OPENURL_T3441221081_H
#ifndef PAGETRANSITION_T3158442731_H
#define PAGETRANSITION_T3158442731_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PageTransition
struct  PageTransition_t3158442731  : public MonoBehaviour_t3962482529
{
public:
	// Page[] PageTransition::pages
	PageU5BU5D_t2014447836* ___pages_4;
	// UnityEngine.RectTransform PageTransition::anchorTop
	RectTransform_t3704657025 * ___anchorTop_5;
	// UnityEngine.RectTransform PageTransition::anchorBottom
	RectTransform_t3704657025 * ___anchorBottom_6;
	// UnityEngine.RectTransform PageTransition::anchorLeft
	RectTransform_t3704657025 * ___anchorLeft_7;
	// UnityEngine.RectTransform PageTransition::anchorRight
	RectTransform_t3704657025 * ___anchorRight_8;
	// Page PageTransition::CurrentPage
	Page_t2586579457 * ___CurrentPage_9;
	// System.Boolean PageTransition::isTransiting
	bool ___isTransiting_10;

public:
	inline static int32_t get_offset_of_pages_4() { return static_cast<int32_t>(offsetof(PageTransition_t3158442731, ___pages_4)); }
	inline PageU5BU5D_t2014447836* get_pages_4() const { return ___pages_4; }
	inline PageU5BU5D_t2014447836** get_address_of_pages_4() { return &___pages_4; }
	inline void set_pages_4(PageU5BU5D_t2014447836* value)
	{
		___pages_4 = value;
		Il2CppCodeGenWriteBarrier((&___pages_4), value);
	}

	inline static int32_t get_offset_of_anchorTop_5() { return static_cast<int32_t>(offsetof(PageTransition_t3158442731, ___anchorTop_5)); }
	inline RectTransform_t3704657025 * get_anchorTop_5() const { return ___anchorTop_5; }
	inline RectTransform_t3704657025 ** get_address_of_anchorTop_5() { return &___anchorTop_5; }
	inline void set_anchorTop_5(RectTransform_t3704657025 * value)
	{
		___anchorTop_5 = value;
		Il2CppCodeGenWriteBarrier((&___anchorTop_5), value);
	}

	inline static int32_t get_offset_of_anchorBottom_6() { return static_cast<int32_t>(offsetof(PageTransition_t3158442731, ___anchorBottom_6)); }
	inline RectTransform_t3704657025 * get_anchorBottom_6() const { return ___anchorBottom_6; }
	inline RectTransform_t3704657025 ** get_address_of_anchorBottom_6() { return &___anchorBottom_6; }
	inline void set_anchorBottom_6(RectTransform_t3704657025 * value)
	{
		___anchorBottom_6 = value;
		Il2CppCodeGenWriteBarrier((&___anchorBottom_6), value);
	}

	inline static int32_t get_offset_of_anchorLeft_7() { return static_cast<int32_t>(offsetof(PageTransition_t3158442731, ___anchorLeft_7)); }
	inline RectTransform_t3704657025 * get_anchorLeft_7() const { return ___anchorLeft_7; }
	inline RectTransform_t3704657025 ** get_address_of_anchorLeft_7() { return &___anchorLeft_7; }
	inline void set_anchorLeft_7(RectTransform_t3704657025 * value)
	{
		___anchorLeft_7 = value;
		Il2CppCodeGenWriteBarrier((&___anchorLeft_7), value);
	}

	inline static int32_t get_offset_of_anchorRight_8() { return static_cast<int32_t>(offsetof(PageTransition_t3158442731, ___anchorRight_8)); }
	inline RectTransform_t3704657025 * get_anchorRight_8() const { return ___anchorRight_8; }
	inline RectTransform_t3704657025 ** get_address_of_anchorRight_8() { return &___anchorRight_8; }
	inline void set_anchorRight_8(RectTransform_t3704657025 * value)
	{
		___anchorRight_8 = value;
		Il2CppCodeGenWriteBarrier((&___anchorRight_8), value);
	}

	inline static int32_t get_offset_of_CurrentPage_9() { return static_cast<int32_t>(offsetof(PageTransition_t3158442731, ___CurrentPage_9)); }
	inline Page_t2586579457 * get_CurrentPage_9() const { return ___CurrentPage_9; }
	inline Page_t2586579457 ** get_address_of_CurrentPage_9() { return &___CurrentPage_9; }
	inline void set_CurrentPage_9(Page_t2586579457 * value)
	{
		___CurrentPage_9 = value;
		Il2CppCodeGenWriteBarrier((&___CurrentPage_9), value);
	}

	inline static int32_t get_offset_of_isTransiting_10() { return static_cast<int32_t>(offsetof(PageTransition_t3158442731, ___isTransiting_10)); }
	inline bool get_isTransiting_10() const { return ___isTransiting_10; }
	inline bool* get_address_of_isTransiting_10() { return &___isTransiting_10; }
	inline void set_isTransiting_10(bool value)
	{
		___isTransiting_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PAGETRANSITION_T3158442731_H
#ifndef PANOERROR_T3840804466_H
#define PANOERROR_T3840804466_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PanoError
struct  PanoError_t3840804466  : public MonoBehaviour_t3962482529
{
public:
	// System.String PanoError::errrorMsg
	String_t* ___errrorMsg_4;
	// TMPro.TextMeshProUGUI PanoError::ui_errorMsg
	TextMeshProUGUI_t529313277 * ___ui_errorMsg_5;

public:
	inline static int32_t get_offset_of_errrorMsg_4() { return static_cast<int32_t>(offsetof(PanoError_t3840804466, ___errrorMsg_4)); }
	inline String_t* get_errrorMsg_4() const { return ___errrorMsg_4; }
	inline String_t** get_address_of_errrorMsg_4() { return &___errrorMsg_4; }
	inline void set_errrorMsg_4(String_t* value)
	{
		___errrorMsg_4 = value;
		Il2CppCodeGenWriteBarrier((&___errrorMsg_4), value);
	}

	inline static int32_t get_offset_of_ui_errorMsg_5() { return static_cast<int32_t>(offsetof(PanoError_t3840804466, ___ui_errorMsg_5)); }
	inline TextMeshProUGUI_t529313277 * get_ui_errorMsg_5() const { return ___ui_errorMsg_5; }
	inline TextMeshProUGUI_t529313277 ** get_address_of_ui_errorMsg_5() { return &___ui_errorMsg_5; }
	inline void set_ui_errorMsg_5(TextMeshProUGUI_t529313277 * value)
	{
		___ui_errorMsg_5 = value;
		Il2CppCodeGenWriteBarrier((&___ui_errorMsg_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PANOERROR_T3840804466_H
#ifndef PANOFORM_T499938582_H
#define PANOFORM_T499938582_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PanoForm
struct  PanoForm_t499938582  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean PanoForm::isGagne
	bool ___isGagne_4;
	// System.Boolean PanoForm::display_email
	bool ___display_email_5;
	// System.Boolean PanoForm::display_nom
	bool ___display_nom_6;
	// System.Boolean PanoForm::display_prenom
	bool ___display_prenom_7;
	// System.Boolean PanoForm::display_societe
	bool ___display_societe_8;
	// System.Boolean PanoForm::display_telephone
	bool ___display_telephone_9;
	// System.Boolean PanoForm::display_datenaissance
	bool ___display_datenaissance_10;
	// System.Boolean PanoForm::display_extra
	bool ___display_extra_11;
	// System.Boolean PanoForm::display_email_oblig
	bool ___display_email_oblig_12;
	// System.Boolean PanoForm::display_nom_oblig
	bool ___display_nom_oblig_13;
	// System.Boolean PanoForm::display_prenom_oblig
	bool ___display_prenom_oblig_14;
	// System.Boolean PanoForm::display_societe_oblig
	bool ___display_societe_oblig_15;
	// System.Boolean PanoForm::display_telephone_oblig
	bool ___display_telephone_oblig_16;
	// System.Boolean PanoForm::display_datenaissance_oblig
	bool ___display_datenaissance_oblig_17;
	// System.Boolean PanoForm::display_extra_oblig
	bool ___display_extra_oblig_18;
	// UnityEngine.GameObject PanoForm::go_email
	GameObject_t1113636619 * ___go_email_19;
	// UnityEngine.GameObject PanoForm::go_nom
	GameObject_t1113636619 * ___go_nom_20;
	// UnityEngine.GameObject PanoForm::go_prenom
	GameObject_t1113636619 * ___go_prenom_21;
	// UnityEngine.GameObject PanoForm::go_societe
	GameObject_t1113636619 * ___go_societe_22;
	// UnityEngine.GameObject PanoForm::go_telephone_datenaissance
	GameObject_t1113636619 * ___go_telephone_datenaissance_23;
	// UnityEngine.GameObject PanoForm::go_telephone
	GameObject_t1113636619 * ___go_telephone_24;
	// UnityEngine.GameObject PanoForm::go_datenaissance
	GameObject_t1113636619 * ___go_datenaissance_25;
	// UnityEngine.GameObject PanoForm::go_extra
	GameObject_t1113636619 * ___go_extra_26;
	// TMPro.TMP_InputField PanoForm::if_email
	TMP_InputField_t1099764886 * ___if_email_27;
	// TMPro.TMP_InputField PanoForm::if_nom
	TMP_InputField_t1099764886 * ___if_nom_28;
	// TMPro.TMP_InputField PanoForm::if_prenom
	TMP_InputField_t1099764886 * ___if_prenom_29;
	// TMPro.TMP_InputField PanoForm::if_societe
	TMP_InputField_t1099764886 * ___if_societe_30;
	// TMPro.TMP_InputField PanoForm::if_telephone
	TMP_InputField_t1099764886 * ___if_telephone_31;
	// TMPro.TMP_InputField PanoForm::if_JJ
	TMP_InputField_t1099764886 * ___if_JJ_32;
	// TMPro.TMP_InputField PanoForm::if_MM
	TMP_InputField_t1099764886 * ___if_MM_33;
	// TMPro.TMP_InputField PanoForm::if_AAAA
	TMP_InputField_t1099764886 * ___if_AAAA_34;
	// TMPro.TMP_InputField PanoForm::if_extra
	TMP_InputField_t1099764886 * ___if_extra_35;
	// TMPro.TextMeshProUGUI PanoForm::if_email_oblig
	TextMeshProUGUI_t529313277 * ___if_email_oblig_36;
	// TMPro.TextMeshProUGUI PanoForm::if_nom_oblig
	TextMeshProUGUI_t529313277 * ___if_nom_oblig_37;
	// TMPro.TextMeshProUGUI PanoForm::if_prenom_oblig
	TextMeshProUGUI_t529313277 * ___if_prenom_oblig_38;
	// TMPro.TextMeshProUGUI PanoForm::if_societe_oblig
	TextMeshProUGUI_t529313277 * ___if_societe_oblig_39;
	// TMPro.TextMeshProUGUI PanoForm::if_telephone_oblig
	TextMeshProUGUI_t529313277 * ___if_telephone_oblig_40;
	// TMPro.TextMeshProUGUI PanoForm::if_dateNaissance_oblig
	TextMeshProUGUI_t529313277 * ___if_dateNaissance_oblig_41;
	// TMPro.TextMeshProUGUI PanoForm::extraLabel
	TextMeshProUGUI_t529313277 * ___extraLabel_42;
	// System.String PanoForm::UserGUID
	String_t* ___UserGUID_43;
	// System.String PanoForm::markerUID
	String_t* ___markerUID_44;
	// System.String PanoForm::appName
	String_t* ___appName_45;
	// System.String PanoForm::token
	String_t* ___token_46;
	// System.String PanoForm::link_of_reward
	String_t* ___link_of_reward_47;
	// System.String PanoForm::extraStr
	String_t* ___extraStr_48;
	// UnityEngine.GameObject PanoForm::panoError
	GameObject_t1113636619 * ___panoError_49;
	// UnityEngine.GameObject PanoForm::panoGagne
	GameObject_t1113636619 * ___panoGagne_50;

public:
	inline static int32_t get_offset_of_isGagne_4() { return static_cast<int32_t>(offsetof(PanoForm_t499938582, ___isGagne_4)); }
	inline bool get_isGagne_4() const { return ___isGagne_4; }
	inline bool* get_address_of_isGagne_4() { return &___isGagne_4; }
	inline void set_isGagne_4(bool value)
	{
		___isGagne_4 = value;
	}

	inline static int32_t get_offset_of_display_email_5() { return static_cast<int32_t>(offsetof(PanoForm_t499938582, ___display_email_5)); }
	inline bool get_display_email_5() const { return ___display_email_5; }
	inline bool* get_address_of_display_email_5() { return &___display_email_5; }
	inline void set_display_email_5(bool value)
	{
		___display_email_5 = value;
	}

	inline static int32_t get_offset_of_display_nom_6() { return static_cast<int32_t>(offsetof(PanoForm_t499938582, ___display_nom_6)); }
	inline bool get_display_nom_6() const { return ___display_nom_6; }
	inline bool* get_address_of_display_nom_6() { return &___display_nom_6; }
	inline void set_display_nom_6(bool value)
	{
		___display_nom_6 = value;
	}

	inline static int32_t get_offset_of_display_prenom_7() { return static_cast<int32_t>(offsetof(PanoForm_t499938582, ___display_prenom_7)); }
	inline bool get_display_prenom_7() const { return ___display_prenom_7; }
	inline bool* get_address_of_display_prenom_7() { return &___display_prenom_7; }
	inline void set_display_prenom_7(bool value)
	{
		___display_prenom_7 = value;
	}

	inline static int32_t get_offset_of_display_societe_8() { return static_cast<int32_t>(offsetof(PanoForm_t499938582, ___display_societe_8)); }
	inline bool get_display_societe_8() const { return ___display_societe_8; }
	inline bool* get_address_of_display_societe_8() { return &___display_societe_8; }
	inline void set_display_societe_8(bool value)
	{
		___display_societe_8 = value;
	}

	inline static int32_t get_offset_of_display_telephone_9() { return static_cast<int32_t>(offsetof(PanoForm_t499938582, ___display_telephone_9)); }
	inline bool get_display_telephone_9() const { return ___display_telephone_9; }
	inline bool* get_address_of_display_telephone_9() { return &___display_telephone_9; }
	inline void set_display_telephone_9(bool value)
	{
		___display_telephone_9 = value;
	}

	inline static int32_t get_offset_of_display_datenaissance_10() { return static_cast<int32_t>(offsetof(PanoForm_t499938582, ___display_datenaissance_10)); }
	inline bool get_display_datenaissance_10() const { return ___display_datenaissance_10; }
	inline bool* get_address_of_display_datenaissance_10() { return &___display_datenaissance_10; }
	inline void set_display_datenaissance_10(bool value)
	{
		___display_datenaissance_10 = value;
	}

	inline static int32_t get_offset_of_display_extra_11() { return static_cast<int32_t>(offsetof(PanoForm_t499938582, ___display_extra_11)); }
	inline bool get_display_extra_11() const { return ___display_extra_11; }
	inline bool* get_address_of_display_extra_11() { return &___display_extra_11; }
	inline void set_display_extra_11(bool value)
	{
		___display_extra_11 = value;
	}

	inline static int32_t get_offset_of_display_email_oblig_12() { return static_cast<int32_t>(offsetof(PanoForm_t499938582, ___display_email_oblig_12)); }
	inline bool get_display_email_oblig_12() const { return ___display_email_oblig_12; }
	inline bool* get_address_of_display_email_oblig_12() { return &___display_email_oblig_12; }
	inline void set_display_email_oblig_12(bool value)
	{
		___display_email_oblig_12 = value;
	}

	inline static int32_t get_offset_of_display_nom_oblig_13() { return static_cast<int32_t>(offsetof(PanoForm_t499938582, ___display_nom_oblig_13)); }
	inline bool get_display_nom_oblig_13() const { return ___display_nom_oblig_13; }
	inline bool* get_address_of_display_nom_oblig_13() { return &___display_nom_oblig_13; }
	inline void set_display_nom_oblig_13(bool value)
	{
		___display_nom_oblig_13 = value;
	}

	inline static int32_t get_offset_of_display_prenom_oblig_14() { return static_cast<int32_t>(offsetof(PanoForm_t499938582, ___display_prenom_oblig_14)); }
	inline bool get_display_prenom_oblig_14() const { return ___display_prenom_oblig_14; }
	inline bool* get_address_of_display_prenom_oblig_14() { return &___display_prenom_oblig_14; }
	inline void set_display_prenom_oblig_14(bool value)
	{
		___display_prenom_oblig_14 = value;
	}

	inline static int32_t get_offset_of_display_societe_oblig_15() { return static_cast<int32_t>(offsetof(PanoForm_t499938582, ___display_societe_oblig_15)); }
	inline bool get_display_societe_oblig_15() const { return ___display_societe_oblig_15; }
	inline bool* get_address_of_display_societe_oblig_15() { return &___display_societe_oblig_15; }
	inline void set_display_societe_oblig_15(bool value)
	{
		___display_societe_oblig_15 = value;
	}

	inline static int32_t get_offset_of_display_telephone_oblig_16() { return static_cast<int32_t>(offsetof(PanoForm_t499938582, ___display_telephone_oblig_16)); }
	inline bool get_display_telephone_oblig_16() const { return ___display_telephone_oblig_16; }
	inline bool* get_address_of_display_telephone_oblig_16() { return &___display_telephone_oblig_16; }
	inline void set_display_telephone_oblig_16(bool value)
	{
		___display_telephone_oblig_16 = value;
	}

	inline static int32_t get_offset_of_display_datenaissance_oblig_17() { return static_cast<int32_t>(offsetof(PanoForm_t499938582, ___display_datenaissance_oblig_17)); }
	inline bool get_display_datenaissance_oblig_17() const { return ___display_datenaissance_oblig_17; }
	inline bool* get_address_of_display_datenaissance_oblig_17() { return &___display_datenaissance_oblig_17; }
	inline void set_display_datenaissance_oblig_17(bool value)
	{
		___display_datenaissance_oblig_17 = value;
	}

	inline static int32_t get_offset_of_display_extra_oblig_18() { return static_cast<int32_t>(offsetof(PanoForm_t499938582, ___display_extra_oblig_18)); }
	inline bool get_display_extra_oblig_18() const { return ___display_extra_oblig_18; }
	inline bool* get_address_of_display_extra_oblig_18() { return &___display_extra_oblig_18; }
	inline void set_display_extra_oblig_18(bool value)
	{
		___display_extra_oblig_18 = value;
	}

	inline static int32_t get_offset_of_go_email_19() { return static_cast<int32_t>(offsetof(PanoForm_t499938582, ___go_email_19)); }
	inline GameObject_t1113636619 * get_go_email_19() const { return ___go_email_19; }
	inline GameObject_t1113636619 ** get_address_of_go_email_19() { return &___go_email_19; }
	inline void set_go_email_19(GameObject_t1113636619 * value)
	{
		___go_email_19 = value;
		Il2CppCodeGenWriteBarrier((&___go_email_19), value);
	}

	inline static int32_t get_offset_of_go_nom_20() { return static_cast<int32_t>(offsetof(PanoForm_t499938582, ___go_nom_20)); }
	inline GameObject_t1113636619 * get_go_nom_20() const { return ___go_nom_20; }
	inline GameObject_t1113636619 ** get_address_of_go_nom_20() { return &___go_nom_20; }
	inline void set_go_nom_20(GameObject_t1113636619 * value)
	{
		___go_nom_20 = value;
		Il2CppCodeGenWriteBarrier((&___go_nom_20), value);
	}

	inline static int32_t get_offset_of_go_prenom_21() { return static_cast<int32_t>(offsetof(PanoForm_t499938582, ___go_prenom_21)); }
	inline GameObject_t1113636619 * get_go_prenom_21() const { return ___go_prenom_21; }
	inline GameObject_t1113636619 ** get_address_of_go_prenom_21() { return &___go_prenom_21; }
	inline void set_go_prenom_21(GameObject_t1113636619 * value)
	{
		___go_prenom_21 = value;
		Il2CppCodeGenWriteBarrier((&___go_prenom_21), value);
	}

	inline static int32_t get_offset_of_go_societe_22() { return static_cast<int32_t>(offsetof(PanoForm_t499938582, ___go_societe_22)); }
	inline GameObject_t1113636619 * get_go_societe_22() const { return ___go_societe_22; }
	inline GameObject_t1113636619 ** get_address_of_go_societe_22() { return &___go_societe_22; }
	inline void set_go_societe_22(GameObject_t1113636619 * value)
	{
		___go_societe_22 = value;
		Il2CppCodeGenWriteBarrier((&___go_societe_22), value);
	}

	inline static int32_t get_offset_of_go_telephone_datenaissance_23() { return static_cast<int32_t>(offsetof(PanoForm_t499938582, ___go_telephone_datenaissance_23)); }
	inline GameObject_t1113636619 * get_go_telephone_datenaissance_23() const { return ___go_telephone_datenaissance_23; }
	inline GameObject_t1113636619 ** get_address_of_go_telephone_datenaissance_23() { return &___go_telephone_datenaissance_23; }
	inline void set_go_telephone_datenaissance_23(GameObject_t1113636619 * value)
	{
		___go_telephone_datenaissance_23 = value;
		Il2CppCodeGenWriteBarrier((&___go_telephone_datenaissance_23), value);
	}

	inline static int32_t get_offset_of_go_telephone_24() { return static_cast<int32_t>(offsetof(PanoForm_t499938582, ___go_telephone_24)); }
	inline GameObject_t1113636619 * get_go_telephone_24() const { return ___go_telephone_24; }
	inline GameObject_t1113636619 ** get_address_of_go_telephone_24() { return &___go_telephone_24; }
	inline void set_go_telephone_24(GameObject_t1113636619 * value)
	{
		___go_telephone_24 = value;
		Il2CppCodeGenWriteBarrier((&___go_telephone_24), value);
	}

	inline static int32_t get_offset_of_go_datenaissance_25() { return static_cast<int32_t>(offsetof(PanoForm_t499938582, ___go_datenaissance_25)); }
	inline GameObject_t1113636619 * get_go_datenaissance_25() const { return ___go_datenaissance_25; }
	inline GameObject_t1113636619 ** get_address_of_go_datenaissance_25() { return &___go_datenaissance_25; }
	inline void set_go_datenaissance_25(GameObject_t1113636619 * value)
	{
		___go_datenaissance_25 = value;
		Il2CppCodeGenWriteBarrier((&___go_datenaissance_25), value);
	}

	inline static int32_t get_offset_of_go_extra_26() { return static_cast<int32_t>(offsetof(PanoForm_t499938582, ___go_extra_26)); }
	inline GameObject_t1113636619 * get_go_extra_26() const { return ___go_extra_26; }
	inline GameObject_t1113636619 ** get_address_of_go_extra_26() { return &___go_extra_26; }
	inline void set_go_extra_26(GameObject_t1113636619 * value)
	{
		___go_extra_26 = value;
		Il2CppCodeGenWriteBarrier((&___go_extra_26), value);
	}

	inline static int32_t get_offset_of_if_email_27() { return static_cast<int32_t>(offsetof(PanoForm_t499938582, ___if_email_27)); }
	inline TMP_InputField_t1099764886 * get_if_email_27() const { return ___if_email_27; }
	inline TMP_InputField_t1099764886 ** get_address_of_if_email_27() { return &___if_email_27; }
	inline void set_if_email_27(TMP_InputField_t1099764886 * value)
	{
		___if_email_27 = value;
		Il2CppCodeGenWriteBarrier((&___if_email_27), value);
	}

	inline static int32_t get_offset_of_if_nom_28() { return static_cast<int32_t>(offsetof(PanoForm_t499938582, ___if_nom_28)); }
	inline TMP_InputField_t1099764886 * get_if_nom_28() const { return ___if_nom_28; }
	inline TMP_InputField_t1099764886 ** get_address_of_if_nom_28() { return &___if_nom_28; }
	inline void set_if_nom_28(TMP_InputField_t1099764886 * value)
	{
		___if_nom_28 = value;
		Il2CppCodeGenWriteBarrier((&___if_nom_28), value);
	}

	inline static int32_t get_offset_of_if_prenom_29() { return static_cast<int32_t>(offsetof(PanoForm_t499938582, ___if_prenom_29)); }
	inline TMP_InputField_t1099764886 * get_if_prenom_29() const { return ___if_prenom_29; }
	inline TMP_InputField_t1099764886 ** get_address_of_if_prenom_29() { return &___if_prenom_29; }
	inline void set_if_prenom_29(TMP_InputField_t1099764886 * value)
	{
		___if_prenom_29 = value;
		Il2CppCodeGenWriteBarrier((&___if_prenom_29), value);
	}

	inline static int32_t get_offset_of_if_societe_30() { return static_cast<int32_t>(offsetof(PanoForm_t499938582, ___if_societe_30)); }
	inline TMP_InputField_t1099764886 * get_if_societe_30() const { return ___if_societe_30; }
	inline TMP_InputField_t1099764886 ** get_address_of_if_societe_30() { return &___if_societe_30; }
	inline void set_if_societe_30(TMP_InputField_t1099764886 * value)
	{
		___if_societe_30 = value;
		Il2CppCodeGenWriteBarrier((&___if_societe_30), value);
	}

	inline static int32_t get_offset_of_if_telephone_31() { return static_cast<int32_t>(offsetof(PanoForm_t499938582, ___if_telephone_31)); }
	inline TMP_InputField_t1099764886 * get_if_telephone_31() const { return ___if_telephone_31; }
	inline TMP_InputField_t1099764886 ** get_address_of_if_telephone_31() { return &___if_telephone_31; }
	inline void set_if_telephone_31(TMP_InputField_t1099764886 * value)
	{
		___if_telephone_31 = value;
		Il2CppCodeGenWriteBarrier((&___if_telephone_31), value);
	}

	inline static int32_t get_offset_of_if_JJ_32() { return static_cast<int32_t>(offsetof(PanoForm_t499938582, ___if_JJ_32)); }
	inline TMP_InputField_t1099764886 * get_if_JJ_32() const { return ___if_JJ_32; }
	inline TMP_InputField_t1099764886 ** get_address_of_if_JJ_32() { return &___if_JJ_32; }
	inline void set_if_JJ_32(TMP_InputField_t1099764886 * value)
	{
		___if_JJ_32 = value;
		Il2CppCodeGenWriteBarrier((&___if_JJ_32), value);
	}

	inline static int32_t get_offset_of_if_MM_33() { return static_cast<int32_t>(offsetof(PanoForm_t499938582, ___if_MM_33)); }
	inline TMP_InputField_t1099764886 * get_if_MM_33() const { return ___if_MM_33; }
	inline TMP_InputField_t1099764886 ** get_address_of_if_MM_33() { return &___if_MM_33; }
	inline void set_if_MM_33(TMP_InputField_t1099764886 * value)
	{
		___if_MM_33 = value;
		Il2CppCodeGenWriteBarrier((&___if_MM_33), value);
	}

	inline static int32_t get_offset_of_if_AAAA_34() { return static_cast<int32_t>(offsetof(PanoForm_t499938582, ___if_AAAA_34)); }
	inline TMP_InputField_t1099764886 * get_if_AAAA_34() const { return ___if_AAAA_34; }
	inline TMP_InputField_t1099764886 ** get_address_of_if_AAAA_34() { return &___if_AAAA_34; }
	inline void set_if_AAAA_34(TMP_InputField_t1099764886 * value)
	{
		___if_AAAA_34 = value;
		Il2CppCodeGenWriteBarrier((&___if_AAAA_34), value);
	}

	inline static int32_t get_offset_of_if_extra_35() { return static_cast<int32_t>(offsetof(PanoForm_t499938582, ___if_extra_35)); }
	inline TMP_InputField_t1099764886 * get_if_extra_35() const { return ___if_extra_35; }
	inline TMP_InputField_t1099764886 ** get_address_of_if_extra_35() { return &___if_extra_35; }
	inline void set_if_extra_35(TMP_InputField_t1099764886 * value)
	{
		___if_extra_35 = value;
		Il2CppCodeGenWriteBarrier((&___if_extra_35), value);
	}

	inline static int32_t get_offset_of_if_email_oblig_36() { return static_cast<int32_t>(offsetof(PanoForm_t499938582, ___if_email_oblig_36)); }
	inline TextMeshProUGUI_t529313277 * get_if_email_oblig_36() const { return ___if_email_oblig_36; }
	inline TextMeshProUGUI_t529313277 ** get_address_of_if_email_oblig_36() { return &___if_email_oblig_36; }
	inline void set_if_email_oblig_36(TextMeshProUGUI_t529313277 * value)
	{
		___if_email_oblig_36 = value;
		Il2CppCodeGenWriteBarrier((&___if_email_oblig_36), value);
	}

	inline static int32_t get_offset_of_if_nom_oblig_37() { return static_cast<int32_t>(offsetof(PanoForm_t499938582, ___if_nom_oblig_37)); }
	inline TextMeshProUGUI_t529313277 * get_if_nom_oblig_37() const { return ___if_nom_oblig_37; }
	inline TextMeshProUGUI_t529313277 ** get_address_of_if_nom_oblig_37() { return &___if_nom_oblig_37; }
	inline void set_if_nom_oblig_37(TextMeshProUGUI_t529313277 * value)
	{
		___if_nom_oblig_37 = value;
		Il2CppCodeGenWriteBarrier((&___if_nom_oblig_37), value);
	}

	inline static int32_t get_offset_of_if_prenom_oblig_38() { return static_cast<int32_t>(offsetof(PanoForm_t499938582, ___if_prenom_oblig_38)); }
	inline TextMeshProUGUI_t529313277 * get_if_prenom_oblig_38() const { return ___if_prenom_oblig_38; }
	inline TextMeshProUGUI_t529313277 ** get_address_of_if_prenom_oblig_38() { return &___if_prenom_oblig_38; }
	inline void set_if_prenom_oblig_38(TextMeshProUGUI_t529313277 * value)
	{
		___if_prenom_oblig_38 = value;
		Il2CppCodeGenWriteBarrier((&___if_prenom_oblig_38), value);
	}

	inline static int32_t get_offset_of_if_societe_oblig_39() { return static_cast<int32_t>(offsetof(PanoForm_t499938582, ___if_societe_oblig_39)); }
	inline TextMeshProUGUI_t529313277 * get_if_societe_oblig_39() const { return ___if_societe_oblig_39; }
	inline TextMeshProUGUI_t529313277 ** get_address_of_if_societe_oblig_39() { return &___if_societe_oblig_39; }
	inline void set_if_societe_oblig_39(TextMeshProUGUI_t529313277 * value)
	{
		___if_societe_oblig_39 = value;
		Il2CppCodeGenWriteBarrier((&___if_societe_oblig_39), value);
	}

	inline static int32_t get_offset_of_if_telephone_oblig_40() { return static_cast<int32_t>(offsetof(PanoForm_t499938582, ___if_telephone_oblig_40)); }
	inline TextMeshProUGUI_t529313277 * get_if_telephone_oblig_40() const { return ___if_telephone_oblig_40; }
	inline TextMeshProUGUI_t529313277 ** get_address_of_if_telephone_oblig_40() { return &___if_telephone_oblig_40; }
	inline void set_if_telephone_oblig_40(TextMeshProUGUI_t529313277 * value)
	{
		___if_telephone_oblig_40 = value;
		Il2CppCodeGenWriteBarrier((&___if_telephone_oblig_40), value);
	}

	inline static int32_t get_offset_of_if_dateNaissance_oblig_41() { return static_cast<int32_t>(offsetof(PanoForm_t499938582, ___if_dateNaissance_oblig_41)); }
	inline TextMeshProUGUI_t529313277 * get_if_dateNaissance_oblig_41() const { return ___if_dateNaissance_oblig_41; }
	inline TextMeshProUGUI_t529313277 ** get_address_of_if_dateNaissance_oblig_41() { return &___if_dateNaissance_oblig_41; }
	inline void set_if_dateNaissance_oblig_41(TextMeshProUGUI_t529313277 * value)
	{
		___if_dateNaissance_oblig_41 = value;
		Il2CppCodeGenWriteBarrier((&___if_dateNaissance_oblig_41), value);
	}

	inline static int32_t get_offset_of_extraLabel_42() { return static_cast<int32_t>(offsetof(PanoForm_t499938582, ___extraLabel_42)); }
	inline TextMeshProUGUI_t529313277 * get_extraLabel_42() const { return ___extraLabel_42; }
	inline TextMeshProUGUI_t529313277 ** get_address_of_extraLabel_42() { return &___extraLabel_42; }
	inline void set_extraLabel_42(TextMeshProUGUI_t529313277 * value)
	{
		___extraLabel_42 = value;
		Il2CppCodeGenWriteBarrier((&___extraLabel_42), value);
	}

	inline static int32_t get_offset_of_UserGUID_43() { return static_cast<int32_t>(offsetof(PanoForm_t499938582, ___UserGUID_43)); }
	inline String_t* get_UserGUID_43() const { return ___UserGUID_43; }
	inline String_t** get_address_of_UserGUID_43() { return &___UserGUID_43; }
	inline void set_UserGUID_43(String_t* value)
	{
		___UserGUID_43 = value;
		Il2CppCodeGenWriteBarrier((&___UserGUID_43), value);
	}

	inline static int32_t get_offset_of_markerUID_44() { return static_cast<int32_t>(offsetof(PanoForm_t499938582, ___markerUID_44)); }
	inline String_t* get_markerUID_44() const { return ___markerUID_44; }
	inline String_t** get_address_of_markerUID_44() { return &___markerUID_44; }
	inline void set_markerUID_44(String_t* value)
	{
		___markerUID_44 = value;
		Il2CppCodeGenWriteBarrier((&___markerUID_44), value);
	}

	inline static int32_t get_offset_of_appName_45() { return static_cast<int32_t>(offsetof(PanoForm_t499938582, ___appName_45)); }
	inline String_t* get_appName_45() const { return ___appName_45; }
	inline String_t** get_address_of_appName_45() { return &___appName_45; }
	inline void set_appName_45(String_t* value)
	{
		___appName_45 = value;
		Il2CppCodeGenWriteBarrier((&___appName_45), value);
	}

	inline static int32_t get_offset_of_token_46() { return static_cast<int32_t>(offsetof(PanoForm_t499938582, ___token_46)); }
	inline String_t* get_token_46() const { return ___token_46; }
	inline String_t** get_address_of_token_46() { return &___token_46; }
	inline void set_token_46(String_t* value)
	{
		___token_46 = value;
		Il2CppCodeGenWriteBarrier((&___token_46), value);
	}

	inline static int32_t get_offset_of_link_of_reward_47() { return static_cast<int32_t>(offsetof(PanoForm_t499938582, ___link_of_reward_47)); }
	inline String_t* get_link_of_reward_47() const { return ___link_of_reward_47; }
	inline String_t** get_address_of_link_of_reward_47() { return &___link_of_reward_47; }
	inline void set_link_of_reward_47(String_t* value)
	{
		___link_of_reward_47 = value;
		Il2CppCodeGenWriteBarrier((&___link_of_reward_47), value);
	}

	inline static int32_t get_offset_of_extraStr_48() { return static_cast<int32_t>(offsetof(PanoForm_t499938582, ___extraStr_48)); }
	inline String_t* get_extraStr_48() const { return ___extraStr_48; }
	inline String_t** get_address_of_extraStr_48() { return &___extraStr_48; }
	inline void set_extraStr_48(String_t* value)
	{
		___extraStr_48 = value;
		Il2CppCodeGenWriteBarrier((&___extraStr_48), value);
	}

	inline static int32_t get_offset_of_panoError_49() { return static_cast<int32_t>(offsetof(PanoForm_t499938582, ___panoError_49)); }
	inline GameObject_t1113636619 * get_panoError_49() const { return ___panoError_49; }
	inline GameObject_t1113636619 ** get_address_of_panoError_49() { return &___panoError_49; }
	inline void set_panoError_49(GameObject_t1113636619 * value)
	{
		___panoError_49 = value;
		Il2CppCodeGenWriteBarrier((&___panoError_49), value);
	}

	inline static int32_t get_offset_of_panoGagne_50() { return static_cast<int32_t>(offsetof(PanoForm_t499938582, ___panoGagne_50)); }
	inline GameObject_t1113636619 * get_panoGagne_50() const { return ___panoGagne_50; }
	inline GameObject_t1113636619 ** get_address_of_panoGagne_50() { return &___panoGagne_50; }
	inline void set_panoGagne_50(GameObject_t1113636619 * value)
	{
		___panoGagne_50 = value;
		Il2CppCodeGenWriteBarrier((&___panoGagne_50), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PANOFORM_T499938582_H
#ifndef PANOGAGNE_T1959658388_H
#define PANOGAGNE_T1959658388_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PanoGagne
struct  PanoGagne_t1959658388  : public MonoBehaviour_t3962482529
{
public:
	// System.String PanoGagne::msg
	String_t* ___msg_4;
	// System.String PanoGagne::URLtogift
	String_t* ___URLtogift_5;
	// TMPro.TextMeshProUGUI PanoGagne::ui_msg
	TextMeshProUGUI_t529313277 * ___ui_msg_6;

public:
	inline static int32_t get_offset_of_msg_4() { return static_cast<int32_t>(offsetof(PanoGagne_t1959658388, ___msg_4)); }
	inline String_t* get_msg_4() const { return ___msg_4; }
	inline String_t** get_address_of_msg_4() { return &___msg_4; }
	inline void set_msg_4(String_t* value)
	{
		___msg_4 = value;
		Il2CppCodeGenWriteBarrier((&___msg_4), value);
	}

	inline static int32_t get_offset_of_URLtogift_5() { return static_cast<int32_t>(offsetof(PanoGagne_t1959658388, ___URLtogift_5)); }
	inline String_t* get_URLtogift_5() const { return ___URLtogift_5; }
	inline String_t** get_address_of_URLtogift_5() { return &___URLtogift_5; }
	inline void set_URLtogift_5(String_t* value)
	{
		___URLtogift_5 = value;
		Il2CppCodeGenWriteBarrier((&___URLtogift_5), value);
	}

	inline static int32_t get_offset_of_ui_msg_6() { return static_cast<int32_t>(offsetof(PanoGagne_t1959658388, ___ui_msg_6)); }
	inline TextMeshProUGUI_t529313277 * get_ui_msg_6() const { return ___ui_msg_6; }
	inline TextMeshProUGUI_t529313277 ** get_address_of_ui_msg_6() { return &___ui_msg_6; }
	inline void set_ui_msg_6(TextMeshProUGUI_t529313277 * value)
	{
		___ui_msg_6 = value;
		Il2CppCodeGenWriteBarrier((&___ui_msg_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PANOGAGNE_T1959658388_H
#ifndef PANOREGLES_T2340488907_H
#define PANOREGLES_T2340488907_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PanoRegles
struct  PanoRegles_t2340488907  : public MonoBehaviour_t3962482529
{
public:
	// System.String PanoRegles::URL_Regles
	String_t* ___URL_Regles_4;
	// UnityEngine.UI.Toggle PanoRegles::accepte
	Toggle_t2735377061 * ___accepte_5;
	// UnityEngine.GameObject PanoRegles::nextPanel
	GameObject_t1113636619 * ___nextPanel_6;
	// System.Boolean PanoRegles::isGagne
	bool ___isGagne_7;

public:
	inline static int32_t get_offset_of_URL_Regles_4() { return static_cast<int32_t>(offsetof(PanoRegles_t2340488907, ___URL_Regles_4)); }
	inline String_t* get_URL_Regles_4() const { return ___URL_Regles_4; }
	inline String_t** get_address_of_URL_Regles_4() { return &___URL_Regles_4; }
	inline void set_URL_Regles_4(String_t* value)
	{
		___URL_Regles_4 = value;
		Il2CppCodeGenWriteBarrier((&___URL_Regles_4), value);
	}

	inline static int32_t get_offset_of_accepte_5() { return static_cast<int32_t>(offsetof(PanoRegles_t2340488907, ___accepte_5)); }
	inline Toggle_t2735377061 * get_accepte_5() const { return ___accepte_5; }
	inline Toggle_t2735377061 ** get_address_of_accepte_5() { return &___accepte_5; }
	inline void set_accepte_5(Toggle_t2735377061 * value)
	{
		___accepte_5 = value;
		Il2CppCodeGenWriteBarrier((&___accepte_5), value);
	}

	inline static int32_t get_offset_of_nextPanel_6() { return static_cast<int32_t>(offsetof(PanoRegles_t2340488907, ___nextPanel_6)); }
	inline GameObject_t1113636619 * get_nextPanel_6() const { return ___nextPanel_6; }
	inline GameObject_t1113636619 ** get_address_of_nextPanel_6() { return &___nextPanel_6; }
	inline void set_nextPanel_6(GameObject_t1113636619 * value)
	{
		___nextPanel_6 = value;
		Il2CppCodeGenWriteBarrier((&___nextPanel_6), value);
	}

	inline static int32_t get_offset_of_isGagne_7() { return static_cast<int32_t>(offsetof(PanoRegles_t2340488907, ___isGagne_7)); }
	inline bool get_isGagne_7() const { return ___isGagne_7; }
	inline bool* get_address_of_isGagne_7() { return &___isGagne_7; }
	inline void set_isGagne_7(bool value)
	{
		___isGagne_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PANOREGLES_T2340488907_H
#ifndef SPLASHTIMER_T2585461764_H
#define SPLASHTIMER_T2585461764_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SplashTimer
struct  SplashTimer_t2585461764  : public MonoBehaviour_t3962482529
{
public:
	// System.String SplashTimer::appName
	String_t* ___appName_4;
	// UnityEngine.UI.Image SplashTimer::channelLogo
	Image_t2670269651 * ___channelLogo_5;
	// System.Single SplashTimer::delay
	float ___delay_6;
	// System.String SplashTimer::scene
	String_t* ___scene_7;
	// System.Int32 SplashTimer::selectedLanguageID
	int32_t ___selectedLanguageID_8;
	// System.String SplashTimer::selectedCountry
	String_t* ___selectedCountry_9;
	// UnityEngine.GameObject SplashTimer::panel
	GameObject_t1113636619 * ___panel_10;
	// System.Boolean SplashTimer::needToUseUpdate
	bool ___needToUseUpdate_11;

public:
	inline static int32_t get_offset_of_appName_4() { return static_cast<int32_t>(offsetof(SplashTimer_t2585461764, ___appName_4)); }
	inline String_t* get_appName_4() const { return ___appName_4; }
	inline String_t** get_address_of_appName_4() { return &___appName_4; }
	inline void set_appName_4(String_t* value)
	{
		___appName_4 = value;
		Il2CppCodeGenWriteBarrier((&___appName_4), value);
	}

	inline static int32_t get_offset_of_channelLogo_5() { return static_cast<int32_t>(offsetof(SplashTimer_t2585461764, ___channelLogo_5)); }
	inline Image_t2670269651 * get_channelLogo_5() const { return ___channelLogo_5; }
	inline Image_t2670269651 ** get_address_of_channelLogo_5() { return &___channelLogo_5; }
	inline void set_channelLogo_5(Image_t2670269651 * value)
	{
		___channelLogo_5 = value;
		Il2CppCodeGenWriteBarrier((&___channelLogo_5), value);
	}

	inline static int32_t get_offset_of_delay_6() { return static_cast<int32_t>(offsetof(SplashTimer_t2585461764, ___delay_6)); }
	inline float get_delay_6() const { return ___delay_6; }
	inline float* get_address_of_delay_6() { return &___delay_6; }
	inline void set_delay_6(float value)
	{
		___delay_6 = value;
	}

	inline static int32_t get_offset_of_scene_7() { return static_cast<int32_t>(offsetof(SplashTimer_t2585461764, ___scene_7)); }
	inline String_t* get_scene_7() const { return ___scene_7; }
	inline String_t** get_address_of_scene_7() { return &___scene_7; }
	inline void set_scene_7(String_t* value)
	{
		___scene_7 = value;
		Il2CppCodeGenWriteBarrier((&___scene_7), value);
	}

	inline static int32_t get_offset_of_selectedLanguageID_8() { return static_cast<int32_t>(offsetof(SplashTimer_t2585461764, ___selectedLanguageID_8)); }
	inline int32_t get_selectedLanguageID_8() const { return ___selectedLanguageID_8; }
	inline int32_t* get_address_of_selectedLanguageID_8() { return &___selectedLanguageID_8; }
	inline void set_selectedLanguageID_8(int32_t value)
	{
		___selectedLanguageID_8 = value;
	}

	inline static int32_t get_offset_of_selectedCountry_9() { return static_cast<int32_t>(offsetof(SplashTimer_t2585461764, ___selectedCountry_9)); }
	inline String_t* get_selectedCountry_9() const { return ___selectedCountry_9; }
	inline String_t** get_address_of_selectedCountry_9() { return &___selectedCountry_9; }
	inline void set_selectedCountry_9(String_t* value)
	{
		___selectedCountry_9 = value;
		Il2CppCodeGenWriteBarrier((&___selectedCountry_9), value);
	}

	inline static int32_t get_offset_of_panel_10() { return static_cast<int32_t>(offsetof(SplashTimer_t2585461764, ___panel_10)); }
	inline GameObject_t1113636619 * get_panel_10() const { return ___panel_10; }
	inline GameObject_t1113636619 ** get_address_of_panel_10() { return &___panel_10; }
	inline void set_panel_10(GameObject_t1113636619 * value)
	{
		___panel_10 = value;
		Il2CppCodeGenWriteBarrier((&___panel_10), value);
	}

	inline static int32_t get_offset_of_needToUseUpdate_11() { return static_cast<int32_t>(offsetof(SplashTimer_t2585461764, ___needToUseUpdate_11)); }
	inline bool get_needToUseUpdate_11() const { return ___needToUseUpdate_11; }
	inline bool* get_address_of_needToUseUpdate_11() { return &___needToUseUpdate_11; }
	inline void set_needToUseUpdate_11(bool value)
	{
		___needToUseUpdate_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPLASHTIMER_T2585461764_H
#ifndef TMP_OPENURL_T489659597_H
#define TMP_OPENURL_T489659597_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMP_OpenUrl
struct  TMP_OpenUrl_t489659597  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean TMP_OpenUrl::isLanguageSensitive
	bool ___isLanguageSensitive_4;
	// TMP_OpenUrl/LinkInfos[] TMP_OpenUrl::urls
	LinkInfosU5BU5D_t2629923838* ___urls_5;

public:
	inline static int32_t get_offset_of_isLanguageSensitive_4() { return static_cast<int32_t>(offsetof(TMP_OpenUrl_t489659597, ___isLanguageSensitive_4)); }
	inline bool get_isLanguageSensitive_4() const { return ___isLanguageSensitive_4; }
	inline bool* get_address_of_isLanguageSensitive_4() { return &___isLanguageSensitive_4; }
	inline void set_isLanguageSensitive_4(bool value)
	{
		___isLanguageSensitive_4 = value;
	}

	inline static int32_t get_offset_of_urls_5() { return static_cast<int32_t>(offsetof(TMP_OpenUrl_t489659597, ___urls_5)); }
	inline LinkInfosU5BU5D_t2629923838* get_urls_5() const { return ___urls_5; }
	inline LinkInfosU5BU5D_t2629923838** get_address_of_urls_5() { return &___urls_5; }
	inline void set_urls_5(LinkInfosU5BU5D_t2629923838* value)
	{
		___urls_5 = value;
		Il2CppCodeGenWriteBarrier((&___urls_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_OPENURL_T489659597_H
#ifndef TANGENTSPACEVISUALIZER_T140554864_H
#define TANGENTSPACEVISUALIZER_T140554864_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TangentSpaceVisualizer
struct  TangentSpaceVisualizer_t140554864  : public MonoBehaviour_t3962482529
{
public:
	// System.Single TangentSpaceVisualizer::offset
	float ___offset_4;
	// System.Single TangentSpaceVisualizer::scale
	float ___scale_5;

public:
	inline static int32_t get_offset_of_offset_4() { return static_cast<int32_t>(offsetof(TangentSpaceVisualizer_t140554864, ___offset_4)); }
	inline float get_offset_4() const { return ___offset_4; }
	inline float* get_address_of_offset_4() { return &___offset_4; }
	inline void set_offset_4(float value)
	{
		___offset_4 = value;
	}

	inline static int32_t get_offset_of_scale_5() { return static_cast<int32_t>(offsetof(TangentSpaceVisualizer_t140554864, ___scale_5)); }
	inline float get_scale_5() const { return ___scale_5; }
	inline float* get_address_of_scale_5() { return &___scale_5; }
	inline void set_scale_5(float value)
	{
		___scale_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TANGENTSPACEVISUALIZER_T140554864_H
#ifndef TUTOMANAGER_T4000134877_H
#define TUTOMANAGER_T4000134877_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TutoManager
struct  TutoManager_t4000134877  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject TutoManager::demo1
	GameObject_t1113636619 * ___demo1_4;
	// UnityEngine.GameObject TutoManager::demo2
	GameObject_t1113636619 * ___demo2_5;
	// UnityEngine.GameObject TutoManager::demo3
	GameObject_t1113636619 * ___demo3_6;
	// System.String TutoManager::url
	String_t* ___url_7;

public:
	inline static int32_t get_offset_of_demo1_4() { return static_cast<int32_t>(offsetof(TutoManager_t4000134877, ___demo1_4)); }
	inline GameObject_t1113636619 * get_demo1_4() const { return ___demo1_4; }
	inline GameObject_t1113636619 ** get_address_of_demo1_4() { return &___demo1_4; }
	inline void set_demo1_4(GameObject_t1113636619 * value)
	{
		___demo1_4 = value;
		Il2CppCodeGenWriteBarrier((&___demo1_4), value);
	}

	inline static int32_t get_offset_of_demo2_5() { return static_cast<int32_t>(offsetof(TutoManager_t4000134877, ___demo2_5)); }
	inline GameObject_t1113636619 * get_demo2_5() const { return ___demo2_5; }
	inline GameObject_t1113636619 ** get_address_of_demo2_5() { return &___demo2_5; }
	inline void set_demo2_5(GameObject_t1113636619 * value)
	{
		___demo2_5 = value;
		Il2CppCodeGenWriteBarrier((&___demo2_5), value);
	}

	inline static int32_t get_offset_of_demo3_6() { return static_cast<int32_t>(offsetof(TutoManager_t4000134877, ___demo3_6)); }
	inline GameObject_t1113636619 * get_demo3_6() const { return ___demo3_6; }
	inline GameObject_t1113636619 ** get_address_of_demo3_6() { return &___demo3_6; }
	inline void set_demo3_6(GameObject_t1113636619 * value)
	{
		___demo3_6 = value;
		Il2CppCodeGenWriteBarrier((&___demo3_6), value);
	}

	inline static int32_t get_offset_of_url_7() { return static_cast<int32_t>(offsetof(TutoManager_t4000134877, ___url_7)); }
	inline String_t* get_url_7() const { return ___url_7; }
	inline String_t** get_address_of_url_7() { return &___url_7; }
	inline void set_url_7(String_t* value)
	{
		___url_7 = value;
		Il2CppCodeGenWriteBarrier((&___url_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TUTOMANAGER_T4000134877_H
#ifndef BASESELECTOR_T1489849420_H
#define BASESELECTOR_T1489849420_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// baseSelector
struct  baseSelector_t1489849420  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.InputField baseSelector::inputField
	InputField_t3762917431 * ___inputField_4;

public:
	inline static int32_t get_offset_of_inputField_4() { return static_cast<int32_t>(offsetof(baseSelector_t1489849420, ___inputField_4)); }
	inline InputField_t3762917431 * get_inputField_4() const { return ___inputField_4; }
	inline InputField_t3762917431 ** get_address_of_inputField_4() { return &___inputField_4; }
	inline void set_inputField_4(InputField_t3762917431 * value)
	{
		___inputField_4 = value;
		Il2CppCodeGenWriteBarrier((&___inputField_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASESELECTOR_T1489849420_H
#ifndef CAPTUREANDSENDSCREEN_T3196082656_H
#define CAPTUREANDSENDSCREEN_T3196082656_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// captureAndSendScreen
struct  captureAndSendScreen_t3196082656  : public MonoBehaviour_t3962482529
{
public:
	// System.String captureAndSendScreen::screenShotURL
	String_t* ___screenShotURL_4;
	// System.String captureAndSendScreen::UDID_User
	String_t* ___UDID_User_5;
	// System.String captureAndSendScreen::INTERFACE
	String_t* ___INTERFACE_6;
	// UnityEngine.GameObject captureAndSendScreen::nodeTex
	GameObject_t1113636619 * ___nodeTex_7;

public:
	inline static int32_t get_offset_of_screenShotURL_4() { return static_cast<int32_t>(offsetof(captureAndSendScreen_t3196082656, ___screenShotURL_4)); }
	inline String_t* get_screenShotURL_4() const { return ___screenShotURL_4; }
	inline String_t** get_address_of_screenShotURL_4() { return &___screenShotURL_4; }
	inline void set_screenShotURL_4(String_t* value)
	{
		___screenShotURL_4 = value;
		Il2CppCodeGenWriteBarrier((&___screenShotURL_4), value);
	}

	inline static int32_t get_offset_of_UDID_User_5() { return static_cast<int32_t>(offsetof(captureAndSendScreen_t3196082656, ___UDID_User_5)); }
	inline String_t* get_UDID_User_5() const { return ___UDID_User_5; }
	inline String_t** get_address_of_UDID_User_5() { return &___UDID_User_5; }
	inline void set_UDID_User_5(String_t* value)
	{
		___UDID_User_5 = value;
		Il2CppCodeGenWriteBarrier((&___UDID_User_5), value);
	}

	inline static int32_t get_offset_of_INTERFACE_6() { return static_cast<int32_t>(offsetof(captureAndSendScreen_t3196082656, ___INTERFACE_6)); }
	inline String_t* get_INTERFACE_6() const { return ___INTERFACE_6; }
	inline String_t** get_address_of_INTERFACE_6() { return &___INTERFACE_6; }
	inline void set_INTERFACE_6(String_t* value)
	{
		___INTERFACE_6 = value;
		Il2CppCodeGenWriteBarrier((&___INTERFACE_6), value);
	}

	inline static int32_t get_offset_of_nodeTex_7() { return static_cast<int32_t>(offsetof(captureAndSendScreen_t3196082656, ___nodeTex_7)); }
	inline GameObject_t1113636619 * get_nodeTex_7() const { return ___nodeTex_7; }
	inline GameObject_t1113636619 ** get_address_of_nodeTex_7() { return &___nodeTex_7; }
	inline void set_nodeTex_7(GameObject_t1113636619 * value)
	{
		___nodeTex_7 = value;
		Il2CppCodeGenWriteBarrier((&___nodeTex_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAPTUREANDSENDSCREEN_T3196082656_H
#ifndef STARTUPLANG_T1517666504_H
#define STARTUPLANG_T1517666504_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// startUpLANG
struct  startUpLANG_t1517666504  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 startUpLANG::selectedLanguageID
	int32_t ___selectedLanguageID_4;

public:
	inline static int32_t get_offset_of_selectedLanguageID_4() { return static_cast<int32_t>(offsetof(startUpLANG_t1517666504, ___selectedLanguageID_4)); }
	inline int32_t get_selectedLanguageID_4() const { return ___selectedLanguageID_4; }
	inline int32_t* get_address_of_selectedLanguageID_4() { return &___selectedLanguageID_4; }
	inline void set_selectedLanguageID_4(int32_t value)
	{
		___selectedLanguageID_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STARTUPLANG_T1517666504_H
#ifndef TESTIMAGE_T2491645257_H
#define TESTIMAGE_T2491645257_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// testImage
struct  testImage_t2491645257  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject testImage::plane
	GameObject_t1113636619 * ___plane_4;
	// UnityEngine.UI.Image testImage::rawImage
	Image_t2670269651 * ___rawImage_5;
	// UnityEngine.UI.RawImage testImage::rawImageLandscape
	RawImage_t3182918964 * ___rawImageLandscape_6;

public:
	inline static int32_t get_offset_of_plane_4() { return static_cast<int32_t>(offsetof(testImage_t2491645257, ___plane_4)); }
	inline GameObject_t1113636619 * get_plane_4() const { return ___plane_4; }
	inline GameObject_t1113636619 ** get_address_of_plane_4() { return &___plane_4; }
	inline void set_plane_4(GameObject_t1113636619 * value)
	{
		___plane_4 = value;
		Il2CppCodeGenWriteBarrier((&___plane_4), value);
	}

	inline static int32_t get_offset_of_rawImage_5() { return static_cast<int32_t>(offsetof(testImage_t2491645257, ___rawImage_5)); }
	inline Image_t2670269651 * get_rawImage_5() const { return ___rawImage_5; }
	inline Image_t2670269651 ** get_address_of_rawImage_5() { return &___rawImage_5; }
	inline void set_rawImage_5(Image_t2670269651 * value)
	{
		___rawImage_5 = value;
		Il2CppCodeGenWriteBarrier((&___rawImage_5), value);
	}

	inline static int32_t get_offset_of_rawImageLandscape_6() { return static_cast<int32_t>(offsetof(testImage_t2491645257, ___rawImageLandscape_6)); }
	inline RawImage_t3182918964 * get_rawImageLandscape_6() const { return ___rawImageLandscape_6; }
	inline RawImage_t3182918964 ** get_address_of_rawImageLandscape_6() { return &___rawImageLandscape_6; }
	inline void set_rawImageLandscape_6(RawImage_t3182918964 * value)
	{
		___rawImageLandscape_6 = value;
		Il2CppCodeGenWriteBarrier((&___rawImageLandscape_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TESTIMAGE_T2491645257_H
#ifndef TESTPLAYER_T3643834628_H
#define TESTPLAYER_T3643834628_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// testPlayer
struct  testPlayer_t3643834628  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Video.VideoPlayer testPlayer::videoPlayer
	VideoPlayer_t1683042537 * ___videoPlayer_4;

public:
	inline static int32_t get_offset_of_videoPlayer_4() { return static_cast<int32_t>(offsetof(testPlayer_t3643834628, ___videoPlayer_4)); }
	inline VideoPlayer_t1683042537 * get_videoPlayer_4() const { return ___videoPlayer_4; }
	inline VideoPlayer_t1683042537 ** get_address_of_videoPlayer_4() { return &___videoPlayer_4; }
	inline void set_videoPlayer_4(VideoPlayer_t1683042537 * value)
	{
		___videoPlayer_4 = value;
		Il2CppCodeGenWriteBarrier((&___videoPlayer_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TESTPLAYER_T3643834628_H
#ifndef TESTRESCALE_T3860525213_H
#define TESTRESCALE_T3860525213_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// testRescale
struct  testRescale_t3860525213  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Bounds testRescale::combinedBounds
	Bounds_t2266837910  ___combinedBounds_4;

public:
	inline static int32_t get_offset_of_combinedBounds_4() { return static_cast<int32_t>(offsetof(testRescale_t3860525213, ___combinedBounds_4)); }
	inline Bounds_t2266837910  get_combinedBounds_4() const { return ___combinedBounds_4; }
	inline Bounds_t2266837910 * get_address_of_combinedBounds_4() { return &___combinedBounds_4; }
	inline void set_combinedBounds_4(Bounds_t2266837910  value)
	{
		___combinedBounds_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TESTRESCALE_T3860525213_H
#ifndef PAGE_T2586579457_H
#define PAGE_T2586579457_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Page
struct  Page_t2586579457  : public DynamicObject_t314656423
{
public:
	// PageTransition Page::pageTransition
	PageTransition_t3158442731 * ___pageTransition_18;
	// System.Boolean Page::isFirstPage
	bool ___isFirstPage_19;
	// Page[] Page::previousPages
	PageU5BU5D_t2014447836* ___previousPages_20;
	// Page Page::previousPage
	Page_t2586579457 * ___previousPage_21;
	// Page[] Page::nextPages
	PageU5BU5D_t2014447836* ___nextPages_22;
	// Page Page::nextPage
	Page_t2586579457 * ___nextPage_23;
	// Page/Status Page::status
	int32_t ___status_24;
	// UnityEngine.Vector3 Page::previousTransition
	Vector3_t3722313464  ___previousTransition_25;

public:
	inline static int32_t get_offset_of_pageTransition_18() { return static_cast<int32_t>(offsetof(Page_t2586579457, ___pageTransition_18)); }
	inline PageTransition_t3158442731 * get_pageTransition_18() const { return ___pageTransition_18; }
	inline PageTransition_t3158442731 ** get_address_of_pageTransition_18() { return &___pageTransition_18; }
	inline void set_pageTransition_18(PageTransition_t3158442731 * value)
	{
		___pageTransition_18 = value;
		Il2CppCodeGenWriteBarrier((&___pageTransition_18), value);
	}

	inline static int32_t get_offset_of_isFirstPage_19() { return static_cast<int32_t>(offsetof(Page_t2586579457, ___isFirstPage_19)); }
	inline bool get_isFirstPage_19() const { return ___isFirstPage_19; }
	inline bool* get_address_of_isFirstPage_19() { return &___isFirstPage_19; }
	inline void set_isFirstPage_19(bool value)
	{
		___isFirstPage_19 = value;
	}

	inline static int32_t get_offset_of_previousPages_20() { return static_cast<int32_t>(offsetof(Page_t2586579457, ___previousPages_20)); }
	inline PageU5BU5D_t2014447836* get_previousPages_20() const { return ___previousPages_20; }
	inline PageU5BU5D_t2014447836** get_address_of_previousPages_20() { return &___previousPages_20; }
	inline void set_previousPages_20(PageU5BU5D_t2014447836* value)
	{
		___previousPages_20 = value;
		Il2CppCodeGenWriteBarrier((&___previousPages_20), value);
	}

	inline static int32_t get_offset_of_previousPage_21() { return static_cast<int32_t>(offsetof(Page_t2586579457, ___previousPage_21)); }
	inline Page_t2586579457 * get_previousPage_21() const { return ___previousPage_21; }
	inline Page_t2586579457 ** get_address_of_previousPage_21() { return &___previousPage_21; }
	inline void set_previousPage_21(Page_t2586579457 * value)
	{
		___previousPage_21 = value;
		Il2CppCodeGenWriteBarrier((&___previousPage_21), value);
	}

	inline static int32_t get_offset_of_nextPages_22() { return static_cast<int32_t>(offsetof(Page_t2586579457, ___nextPages_22)); }
	inline PageU5BU5D_t2014447836* get_nextPages_22() const { return ___nextPages_22; }
	inline PageU5BU5D_t2014447836** get_address_of_nextPages_22() { return &___nextPages_22; }
	inline void set_nextPages_22(PageU5BU5D_t2014447836* value)
	{
		___nextPages_22 = value;
		Il2CppCodeGenWriteBarrier((&___nextPages_22), value);
	}

	inline static int32_t get_offset_of_nextPage_23() { return static_cast<int32_t>(offsetof(Page_t2586579457, ___nextPage_23)); }
	inline Page_t2586579457 * get_nextPage_23() const { return ___nextPage_23; }
	inline Page_t2586579457 ** get_address_of_nextPage_23() { return &___nextPage_23; }
	inline void set_nextPage_23(Page_t2586579457 * value)
	{
		___nextPage_23 = value;
		Il2CppCodeGenWriteBarrier((&___nextPage_23), value);
	}

	inline static int32_t get_offset_of_status_24() { return static_cast<int32_t>(offsetof(Page_t2586579457, ___status_24)); }
	inline int32_t get_status_24() const { return ___status_24; }
	inline int32_t* get_address_of_status_24() { return &___status_24; }
	inline void set_status_24(int32_t value)
	{
		___status_24 = value;
	}

	inline static int32_t get_offset_of_previousTransition_25() { return static_cast<int32_t>(offsetof(Page_t2586579457, ___previousTransition_25)); }
	inline Vector3_t3722313464  get_previousTransition_25() const { return ___previousTransition_25; }
	inline Vector3_t3722313464 * get_address_of_previousTransition_25() { return &___previousTransition_25; }
	inline void set_previousTransition_25(Vector3_t3722313464  value)
	{
		___previousTransition_25 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PAGE_T2586579457_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2900 = { sizeof (baseSelector_t1489849420), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2900[1] = 
{
	baseSelector_t1489849420::get_offset_of_inputField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2901 = { sizeof (CameraFacingBillboard_t1991403390), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2901[3] = 
{
	CameraFacingBillboard_t1991403390::get_offset_of_referenceCamera_4(),
	CameraFacingBillboard_t1991403390::get_offset_of_reverseFace_5(),
	CameraFacingBillboard_t1991403390::get_offset_of_axis_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2902 = { sizeof (Axis_t1613265627)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2902[7] = 
{
	Axis_t1613265627::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2903 = { sizeof (captureAndSendScreen_t3196082656), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2903[4] = 
{
	captureAndSendScreen_t3196082656::get_offset_of_screenShotURL_4(),
	captureAndSendScreen_t3196082656::get_offset_of_UDID_User_5(),
	captureAndSendScreen_t3196082656::get_offset_of_INTERFACE_6(),
	captureAndSendScreen_t3196082656::get_offset_of_nodeTex_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2904 = { sizeof (U3CIMPL_sendCaptureU3Ec__Iterator0_t1292966789), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2904[11] = 
{
	U3CIMPL_sendCaptureU3Ec__Iterator0_t1292966789::get_offset_of_U3CwidthU3E__0_0(),
	U3CIMPL_sendCaptureU3Ec__Iterator0_t1292966789::get_offset_of_U3CheightU3E__0_1(),
	U3CIMPL_sendCaptureU3Ec__Iterator0_t1292966789::get_offset_of_U3CrendererBackU3E__0_2(),
	U3CIMPL_sendCaptureU3Ec__Iterator0_t1292966789::get_offset_of_U3CtexU3E__0_3(),
	U3CIMPL_sendCaptureU3Ec__Iterator0_t1292966789::get_offset_of_U3CbytesU3E__0_4(),
	U3CIMPL_sendCaptureU3Ec__Iterator0_t1292966789::get_offset_of_U3CformU3E__0_5(),
	U3CIMPL_sendCaptureU3Ec__Iterator0_t1292966789::get_offset_of_U3CwU3E__0_6(),
	U3CIMPL_sendCaptureU3Ec__Iterator0_t1292966789::get_offset_of_U24this_7(),
	U3CIMPL_sendCaptureU3Ec__Iterator0_t1292966789::get_offset_of_U24current_8(),
	U3CIMPL_sendCaptureU3Ec__Iterator0_t1292966789::get_offset_of_U24disposing_9(),
	U3CIMPL_sendCaptureU3Ec__Iterator0_t1292966789::get_offset_of_U24PC_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2905 = { sizeof (Click3D_t85340219), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2905[28] = 
{
	Click3D_t85340219::get_offset_of_fullscreenBtn_4(),
	Click3D_t85340219::get_offset_of_playBtn_5(),
	Click3D_t85340219::get_offset_of_pauseBtn_6(),
	Click3D_t85340219::get_offset_of_closeBtn_7(),
	Click3D_t85340219::get_offset_of_ray_8(),
	Click3D_t85340219::get_offset_of_hitAllTemp_9(),
	Click3D_t85340219::get_offset_of_collider3D_10(),
	Click3D_t85340219::get_offset_of_url3D_11(),
	Click3D_t85340219::get_offset_of_GUID_12(),
	Click3D_t85340219::get_offset_of_container_13(),
	Click3D_t85340219::get_offset_of_closeButton_14(),
	Click3D_t85340219::get_offset_of_contentManager_15(),
	Click3D_t85340219::get_offset_of_face_16(),
	Click3D_t85340219::get_offset_of_FS_OBJ_container_17(),
	Click3D_t85340219::get_offset_of_FS_OBJ_18(),
	Click3D_t85340219::get_offset_of_gears_19(),
	Click3D_t85340219::get_offset_of_CameraOBJContainer_20(),
	Click3D_t85340219::get_offset_of_sphereCloseUp_21(),
	Click3D_t85340219::get_offset_of_text3d_22(),
	Click3D_t85340219::get_offset_of_cubeEtalon_23(),
	Click3D_t85340219::get_offset_of_hasBeenDownloaded_24(),
	Click3D_t85340219::get_offset_of_requestAutoPlay_25(),
	0,
	Click3D_t85340219::get_offset_of_isOpen_27(),
	Click3D_t85340219::get_offset_of_ID_28(),
	Click3D_t85340219::get_offset_of_isDownloading_29(),
	Click3D_t85340219::get_offset_of_combinedBounds_30(),
	Click3D_t85340219::get_offset_of_pathDownloading_31(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2906 = { sizeof (U3Cdownload3DObjectU3Ec__Iterator0_t465293986), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2906[4] = 
{
	U3Cdownload3DObjectU3Ec__Iterator0_t465293986::get_offset_of_U24this_0(),
	U3Cdownload3DObjectU3Ec__Iterator0_t465293986::get_offset_of_U24current_1(),
	U3Cdownload3DObjectU3Ec__Iterator0_t465293986::get_offset_of_U24disposing_2(),
	U3Cdownload3DObjectU3Ec__Iterator0_t465293986::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2907 = { sizeof (U3CloadIMPU3Ec__Iterator1_t3157777824), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2907[14] = 
{
	U3CloadIMPU3Ec__Iterator1_t3157777824::get_offset_of_U3CoutputFileU3E__0_0(),
	U3CloadIMPU3Ec__Iterator1_t3157777824::get_offset_of_U3CoutputPathU3E__0_1(),
	U3CloadIMPU3Ec__Iterator1_t3157777824::get_offset_of_url_2(),
	U3CloadIMPU3Ec__Iterator1_t3157777824::get_offset_of_U3CwwwU3E__1_3(),
	U3CloadIMPU3Ec__Iterator1_t3157777824::get_offset_of_U3CvalU3E__2_4(),
	U3CloadIMPU3Ec__Iterator1_t3157777824::get_offset_of_U3CvalfU3E__2_5(),
	U3CloadIMPU3Ec__Iterator1_t3157777824::get_offset_of_U3CdataU3E__1_6(),
	U3CloadIMPU3Ec__Iterator1_t3157777824::get_offset_of_U3CcontainerBoundsU3E__0_7(),
	U3CloadIMPU3Ec__Iterator1_t3157777824::get_offset_of_U3CobjU3E__0_8(),
	U3CloadIMPU3Ec__Iterator1_t3157777824::get_offset_of_U3CradiusU3E__0_9(),
	U3CloadIMPU3Ec__Iterator1_t3157777824::get_offset_of_U24this_10(),
	U3CloadIMPU3Ec__Iterator1_t3157777824::get_offset_of_U24current_11(),
	U3CloadIMPU3Ec__Iterator1_t3157777824::get_offset_of_U24disposing_12(),
	U3CloadIMPU3Ec__Iterator1_t3157777824::get_offset_of_U24PC_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2908 = { sizeof (ClickAudio_t2208965782), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2908[14] = 
{
	ClickAudio_t2208965782::get_offset_of_hit_4(),
	ClickAudio_t2208965782::get_offset_of_ray_5(),
	ClickAudio_t2208965782::get_offset_of_hitAllTemp_6(),
	ClickAudio_t2208965782::get_offset_of_colliderPlay_7(),
	ClickAudio_t2208965782::get_offset_of_colliderPause_8(),
	ClickAudio_t2208965782::get_offset_of_colliderSoundOn_9(),
	ClickAudio_t2208965782::get_offset_of_colliderSoundOff_10(),
	ClickAudio_t2208965782::get_offset_of_colliderPlayer_11(),
	ClickAudio_t2208965782::get_offset_of_audioURL_12(),
	ClickAudio_t2208965782::get_offset_of_image_13(),
	ClickAudio_t2208965782::get_offset_of_plane_14(),
	ClickAudio_t2208965782::get_offset_of_button_15(),
	ClickAudio_t2208965782::get_offset_of_requestAutoPlay_16(),
	ClickAudio_t2208965782::get_offset_of_audioPlayer_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2909 = { sizeof (U3CloadImageU3Ec__Iterator0_t710270773), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2909[12] = 
{
	U3CloadImageU3Ec__Iterator0_t710270773::get_offset_of_U3CwwwU3E__0_0(),
	U3CloadImageU3Ec__Iterator0_t710270773::get_offset_of_U3CrdU3E__0_1(),
	U3CloadImageU3Ec__Iterator0_t710270773::get_offset_of_posX_2(),
	U3CloadImageU3Ec__Iterator0_t710270773::get_offset_of_posY_3(),
	U3CloadImageU3Ec__Iterator0_t710270773::get_offset_of_sizeX_4(),
	U3CloadImageU3Ec__Iterator0_t710270773::get_offset_of_sizeY_5(),
	U3CloadImageU3Ec__Iterator0_t710270773::get_offset_of_U3CratioU3E__0_6(),
	U3CloadImageU3Ec__Iterator0_t710270773::get_offset_of_U3CwwwAudioU3E__0_7(),
	U3CloadImageU3Ec__Iterator0_t710270773::get_offset_of_U24this_8(),
	U3CloadImageU3Ec__Iterator0_t710270773::get_offset_of_U24current_9(),
	U3CloadImageU3Ec__Iterator0_t710270773::get_offset_of_U24disposing_10(),
	U3CloadImageU3Ec__Iterator0_t710270773::get_offset_of_U24PC_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2910 = { sizeof (ClickMe_t220080192), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2910[23] = 
{
	ClickMe_t220080192::get_offset_of_hit_4(),
	ClickMe_t220080192::get_offset_of_ray_5(),
	ClickMe_t220080192::get_offset_of_hitAllTemp_6(),
	ClickMe_t220080192::get_offset_of_colliderFullScreen_7(),
	ClickMe_t220080192::get_offset_of_colliderPlay_8(),
	ClickMe_t220080192::get_offset_of_colliderPause_9(),
	ClickMe_t220080192::get_offset_of_colliderSoundOn_10(),
	ClickMe_t220080192::get_offset_of_colliderSoundOff_11(),
	ClickMe_t220080192::get_offset_of_colliderPlayer_12(),
	ClickMe_t220080192::get_offset_of_fullscreenVideoPlayer_13(),
	ClickMe_t220080192::get_offset_of_playerLandscape_14(),
	ClickMe_t220080192::get_offset_of_playerPortrait_15(),
	ClickMe_t220080192::get_offset_of_landscape_Raw_16(),
	ClickMe_t220080192::get_offset_of_portrait_Raw_17(),
	ClickMe_t220080192::get_offset_of_urlVideo_18(),
	ClickMe_t220080192::get_offset_of_vidWidth_19(),
	ClickMe_t220080192::get_offset_of_vidHeight_20(),
	ClickMe_t220080192::get_offset_of_image_21(),
	ClickMe_t220080192::get_offset_of_playerCtrl3D_22(),
	ClickMe_t220080192::get_offset_of_iconeFS_23(),
	ClickMe_t220080192::get_offset_of_plane_24(),
	ClickMe_t220080192::get_offset_of_button_25(),
	ClickMe_t220080192::get_offset_of_playerVideo3D_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2911 = { sizeof (U3CloadImageU3Ec__Iterator0_t3805092064), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2911[11] = 
{
	U3CloadImageU3Ec__Iterator0_t3805092064::get_offset_of_U3CwwwU3E__0_0(),
	U3CloadImageU3Ec__Iterator0_t3805092064::get_offset_of_U3CrdU3E__0_1(),
	U3CloadImageU3Ec__Iterator0_t3805092064::get_offset_of_posX_2(),
	U3CloadImageU3Ec__Iterator0_t3805092064::get_offset_of_posY_3(),
	U3CloadImageU3Ec__Iterator0_t3805092064::get_offset_of_sizeX_4(),
	U3CloadImageU3Ec__Iterator0_t3805092064::get_offset_of_sizeY_5(),
	U3CloadImageU3Ec__Iterator0_t3805092064::get_offset_of_U3CratioU3E__0_6(),
	U3CloadImageU3Ec__Iterator0_t3805092064::get_offset_of_U24this_7(),
	U3CloadImageU3Ec__Iterator0_t3805092064::get_offset_of_U24current_8(),
	U3CloadImageU3Ec__Iterator0_t3805092064::get_offset_of_U24disposing_9(),
	U3CloadImageU3Ec__Iterator0_t3805092064::get_offset_of_U24PC_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2912 = { sizeof (U3CplayVideo_IMPU3Ec__Iterator1_t2231702528), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2912[4] = 
{
	U3CplayVideo_IMPU3Ec__Iterator1_t2231702528::get_offset_of_U24this_0(),
	U3CplayVideo_IMPU3Ec__Iterator1_t2231702528::get_offset_of_U24current_1(),
	U3CplayVideo_IMPU3Ec__Iterator1_t2231702528::get_offset_of_U24disposing_2(),
	U3CplayVideo_IMPU3Ec__Iterator1_t2231702528::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2913 = { sizeof (ClickMeChroma_t2203948016), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2913[15] = 
{
	ClickMeChroma_t2203948016::get_offset_of_hit_4(),
	ClickMeChroma_t2203948016::get_offset_of_ray_5(),
	ClickMeChroma_t2203948016::get_offset_of_hitAllTemp_6(),
	ClickMeChroma_t2203948016::get_offset_of_collider_7(),
	ClickMeChroma_t2203948016::get_offset_of_urlVideo_8(),
	ClickMeChroma_t2203948016::get_offset_of_vidWidth_9(),
	ClickMeChroma_t2203948016::get_offset_of_vidHeight_10(),
	ClickMeChroma_t2203948016::get_offset_of_image_11(),
	ClickMeChroma_t2203948016::get_offset_of_plane_12(),
	ClickMeChroma_t2203948016::get_offset_of_button_13(),
	ClickMeChroma_t2203948016::get_offset_of_playerContainer_14(),
	ClickMeChroma_t2203948016::get_offset_of_videoPlayer_15(),
	ClickMeChroma_t2203948016::get_offset_of_playerChromakey_16(),
	ClickMeChroma_t2203948016::get_offset_of_requestAutoPlay_17(),
	ClickMeChroma_t2203948016::get_offset_of_initialized_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2914 = { sizeof (U3CloadImageU3Ec__Iterator0_t689634614), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2914[12] = 
{
	U3CloadImageU3Ec__Iterator0_t689634614::get_offset_of_U3CwwwU3E__0_0(),
	U3CloadImageU3Ec__Iterator0_t689634614::get_offset_of_U3CrdU3E__0_1(),
	U3CloadImageU3Ec__Iterator0_t689634614::get_offset_of_posX_2(),
	U3CloadImageU3Ec__Iterator0_t689634614::get_offset_of_posY_3(),
	U3CloadImageU3Ec__Iterator0_t689634614::get_offset_of_sizeX_4(),
	U3CloadImageU3Ec__Iterator0_t689634614::get_offset_of_sizeY_5(),
	U3CloadImageU3Ec__Iterator0_t689634614::get_offset_of_U3CratioU3E__0_6(),
	U3CloadImageU3Ec__Iterator0_t689634614::get_offset_of_inclinaison_7(),
	U3CloadImageU3Ec__Iterator0_t689634614::get_offset_of_U24this_8(),
	U3CloadImageU3Ec__Iterator0_t689634614::get_offset_of_U24current_9(),
	U3CloadImageU3Ec__Iterator0_t689634614::get_offset_of_U24disposing_10(),
	U3CloadImageU3Ec__Iterator0_t689634614::get_offset_of_U24PC_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2915 = { sizeof (U3CprepareVideo_IMPU3Ec__Iterator1_t2281535404), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2915[5] = 
{
	U3CprepareVideo_IMPU3Ec__Iterator1_t2281535404::get_offset_of_U3CwaitTimeU3E__0_0(),
	U3CprepareVideo_IMPU3Ec__Iterator1_t2281535404::get_offset_of_U24this_1(),
	U3CprepareVideo_IMPU3Ec__Iterator1_t2281535404::get_offset_of_U24current_2(),
	U3CprepareVideo_IMPU3Ec__Iterator1_t2281535404::get_offset_of_U24disposing_3(),
	U3CprepareVideo_IMPU3Ec__Iterator1_t2281535404::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2916 = { sizeof (U3CplayVideo_IMPU3Ec__Iterator2_t2424110195), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2916[4] = 
{
	U3CplayVideo_IMPU3Ec__Iterator2_t2424110195::get_offset_of_U24this_0(),
	U3CplayVideo_IMPU3Ec__Iterator2_t2424110195::get_offset_of_U24current_1(),
	U3CplayVideo_IMPU3Ec__Iterator2_t2424110195::get_offset_of_U24disposing_2(),
	U3CplayVideo_IMPU3Ec__Iterator2_t2424110195::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2917 = { sizeof (ClickVideo_t284620550), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2917[29] = 
{
	ClickVideo_t284620550::get_offset_of_isMarkedForDistroy_4(),
	ClickVideo_t284620550::get_offset_of_initialized_5(),
	ClickVideo_t284620550::get_offset_of_hit_6(),
	ClickVideo_t284620550::get_offset_of_ray_7(),
	ClickVideo_t284620550::get_offset_of_hitAllTemp_8(),
	ClickVideo_t284620550::get_offset_of_imgTest_9(),
	ClickVideo_t284620550::get_offset_of_colliderFullScreen_10(),
	ClickVideo_t284620550::get_offset_of_colliderPlay_11(),
	ClickVideo_t284620550::get_offset_of_colliderPause_12(),
	ClickVideo_t284620550::get_offset_of_colliderSoundOn_13(),
	ClickVideo_t284620550::get_offset_of_colliderSoundOff_14(),
	ClickVideo_t284620550::get_offset_of_colliderPlayer_15(),
	ClickVideo_t284620550::get_offset_of_fullscreenVideoPlayer_16(),
	ClickVideo_t284620550::get_offset_of_playerLandscape_17(),
	ClickVideo_t284620550::get_offset_of_playerPortrait_18(),
	ClickVideo_t284620550::get_offset_of_landscape_Raw_19(),
	ClickVideo_t284620550::get_offset_of_portrait_Raw_20(),
	ClickVideo_t284620550::get_offset_of_urlVideo_21(),
	ClickVideo_t284620550::get_offset_of_vidWidth_22(),
	ClickVideo_t284620550::get_offset_of_vidHeight_23(),
	ClickVideo_t284620550::get_offset_of_image_24(),
	ClickVideo_t284620550::get_offset_of_playerCtrl3D_25(),
	ClickVideo_t284620550::get_offset_of_iconeFS_26(),
	ClickVideo_t284620550::get_offset_of_plane_27(),
	ClickVideo_t284620550::get_offset_of_button_28(),
	ClickVideo_t284620550::get_offset_of_playerVideo3D_29(),
	ClickVideo_t284620550::get_offset_of_fsvp_30(),
	ClickVideo_t284620550::get_offset_of_GO_playerContainer_31(),
	ClickVideo_t284620550::get_offset_of_requestAutoPlay_32(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2918 = { sizeof (U3CloadImageU3Ec__Iterator0_t2755249628), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2918[11] = 
{
	U3CloadImageU3Ec__Iterator0_t2755249628::get_offset_of_U3CwwwU3E__0_0(),
	U3CloadImageU3Ec__Iterator0_t2755249628::get_offset_of_U3CrdU3E__0_1(),
	U3CloadImageU3Ec__Iterator0_t2755249628::get_offset_of_posX_2(),
	U3CloadImageU3Ec__Iterator0_t2755249628::get_offset_of_posY_3(),
	U3CloadImageU3Ec__Iterator0_t2755249628::get_offset_of_sizeX_4(),
	U3CloadImageU3Ec__Iterator0_t2755249628::get_offset_of_sizeY_5(),
	U3CloadImageU3Ec__Iterator0_t2755249628::get_offset_of_U3CratioU3E__0_6(),
	U3CloadImageU3Ec__Iterator0_t2755249628::get_offset_of_U24this_7(),
	U3CloadImageU3Ec__Iterator0_t2755249628::get_offset_of_U24current_8(),
	U3CloadImageU3Ec__Iterator0_t2755249628::get_offset_of_U24disposing_9(),
	U3CloadImageU3Ec__Iterator0_t2755249628::get_offset_of_U24PC_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2919 = { sizeof (U3CprepareVideo_IMPU3Ec__Iterator1_t2339890399), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2919[5] = 
{
	U3CprepareVideo_IMPU3Ec__Iterator1_t2339890399::get_offset_of_U3CwaitTimeU3E__0_0(),
	U3CprepareVideo_IMPU3Ec__Iterator1_t2339890399::get_offset_of_U24this_1(),
	U3CprepareVideo_IMPU3Ec__Iterator1_t2339890399::get_offset_of_U24current_2(),
	U3CprepareVideo_IMPU3Ec__Iterator1_t2339890399::get_offset_of_U24disposing_3(),
	U3CprepareVideo_IMPU3Ec__Iterator1_t2339890399::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2920 = { sizeof (CloudRecoTrackableEventHandler_t3792508927), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2920[4] = 
{
	CloudRecoTrackableEventHandler_t3792508927::get_offset_of_scanLine_4(),
	CloudRecoTrackableEventHandler_t3792508927::get_offset_of_contentManager_5(),
	CloudRecoTrackableEventHandler_t3792508927::get_offset_of_mTrackableBehaviour_6(),
	CloudRecoTrackableEventHandler_t3792508927::get_offset_of_isTracking_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2921 = { sizeof (ContentManager2_t2980555998), -1, sizeof(ContentManager2_t2980555998_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2921[54] = 
{
	ContentManager2_t2980555998_StaticFields::get_offset_of_baseName_4(),
	ContentManager2_t2980555998::get_offset_of_captureSend_5(),
	ContentManager2_t2980555998::get_offset_of_AugmentationObject_6(),
	ContentManager2_t2980555998::get_offset_of_AnimationsManager_7(),
	ContentManager2_t2980555998::get_offset_of_calendarWidget_8(),
	ContentManager2_t2980555998::get_offset_of_isMarkerShown_9(),
	ContentManager2_t2980555998::get_offset_of_UserGUID_10(),
	ContentManager2_t2980555998::get_offset_of_appName_11(),
	ContentManager2_t2980555998::get_offset_of_markerWidget_12(),
	ContentManager2_t2980555998::get_offset_of_actionWidget_13(),
	ContentManager2_t2980555998::get_offset_of_jeuWidget_14(),
	ContentManager2_t2980555998::get_offset_of_imageWidget_15(),
	ContentManager2_t2980555998::get_offset_of_videoWidget_16(),
	ContentManager2_t2980555998::get_offset_of_audioWidget_17(),
	ContentManager2_t2980555998::get_offset_of_chromaKeyWidget_18(),
	ContentManager2_t2980555998::get_offset_of_videoPlayerPortrait_19(),
	ContentManager2_t2980555998::get_offset_of_videoPlayerPortrait_RAW_20(),
	ContentManager2_t2980555998::get_offset_of_videoPlayerLandscape_21(),
	ContentManager2_t2980555998::get_offset_of_videoPlayerLandscape_RAW_22(),
	ContentManager2_t2980555998::get_offset_of_fullscreenVideoPlayer_23(),
	ContentManager2_t2980555998::get_offset_of_objectWidget_24(),
	ContentManager2_t2980555998::get_offset_of_rawImgPortrait_25(),
	ContentManager2_t2980555998::get_offset_of_rawImgLandscape_26(),
	ContentManager2_t2980555998::get_offset_of_panelBack_27(),
	ContentManager2_t2980555998::get_offset_of_CameraOBJContainer_28(),
	ContentManager2_t2980555998::get_offset_of_FS_OBJ_container_29(),
	ContentManager2_t2980555998::get_offset_of_closeButton_30(),
	ContentManager2_t2980555998::get_offset_of_backButtonBackMarker_31(),
	ContentManager2_t2980555998::get_offset_of_backMarkerON_32(),
	ContentManager2_t2980555998::get_offset_of_backMarkerOFF_33(),
	ContentManager2_t2980555998::get_offset_of_isShowingBackMarker_34(),
	ContentManager2_t2980555998::get_offset_of_sphereCloseUp_35(),
	ContentManager2_t2980555998::get_offset_of_cubeEtalon_36(),
	ContentManager2_t2980555998::get_offset_of_closeMarkerButton_37(),
	ContentManager2_t2980555998::get_offset_of_sliderZoom_38(),
	ContentManager2_t2980555998::get_offset_of_cancelButton_39(),
	ContentManager2_t2980555998::get_offset_of_fullscreen3DObject_40(),
	ContentManager2_t2980555998::get_offset_of_emailPanel_41(),
	ContentManager2_t2980555998::get_offset_of_emailPanel_mail_42(),
	ContentManager2_t2980555998::get_offset_of_emailPanel_text_43(),
	ContentManager2_t2980555998::get_offset_of_emailPanel_button_44(),
	ContentManager2_t2980555998::get_offset_of_screenShotURL_45(),
	ContentManager2_t2980555998::get_offset_of_nodeTex_46(),
	ContentManager2_t2980555998::get_offset_of_mPixelFormat_47(),
	ContentManager2_t2980555998::get_offset_of_mFormatRegistered_48(),
	ContentManager2_t2980555998::get_offset_of_mCloudRecoBehaviour_49(),
	ContentManager2_t2980555998::get_offset_of_mTrackableBehaviour_50(),
	ContentManager2_t2980555998::get_offset_of_mIsShowingMarkerData_51(),
	ContentManager2_t2980555998::get_offset_of_currentMarker_52(),
	ContentManager2_t2980555998::get_offset_of_isTracking3D_53(),
	ContentManager2_t2980555998::get_offset_of_pano_regles_54(),
	ContentManager2_t2980555998::get_offset_of_pano_data_55(),
	ContentManager2_t2980555998::get_offset_of_pano_error_56(),
	ContentManager2_t2980555998::get_offset_of_pano_gagne_57(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2922 = { sizeof (U3Cget_contentU3Ec__Iterator0_t243170899), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2922[12] = 
{
	U3Cget_contentU3Ec__Iterator0_t243170899::get_offset_of_markerUID_0(),
	U3Cget_contentU3Ec__Iterator0_t243170899::get_offset_of_U3CcalendarU3E__1_1(),
	U3Cget_contentU3Ec__Iterator0_t243170899::get_offset_of_U3CurlU3E__2_2(),
	U3Cget_contentU3Ec__Iterator0_t243170899::get_offset_of_U3CwwwU3E__2_3(),
	U3Cget_contentU3Ec__Iterator0_t243170899::get_offset_of_U3CmngU3E__2_4(),
	U3Cget_contentU3Ec__Iterator0_t243170899::get_offset_of_U3ClayerU3E__2_5(),
	U3Cget_contentU3Ec__Iterator0_t243170899::get_offset_of_U3CRtU3E__2_6(),
	U3Cget_contentU3Ec__Iterator0_t243170899::get_offset_of_U3Cindex3DObjU3E__2_7(),
	U3Cget_contentU3Ec__Iterator0_t243170899::get_offset_of_U24this_8(),
	U3Cget_contentU3Ec__Iterator0_t243170899::get_offset_of_U24current_9(),
	U3Cget_contentU3Ec__Iterator0_t243170899::get_offset_of_U24disposing_10(),
	U3Cget_contentU3Ec__Iterator0_t243170899::get_offset_of_U24PC_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2923 = { sizeof (U3CIMPL_sendCaptureU3Ec__Iterator1_t421012197), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2923[10] = 
{
	U3CIMPL_sendCaptureU3Ec__Iterator1_t421012197::get_offset_of_U3CrendererBackU3E__1_0(),
	U3CIMPL_sendCaptureU3Ec__Iterator1_t421012197::get_offset_of_U3CtexU3E__1_1(),
	U3CIMPL_sendCaptureU3Ec__Iterator1_t421012197::get_offset_of_U3Ctex2U3E__1_2(),
	U3CIMPL_sendCaptureU3Ec__Iterator1_t421012197::get_offset_of_U3CbytesU3E__1_3(),
	U3CIMPL_sendCaptureU3Ec__Iterator1_t421012197::get_offset_of_U3CformU3E__1_4(),
	U3CIMPL_sendCaptureU3Ec__Iterator1_t421012197::get_offset_of_U3CwU3E__1_5(),
	U3CIMPL_sendCaptureU3Ec__Iterator1_t421012197::get_offset_of_U24this_6(),
	U3CIMPL_sendCaptureU3Ec__Iterator1_t421012197::get_offset_of_U24current_7(),
	U3CIMPL_sendCaptureU3Ec__Iterator1_t421012197::get_offset_of_U24disposing_8(),
	U3CIMPL_sendCaptureU3Ec__Iterator1_t421012197::get_offset_of_U24PC_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2924 = { sizeof (CreditLoader_t1693052478), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2924[16] = 
{
	CreditLoader_t1693052478::get_offset_of_contentManager_4(),
	CreditLoader_t1693052478::get_offset_of_titre_5(),
	CreditLoader_t1693052478::get_offset_of_textPro_6(),
	CreditLoader_t1693052478::get_offset_of_mail_7(),
	CreditLoader_t1693052478::get_offset_of_tel_8(),
	CreditLoader_t1693052478::get_offset_of_icone_mail_9(),
	CreditLoader_t1693052478::get_offset_of_icone_phone_10(),
	CreditLoader_t1693052478::get_offset_of_scrollbarCredit_11(),
	CreditLoader_t1693052478::get_offset_of_logoAlpha_12(),
	CreditLoader_t1693052478::get_offset_of_channelLogo_13(),
	CreditLoader_t1693052478::get_offset_of_closeButton_14(),
	CreditLoader_t1693052478::get_offset_of_button1_15(),
	CreditLoader_t1693052478::get_offset_of_button2_16(),
	CreditLoader_t1693052478::get_offset_of_button3_17(),
	CreditLoader_t1693052478::get_offset_of_button4_18(),
	CreditLoader_t1693052478::get_offset_of_etape1_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2925 = { sizeof (U3CgetCreditU3Ec__Iterator0_t2852871274), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2925[13] = 
{
	U3CgetCreditU3Ec__Iterator0_t2852871274::get_offset_of_U3CurlU3E__0_0(),
	U3CgetCreditU3Ec__Iterator0_t2852871274::get_offset_of_U3CwwwU3E__0_1(),
	U3CgetCreditU3Ec__Iterator0_t2852871274::get_offset_of_U3CNbaseU3E__0_2(),
	U3CgetCreditU3Ec__Iterator0_t2852871274::get_offset_of_U3CcorpsTextU3E__0_3(),
	U3CgetCreditU3Ec__Iterator0_t2852871274::get_offset_of_U3CwwwimageU3E__0_4(),
	U3CgetCreditU3Ec__Iterator0_t2852871274::get_offset_of_U3CtextureU3E__0_5(),
	U3CgetCreditU3Ec__Iterator0_t2852871274::get_offset_of_U3CrecU3E__0_6(),
	U3CgetCreditU3Ec__Iterator0_t2852871274::get_offset_of_U3CspriteToUseU3E__0_7(),
	U3CgetCreditU3Ec__Iterator0_t2852871274::get_offset_of_U3CnewColU3E__0_8(),
	U3CgetCreditU3Ec__Iterator0_t2852871274::get_offset_of_U24this_9(),
	U3CgetCreditU3Ec__Iterator0_t2852871274::get_offset_of_U24current_10(),
	U3CgetCreditU3Ec__Iterator0_t2852871274::get_offset_of_U24disposing_11(),
	U3CgetCreditU3Ec__Iterator0_t2852871274::get_offset_of_U24PC_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2926 = { sizeof (DeferredFogEffect_t753821049), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2926[5] = 
{
	DeferredFogEffect_t753821049::get_offset_of_deferredFog_4(),
	DeferredFogEffect_t753821049::get_offset_of_fogMaterial_5(),
	DeferredFogEffect_t753821049::get_offset_of_deferredCamera_6(),
	DeferredFogEffect_t753821049::get_offset_of_frustumCorners_7(),
	DeferredFogEffect_t753821049::get_offset_of_vectorArray_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2927 = { sizeof (EmissiveOscillator_t3402490064), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2927[2] = 
{
	EmissiveOscillator_t3402490064::get_offset_of_emissiveRenderer_4(),
	EmissiveOscillator_t3402490064::get_offset_of_emissiveMaterial_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2928 = { sizeof (ExtensionMethods_t729511191), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2929 = { sizeof (Face3DManager_t2951744723), -1, sizeof(Face3DManager_t2951744723_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2929[5] = 
{
	Face3DManager_t2951744723_StaticFields::get_offset_of__Instance_0(),
	Face3DManager_t2951744723::get_offset_of_isShowingContent_1(),
	Face3DManager_t2951744723::get_offset_of_listFaces_2(),
	Face3DManager_t2951744723::get_offset_of_isFullscreen_3(),
	Face3DManager_t2951744723::get_offset_of_selected_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2930 = { sizeof (FocusManager_t1358311062), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2931 = { sizeof (FullscreenVideoPlayer_t1619346304), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2931[20] = 
{
	FullscreenVideoPlayer_t1619346304::get_offset_of_landScapeObject_4(),
	FullscreenVideoPlayer_t1619346304::get_offset_of_portraitObject_5(),
	FullscreenVideoPlayer_t1619346304::get_offset_of_videoPlayer_6(),
	FullscreenVideoPlayer_t1619346304::get_offset_of_audioSrc_7(),
	FullscreenVideoPlayer_t1619346304::get_offset_of_currentFrame_8(),
	FullscreenVideoPlayer_t1619346304::get_offset_of_landscape_Raw_9(),
	FullscreenVideoPlayer_t1619346304::get_offset_of_portrait_Raw_10(),
	FullscreenVideoPlayer_t1619346304::get_offset_of_sliderLandscape_11(),
	FullscreenVideoPlayer_t1619346304::get_offset_of_sliderPortrait_12(),
	FullscreenVideoPlayer_t1619346304::get_offset_of_seekBarLandscape_13(),
	FullscreenVideoPlayer_t1619346304::get_offset_of_seekBarPortrait_14(),
	FullscreenVideoPlayer_t1619346304::get_offset_of_playButtonLandscape_15(),
	FullscreenVideoPlayer_t1619346304::get_offset_of_playButtonPortrait_16(),
	FullscreenVideoPlayer_t1619346304::get_offset_of_pauseButtonLandscape_17(),
	FullscreenVideoPlayer_t1619346304::get_offset_of_pauseButtonPortrait_18(),
	FullscreenVideoPlayer_t1619346304::get_offset_of_panelLandscape_19(),
	FullscreenVideoPlayer_t1619346304::get_offset_of_panelPortrait_20(),
	FullscreenVideoPlayer_t1619346304::get_offset_of_contentManager_21(),
	FullscreenVideoPlayer_t1619346304::get_offset_of_url_22(),
	FullscreenVideoPlayer_t1619346304::get_offset_of_isLandscape_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2932 = { sizeof (U3CplayVideoU3Ec__Iterator0_t3060609596), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2932[4] = 
{
	U3CplayVideoU3Ec__Iterator0_t3060609596::get_offset_of_U24this_0(),
	U3CplayVideoU3Ec__Iterator0_t3060609596::get_offset_of_U24current_1(),
	U3CplayVideoU3Ec__Iterator0_t3060609596::get_offset_of_U24disposing_2(),
	U3CplayVideoU3Ec__Iterator0_t3060609596::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2933 = { sizeof (GPUInstancingTest_t3692336574), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2933[3] = 
{
	GPUInstancingTest_t3692336574::get_offset_of_prefab_4(),
	GPUInstancingTest_t3692336574::get_offset_of_instances_5(),
	GPUInstancingTest_t3692336574::get_offset_of_radius_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2934 = { sizeof (GyroManager_t2156710008), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2934[3] = 
{
	GyroManager_t2156710008::get_offset_of_quads_4(),
	GyroManager_t2156710008::get_offset_of_labels_5(),
	GyroManager_t2156710008::get_offset_of_cam_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2935 = { sizeof (HelpManager_t1803709799), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2935[25] = 
{
	HelpManager_t1803709799::get_offset_of_panel1_4(),
	HelpManager_t1803709799::get_offset_of_panel2_5(),
	HelpManager_t1803709799::get_offset_of_panel3_6(),
	HelpManager_t1803709799::get_offset_of_panel4_7(),
	HelpManager_t1803709799::get_offset_of_help1_text_8(),
	HelpManager_t1803709799::get_offset_of_help2_text_9(),
	HelpManager_t1803709799::get_offset_of_help3_text_10(),
	HelpManager_t1803709799::get_offset_of_help4_text_11(),
	HelpManager_t1803709799::get_offset_of_help5_text_12(),
	HelpManager_t1803709799::get_offset_of_help7_text_13(),
	HelpManager_t1803709799::get_offset_of_help9_text_14(),
	HelpManager_t1803709799::get_offset_of_help1_15(),
	HelpManager_t1803709799::get_offset_of_help2_16(),
	HelpManager_t1803709799::get_offset_of_help3_17(),
	HelpManager_t1803709799::get_offset_of_help4_18(),
	HelpManager_t1803709799::get_offset_of_help5_19(),
	HelpManager_t1803709799::get_offset_of_help6_20(),
	HelpManager_t1803709799::get_offset_of_help7_21(),
	HelpManager_t1803709799::get_offset_of_help8_22(),
	HelpManager_t1803709799::get_offset_of_help9_23(),
	HelpManager_t1803709799::get_offset_of_help10_24(),
	HelpManager_t1803709799::get_offset_of_firstPressPos_25(),
	HelpManager_t1803709799::get_offset_of_secondPressPos_26(),
	HelpManager_t1803709799::get_offset_of_currentSwipe_27(),
	HelpManager_t1803709799::get_offset_of_currentPage_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2936 = { sizeof (ImageContent_t959306442), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2936[11] = 
{
	ImageContent_t959306442::get_offset_of_urlImage_4(),
	ImageContent_t959306442::get_offset_of_plane_5(),
	ImageContent_t959306442::get_offset_of_canClick_6(),
	ImageContent_t959306442::get_offset_of_contentManager_7(),
	ImageContent_t959306442::get_offset_of_hit_8(),
	ImageContent_t959306442::get_offset_of_ray_9(),
	ImageContent_t959306442::get_offset_of_hitAllTemp_10(),
	ImageContent_t959306442::get_offset_of_collider_11(),
	ImageContent_t959306442::get_offset_of_rawImage_12(),
	ImageContent_t959306442::get_offset_of_rawImageLandscape_13(),
	ImageContent_t959306442::get_offset_of_panelBack_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2937 = { sizeof (U3CloadImageU3Ec__Iterator0_t2022358625), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2937[13] = 
{
	U3CloadImageU3Ec__Iterator0_t2022358625::get_offset_of_url_0(),
	U3CloadImageU3Ec__Iterator0_t2022358625::get_offset_of_U3CwwwU3E__0_1(),
	U3CloadImageU3Ec__Iterator0_t2022358625::get_offset_of_U3CrdU3E__0_2(),
	U3CloadImageU3Ec__Iterator0_t2022358625::get_offset_of_posX_3(),
	U3CloadImageU3Ec__Iterator0_t2022358625::get_offset_of_posY_4(),
	U3CloadImageU3Ec__Iterator0_t2022358625::get_offset_of_layer_5(),
	U3CloadImageU3Ec__Iterator0_t2022358625::get_offset_of_sizeX_6(),
	U3CloadImageU3Ec__Iterator0_t2022358625::get_offset_of_sizeY_7(),
	U3CloadImageU3Ec__Iterator0_t2022358625::get_offset_of_rotation_8(),
	U3CloadImageU3Ec__Iterator0_t2022358625::get_offset_of_U24this_9(),
	U3CloadImageU3Ec__Iterator0_t2022358625::get_offset_of_U24current_10(),
	U3CloadImageU3Ec__Iterator0_t2022358625::get_offset_of_U24disposing_11(),
	U3CloadImageU3Ec__Iterator0_t2022358625::get_offset_of_U24PC_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2938 = { sizeof (ImageWidget_t3173080225), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2938[4] = 
{
	ImageWidget_t3173080225::get_offset_of_imageLandscape_4(),
	ImageWidget_t3173080225::get_offset_of_imagePortrait_5(),
	ImageWidget_t3173080225::get_offset_of_panelBack_6(),
	ImageWidget_t3173080225::get_offset_of_contentManager_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2939 = { sizeof (PanoError_t3840804466), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2939[2] = 
{
	PanoError_t3840804466::get_offset_of_errrorMsg_4(),
	PanoError_t3840804466::get_offset_of_ui_errorMsg_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2940 = { sizeof (PanoForm_t499938582), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2940[47] = 
{
	PanoForm_t499938582::get_offset_of_isGagne_4(),
	PanoForm_t499938582::get_offset_of_display_email_5(),
	PanoForm_t499938582::get_offset_of_display_nom_6(),
	PanoForm_t499938582::get_offset_of_display_prenom_7(),
	PanoForm_t499938582::get_offset_of_display_societe_8(),
	PanoForm_t499938582::get_offset_of_display_telephone_9(),
	PanoForm_t499938582::get_offset_of_display_datenaissance_10(),
	PanoForm_t499938582::get_offset_of_display_extra_11(),
	PanoForm_t499938582::get_offset_of_display_email_oblig_12(),
	PanoForm_t499938582::get_offset_of_display_nom_oblig_13(),
	PanoForm_t499938582::get_offset_of_display_prenom_oblig_14(),
	PanoForm_t499938582::get_offset_of_display_societe_oblig_15(),
	PanoForm_t499938582::get_offset_of_display_telephone_oblig_16(),
	PanoForm_t499938582::get_offset_of_display_datenaissance_oblig_17(),
	PanoForm_t499938582::get_offset_of_display_extra_oblig_18(),
	PanoForm_t499938582::get_offset_of_go_email_19(),
	PanoForm_t499938582::get_offset_of_go_nom_20(),
	PanoForm_t499938582::get_offset_of_go_prenom_21(),
	PanoForm_t499938582::get_offset_of_go_societe_22(),
	PanoForm_t499938582::get_offset_of_go_telephone_datenaissance_23(),
	PanoForm_t499938582::get_offset_of_go_telephone_24(),
	PanoForm_t499938582::get_offset_of_go_datenaissance_25(),
	PanoForm_t499938582::get_offset_of_go_extra_26(),
	PanoForm_t499938582::get_offset_of_if_email_27(),
	PanoForm_t499938582::get_offset_of_if_nom_28(),
	PanoForm_t499938582::get_offset_of_if_prenom_29(),
	PanoForm_t499938582::get_offset_of_if_societe_30(),
	PanoForm_t499938582::get_offset_of_if_telephone_31(),
	PanoForm_t499938582::get_offset_of_if_JJ_32(),
	PanoForm_t499938582::get_offset_of_if_MM_33(),
	PanoForm_t499938582::get_offset_of_if_AAAA_34(),
	PanoForm_t499938582::get_offset_of_if_extra_35(),
	PanoForm_t499938582::get_offset_of_if_email_oblig_36(),
	PanoForm_t499938582::get_offset_of_if_nom_oblig_37(),
	PanoForm_t499938582::get_offset_of_if_prenom_oblig_38(),
	PanoForm_t499938582::get_offset_of_if_societe_oblig_39(),
	PanoForm_t499938582::get_offset_of_if_telephone_oblig_40(),
	PanoForm_t499938582::get_offset_of_if_dateNaissance_oblig_41(),
	PanoForm_t499938582::get_offset_of_extraLabel_42(),
	PanoForm_t499938582::get_offset_of_UserGUID_43(),
	PanoForm_t499938582::get_offset_of_markerUID_44(),
	PanoForm_t499938582::get_offset_of_appName_45(),
	PanoForm_t499938582::get_offset_of_token_46(),
	PanoForm_t499938582::get_offset_of_link_of_reward_47(),
	PanoForm_t499938582::get_offset_of_extraStr_48(),
	PanoForm_t499938582::get_offset_of_panoError_49(),
	PanoForm_t499938582::get_offset_of_panoGagne_50(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2941 = { sizeof (U3CsendEmail_IMPU3Ec__Iterator0_t3798648854), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2941[9] = 
{
	U3CsendEmail_IMPU3Ec__Iterator0_t3798648854::get_offset_of_U3CurlU3E__0_0(),
	U3CsendEmail_IMPU3Ec__Iterator0_t3798648854::get_offset_of_U3CwwwU3E__0_1(),
	U3CsendEmail_IMPU3Ec__Iterator0_t3798648854::get_offset_of_U3CNbaseU3E__0_2(),
	U3CsendEmail_IMPU3Ec__Iterator0_t3798648854::get_offset_of_U3CstatusU3E__0_3(),
	U3CsendEmail_IMPU3Ec__Iterator0_t3798648854::get_offset_of_U3CtextDisplayU3E__0_4(),
	U3CsendEmail_IMPU3Ec__Iterator0_t3798648854::get_offset_of_U24this_5(),
	U3CsendEmail_IMPU3Ec__Iterator0_t3798648854::get_offset_of_U24current_6(),
	U3CsendEmail_IMPU3Ec__Iterator0_t3798648854::get_offset_of_U24disposing_7(),
	U3CsendEmail_IMPU3Ec__Iterator0_t3798648854::get_offset_of_U24PC_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2942 = { sizeof (PanoGagne_t1959658388), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2942[3] = 
{
	PanoGagne_t1959658388::get_offset_of_msg_4(),
	PanoGagne_t1959658388::get_offset_of_URLtogift_5(),
	PanoGagne_t1959658388::get_offset_of_ui_msg_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2943 = { sizeof (PanoRegles_t2340488907), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2943[4] = 
{
	PanoRegles_t2340488907::get_offset_of_URL_Regles_4(),
	PanoRegles_t2340488907::get_offset_of_accepte_5(),
	PanoRegles_t2340488907::get_offset_of_nextPanel_6(),
	PanoRegles_t2340488907::get_offset_of_isGagne_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2944 = { sizeof (LangManager_t3681335332), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2944[69] = 
{
	LangManager_t3681335332::get_offset_of_CTN1_4(),
	LangManager_t3681335332::get_offset_of_CTN2_5(),
	LangManager_t3681335332::get_offset_of_CTN3_6(),
	LangManager_t3681335332::get_offset_of_CTN4_7(),
	LangManager_t3681335332::get_offset_of_CTN5_8(),
	LangManager_t3681335332::get_offset_of_SV_CTN1_9(),
	LangManager_t3681335332::get_offset_of_SV_CTN2_10(),
	LangManager_t3681335332::get_offset_of_SV_CTN3_11(),
	LangManager_t3681335332::get_offset_of_SV_CTN4_12(),
	LangManager_t3681335332::get_offset_of_SV_CTN5_13(),
	LangManager_t3681335332::get_offset_of_imageNA_14(),
	LangManager_t3681335332::get_offset_of_CTN1_ca_15(),
	LangManager_t3681335332::get_offset_of_CTN1_do_16(),
	LangManager_t3681335332::get_offset_of_CTN1_tt_17(),
	LangManager_t3681335332::get_offset_of_CTN1_us_18(),
	LangManager_t3681335332::get_offset_of_CTN1_sv_19(),
	LangManager_t3681335332::get_offset_of_CTN1_gt_20(),
	LangManager_t3681335332::get_offset_of_CTN1_mx_21(),
	LangManager_t3681335332::get_offset_of_CTN1_ni_22(),
	LangManager_t3681335332::get_offset_of_CTN1_pa_23(),
	LangManager_t3681335332::get_offset_of_CTN2_ar_24(),
	LangManager_t3681335332::get_offset_of_CTN2_bo_25(),
	LangManager_t3681335332::get_offset_of_CTN2_br_26(),
	LangManager_t3681335332::get_offset_of_CTN2_cl_27(),
	LangManager_t3681335332::get_offset_of_CTN2_co_28(),
	LangManager_t3681335332::get_offset_of_CTN2_ec_29(),
	LangManager_t3681335332::get_offset_of_CTN2_py_30(),
	LangManager_t3681335332::get_offset_of_CTN2_uy_31(),
	LangManager_t3681335332::get_offset_of_CTN3_gr_32(),
	LangManager_t3681335332::get_offset_of_CTN3_tr_33(),
	LangManager_t3681335332::get_offset_of_CTN3_lt_34(),
	LangManager_t3681335332::get_offset_of_CTN3_pl_35(),
	LangManager_t3681335332::get_offset_of_CTN3_ru_36(),
	LangManager_t3681335332::get_offset_of_CTN3_dk_37(),
	LangManager_t3681335332::get_offset_of_CTN3_it_38(),
	LangManager_t3681335332::get_offset_of_CTN3_ch_39(),
	LangManager_t3681335332::get_offset_of_CTN3_no_40(),
	LangManager_t3681335332::get_offset_of_CTN3_se_41(),
	LangManager_t3681335332::get_offset_of_CTN3_at_42(),
	LangManager_t3681335332::get_offset_of_CTN3_fr_43(),
	LangManager_t3681335332::get_offset_of_CTN3_de_44(),
	LangManager_t3681335332::get_offset_of_CTN3_is_45(),
	LangManager_t3681335332::get_offset_of_CTN3_ie_46(),
	LangManager_t3681335332::get_offset_of_CTN3_gb_47(),
	LangManager_t3681335332::get_offset_of_CTN3_es_48(),
	LangManager_t3681335332::get_offset_of_CTN3_be_49(),
	LangManager_t3681335332::get_offset_of_CTN3_nl_50(),
	LangManager_t3681335332::get_offset_of_CTN4_cn_51(),
	LangManager_t3681335332::get_offset_of_CTN4_in_52(),
	LangManager_t3681335332::get_offset_of_CTN4_jp_53(),
	LangManager_t3681335332::get_offset_of_CTN4_kr_54(),
	LangManager_t3681335332::get_offset_of_CTN4_pk_55(),
	LangManager_t3681335332::get_offset_of_CTN4_sg_56(),
	LangManager_t3681335332::get_offset_of_CTN4_sr_57(),
	LangManager_t3681335332::get_offset_of_CTN5_au_58(),
	LangManager_t3681335332::get_offset_of_CTN5_nz_59(),
	LangManager_t3681335332::get_offset_of_button1_60(),
	LangManager_t3681335332::get_offset_of_button2_61(),
	LangManager_t3681335332::get_offset_of_button3_62(),
	LangManager_t3681335332::get_offset_of_panelConfirmation_63(),
	LangManager_t3681335332::get_offset_of_confirmationText_64(),
	LangManager_t3681335332::get_offset_of_confirmationFlag_65(),
	LangManager_t3681335332::get_offset_of_buttonClose_66(),
	LangManager_t3681335332::get_offset_of_selectedLanguageID_67(),
	LangManager_t3681335332::get_offset_of_allCTN1_flags_68(),
	LangManager_t3681335332::get_offset_of_allCTN2_flags_69(),
	LangManager_t3681335332::get_offset_of_allCTN3_flags_70(),
	LangManager_t3681335332::get_offset_of_allCTN4_flags_71(),
	LangManager_t3681335332::get_offset_of_allCTN5_flags_72(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2945 = { sizeof (U3CcheckWWWlanguageU3Ec__Iterator0_t934303629), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2945[13] = 
{
	U3CcheckWWWlanguageU3Ec__Iterator0_t934303629::get_offset_of_cnty_0(),
	U3CcheckWWWlanguageU3Ec__Iterator0_t934303629::get_offset_of_U3Clg1U3E__0_1(),
	U3CcheckWWWlanguageU3Ec__Iterator0_t934303629::get_offset_of_U3Clg2U3E__0_2(),
	U3CcheckWWWlanguageU3Ec__Iterator0_t934303629::get_offset_of_U3Clg3U3E__0_3(),
	U3CcheckWWWlanguageU3Ec__Iterator0_t934303629::get_offset_of_U3CvalKeyU3E__0_4(),
	U3CcheckWWWlanguageU3Ec__Iterator0_t934303629::get_offset_of_U3CurlU3E__0_5(),
	U3CcheckWWWlanguageU3Ec__Iterator0_t934303629::get_offset_of_U3CwwwU3E__0_6(),
	U3CcheckWWWlanguageU3Ec__Iterator0_t934303629::get_offset_of_U3CtmU3E__0_7(),
	U3CcheckWWWlanguageU3Ec__Iterator0_t934303629::get_offset_of_U3CNbaseU3E__0_8(),
	U3CcheckWWWlanguageU3Ec__Iterator0_t934303629::get_offset_of_U24this_9(),
	U3CcheckWWWlanguageU3Ec__Iterator0_t934303629::get_offset_of_U24current_10(),
	U3CcheckWWWlanguageU3Ec__Iterator0_t934303629::get_offset_of_U24disposing_11(),
	U3CcheckWWWlanguageU3Ec__Iterator0_t934303629::get_offset_of_U24PC_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2946 = { sizeof (U3CGetLanguageJSONU3Ec__Iterator1_t1445961214), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2946[7] = 
{
	U3CGetLanguageJSONU3Ec__Iterator1_t1445961214::get_offset_of_U3CurlU3E__0_0(),
	U3CGetLanguageJSONU3Ec__Iterator1_t1445961214::get_offset_of_U3CwwwU3E__0_1(),
	U3CGetLanguageJSONU3Ec__Iterator1_t1445961214::get_offset_of_U3CmanagerU3E__0_2(),
	U3CGetLanguageJSONU3Ec__Iterator1_t1445961214::get_offset_of_U24this_3(),
	U3CGetLanguageJSONU3Ec__Iterator1_t1445961214::get_offset_of_U24current_4(),
	U3CGetLanguageJSONU3Ec__Iterator1_t1445961214::get_offset_of_U24disposing_5(),
	U3CGetLanguageJSONU3Ec__Iterator1_t1445961214::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2947 = { sizeof (LanguageIDHolder_t3554124395), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2947[2] = 
{
	LanguageIDHolder_t3554124395::get_offset_of_codeLang_4(),
	LanguageIDHolder_t3554124395::get_offset_of_language_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2948 = { sizeof (JSONNodeType_t2191432521)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2948[8] = 
{
	JSONNodeType_t2191432521::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2949 = { sizeof (JSONNode_t2946056997), -1, sizeof(JSONNode_t2946056997_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2949[1] = 
{
	JSONNode_t2946056997_StaticFields::get_offset_of_m_EscapeBuilder_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2950 = { sizeof (U3CU3Ec__Iterator0_t2360448098), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2950[3] = 
{
	U3CU3Ec__Iterator0_t2360448098::get_offset_of_U24current_0(),
	U3CU3Ec__Iterator0_t2360448098::get_offset_of_U24disposing_1(),
	U3CU3Ec__Iterator0_t2360448098::get_offset_of_U24PC_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2951 = { sizeof (U3CU3Ec__Iterator1_t2360382562), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2951[3] = 
{
	U3CU3Ec__Iterator1_t2360382562::get_offset_of_U24current_0(),
	U3CU3Ec__Iterator1_t2360382562::get_offset_of_U24disposing_1(),
	U3CU3Ec__Iterator1_t2360382562::get_offset_of_U24PC_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2952 = { sizeof (U3CU3Ec__Iterator2_t2360579170), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2952[8] = 
{
	U3CU3Ec__Iterator2_t2360579170::get_offset_of_U24locvar0_0(),
	U3CU3Ec__Iterator2_t2360579170::get_offset_of_U3CCU3E__1_1(),
	U3CU3Ec__Iterator2_t2360579170::get_offset_of_U24locvar1_2(),
	U3CU3Ec__Iterator2_t2360579170::get_offset_of_U3CDU3E__2_3(),
	U3CU3Ec__Iterator2_t2360579170::get_offset_of_U24this_4(),
	U3CU3Ec__Iterator2_t2360579170::get_offset_of_U24current_5(),
	U3CU3Ec__Iterator2_t2360579170::get_offset_of_U24disposing_6(),
	U3CU3Ec__Iterator2_t2360579170::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2953 = { sizeof (JSONArray_t2340361630), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2953[1] = 
{
	JSONArray_t2340361630::get_offset_of_m_List_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2954 = { sizeof (U3CU3Ec__Iterator0_t2066799033), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2954[6] = 
{
	U3CU3Ec__Iterator0_t2066799033::get_offset_of_U24locvar0_0(),
	U3CU3Ec__Iterator0_t2066799033::get_offset_of_U3CNU3E__1_1(),
	U3CU3Ec__Iterator0_t2066799033::get_offset_of_U24this_2(),
	U3CU3Ec__Iterator0_t2066799033::get_offset_of_U24current_3(),
	U3CU3Ec__Iterator0_t2066799033::get_offset_of_U24disposing_4(),
	U3CU3Ec__Iterator0_t2066799033::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2955 = { sizeof (U3CGetEnumeratorU3Ec__Iterator1_t1509986145), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2955[6] = 
{
	U3CGetEnumeratorU3Ec__Iterator1_t1509986145::get_offset_of_U24locvar0_0(),
	U3CGetEnumeratorU3Ec__Iterator1_t1509986145::get_offset_of_U3CNU3E__1_1(),
	U3CGetEnumeratorU3Ec__Iterator1_t1509986145::get_offset_of_U24this_2(),
	U3CGetEnumeratorU3Ec__Iterator1_t1509986145::get_offset_of_U24current_3(),
	U3CGetEnumeratorU3Ec__Iterator1_t1509986145::get_offset_of_U24disposing_4(),
	U3CGetEnumeratorU3Ec__Iterator1_t1509986145::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2956 = { sizeof (JSONObject_t4158403488), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2956[2] = 
{
	JSONObject_t4158403488::get_offset_of_m_Dict_1(),
	JSONObject_t4158403488::get_offset_of_jsonKey_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2957 = { sizeof (U3CRemoveU3Ec__AnonStorey3_t2674960018), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2957[1] = 
{
	U3CRemoveU3Ec__AnonStorey3_t2674960018::get_offset_of_aNode_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2958 = { sizeof (U3CU3Ec__Iterator0_t2553288236), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2958[6] = 
{
	U3CU3Ec__Iterator0_t2553288236::get_offset_of_U24locvar0_0(),
	U3CU3Ec__Iterator0_t2553288236::get_offset_of_U3CNU3E__1_1(),
	U3CU3Ec__Iterator0_t2553288236::get_offset_of_U24this_2(),
	U3CU3Ec__Iterator0_t2553288236::get_offset_of_U24current_3(),
	U3CU3Ec__Iterator0_t2553288236::get_offset_of_U24disposing_4(),
	U3CU3Ec__Iterator0_t2553288236::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2959 = { sizeof (U3CGetEnumeratorU3Ec__Iterator1_t1727131513), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2959[6] = 
{
	U3CGetEnumeratorU3Ec__Iterator1_t1727131513::get_offset_of_U24locvar0_0(),
	U3CGetEnumeratorU3Ec__Iterator1_t1727131513::get_offset_of_U3CNU3E__1_1(),
	U3CGetEnumeratorU3Ec__Iterator1_t1727131513::get_offset_of_U24this_2(),
	U3CGetEnumeratorU3Ec__Iterator1_t1727131513::get_offset_of_U24current_3(),
	U3CGetEnumeratorU3Ec__Iterator1_t1727131513::get_offset_of_U24disposing_4(),
	U3CGetEnumeratorU3Ec__Iterator1_t1727131513::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2960 = { sizeof (U3CU3Ec__Iterator2_t2935625260), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2960[6] = 
{
	U3CU3Ec__Iterator2_t2935625260::get_offset_of_U24locvar0_0(),
	U3CU3Ec__Iterator2_t2935625260::get_offset_of_U3CkeyU3E__1_1(),
	U3CU3Ec__Iterator2_t2935625260::get_offset_of_U24this_2(),
	U3CU3Ec__Iterator2_t2935625260::get_offset_of_U24current_3(),
	U3CU3Ec__Iterator2_t2935625260::get_offset_of_U24disposing_4(),
	U3CU3Ec__Iterator2_t2935625260::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2961 = { sizeof (JSONString_t3803360443), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2961[1] = 
{
	JSONString_t3803360443::get_offset_of_m_Data_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2962 = { sizeof (JSONNumber_t4005729108), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2962[1] = 
{
	JSONNumber_t4005729108::get_offset_of_m_Data_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2963 = { sizeof (JSONBool_t130112664), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2963[1] = 
{
	JSONBool_t130112664::get_offset_of_m_Data_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2964 = { sizeof (JSONNull_t1736727710), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2965 = { sizeof (JSONLazyCreator_t3621052039), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2965[2] = 
{
	JSONLazyCreator_t3621052039::get_offset_of_m_Node_1(),
	JSONLazyCreator_t3621052039::get_offset_of_m_Key_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2966 = { sizeof (JSON_t1924642173), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2967 = { sizeof (MainMenuManager_t3185368703), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2968 = { sizeof (MarkerContent_t762212139), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2968[6] = 
{
	MarkerContent_t762212139::get_offset_of_urlImage_4(),
	MarkerContent_t762212139::get_offset_of_plane_5(),
	MarkerContent_t762212139::get_offset_of_afficheMarker_6(),
	MarkerContent_t762212139::get_offset_of_backButtonBackMarker_7(),
	MarkerContent_t762212139::get_offset_of_backMarkerON_8(),
	MarkerContent_t762212139::get_offset_of_backMarkerOFF_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2969 = { sizeof (U3CloadImageU3Ec__Iterator0_t936851250), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2969[17] = 
{
	U3CloadImageU3Ec__Iterator0_t936851250::get_offset_of_url_low_0(),
	U3CloadImageU3Ec__Iterator0_t936851250::get_offset_of_U3CwwwLowU3E__0_1(),
	U3CloadImageU3Ec__Iterator0_t936851250::get_offset_of_U3CrdU3E__0_2(),
	U3CloadImageU3Ec__Iterator0_t936851250::get_offset_of_posX_3(),
	U3CloadImageU3Ec__Iterator0_t936851250::get_offset_of_posY_4(),
	U3CloadImageU3Ec__Iterator0_t936851250::get_offset_of_layer_5(),
	U3CloadImageU3Ec__Iterator0_t936851250::get_offset_of_sizeX_6(),
	U3CloadImageU3Ec__Iterator0_t936851250::get_offset_of_sizeY_7(),
	U3CloadImageU3Ec__Iterator0_t936851250::get_offset_of_rotation_8(),
	U3CloadImageU3Ec__Iterator0_t936851250::get_offset_of_url_thumb_9(),
	U3CloadImageU3Ec__Iterator0_t936851250::get_offset_of_U3CwwwThumbU3E__0_10(),
	U3CloadImageU3Ec__Iterator0_t936851250::get_offset_of_url_11(),
	U3CloadImageU3Ec__Iterator0_t936851250::get_offset_of_U3CwwwU3E__0_12(),
	U3CloadImageU3Ec__Iterator0_t936851250::get_offset_of_U24this_13(),
	U3CloadImageU3Ec__Iterator0_t936851250::get_offset_of_U24current_14(),
	U3CloadImageU3Ec__Iterator0_t936851250::get_offset_of_U24disposing_15(),
	U3CloadImageU3Ec__Iterator0_t936851250::get_offset_of_U24PC_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2970 = { sizeof (MarkerData_t1252592219), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2970[9] = 
{
	MarkerData_t1252592219::get_offset_of_visible_0(),
	MarkerData_t1252592219::get_offset_of_id_1(),
	MarkerData_t1252592219::get_offset_of_target_url_2(),
	MarkerData_t1252592219::get_offset_of_target_thumb_url_3(),
	MarkerData_t1252592219::get_offset_of_target_low_lod_url_4(),
	MarkerData_t1252592219::get_offset_of_width_5(),
	MarkerData_t1252592219::get_offset_of_height_6(),
	MarkerData_t1252592219::get_offset_of_layerCount_7(),
	MarkerData_t1252592219::get_offset_of_layers_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2971 = { sizeof (MarkerLayer_t3962172947), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2971[51] = 
{
	MarkerLayer_t3962172947::get_offset_of_enable_0(),
	MarkerLayer_t3962172947::get_offset_of_posX_1(),
	MarkerLayer_t3962172947::get_offset_of_posY_2(),
	MarkerLayer_t3962172947::get_offset_of_rotation_3(),
	MarkerLayer_t3962172947::get_offset_of_inclinaison_4(),
	MarkerLayer_t3962172947::get_offset_of_width_5(),
	MarkerLayer_t3962172947::get_offset_of_height_6(),
	MarkerLayer_t3962172947::get_offset_of_type_7(),
	MarkerLayer_t3962172947::get_offset_of_image_8(),
	MarkerLayer_t3962172947::get_offset_of_isChromakey_9(),
	MarkerLayer_t3962172947::get_offset_of_zip1_10(),
	MarkerLayer_t3962172947::get_offset_of_zip2_11(),
	MarkerLayer_t3962172947::get_offset_of_GUID_12(),
	MarkerLayer_t3962172947::get_offset_of_videoWidth_13(),
	MarkerLayer_t3962172947::get_offset_of_videoHeight_14(),
	MarkerLayer_t3962172947::get_offset_of_url_15(),
	MarkerLayer_t3962172947::get_offset_of_action_16(),
	MarkerLayer_t3962172947::get_offset_of_autoplay_17(),
	MarkerLayer_t3962172947::get_offset_of_jeu_CodeErreur_18(),
	MarkerLayer_t3962172947::get_offset_of_jeu_strErreur_19(),
	MarkerLayer_t3962172947::get_offset_of_jeu_imageAGratter_20(),
	MarkerLayer_t3962172947::get_offset_of_jeu_rejouer_21(),
	MarkerLayer_t3962172947::get_offset_of_jeu_reglement_22(),
	MarkerLayer_t3962172947::get_offset_of_jeu_status_23(),
	MarkerLayer_t3962172947::get_offset_of_jeu_tirage_24(),
	MarkerLayer_t3962172947::get_offset_of_jeu_id_25(),
	MarkerLayer_t3962172947::get_offset_of_jeu_name_26(),
	MarkerLayer_t3962172947::get_offset_of_jeu_reward_msg_27(),
	MarkerLayer_t3962172947::get_offset_of_jeu_link_of_reward_28(),
	MarkerLayer_t3962172947::get_offset_of_jeu_source_gagne_29(),
	MarkerLayer_t3962172947::get_offset_of_jeu_thumb_30(),
	MarkerLayer_t3962172947::get_offset_of_jeu_token_31(),
	MarkerLayer_t3962172947::get_offset_of_jeu_recupMailTirage_32(),
	MarkerLayer_t3962172947::get_offset_of_jeu_source_perdu_33(),
	MarkerLayer_t3962172947::get_offset_of_jeu_thumb_perdu_34(),
	MarkerLayer_t3962172947::get_offset_of_jeu_data_email_35(),
	MarkerLayer_t3962172947::get_offset_of_jeu_data_email_oblig_36(),
	MarkerLayer_t3962172947::get_offset_of_jeu_data_nom_37(),
	MarkerLayer_t3962172947::get_offset_of_jeu_data_nom_oblig_38(),
	MarkerLayer_t3962172947::get_offset_of_jeu_data_prenom_39(),
	MarkerLayer_t3962172947::get_offset_of_jeu_data_prenom_oblig_40(),
	MarkerLayer_t3962172947::get_offset_of_jeu_data_societe_41(),
	MarkerLayer_t3962172947::get_offset_of_jeu_data_societe_oblig_42(),
	MarkerLayer_t3962172947::get_offset_of_jeu_data_telephone_43(),
	MarkerLayer_t3962172947::get_offset_of_jeu_data_telephone_oblig_44(),
	MarkerLayer_t3962172947::get_offset_of_jeu_data_datenaissance_45(),
	MarkerLayer_t3962172947::get_offset_of_jeu_data_datenaissance_oblig_46(),
	MarkerLayer_t3962172947::get_offset_of_jeu_data_extrafield_47(),
	MarkerLayer_t3962172947::get_offset_of_jeu_data_extrafield_oblig_48(),
	MarkerLayer_t3962172947::get_offset_of_jeu_data_str_extrafield_49(),
	MarkerLayer_t3962172947::get_offset_of_son_source_50(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2972 = { sizeof (MarkerManager_t1031803141), -1, sizeof(MarkerManager_t1031803141_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2972[4] = 
{
	MarkerManager_t1031803141::get_offset_of_layerCount_0(),
	MarkerManager_t1031803141::get_offset_of_marker_1(),
	MarkerManager_t1031803141_StaticFields::get_offset_of__Instance_2(),
	MarkerManager_t1031803141_StaticFields::get_offset_of_U3CU3Ef__switchU24map0_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2973 = { sizeof (OBJLoaderManager_t3565543175), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2973[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2974 = { sizeof (U3CloadU3Ec__Iterator0_t1679360452), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2974[4] = 
{
	U3CloadU3Ec__Iterator0_t1679360452::get_offset_of_U3CwwwU3E__0_0(),
	U3CloadU3Ec__Iterator0_t1679360452::get_offset_of_U24current_1(),
	U3CloadU3Ec__Iterator0_t1679360452::get_offset_of_U24disposing_2(),
	U3CloadU3Ec__Iterator0_t1679360452::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2975 = { sizeof (SplashTimer_t2585461764), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2975[8] = 
{
	SplashTimer_t2585461764::get_offset_of_appName_4(),
	SplashTimer_t2585461764::get_offset_of_channelLogo_5(),
	SplashTimer_t2585461764::get_offset_of_delay_6(),
	SplashTimer_t2585461764::get_offset_of_scene_7(),
	SplashTimer_t2585461764::get_offset_of_selectedLanguageID_8(),
	SplashTimer_t2585461764::get_offset_of_selectedCountry_9(),
	SplashTimer_t2585461764::get_offset_of_panel_10(),
	SplashTimer_t2585461764::get_offset_of_needToUseUpdate_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2976 = { sizeof (U3CloadCreditImageU3Ec__Iterator0_t278559497), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2976[11] = 
{
	U3CloadCreditImageU3Ec__Iterator0_t278559497::get_offset_of_U3CurlU3E__0_0(),
	U3CloadCreditImageU3Ec__Iterator0_t278559497::get_offset_of_U3CwwwU3E__0_1(),
	U3CloadCreditImageU3Ec__Iterator0_t278559497::get_offset_of_U3CNbaseU3E__0_2(),
	U3CloadCreditImageU3Ec__Iterator0_t278559497::get_offset_of_U3CwwwimageU3E__0_3(),
	U3CloadCreditImageU3Ec__Iterator0_t278559497::get_offset_of_U3CtextureU3E__0_4(),
	U3CloadCreditImageU3Ec__Iterator0_t278559497::get_offset_of_U3CrecU3E__0_5(),
	U3CloadCreditImageU3Ec__Iterator0_t278559497::get_offset_of_U3CspriteToUseU3E__0_6(),
	U3CloadCreditImageU3Ec__Iterator0_t278559497::get_offset_of_U24this_7(),
	U3CloadCreditImageU3Ec__Iterator0_t278559497::get_offset_of_U24current_8(),
	U3CloadCreditImageU3Ec__Iterator0_t278559497::get_offset_of_U24disposing_9(),
	U3CloadCreditImageU3Ec__Iterator0_t278559497::get_offset_of_U24PC_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2977 = { sizeof (U3CDeleteCacheU3Ec__Iterator1_t475136523), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2977[11] = 
{
	U3CDeleteCacheU3Ec__Iterator1_t475136523::get_offset_of_U3CpathU3E__0_0(),
	U3CDeleteCacheU3Ec__Iterator1_t475136523::get_offset_of_U3CdiU3E__0_1(),
	U3CDeleteCacheU3Ec__Iterator1_t475136523::get_offset_of_U24locvar0_2(),
	U3CDeleteCacheU3Ec__Iterator1_t475136523::get_offset_of_U24locvar1_3(),
	U3CDeleteCacheU3Ec__Iterator1_t475136523::get_offset_of_U3CfileU3E__1_4(),
	U3CDeleteCacheU3Ec__Iterator1_t475136523::get_offset_of_U24locvar2_5(),
	U3CDeleteCacheU3Ec__Iterator1_t475136523::get_offset_of_U24locvar3_6(),
	U3CDeleteCacheU3Ec__Iterator1_t475136523::get_offset_of_U3CdirU3E__2_7(),
	U3CDeleteCacheU3Ec__Iterator1_t475136523::get_offset_of_U24current_8(),
	U3CDeleteCacheU3Ec__Iterator1_t475136523::get_offset_of_U24disposing_9(),
	U3CDeleteCacheU3Ec__Iterator1_t475136523::get_offset_of_U24PC_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2978 = { sizeof (startUpLANG_t1517666504), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2978[1] = 
{
	startUpLANG_t1517666504::get_offset_of_selectedLanguageID_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2979 = { sizeof (U3CGetLanguageJSONU3Ec__Iterator0_t3550365120), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2979[7] = 
{
	U3CGetLanguageJSONU3Ec__Iterator0_t3550365120::get_offset_of_U3CurlU3E__0_0(),
	U3CGetLanguageJSONU3Ec__Iterator0_t3550365120::get_offset_of_U3CwwwU3E__0_1(),
	U3CGetLanguageJSONU3Ec__Iterator0_t3550365120::get_offset_of_U3CmanagerU3E__0_2(),
	U3CGetLanguageJSONU3Ec__Iterator0_t3550365120::get_offset_of_U24this_3(),
	U3CGetLanguageJSONU3Ec__Iterator0_t3550365120::get_offset_of_U24current_4(),
	U3CGetLanguageJSONU3Ec__Iterator0_t3550365120::get_offset_of_U24disposing_5(),
	U3CGetLanguageJSONU3Ec__Iterator0_t3550365120::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2980 = { sizeof (TangentSpaceVisualizer_t140554864), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2980[2] = 
{
	TangentSpaceVisualizer_t140554864::get_offset_of_offset_4(),
	TangentSpaceVisualizer_t140554864::get_offset_of_scale_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2981 = { sizeof (testImage_t2491645257), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2981[3] = 
{
	testImage_t2491645257::get_offset_of_plane_4(),
	testImage_t2491645257::get_offset_of_rawImage_5(),
	testImage_t2491645257::get_offset_of_rawImageLandscape_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2982 = { sizeof (testPlayer_t3643834628), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2982[1] = 
{
	testPlayer_t3643834628::get_offset_of_videoPlayer_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2983 = { sizeof (testRescale_t3860525213), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2983[1] = 
{
	testRescale_t3860525213::get_offset_of_combinedBounds_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2984 = { sizeof (TranslationManager_t363825059), -1, sizeof(TranslationManager_t363825059_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2984[3] = 
{
	TranslationManager_t363825059::get_offset_of_lang_0(),
	TranslationManager_t363825059_StaticFields::get_offset_of_choosenLang_1(),
	TranslationManager_t363825059_StaticFields::get_offset_of__Instance_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2985 = { sizeof (TutoManager_t4000134877), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2985[4] = 
{
	TutoManager_t4000134877::get_offset_of_demo1_4(),
	TutoManager_t4000134877::get_offset_of_demo2_5(),
	TutoManager_t4000134877::get_offset_of_demo3_6(),
	TutoManager_t4000134877::get_offset_of_url_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2986 = { sizeof (ActionLauncher_t253899709), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2986[1] = 
{
	ActionLauncher_t253899709::get_offset_of_m_OnLaunchAction_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2987 = { sizeof (LaunchActionEvent_t1220164713), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2988 = { sizeof (DynamicObject_t314656423), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2988[14] = 
{
	DynamicObject_t314656423::get_offset_of_showTransition_4(),
	DynamicObject_t314656423::get_offset_of_transitionSpeed_5(),
	DynamicObject_t314656423::get_offset_of_onShowTransitionStartAction_6(),
	DynamicObject_t314656423::get_offset_of_onShowTransitionDoneAction_7(),
	DynamicObject_t314656423::get_offset_of_onHideTransitionStartAction_8(),
	DynamicObject_t314656423::get_offset_of_onHideTransitionDoneAction_9(),
	DynamicObject_t314656423::get_offset_of_onTransitionDoneEvt_10(),
	DynamicObject_t314656423::get_offset_of_showTransitionVector_11(),
	DynamicObject_t314656423::get_offset_of_rectTransform_12(),
	DynamicObject_t314656423::get_offset_of_anchorTop_13(),
	DynamicObject_t314656423::get_offset_of_anchorBottom_14(),
	DynamicObject_t314656423::get_offset_of_anchorLeft_15(),
	DynamicObject_t314656423::get_offset_of_anchorRight_16(),
	DynamicObject_t314656423::get_offset_of_elements_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2989 = { sizeof (LaunchActionEvent_t193601726), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2990 = { sizeof (OnTransitionDoneDeleg_t1142753823), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2991 = { sizeof (Transition_t850483325)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2991[5] = 
{
	Transition_t850483325::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2992 = { sizeof (Page_t2586579457), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2992[8] = 
{
	Page_t2586579457::get_offset_of_pageTransition_18(),
	Page_t2586579457::get_offset_of_isFirstPage_19(),
	Page_t2586579457::get_offset_of_previousPages_20(),
	Page_t2586579457::get_offset_of_previousPage_21(),
	Page_t2586579457::get_offset_of_nextPages_22(),
	Page_t2586579457::get_offset_of_nextPage_23(),
	Page_t2586579457::get_offset_of_status_24(),
	Page_t2586579457::get_offset_of_previousTransition_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2993 = { sizeof (Status_t3896441745)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2993[5] = 
{
	Status_t3896441745::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2994 = { sizeof (MenuBurger_t1089396821), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2994[14] = 
{
	MenuBurger_t1089396821::get_offset_of_isOpenAtStart_4(),
	MenuBurger_t1089396821::get_offset_of_status_5(),
	MenuBurger_t1089396821::get_offset_of_videoTransition_6(),
	MenuBurger_t1089396821::get_offset_of_videoOpenTransition_7(),
	MenuBurger_t1089396821::get_offset_of_videoCloseTransition_8(),
	MenuBurger_t1089396821::get_offset_of_menuSpeed_9(),
	MenuBurger_t1089396821::get_offset_of_anchorOpen_10(),
	MenuBurger_t1089396821::get_offset_of_anchorClose_11(),
	MenuBurger_t1089396821::get_offset_of_menuBody_12(),
	MenuBurger_t1089396821::get_offset_of_backgoundBody_13(),
	MenuBurger_t1089396821::get_offset_of_toggleMenuButtonStyle_14(),
	MenuBurger_t1089396821::get_offset_of_iconMenu_15(),
	MenuBurger_t1089396821::get_offset_of_closeMvt_16(),
	MenuBurger_t1089396821::get_offset_of_openMvt_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2995 = { sizeof (MenuBurgerStatus_t3073057943)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2995[5] = 
{
	MenuBurgerStatus_t3073057943::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2996 = { sizeof (OpenURL_t3441221081), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2996[2] = 
{
	OpenURL_t3441221081::get_offset_of_openUrlButton_4(),
	OpenURL_t3441221081::get_offset_of_url_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2997 = { sizeof (PageTransition_t3158442731), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2997[7] = 
{
	PageTransition_t3158442731::get_offset_of_pages_4(),
	PageTransition_t3158442731::get_offset_of_anchorTop_5(),
	PageTransition_t3158442731::get_offset_of_anchorBottom_6(),
	PageTransition_t3158442731::get_offset_of_anchorLeft_7(),
	PageTransition_t3158442731::get_offset_of_anchorRight_8(),
	PageTransition_t3158442731::get_offset_of_CurrentPage_9(),
	PageTransition_t3158442731::get_offset_of_isTransiting_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2998 = { sizeof (U3CGotoSpecificPageU3Ec__AnonStorey0_t632505814), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2998[1] = 
{
	U3CGotoSpecificPageU3Ec__AnonStorey0_t632505814::get_offset_of_currentPage_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2999 = { sizeof (TMP_OpenUrl_t489659597), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2999[2] = 
{
	TMP_OpenUrl_t489659597::get_offset_of_isLanguageSensitive_4(),
	TMP_OpenUrl_t489659597::get_offset_of_urls_5(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
