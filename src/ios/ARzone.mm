//
//  Arzone.m
//  UnityCordovaApp
//
//  Created by richard roussel on 10/08/2018.
//

#import "ARzone.h"

#include "RegisterMonoModules.h"
#include "RegisterFeatures.h"

#include "UnityAppController.h"

@implementation ARzone

static const int constsection = 0;

void UnityInitTrampoline();

- (void)initUnity {
    
    UnityInitStartupTime();
    
    UnityInitTrampoline();
    
    char ** argv;
    argv = (char **)malloc(sizeof(char*));
    argv[0] = (char *)([[[NSBundle mainBundle] executablePath] UTF8String]);
    
    UnityInitRuntime(1, argv);
    
    RegisterMonoModules();
    NSLog(@"-> registered mono modules %p\n", &constsection);
    RegisterFeatures();
    
    // iOS terminates open sockets when an application enters background mode.
    // The next write to any of such socket causes SIGPIPE signal being raised,
    // even if the request has been done from scripting side. This disables the
    // signal and allows Mono to throw a proper C# exception.
    //std::signal(SIGPIPE, SIG_IGN);
    
}

@end

