#!/usr/bin/env node

var path = require('path');
var fs = require('fs');
var xml2js = require('./xml2js');

function loadConfigXMLDoc(filePath) {
    var json = "";
    try {
        var fileData = fs.readFileSync(filePath, 'ascii');
        var parser = new xml2js.Parser();
        parser.parseString(fileData.substring(0, fileData.length), function (err, result)
            { json = result; });
        return json;
    } catch (ex) {
        console.log(ex)
    }
}

function IOSAfterPluginInstall(context) {
	var projDir;		// Path to project
	var pluginDir;		// Path to MFP plugin
    //var platformDir;	// Path to platform

    var rootdir = context.opts.projectRoot;

    var configXMLPath = "config.xml";
    var rawJSON = loadConfigXMLDoc(configXMLPath);
    var name = rawJSON.widget.name[0];


    pluginDir = path.join(rootdir,'plugins',context.opts.plugin.id,'src','ios')

    console.log(pluginDir);

    platformDir = path.join(rootdir,'platforms','ios');


    var pluginAppDelegateFile;			// Path to plugin AppDelegate.m
    var cdvAppDelegateFile;		// Path to app's AppDelegate.m
    var cdvAppDelegateFileBackup;	// Path to AppDelegate.m backup

    var pluginConfigFile;			// Path to plugin build.xconfig
    var cdvConfigFile;		// Path to app's build.xconfig
    var cdvConfigFileBackup;	// Path to build.xconfig backup

    var pluginPCHFile;			// Path to plugin build.xconfig
    var cdvPCHFile;		// Path to app's build.xconfig
    var cdvPCHFileBackup;	// Path to build.xconfig backup
    

    // ================= copying AppDelegate.m =======================================
    console.log("backingup/Copying AppDelegate.m");
    pluginAppDelegateFile = path.join(pluginDir, 'AppDelegate.m');
    cdvAppDelegateFile = path.join(platformDir, name, 'Classes','AppDelegate.m');
    cdvAppDelegateFileBackup = cdvAppDelegateFile + '.bak';


    // Make a backup of AppDelegate.m before replacing with Worklight's version
    if (!fs.existsSync(cdvAppDelegateFileBackup))
        fs.copyFileSync(cdvAppDelegateFile, cdvAppDelegateFileBackup);
    else
        console.log('A back up of AppDelegate.m already exists.');

    fs.copyFileSync(pluginAppDelegateFile, cdvAppDelegateFile);

    // ================= copying AppDelegate.h =======================================
    console.log("backingup/Copying AppDelegate.h");
    pluginAppDelegateFile = path.join(pluginDir, 'AppDelegate.h');
    cdvAppDelegateFile = path.join(platformDir, name, 'Classes','AppDelegate.h');
    cdvAppDelegateFileBackup = cdvAppDelegateFile + '.bak';

    //backup
    if (!fs.existsSync(cdvAppDelegateFileBackup))
        fs.copyFileSync(cdvAppDelegateFile, cdvAppDelegateFileBackup);
    else
        console.log('A back up of AppDelegate.h already exists.');

    // copy
    fs.copyFileSync(pluginAppDelegateFile, cdvAppDelegateFile);

    // ================= copying build.xcconfig =======================================
    console.log("adding buildARZONE.xcconfig");
    pluginConfigFile = path.join(pluginDir, 'buildARZONE.xcconfig');
    cdvConfigFile = path.join(platformDir, 'cordova','buildARZONE.xcconfig');
    cdvConfigFileToModify = path.join(platformDir, 'cordova','build.xcconfig');
    cdvConfigFileBackup = cdvConfigFileToModify + '.bak';

    //backup
    if (!fs.existsSync(cdvConfigFileBackup))
    {
        console.log("backing up build.xcconfig to: "+cdvConfigFileBackup);
        fs.copyFileSync(cdvConfigFileToModify, cdvConfigFileBackup);
    }
    else
        console.log('A back up of build.xcconfig already exists.');

    // copy
    fs.copyFileSync(pluginConfigFile, cdvConfigFile);
    
    // ================= adding lines to build.xconfig =======================================
    /* 
    PROJECT_NAME = {MyProjectName}
    GCC_PREFIX_HEADER = {MyProjectName}/{MyProjectName}-Prefix.pch
    */
    //fs.appendFileSync(cdvConfigFile, 'PROJECT_NAME = '+name+'\n');
    //fs.appendFileSync(cdvConfigFile, 'GCC_PREFIX_HEADER = '+name+'/'+name+'-Prefix.pch\n');

    console.log("modifying build.xcconfig");
    fs.appendFileSync(cdvConfigFileToModify, '#include \"buildARZONE.xcconfig\"\n');

    // ================= copying prefix.pch =======================================
    console.log("backingup/Copying prefix-header.pch");
    var src = path.join(platformDir, name, name+'-Prefix.pch');
    var dest = path.join(platformDir,  'CordovaLib',name, name+'-Prefix.pch');

    pluginPCHFile = path.join(pluginDir, 'prefix.pch');
    cdvPCHFile = src;
    cdvPCHFileBackup = cdvPCHFile + '.bak';


    if (!fs.existsSync(cdvPCHFileBackup))
        fs.copyFileSync(cdvPCHFile, cdvPCHFileBackup);
    else
        console.log('A back up of '+name+'-Prefix.pch already exists.');

    fs.copyFileSync(pluginPCHFile, cdvPCHFile);


    if (!fs.existsSync(path.join(platformDir, 'CordovaLib',name)))
        fs.mkdirSync(path.join(platformDir, 'CordovaLib',name));


    fs.copyFileSync(src, dest);



}

module.exports = IOSAfterPluginInstall;
